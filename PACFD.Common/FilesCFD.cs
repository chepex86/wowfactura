﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PACFD.Common
{
    public static class  FilesCFD
    {
        public static void WriteInUTF8(string fileName, string content)
        {
            try
            {

                FileStream fs = File.Create(fileName);
                byte[] bytes = Encoding.UTF8.GetBytes(content.ToString());

                foreach (byte b in bytes)
                {
                    fs.WriteByte(b);
                }
                fs.Flush();
                fs.Close();


            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] WriteInUTF8 " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

        }
    }
}
