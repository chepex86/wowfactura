﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Common
{
    public static class Encode
    {
        public static string HtmlEncode(this string text)
        {
            if (string.IsNullOrEmpty(text))
            { return text; }

            text = System.Web.HttpUtility.HtmlEncode(text);            
            text = text.Replace("'", "&apos;");
            text = text.Replace("\"", "&quot;");
       
            return text;
        }
    }
}
