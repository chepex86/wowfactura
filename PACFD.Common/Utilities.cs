﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;
using System.ComponentModel;

namespace PACFD.Common
{
    public partial class Utilities
    {
        private static string RemoveOwnerText(string CommandText)
        {
            CommandText = CommandText.ToUpper().Replace("dbo.".ToUpper(), "").Replace("[dbo].".ToUpper(), "");
            CommandText = CommandText.ToUpper().Replace("userprog.".ToUpper(), "").Replace("[userprog].".ToUpper(), "");
            return CommandText;
        }
        public static void RemoveOwnerSqlCommand(Component component)
        {
            if (component.GetType().Name.Contains("TableAdapter"))
            {
                try
                {
                    PropertyInfo adapterProp = component.GetType().GetProperty("CommandCollection", BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.Instance);
                    if (adapterProp != null)
                    {
                        SqlCommand[] commands = adapterProp.GetValue(component, null) as SqlCommand[];
                        if (commands != null)
                        {
                            SqlCommand cmd = null;
                            for (int i = 0; i < commands.Length; i++)
                            {
                                cmd = commands[i];
                                if (cmd != null)
                                {
                                    cmd.CommandText = RemoveOwnerText(cmd.CommandText);
                                }
                            }
                        }
                    }
                }
                catch { }

                try
                {

                    PropertyInfo adapterProp = component.GetType().GetProperty("Adapter", BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.Instance);
                    if (adapterProp != null)
                    {
                        SqlDataAdapter adapter = adapterProp.GetValue(component, null) as SqlDataAdapter;
                        if (adapter != null)
                        {
                            if (adapter.InsertCommand != null)
                                adapter.InsertCommand.CommandText = RemoveOwnerText(adapter.InsertCommand.CommandText);
                            if (adapter.DeleteCommand != null)
                                adapter.DeleteCommand.CommandText = RemoveOwnerText(adapter.DeleteCommand.CommandText);
                            if (adapter.UpdateCommand != null)
                                adapter.UpdateCommand.CommandText = RemoveOwnerText(adapter.UpdateCommand.CommandText);
                            if (adapter.SelectCommand != null)
                                adapter.SelectCommand.CommandText = RemoveOwnerText(adapter.SelectCommand.CommandText);
                        }
                    }
                }
                catch { }
            }
        } 
    }
}
