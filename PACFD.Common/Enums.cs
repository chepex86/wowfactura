﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Common
{
    public enum eRoles
    {
        Administrator = 0,
        SAT = 1,
        AdvancedClient = 2,
        BasicClient = 3,
    }
    /// <summary>
    /// Type of message to write in the log file.
    /// </summary>
    public enum LogFlag
    {
        None = 0,
        Error = 1,
        StackTrace = 2,
        Custom = 4,
        Warning = 6,
        PacTrace = 8,
    }
    /// <summary>
    /// Pac errors
    /// </summary>
    public enum PacSealErrorTypes
    {
        /// <summary>
        /// Nothing it's done with the billing seal.
        /// </summary>
        None = 0,
        /// <summary>
        /// Set sealed success and notification mail send.
        /// </summary>
        SealMailSend = 1,
        /// <summary>
        /// Set sealed success but not notification mail send.
        /// </summary>
        SealNotMailSend = 2,
        ManualSealMailSend = 3,
        ManualSealNotMailSend = 4,
        /// <summary>
        /// Set billing cancelled and mail send.
        /// </summary>
        CancelMailSend = 5,
        /// <summary>
        /// Set billing cancelled but not mail send.
        /// </summary>
        CancelNotMailSend = 6,
        /// <summary>
        /// Set an error state when try sealing the billing.
        /// </summary>
        SealError = 9,
        XmlError = 10,
        CancelError = 11,
        /// <summary>
        /// An unknown state.
        /// </summary>
        Unknown = 999
    }

    public enum TaxesType
    {
        IVATransfer = 0,
        IVADetained,
        ISRDetained,
    }

    public enum TypeFilter
    {
        Report_001 = 0,
        Report_002 = 1,
        Report_003 = 2,
        Report_004 = 5,
        InvoiceReport = 6,
    }

    public enum ExportFormatTypes
    {
        Excel = 0,
        RichText = 1,
        WordForWindows = 2,
        PDF = 3,
    }
}
