﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;


namespace PACFD.Common
{
    public class Cryptography
    {
        private const string DefaultPublicKey = "Un1Vis!t";

        private Cryptography()
        {
        }

        private static string StripNullCharacters(string stringWithNulls)
        {
            if (stringWithNulls == null || stringWithNulls == string.Empty)
                return string.Empty;

            return stringWithNulls.Replace("\0", string.Empty);
        }

        public static string EncryptUnivisitString(string text)
        {
            return EncryptUnivisitString(text, DefaultPublicKey);
        }

        public static string EncryptUnivisitString(string text, string key)
        {
            byte[] bytValue;
            byte[] bytKey;
            byte[] bytEncoded = new byte[0];
            byte[] bytIV = { 121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62 };
            int intLength;
            int intRemaining;
            MemoryStream objMemoryStream = new MemoryStream();
            CryptoStream objCryptoStream = null;
            RijndaelManaged objRijndaelManaged;


            //   **********************************************************************
            //   ******  Strip any null character from string to be encrypted    ******
            //   **********************************************************************

            text = StripNullCharacters(text);

            //   **********************************************************************
            //   ******  Value must be within ASCII range (i.e., no DBCS chars)  ******
            //   **********************************************************************

            bytValue = Encoding.ASCII.GetBytes(text.ToCharArray());

            intLength = key.Length;

            //   ********************************************************************
            //  ******   Encryption Key must be 256 bits long (32 bytes)      ******
            //   ******   If it is longer than 32 bytes it will be truncated.  ******
            //   ******   If it is shorter than 32 bytes it will be padded     ******
            //   ******   with upper-case Xs.                                  ****** 
            //   ********************************************************************

            if (intLength >= 32)
            {
                key = key.Substring(0, 32);
            }
            else
            {
                intLength = key.Length;
                intRemaining = 32 - intLength;
                key = key + new String('X', intRemaining);
            }

            bytKey = Encoding.ASCII.GetBytes(key.ToCharArray());

            objRijndaelManaged = new RijndaelManaged();

            //   ***********************************************************************
            //   ******  Create the encryptor and write value to it after it is   ******
            //   ******  converted into a byte array                              ******
            //   ***********************************************************************

            try
            {
                objCryptoStream = new CryptoStream(
                    objMemoryStream,
                    objRijndaelManaged.CreateEncryptor(bytKey, bytIV),
                    CryptoStreamMode.Write
                    );
                objCryptoStream.Write(bytValue, 0, bytValue.Length);

                objCryptoStream.FlushFinalBlock();

                bytEncoded = objMemoryStream.ToArray();

            }
            catch { }
            finally
            {
                try
                {
                    if (objMemoryStream != null)
                        objMemoryStream.Close();

                    if (objCryptoStream != null)
                        objCryptoStream.Close();
                }
                catch { }
            }

            //   ***********************************************************************
            //   ******   Return encryptes value (converted from  byte Array to   ******
            //   ******   a base64 string).  Base64 is MIME encoding)             ******
            //   ***********************************************************************
            return Convert.ToBase64String(bytEncoded);
        }

        public static string DecryptUnivisitString(string text)
        {
            return DecryptUnivisitString(text, DefaultPublicKey);
        }

        public static string DecryptUnivisitString(string text, string key)
        {
            byte[] bytDataToBeDecrypted;
            byte[] bytTemp;
            byte[] bytIV = { 121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62 };
            RijndaelManaged objRijndaelManaged = new RijndaelManaged();
            MemoryStream objMemoryStream = null;
            CryptoStream objCryptoStream = null;
            byte[] bytDecryptionKey;

            int intLength;
            int intRemaining;
            string strReturnString = string.Empty;

            //   *****************************************************************
            //   ******   Convert base64 encrypted value to byte array      ******
            //   *****************************************************************

            bytDataToBeDecrypted = Convert.FromBase64String(text);

            //   ********************************************************************
            //   ******   Encryption Key must be 256 bits long (32 bytes)      ******
            //   ******   If it is longer than 32 bytes it will be truncated.  ******
            //   ******   If it is shorter than 32 bytes it will be padded     ******
            //   ******   with upper-case Xs.                                  ****** 
            //   ********************************************************************

            intLength = key.Length;

            if (intLength >= 32)
            {
                key = key.Substring(0, 32);
            }
            else
            {
                intRemaining = 32 - intLength;
                key = key + new String('X', intRemaining);
            }

            bytDecryptionKey = Encoding.ASCII.GetBytes(key.ToCharArray());

            bytTemp = new byte[bytDataToBeDecrypted.Length];

            objMemoryStream = new MemoryStream(bytDataToBeDecrypted);

            //   ***********************************************************************
            //   ******  Create the decryptor and write value to it after it is   ******
            //   ******  converted into a byte array                              ******
            //   ***********************************************************************

            try
            {

                objCryptoStream = new CryptoStream(
                    objMemoryStream,
                    objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV),
                    CryptoStreamMode.Read
                    );

                objCryptoStream.Read(bytTemp, 0, bytTemp.Length);

                //objCryptoStream.FlushFinalBlock();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                try
                {
                    if (objMemoryStream != null)
                        objMemoryStream.Close();

                    if (objCryptoStream != null)
                        objCryptoStream.Close();
                }
                catch { }
            }

            //   *****************************************
            //   ******   Return decypted value     ******
            //   *****************************************

            return StripNullCharacters(Encoding.ASCII.GetString(bytTemp));
        }
    }

}
