﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Common
{
    public class ErrorManager
    {
        public clsError Error { get; set; }
        public bool HaveError { get; set; }


        public ErrorManager()
        {
            HaveError = false;
            Error = new clsError("", "");

        }

        public void SetErrorPAC(string message)
        {
            string c = string.Empty, m = string.Empty;
            message = message.ToUpper();
            if (message.Contains("301".ToUpper())
                || message.Contains("302".ToUpper())
                || message.Contains("304".ToUpper())
                || message.Contains("BZ.152".ToUpper())
                || message.Contains("BZ.153".ToUpper())
                || message.Contains("BZ.155".ToUpper())
                || message.Contains("BZ.156".ToUpper())
                || message.Contains("BZ.157".ToUpper())
                || message.Contains("BZ.158".ToUpper())
                || message.Contains("BZ.159".ToUpper())
                || message.Contains("BZ.160".ToUpper())
                || message.Contains("BZ.161".ToUpper())
                || message.Contains("BZ.162".ToUpper())
                || message.Contains("BZ.163".ToUpper())
                || message.Contains("BZ.164".ToUpper())
                || message.Contains("BZ.165".ToUpper()))
            {
                c = "300";
                m = "Error en la estructira del xml";
            }
            else if (message.Contains("BZ.166".ToUpper())
                    || message.Contains("BZ.167".ToUpper()))
            {
                c = "166";
                m = "No es posible generar la cadena original para validar el CFDI, error en el complemento";
            }
            else if (message.Contains("BZ.168".ToUpper()))
            {
                c = "168";
                m = "El servicio de validación de datos del emisor no devolvió una respuesta";
            }
            else if (message.Contains("BZ.170".ToUpper()))
            {
                c = "170";
                m = "No fue posible generar el timbre del CFDI, el servicio de timbrado regreso un error";
            }
            else if (message.Contains("BZ.171".ToUpper()))
            {
                c = "171";
                m = "No fue posible generar el timbre del CFDI, el servicio de timbrado regreso un timbre inválido";
            }
            else if (message.Contains("BZ.172".ToUpper()))
            {
                c = "172";
                m = "No fue posible almacenar el CFDI";
            }
            else if (message.Contains("BZ.169".ToUpper()))
            {
                c = "169";
                m = "No fue posible generar el timbre del CFDI, el servicio de timbrado no devolvió una respuesta";
            }
            else if (message.Contains("BZ.150".ToUpper())
                    || message.Contains("BZ.151".ToUpper()))
            {
                c = "150";
                m = "Error en la conexion con el PAC";
            }
            else if (message.Contains("307".ToUpper()))
            {
                c = "307";
                m = "El CFDI contiene un timbre previo";
            }
            else if (message.Contains("401".ToUpper()))
            {
                c = "401";
                m = "No es posible timbrar el CFDI, se han excedido las 72 horas con las que cuenta, a partir de la generación del mismo, para generarle un timbre. Favor de generar un nuevo CFDI";
            }
            else if (message.Contains("403".ToUpper()))
            {
                c = "403";
                m = "Sólo se pueden timbrar CFDI que hayan sido generados a partir del 01 de enero del 2011";
            }
            else
            {
                c = "999";
                m = message;
            }

            SetCustomError(c, m);
        }



        public void SetErrorBAJIOPAC(string message)
        {
            string c = string.Empty, m = string.Empty;
            message = message.ToUpper();

            if (message.Contains("301".ToUpper()))
            {
                c = "301";
                m = "El CFDI no tiene una estructura XML correcta";
            }
            else if (message.Contains("302".ToUpper()))
            {
                c = "302";
                m = "El sello del emisor no es válido.";
            }
            else if (message.Contains("303".ToUpper()))
            {
                c = "303";
                m = "El Certificado de Sello Digital no corresponde al contribuyente emisor.";
            }
            else if (message.Contains("304".ToUpper()))
            {
                c = "304";
                m = "El certificado se encuentra revocado o caduco.";
            }
            else if (message.Contains("305".ToUpper()))
            {
                c = "305";
                m = "La fecha del CFDI está fuera del rango de la validez del certificado.";
            }
            else if (message.Contains("306".ToUpper()))
            {
                c = "306";
                m = "El certificado usado para generar el sello digital no es un Certificado de Sello Digital.";
            }
            else if (message.Contains("307".ToUpper()))
            {
                c = "307";
                m = "El CFDI ya ha sido timbrado previamente.";
            }
            else if (message.Contains("308".ToUpper()))
            {
                c = "308";
                m = "El certificado utilizado para generar el sello digital no ha sido emitido por el SAT.";
            }
            else if (message.Contains("401".ToUpper()))
            {
                c = "401";
                m = "La fecha del comprobante está fuera del rango de timbrado permitido.";
            }
            else if (message.Contains("402".ToUpper()))
            {
                c = "402";
                m = "El contribuyente no se encuentra dentro del régimen fiscal para emitir CFDI.";
            }
            else if (message.Contains("403".ToUpper()))
            {
                c = "403";
                m = "La fecha de emisión del CFDI no puede ser anterior al 1 de enero de 2011.";
            }
            else if (message.Contains("611".ToUpper()))
            {
                c = "611";
                m = "Los parámetros recibidos están incompletos o no se encuentran donde se esperarían.";
            }
            else if (message.Contains("612".ToUpper()))
            {
                c = "612";
                m = "El archivo XML o alguno de sus atributos está malformado.";
            }
            else if (message.Contains("620".ToUpper()))
            {
                c = "620";
                m = "Permiso denegado.";
            }
            else if (message.Contains("621".ToUpper()))
            {
                c = "621";
                m = "Datos no válidos. La estructura de los datos contenidos en el XML no es válida para CFDI.";
            }
            else if (message.Contains("630".ToUpper()))
            {
                c = "630";
                m = "Se han agotado los timbres de la implementación.";
            }
            else if (message.Contains("631".ToUpper()))
            {
                c = "631";
                m = "Se han agotado los timbres del emisor.";
            }
            else if (message.Contains("632".ToUpper()))
            {
                c = "632";
                m = "Se ha alcanzado el límite de uso justo permitido por transacción.";
            }
            else if (message.Contains("633".ToUpper()))
            {
                c = "633";
                m = "Uso indebido de cuenta de producción en pruebas o cuenta de prueba en producción.";
            }
            else if (message.Contains("500".ToUpper()))
            {
                c = "500";
                m = "Han ocurrido errores que no han permitido completar el proceso de validación / certificación.";
            }
            else if (message.Contains("501".ToUpper()))
            {
                c = "501";
                m = "Ha ocurrido un error de conexión a la base de datos.";
            }
            else if (message.Contains("502".ToUpper()))
            {
                c = "502";
                m = "Ha fallado al intentar recuperar o almacenar información en la base de datos.";
            }
            else if (message.Contains("503".ToUpper()))
            {
                c = "503";
                m = "Se ha alcanzado el límite de licencias de acceso concurrente a la base de datos.";
            }
            else if (message.Contains("601".ToUpper()))
            {
                c = "601";
                m = "Error de autenticación, el nombre de usuario o contraseña son incorrectos.";
            }
            else if (message.Contains("602".ToUpper()))
            {
                c = "602";
                m = "La cuenta de usuario se encuentra bloqueada.";
            }
            else if (message.Contains("603".ToUpper()))
            {
                c = "603";
                m = "La contraseña de la cuenta ha expirado.";
            }
            else if (message.Contains("604".ToUpper()))
            {
                c = "604";
                m = "Se ha superado el número máximo permitido de intentos fallidos de autenticación.";
            }
            else if (message.Contains("605".ToUpper()))
            {
                c = "605";
                m = "El usuario se encuentra inactivo.";
            }
            else if (message.Contains("1401".ToUpper()))
            {
                c = "1401";
                m = "El Namespace del XML no corresponde con el Namespace de CFDI.";
            }
            else if (message.Contains("1402".ToUpper()))
            {
                c = "1402";
                m = "No se encuentran los datos del emisor en el CFDI.";
            }
            else if (message.Contains("1403".ToUpper()))
            {
                c = "1403";
                m = "No se encuentran los datos del receptor en el CFDI.";
            }
            else if (message.Contains("102".ToUpper()))
            {
                c = "102";
                m = "El Resultado de la Digestion.";
            }
            else
            {
                c = "999";
                m = message;
            }

            SetCustomError(c, m);
        }

        public void SetError(string c)
        {
            string m = string.Empty;
            SetCustomError(c, m);
        }

        public void SetCustomError(string c, string m)
        {
            HaveError = true;
            Error.Code = c;
            Error.Message = m;
        }

    }

     public class clsError
    {
        public string Code { get; set; }
        public string Message { get; set; }

        public clsError(string c, string m)
        {
            Code = c;
            Message = m;
        }


    }
}
