﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace PACFD.Common
{
    public static class WebServiceErrorTypeExtension
    {
        /// <summary>
        /// Get the description text of an WebServiceErrorType flag.
        /// </summary>
        /// <param name="value">WebServiceErrorType flag</param>
        /// <returns>If success return a string else flag name.</returns>
        public static string ToDescription(this Enum value)
        {
            Type type = value.GetType();
            MemberInfo[] memInfo = type.GetMember(value.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return value.ToString();
        }
    }

    /// <summary>
    /// Enum used for error code handler.
    /// </summary>
    public enum WebServiceErrorType
    {
        /// <summary>
        /// Not defined error.
        /// </summary>
        [Description("Error no definido.")]
        None = 0,
        /// <summary>
        /// Internal system error.
        /// </summary>
        [Description("Error interno del sistema.")]
        SystemError = 1,
        /// <summary>
        /// Document isn't valid.
        /// </summary>
        [Description("El documento no es valido.")]
        DocumentNull = 2,
        /// <summary>
        /// Node billing not found.
        /// </summary>
        [Description("Nodo 'facturación' no encontrado.")]
        NodeBillingNotFound = 3,
        /// <summary>
        /// Node user not found.
        /// </summary>
        [Description("Nodo 'usuario' no encontrado.")]
        NodeUserNotFound = 4,
        /// <summary>
        /// Node user, attribute name or password not found.
        /// </summary>
        [Description("Nodo 'usuario', atributo 'nombre' o 'contraseña' no encontrado.")]
        NodeUserAttributeUserOrPasswordNotFound = 5,
        /// <summary>
        /// User or password not found.
        /// </summary>
        [Description("Usuario o contraseña no encontrado.")]
        UserOrPasswordNotFound = 6,
        /// <summary>
        /// User or password not valid.
        /// </summary>
        [Description("Usuario o contraseña no valido.")]
        UserOrPasswordNotValid = 7,
        /// <summary>
        /// Inconsistency in billing type.
        /// </summary>
        [Description("Inconsistencia en el tipo de facturador.")]
        BillerBillingError = 8,
        /// <summary>
        /// Billing not found.
        /// </summary>
        [Description("Factura no encontrada. serie: '{0}', folio: '{1}'")]
        BillingNotFound = 9,
        /// <summary>
        /// Billing already cancelled.
        /// </summary>
        [Description("Factura ya cancelada. serie: '{0}', folio: '{1}'.")]
        BillingAlreadyCanceled = 10,
        /// <summary>
        /// The month doesn't exist, insert month form 1 to 12.
        /// </summary>
        [Description("El mes {0} no existe, introduce un mes de 1-12.")]
        MonthNotExist = 11,
        /// <summary>
        /// Not report generated in date.
        /// </summary>
        [Description("No ha sido generado ningun reporte con la fecha {0}/{1}.")]
        NotReportGeneratedInDate = 12,
        /// <summary>
        /// The report is empty, no report generated in date.
        /// </summary>
        [Description("El reporte {0}/{1} esta vacío, no se genero ningun comprobante en esa fecha.")]
        DateReportIsEmpty = 13,
        /// <summary>
        /// Can't load xml document, incorrect format.
        /// </summary>
        [Description("No se pudo cargar el documento, formato incorrecto.")]
        DocumentErrorFormat = 14,
        /// <summary>
        /// Can't load xml document, incorrect format.
        /// </summary>
        [Description("El folio no es valido.")]
        FolioIsNullOrEmpty = 15,
        /// <summary>
        /// Folio or serial not found.
        /// </summary>
        [Description("El folio o la serie no se encontrarón.")]
        FolioOrSerieNotFound = 16,
        /// <summary>
        /// aplicaimpuestos is not valid use 1 or 0.
        /// </summary>
        [Description("El valor de aplicaimpuestos solo puede ser 1 u/o 0. Valor:'{0}'.")]
        BillingApplyTaxError = 17,
        /// <summary>
        /// Qr image is null or empty.
        /// </summary>
        [Description("La imagen del CBB o Qr esta vacia.")]
        BillingQrImageIsNull = 18,
        /// <summary>
        /// Folio or serial not available.
        /// </summary>
        [Description("Folio o serie no disponible.")]
        BillingFolioOrSerialNotAvailable = 19,
        /// <summary>
        /// Price is not valid.
        /// </summary>
        [Description("El precio no es valido. value='{0}'")]
        BillingPriceError = 20,
        /// <summary>
        /// Amount in concept is not valid.
        /// </summary>
        [Description("La cantidad del concepto no es valida. value='{0}'")]
        BillingConceptDetailAmountIsNotValid = 21,
        /// <summary>
        /// Concept code not found.
        /// </summary>
        [Description("El codigo del concepto no fué encontrado. value='{0}'")]
        BillingConceptCodeNotFound = 22,
        /// <summary>
        /// Transfer taxes or denied taxes aren't valid.
        /// </summary>
        [Description("Los impuestos retenidos o transferidos no son validos. value='{0}'")]
        BillingTaxesTotal = 23,
        /// <summary>
        /// Tax name is not valid.
        /// </summary>
        [Description("El nombre del impuesto no es valido. value='{0}'")]
        BillingTaxesNameIsNotValid = 24,
        /// <summary>
        /// Digital certificates aren't valid.
        /// </summary>
        [Description("Los certificados digitales no son validas. value='{0}'")]
        BillingNotValidDigitalCertificates = 25,
        /// <summary>
        /// Billing total is not valid.
        /// </summary>
        [Description("El total no es valido. value='{0}'")]
        BillingTotalIsNotValid = 26,
        /// <summary>
        /// Billing sub total is not valid.
        /// </summary>
        [Description("El sub total no es valido. value='{0}'")]
        BillingSubTotalIsNotValid = 27,
        /// <summary>
        /// Billing type is not valid.
        /// </summary>
        [Description("El tipo de factura no es valido. value='{0}'")]
        BillingInternalBillingType = 28,
        /// <summary>
        /// IsCredit value is not valid.
        /// </summary>
        [Description("El valor de credito no es vaido. value='{0}'")]
        BillingIsCredit = 29,
        /// <summary>
        /// Payment type is not valid.
        /// </summary>
        [Description("El tipo de pago no es valido. value='{0}'")]
        BillingPaymentType = 30,
        /// <summary>
        /// Billing type is not valid.
        /// </summary>
        [Description("El tipo de factura no es valido. value='{0}'")]
        BillingBillingType = 31,
        /// <summary>
        /// Receptor is not fount
        /// </summary>
        [Description("El receptor no fué encontrado. value='{0}'")]
        BillingReceptorNotFound = 32,
        /// <summary>
        /// Discount is not valid.
        /// </summary>
        [Description("El descuento no es valido. value='{0}'")]
        BillingDiscountIsNotValid = 33,
        /// <summary>
        /// Tax type is not valid use "T", "R", "t" or "r".
        /// </summary>
        [Description("El tipo de impuesto no es valido use 'T', 'R', 't' o 'r'. value='{0}'")]
        BillingTaxeTypeNotValid = 34,
        /// <summary>
        /// Billing address is empty
        /// </summary>
        [Description("El campo dirección esta vacío.")]
        BillingAddressIsEmpty = 35,
        /// <summary>
        /// Municipality is empty.
        /// </summary>
        [Description("El campo municipio esta vacío.")]
        BillingMunicipalityIsEmpty = 36,
        /// <summary>
        /// State is empty.
        /// </summary>
        [Description("El campo estado esta vacío.")]
        BillingStateIsEmpty = 37,
        /// <summary>
        /// Country is empty.
        /// </summary>
        [Description("El campo país esta vacío.")]
        BillingCountryIsEmpty = 38,
        /// <summary>
        /// Zip code is empty.
        /// </summary>
        [Description("El codigo postal esta vacío.")]
        BillingZipcodeIsEmpty = 39,
        /// <summary>
        /// RFC is empty.
        /// </summary>
        [Description("El campo RFC esta vacío.")]
        BillingRFCIsEmpty = 40,
        /// <summary>
        /// Name is empty.
        /// </summary>
        [Description("El campo nombre esta vacío.")]
        BillingNameIsEmpty = 41,
        /// <summary>
        /// Billing location is empty
        /// </summary>
        [Description("El campo ciudad esta vacío.")]
        BillingLocationIsEmpty = 42,
        /// <summary>
        /// Zip code is not valid.
        /// </summary>
        [Description("El campo codigo postal no es valido.")]
        BillingZipcodeIsNotValid = 43,
        /// <summary>
        /// RFC is not valid.
        /// </summary>
        [Description("El campo rfc no es valido.")]
        BillingRFCIsNotValid = 44,
        /// <summary>
        /// Billing Type is empty.
        /// </summary>
        [Description("El campo tipo factura esta vacío.")]
        BillingBillingTypeIsEmpty = 45,
        /// <summary>
        /// External folio already excist.
        /// </summary>
        [Description("El campo folio externo ya existe. serie y folio:'{0}'.")]
        BillingExternalFolioAlreadyExist = 46,
        /// <summary>
        /// Send mail failed.
        /// </summary>
        [Description("No se pudo mandar correo electronico. serie y folio:'{0}'.")]
        ErrorSendingMail = 47,
        /// <summary>
        /// Send mail failed.
        /// </summary>
        [Description("No se pudo mandar correo electronico. serie y folio:'{0}'. Destinatario incorrecto")]
        ErrorSendingMailBadFormatVariableTo = 48,
        /// <summary>
        /// Billing porcentage discount is not valid.
        /// </summary>
        [Description("El porcentage de descuento en la concepto no es valido.")]
        BillingPorcentageDiscountIsNotValid = 49,
        /// <summary>
        /// Currency type is not correct.
        /// </summary>
        [Description("El tipo de cambio no es correcto. Verifique el campo ' moneda ' (mxn o usd) y ' tipocambio ' (decimal) "
            + "en la factura y el tipo de moneda permitido para facturar en la empresa.")]
        BillingCurrencyError = 50,
        /// <summary>
        /// Excahneg rate is not valid, is empty or missing.
        /// </summary>
        [Description("El tipo de cambio no es correcto. Campo ' tipocambio ' no es valido, esta vacío o no a sido enviado en la factura.")]
        BillingCurrencyExchagneRateIsNotValidOrEmpty = 51,
        /// <summary>
        /// Currency type is not sopported.
        /// </summary>
        [Description("El tipo de cambio no es soportado.")]
        BillingCurrencyIsNotSopported = 52,
        /// <summary>
        /// Company is not "dicataminada".
        /// </summary>
        [Description("La empresa no es dictaminada.")]
        BillingCurrencyIsNotDictaminated = 53,
        /// <summary>
        /// Biller bill or seal access not valid.
        /// </summary>
        [Description("La empresa no tiene el nivel adecuado para el tipo de comprobante o sellado.")]
        BillerElectronicBillingTypeIsNotValid = 54,
        /// <summary>
        /// Billing is not active.
        /// </summary>
        [Description("Comprobante no activo, no pude ser sellado.")]
        BillingIsNotActive = 55,
        /// <summary>
        /// Error sealing comprobant.
        /// </summary>
        [Description("Falla al sellar comprobante.")]
        SealingFailed = 56,
        /// <summary>
        /// Date time sended is equal or minor to actual date time.
        /// </summary>
        [Description("La fecha {0}/{1} enviada debe ser igual o menor a la fecha actual {2}.")]
        InvalidDate = 57,
        /// <summary>
        /// Company haven't had actually with sopport to generate montly reports for SAT.
        /// </summary>
        [Description("La empresa actualmente no cuenta con soporte para generar reportes mensuales para el SAT.")]
        BillerElectronicBillingTypeForMonthllyReportIsNotValid = 58,

        [Description("El CFDI con el folio externo ya existe. El comprobante era un precomprobante y fue timbrado. Cancele el comprobante para volver a usar el folio externo.")]
        CFDIInsertingErrorExternalFolioActiveAsPrebilling = 59,

        [Description("El folio externo ya existe. Cancele el comprobante para volver ha usar el folio externo.")]
        BillingInsertingErrorExternalFolioActive = 60,
        /// <summary>
        /// Pac is not configured.
        /// </summary>
        [Description("El pac no esta configurado correctamente.")]
        BillingInsertingPacNotConfigured = 61,
        [Description("Codigo de sucursal enviado. No se puede encontrar la sucursal, verifique la configuración.")]
        BillingBranchIsNotValid = 62,

        [Description("La addenda contiene errores.")]
        BillingAddendumFormatError = 63,
        /// <summary>
        /// Fiscal regime is missing.
        /// </summary>
        [Description("El regimen fiscal no esta configurado.")]
        BillingFisicalRegime = 64,
        /// <summary>
        /// Place Dispatch is empty
        /// </summary>
        [Description("El lugar de expedicion esta vacio.")]
        BillingPlaceDispatchIsEmpty = 65,
        /// <summary>
        /// CFD and CFDI update needed.
        /// </summary>
        [Description("A partir del 1 Julio de 2012 los Emisores de CFD 2.0 y CFDI 3.0 deben actualizarse al uso de CFD 2.2 y CFDI 3.2")]
        BillingCfdv2_0AndCfdiv3_0NeedCfdv2_2AndCfdi3_2 = 66,
        /// <summary>
        /// numctapago is not valid
        /// </summary>
        [Description("El campo numctapago debe contener de 4 a 250 caracteres")]
        BillingNumCtaPagoIsNotValid = 67,
    }
}