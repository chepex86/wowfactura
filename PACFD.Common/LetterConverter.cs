﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace PACFD.Common
{
    public enum CurrencyType
    {
        None = 0,
        MXN = 1,
        USD = 2,
    }

    public class LetterConverter
    {
        private const string DOT = ".";
        public LetterConverter()
        {
            //
            // TODO: agregar aquí la lógica del constructor
            //
        }
        public static int InStr(string param, string param2)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            int result = -1;
            if (param.Length > 0)
                result = param.IndexOf(param2);
            //return the result of the operation
            return result;
        }
        public static int InStr(int startIndex, string param, string param2)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            int result = -1;
            if (param.Length > 0)
                result = param.IndexOf(param2, startIndex);
            //return the result of the operation
            return result;
        }
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = (param.Length >= length) ? param.Substring(0, length) : "";
            //return the result of the operation
            return result;
        }
        public static string Trim(string param)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Trim();
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = (param.Length >= length) ? param.Substring(param.Length - length, length) : "";
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }
        public static int Val(string param)
        {
            //#if DEBUG
            //            LogManager.WriteCustom("Convertidor a letras", new Exception(param));
            //#endif
            //LogManager.WriteCustom("Convertidor a letras", new Exception(param));
            return int.Parse(param);
        }
        public static decimal ValF(string param)
        {
              CultureInfo cul = new CultureInfo("en-US");
              return decimal.Parse(param, cul);
        }
        public static int Len(string param)
        {
            return param.Length;
        }
        public string NumberToLetter(string amount, bool toUpper, CurrencyType currency)
        {
            //string numero = amount.ToString("0.00");

            switch (currency)
            {
                default:
                case CurrencyType.None:
                case CurrencyType.MXN:
                    if (toUpper)
                        return NumberToLetter(amount, "PESOS").ToUpper();

                    return NumberToLetter(amount, "PESOS");
                case CurrencyType.USD:
                    if (toUpper)
                        return NumberToLetter(amount, "DOLARES").ToUpper();

                    return NumberToLetter(amount, "DOLARES");
            }
        }
        public string NumberToLetter(decimal amount, CurrencyType currency)
        {
            CultureInfo cul = new CultureInfo("en-Us");
            string numero = Convert.ToString(amount, cul); // = amount.ToString("0.00");
            return NumberToLetter(numero, true, currency);
        }
        public string NumberToLetter(string Number, string Moneda)
        {

            //LogManager.WriteCustom("NumberToLetter", new Exception(Number));
            string s;
            int DecimalPlace;
            string IntPart;
            string Cents;
            //s = Format(Val(Number), "0.00")
            s = string.Format("{0:0.00}", ValF(Number));
            DecimalPlace = s.IndexOf(DOT);
            if (DecimalPlace > 0)
            {
                IntPart = Left(s, DecimalPlace);
                Cents = Left(Mid(s, DecimalPlace + 1, 2), 2);
            }
            else
            {
                IntPart = s;
                Cents = "";
            }
            if (IntPart == "0" || IntPart == "")
            {
                s = "Cero ";
            }
            else if (Len(IntPart) > 7)
            {
                s = IntNumToSpanish(Val(Left(IntPart, Len(IntPart) - 6))) + "Millones " + IntNumToSpanish(Val(Right(IntPart, 6)));

            }
            else
            {
                s = IntNumToSpanish(Val(IntPart));
                if (IntPart.Length > 2 && IntPart.Substring(IntPart.Length - 3) != "100" && s.IndexOf("Ciento") == -1)
                    s = s.Replace("Cien", "Ciento");
            }
            if (Right(s, 9) == "Millones " || Right(s, 7) == "Millón ")
            {
                s = s + "de ";
            }
            switch (s)
            {
                case "Un ":
                case "Una ":
                    s = s + Singular(Moneda);
                    break;
                default:
                    s = s + Moneda;
                    break;
            }
            s = s + " ";
            /*if( Val(Cents)>0 )
            {
                Cents = "con " + IntNumToSpanish(Val(Cents)) + "Centavos";
            }
            else
            {
                Cents = "con Cero Centavos";
            }*/

            string result = "";
            if (Moneda.ToUpper() == "DOLARES".ToUpper())
                result = string.Format("{0} {1:0.##}/100 USD", s.Trim(), Cents);
            else
                result = string.Format("{0} {1:0.##}/100 M.N.", s.Trim(), Cents);
            return result;

            //return (Trim(s + Cents));
        }//

        public string IntNumToSpanish(int numero)
        {
            int ptr;
            int n;
            int i;
            string s;
            string rtn = "";
            string tem;
            s = string.Format("{0}", numero);
            n = Len(s);
            tem = "";
            i = n;
            while (i != 0)
            {
                tem = EvalPart(Val(Mid(s, n - i, 1) + CloneChain(i - 1, "0")));
                if (tem != "Cero")
                {
                    rtn = rtn + tem + " ";
                }
                i = i - 1;
            }
            //Filters
            //filterThousands
            ReplaceAll(ref rtn, " Mil Mil", " Un Mil");
            do
            {
                ptr = InStr(rtn, "Mil ");
                if (ptr >= 0)
                {
                    if (InStr(ptr + 1, rtn, "Mil ") > 0)
                    {
                        ReplaceStringFrom(ref rtn, "Mil ", "", ptr);
                    }
                    else
                    {
                        break;
                    }
                }
                else { break; }
            }
            while (true);
            //filterHundreds
            ptr = 0;
            do
            {
                ptr = InStr(ptr + 1, rtn, "Cien ");
                if (ptr > 0)
                {
                    tem = Left(Mid(rtn, ptr + 5), 1);
                    if (tem == "M" || tem == "")
                    { }
                    else
                    {
                        ReplaceStringFrom(ref rtn, "Cien", "Ciento", ptr);
                    }
                }
            } while (ptr != -1);
            //filterMisc
            ReplaceAll(ref rtn, "Diez Un", "Once");
            ReplaceAll(ref rtn, "Diez Dos", "Doce");
            ReplaceAll(ref rtn, "Diez Tres", "Trece");
            ReplaceAll(ref rtn, "Diez Cuatro", "Catorce");
            ReplaceAll(ref rtn, "Diez Cinco", "Quince");
            ReplaceAll(ref rtn, "Diez Seis", "Dieciseis");
            ReplaceAll(ref rtn, "Diez Siete", "Diecisiete");
            ReplaceAll(ref rtn, "Diez Ocho", "Dieciocho");
            ReplaceAll(ref rtn, "Diez Nueve", "Diecinueve");
            ReplaceAll(ref rtn, "Veinte Un", "Veintiun");
            ReplaceAll(ref rtn, "Veinte Dos", "Veintidos");
            ReplaceAll(ref rtn, "Veinte Tres", "Veintitres");
            ReplaceAll(ref rtn, "Veinte Cuatro", "Veinticuatro");
            ReplaceAll(ref rtn, "Veinte Cinco", "Veinticinco");
            ReplaceAll(ref rtn, "Veinte Seis", "Veintiseís");
            ReplaceAll(ref rtn, "Veinte Siete", "Veintisiete");
            ReplaceAll(ref rtn, "Veinte Ocho", "Veintiocho");
            ReplaceAll(ref rtn, "Veinte Nueve", "Veintinueve");
            //filterOne
            if (Left(rtn, 1) == "M")
            {
                rtn = "Un " + rtn;
            }
            //Un Mil...
            if (Left(rtn, 6) == "Un Mil" && Left(rtn, 7) != "Un Mill")
            {
                rtn = Mid(rtn, 3);
            }
            //addAnd
            for (i = 65; i <= 88; i++)
            {
                if (i != 77)
                {
                    ReplaceAll(ref rtn, "a " + (char)(i), "* y " + (char)(i));
                }
            }
            ReplaceAll(ref rtn, "*", "a");
            return rtn;
        }
        private string EvalPart(int x)
        {
            string rtn;
            string s = "";
            int i = 0;
            do
            {
                switch (x)
                {
                    case 0: s = "Cero"; break;
                    case 1: s = "Un"; break;
                    case 2: s = "Dos"; break;
                    case 3: s = "Tres"; break;
                    case 4: s = "Cuatro"; break;
                    case 5: s = "Cinco"; break;
                    case 6: s = "Seis"; break;
                    case 7: s = "Siete"; break;
                    case 8: s = "Ocho"; break;
                    case 9: s = "Nueve"; break;
                    case 10: s = "Diez"; break;
                    case 20: s = "Veinte"; break;
                    case 30: s = "Treinta"; break;
                    case 40: s = "Cuarenta"; break;
                    case 50: s = "Cincuenta"; break;
                    case 60: s = "Sesenta"; break;
                    case 70: s = "Setenta"; break;
                    case 80: s = "Ochenta"; break;
                    case 90: s = "Noventa"; break;
                    case 100: s = "Cien"; break;
                    case 200: s = "Doscientos"; break;
                    case 300: s = "Trescientos"; break;
                    case 400: s = "Cuatrocientos"; break;
                    case 500: s = "Quinientos"; break;
                    case 600: s = "Seiscientos"; break;
                    case 700: s = "Setecientos"; break;
                    case 800: s = "Ochocientos"; break;
                    case 900: s = "Novecientos"; break;
                    case 1000: s = "Mil"; break;
                    case 1000000: s = "Millón"; break;
                }
                if (s == "")
                {
                    i = i + 1;
                    x = x / 1000;
                    if (x == 0)
                    {
                        i = 0;
                    }
                }
                else
                {
                    break;
                }
            } while (i != 0);

            rtn = s;
            switch (i)
            {
                case 0: s = ""; break;
                case 1: s = " Mil"; break;
                case 2: s = " Millones"; break;
                case 3: s = " Billones"; break;
            }
            return rtn + s;
        }
        private void ReplaceStringFrom(ref string s, string OldWrd, string NewWrd, int ptr)
        {
            s = Left(s, ptr) + NewWrd + Mid(s, Len(OldWrd) + ptr);
        }
        private string Singular(string s)
        {
            if (Len(s) >= 2)
            {
                if (Right(s, 1) == "s")
                {
                    if (Right(s, 2) == "es")
                    {
                        return Left(s, Len(s) - 2);
                    }
                    else
                    {
                        return Left(s, Len(s) - 1);
                    }
                }
                else
                {
                    return s;
                }
            }
            return string.Empty;
        }
        private string CloneChain(int n, string Chr)
        {
            int i;
            string CharClone;
            string rtn = "";
            if (Len(Chr) > 0)
            {
                CharClone = Mid(Chr, 0, 1);
                for (i = 1; i <= n; i++)
                {
                    rtn = rtn + CharClone;
                }
            }
            return rtn;
        }
        private void ReplaceAll(ref string s, string OldWrd, string NewWrd)
        {
            int ptr;
            do
            {
                ptr = InStr(s, OldWrd);
                if (ptr >= 0)
                {
                    s = Left(s, ptr) + NewWrd + Mid(s, Len(OldWrd) + ptr);
                }
            } while (ptr != -1);
        }
    }
}
