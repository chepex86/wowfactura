﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PACFD.Common
{
    /// <summary>
    /// Class used to write messages in a log file.
    /// To configure Log logic/enable a Web.config is needed, add under appSettings 
    /// ( add key = "Log-Enabled" value="true" ), more logic state are public inside
    /// the LogManager class in form of constants variables.
    /// </summary>
    public sealed class LogManager
    {
        const string LogName = "Log.log";
        const string LogTraceName = "TaceLog.log";
        const string LogPacName = "PacLog.log";
        /// <summary>
        /// Indicate the log is enabled, if not enabled the log isn't write.
        /// Web.config / appSettings / add key ="Log-Enabled" value="true"
        /// </summary>
        public const string LogEnabled = "Log-Enabled";
        /// <summary>
        /// Indicate the log path file, if defautl is set, a Log folder is make.
        /// Web.config / appSettings / add key ="Log-Path" value="Default"
        /// </summary>
        public const string LogPath = "Log-Path";
        /// <summary>
        /// Indicate if the webservice can write a log file, not apply in the log class, must be checked out side log class.
        /// Web.config / appSettings / add key ="Log-WebService-Tarce" value="true"
        /// </summary>
        public const string LogTrace = "Log-WebService-Tarce";
        /// <summary>
        /// Enable or disable write log in a separate file.
        /// Web.config / appSettings / add key ="Log-StackTrace-SeparateFile" value="true"
        /// </summary>
        public const string LogStackTraceInSeparateFile = "Log-StackTrace-SeparateFile";

        /// <summary>
        /// Indicate if the pac can write a log file, not apply in the log class, must be checked out side log class.
        /// Web.config / appSettings / add key ="Log-Pac-Tarce" value="true"
        /// </summary>
        public const string LogPacTrace = "Log-Pac-Tarce";


        /// <summary>
        /// Get a boolean value indicating if the log is enabled.
        /// </summary>
        public static bool Enabled
        {
            get
            {
                bool result;
                string s = System.Configuration.ConfigurationManager.AppSettings[LogEnabled] as string;

                if (string.IsNullOrEmpty(s))
                    return false;

                if (!bool.TryParse(s, out result))
                    return false;

                return result;
            }
        }
        /// <summary>
        /// Get a boolean value with the enabled state of the trace in the web service to regis in and outs petitions.
        /// </summary>
        public static bool WebServiceTraceEnable
        {
            get
            {
                bool result;
                string s = System.Configuration.ConfigurationManager.AppSettings[LogTrace] as string;

                if (string.IsNullOrEmpty(s))
                    return false;

                if (!bool.TryParse(s, out result))
                    return false;

                return result;
            }
        }
        /// <summary>
        /// Get a boolean value with the enabled state of the trace in the web service to regis in and outs petitions.
        /// </summary>
        public static bool PacTraceEnable
        {
            get
            {
                bool result;
                string s = System.Configuration.ConfigurationManager.AppSettings[LogPacTrace] as string;

                if (string.IsNullOrEmpty(s))
                    return false;

                if (!bool.TryParse(s, out result))
                    return false;

                return result;
            }
        }
        /// <summary>
        /// Get a boolean value indicating if the stack tace must be write in a separate file.
        /// </summary>
        public static bool StackTraceSeparateFile
        {
            get
            {
                bool result;
                string s = System.Configuration.ConfigurationManager.AppSettings[LogStackTraceInSeparateFile] as string;

                if (string.IsNullOrEmpty(s))
                    return false;

                if (!bool.TryParse(s, out result))
                    return false;

                return result;
            }
        }

        public static void WritePacTrace(string title, string text)
        {
            Write(title, text, LogFlag.PacTrace);
        }

        /// <summary>
        /// Write a custom message in the log.
        /// </summary>
        /// <param name="title">Message title.</param>
        /// <param name="text">Text of the message.</param>        
        public static void WriteCustom(string title, string text)
        {
            Write(title, text, LogFlag.Custom);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="ex"></param>
        public static void WriteCustom(string title, Exception ex)
        {
            Write(title, ex.Message + " " + ex.StackTrace, LogFlag.Custom);
        }
        /// <summary>
        /// Write an error message in the log file.
        /// </summary>
        /// <param name="ex">Exception used to write the log message.</param>
        public static void WriteError(Exception ex)
        {
            if (ex != null && ex.Message != null && ex.StackTrace != null)

                Write(string.Empty, ex.Message + " " + ex.StackTrace, LogFlag.Error);
            else
                Write("viene null algo en WriteError", "viene null algo en WriteError", LogFlag.Error);
        }
        /// <summary>
        /// Write a warning message in the log file.
        /// </summary>
        /// <param name="ex">Exception used to write the log message.</param>
        public static void WriteWarning(Exception ex)
        {
            Write(string.Empty, ex.Message, LogFlag.Warning);
        }
        /// <summary>
        /// Write a stack trace in the log file.
        /// </summary>
        /// <param name="ex">Exception used to write the log message.</param>
        public static void WriteStackTrace(Exception ex)
        {
            Write(string.Empty, ex.Message, LogFlag.StackTrace);
        }
        /// <summary>
        /// Write a message in the log file.
        /// </summary>
        /// <param name="title">Message log title.</param>
        /// <param name="text">Text of the message.</param>
        /// <param name="flag">Type of message to write.</param>
        public static void Write(string title, string text, LogFlag flag)
        {
            string dir = System.Configuration.ConfigurationManager.AppSettings[LogPath] as string;

            if (!Enabled)
                return;

            if (dir.ToLower().Trim() == "default" || string.IsNullOrEmpty(dir))
            {
                if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Request == null)
                {
                    System.Diagnostics.Debug.WriteLine("Log no es accessible: " + text);
                    return;
                }

                dir = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Log";
            }

            //add your logic for stack trace here..

            text = string.Format("{0} {1} {2}",
                    DateTime.Now.ToString("[yyyy/MM/dd hh:mm:ss]"),
                    (flag == LogFlag.Custom || flag == LogFlag.PacTrace) ? title :
                    flag == LogFlag.Error ? "[ERROR]" : flag == LogFlag.StackTrace ? "[StackTrace]" : flag == LogFlag.Warning ? "[Warning]" : string.Empty,
                    text);

            try
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                string fileName = dir + "\\" + DateTime.Now.ToString("yyyyMMdd") + LogName;

                if (flag == LogFlag.StackTrace)
                    fileName = dir + "\\" + DateTime.Now.ToString("yyyyMMdd") + LogTraceName;
                else if (flag == LogFlag.PacTrace)
                    fileName = dir + "\\" + DateTime.Now.ToString("yyyyMMdd") + LogPacName;

                using (StreamWriter w = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate)))
                {
                    w.BaseStream.Seek(w.BaseStream.Length, SeekOrigin.Begin);
                    w.WriteLine(text);
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("[ERROR] Log {0} {1}.", System.Reflection.MethodInfo.GetCurrentMethod().Name, ex.Message));
            }
        }
    }
}
