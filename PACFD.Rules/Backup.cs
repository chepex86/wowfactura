﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.Common;
using PACFD.DataAccess.BackupLocalDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class Backup
    {
        public bool Update(int BillerID, String Name, String Path, bool Status, String ErrorQuery, int UserID, String UserName)
        {
            Backup bUp = new Backup();
            BackupLocalDataSet ds = new BackupLocalDataSet();

            BackupLocalDataSet.BackupLocalRow row = ds._BackupLocal.NewBackupLocalRow();

            row.BillerID = BillerID;
            row.Name = Name;
            row.Path = Path;
            row.Status = Status;
            row.Date = DateTime.Now;
            row.ErrorQuery = ErrorQuery;

            ds._BackupLocal.AddBackupLocalRow(row);

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(UserID, String.Format("El usuario {0} realizo un respaldo de la base de datos.",
                UserName), BillerID, null, ds.GetXml());
            #endregion

            return this.Update(ds._BackupLocal) ? true : false;
        }

        public bool Update(BackupLocalDataSet.BackupLocalDataTable table)
        {
            BackupLocalTableAdapter bUp;

            try
            {
                bUp = new BackupLocalTableAdapter();
                Tool.RemoveOwnerSqlCommand(bUp);
                bUp.Update(table);
                bUp.Dispose();
                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public BackupLocalDataSet.BackupLocal_GetInfoDataTable GetInfo(int BillerID)
        {
            BackupLocalDataSet.BackupLocal_GetInfoDataTable table;

            table = new BackupLocalDataSet.BackupLocal_GetInfoDataTable();
            this.GetInfo(table, BillerID);

            return table;
        }

        public bool GetInfo(BackupLocalDataSet.BackupLocal_GetInfoDataTable table, int BillerID)
        {
            BackupLocal_GetInfoTableAdapter adapter;

            try
            {
                adapter = new BackupLocal_GetInfoTableAdapter();
                Tool.RemoveOwnerSqlCommand(adapter);
                adapter.Fill(table, BillerID);
                adapter.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public BackupLocalDataSet.BackupLocal_GetSuccessPathDataTable GetSuccessPath()
        {
            BackupLocalDataSet.BackupLocal_GetSuccessPathDataTable table;

            table = new BackupLocalDataSet.BackupLocal_GetSuccessPathDataTable();
            this.GetSuccessPath(table);

            return table;
        }

        public bool GetSuccessPath(BackupLocalDataSet.BackupLocal_GetSuccessPathDataTable table)
        {
            BackupLocal_GetSuccessPathTableAdapter adapter;

            try
            {
                adapter = new BackupLocal_GetSuccessPathTableAdapter();
                Tool.RemoveOwnerSqlCommand(adapter);
                adapter.Fill(table);
                adapter.Dispose();
                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
    }
}