﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.Common;
using PACFD.DataAccess.PACFDLogDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Log rules for transactions
    /// </summary>
    public class PACFDLog
    {
        /// <summary>
        /// Fast log transaction access
        /// </summary>
        public static PACFDLog LogManager { get { return new PACFDLog(); } }
        /// <summary>
        /// Add, update or delete a row
        /// </summary>
        /// <param name="table">Table with the row(s) to alter.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFDLogDataSet.PACFDLogDataTable table)
        {
            try
            {
                using (PACFDLogTableAdapter adapter = new PACFDLogTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }

                return true; 
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        /// <summary>
        /// Insert a new log on the data base.
        /// </summary>
        /// <param name="userid">User ID that do the transaction.</param>
        /// <param name="description">Description of the transaction.</param>
        /// <param name="billerid">Biller ID altered.</param>
        /// <param name="datasetbefore">DataSet before been alter.</param>
        /// <param name="datasetafter">DataSet after been alter.</param>
        /// <returns>If success return true else false.</returns>
        public bool Insert(int userid, String description, int? billerid, String datasetbefore, String datasetafter)
        {
            bool result = false;

            using (PACFDLogDataSet.PACFDLogDataTable table = new PACFDLogDataSet.PACFDLogDataTable())
            {
                PACFDLogDataSet.PACFDLogRow row = table.NewPACFDLogRow();

                row.UserID = userid;
                row.DataSetBefore = datasetbefore;
                row.DataSetAfter = datasetafter;
                row.ModificationDate = DateTime.Now;
                row.Description = description;

                if (billerid == null)
                    row.SetBillerIDNull();
                else
                    row.BillerID = (int)billerid;

                table.AddPACFDLogRow(row);
                row.AcceptChanges();
                row.SetAdded();

                result = this.Update(table);
            }

            return result;
        }

        public PACFDLogDataSet.LogViewer_GetBySearchingDataTable GetLogsByFilter(int? userid, int? billerid, DateTime? date, String description, bool? before, bool? after)
        {
            PACFDLogDataSet.LogViewer_GetBySearchingDataTable table;

            table = new PACFDLogDataSet.LogViewer_GetBySearchingDataTable();
            this.GetLogsByFilter(table, userid, billerid, date, description, before, after);

            return table;
        }

        public bool GetLogsByFilter(PACFDLogDataSet.LogViewer_GetBySearchingDataTable table, int? userid, int? billerid, DateTime? date, String description, bool? before, bool? after)
        {
            LogViewer_GetBySearchingTableAdapter adapter;

            try
            {
                using (adapter = new LogViewer_GetBySearchingTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, userid, billerid, date, description, before, after);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
    }
}