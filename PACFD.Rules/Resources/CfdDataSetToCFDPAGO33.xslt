﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" encoding="utf-8" />
  <xsl:template match="/">
    <cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:pago10="http://www.sat.gob.mx/Pagos" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd">
      <xsl:attribute name="Version" >3.3</xsl:attribute>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/Serial != ''">
        <xsl:attribute name="Serie">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/Serial"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/Folio != ''">
        <xsl:attribute name="Folio">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/Folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/BillingDate != ''">
        <xsl:attribute name="Fecha">
          <xsl:variable name="varFecha" select="/BillingsDataSet/BillingsPayments_3/BillingDate"/>
          <xsl:value-of select="substring($varFecha,1,19) "/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/Seal != ''">
        <xsl:attribute name="Sello">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/Seal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/CertificateNumber != ''">
        <xsl:attribute name="NoCertificado">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/CertificateNumber"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/Certificate != ''">
        <xsl:attribute name="Certificado">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/Certificate"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="0 != ''">
        <xsl:attribute name="SubTotal">
          <xsl:value-of select="0"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="0 != ''">
        <xsl:attribute name="Total">
          <xsl:value-of select="0"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/BillingType != ''">
        <xsl:attribute name="TipoDeComprobante">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/BillingType"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/BillingsPayments_3/PlaceDispatch != ''">
        <xsl:attribute name="LugarExpedicion">
          <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/PlaceDispatch"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="'XXX' != ''">
        <xsl:attribute name="Moneda">
          <xsl:value-of select="'XXX'"/>
        </xsl:attribute>
      </xsl:if>
      <cfdi:Emisor>
        <xsl:if test="/BillingsDataSet/Billers/RFC != ''">
          <xsl:attribute name="Rfc">
            <xsl:value-of select="/BillingsDataSet/Billers/RFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billers/Name != ''">
          <xsl:attribute name="Nombre">
            <xsl:value-of select="/BillingsDataSet/Billers/Name"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billers/TaxSystem != ''">
          <xsl:attribute name="RegimenFiscal">
            <xsl:value-of select="/BillingsDataSet/Billers/TaxSystem"/>
          </xsl:attribute>
        </xsl:if>
      </cfdi:Emisor>
      <cfdi:Receptor UsoCFDI="P01">
        <xsl:if test="/BillingsDataSet/Receptors/RFC != ''">
          <xsl:attribute name="Rfc">
            <xsl:value-of select="/BillingsDataSet/Receptors/RFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Receptors/Name != ''">
          <xsl:attribute name="Nombre">
            <xsl:value-of select="/BillingsDataSet/Receptors/Name"/>
          </xsl:attribute>
        </xsl:if>
      </cfdi:Receptor>
      <cfdi:Conceptos>
        <cfdi:Concepto ClaveProdServ="84111506" Cantidad="1" ClaveUnidad="ACT" Descripcion="Pago" ValorUnitario="0" Importe="0"></cfdi:Concepto>
      </cfdi:Conceptos>

      <cfdi:Complemento>
        <pago10:Pagos Version="1.0">
          <pago10:Pago>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/PaymentDate != ''">
              <xsl:attribute name="FechaPago">
                <xsl:variable name="varFecha" select="/BillingsDataSet/BillingsPayments_3/PaymentDate"/>
                <xsl:value-of select="substring($varFecha,1,19) "/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/PaymentForm != ''">
              <xsl:attribute name="FormaDePagoP" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/PaymentForm"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/CurrencyCode != ''">
              <xsl:attribute name="MonedaP" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/CurrencyCode"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="Monto" >
              <xsl:value-of select="format-number(/BillingsDataSet/Billings/Total, '0.00')"/>
            </xsl:attribute>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/Reference != ''">
              <xsl:attribute name="NumOperacion" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/Reference"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/RfcEmisorCtaOrd != ''">
              <xsl:attribute name="RfcEmisorCtaOrd" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/RfcEmisorCtaOrd"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/CtaOrdenante != ''">
              <xsl:attribute name="CtaOrdenante" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/CtaOrdenante"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/RfcEmisorCtaBen != ''">
              <xsl:attribute name="RfcEmisorCtaBen" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/RfcEmisorCtaBen"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/CtaBeneficiario != ''">
              <xsl:attribute name="CtaBeneficiario" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/CtaBeneficiario"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/BankName != ''">
              <xsl:attribute name="NomBancoOrdExt" >
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/BankName"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:for-each select="/BillingsDataSet/Billings">
              <pago10:DoctoRelacionado>
                <xsl:if test="UUID != ''">
                  <xsl:attribute name="IdDocumento" >
                    <xsl:value-of select="UUID"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="CurrencyCode != ''">
                  <xsl:attribute name="MonedaDR" >
                    <xsl:value-of select="CurrencyCode"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="PaymentMethod != ''">
                  <xsl:attribute name="MetodoDePagoDR" >
                    <xsl:value-of select="PaymentMethod"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="Partiality != ''">
                  <xsl:attribute name="NumParcialidad" >
                    <xsl:value-of select="Partiality"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ImpSaldoAnt != ''">
                  <xsl:attribute name="ImpSaldoAnt" >
                    <xsl:value-of select="format-number(ImpSaldoAnt, '0.00')"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ImpSaldoInsoluto != ''">
                  <xsl:attribute name="ImpSaldoInsoluto" >
                    <xsl:value-of select="format-number(ImpSaldoInsoluto, '0.00')"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="ImpPagado  != ''">
                  <xsl:attribute name="ImpPagado" >
                    <xsl:value-of select="format-number(ImpPagado , '0.00')"/>
                  </xsl:attribute>
                </xsl:if>
              </pago10:DoctoRelacionado>
            </xsl:for-each>
          </pago10:Pago>
        </pago10:Pagos>
        <xsl:if test="/BillingsDataSet/BillingsPayments_3/TUUID != ''">
          <tfd:TimbreFiscalDigital xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" >
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/VersionSat != ''">
              <xsl:attribute name="Version">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/VersionSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/UUID != ''">
              <xsl:attribute name="UUID">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/UUID"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/DateStamped != ''">
              <xsl:variable name="varSalida" select="/BillingsDataSet/BillingsPayments_3/DateStamped"/>
              <xsl:attribute name="FechaTimbrado">
                <xsl:value-of select="substring($varSalida,1,19)"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/RfcProvCertic != ''">
              <xsl:attribute name="RfcProvCertif">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/RfcProvCertic"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/SelloCFD != ''">
              <xsl:attribute name="SelloCFD">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/SelloCFD"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/CertificateNumberSat != ''">
              <xsl:attribute name="NoCertificadoSAT">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/CertificateNumberSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/BillingsPayments_3/SealSat != ''">
              <xsl:attribute name="SelloSAT">
                <xsl:value-of select="/BillingsDataSet/BillingsPayments_3/SealSat"/>
              </xsl:attribute>
            </xsl:if>
          </tfd:TimbreFiscalDigital>
        </xsl:if>
      </cfdi:Complemento>
    </cfdi:Comprobante>
  </xsl:template>
</xsl:stylesheet>