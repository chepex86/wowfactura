﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" encoding="utf-8" />

  <xsl:key name="kStmtByKeys" match="/BillingsDataSet/Billings/BillingsDetails" use="concat(TaxType,FactorType,TasaOCuota)"/>

  <xsl:template match="/">
    <cfdi:Comprobante xmlns="http://www.sat.gob.mx/cfd/4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/iedu http://www.sat.gob.mx/sitio_internet/cfd/iedu/iedu.xsd" xmlns:iedu="http://www.sat.gob.mx/iedu" xmlns:cfdi="http://www.sat.gob.mx/cfd/4">
      <xsl:attribute name="Version" >4.0</xsl:attribute>
      <xsl:if test="/BillingsDataSet/Billings/Exportacion != ''">
        <xsl:attribute name="Exportacion">
          <xsl:value-of select="/BillingsDataSet/Billings/Exportacion"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Serial != ''">
        <xsl:attribute name="Serie">
          <xsl:value-of select="/BillingsDataSet/Billings/Serial"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Folio != ''">
        <xsl:attribute name="Folio">
          <xsl:value-of select="/BillingsDataSet/Billings/Folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingDate != ''">
        <xsl:attribute name="Fecha">
          <xsl:variable name="varFecha" select="/BillingsDataSet/Billings/BillingDate"/>
          <xsl:value-of select="substring($varFecha,1,19) "/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Seal != ''">
        <xsl:attribute name="Sello">
          <xsl:value-of select="/BillingsDataSet/Billings/Seal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentForm != ''">
        <xsl:attribute name="FormaPago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentForm"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/CertificateNumber != ''">
        <xsl:attribute name="NoCertificado">
          <xsl:value-of select="/BillingsDataSet/Billings/CertificateNumber"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Certificate != ''">
        <xsl:attribute name="Certificado">
          <xsl:value-of select="/BillingsDataSet/Billings/Certificate"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Certificate != ''">
        <xsl:attribute name="Certificado">
          <xsl:value-of select="/BillingsDataSet/Billings/Certificate"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentTerms != ''">
        <xsl:attribute name="CondicionesDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentTerms"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/SubTotal != ''">
        <xsl:attribute name="SubTotal">
          <xsl:value-of select="format-number(/BillingsDataSet/Billings/SubTotal, '0.00')"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Discount &gt; 0">
        <xsl:attribute name="Descuento">
          <xsl:value-of select="format-number(/BillingsDataSet/Billings/Discount, '0.00')"/>
        </xsl:attribute>
        <xsl:attribute name="motivoDescuento">Descuento</xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Total != ''">
        <xsl:attribute name="Total">
          <xsl:value-of select="format-number(/BillingsDataSet/Billings/Total, '0.00')"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentMethod != ''">
        <xsl:attribute name="MetodoPago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentMethod"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingType != ''">
        <xsl:attribute name="TipoDeComprobante">
          <xsl:value-of select="/BillingsDataSet/Billings/BillingType"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PlaceDispatch != ''">
        <xsl:attribute name="LugarExpedicion">
          <xsl:value-of select="/BillingsDataSet/Billings/PlaceDispatch"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/CurrencyCode != ''">
        <xsl:attribute name="Moneda">
          <xsl:value-of select="/BillingsDataSet/Billings/CurrencyCode"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/ExchangeRateMXN != ''">
        <xsl:variable name="exchange">
          <xsl:choose>
            <xsl:when test="number(/BillingsDataSet/Billings/ExchangeRateMXN) != number(1)">
              <xsl:value-of select="/BillingsDataSet/Billings/ExchangeRateMXN"/>
            </xsl:when>
            <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:attribute name="TipoCambio">
          <xsl:value-of select="$exchange"/>
        </xsl:attribute>
      </xsl:if>

      <xsl:if test="/BillingsDataSet/Billings/BillingGeneralPeriod">
        <!-- Cliente General Nodo de Informacion Global-->
        <cfdi:InformacionGlobal>
          <xsl:attribute name="Periodicidad">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingGeneralPeriod/Period"/>
          </xsl:attribute>
          <xsl:attribute name="Meses">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingGeneralPeriod/Month"/>
          </xsl:attribute>
          <xsl:attribute name="Año">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingGeneralPeriod/Year"/>
          </xsl:attribute>
        </cfdi:InformacionGlobal>
      </xsl:if>


      <cfdi:Emisor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerRFC != ''">
          <xsl:attribute name="Rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerName != ''">
          <xsl:attribute name="Nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerName"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/TaxSystem != ''">
          <xsl:attribute name="RegimenFiscal">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/TaxSystem"/>
          </xsl:attribute>
        </xsl:if>
      </cfdi:Emisor>
      <cfdi:Receptor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC != ''">
          <xsl:attribute name="Rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName != ''">
          <xsl:attribute name="Nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName"/>
          </xsl:attribute>
        </xsl:if>

        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Zipcode != ''">
          <xsl:attribute name="DomicilioFiscalReceptor">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Zipcode"/>
          </xsl:attribute>
        </xsl:if>
        
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/CFDIUse != ''">
          <xsl:attribute name="UsoCFDI">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/CFDIUse"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/TaxSystem != ''">
          <xsl:attribute name="RegimenFiscalReceptor">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/TaxSystem"/>
          </xsl:attribute>
        </xsl:if>
        
        
      </cfdi:Receptor>
      <cfdi:Conceptos>
        <xsl:for-each select="/BillingsDataSet/Billings/BillingsDetails">
          <cfdi:Concepto>
            <xsl:if test="CountString != ''">
              <xsl:attribute name="Cantidad">
                <xsl:value-of select="CountString"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="ClaveProdServ != ''">
              <xsl:attribute name="ClaveProdServ">
                <xsl:value-of select="ClaveProdServ"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Unit != ''">
              <xsl:attribute name="ClaveUnidad">
                <xsl:value-of select="Unit"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="UnitType != ''">
                <xsl:attribute name="Unidad">
                  <xsl:value-of select="UnitType"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="Unidad">No Aplica</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>            
            <xsl:if test="IdentificationNumber != ''">
              <xsl:attribute name="NoIdentificacion">
                <xsl:value-of select="IdentificationNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Description != ''">
              <xsl:attribute name="Descripcion">
                <xsl:value-of select="Description"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="UnitValue != ''">
              <xsl:attribute name="ValorUnitario">
                <xsl:value-of select="UnitValue"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:variable name="amountVar" select="format-number(Amount, '0.00')"/>
            <xsl:if test="Amount != ''">
              <xsl:attribute name="Importe">
                <xsl:value-of select="format-number(Amount, '0.00')"/>
              </xsl:attribute>
            </xsl:if>
              <xsl:attribute name="ObjetoImp">
                <xsl:choose>
                  <xsl:when test="TaxObject != ''">
                    <xsl:value-of select="TaxObject"/>
                  </xsl:when>
                  <xsl:otherwise>02</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            
            <cfdi:Impuestos>
        <!--      <xsl:if test="Amount &gt; 0"> -->
                <cfdi:Traslados>
                    <cfdi:Traslado>
                      <xsl:if test="Amount != ''">
                        <xsl:attribute name="Base">
                          <xsl:value-of select="format-number(Amount, '0.00')"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="TaxType != ''">
                        <xsl:attribute name="Impuesto">
                          <xsl:value-of select="TaxType"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="FactorType != ''">
                        <xsl:attribute name="TipoFactor">
                          <xsl:value-of select="FactorType"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="TasaOCuota != '' and FactorType != 'Exento'">
                        <xsl:attribute name="TasaOCuota">
                          <xsl:value-of select="format-number(TasaOCuota, '0.000000')"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="Tax != '' and FactorType != 'Exento'">
                        <xsl:attribute name="Importe">
                          <xsl:value-of select="format-number(Tax, '0.00')"/>
                        </xsl:attribute>
                      </xsl:if>
                    </cfdi:Traslado>
                </cfdi:Traslados>
                <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained &gt; 0">
                  <cfdi:Retenciones>
                    <xsl:for-each select="/BillingsDataSet/Billings/Taxes/DetainedTaxes">
                      <xsl:variable name="taxName1">
                        <xsl:choose>
                          <xsl:when test="Name = 'IVA'">002</xsl:when>
                          <xsl:when test="Name = 'IEPS'">003</xsl:when>
                          <xsl:otherwise>001</xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>
                      <cfdi:Retencion TipoFactor="Tasa">
                        <xsl:if test="$taxName1 != ''">
                          <xsl:attribute name="Impuesto">
                            <xsl:value-of select="$taxName1"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$amountVar != ''">
                          <xsl:attribute name="Base">
                            <xsl:value-of select="$amountVar"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="Import != ''">
                          <xsl:attribute name="Importe">
                            <xsl:value-of select="format-number(Import, '0.00')"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="TaxRatePercentage != ''">
                          <xsl:attribute name="TasaOCuota">
                            <xsl:value-of select="format-number(TaxRatePercentage div 100, '0.000000')"/>
                          </xsl:attribute>
                        </xsl:if>
                      </cfdi:Retencion>
                    </xsl:for-each>
                  </cfdi:Retenciones>
                </xsl:if>
         <!--     </xsl:if> -->
            </cfdi:Impuestos>
            
            <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/StudentName != ''">
              <cfdi:ComplementoConcepto>
                <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/StudentName != ''">
                  <iedu:instEducativas version="1.0">
                    <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/StudentName != ''">
                      <xsl:attribute name="nombreAlumno">
                        <xsl:value-of select="/BillingsDataSet/Billings/BillingsDetails/StudentName"/>
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/StudentCurp != ''">
                    <xsl:attribute name="CURP">
                      <xsl:value-of select="/BillingsDataSet/Billings/BillingsDetails/StudentCurp"/>
                    </xsl:attribute>
                  </xsl:if>
                    <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/EducationLevel != ''">
                      <xsl:attribute name="nivelEducativo">
                        <xsl:value-of select="/BillingsDataSet/Billings/BillingsDetails/EducationLevel"/>
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="/BillingsDataSet/Billings/BillingsDetails/AutrVOE != ''">
                      <xsl:attribute name="autRVOE">
                        <xsl:value-of select="/BillingsDataSet/Billings/BillingsDetails/AutrVOE"/>
                      </xsl:attribute>
                    </xsl:if>
                  </iedu:instEducativas>
                </xsl:if>
              </cfdi:ComplementoConcepto>
            </xsl:if>
          </cfdi:Concepto>
        </xsl:for-each>
      </cfdi:Conceptos>
      
      
      <cfdi:Impuestos>
        <xsl:if test="/BillingsDataSet/Billings/BillingsDetails[generate-id() = generate-id(key('kStmtByKeys', concat(TaxType,FactorType,TasaOCuota) )[FactorType != 'Exento'])] ">
          <xsl:attribute name="TotalImpuestosTrasladados">
            <xsl:value-of select="format-number(/BillingsDataSet/Billings/Taxes/TotalTransfer, '0.00')"/>
          </xsl:attribute>
        </xsl:if>
        
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained &gt; 0">
           <xsl:attribute name="TotalImpuestosRetenidos">
            <xsl:value-of select="format-number(/BillingsDataSet/Billings/Taxes/TotalDetained, '0.00')"/>
          </xsl:attribute>
        </xsl:if>
          
          <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained &gt; 0">
            <cfdi:Retenciones>
              <xsl:for-each select="/BillingsDataSet/Billings/Taxes/DetainedTaxes">
                <xsl:variable name="taxName1">
                  <xsl:choose>
                    <xsl:when test="Name = 'ISR'">001</xsl:when>
                    <xsl:when test="Name = 'IVA'">002</xsl:when>
                    <xsl:when test="Name = 'IEPS'">003</xsl:when>
                    <xsl:otherwise>002</xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <cfdi:Retencion>
                  <xsl:if test="$taxName1 != ''">
                    <xsl:attribute name="Impuesto">
                      <xsl:value-of select="$taxName1"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="Import != ''">
                    <xsl:attribute name="Importe">
                      <xsl:value-of select="format-number(Import, '0.00')"/>
                    </xsl:attribute>
                  </xsl:if>
                </cfdi:Retencion>
              </xsl:for-each>
            </cfdi:Retenciones>
          </xsl:if>
        <!-- Vamos a ver como funciona con el detail.. -->
        
          <cfdi:Traslados>
            <!-- Agrupacion y suma por Impuesto,TasaOcuota-->
              <xsl:for-each select="/BillingsDataSet/Billings/BillingsDetails[generate-id() = generate-id(key('kStmtByKeys', concat(TaxType,FactorType,TasaOCuota) )[1])] ">
                <xsl:variable name="vkeyGroup" select="key('kStmtByKeys', concat(TaxType,FactorType,TasaOCuota))"/>
                <cfdi:Traslado>
                  <xsl:attribute name="Base">
                    <xsl:value-of select="format-number(sum($vkeyGroup/Amount), '0.00')"/>
                  </xsl:attribute>
                  <xsl:attribute name="Impuesto">
                    <xsl:value-of select="$vkeyGroup/TaxType"/>
                  </xsl:attribute>
                  <xsl:attribute name="TipoFactor">
                    <xsl:value-of select="$vkeyGroup/FactorType"/>
                  </xsl:attribute>
                  <xsl:if test="$vkeyGroup/FactorType !='Exento'">
                      <xsl:attribute name="TasaOCuota">
                      <xsl:value-of select="format-number($vkeyGroup/TasaOCuota, '0.000000')"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$vkeyGroup/FactorType !='Exento' or sum($vkeyGroup/Tax)>0">
                    <xsl:attribute name="Importe">
                      <xsl:value-of select="format-number(sum($vkeyGroup/Tax), '0.00')"/>
                    </xsl:attribute>
                  </xsl:if>
                  </cfdi:Traslado>
            </xsl:for-each>

          </cfdi:Traslados>
      </cfdi:Impuestos>
      <xsl:if test="/BillingsDataSet/Billings/UUID != ''">
        <cfdi:Complemento>
          <tfd:TimbreFiscalDigital xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd"  xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" >
            <xsl:if test="/BillingsDataSet/Billings/VersionSat != ''">
              <xsl:attribute name="Version">
                <xsl:value-of select="/BillingsDataSet/Billings/VersionSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/UUID != ''">
              <xsl:attribute name="UUID">
                <xsl:value-of select="/BillingsDataSet/Billings/UUID"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/DateStamped != ''">
              <xsl:variable name="varSalida" select="/BillingsDataSet/Billings/DateStamped"/>
              <xsl:attribute name="FechaTimbrado">
                <xsl:value-of select="substring($varSalida,1,19)"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/RfcProvCertic != ''">
              <xsl:attribute name="RfcProvCertif">
                <xsl:value-of select="/BillingsDataSet/Billings/RfcProvCertic"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/SelloCFD != ''">
              <xsl:attribute name="SelloCFD">
                <xsl:value-of select="/BillingsDataSet/Billings/SelloCFD"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/CertificateNumberSat!= ''">
              <xsl:attribute name="NoCertificadoSAT">
                <xsl:value-of select="/BillingsDataSet/Billings/CertificateNumberSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/SealSat != ''">
              <xsl:attribute name="SelloSAT">
                <xsl:value-of select="/BillingsDataSet/Billings/SealSat"/>
              </xsl:attribute>
            </xsl:if>
          </tfd:TimbreFiscalDigital>
        </cfdi:Complemento>
      </xsl:if>
    </cfdi:Comprobante>
  </xsl:template>
</xsl:stylesheet>