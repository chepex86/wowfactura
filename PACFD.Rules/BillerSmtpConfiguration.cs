﻿#region Usings
using System;
using PACFD.DataAccess.BillerSmtpConfigurationDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities; 
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class used as a ruler layer for BillerSmtpConfiguration
    /// </summary>
    public class BillerSmtpConfiguration
    {
        /// <summary>
        ///  Update, delete or modify a BillerSmtpConfigurationDataTable.
        /// </summary>
        /// <param name="table">Table used to update, deleted or modify.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable table)
        {
            try
            {
                using (BillerSmtpConfigurationTableAdapter adapter = new BillerSmtpConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }
        /// <summary>
        /// Get a BillerSmtpConfigurationDataTable by the biller ID.
        /// </summary>
        /// <param name="billerid">Biller ID used as filter.</param>
        /// <returns>return a PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable</returns>
        public PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable SelectByBillerID(int billerid)
        {
            PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable table =
                new PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable();

            try
            {
                using (BillerSmtpConfigurationTableAdapter adapter = new BillerSmtpConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(string.Format("[ERROR] {1} {2} {3}", this.GetType().FullName,
                    System.Reflection.MethodInfo.GetCurrentMethod().Name, ex.Message));
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}
