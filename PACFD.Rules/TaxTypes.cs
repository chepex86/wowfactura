﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class TaxTypes
    {
        /// <summary>
        /// Update or insert a taxtype and taxvalue dataset.
        /// </summary>
        /// <param name="dataset">DataSet to be update or insert.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFD.DataAccess.TaxTypesDataSet dataset)
        {
            try
            {
                using (DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.UpdateTax(dataset);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public bool Update(PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable table)
        {
            try
            {
                using (DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxtypeid"></param>
        /// <returns></returns>
        public PACFD.DataAccess.TaxTypesDataSet SelectTaxByTaxTypeID(int taxtypeid)
        {
            PACFD.DataAccess.TaxTypesDataSet dataset = new PACFD.DataAccess.TaxTypesDataSet();

            try
            {
                using (DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter adapter = new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter())
                {
                    //adapter.Fill(dataset.TaxTypes, taxtypeid);
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.SelectTaxByID(dataset, taxtypeid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return dataset;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table with all the available taxtypes in the database.
        /// </summary>
        /// <returns>Return a PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table</returns>
        public PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable SelectAllTaxTypes()
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table =
                new PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesGetAllTableAdapter adapter =
                    new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesGetAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable table by taxtype ID.
        /// </summary>
        /// <param name="taxtypeid">TaxType ID to search for.</param>        
        public PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable SelectByID(int taxtypeid)
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable table =
                new PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtypeid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTypesDataSet.TaxValuesGetByTypeIDDataTable by taxtype ID.
        /// </summary>
        /// <param name="taxtypeid">TaxType ID to search for.</param>        
        public PACFD.DataAccess.TaxTypesDataSet.TaxValuesGetByTypeIDDataTable SelectTaxValueByTaxTypeID(int taxtypeid)
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesGetByTypeIDDataTable table =
                new PACFD.DataAccess.TaxTypesDataSet.TaxValuesGetByTypeIDDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxValuesGetByTypeIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxValuesGetByTypeIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtypeid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }


        /*
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTypesDataSet.TaxType_SelectByBillerIDDataTable table by biller ID.
        /// </summary>
        /// <param name="billerid">Biller ID to search for.</param>        
        private PACFD.DataAccess.TaxTypesDataSet.TaxType_SelectByBillerIDDataTable SelectByBillerID(int billerid)
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxType_SelectByBillerIDDataTable dataset =
                new PACFD.DataAccess.TaxTypesDataSet.TaxType_SelectByBillerIDDataTable();
            DataAccess.TaxTypesDataSetTableAdapters.TaxType_SelectByBillerIDTableAdapter adapter;

            try
            {
                adapter = new PACFD.DataAccess.TaxTypesDataSetTableAdapters.TaxType_SelectByBillerIDTableAdapter();
                adapter.Fill(dataset, billerid);
                adapter.Dispose();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return dataset;
        }*/
    }
}
