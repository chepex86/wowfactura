﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class layer for a excel report operations.
    /// </summary>
    public class BillingExcel
    {
        /// <summary>
        /// Get a list of billings with the payments.
        /// </summary>
        /// <param name="receptorid">Receptor ID as filter.</param>
        /// <param name="initialdate">Optional initial date.</param>
        /// <param name="enddate">Optional end date.</param>
        /// <param name="ispaid">Optional bool paid.</param>
        /// <param name="iscredit">Optional bool is a credit.</param>
        /// <param name="isactive">Optional bool is active.</param>
        /// <returns>DataAccess.BillingExcelDataSet</returns>
        public DataAccess.BillingExcelDataSet GetDataSetForExcel(int receptorid, DateTime? initialdate, DateTime? enddate, bool? ispaid, bool? iscredit, bool? isactive)
        {
            DataAccess.BillingExcelDataSet dataset = new PACFD.DataAccess.BillingExcelDataSet();
            DataAccess.BillingExcelDataSet.BillingsDetailsDataTable detailslist = new PACFD.DataAccess.BillingExcelDataSet.BillingsDetailsDataTable();
            DataAccess.BillingExcelDataSet.BillingPaymentsDataTable paymentslist = new PACFD.DataAccess.BillingExcelDataSet.BillingPaymentsDataTable();

            if (receptorid < 1)
            { return dataset; }

            try
            {
                using (DataAccess.BillingExcelDataSetTableAdapters.BillingsTableAdapter adapter =
                    new PACFD.DataAccess.BillingExcelDataSetTableAdapters.BillingsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(dataset.Billings, receptorid, initialdate, enddate, ispaid, iscredit, isactive);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return dataset;
            }

            if (dataset.Billings.Count < 1)
            { return dataset; }

            try
            {
                using (DataAccess.BillingExcelDataSetTableAdapters.BillingsReceptorsTableAdapter adapter =
                    new PACFD.DataAccess.BillingExcelDataSetTableAdapters.BillingsReceptorsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(dataset.BillingsReceptors, dataset.Billings[0].BillingID);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            if (dataset.Billings.Count > 0)
            {
                for (int i = 0; i < dataset.Billings.Count; i++)
                {
                    using (DataAccess.BillingExcelDataSet.BillingsDetailsDataTable detail = new PACFD.DataAccess.BillingExcelDataSet.BillingsDetailsDataTable())
                    {
                        try
                        {
                            using (DataAccess.BillingExcelDataSetTableAdapters.BillingsDetailsTableAdapter adapter =
                                new PACFD.DataAccess.BillingExcelDataSetTableAdapters.BillingsDetailsTableAdapter())
                            {
                                Tool.RemoveOwnerSqlCommand(adapter);
                                adapter.Fill(detail, dataset.Billings[i].BillingID);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.WriteError(ex);
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }

                        if (detail.Count > 0)
                        {
                            for (int i2 = 0; i2 < detail.Count; i2++)
                            {
                                dataset.BillingsDetails.ImportRow(detail[i2]);
                            }
                        }
                    }
                }

                for (int i = 0; i < dataset.Billings.Count; i++)
                {
                    using (DataAccess.BillingExcelDataSet.BillingPaymentsDataTable pay = new PACFD.DataAccess.BillingExcelDataSet.BillingPaymentsDataTable())
                    {
                        try
                        {
                            using (DataAccess.BillingExcelDataSetTableAdapters.BillingPaymentsTableAdapter adapter =
                                new PACFD.DataAccess.BillingExcelDataSetTableAdapters.BillingPaymentsTableAdapter())
                            {
                                Tool.RemoveOwnerSqlCommand(adapter);
                                adapter.Fill(pay, dataset.Billings[i].BillingID);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.WriteError(ex);
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }

                        if (pay.Count > 0)
                        {
                            for (int i2 = 0; i2 < pay.Count; i2++)
                            {
                                dataset.BillingPayments.ImportRow(pay[i2]);
                            }
                        }
                    }
                }
            }

            paymentslist.Dispose();
            paymentslist = null;
            detailslist.Dispose();
            detailslist = null;

            return dataset;
        }
    }
}
