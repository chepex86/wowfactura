﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Rules.BillingsExceptions;
using Tool = PACFD.Common.Utilities;
using PACFD.DataAccess;
using System.Globalization;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// WebService partial class.
    /// </summary>
    partial class Billings
    {
        /// <summary> 
        /// Registry a billing from a xml. 
        /// If the client doesn't exist then is inserted.
        /// If the concept doesn't exist then is inserted.
        /// </summary>
        /// <param name="e">Arguments with the value to add.</param>
        public PACFD.DataAccess.BillingsDataSet GetBillingFromXMLWebService(BillingFromXMLWebServiceEventArgs e)
        {
            Billings.BillingsHelperWebServiceClass helper = new BillingsHelperWebServiceClass(this);
            PACFD.DataAccess.BillingsDataSet billingdataset = new PACFD.DataAccess.BillingsDataSet();
            PACFD.Rules.Receptors receptor = new Receptors();
            PACFD.Rules.Concepts concepts = new Concepts();
            System.Xml.XmlNode invoicenode, childnode, parentnode, obsNode;
            string stemp = null;
            int id = 0;
            decimal subtotal = 0;
            // se agrego este campo para observaciones personalizadas
            const string token_observacionfields = "observacionFields";
   
            const string token_fieldObs = "fieldObs"; 
            const string token_taxes = "impuesto";
            const string token_concept = "concepto";
            const string token_invoice = "factura";
            const string token_billing = "facturacion";
            const string token_total = "total";
            const string token_subtotal = "subtotal";
            const string token_name = "nombre";
            const string token_rate = "rate";
            const string token_rfc = "rfc";
            const string token_price = "precio";
            //
            const string token_usocfdi = "usocfdi";
            const string token_payform = "formapago";
            const string token_paymethod = "metododepago"; //correccion de error, hubo confucion de documentacion

            //
            const string token_discount = "descuento";
            const string token_iscredit = "acredito";
            const string token_applytax = "aplicaimpuestos";
            const string token_taxtype = "tipo";
            const string token_ammount = "cantidad";
            const string token_code = "codigo";
            const string token_operation = "operacion";
            const string token_documentname = "documento";
            const string token_conditions = "condiciones";
            const string token_externalfolio = "folioexterno";
            const string token_percentagediscount = "porcentajedescuento";
            const string token_branch = "sucursal";
            const string token_printonlydescription = "imprimirsolodescripcion";
            const string token_placedispatch = "lugarexpedicion";
            const string token_numctapago = "numctapago";
            const string token_tipoimpuesto = "tipoimpuesto";
            const string token_tipofactor = "tipofactor";
            const string token_importe = "importe";



            PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow;
            PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow denietaxrow;
            PACFD.DataAccess.BillingsDataSet.TransferTaxesRow transfertaxrow;
            PACFD.DataAccess.ReceptorsDataSet.SearchByRFCDataTable receptortable;
            PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow billingsdetailsrow;
            PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable conceptsearchtable;
            PACFD.DataAccess.BillingsDataSet.BillingsIssuedRow billingsssuedrow = null;
            PACFD.DataAccess.BillingsDataSet.BillingFieldsRow billfields = null;

            
            if (e.Document == null)
                return billingdataset;

            parentnode = e.Document.SelectSingleNode(token_billing);

            if (parentnode == null)
                return billingdataset;
            
            billingrow = billingdataset.Billings.NewBillingsRow();
            
            for (int index = 0; index < parentnode.ChildNodes.Count; index++)
            {
                invoicenode = parentnode.ChildNodes[index];

                if (invoicenode.Name.ToLower() != token_invoice)
                    continue;
               
               
               
                billingrow.ExchangeRateMXN = 1;
                billingrow.Active = true;
                billingrow.BillerID = e.BillerId;
                billingrow.BillingID = index * -1;
                billingrow.BillingDate = DateTime.Now;
                billingrow.ElectronicBillingType = (int)e.ElectronicBillingType;
                billingrow.PrintTemplateID = (new PACFD.Rules.WebServiceAccesses()).GetPrintID(e.BillerId);

                billingrow.Version =
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? "1.0" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD ? "2.0" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ? "2.2" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ? "3.0" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ? "3.2" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ? "3.3" :
                                        e.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0 ? "4.0" : String.Empty;

                billingrow.OriginalString = string.Empty;
                billingrow.Seal = string.Empty;
                billingrow.TaxTemplateName = string.Empty;
                billingrow.DiscountDescription = string.Empty;
                billingrow.CurrencyCode = (new PACFD.Rules.Billers()).GetCurrencyByBillerID(e.BillerId);
                billingrow.PreBilling = false;
                billingrow.InsertOrigin = byte.Parse((e.IsFromWebService ? 2 : 1).ToString());

                helper.SetCurrency(ref billingrow, invoicenode, e);

                //codigo anterior para validad e o i

                //if (invoicenode.Attributes[token_operation] == null || string.IsNullOrEmpty(invoicenode.Attributes[token_operation].Value))
                //    throw new BillingBillingTypeIsEmpty(string.Format("Billing type is not valid. value is \"{0}\" must be \"E\" or \"I\".", string.Empty), index, 3, string.Empty);

                //if (invoicenode.Attributes[token_operation].Value.ToLower() != "e" && invoicenode.Attributes[token_operation].Value.ToLower() != "i")
                //    throw new BillingBillingTypeException(string.Format("Billing type is not valid. value is \"{0}\" must be \"E\" or \"I\".", invoicenode.Attributes[token_operation].Value), index, 3, invoicenode.Attributes[token_operation].Value);

                // AUNQUE HAY MUCHAS OPERACIONES TALES COMO  I,E,T,N,P  VALIDAMOS SOLO E,I
                if ( invoicenode.Attributes[token_operation] == null ||
                     string.IsNullOrEmpty(invoicenode.Attributes[token_operation].Value) ||
                     invoicenode.Attributes[token_operation].Value.ToUpper() != "E" ||
                     invoicenode.Attributes[token_operation].Value.ToUpper() != "I" )
                        throw new BillingBillingTypeIsEmpty(string.Format("Billing type is not valid. value is \"{0}\" must be \"E\" or \"I\".", string.Empty), index, 3, string.Empty);

                // validamos la sucursal
                if (invoicenode.Attributes[token_branch] != null)
                    helper.FillBranch(e.BillerId, invoicenode.Attributes[token_branch].Value, ref billingdataset, ref billingsssuedrow, ref billingrow);

                // validamos la Version CFDI Valida
                if (e.ElectronicBillingType == ElectronicBillingType.CFD2_2 || 
                    e.ElectronicBillingType == ElectronicBillingType.CFDI3_2 || 
                    e.ElectronicBillingType == ElectronicBillingType.CFDI3_3 ||
                    e.ElectronicBillingType == ElectronicBillingType.CFDI4_0)
                {
                    if (invoicenode.Attributes[token_placedispatch] != null && !string.IsNullOrEmpty(invoicenode.Attributes[token_placedispatch].Value))
                        billingrow.PlaceDispatch = invoicenode.Attributes[token_placedispatch].Value;
                    else
                    {
                        helper.SetPlaceDispatch(e, ref billingrow, billingsssuedrow);
                    }
                }


                if (invoicenode.Attributes["Observations"] != null && !string.IsNullOrEmpty(invoicenode.Attributes["Observations"].Value))
                    billingrow.Observations = invoicenode.Attributes["Observations"].Value;

                billingrow.BillingType = invoicenode.Attributes[token_operation].Value.ToUpper();  //forzar a letra mayuscula  // == "e" ? "E" : "I";

                childnode = invoicenode.SelectSingleNode("cliente");

                if (childnode == null)
                    throw new BillingReceptorNotFoundException("Receptor not found.", 0, 0, string.Empty);

                receptortable = receptor.SelectByRFC(e.BillerId, childnode.Attributes[token_rfc].Value);

                string usocfdi = string.Empty;
                if (invoicenode.Attributes[token_usocfdi] == null || invoicenode.Attributes[token_usocfdi].ToString() == "")
                {
                    usocfdi = "P01";
                }
                else
                {
                    usocfdi = invoicenode.Attributes[token_usocfdi] != null ? invoicenode.Attributes[token_usocfdi].Value : "P01";
                }

                if (receptortable.Count < 1)
                {
                    if (!helper.InsertClient(childnode, e.BillerId, usocfdi, ref receptortable))
                        throw new BillingReceptorNotFoundException(string.Format("Receptor not found. value is \"{0}\".", childnode.Attributes[token_rfc].Value), index, 2, childnode.Attributes[token_rfc].Value);
                }
                else
                {
                    helper.UpdateClient(childnode, e.BillerId, receptortable[0].ReceptorID, usocfdi, ref receptortable);
                }

                billingrow.ReceptorID = receptortable[0].ReceptorID;

                ///-
                ///metodo de pago
                ///-
                if (invoicenode.Attributes[token_paymethod] == null)
                {
                    billingrow.PaymentForm = "99";
                }
                else
                {
                    stemp = invoicenode.Attributes[token_paymethod] != null ? invoicenode.Attributes[token_paymethod].Value : string.Empty;

                    billingrow.PaymentForm = stemp;
                }

                if (invoicenode.Attributes[token_payform] == null)
                {
                    billingrow.PaymentMethod = "PUE";
                }
                else
                {
                    stemp = invoicenode.Attributes[token_payform] != null ? invoicenode.Attributes[token_payform].Value : string.Empty;

                    billingrow.PaymentMethod = stemp;
                }

                stemp = string.Empty; //not need this variable anymore

                if (invoicenode.Attributes[token_numctapago] != null && !string.IsNullOrEmpty(invoicenode.Attributes[token_numctapago].Value)
                    && invoicenode.Attributes[token_numctapago].Value.Trim().Length > 0)
                {
                    if (invoicenode.Attributes[token_numctapago].Value.Trim().Length < 4)
                        throw new PACFD.Rules.BillingsExceptions.BillingExceptionBase("NumCtaPago is send but not valid.",
                             0, 0, PACFD.Common.WebServiceErrorType.BillingNumCtaPagoIsNotValid, invoicenode.Attributes[token_numctapago].Value);

                    billingrow.AccountNumberPayment = invoicenode.Attributes[token_numctapago].Value.Trim();
                }

                billingrow.PaymentTerms = invoicenode.Attributes[token_conditions].Value;

                if (invoicenode.Attributes[token_externalfolio] != null)
                {
                    if (!e.IsFromWebService)
                    {
                        if (e.AllowRepeatExternalFolio && !string.IsNullOrEmpty(stemp = helper.ExistExternalFolio(e.BillerId, invoicenode.Attributes[token_externalfolio].Value)))
                            throw new BillingExternalFolioAlreadyExist("External folio already exist.", index, -1, stemp);
                    }
                    else
                    {
                        if (!e.AllowRepeatExternalFolio)
                        {
                            if (!string.IsNullOrEmpty(stemp = helper.ExistExternalFolio(e.BillerId, invoicenode.Attributes[token_externalfolio].Value)))
                            {
                                if (e.ElectronicBillingType == ElectronicBillingType.CFDI)
                                    throw new WebServiceCFDIInsertingErrorExternalFolioActiveAsPrebilling("CFDI external folio is a prebilling."
                                       , invoicenode.Attributes[token_externalfolio].Value
                                       , helper.GetBillingsForExternalFolioOfWebservice(e.BillerId, invoicenode.Attributes[token_externalfolio].Value));

                                //else CFD
                                throw new WebServiceBillingInsertingErrorExternalFolioActive("External folio already exist."
                                       , invoicenode.Attributes[token_externalfolio].Value
                                       , helper.GetBillingsForExternalFolioOfWebservice(e.BillerId, invoicenode.Attributes[token_externalfolio].Value));
                            }
                        }
                    }

                    billingrow.ExternalFolio = invoicenode.Attributes[token_externalfolio].Value.Trim();
                }

                childnode = null;

                if (invoicenode.Attributes[token_iscredit] == null)
                {
                    billingrow.IsCredit = false;
                    billingrow.IsPaid = true;
                }
                else
                {
                    switch (invoicenode.Attributes[token_iscredit].Value)
                    {
                        case "0":
                            billingrow.IsCredit = false;
                            billingrow.IsPaid = true;
                            break;
                        case "1":
                            billingrow.IsCredit = true;
                            billingrow.IsPaid = false;

                            if (billingrow.BillingType == "E")
                            {
                                billingrow.IsCredit = false;
                                billingrow.IsPaid = true;
                            }
                            break;
                        default: throw new BillingCreditException(string.Format("Is credit is not invalid. value is \"{0}\" must be \"0\" or \"1\".", invoicenode.Attributes[token_iscredit].Value), index, 6, invoicenode.Attributes[token_iscredit].Value);
                    }
                }

                if (invoicenode.Attributes[token_documentname] == null || string.IsNullOrEmpty(invoicenode.Attributes[token_documentname].Value.Trim()))
                    throw new BillingInternalBillingTypeException(string.Format("Internal billing type is not valid. Value is \"{0}\" can't be empty.",
                        invoicenode.Attributes[token_documentname] == null ? string.Empty : invoicenode.Attributes[token_documentname].Value), index, 7,
                        invoicenode.Attributes[token_documentname] == null ? string.Empty : invoicenode.Attributes[token_documentname].Value);

                billingrow.InternalBillingType = invoicenode.Attributes[token_documentname].Value;

                if (invoicenode.Attributes[token_subtotal] == null || string.IsNullOrEmpty(invoicenode.Attributes[token_subtotal].Value))
                    throw new BillingSubTotalException(string.Format("Sub total is not valid. Values is \"{0}\". can't be empty or non decimal.",
                        invoicenode.Attributes[token_subtotal] == null ? string.Empty : invoicenode.Attributes[token_subtotal].Value), index, 8,
                        invoicenode.Attributes[token_subtotal] == null ? string.Empty : invoicenode.Attributes[token_subtotal].Value);

                if (!decimal.TryParse(invoicenode.Attributes[token_subtotal].Value, out subtotal))
                    throw new BillingSubTotalException(string.Format("Sub total is not valid. Values is \"{0}\". can't be empty or non decimal.", invoicenode.Attributes[token_subtotal].Value), index, 8, invoicenode.Attributes[token_subtotal].Value);

                billingrow.SubTotal = subtotal;
                subtotal = 0;

                if (invoicenode.Attributes[token_total] == null || string.IsNullOrEmpty(invoicenode.Attributes[token_total].Value))
                    throw new BillingTotalException(string.Format("Total is not valid. Values is \"{0}\". can't be empty or non decimal.",
                        invoicenode.Attributes[token_total] == null ? string.Empty : invoicenode.Attributes[token_total].Value), index, 9,
                        invoicenode.Attributes[token_total] == null ? string.Empty : invoicenode.Attributes[token_total].Value);

                if (!decimal.TryParse(invoicenode.Attributes[token_total].Value, out subtotal))
                    throw new BillingTotalException(string.Format("Total is not valid. Values is \"{0}\". can't be empty or non decimal.", invoicenode.Attributes[token_total].Value), index, 9, invoicenode.Attributes[token_total].Value);

                billingrow.Total = subtotal;
                subtotal = 0;

                if (invoicenode.Attributes[token_discount] != null)
                    if (!decimal.TryParse(invoicenode.Attributes[token_discount].Value, out subtotal))
                        throw new BillingDiscountIsNotValid("Discount in billing is not valid.", index, 10, invoicenode.Attributes[token_discount].Value);

                billingrow.Discount = subtotal;



                if (!this.GetBillingFromStream_FillSerial(ref billingrow, e.BillerId, e.ElectronicBillingType))
                    throw new BillingNotValidSeriesOrFoliosException(string.Format("Serial is not valid."), index, -1, string.Empty);

                if (e.ElectronicBillingType != ElectronicBillingType.CBB && e.ElectronicBillingType != ElectronicBillingType.Indeterminate)
                {
                    if (!this.GetBillingFromStream_DigitalCertificateRowDataSet(ref billingrow, e.BillerId))
                        throw new BillingNotValidDigitalCertificatesException("Not digital certificate available for emisor.", -1, -1, string.Empty);
                }
                else
                {
                    billingrow.OriginalString =
                        billingrow.CertificateNumber =
                        billingrow.Certificate =
                        billingrow.Seal = string.Empty;
                }

                //add the billing row, 
                //to be available
                //for the rows parent 
                //set on other table.

                if (billingrow.PaymentMethod == "PPD")
                    billingrow.IsPaid = false;

                billingdataset.Billings.AddBillingsRow(billingrow);
                billingdataset.Billings[billingdataset.Billings.Count - 1].AcceptChanges();
                billingdataset.Billings[billingdataset.Billings.Count - 1].SetAdded();

                this.GetBillingFromStream_FillBiller(ref billingdataset, e.BillerId);
                this.GetBillingFromStream_ReceptorDataSet(ref billingdataset, receptortable[0].ReceptorID);

                if (e.ElectronicBillingType == ElectronicBillingType.CFD2_2 || e.ElectronicBillingType == ElectronicBillingType.CFDI3_2 || e.ElectronicBillingType == ElectronicBillingType.CFDI3_3 || e.ElectronicBillingType == ElectronicBillingType.CFDI4_0)
                    helper.SetFiscalRegime(e, ref billingdataset);

                if (billingsssuedrow != null)
                {
                    billingsssuedrow.SetParentRow(billingrow);
                    billingdataset.BillingsIssued.AddBillingsIssuedRow(billingsssuedrow);
                }
                
               
                for (int i = 0; i < invoicenode.ChildNodes.Count; i++)
                {
                    childnode = invoicenode.ChildNodes[i];

                    switch (childnode.Name.ToLower())
                    {
                        case token_taxes:
                            if (billingdataset.Taxes.Count < 1)// index)
                            {
                                billingdataset.Taxes.AddTaxesRow(billingdataset.Billings[0], 0, 0);
                                billingdataset.Taxes[0].SetParentRow(billingdataset.Billings[billingdataset.Billings.Count - 1]);
                                billingdataset.Taxes[0].AcceptChanges();
                                billingdataset.Taxes[0].SetAdded();

                            }

                            switch (childnode.Attributes[token_tipoimpuesto].Value.ToUpper())
                            {
                                case "002":
                                    transfertaxrow = billingdataset.TransferTaxes.NewTransferTaxesRow();
                                    transfertaxrow.TransferTaxeID = billingdataset.TransferTaxes.Count * -1;

                                    if (childnode.Attributes[token_name] == null || string.IsNullOrEmpty(childnode.Attributes[token_name].Value))
                                        throw new BillingTaxesNameIsNotValidException(string.Format("Transfer tax is not valid. Value is \"{0}\". can't be empty.",
                                            childnode.Attributes[token_name] == null ? string.Empty : childnode.Attributes[token_name].Value), index, 2,
                                            childnode.Attributes[token_name] == null ? string.Empty : childnode.Attributes[token_name].Value);

                                    transfertaxrow.Name = childnode.Attributes[token_name].Value;

                                    if (childnode.Attributes[token_total] == null || string.IsNullOrEmpty(childnode.Attributes[token_total].Value))
                                        throw new BillingTaxesTotalException(string.Format("Transfer tax total is not valid. Value is \"{0}\".",
                                            childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value), index, 3,
                                            childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value);

                                    if (!decimal.TryParse(childnode.Attributes[token_total].Value, out subtotal))
                                        throw new BillingTaxesTotalException(string.Format("Transfer tax total is not valid. Value is \"{0}\".", childnode.Attributes[token_total].Value), index, 3, childnode.Attributes[token_total].Value);

                                    transfertaxrow.Import = subtotal;
                                    //billingdataset.Taxes[billingdataset.Taxes.Count - 1].TotalTransfer += subtotal;
                                    billingdataset.Taxes[0].TotalTransfer += subtotal;

                                    if (childnode.Attributes[token_rate] == null || string.IsNullOrEmpty(childnode.Attributes[token_rate].Value))
                                        throw new BillingTaxesTotalException(string.Format("Transfer tax rate % is not valid. Value is \"{0}\".",
                                            childnode.Attributes[token_rate] == null ? string.Empty : childnode.Attributes[token_rate].Value), index, 4,
                                            childnode.Attributes[token_rate] == null ? string.Empty : childnode.Attributes[token_rate].Value);

                                    if (!decimal.TryParse(childnode.Attributes[token_rate].Value, out subtotal))
                                        throw new BillingTaxesTotalException(string.Format("Transfer tax rate % is not valid. Value is \"{0}\".",
                                            childnode.Attributes[token_rate].Value), index, 4, childnode.Attributes[token_rate].Value);

                                    transfertaxrow.TaxRatePercentage = subtotal;
                                    transfertaxrow.IvaAffected = false;
                                    transfertaxrow.BillingID = billingdataset.Billings[billingdataset.Billings.Count - 1].BillingID;
                                    billingdataset.TransferTaxes.AddTransferTaxesRow(transfertaxrow);
                                    billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].SetParentRow(billingdataset.Billings[billingdataset.Billings.Count - 1]);
                                    billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].AcceptChanges();
                                    billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].SetAdded();
                                    break;
                                case "003":
                                    denietaxrow = billingdataset.DetainedTaxes.NewDetainedTaxesRow();
                                    denietaxrow.DetainedTaxeID = billingdataset.DetainedTaxes.Count * -1;

                                    if (childnode.Attributes[token_name] == null || string.IsNullOrEmpty(childnode.Attributes[token_name].Value))
                                        throw new BillingTaxesNameIsNotValidException(string.Format("Detained tax is not valid. Value is \"{0}\". can't be empty.",
                                            childnode.Attributes[token_name] == null ? string.Empty : childnode.Attributes[token_name].Value), index, 2,
                                            childnode.Attributes[token_name] == null ? string.Empty : childnode.Attributes[token_name].Value);

                                    denietaxrow.Name = childnode.Attributes[token_name].Value;

                                    if (childnode.Attributes[token_total] == null || string.IsNullOrEmpty(childnode.Attributes[token_total].Value))
                                        throw new BillingTaxesTotalException(string.Format("Detained tax total is not valid. Value is \"{0}\".",
                                          childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value), index, 3,
                                          childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value);

                                    if (!decimal.TryParse(childnode.Attributes[token_total].Value, out subtotal))
                                        throw new BillingTaxesTotalException(string.Format("Detained tax total is not valid. Value is \"{0}\".", childnode.Attributes[token_total].Value), index, 3, childnode.Attributes[token_total].Value);

                                    denietaxrow.Import = subtotal;
                                    //billingdataset.Taxes[billingdataset.Taxes.Count - 1].TotalDetained += subtotal;
                                    billingdataset.Taxes[0].TotalDetained += subtotal;

                                    if (childnode.Attributes[token_rate] == null || string.IsNullOrEmpty(childnode.Attributes[token_rate].Value))
                                        throw new BillingTaxesTotalException(string.Format("Detained tax rate % is not valid. Value is \"{0}\".",
                                            childnode.Attributes[token_rate] == null ? string.Empty : childnode.Attributes[token_rate].Value),
                                            index, 4, childnode.Attributes[token_rate] == null ? string.Empty : childnode.Attributes[token_rate].Value);

                                    if (!decimal.TryParse(childnode.Attributes[token_rate].Value, out subtotal))
                                        throw new BillingTaxesTotalException(string.Format("Detained tax rate % is not valid. Value is \"{0}\".", childnode.Attributes[token_rate].Value), index, 4, childnode.Attributes[token_rate].Value);

                                    denietaxrow.TaxRatePercentage = subtotal;
                                    denietaxrow.IvaAffected = true;
                                    denietaxrow.BillingID = billingdataset.Billings[billingdataset.Billings.Count - 1].BillingID;
                                    billingdataset.DetainedTaxes.AddDetainedTaxesRow(denietaxrow);
                                    billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].SetParentRow(billingdataset.Billings[billingdataset.Billings.Count - 1]);
                                    billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].AcceptChanges();
                                    billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].SetAdded();
                                    break;
                                default:
                                    throw new PACFD.Rules.BillingsExceptions.BillingTaxeTypeNotValid("Tax type is not valid, use \"T\", \"R\", \"t\" or \"r\". Value is {0}", index, 5, childnode.Attributes[token_taxtype].Value);
                            }
                            break;
                        case token_concept:
                            conceptsearchtable = new PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable();
                            concepts.SearchByCode(conceptsearchtable, e.BillerId, childnode.Attributes[token_code].Value);

                            if (conceptsearchtable.Count < 1)
                            {
                                if (!helper.InsertConcept(childnode, e.BillerId, ref conceptsearchtable, e))
                                    throw new BillingConceptCodeNotFoundException(string.Format("Concept code not valid or not found. Value is \"{0}\".", childnode.Attributes[token_code].Value), index, 2, childnode.Attributes[token_code].Value);
                            }
                            else
                            {
                                if (!helper.IfConceptIsDifferentUpdate(childnode, ref conceptsearchtable))
                                    throw new BillingConceptCodeNotFoundException(string.Format("Concept code not valid or not found. Value is \"{0}\".", childnode.Attributes[token_code].Value), index, 2, childnode.Attributes[token_code].Value);
                            }

                            bool PrintOnlyDescription = false;
                            if (childnode.Attributes[token_printonlydescription] != null && !string.IsNullOrEmpty(childnode.Attributes[token_printonlydescription].Value)
                                && (childnode.Attributes[token_printonlydescription].Value == "1" || childnode.Attributes[token_printonlydescription].Value.ToUpper() == "TRUE"))
                            {
                                PrintOnlyDescription = true;
                            }

                            billingsdetailsrow = billingdataset.BillingsDetails.NewBillingsDetailsRow();
                            billingsdetailsrow.BillingID = billingdataset.Billings[billingdataset.Billings.Count - 1].BillingID;
                            billingsdetailsrow.BillingsDetailsID = billingdataset.BillingsDetails.Count * -1;
                            billingsdetailsrow.ConceptID = conceptsearchtable[0].ConceptID;
                            billingsdetailsrow.PrintOnlyDescription = PrintOnlyDescription;

                            if (childnode.Attributes[token_ammount] == null || string.IsNullOrEmpty(childnode.Attributes[token_ammount].Value))
                                throw new BillingConceptDetailCountException(string.Format("Concept amount is not valid. Value is \"{0}\".",
                                    childnode.Attributes[token_ammount] == null ? string.Empty : childnode.Attributes[token_ammount].Value),
                                    index, 4, childnode.Attributes[token_ammount] == null ? string.Empty : childnode.Attributes[token_ammount].Value);

                            if (!int.TryParse(childnode.Attributes[token_ammount].Value, out id))
                                throw new BillingConceptDetailCountException(string.Format("Concept amount is not valid. Value is \"{0}\".", childnode.Attributes[token_ammount].Value), index, 4, childnode.Attributes[token_ammount].Value);

                            billingsdetailsrow.Count = id;
                            billingsdetailsrow.CountString = billingsdetailsrow.Count.ToString("0.####");

                            switch (childnode.Attributes[token_applytax].Value)
                            {
                                case "0": billingsdetailsrow.AppliesTax = false; break;
                                case "1": billingsdetailsrow.AppliesTax = true; break;
                                default: throw new BillingConceptApplyTaxesException(string.Format("Concept apply tax is not valid. Value is \"{0}\".", childnode.Attributes[token_applytax].Value), index, 3, childnode.Attributes[token_applytax].Value);
                            }

                            if (childnode.Attributes[token_percentagediscount] != null && !string.IsNullOrEmpty(childnode.Attributes[token_percentagediscount].Value))
                            {
                                subtotal = 0;

                                if (!decimal.TryParse(childnode.Attributes[token_percentagediscount].Value, out subtotal))
                                    throw new PACFD.Rules.BillingsExceptions.BillingPorcentageDiscountIsNotValid(string.Format("Discount is not valid. Value = \"{0}\"", childnode.Attributes[token_percentagediscount].Value), index, 4, childnode.Attributes[token_percentagediscount].Value);

                                billingsdetailsrow.DiscountPercentage = subtotal;
                            }
                            else
                            {
                                billingsdetailsrow.DiscountPercentage = 0;
                            }

                            billingsdetailsrow.IdentificationNumber = conceptsearchtable[0].Code;

                            if (childnode.Attributes[token_price] == null || string.IsNullOrEmpty(childnode.Attributes[token_price].Value))
                                throw new BillingPriceException(string.Format("Price is not valid or can't be empty. Value is \"{0}\".",
                                   childnode.Attributes[token_price] == null ? string.Empty : childnode.Attributes[token_price].Value),
                                   index, 6, childnode.Attributes[token_price] == null ? string.Empty : childnode.Attributes[token_price].Value);

                            if (!decimal.TryParse(childnode.Attributes[token_price].Value, out subtotal))
                                throw new BillingPriceException(string.Format("Price is not valid or can't be empty. Value is \"{0}\".",
                                    childnode.Attributes[token_price] == null ? string.Empty : childnode.Attributes[token_price].Value),
                                    index, 6, childnode.Attributes[token_price] == null ? string.Empty : childnode.Attributes[token_price].Value);

                            billingsdetailsrow.UnitValue = subtotal;

                            if (childnode.Attributes[token_total] == null || string.IsNullOrEmpty(childnode.Attributes[token_total].Value))
                                throw new BillingPriceException(string.Format("Total is not valid or can't be empty. Value is \"{0}\".",
                                    childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value),
                                    index, 7, childnode.Attributes[token_total] == null ? string.Empty : childnode.Attributes[token_total].Value);

                            if (!decimal.TryParse(childnode.Attributes[token_total].Value, out subtotal))
                                throw new BillingPriceException(string.Format("Total is not valid or can't be empty. Value is \"{0}\".", childnode.Attributes[token_total].Value), index, 7, childnode.Attributes[token_total].Value);

                            billingsdetailsrow.Amount = subtotal;
                            billingsdetailsrow.Unit = conceptsearchtable[0].UnitType;
                            billingsdetailsrow.Description = conceptsearchtable[0].Description;
                            billingsdetailsrow.TaxType = childnode.Attributes[token_tipoimpuesto] == null ? string.Empty : childnode.Attributes[token_tipoimpuesto].Value;
                            billingsdetailsrow.FactorType = childnode.Attributes[token_tipofactor] == null ? string.Empty : childnode.Attributes[token_tipofactor].Value;
                            billingsdetailsrow.ClaveProdServ = conceptsearchtable[0].IsClavProdServNull() || string.IsNullOrEmpty(conceptsearchtable[0].ClavProdServ) ? "01010101" : conceptsearchtable[0].ClavProdServ;

                            billingsdetailsrow.UnitType = conceptsearchtable[0].UnitType.GetClaveUnidadName(); 
                            if (decimal.TryParse(childnode.Attributes[token_importe].Value, out subtotal))
                                billingsdetailsrow.Tax = subtotal;

                            if (decimal.TryParse(childnode.Attributes[token_rate].Value, out subtotal))
                                billingsdetailsrow.TasaOCuota = (subtotal / 100).ToString();

                            billingdataset.BillingsDetails.AddBillingsDetailsRow(billingsdetailsrow);
                            billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].SetParentRow(billingdataset.Billings[billingdataset.Billings.Count - 1]);
                            billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].AcceptChanges();
                            billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].SetAdded();
                            break;
                    }

                    childnode = null;
                }

            }

           
            // esto es para los fields de observacion
            System.Xml.XmlNodeList nds = parentnode.SelectNodes("/facturacion/observacionFields/fieldObs");

           
                for (int o = 0; o < nds.Count; o++)
                {
                    obsNode = nds[o];
                    if (obsNode.Name.ToLower() == token_fieldObs.ToLower())
                    {
                        billfields = billingdataset.BillingFields.NewBillingFieldsRow();
                        billfields.SetParentRow(billingrow);
                        billfields.FName = obsNode.Attributes["name"].Value;
                        billfields.FValue = obsNode.Attributes["value"].Value;
                        billingdataset.BillingFields.AddBillingFieldsRow(billfields);
                    }
                }

            return billingdataset;
        }

        public string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        /// <summary>
        /// Get a data set formed by the stream file.
        /// </summary>
        /// <param name="input">Stream with the billings to parse.</param>
        /// <returns>if success return a PACFD.DataAccess.BillingsDataSet</returns>
        public PACFD.DataAccess.BillingsDataSet GetBillingFromStream(System.IO.Stream input, int billerid, ElectronicBillingType elbilltype)
        {
            string line;
            string[] splitline;
            int index = -1;
            System.IO.StreamReader reader = new System.IO.StreamReader(input);
            PACFD.DataAccess.BillingsDataSet billingdataset = new PACFD.DataAccess.BillingsDataSet();
            PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow;
            PACFD.DataAccess.ReceptorsDataSet.SearchByRFCDataTable receptortable;
            PACFD.Rules.Receptors receptor = new Receptors();
            ExceptionCollection exceptionlist = new ExceptionCollection();
            decimal subtotal = 0;
            PACFD.DataAccess.BillingsDataSet.TransferTaxesRow transfertaxrow;
            PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow denietaxrow;
            PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable conceptsearchtable;
            PACFD.Rules.Concepts concepts = new Concepts();
            PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow billingsdetailsrow;
            int id;
            bool concepterro = true;

            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                splitline = line.Split('|');

                if (splitline.Length < 1)
                    continue;

                switch (splitline[0].ToUpper())
                {
                    case HEADER_STREAM_CONCEPT: //billing
                        index++;

                        receptortable = receptor.SelectByRFC(billerid, splitline[1]);

                        if (receptortable.Count < 1)
                            throw new BillingReceptorNotFoundException(string.Format("Receptor not found. RFC value is \"{0}\".", splitline[1]), index, 2, splitline[1]);

                        billingrow = billingdataset.Billings.NewBillingsRow();
                        billingrow.Active = true;
                        billingrow.BillerID = billerid;
                        billingrow.BillingID = index * -1;
                        billingrow.BillingDate = DateTime.Now;

                        if (splitline[2].ToLower() != "e" && splitline[2].ToLower() != "i")
                            throw new BillingBillingTypeException(string.Format("Billing type is not valid. value is \"{0}\" must be \"E\" or \"I\".", splitline[2]), index, 3, splitline[2]);

                        billingrow.BillingType = splitline[2].ToLower() == "e" ? "egreso" : "ingreso";
                        billingrow.ReceptorID = receptortable[0].ReceptorID;

                        switch (splitline[3])
                        {
                            case "1": billingrow.PaymentMethod = "Efectivo"; break;
                            case "2": billingrow.PaymentMethod = "Tarjeta"; break;
                            case "3": billingrow.PaymentMethod = "Cheque"; break;
                            default: throw new BillingPaymentTypeException(string.Format("Payment method is not valid. value is \"{0}\" must be \"1\", \"2\", \"3\".", splitline[3]), index, 4, splitline[3]);
                        }

                        billingrow.PaymentTerms = splitline[4];
                        billingrow.PaymentForm = "Pago en una sola exhibicion";

                        switch (splitline[5])
                        {
                            case "0":
                                billingrow.IsCredit = false;
                                billingrow.IsPaid = true;
                                break;
                            case "1":
                                billingrow.IsCredit = true;
                                billingrow.IsPaid = false;

                                if (billingrow.BillingType == "egreso")
                                {
                                    billingrow.IsCredit = false;
                                    billingrow.IsPaid = true;
                                }
                                break;
                            default: throw new BillingCreditException(string.Format("Is credit is not invalid. value is \"{0}\" must be \"0\" or \"1\".", splitline[5]), index, 6, splitline[6]);
                        }

                        if (string.IsNullOrEmpty(splitline[6].Trim()))
                            throw new BillingInternalBillingTypeException(string.Format("Internal billing type is not valid. Value is \"{0}\" can't be empty.", splitline[6]), index, 7, splitline[6]);

                        billingrow.InternalBillingType = splitline[6];

                        if (string.IsNullOrEmpty(splitline[7]))
                            throw new BillingSubTotalException(string.Format("Sub total is not valid. Values is \"{0}\". can't be empty or non decimal.", splitline[7]), index, 8, splitline[7]);

                        if (!decimal.TryParse(splitline[7], out subtotal))
                            throw new BillingSubTotalException(string.Format("Sub total is not valid. Values is \"{0}\". can't be empty or non decimal.", splitline[7]), index, 8, splitline[7]);

                        billingrow.SubTotal = subtotal;

                        if (string.IsNullOrEmpty(splitline[8]))
                            throw new BillingTotalException(string.Format("Total is not valid. Values is \"{0}\". can't be empty or non decimal.", splitline[8]), index, 9, splitline[8]);

                        if (!decimal.TryParse(splitline[8], out subtotal))
                            throw new BillingTotalException(string.Format("Total is not valid. Values is \"{0}\". can't be empty or non decimal.", splitline[8]), index, 9, splitline[8]);

                        billingrow.Total = subtotal;
                        billingrow.CurrencyCode = (new PACFD.Rules.Billers()).GetCurrencyByBillerID(billerid);
                        //this.GetBillingFromXMLWebService_GetCurrencyByBillerID(billerid);//"MXN";
                        billingrow.PrintTemplateID = (new PACFD.Rules.WebServiceAccesses()).GetPrintID(billerid);
                        billingrow.PreBilling = false;

                        billingrow.Version =
                            elbilltype == PACFD.Rules.ElectronicBillingType.CBB ? "1.0" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFD ? "2.0" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFD2_2 ? "2.2" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFDI ? "3.0" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFDI3_2 ? "3.2" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFDI3_3 ? "3.3" :
                            elbilltype == PACFD.Rules.ElectronicBillingType.CFDI4_0 ? "4.0" :

                            String.Empty;

                        billingrow.DiscountDescription =
                            billingrow.TaxTemplateName =
                            billingrow.OriginalString =
                            billingrow.Seal = string.Empty;
                        billingrow.Discount = 0;

                        if (!this.GetBillingFromStream_FillSerial(ref billingrow, billerid, elbilltype))
                            throw new BillingNotValidSeriesOrFoliosException(string.Format("Serial is not valid. "), index, -1, string.Empty);

                        if (elbilltype != ElectronicBillingType.CBB && elbilltype != ElectronicBillingType.Indeterminate)
                        {
                            if (!this.GetBillingFromStream_DigitalCertificateRowDataSet(ref billingrow, billerid))
                                throw new BillingNotValidDigitalCertificatesException("Not digital certificate available for emisor.", -1, -1, string.Empty);
                        }
                        else
                        {
                            billingrow.OriginalString =
                                billingrow.CertificateNumber =
                                billingrow.Certificate =
                                billingrow.Seal = string.Empty;
                        }

                        billingdataset.Billings.AddBillingsRow(billingrow);
                        billingdataset.Billings[index].AcceptChanges();
                        billingdataset.Billings[index].SetAdded();

                        this.GetBillingFromStream_FillBiller(ref billingdataset, billerid);
                        this.GetBillingFromStream_ReceptorDataSet(ref billingdataset, receptortable[0].ReceptorID);

                        concepterro = false;
                        break;
                    case HEADER_STREAM_IVA: //iva

                        if (concepterro)
                            continue;

                        if (billingdataset.Taxes.Count <= index)
                        {
                            billingdataset.Taxes.AddTaxesRow(billingdataset.Billings[index], 0, 0);
                            billingdataset.Taxes[index].SetParentRow(billingdataset.Billings[index]);
                            billingdataset.Taxes[index].AcceptChanges();
                            billingdataset.Taxes[index].SetAdded();
                        }

                        switch (splitline[4].ToUpper())
                        {
                            case "T":
                                transfertaxrow = billingdataset.TransferTaxes.NewTransferTaxesRow();
                                transfertaxrow.TransferTaxeID = (billingdataset.TransferTaxes.Count) * -1;

                                if (string.IsNullOrEmpty(splitline[1]))
                                    throw new BillingTaxesNameIsNotValidException(string.Format("Transfer tax is not valid. Value is \"{0}\". can't be empty.", splitline[1]), index, 2, splitline[1]);

                                transfertaxrow.Name = splitline[1];

                                if (string.IsNullOrEmpty(splitline[2]))
                                    throw new BillingTaxesTotalException(string.Format("Transfer tax total is not valid. Value is \"{0}\".", splitline[2]), index, 3, splitline[2]);

                                if (!decimal.TryParse(splitline[2], out subtotal))
                                    throw new BillingTaxesTotalException(string.Format("Transfer tax total is not valid. Value is \"{0}\".", splitline[2]), index, 3, splitline[2]);

                                transfertaxrow.Import = subtotal;
                                billingdataset.Taxes[index].TotalTransfer += subtotal;

                                if (string.IsNullOrEmpty(splitline[3]))
                                    throw new BillingTaxesTotalException(string.Format("Transfer tax rate % is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                                if (!decimal.TryParse(splitline[3], out subtotal))
                                    throw new BillingTaxesTotalException(string.Format("Transfer tax rate % is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                                transfertaxrow.TaxRatePercentage = subtotal;
                                transfertaxrow.IvaAffected = false;
                                transfertaxrow.BillingID = billingdataset.Billings[index].BillingID;
                                billingdataset.TransferTaxes.AddTransferTaxesRow(transfertaxrow);
                                billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].SetParentRow(billingdataset.Billings[index]);
                                billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].AcceptChanges();
                                billingdataset.TransferTaxes[billingdataset.TransferTaxes.Count - 1].SetAdded();
                                break;
                            case "R":
                                denietaxrow = billingdataset.DetainedTaxes.NewDetainedTaxesRow();
                                denietaxrow.DetainedTaxeID = (billingdataset.DetainedTaxes.Count) * -1;

                                if (string.IsNullOrEmpty(splitline[1]))
                                    throw new BillingTaxesNameIsNotValidException(string.Format("Detained tax is not valid. Value is \"{0}\". can't be empty.", splitline[1]), index, 2, splitline[1]);

                                denietaxrow.Name = splitline[1];

                                if (string.IsNullOrEmpty(splitline[2]))
                                    throw new BillingTaxesTotalException(string.Format("Detained tax total is not valid. Value is \"{0}\".", splitline[2]), index, 3, splitline[2]);

                                if (!decimal.TryParse(splitline[2], out subtotal))
                                    throw new BillingTaxesTotalException(string.Format("Detained tax total is not valid. Value is \"{0}\".", splitline[2]), index, 3, splitline[2]);

                                denietaxrow.Import = subtotal;
                                billingdataset.Taxes[index].TotalDetained += subtotal;

                                if (string.IsNullOrEmpty(splitline[3]))
                                    throw new BillingTaxesTotalException(string.Format("Detained tax rate % is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                                if (!decimal.TryParse(splitline[3], out subtotal))
                                    throw new BillingTaxesTotalException(string.Format("Detained tax rate % is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                                denietaxrow.TaxRatePercentage = subtotal;
                                denietaxrow.IvaAffected = true;
                                denietaxrow.BillingID = billingdataset.Billings[index].BillingID;
                                billingdataset.DetainedTaxes.AddDetainedTaxesRow(denietaxrow);
                                billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].SetParentRow(billingdataset.Billings[index]);
                                billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].AcceptChanges();
                                billingdataset.DetainedTaxes[billingdataset.DetainedTaxes.Count - 1].SetAdded();
                                break;
                            default:
                                throw new PACFD.Rules.BillingsExceptions.BillingTaxeTypeNotValid(string.Format("Tax type is not valid, use \"T\", \"R\", \"t\" or \"r\". value is {0}", splitline[4]), index, 5, splitline[4]);
                        }
                        break;
                    case HEADER_STREAM_MOVE: //concept

                        if (concepterro)
                            continue;

                        conceptsearchtable = new PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable();
                        concepts.SearchByCode(conceptsearchtable, billerid, splitline[1]);

                        if (conceptsearchtable.Count < 1)
                            throw new BillingConceptCodeNotFoundException(string.Format("Concept code not valid or not found. Value is \"{0}\".", splitline[1]), index, 2, splitline[1]);

                        billingsdetailsrow = billingdataset.BillingsDetails.NewBillingsDetailsRow();
                        billingsdetailsrow.BillingID = billingdataset.Billings[index].BillingID;
                        billingsdetailsrow.BillingsDetailsID = billingdataset.BillingsDetails.Count * -1;
                        billingsdetailsrow.ConceptID = conceptsearchtable[0].ConceptID;

                        if (string.IsNullOrEmpty(splitline[3]))
                            throw new BillingConceptDetailCountException(string.Format("Concept ammount is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                        if (!int.TryParse(splitline[3], out id))
                            throw new BillingConceptDetailCountException(string.Format("Concept ammount is not valid. Value is \"{0}\".", splitline[3]), index, 4, splitline[3]);

                        billingsdetailsrow.Count = id;

                        switch (splitline[2])
                        {
                            case "0": billingsdetailsrow.AppliesTax = false; break;
                            case "1": billingsdetailsrow.AppliesTax = true; break;
                            default: throw new BillingConceptApplyTaxesException(string.Format("Concept apply tax is not valid. Value is \"{0}\".", splitline[2]), index, 3, splitline[2]);
                        }

                        if (!string.IsNullOrEmpty(splitline[4]))
                            if (decimal.TryParse(splitline[4], out subtotal))
                                billingsdetailsrow.DiscountPercentage = subtotal;

                        billingsdetailsrow.IdentificationNumber = conceptsearchtable[0].Code;

                        if (string.IsNullOrEmpty(splitline[5]))
                            throw new BillingPriceException(string.Format("Price is not valid or can't be empty. Value is \"{0}\".", splitline[5]), index, 6, splitline[5]);

                        if (!decimal.TryParse(splitline[5], out subtotal))
                            throw new BillingPriceException(string.Format("Price is not valid or can't be empty. Value is \"{0}\".", splitline[5]), index, 6, splitline[5]);

                        billingsdetailsrow.UnitValue = subtotal;

                        if (string.IsNullOrEmpty(splitline[6]))
                            throw new BillingPriceException(string.Format("Total is not valid or can't be empty. Value is \"{0}\".", splitline[5]), index, 7, splitline[6]);

                        if (!decimal.TryParse(splitline[6], out subtotal))
                            throw new BillingPriceException(string.Format("Total is not valid or can't be empty. Value is \"{0}\".", splitline[5]), index, 7, splitline[6]);

                        billingsdetailsrow.Amount = subtotal;
                        billingsdetailsrow.Unit = conceptsearchtable[0].UnitType;
                        billingsdetailsrow.Description = conceptsearchtable[0].Description;
                        billingdataset.BillingsDetails.AddBillingsDetailsRow(billingsdetailsrow);
                        billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].SetParentRow(billingdataset.Billings[index]);
                        billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].AcceptChanges();
                        billingdataset.BillingsDetails[billingdataset.BillingsDetails.Count - 1].SetAdded();
                        break;
                }
            }

            if (exceptionlist.Count > 0)
                throw new ExceptionList(exceptionlist);

            return billingdataset;
        }
        /// <summary>
        /// Fill billers data set
        /// </summary> 
        /// <param name="billingdataset"></param>
        /// <param name="billerid"></param>
        /// <returns></returns>
        private bool GetBillingFromStream_FillBiller(ref PACFD.DataAccess.BillingsDataSet billingdataset, int billerid)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsBillersRow row;
            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow cityrow;

            using (PACFD.DataAccess.BillersDataSet.BillersDataTable table = (new PACFD.Rules.Billers()).SelectByID(billerid))
            {
                //if (table.Count < 1)
                //{
                //    table.Dispose();
                //    table = null;
                //    throw new Billings.NotBillerFoundException("Biller not found.", this.BillerID);
                //}

                //cityrow = this.GetBillingFromStream_GetCityStateCountry(int.Parse(table[0].Location));

                row = billingdataset.BillingsBillers.NewBillingsBillersRow();
                row.BeginEdit();
                row.BillerAddress = table[0].Address;
                row.BillerName = table[0].Name;
                row.BillerRFC = table[0].RFC;
                row.BillingID = billingdataset.Billings[billingdataset.Billings.Count - 1].BillerID;
                row.BillingsRow = billingdataset.Billings[0];
                row.Colony = table[0].Colony;
                row.Country = table[0].Country;//cityrow.Country;
                row.ExternalNumber = table[0].ExternalNumber;
                row.InternalNumber = table[0].InternalNumber;
                row.Location = table[0].Location;//cityrow.City;
                row.Municipality = table[0].Municipality;//cityrow.Municipality;
                row.Reference = table[0].Reference;
                row.State = table[0].State;//cityrow.State;
                row.Zipcode = table[0].Zipcode;
                row.IssuedInDifferentPlace = table[0].IssuedInDifferentPlace;
                row.BillingID = billingdataset.Billings[billingdataset.Billings.Count - 1].BillingID;
                row.TaxSystem = table[0].TaxSystem;
                row.EndEdit();

                billingdataset.BillingsBillers.AddBillingsBillersRow(row);
                row.AcceptChanges();
                row.SetParentRow(billingdataset.Billings[billingdataset.Billings.Count - 1]);
                row.SetAdded();
            }

            return true;
        }
        /// <summary>
        /// Fill the receptor data.
        /// </summary>
        /// <param name="billingsdataset">DataSet to be filled.</param>
        /// <param name="receptorid">Receptor ID to search.</param>
        private void GetBillingFromStream_ReceptorDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset, int receptorid)
        {
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
            PACFD.DataAccess.BillingsDataSet.BillingsReceptorsRow row;

            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow cityrow;


            receptor = new PACFD.Rules.Receptors();
            table = receptor.SelectByID(receptorid);
            receptor = null;

            //if (table.Count < 1)
            //{
            //    table.Dispose();
            //    table = null;
            //    throw new Billings.NotReceptorFoundException("Receptor not found.", this.ReceptorID);
            //}

            //cityrow = this.GetBillingFromStream_GetCityStateCountry(int.Parse(table[0].Location));

            row = billingsdataset.BillingsReceptors.NewBillingsReceptorsRow();
            row.BeginEdit();
            row.BillingID = billingsdataset.Billings[billingsdataset.Billings.Count - 1].BillingID;
            //row.BillingsRow = billingsdataset.Billings[0];
            row.Colony = table[0].Colony;
            row.Country = table[0].Country;//;cityrow.Country
            row.ExternalNumber = table[0].ExternalNumber;
            row.InternalNumber = table[0].InternalNumber;
            row.Location = table[0].Location;//cityrow.City;
            row.Municipality = table[0].Municipality;//cityrow.Municipality;
            row.ReceptorAddress = table[0].Address;
            row.ReceptorName = table[0].Name;
            row.ReceptorRFC = table[0].RFC;
            row.Reference = table[0].Reference;
            row.State = table[0].State;//cityrow.State;
            row.Zipcode = table[0].Zipcode;
            row.TaxSystem = table[0].FiscalRegime;
            row.CFDIUse = table[0].CFDIUse;

            row.EndEdit();

            billingsdataset.BillingsReceptors.AddBillingsReceptorsRow(row);
            row.AcceptChanges();
            row.SetParentRow(billingsdataset.Billings[billingsdataset.Billings.Count - 1]);
            row.SetAdded();
            table.Dispose();
            table = null;
        }
        /// <summary>
        /// Fill the serial values of the billing.
        /// </summary>
        /// <param name="billingrow">Billing row.</param>
        /// <param name="billerid">Biller ID.</param>
        private bool GetBillingFromStream_FillSerial(ref PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow, int billerid, ElectronicBillingType elbilltype)
        {
            PACFD.Rules.Series series = new Series();
            int? branchid = billingrow.IsBranchIDNull() ? (int?)null : billingrow.BranchID;

            using (PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table = series.GetUnusedSerialByBillerID(billerid, branchid))
            {
                if (table.Count < 1)
                    throw new BillingsExceptions.BillingFolioOrSerialNotAvailable("Folio or serial not available", -1, -1, string.Empty);

                if (elbilltype == ElectronicBillingType.CFDI)
                {
                    billingrow.SetApprovalNumberNull();
                    billingrow.SetApprovalYearNull();
                }
                else
                {
                    billingrow.ApprovalNumber = table[0].AprovationNumber;
                    billingrow.ApprovalYear = table[0].AprovationYear;
                }

                billingrow.Serial = table[0].Serial;
                billingrow.Folio = table[0].Folio;
                billingrow.FolioID = table[0].FolioID;
                billingrow.SerialID = table[0].SerialID;

                if (elbilltype == ElectronicBillingType.CBB)
                {
                    if (table[0].QRImage == null)
                        throw new BillingsExceptions.BillingQrImageIsNull("Qr image is null.", -1, -1, string.Empty);

                    billingrow.QrImage = table[0].QRImage;
                }
            }

            return true;
        }
        /// <summary>
        /// Fill the digital certification of a billing
        /// </summary>
        /// <param name="billingrow"></param>
        /// <param name="billerid"></param>
        private bool GetBillingFromStream_DigitalCertificateRowDataSet(ref PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow, int billerid)
        {
            PACFD.Rules.DigitalCertificates digital = new PACFD.Rules.DigitalCertificates();

            using (PACFD.DataAccess.DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table = digital.GetActiveByBillerID(billerid))
            {
                digital = null;

                if (table.Count < 1)
                    return false;

                if (table[0].IsCertificateNumberNull())
                    return false;

                //    throw new BillingNotValidDigitalCertificatesException(
                //        "Not valid digital certificate: CertificateNumber is null or not valid.", table[0].DigitalCertificateID, -1, -1);

                billingrow.CertificateNumber = table[0].CertificateNumber;
                billingrow.Certificate = table[0].Certificate;
                billingrow.Seal = table[0].Seal;
            }

            return true;
        }

        /// <summary>
        /// Class used as a helper class for the billings parent class.
        /// </summary>
        private class BillingsHelperWebServiceClass
        {
            private Billings Parent { get; set; }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Billings parent class.</param>
            internal BillingsHelperWebServiceClass(Billings owner)
            {
                this.Parent = owner;
            }


            public void SetFiscalRegime(BillingFromXMLWebServiceEventArgs e, ref PACFD.DataAccess.BillingsDataSet billing)
            {
                PACFD.Rules.Billers biller = new Billers();
                int i = 0;

                using (PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable table = biller.FiscalRegimeTable.GetByBillerID(e.BillerId))
                {
                    if (table.Count < 1)
                        throw new BillingFiscalRegimeNotConfigurated("Fiscal regime is not configurated ", 0, 0, string.Empty);

                    foreach (var r in table)
                    {
                        billing.BillingFiscalRegime.AddBillingFiscalRegimeRow(r.Regime, billing.Billings[0]);
                        billing.BillingFiscalRegime[billing.BillingFiscalRegime.Count - 1].SetParentRow(billing.Billings[0]);
                        billing.BillingFiscalRegime[billing.BillingFiscalRegime.Count - 1].AcceptChanges();
                        billing.BillingFiscalRegime[billing.BillingFiscalRegime.Count - 1].SetAdded();
                    }
                }
            }

            public void SetPlaceDispatch(BillingFromXMLWebServiceEventArgs e, ref PACFD.DataAccess.BillingsDataSet.BillingsRow billrow, PACFD.DataAccess.BillingsDataSet.BillingsIssuedRow issuedrow)
            {
                //System.Data.DataRow[] rows;
                string s = null;

                if (issuedrow != null)
                    s = issuedrow.Zipcode;
                
                s = s == null ? string.Empty : s.Trim();

                if (string.IsNullOrEmpty(s) && e.BillerTable.Count > 0)
                    s = e.BillerTable[0].Zipcode;

                billrow.PlaceDispatch = s;

                //using (PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable t = new PACFD.Rules.PlaceDispatch().SelectByBillerID(e.BillerId))
                //{
                //    if (t.Count < 1)
                //    {
                //        throw new BillingPlaceDispatchIsEmpty("Place Dipatch is empty. CFDI 3.2 or higher needed. CFD 2.2 or higher needed.", 0, 0, string.Empty);
                //    }

                //    rows = t.Select("Active=1");

                //    if (rows == null || rows.Length < 1)
                //        throw new BillingPlaceDispatchIsEmpty("Place Dipatch active is not selected. CFDI 3.2 or higher needed. CFD 2.2 or higher needed.", 0, 0, string.Empty);

                //    PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDRow row = rows[0] as PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDRow;
                //    billrow.PlaceDispatch = row.PlaceDispatch;
                //}                 
            }

            public void FillBranch(int billerid, string code, ref PACFD.DataAccess.BillingsDataSet billingdataset, ref PACFD.DataAccess.BillingsDataSet.BillingsIssuedRow billingsssuedrow, ref PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow)
            {
                billingsssuedrow = billingdataset.BillingsIssued.NewBillingsIssuedRow();

                using (DataAccess.BranchesDataSet.GetByCodeDataTable table = (new Rules.Branches()).SelectByCode(billerid, null, code))
                {
                    if (table.Count < 1)
                        throw new BillingBranchIsNotValid("Can't find any branch with code" + code, 0, code);

                    billingsssuedrow.Address = table[0].Address;
                    //row.BillingID = billingdataset.Billings[0].BillingID;
                    billingsssuedrow.Colony = table[0].Colony;
                    billingsssuedrow.Country = table[0].Country;
                    billingsssuedrow.ExternalNumber = table[0].ExternalNumber;
                    billingsssuedrow.InternalNumber = table[0].InternalNumber;
                    billingsssuedrow.Location = table[0].Location;
                    billingsssuedrow.Municipality = table[0].Municipality;
                    billingsssuedrow.Reference = table[0].Reference;
                    billingsssuedrow.State = table[0].State;
                    billingsssuedrow.Zipcode = table[0].Zipcode;
                    billingrow.BranchID = table[0].BranchID;
                }

            }

            public PACFD.DataAccess.BillingsDataSet GetBillingsFromExternalFolio(int billerid, string externalfolio)
            {
                int billingid;

                using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable folio = this.Parent.SelectByExternalFolio(billerid, externalfolio))
                {
                    if (folio.Count < 1)
                        throw new BillingsExceptions.BillingExceptionBase("Error on the system. Getting by external folio.", 0, 0, string.Empty);

                    billingid = folio[0].BillingID;
                }

                return this.Parent.GetFullBilling(billingid);
            }
            /// <summary>
            /// Get and try to seal an excisting billing that petition comes from a webservice.
            /// </summary>
            /// <param name="billerid">Biller ID.</param>
            /// <param name="externalfolio">External folio to look for.</param>
            /// <returns>PACFD.DataAccess.BillingsDataSet.BillingExceptionBase </returns>
            /// <exception cref="PACFD.Rules.BillingsExceptions.">Error on the system. Getting by external folio.</exception>
            public PACFD.DataAccess.BillingsDataSet GetBillingsForExternalFolioOfWebservice(int billerid, string externalfolio)
            {
                int billingid;
                PACFD.DataAccess.BillingsDataSet d;

                using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable folio = this.Parent.SelectByExternalFolio(billerid, externalfolio))
                {
                    if (folio.Count < 1)
                    { throw new BillingsExceptions.BillingExceptionBase("Error on the system. Getting by external folio.", 0, 0, string.Empty); }

                    billingid = folio[0].BillingID;
                }

                d = this.Parent.GetFullBilling(billingid);

                //if (d.Billings[0].PreBilling)
                //{
                //    error = new PACFD.Common.ErrorManager();
                //    this.Parent.SealCFD(d, ref error);

                //    //if(error.Error.

                //    d.Billings[0].PreBilling = true;
                //    this.Parent.Update(d);
                //    wasprebilling = true;
                //}

                return d;
            }

            public void SetCurrency(ref PACFD.DataAccess.BillingsDataSet.BillingsRow billrow, System.Xml.XmlNode invoice, BillingFromXMLWebServiceEventArgs e)
            {
                const string mxn = "mxn";
                const string usd = "usd";
                const string token_currency = "moneda";
                const string token_exchangerate = "tipocambio";
                string currency = string.Empty;
                decimal exchagerate = 0;

                if (invoice == null)
                    throw new PACFD.Rules.BillingsExceptions.BillingCurrencyError("Invoice is null.", 0, 0, string.Empty);

                using (PACFD.DataAccess.BillersDataSet.BillersDataTable table = (new Billers()).SelectByID(e.BillerId))
                {
                    if (table.Count < 1)
                        throw new PACFD.Rules.BillingsExceptions.BillingCurrencyError("Currencybiller ID not found.", 0, 0, string.Empty);

                    currency = invoice.Attributes[token_currency] == null ? null : invoice.Attributes[token_currency].Value.Trim().ToLower();

                    if (table[0].CurrencyCode.ToLower() == mxn && (currency == null || currency == mxn))
                    {
                        billrow.ExchangeRateMXN = 1;
                        billrow.CurrencyCode = mxn.ToUpper();
                        return;
                    }

                    if (invoice.Attributes[token_exchangerate] == null || !decimal.TryParse(invoice.Attributes[token_exchangerate].Value, out exchagerate))
                        throw new PACFD.Rules.BillingsExceptions.BillingCurrencyExchagneRateIsNotValidOrEmpty("Exchange rate is not decimal or empty.", 0, 0, string.Empty);

                    if (table[0].CurrencyCode.Trim().ToLower() == usd)
                    {
                        if ((e.ElectronicBillingType == ElectronicBillingType.CFD && currency == mxn) || (e.ElectronicBillingType == ElectronicBillingType.CFD && currency == usd)
                            || (e.ElectronicBillingType == ElectronicBillingType.CBB && currency == mxn) || (e.ElectronicBillingType == ElectronicBillingType.CFDI && currency == mxn))
                            if (table[0].IsIsDictaminatedNull() || !table[0].IsDictaminated)
                                throw new PACFD.Rules.BillingsExceptions.BillingCurrencyBillerIsNotDictaminated("Currency is not accepted, biller is not dictaminated.", 0, 0, string.Empty);

                        billrow.ExchangeRateMXN = exchagerate;
                        billrow.CurrencyCode = currency == null ? usd.ToUpper() : currency.ToUpper();
                    }
                    else if (table[0].CurrencyCode.Trim().ToLower() == mxn)
                    {
                        if (table[0].IsIsDictaminatedNull() || !table[0].IsDictaminated)
                            throw new PACFD.Rules.BillingsExceptions.BillingCurrencyBillerIsNotDictaminated("Currency is not accepted, biller is not dictaminated.", 0, 0, string.Empty);

                        billrow.ExchangeRateMXN = exchagerate;
                        billrow.CurrencyCode = usd.ToUpper();
                    }
                    else
                    {
                        throw new PACFD.Rules.BillingsExceptions.BillingCurrencyIsNotSopported("Currency is not soported (not MXN or USD).", 0, 0, string.Empty);
                    }
                }
            }
            /// <summary>
            /// Get a string "serial - folio"/ " - folio" from billing by external folio, if the billing is not active (cancelled) then
            /// the billing is not take in count.
            /// </summary>
            /// <param name="billerid">Biller ID used to filter.</param>
            /// <param name="folio">Folio to look for.</param>
            /// <returns>If success return a "serial - folio" / " - folio" string else empty.</returns>
            public string ExistExternalFolio(int billerid, string folio)
            {
                using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable table = this.Parent.SelectByExternalFolio(billerid, folio))
                {
                    foreach (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioRow item in table)
                    {
                        if (string.IsNullOrEmpty(item.ExternalFolio) || !item.Active)
                        { continue; }

                        if (item.ExternalFolio.Trim().ToLower() == folio.Trim().ToLower())
                        { return string.Format("{0} - {1}", item.Serial, item.Folio); }
                    }
                }

                return string.Empty;
            }
            /// <summary>
            /// Insert a new client on the data base.
            /// </summary>
            /// <param name="client">Client data.</param>
            /// <param name="billerid">Biller parent of the client.</param>
            /// <param name="tabletarget">Available receptor to be update.</param>
            /// <returns>If success return true else false.</returns>
            public bool InsertClient(System.Xml.XmlNode client, int billerid, string usocfdi, ref PACFD.DataAccess.ReceptorsDataSet.SearchByRFCDataTable tabletarget)
            {
                String izipcode = string.Empty;
                const string token_rfc = "rfc";
                const string token_name = "nombre";
                const string token_address = "direccion";
                const string token_location = "ciudad";
                const string token_municipality = "municipio";
                const string token_state = "estado";
                const string token_country = "pais";
                const string token_zipcode = "codigopostal";
                const string token_colony = "colonia";
                PACFD.Rules.Receptors ruls = new Receptors();
                PACFD.DataAccess.ReceptorsDataSet.ReceptorsRow row;
                PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
                System.Text.RegularExpressions.Regex rfcvalidate = new System.Text.RegularExpressions.Regex(@"([A-Z|a-z]|&){3,4}\d{6}\w{3}$");
                PACFD.DataAccess.ReceptorsDataSet.SearchByRFCRow rowtarget;

                if ((client.Attributes[token_rfc] == null || string.IsNullOrEmpty(client.Attributes[token_rfc].Value)))
                    throw new PACFD.Rules.BillingsExceptions.BillingRFCIsEmpty("RFC of the client is empty.", -1, 1, string.Empty);

                if (!rfcvalidate.IsMatch(client.Attributes[token_rfc].Value))
                    throw new PACFD.Rules.BillingsExceptions.BillingRFCIsNotValid("RFC of the client is not valid.", -1, 1, string.Empty);

                izipcode = string.Empty;

                if (client.Attributes[token_zipcode] != null)
                    izipcode = client.Attributes[token_zipcode].Value;

                if (izipcode.ToString().Length > 5 /*|| izipcode < -1*/)
                    throw new PACFD.Rules.BillingsExceptions.BillingZipcodeIsNotValid("Zipcode of the client is not valid.", -1, 1, string.Empty);

                using (table = new PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable())
                {
                    row = table.NewReceptorsRow();
                    row.BeginEdit();
                    row.ReceptorID = -1;
                    row.BillerID = billerid;
                    row.RFC = client.Attributes[token_rfc].Value;

                    row.Name = client.Attributes[token_name] == null ? string.Empty :
                        client.Attributes[token_name].Value.Replace("&apos;", "'");

                    row.Address = client.Attributes[token_address] == null ? string.Empty :
                        client.Attributes[token_address].Value.Replace("&apos;", "'");

                    row.Colony = client.Attributes[token_colony].Value;
                    row.Phone =
                        row.ExternalNumber =
                        row.InternalNumber =
                        row.Reference =
                        row.Email =
                        string.Empty;
                    row.Location = client.Attributes[token_location] == null ? string.Empty : client.Attributes[token_location].Value;
                    row.Municipality = client.Attributes[token_municipality] == null ? string.Empty : client.Attributes[token_municipality].Value;
                    row.State = client.Attributes[token_state] == null ? string.Empty : client.Attributes[token_state].Value;
                    row.Country = client.Attributes[token_country] == null ? string.Empty : client.Attributes[token_country].Value;
                    row.Zipcode = izipcode;
                    row.EndEdit();
                    row.CFDIUse = usocfdi;
                    table.AddReceptorsRow(row);
                    row.AcceptChanges();
                    row.SetAdded();

                    if (!ruls.Update(table))
                        return false;

                    rowtarget = tabletarget.NewSearchByRFCRow();
                    rowtarget.BeginEdit();
                    rowtarget.ReceptorID = row.ReceptorID;
                    rowtarget.BillerID = row.BillerID;
                    rowtarget.RFC = row.RFC;
                    rowtarget.Name = row.Name;
                    rowtarget.Address = row.Address;
                    rowtarget.Phone = row.Phone;
                    rowtarget.ExternalNumber = row.ExternalNumber;
                    rowtarget.InternalNumber = row.InternalNumber;
                    rowtarget.Reference = row.Reference;
                    rowtarget.Email = row.Email;
                    rowtarget.Colony = row.Colony;
                    rowtarget.Location = row.Location;
                    rowtarget.Municipality = row.Municipality;
                    rowtarget.State = row.State;
                    rowtarget.Country = row.Country;
                    rowtarget.Zipcode = row.Zipcode;
                    rowtarget.CFDIUse = row.CFDIUse;
                    rowtarget.EndEdit();
                    tabletarget.AddSearchByRFCRow(rowtarget);
                    rowtarget.AcceptChanges();
                }

                return true;
            }
            /// <summary>
            /// Update an existing client on the data base.
            /// </summary>
            /// <param name="client">Clinet node.</param>
            /// <param name="billerid">Biller ID.</param>
            /// <param name="receptorid">Receptor ID.</param>
            /// <param name="tabletarget"></param>
            /// <returns></returns>
            public bool UpdateClient(System.Xml.XmlNode client, int billerid, int receptorid, string usocfdi, ref DataAccess.ReceptorsDataSet.SearchByRFCDataTable tabletarget)
            {
                const string token_name = "nombre";
                const string token_rfc = "rfc";
                const string token_address = "direccion";
                const string token_location = "ciudad";
                const string token_municipality = "municipio";
                const string token_state = "estado";
                const string token_country = "pais";
                const string token_zipcode = "codigopostal";
                const string token_colony = "colonia";
                PACFD.Rules.Receptors receptor;
                String i = string.Empty;
                DataAccess.ReceptorsDataSet.ReceptorsDataTable newtable;
                System.Text.RegularExpressions.Regex rfcvalidate = new System.Text.RegularExpressions.Regex(@"([A-Z|a-z]|&){3,4}\d{6}\w{3}$");

                if ((client.Attributes[token_rfc] == null || string.IsNullOrEmpty(client.Attributes[token_rfc].Value)))
                    throw new PACFD.Rules.BillingsExceptions.BillingRFCIsEmpty("RFC of the client is empty.", -1, 1, string.Empty);

                if (!rfcvalidate.IsMatch(client.Attributes[token_rfc].Value))
                    throw new PACFD.Rules.BillingsExceptions.BillingRFCIsNotValid("RFC of the client is not valid.", -1, 1, string.Empty);

                //if (!int.TryParse(client.Attributes[token_zipcode].Value, out i))
                //    i = -1;

                //i = client.Attributes[token_zipcode].Value;

                //throw new PACFD.Rules.BillingsExceptions.BillingZipcodeIsNotValid("Zipcode of the client is not valid.", -1, 1, string.Empty);

                if (i.ToString().Length > 5 /*|| i < -1*/)
                    throw new PACFD.Rules.BillingsExceptions.BillingZipcodeIsNotValid("Zipcode of the client is not valid.", -1, 1, string.Empty);

                //if (
                //  (client.Attributes[token_rfc].Value != tabletarget[0].RFC) ||
                //(client.Attributes[token_name].Value != tabletarget[0].Name) ||
                //(client.Attributes[token_address].Value != tabletarget[0].Address) ||
                //( client.Attributes[token_location].Value != tabletarget[0].Location) ||
                //(client.Attributes[token_municipality].Value != tabletarget[0].Municipality) ||
                //(client.Attributes[token_state].Value != tabletarget[0].State) ||
                //(client.Attributes[token_country].Value != tabletarget[0].Country) ||
                //  (client.Attributes[token_zipcode].Value != tabletarget[0].Zipcode.ToString()) //||
                //(client.Attributes[token_colony].Value != tabletarget[0].Colony.ToString())
                //   )
                {
                    receptor = new Receptors();

                    tabletarget[0].RFC = client.Attributes[token_rfc].Value;

                    tabletarget[0].Name = client.Attributes[token_name] == null ? string.Empty :
                        client.Attributes[token_name].Value.Replace("&apos;", "'");

                    tabletarget[0].Address = client.Attributes[token_address] == null ? string.Empty :
                        client.Attributes[token_address].Value.Replace("&apos;", "'");

                    tabletarget[0].Location = client.Attributes[token_location] == null ? string.Empty : client.Attributes[token_location].Value;
                    tabletarget[0].Municipality = client.Attributes[token_municipality] == null ? string.Empty : client.Attributes[token_municipality].Value;
                    tabletarget[0].State = client.Attributes[token_state] == null ? string.Empty : client.Attributes[token_state].Value;
                    tabletarget[0].Country = client.Attributes[token_country] == null ? string.Empty : client.Attributes[token_country].Value;
                    tabletarget[0].Colony = client.Attributes[token_colony] == null ? string.Empty : client.Attributes[token_colony].Value;

                    //if (!int.TryParse(client.Attributes[token_zipcode].Value, out i))
                    //    i = -1;
                    //i = client.Attributes[token_zipcode].Value;

                    tabletarget[0].Zipcode = client.Attributes[token_zipcode] == null ? string.Empty : client.Attributes[token_zipcode].Value;
                    tabletarget[0].CFDIUse = usocfdi;
                    newtable = new PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable();
                    newtable.AddReceptorsRow(
                        billerid,
                        tabletarget[0].RFC,
                        tabletarget[0].Name,
                        tabletarget[0].Address,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        client.Attributes[token_colony].Value,
                        tabletarget[0].Location,
                        string.Empty,
                        tabletarget[0].Municipality,
                        tabletarget[0].State,
                        tabletarget[0].Country,
                        String.Empty,
                        tabletarget[0].Zipcode,
                        tabletarget[0].CFDIUse,tabletarget[0].FiscalRegime);

                    newtable[0].ReceptorID = receptorid;

                    newtable[0].AcceptChanges();
                    newtable[0].SetModified();

                    receptor.Update(newtable);

                    tabletarget[0].RFC = newtable[0].RFC;
                    tabletarget[0].Name = newtable[0].Name;
                    tabletarget[0].Address = newtable[0].Address;
                    tabletarget[0].Location = newtable[0].Location;
                    tabletarget[0].Municipality = newtable[0].Municipality;
                    tabletarget[0].State = newtable[0].State;
                    tabletarget[0].Country = newtable[0].Country;
                    tabletarget[0].Colony = newtable[0].Colony;
                    tabletarget[0].CFDIUse = newtable[0].CFDIUse;
                    tabletarget[0].AcceptChanges();
                }

                return true;
            }
            /// <summary>
            /// Insert a new concept on the data base.
            /// </summary>
            /// <param name="concept"></param>
            /// <param name="billerid"></param>
            /// <param name="tabletarget"></param>
            /// <returns></returns>
            public bool InsertConcept(System.Xml.XmlNode concept, int billerid, ref PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable tabletarget, BillingFromXMLWebServiceEventArgs e)
            {
                const string token_description = "descripcion";
                const string token_applytax = "aplicaimpuestos";
                const string token_code = "codigo";
                const string token_price = "precio";
                const string token_unittype = "unidadtipo";
                const string token_unittype_noapply = "No Aplica";
                PACFD.Rules.Concepts ruls = new Concepts();
                PACFD.DataAccess.ConceptsDataSet.ConceptsRow row;
                PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable table;
                PACFD.DataAccess.ConceptsDataSet.Concepts_ImportRow rowtarget;
                bool applytax = true;
                decimal price = 0.0m;

                if ((concept.Attributes[token_description] == null || string.IsNullOrEmpty(concept.Attributes[token_description].Value)) ||
                    (concept.Attributes[token_code] == null || string.IsNullOrEmpty(concept.Attributes[token_code].Value)) ||
                    (concept.Attributes[token_price] == null || string.IsNullOrEmpty(concept.Attributes[token_price].Value)))
                    return false;

                if (concept.Attributes[token_applytax] != null)
                    if (!bool.TryParse(concept.Attributes[token_applytax].Value.ToString(), out applytax))
                        applytax = true;

                if (concept.Attributes[token_price] != null)
                    if (!decimal.TryParse(concept.Attributes[token_price].Value.ToString(), out price))
                        price = 0.0m;

                using (table = new PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable())
                {
                    row = table.NewConceptsRow();
                    row.BeginEdit();
                    row.Active = true;
                    row.BillerID = billerid;
                    row.ConceptID = -1;
                    row.Description = concept.Attributes[token_description].Value;
                    row.UnitType = (e.ElectronicBillingType == ElectronicBillingType.CFDI3_2 || e.ElectronicBillingType == ElectronicBillingType.CFD2_2 || e.ElectronicBillingType == ElectronicBillingType.CFDI3_3 || e.ElectronicBillingType == ElectronicBillingType.CFDI4_0) ?
                        (concept.Attributes[token_unittype] == null ? token_unittype_noapply : concept.Attributes[token_unittype].Value) : token_unittype_noapply;
                    row.TaxType = 0;
                    row.Account = string.Empty;
                    row.AppliesTax = applytax;
                    row.Code = concept.Attributes[token_code].Value;
                    row.TaxRatePercentage = 0;
                    row.UnitPrice = price;
                    row.EndEdit();
                    table.AddConceptsRow(row);
                    row.AcceptChanges();
                    row.SetAdded();

                    if (!ruls.Update(table))
                        return false;

                    rowtarget = tabletarget.NewConcepts_ImportRow();
                    rowtarget.BeginEdit();
                    rowtarget.Active = row.Active;
                    rowtarget.BillerID = row.BillerID;
                    rowtarget.ConceptID = row.ConceptID;
                    rowtarget.Description = row.Description;
                    rowtarget.UnitType = row.UnitType;
                    rowtarget.TaxType = row.TaxType;
                    rowtarget.Account = row.Account;
                    rowtarget.AppliesTax = row.AppliesTax;
                    rowtarget.Code = row.Code;
                    rowtarget.TaxRatePercentage = row.TaxRatePercentage;
                    rowtarget.UnitPrice = row.UnitPrice;
                    rowtarget.EndEdit();
                    tabletarget.AddConcepts_ImportRow(rowtarget);
                    rowtarget.AcceptChanges();
                }

                return true;
            }

            public bool IfConceptIsDifferentUpdate(System.Xml.XmlNode concept, ref PACFD.DataAccess.ConceptsDataSet.Concepts_ImportDataTable tablereference)
            {
                PACFD.Rules.Concepts ruls = new Concepts();
                bool update = false;
                decimal ddecimal = 0.0m;
                const string token_UnitPrice = "precio";
                const string token_Description = "descripcion";
                const string token_AppliesTax = "aplicaimpuestos";
                const string token_Code = "codigo";
                const string token_unittype = "unidadtipo";
                const string token_unittype_noapply = "E48";
                bool bbool = false;

                if (concept.Attributes[token_UnitPrice] != null && (tablereference[0].UnitPrice.ToString() != concept.Attributes[token_UnitPrice].Value.Trim()))
                {
                    if (decimal.TryParse(concept.Attributes[token_UnitPrice].Value, out ddecimal))
                    {
                        update = true;
                        tablereference[0].UnitPrice = ddecimal;
                    }
                }

                if (concept.Attributes[token_Description] != null && (tablereference[0].Description.Trim().ToLower() != concept.Attributes[token_Description].Value.Trim().ToLower()))
                {
                    update = true;
                    tablereference[0].Description = concept.Attributes[token_Description].Value.Trim();
                }
                if (concept.Attributes[token_Code] != null && (tablereference[0].Code.ToString() != concept.Attributes[token_Code].Value.Trim().ToLower()))
                {
                    update = true;
                    tablereference[0].Code = concept.Attributes[token_Code].Value.Trim();
                }

                if (concept.Attributes[token_unittype] != null && string.IsNullOrEmpty(concept.Attributes[token_unittype].Value))
                    tablereference[0].UnitType = concept.Attributes[token_unittype].Value;
                else
                    tablereference[0].UnitType = token_unittype_noapply;

                if (concept[token_AppliesTax] != null)
                {
                    switch (concept[token_AppliesTax].Value.Trim().ToLower())
                    {
                        case "1":
                        case "true":
                        case "si":
                        case "yes":
                        default: bbool = true; break;
                        case "0":
                        case "false":
                        case "no":
                        case "not": bbool = false; break;
                    }

                    if (tablereference[0].AppliesTax != bbool)
                    {
                        tablereference[0].AppliesTax = bbool;
                        update = true;
                    }
                }

                if (!tablereference[0].Active)
                    update =
                        tablereference[0].Active = true;

                if (update)
                    if (!ruls.Update(tablereference))
                        return false;

                return true;
            }
        }

        /// <summary>
        /// Class used as arguments to generate a bill from xml docuemnt sended.
        /// </summary>
        public class BillingFromXMLWebServiceEventArgs : EventArgs
        {
            /// <summary>
            /// Document send by the user.
            /// </summary>
            public System.Xml.XmlDocument Document { get; private set; }
            /// <summary>
            /// Billerd ID that send the petition.
            /// </summary>
            public int BillerId { get; private set; }
            /// <summary>
            /// Type of electronic bill detected previous.
            /// </summary>
            public ElectronicBillingType ElectronicBillingType { get; private set; }
            /// <summary>
            /// Get a boolean value indicating if the external folio need to be validated.
            /// </summary>
            public Boolean AllowRepeatExternalFolio { get; private set; }
            /// <summary>
            /// Get a boolean value indicating if the insertion is from the web service.
            /// </summary>
            public bool IsFromWebService { get; private set; }
            /// <summary>
            /// Get the actual biller table not billingbiller.
            /// </summary>
            public PACFD.DataAccess.BillersDataSet.BillersDataTable BillerTable { get; private set; }

            /// <summary>
            /// Create a enw instance of the class. Use this constructor if the insertion is not from a web service call.
            /// If the insertion comes from a web service, use other constructor.
            /// </summary>
            /// <param name="document">Document that have the concepts, prices, etc..</param>
            /// <param name="billerid">Biller that make the insertion.</param>
            /// <param name="elbilltype">Type of electronic bill to insert.</param>
            /// <param name="externaloliovalidation">Indicate if the external folio must be validated.</param>
            public BillingFromXMLWebServiceEventArgs(System.Xml.XmlDocument document, PACFD.DataAccess.BillersDataSet.BillersDataTable billertable
                , int billerid, ElectronicBillingType elbilltype, bool allowrepeatexternalfolio) :
                this(document, billertable, billerid, elbilltype, allowrepeatexternalfolio, false) { }
            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="document">Document that have the concepts, prices, etc..</param>
            /// <param name="billerid">Biller that make the insertion.</param>
            /// <param name="elbilltype">Type of electronic bill to insert.</param>
            /// <param name="externaloliovalidation">Indicate if the external folio must be validated.</param>
            /// <param name="iswebservice">
            /// True if the insertion comes from a web service call else false. If true and the external folio already exist and the 
            /// biller is a CFDI certificated, then the billing is sealed an returned.
            /// </param>
            public BillingFromXMLWebServiceEventArgs(System.Xml.XmlDocument document, PACFD.DataAccess.BillersDataSet.BillersDataTable billertable
                , int billerid, ElectronicBillingType elbilltype, bool allowrepeatexternalfolio, bool iswebservice)
            {
                this.Document = document;
                this.BillerId = billerid;
                this.ElectronicBillingType = elbilltype;
                this.AllowRepeatExternalFolio = allowrepeatexternalfolio;
                this.IsFromWebService = iswebservice;
                this.BillerTable = billertable;
            }
        }
    }

    public static class Helperss
    {
        public static string GetPlaceDispatch(string zipcode, int billerid)
        {
            PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable table = (new PACFD.Rules.PlaceDispatch()).SelectByBillerID(billerid);
            var rows = table.Select("Zipcode = '" + zipcode + "'");
            if (rows.Count() > 0)
                return rows[0]["PlaceDispatch"].ToString() + "    -     " + zipcode;
            return zipcode;
        }

        public static string GetTaxSystemByCode(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter fiscal = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter();
            fiscal.Fill(catalogs.CatRegimenFiscal);
            var reg = catalogs.CatRegimenFiscal.Select("c_RegimenFiscal ='" + code + "'");
            if (reg.Count() > 0)
                return code + " - " + reg[0]["Descripcion"];
            return code;
        }

        public static string GetPaymentFormByCode(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatFormaPagoTableAdapter forma = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatFormaPagoTableAdapter();
            forma.Fill(catalogs.CatFormaPago);
            var reg = catalogs.CatFormaPago.Select("c_FormaPago ='" + code + "'");
            if (reg.Count() > 0)
                return code + " - " + reg[0]["Descripcion"];
            return code;
        }

        public static string GetPaymentMethodByCode(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatMetodoPagoTableAdapter metodo = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatMetodoPagoTableAdapter();
            metodo.Fill(catalogs.CatMetodoPago);
            var reg = catalogs.CatMetodoPago.Select("c_MetodoPago ='" + code + "'");
            if (reg.Count() > 0)
                return code + " - " + reg[0]["Descripcion"];
            return code;
        }

        public static string GetImpuesto(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatImpuestoTableAdapter impuesto = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatImpuestoTableAdapter();
            impuesto.Fill(catalogs.CatImpuesto);
            var reg = catalogs.CatImpuesto.Select("c_Impuesto ='" + code + "'");
            if (reg.Count() > 0)
                return code + " - " + reg[0]["Descripcion"];
            return code;
        }

        public static string GetClaveProdServCode(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter();
            cfdi.FillByClave(catalogs.CatClaveProdServ, code);
            var cp = catalogs.CatClaveProdServ;
            string result = "";
            if (cp != null && cp.Count() > 0)
                return cp[0].c_ClaveProdServ.ToString() + " - " + cp[0].Descripcion.ToString();
            return code;
        }

        public static BillingCatalogs.CatRegimenFiscalDataTable GetTaxSystem()
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter fiscal = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter();
            fiscal.Fill(catalogs.CatRegimenFiscal);
            return catalogs.CatRegimenFiscal;
        }

        public static BillingCatalogs.CatUsoCFDIDataTable GetUsoCFDI()
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter();
            cfdi.Fill(catalogs.CatUsoCFDI);
            return catalogs.CatUsoCFDI;

        }

        public static string GetUsoCFDIByCode(string code)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter impuesto = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter();
            impuesto.Fill(catalogs.CatUsoCFDI);
            var reg = catalogs.CatUsoCFDI.Select("c_UsoCFDI ='" + code + "'");
            if (reg.Count() > 0)
                return code + " - " + reg[0]["Descripcion"];
            return code;
        }

        public static bool IsValidZipCode(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatCodigoPostalTableAdapter codigo = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatCodigoPostalTableAdapter();
            codigo.Fill(catalogs.CatCodigoPostal);
            var cp = catalogs.CatCodigoPostal.Select("c_CodigoPostal = " + source);
            return cp != null && cp.Count() > 0;
        }

        public static string GetClaveUnidadName(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter();
            cfdi.FillByName(catalogs.CatClaveUnidad, source);
            var cp = catalogs.CatClaveUnidad;
            string result = "";
            if (cp != null && cp.Count() > 0)
                result = cp[0].Nombre.ToString();
            return result;
        }
    }
}