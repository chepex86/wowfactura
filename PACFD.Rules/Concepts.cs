﻿#region usings
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.ConceptsDataSetTableAdapters;
using PACFD.Common;
using System.Data;
using System.IO;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public enum TypesOfSearchConcepts
    {
        Code = 0,
        Type = 1,
        Description = 2,
        All = 3,
    }
    public enum TypesErrorsImport
    {
        Success = -1,
        Twice = 0,
        MissingHeader = 1,
        MaxLength = 2,
        StringEmpty = 3,
        ErrorConversion = 4,
        ReadingError = 5,
        FatalError = 6,
        CountryNotFound = 7,
    }
    /// <summary> 
    /// 
    /// </summary>
    public class Concepts
    {
        /// <summary>
        /// String with all the unit type "UMC" values.
        /// </summary>
        public const string ConceptUnitTypeList = "[Sin Asignar],Barril,Botella,Cabeza,Caja,Cientos,Decenas,Docenas,Gramo,Gramo Neto,Juego,Kilo,Kilowatt,Kilowatt/Hora,Litro,Metro Cuadrado,Metro Cubico,Metro Lineal,Millar,Paquete,Par,Pieza,Servicio,Tonelada";

        /// <summary>
        ///  
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public int Update_Delete(ConceptsDataSet.ConceptsDataTable table)
        {
            try
            {
                using (ConceptsTableAdapter taConcepts = new ConceptsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taConcepts);
                    taConcepts.Update(table);
                }
            }
            catch (Exception ex)
            {
                for (int i = 0; i < ex.Message.Length - 27; i++)
                {
                    if (ex.Message.Substring(i, 27) == "FK_BillingsDetails_Concepts")
                        return 2;
                }
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return 0;
            }

            return 1;
        }
        public bool Update(ConceptsDataSet.ConceptsDataTable table)
        {
            try
            {
                using (ConceptsTableAdapter taConcepts = new ConceptsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taConcepts);
                    taConcepts.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ConceptsDataSet.ConceptsDataTable SelectByID(int id)
        {
            ConceptsDataSet.ConceptsDataTable table;

            table = new ConceptsDataSet.ConceptsDataTable();
            this.SelectByID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(ConceptsDataSet.ConceptsDataTable table, int id)
        {
            ConceptsTableAdapter adapter;

            try
            {
                using (adapter = new ConceptsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <returns></returns>
        public ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable GetTaxRatePercentageByBillerID(int BillerID)
        {
            ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable table;

            table = new ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable();
            this.GetTaxRatePercentageByBillerID(table, BillerID);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <returns></returns>
        public bool GetTaxRatePercentageByBillerID(ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable table, int BillerID)
        {
            try
            {
                using (Concepts_GetTaxRatePercentageTableAdapter adapter = new Concepts_GetTaxRatePercentageTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }

        public ConceptsDataSet.Concepts_GetByFkBillerIDDataTable GetConceptsByBillerID(int BillerID)
        {
            ConceptsDataSet.Concepts_GetByFkBillerIDDataTable table;

            table = new ConceptsDataSet.Concepts_GetByFkBillerIDDataTable();
            this.GetConceptsByBillerID(table, BillerID);

            return table;
        }

        public bool GetConceptsByBillerID(ConceptsDataSet.Concepts_GetByFkBillerIDDataTable table, int BillerID)
        {
            try
            {
                using (Concepts_GetByFkBillerIDTableAdapter adapter = new Concepts_GetByFkBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }
        
        #region Select Concepts By Searching

        public ConceptsDataSet.Concepts_GetBySearchingDataTable SelectConceptByBillerID(int BillerID)
        {
            ConceptsDataSet.Concepts_GetBySearchingDataTable table;
            string c = String.Empty;
            table = this.SelectConceptBySearching(BillerID, c, c, c);

            return table;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ConceptsDataSet.Concepts_GetBySearchingDataTable SelectConceptBySearching(int BillerID, string type, string descript, string code)
        {
            ConceptsDataSet.Concepts_GetBySearchingDataTable table;

            table = new ConceptsDataSet.Concepts_GetBySearchingDataTable();
            this.SelectConceptBySearching(table, BillerID, type, descript, code);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SelectConceptBySearching(ConceptsDataSet.Concepts_GetBySearchingDataTable table, int BillerID, string type, string descript, string code)
        {
            Concepts_GetBySearchingTableAdapter adapter;

            try
            {
                using (adapter = new Concepts_GetBySearchingTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, type, descript, code);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select By BillerID And Description
        /// <summary>
        /// Get a DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table with consepts filtered by
        /// the billerID and description.
        /// </summary>
        /// <param name="billerid">BillerID used to filter.</param>
        /// <param name="description">Description used to filter.</param>
        /// <returns>Return a DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table.</returns>
        public DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable SelectByBillerIDAndDescription(int billerid, string description)
        {
            DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table;
            DataAccess.ConceptsDataSetTableAdapters.GetByBillerIDAndDescriptionTableAdapter adapter;

            table = new ConceptsDataSet.GetByBillerIDAndDescriptionDataTable();

            try
            {
                using (adapter = new GetByBillerIDAndDescriptionTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, description);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        #endregion

        #region Search By Code
        public bool SearchByCode(int BillerID, String code)
        {
            ConceptsDataSet.Concepts_GetByCodeDataTable table;

            table = new ConceptsDataSet.Concepts_GetByCodeDataTable();
            this.SearchByCode(table, BillerID, code);

            return table.Count > 0 ? true : false;
        }
        public bool SearchByCode(ConceptsDataSet.Concepts_GetByCodeDataTable table, int BillerID, String code)
        {
            Concepts_GetByCodeTableAdapter adapter;

            try
            {
                using (adapter = new Concepts_GetByCodeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, code);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        #endregion

        #region Concepts Import

        #region update Import
        public bool Update(ConceptsDataSet.Concepts_ImportDataTable table)
        {
            Concepts_ImportTableAdapter taConcepts;

            try
            {
                using (taConcepts = new Concepts_ImportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taConcepts);
                    taConcepts.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }
        #endregion

        #region Search By Code

        //public ConceptsDataSet.Concepts_ImportDataTable SearchByCode(int billerid, String code)
        //{
        //    ConceptsDataSet.Concepts_ImportDataTable table = new ConceptsDataSet.Concepts_ImportDataTable();
        //    this.SearchByCode(table, billerid, code);
        //    return table;
        //}

        public bool SearchByCode(ConceptsDataSet.Concepts_ImportDataTable table, int BillerID, String code)
        {
            try
            {
                using (Concepts_ImportTableAdapter adapter = new Concepts_ImportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, code);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Read File
        public String ReadFile(ref DataTable table, int BillerID, String File)
        {
            table = new DataTable();
            DataRow row;
            String codeAux = String.Empty;
            int cont = 0;

            try
            {

                int Lines = File.Split(Environment.NewLine[1]).Length;

                String[] products = new String[Lines];
                Array.Copy(File.Split(Environment.NewLine[1]), products, Lines);

                for (int i = 0; i < products.Length; i += 1)
                {
                    codeAux = products[i].Split('|')[1];

                    for (int j = products.Length - 1; j > i; j -= 1)
                        if (codeAux == products[j].Split('|')[1])
                            return ((int)TypesErrorsImport.Twice).ToString() + "_" + (i + 1) + "_" + (j + 1)
                                + "_" + products[i] + "_" + products[j];
                }

                codeAux = String.Empty;

                table.Columns.Add("Code");
                table.Columns.Add("Type");
                table.Columns.Add("Description");
                table.Columns.Add("UnitPrice");
                table.Columns.Add("ApplyTax");
                table.Columns.Add("Account");
                table.Columns.Add("CodeExists");
                table.Columns.Add("State");

                for (int i = 0; i < products.Length; i += 1)
                {
                    row = table.NewRow();

                    if (products[i].Split('|')[cont].ToUpper() != "P")
                        return ((int)TypesErrorsImport.MissingHeader).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];

                    cont += 1;

                    codeAux = products[i].Split('|')[cont];

                    if (codeAux.Length > 10)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];

                    row["CodeExists"] = this.SearchByCode(BillerID, codeAux) ? true : false;
                    row["State"] = "0";

                    if (codeAux == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i] + "_" + cont;

                    row["Code"] = codeAux;
                    cont += 1;

                    if (products[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i] + "_" + cont;
                    if (products[i].Split('|')[cont].Length > 30)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];

                    row["Type"] = products[i].Split('|')[cont];
                    cont += 1;

                    if (products[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i] + "_" + cont;
                    if (products[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];

                    row["Description"] = products[i].Split('|')[cont];
                    cont += 1;

                    if (products[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i] + "_" + cont;

                    try
                    {
                        row["UnitPrice"] = Convert.ToDecimal(products[i].Split('|')[cont]);
                        cont += 1;
                    }
                    catch { return ((int)TypesErrorsImport.ErrorConversion).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i]; }

                    if (products[i].Split('|')[cont] == String.Empty || products[i].Split('|')[cont] == "0" || products[i].Split('|')[cont] == "1") { }
                    else return ((int)TypesErrorsImport.ReadingError).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];

                    row["ApplyTax"] = products[i].Split('|')[cont];
                    cont += 1;

                    if (products[i].Split('|')[cont].Length > 30)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + products[i];
                    row["Account"] = products[i].Split('|')[cont];
                    cont = 0;

                    table.Rows.Add(row);
                }

                table.AcceptChanges();
                return ((int)TypesErrorsImport.Success).ToString();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return ((int)TypesErrorsImport.FatalError).ToString();
            }
        }
        #endregion

        #region RandomNumber
        protected String RandomNumber()
        {
            Random r = new Random(DateTime.Now.Millisecond);
            return r.Next(0, 1000).ToString();
        }
        #endregion

        #region Import Insert Row
        public bool Import_InsertRow(int BillerID, DataRow row)
        {
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.ConceptsRow drConcept;

            try
            {
                drConcept = ds.Concepts.NewConceptsRow();

                if (Convert.ToInt32(row["State"]) == 0)
                {
                    drConcept.Code = row["Code"].ToString();
                }
                else if (Convert.ToInt32(row["State"]) == 2)
                {
                    drConcept.Code = "AG-" + this.RandomNumber() + DateTime.Now.Minute + DateTime.Now.Second;
                    row["Code"] = drConcept.Code;
                }

                drConcept.BillerID = BillerID;
                drConcept.UnitType = row["Type"].ToString();
                drConcept.Description = row["Description"].ToString();
                drConcept.UnitPrice = Convert.ToDecimal(row["UnitPrice"].ToString());
                drConcept.TaxRatePercentage = Convert.ToDecimal(0);
                drConcept.Active = true;
                drConcept.TaxType = 0;
                drConcept.AppliesTax = row["ApplyTax"].ToString() == "1" ? true : false;
                drConcept.Account = row["Account"].ToString();

                ds.Concepts.AddConceptsRow(drConcept);

                this.Update(ds.Concepts);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion

        #region Import Modify Row
        public bool Import_ModifyRow(int BillerID, DataRow row)
        {
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.Concepts_ImportRow drImport;

            try
            {
                this.SearchByCode(ds.Concepts_Import, BillerID, row["Code"].ToString());
                drImport = ds.Concepts_Import[0];

                drImport.Code = row["Code"].ToString();
                drImport.BillerID = BillerID;
                drImport.UnitType = row["Type"].ToString();
                drImport.Description = row["Description"].ToString();
                drImport.UnitPrice = Convert.ToDecimal(row["UnitPrice"].ToString());
                drImport.Active = true;
                drImport.AppliesTax = row["ApplyTax"].ToString() == "1" ? true : false;
                drImport.Account = row["Account"].ToString();

                this.Update(ds.Concepts_Import);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion

        #region Import
        public DataTable Import(int BillerID, DataTable table)
        {
            if (table == null)
                return table;

            if (table.Rows.Count < 1)
                return null;

            DataRow row;
            ArrayList array = new ArrayList();

            try
            {
                for (int i = 0; i < table.Rows.Count; i += 1)
                {
                    row = table.Rows[i];

                    switch (Convert.ToInt32(table.Rows[i]["State"]))
                    {
                        case 0:
                        case 2: //Correcto / Editar (insert)

                            table.Rows[i]["CodeExists"] = this.Import_InsertRow(BillerID, row);
                            break;

                        case 1: //Ignorar

                            array.Add(table.Rows[i]);
                            //table.Rows[i].Delete();
                            break;

                        case 3: //Sobreescribir (update)

                            table.Rows[i]["CodeExists"] = this.Import_ModifyRow(BillerID, row);
                            break;
                    }

                    table.AcceptChanges();
                }

                this.DeleteRowsInTable(ref table, array);

                return table;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return null;
            }
        }
        #endregion

        #region Delete Rows In Table
        private void DeleteRowsInTable(ref DataTable dt, ArrayList array)
        {
            if (array == null || array.Count < 1)
                return;

            foreach (DataRow dr in array)
                dt.Rows.Remove(dr);
        }
        #endregion
        #endregion
    }
}