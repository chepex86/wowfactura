﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Layer class for smtp configuration table.
    /// </summary>
    public class SmtpConfiguration
    {
        /// <summary>
        /// Update, delete or insert a new DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table.
        /// Only one table can exist in the data base.
        /// </summary>
        /// <param name="table">Table to be update, delete or inserted</param>
        /// <returns>
        /// If success return true else false. 
        /// If a SmtpConfigurationDataTable already exist and an insertion is intented
        /// then return false and the insetion is not executed.
        /// </returns>
        public bool Update(DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table)
        {
            if (this.Select().Count > 0)
            {
                foreach (var item in table)
                {
                    if (item.SmtpConfigurationId > 0)
                        continue;

                    LogManager.WriteError(new Exception("Already exist a SmtpConfiguration table."));
                    return false;
                }
            }

            try
            {
                using (DataAccess.SmtpConfigurationDataSetTableAdapters.SmtpConfigurationTableAdapter adapter =
                    new PACFD.DataAccess.SmtpConfigurationDataSetTableAdapters.SmtpConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Get all the DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table.
        /// </summary>
        /// <returns>Return DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table.</returns>
        public DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable Select()
        {
            DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table =
                new PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable();

            try
            {
                using (DataAccess.SmtpConfigurationDataSetTableAdapters.SmtpConfigurationTableAdapter adapter =
                    new PACFD.DataAccess.SmtpConfigurationDataSetTableAdapters.SmtpConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}
