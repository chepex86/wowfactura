﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool = PACFD.Common.Utilities;
using PACFD.Common;
#endregion

namespace PACFD.Rules
{
    public class PlaceDispatch
    {
        /// <summary>
        /// Select all the rows from the biller ID specifict.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the rows.</param>
        public PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable SelectByBillerID(int billerid)
        {
            PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable table =
                new PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable();

            try
            {
                using (PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.GetByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.GetByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Update the row in the data base. Update, insert, delete or modify.
        /// </summary>
        /// <param name="table">Table with the rows to be modify.</param>
        /// <returns>If success true else false.</returns>
        public bool Update(PACFD.DataAccess.PlaceDispatchDataSet.PlaceDispatchDataTable table)
        {
            try
            {
                using (PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.PlaceDispatchTableAdapter adapter =
                    new PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.PlaceDispatchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Set the active state of the Lugare de Expedición, and set all others active = false.
        /// </summary>
        /// <param name="billerid">Biller ID owner.</param>
        /// <param name="placeid">Place to set active state.</param>
        /// <param name="active">Active state of the place.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetActive(int billerid, int placeid, bool active)
        {
            try
            {
                using (PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.QueriesTableAdapter query =
                    new PACFD.DataAccess.PlaceDispatchDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spPlaceDispatch_Active(placeid, billerid, active);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
    }
}
