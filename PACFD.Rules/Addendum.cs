﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class Addendum
    {
        /// <summary>
        /// Select an addendum by ID.
        /// </summary>
        /// <param name="addendumid">Addendum ID</param>
        /// <returns>DataAccess.AddendumDataSet.AddendumDataTable</returns>
        public DataAccess.AddendumDataSet.AddendumDataTable SelectByID(int addendumid)
        {
            DataAccess.AddendumDataSet.AddendumDataTable table = new PACFD.DataAccess.AddendumDataSet.AddendumDataTable();

            try
            {
                using (DataAccess.AddendumDataSetTableAdapters.AddendumTableAdapter adapter =
                    new PACFD.DataAccess.AddendumDataSetTableAdapters.AddendumTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, addendumid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return table;
        }
        /// <summary>
        /// Get all the addendum.
        /// </summary>
        /// <returns>DataAccess.AddendumDataSet.GetAllDataTable</returns>
        public DataAccess.AddendumDataSet.GetAllDataTable SelectAll()
        {
            DataAccess.AddendumDataSet.GetAllDataTable table = new PACFD.DataAccess.AddendumDataSet.GetAllDataTable();

            try
            {
                using (DataAccess.AddendumDataSetTableAdapters.GetAllTableAdapter adapter =
                    new PACFD.DataAccess.AddendumDataSetTableAdapters.GetAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return table;
        }
        /// <summary>
        /// Update, modify, insert or delete row(s) from the data base.
        /// </summary>
        /// <param name="table">Table with the fields to be modify.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.AddendumDataSet.AddendumDataTable table)
        {
            try
            {
                using (DataAccess.AddendumDataSetTableAdapters.AddendumTableAdapter adapter =
                    new PACFD.DataAccess.AddendumDataSetTableAdapters.AddendumTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }

            return true;
        }
    }
}
