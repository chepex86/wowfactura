﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using PACFD.DataAccess;
using System.Data;
using System.Net;
using System.IO;
using PACFD.DataAccess.SystemConfigurationDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class SystemConfiguration
    {
        public bool Update(SystemConfigurationDataSet.SystemConfigurationDataTable table)
        {
            try
            {
                using (SystemConfigurationTableAdapter adapter = new SystemConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }

        public SystemConfigurationDataSet.SystemConfigurationDataTable GetAllSystemConfiguration()
        {
            SystemConfigurationDataSet.SystemConfigurationDataTable table;

            table = new SystemConfigurationDataSet.SystemConfigurationDataTable();
            this.GetAllSystemConfiguration(table);

            return table;
        }

        public bool GetAllSystemConfiguration(SystemConfigurationDataSet.SystemConfigurationDataTable table)
        {
            try
            {
                using (SystemConfigurationTableAdapter adapter = new SystemConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }

        public bool OverwriteConfiguration()
        {
            SystemConfigurationDataSet.SystemConfigurationDataTable table;
            SystemConfigurationDataSet.SystemConfigurationRow row;

            table = this.GetAllSystemConfiguration();
            row = table[0];

            try
            {
                row.LatestNewsReview = DateTime.Now;
                this.Update(table);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }

        public bool AddSystemConfiguration()
        {
            SystemConfigurationDataSet.SystemConfigurationDataTable table;
            table = new SystemConfigurationDataSet.SystemConfigurationDataTable();
            SystemConfigurationDataSet.SystemConfigurationRow row;

            try
            {
                row = table.NewSystemConfigurationRow();

                row.LatestNewsReview = DateTime.Now;
                row.SystemConfigurationID = 1;
                row.CFDEnable = true;
                row.CBBEnable = false;
                row.CFDIEnable = false;
                row.CFD2_2Enable = false;
                row.CFDI3_2Enable = false;
                row.CFDI3_3Enable = false;
                row.CFDI4_0Enable = true;
                table.AddSystemConfigurationRow(row);

                this.Update(table);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
    }
}