﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Data;
using System.IO;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.DataAccess.NewsDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class News
    {
        #region Update
        public bool Update(NewsDataSet.NewsDataTable table)
        {
            NewsTableAdapter adapter;

            try
            {
                using (adapter = new NewsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        #endregion

        #region Get All News
        public NewsDataSet.NewsDataTable GetAllNews()
        {
            using (NewsDataSet.NewsDataTable table = new NewsDataSet.NewsDataTable())
            {
                DataSet ds = new DataSet();
                this.GetAllNews(table);

                if (table.Count > 0)
                    return table;

                ds = this.GetXMLDataSet();

                if (ds == null)
                    return table;

                this.AddNews(ds.Tables[2]);

                ds.Dispose();

                return this.GetAllNews();
            }
        }

        public bool GetAllNews(NewsDataSet.NewsDataTable table)
        {
            try
            {
                using (NewsTableAdapter adapter = new NewsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        #endregion

        #region Get XML DataSet
        public DataSet GetXMLDataSet()
        {
            try
            {
                HttpWebRequest xmlRequest = (HttpWebRequest)WebRequest.Create("http://www.sat.gob.mx/sitio_internet/NovedadesSAT.rss");
                WebResponse xmlResponse = xmlRequest.GetResponse();
                Stream responseStream = xmlResponse.GetResponseStream();
                DataSet xmlData = new DataSet();
                xmlData.ReadXml(responseStream);
                return xmlData;
            }
            catch (Exception ex)
            {
                LogManager.WriteWarning(ex);
                return null;
            }
        }
        #endregion

        #region Add News
        public bool AddNews(DataTable dt)
        {
            NewsDataSet.NewsDataTable dtNews = new NewsDataSet.NewsDataTable();
            NewsDataSet.NewsRow rNews;

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    rNews = dtNews.NewNewsRow();

                    rNews.NewsID = i + 1;
                    rNews.Title = dt.Rows[i]["title"].ToString();

                    if (dt.Rows[i]["link"].ToString().Contains("http://www.sat.gob.mx"))
                        rNews.Link = dt.Rows[i]["link"].ToString();
                    else
                        rNews.Link = "http://www.sat.gob.mx" + dt.Rows[i]["link"].ToString();
                    rNews.Guid = dt.Rows[i]["guid"].ToString();
                    rNews.Description = dt.Rows[i]["description"].ToString();
                    rNews.PublicationDate = Convert.ToDateTime(dt.Rows[i]["pubDate"].ToString());
                    rNews.ChannelID = Convert.ToInt32(dt.Rows[i]["channel_Id"].ToString());

                    dtNews.AddNewsRow(rNews);
                }

                this.Update(dtNews);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        #endregion

        #region Delete All News
        public bool DeleteAllNews()
        {
            NewsDataSet dsNews = new NewsDataSet();
            NewsDataSet.NewsRow rNews;

            this.GetAllNews(dsNews.News);

            try
            {
                for (int i = 0; i < dsNews.News.Count; i++)
                {
                    rNews = dsNews.News[i];
                    rNews.Delete();
                }

                this.Update(dsNews.News);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        #endregion

        #region Compare Dates
        public int CompareDates(DateTime dt1)
        {
            int cont = 0;

            try
            {
                while (dt1 < DateTime.Now)
                {
                    cont += 1;
                    dt1 = dt1.AddHours(1);
                }

                return cont;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return -1;
            }
        }
        #endregion
    }
}