﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when the credit value is not valid.
    /// </summary>
    public class BillingCreditException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingIsCredit; } }
        public BillingCreditException(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
