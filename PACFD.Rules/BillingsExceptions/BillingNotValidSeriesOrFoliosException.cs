﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when not a filio or serial is available.
    /// </summary>
    public class BillingNotValidSeriesOrFoliosException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingFolioOrSerialNotAvailable; } }
        public BillingNotValidSeriesOrFoliosException(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
