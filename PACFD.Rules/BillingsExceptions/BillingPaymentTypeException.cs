﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when payment type is not accepted.
    /// </summary>
    public class BillingPaymentTypeException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingPaymentType; } }

        public BillingPaymentTypeException(string msg, int rowindex, int valueindex, string value)
            : base(msg, rowindex, valueindex, value) { }
    }
}
