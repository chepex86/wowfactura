﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// 
    /// </summary>
    public class BillingTaxeTypeNotValid : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingTaxeTypeNotValid; } }

        public BillingTaxeTypeNotValid(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
