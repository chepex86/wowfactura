﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    public class WebServiceCFDIInsertingErrorExternalFolioActiveAsPrebilling : BillingExceptionBase
    {
        public PACFD.DataAccess.BillingsDataSet Billling { get; private set; }
        public bool IsPrebilling { get { return this.Billling.Billings[0].PreBilling; } }

        public WebServiceCFDIInsertingErrorExternalFolioActiveAsPrebilling(string msg, string value, PACFD.DataAccess.BillingsDataSet dataset) :
            base(msg, 0, 0, PACFD.Common.WebServiceErrorType.CFDIInsertingErrorExternalFolioActiveAsPrebilling, value)
        {
            this.Billling = dataset;
        }
    }
}
