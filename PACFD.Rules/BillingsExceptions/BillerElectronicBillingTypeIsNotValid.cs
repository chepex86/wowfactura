﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Biller bill or seal access level invalid.
    /// </summary>
    public class BillerElectronicBillingTypeIsNotValid : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillerElectronicBillingTypeIsNotValid; } }

        public BillerElectronicBillingTypeIsNotValid(string msg) : base(msg, 0, 0, string.Empty) { }
    }
}
