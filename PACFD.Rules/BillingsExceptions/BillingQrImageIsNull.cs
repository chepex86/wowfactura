﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// 
    /// </summary>
    public class BillingQrImageIsNull : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingQrImageIsNull; } }

        public BillingQrImageIsNull(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
