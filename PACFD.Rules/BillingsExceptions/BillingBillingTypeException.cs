﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when the billing type is not know or don't 
    /// match with the data base.
    /// </summary>
    public class BillingBillingTypeException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingBillingType; } }
        public BillingBillingTypeException(string msg, int rowindex, int valueindex, string value)
            : base(msg, rowindex, valueindex, value) { }
    }
}
