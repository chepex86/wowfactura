﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    public class BillingBranchIsNotValid : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingBranchIsNotValid; } }

        public BillingBranchIsNotValid(string msg, int rowindex, string value) : base(msg, rowindex, 0, value) { }
    }
}
