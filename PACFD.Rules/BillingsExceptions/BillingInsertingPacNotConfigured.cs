﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    public class BillingInsertingPacNotConfigured : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingInsertingPacNotConfigured; } }


        public BillingInsertingPacNotConfigured(string msg) : base(msg, 0, 0, string.Empty) { }
    }
}
