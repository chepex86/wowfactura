﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    public class BillingRFCIsEmpty : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingRFCIsEmpty; } }

        public BillingRFCIsEmpty(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
