﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when digital certificate is not valid.
    /// </summary>
    public class BillingNotValidDigitalCertificatesException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingNotValidDigitalCertificates; } }

        public int DigitalCertificateID { get; private set; }

        public BillingNotValidDigitalCertificatesException(string msg, int rowindex, int valueindex, string value)
            : base(msg, rowindex, valueindex, value) { }

        public BillingNotValidDigitalCertificatesException(string msg, int digitalcertificateID, int rowindex, int valueindex, string value)
            : this(msg, rowindex, valueindex, value) { this.DigitalCertificateID = digitalcertificateID; }
    }
}
