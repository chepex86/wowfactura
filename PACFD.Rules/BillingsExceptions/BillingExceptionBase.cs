﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Exception base for exception handlers.
    /// </summary>
    public class BillingExceptionBase : Exception
    {
        public int ValueIndex { get; private set; }
        public int RowIndex { get; private set; }
        public virtual PACFD.Common.WebServiceErrorType ErrorCode { get; private set; }
        public string Value { get; private set; }

        public BillingExceptionBase(string msg, int rowindex, int valueindex, string value)
            : base(msg)
        {
            this.ValueIndex = valueindex;
            this.RowIndex = rowindex;
            this.Value = value;
            this.ErrorCode = PACFD.Common.WebServiceErrorType.None;
        }
        public BillingExceptionBase(string msg, int rowindex, int valueindex, PACFD.Common.WebServiceErrorType errorcode, string value)
            : base(msg)
        {
            this.ValueIndex = valueindex;
            this.RowIndex = rowindex;
            this.Value = value;
            this.ErrorCode = errorcode;
        }
    }
}
