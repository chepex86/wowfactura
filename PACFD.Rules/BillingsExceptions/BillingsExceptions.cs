﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// A list class used to store exceptions.
    /// </summary>
    public class ExceptionCollection : List<Exception> { }

    public class ExceptionList : Exception
    {
        public ExceptionCollection Exceptions { get; private set; }

        public ExceptionList(ExceptionCollection list)
        {
            this.Exceptions = list;
        }
    }
}
