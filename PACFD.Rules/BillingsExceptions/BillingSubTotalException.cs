﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when sub total is not valid.
    /// </summary>
    public class BillingSubTotalException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingSubTotalIsNotValid; } }
        public BillingSubTotalException(string msg, int rowindex, int valueindex, string value) : base(msg, rowindex, valueindex, value) { }
    }
}
