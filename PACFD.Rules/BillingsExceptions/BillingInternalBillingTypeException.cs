﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Error generated when billing type is not valid.
    /// </summary>
    public class BillingInternalBillingTypeException : BillingExceptionBase
    {
        public override PACFD.Common.WebServiceErrorType ErrorCode { get { return PACFD.Common.WebServiceErrorType.BillingInternalBillingType; } }
        public BillingInternalBillingTypeException(string msg, int rowindex, int valueindex, string value)
            : base(msg, rowindex, valueindex, value) { }
    }
}
