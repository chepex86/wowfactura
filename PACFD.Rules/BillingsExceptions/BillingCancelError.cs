﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;

namespace PACFD.Rules.BillingsExceptions
{
    /// <summary>
    /// Throw an excption when the pac can't cancel a billing.
    /// </summary>
    public class BillingCancelError : Exception
    {
        /// <summary>
        /// Error detail send by the pac.
        /// </summary>
        public ErrorManager PacError { get; private set; }
        /// <summary>
        /// Electronic billing type.
        /// </summary>
        public PACFD.Rules.ElectronicBillingType ElectronicBillingType { get; private set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="electronictype">Electronic billing type.</param>
        /// <param name="error">Error send by the Pac.</param>
        public BillingCancelError(PACFD.Rules.ElectronicBillingType electronictype, ErrorManager error) : base(error.Error.Message)
        {
            this.PacError = error;
            this.ElectronicBillingType = electronictype;
        }
    }
}
