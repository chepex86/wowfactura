﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.DataAccess.CountrySelectorDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class CountrySelectors
    {
        public CountrySelectorDataSet.Cities_GetByNameDataTable SearchCity(String cityname)
        {
            CountrySelectorDataSet.Cities_GetByNameDataTable table;

            table = new CountrySelectorDataSet.Cities_GetByNameDataTable();

            try
            {
                using (Cities_GetByNameTableAdapter adapter = new Cities_GetByNameTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, cityname);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.Countries_GetAllDataTable SelectAllCountries()
        {
            CountrySelectorDataSet.Countries_GetAllDataTable table;
            table = new CountrySelectorDataSet.Countries_GetAllDataTable();

            try
            {
                using (Countries_GetAllTableAdapter adapter = new Countries_GetAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.State_GetByCountryIDDataTable SelectStateByCountryID(String countryid)
        {
            CountrySelectorDataSet.State_GetByCountryIDDataTable table;
            table = new CountrySelectorDataSet.State_GetByCountryIDDataTable();

            try
            {
                using (State_GetByCountryIDTableAdapter adapter = new State_GetByCountryIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, countryid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.Municipality_GetByStateIDDataTable SelectMunicipalityByStateID(int stateid)
        {
            CountrySelectorDataSet.Municipality_GetByStateIDDataTable table;
            table = new CountrySelectorDataSet.Municipality_GetByStateIDDataTable();

            try
            {
                using (Municipality_GetByStateIDTableAdapter adapter = new Municipality_GetByStateIDTableAdapter())
                {
                    adapter.Fill(table, stateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.Cities_GetByMunicipalityIDDataTable SelectCityByMunicipalityID(int municipalityid)
        {
            CountrySelectorDataSet.Cities_GetByMunicipalityIDDataTable table;
            table = new CountrySelectorDataSet.Cities_GetByMunicipalityIDDataTable();

            try
            {
                using (Cities_GetByMunicipalityIDTableAdapter adapter = new Cities_GetByMunicipalityIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, municipalityid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.Cities_GetByStateCodeDataTable SelectCityByStateCode(String code)
        {
            CountrySelectorDataSet.Cities_GetByStateCodeDataTable table;
            table = new CountrySelectorDataSet.Cities_GetByStateCodeDataTable();

            try
            {
                using (Cities_GetByStateCodeTableAdapter adapter = new Cities_GetByStateCodeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, code);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable SelectCityStateCountryByCityID(int cityid)
        {
            CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable table;
            table = new CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable();

            try
            {
                using (Cities_GetCityStateCountryByCityIDTableAdapter adapter = new Cities_GetCityStateCountryByCityIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, cityid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}