﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.CustomReportsDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class CustomReports
    {
        #region Report_001
        public CustomReportsDataSet.Report_001DataTable Report_001(int? BillingID, DateTime? Start, DateTime? End, int? ReceptorID, string UnitType, bool? Status, int? ConceptID, bool? PreBilling, bool? IsCredit)
        {
            CustomReportsDataSet.Report_001DataTable table;

            table = new CustomReportsDataSet.Report_001DataTable();
            this.Report_001(table, BillingID, Start, End, ReceptorID, UnitType, Status, ConceptID, PreBilling, IsCredit);

            return table;
        }
        public bool Report_001(CustomReportsDataSet.Report_001DataTable table, int? BillingID, DateTime? Start, DateTime? End, int? ReceptorID, string UnitType, bool? Status, int? ConceptID, bool? PreBilling, bool? IsCredit)
        {
            Report_001TableAdapter adapter;

            try
            {
                using (adapter = new Report_001TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillingID, Start, End, ReceptorID, UnitType, Status, ConceptID, PreBilling, IsCredit);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Report_002 Concepts/Products
        public CustomReportsDataSet.Report_002DataTable Report_002(int BillerID, int? AppliesTax, string UnitType)
        {
            CustomReportsDataSet.Report_002DataTable table;

            table = new CustomReportsDataSet.Report_002DataTable();
            this.Report_002(table, BillerID, AppliesTax, UnitType);

            return table;
        }
        public bool Report_002(CustomReportsDataSet.Report_002DataTable table, int? BillerID, int? AppliesTax, string UnitType)
        {
            Report_002TableAdapter adapter;

            try
            {
                using (adapter = new Report_002TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, UnitType, AppliesTax);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Report_003 Receptors
        public CustomReportsDataSet.Report_003DataTable Report_003(int BillerID)
        {
            CustomReportsDataSet.Report_003DataTable table;

            table = new CustomReportsDataSet.Report_003DataTable();
            this.Report_003(table, BillerID);

            return table;
        }
        public bool Report_003(CustomReportsDataSet.Report_003DataTable table, int BillerID)
        {
            Report_003TableAdapter adapter;

            try
            {
                using (adapter = new Report_003TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Report_004 
        public CustomReportsDataSet.Report_004DataTable Report_004(int? BillingID, DateTime? Start, DateTime? End, int? ReceptorID, string UnitType, bool? Status, int? ConceptID)
        {
            CustomReportsDataSet.Report_004DataTable table;

            table = new CustomReportsDataSet.Report_004DataTable();
            this.Report_004(table, BillingID, Start, End, ReceptorID, UnitType, Status, ConceptID);

            return table;
        }
        public bool Report_004(CustomReportsDataSet.Report_004DataTable table, int? BillingID, DateTime? Start, DateTime? End, int? ReceptorID, string UnitType, bool? Status, int? ConceptID)
        {
            Report_004TableAdapter adapter;

            try
            {
                using (adapter = new Report_004TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillingID, Start, End, ReceptorID, UnitType, Status, ConceptID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region InvoiceReport
        public CustomReportsDataSet.InvoiceReportDataTable InvoiceReport(int? GroupID, int? BillerID, Byte? TypeOfBill, DateTime? Start, DateTime? End)
        {
            CustomReportsDataSet.InvoiceReportDataTable table;

            table = new CustomReportsDataSet.InvoiceReportDataTable();
            this.InvoiceReport(table, GroupID, BillerID, TypeOfBill, Start, End);

            return table;
        }
        private bool InvoiceReport(CustomReportsDataSet.InvoiceReportDataTable table, int? GroupID, int? BillerID, Byte? TypeOfBill, DateTime? Start, DateTime? End)
        {
            try
            {
                using (InvoiceReportTableAdapter adapter = new InvoiceReportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, GroupID, BillerID, TypeOfBill, Start, End);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion
    }
}