﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool = PACFD.Common.Utilities;
using PACFD.Common;

namespace PACFD.Rules
{
    public class BillingsAddendum
    {
        public Boolean Update(DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable table)
        {
            try
            {
                using (DataAccess.BillingsAddendumDataSetTableAdapters.BillingsAddendumTableAdapter adapter =
                    new PACFD.DataAccess.BillingsAddendumDataSetTableAdapters.BillingsAddendumTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable SelectByID(int billingid)
        {
            DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable table = new PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable();

            try
            {
                using (DataAccess.BillingsAddendumDataSetTableAdapters.BillingsAddendumTableAdapter adapter =
                    new PACFD.DataAccess.BillingsAddendumDataSetTableAdapters.BillingsAddendumTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billingid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }

        public bool Exist(int billingid)
        {
            using (DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable table = this.SelectByID(billingid))
            { return (table.Count > 0); }
        }
    }
}
