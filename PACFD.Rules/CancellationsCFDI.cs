﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules.mx.com.buzone.prepserviciosweb;

namespace PACFD.Rules
{
    public class CancellationsCFDI
    {

        public static bool SendToCancellation(BillingsDataSet ds, ref ErrorManager error)
        {
            try
            {

                string userName = string.Empty;
                string password = string.Empty;                
                string url = string.Empty;
                int billerID = ds.Billings[0].BillerID;
                bool simulate = false;
                
                     
                if (LoadConfiguration(billerID, ref url, ref userName, ref password,ref simulate ))
                {
                    if (simulate)
                    {
                        return true;
                    }
                    else
                    {
                        CancelacionCFDService ws = new CancelacionCFDService();
                        CancelacionResponse res = new CancelacionResponse();
                        CancelacionRequest req = new CancelacionRequest();
                        req.usuario = userName;
                        req.password = password;
                        req.numeroActivacion = "100";
                        req.SolicitudCancelacion = new Cancelaciones
                        {
                            Cancelacion = new CancelacionesCancelacion
                            {
                                RfcEmisor = ds.BillingsBillers[0]?.BillerRFC,
                                Folios = new CancelacionesCancelacionFolios
                                {
                                    UUID = !ds.Billings[0].IsUUIDNull() ? ds.Billings[0].UUID : ""
                                }
                            }
                        };
                        ws.Url = url;

                        LogManager.WritePacTrace("Request", GetXmlOfRequest(req));
                        try
                        {
                            res = ws.CancelacionCFD(req);
                        }
                        catch (Exception ex)
                        {
                            
                            LogManager.WritePacTrace("Responce", ex.Message);
                            error.SetErrorPAC("Error del PAC");
                            return false;
                        }
                        if (res != null)
                        {

                            //LogManager.WritePacTrace("Responce", GetXmlOfResponce(res[0]));

                            //if (res[0].Status == 201 || res[0].Status == 202)
                            //    return true;
                            //else
                            //{
                            //    error.SetCustomError(res[0].Status.ToString(), res[0].ErrorDescription != null ? res[0].ErrorDescription : "Error desconocido");
                            //}

                        }
                        else
                        {
                            error.SetCustomError("Stamping002", "Respuesta vacia");
                            LogManager.WritePacTrace("Respuesta vacia", string.Empty);
                        }
                    }
                    

                }
                else
                {
                    error.SetCustomError("Stamping001", "Acceso al PAC no configurado");
                    LogManager.WritePacTrace("PAC no configurado", string.Empty);
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return false;
        }

//        private static string GetXmlOfResponce(Output rs)
//        {
//            try
//            {
//                if (rs != null)
//                {
//                    #region XML
//                    string xml =
//@"<CancelaCFDISATResponse>
//    <CancelaCFDISATResult>
//        <Output>
//          <Status>{0}</Status>
//          <FieldName>{1}</FieldName>
//          <ErrorDescription>{2}</ErrorDescription>
//        </Output>        
//    </CancelaCFDISATResult>
//</CancelaCFDISATResponse>";
//                    #endregion
//                    return string.Format(xml
//                            , rs.Status != null ? rs.Status.ToString() : string.Empty
//                            , rs.FieldName != null ? rs.FieldName : string.Empty
//                            , rs.ErrorDescription != null ? rs.ErrorDescription : "");
//                }

//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }
//            return string.Empty;
//        }

        private static string GetXmlOfRequest(CancelacionRequest rq)
        {
            try
            {
                if (rq != null)
                {
                    #region XML
                    string xml =
@"<CancelaCFDISAT>
    <input>
        <SenderId>{0}</SenderId>
        <SenderPwd>{1}</SenderPwd>
        <UUIDList>
            <string>{2}</string>
        </UUIDList>
    </input>
</CancelaCFDISAT>";
                    #endregion
                    return string.Format(xml
                            , rq.usuario != null ? rq.usuario : string.Empty
                            , rq.password != null ? rq.password : string.Empty
                            , rq.SolicitudCancelacion != null && rq.SolicitudCancelacion.Cancelacion != null && rq.SolicitudCancelacion.Cancelacion.Folios != null ? rq.SolicitudCancelacion.Cancelacion.Folios.UUID
                            : string.Empty
                        );
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return string.Empty;
        }
    

        private static bool LoadConfiguration(int billerID, ref string url, ref string user, ref string password, ref bool simulate)
        {
            try
            {
                PacConfiguration admin = new PacConfiguration();
                PacConfigurationDataSet.GetByBillerIDDataTable table = admin.SelectByBillerID(billerID);
                simulate = false;
                if (table != null && table.Rows.Count > 0)
                {
                    PacConfigurationDataSet.GetByBillerIDRow dr = table[0];
                    if (dr != null)
                    {
                        url = dr.UrlCancellation.Trim();                        
                        user = dr.Username.Trim();
                        password = Cryptography.DecryptUnivisitString(dr.Password.Trim());
                        if (!dr.IsSimulateNull())
                            simulate = dr.Simulate;
                        return true;
                    }

                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return false;
        }


    }
}
