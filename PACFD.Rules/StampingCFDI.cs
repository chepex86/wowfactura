﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using System.Net;
using PACFD.DataAccess;
using System.IO;
using System.Xml;

using PACFD.DataAccess.SystemConfigurationDataSetTableAdapters;
using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security.Tokens;
//using PACFD.Rules.mx.com.buzone.prepws;
using PACFD.Rules.PACBajio;
using System.Xml.Serialization;
using System.Text.RegularExpressions;

using SW.Helpers;
using SW.Services.Stamp;


#endregion

namespace PACFD.Rules
{
    public class StampingCFDI
    {
        public static bool SendToStamping(BillingsDataSet ds, ref ErrorManager error)
        {
            string userName = string.Empty;
            string password = string.Empty;
            string fileType = string.Empty;
            string token = string.Empty;
            string url = string.Empty;
            bool simulate = false;
            try
            {
                int billerID = ds.Billings[0].BillerID;

                if (LoadConfiguration(billerID, ref url, ref userName, ref password, ref fileType, ref token, ref simulate))
                {
                    if (simulate)
                    {
                        ds.Billings[0].UUID = string.Format("3C65608E-767F-4BBD-Testxxxx-{0}", DateTime.Now.ToString("yyddmmss"));
                        ds.Billings[0].DateStamped = DateTime.Now;
                        ds.Billings[0].CertificateNumberSat = "30001000000100000801";
                        ds.Billings[0].SealSat = "XVMSOakrqNt7cJtGIa3HEYI/otX3J2imZpGP4O8wBk6wKct1zgoTQKKQQMPYAFt64huYe5j6BHbwSpWC8ob7w3tN9tcM3VXVAo7SJmRNwO1EYr16JaBrQwkalt4l6pQxvRgZ6a3gDLIDNDh1RyhxWl4aekl6+PERXpxCZCx+8HQ=";
                        ds.Billings[0].VersionSat = "1.0";
                        ds.Billings[0].BatchIdSat = "0000";
                        ds.Billings[0].StatusSat = "0";
                        ds.Billings[0].RfcProvCertic = "SFE0807172W7";
                        ds.Billings[0].SelloCFD = "kqFJelLV1B99l8DunR98pn5k8uCwi94k+PhZJ2crw5Et2+qSkoROBlOWsoXrv93j5UUPBkxTmlStwTGBHSHWiQr1ewA4/fzseNIaWokVbIr8iFwrZETgZWp6Q+dErqLntDPISWYHmXfnvXi/Om7hCtklZC/msv3ZmmojcZEkjJb0Rk+sVDh+qm3sRbx40k1xKX1xtgLnX4/P/DwZlv+mj31YE4MEpy48xMjBA7dPNE4dF9UL3mTAQhwMV40MCrLpjTn95ov8mnL0ftaxuqzGXhqqNcDk1YF5OtBXuGFKwWAfY53bNxz1GeVY1+/8xZsmUmuukwA5uvIa7ghGt/4twA==";
                        ds.Billings[0].OrignalStringSat = GenerateOrinalStringTFD(ds);

                        LogManager.WritePacTrace("Response", "Simulado por el buzonE es chafa");
                        return true;
                    }
                    else
                    {

                        StampResponseV4 rs = null;
                        string xml = GetXmlCFDI(ds);
                        byte[] cfdiBase64 = System.Text.Encoding.UTF8.GetBytes(xml);
                        LogManager.WritePacTrace("Request", xml);

                        try
                        {

                            Stamp stamp = new Stamp(url, userName, password,0,null);
                            rs = stamp.TimbrarV4(xml);
                            LogManager.WritePacTrace("Response", toXml(rs));
                        }
                        catch (Exception ex)
                        {
                            error.SetCustomError("500",ex.Message);
                            LogManager.WritePacTrace("Response", ex.Message);
                            throw ex;
                        }
                        if (rs != null)
                        {
                            if ( rs.status=="error")//   rs.uuid == null ||rs.uuid == "") 307 ojo. 
                            {

                                // Ya existe el comprobante firmado..  extraerlo.. 
                                if (!rs.message.StartsWith("307.") && rs.messageDetail != "" )
                                {
                                    error.SetCustomError("405", rs.message);
                                    LogManager.WritePacTrace("Response", rs.message + " DETAIL  :" + rs.messageDetail);
                                    throw new Exception("405" + rs.message);
                            }
                            else
                            {
                                string recoveredCFDI = rs.messageDetail;
                                rs = new StampResponseV4();
                                rs.data = new Data_Complete();
                                rs.data.cfdi = recoveredCFDI;
                                // me faltan los demas datos.. 



                                XmlDocument documente = new XmlDocument();
                                documente.LoadXml(recoveredCFDI);

                                NameTable nte = new NameTable();
                                XmlNamespaceManager nsmgre = new XmlNamespaceManager(nte);
                                nsmgre.AddNamespace("cfdi", "http://www.sat.gob.mx/cfd/4");
                                nsmgre.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");

                                XmlElement roote = documente.DocumentElement;
                                XmlElement nodeTFDe = (XmlElement)roote.SelectSingleNode("cfdi:Complemento", nsmgre).SelectSingleNode("tfd:TimbreFiscalDigital", nsmgre);

                                XmlAttribute rfce = nodeTFDe.GetAttributeNode("RfcProvCertif");
                                XmlAttribute uuide = nodeTFDe.GetAttributeNode("UUID");
                                XmlAttribute selloCFDSATe = nodeTFDe.GetAttributeNode("SelloCFD");
                                XmlAttribute selloSATe = nodeTFDe.GetAttributeNode("SelloSAT");
                                XmlAttribute FechaTimbradoe = nodeTFDe.GetAttributeNode("FechaTimbrado");

                                XmlAttribute versionSATe = nodeTFDe.GetAttributeNode("Version");
                                XmlAttribute NoCertificadoSATe = nodeTFDe.GetAttributeNode("NoCertificadoSAT");


                                rs.data.cadenaOriginalSAT = "";  // No viene de regreso.  abajo hay una validacion para que la haga.. con el ds
                                rs.data.noCertificadoSAT = NoCertificadoSATe.InnerText;
                                rs.data.uuid = uuide.InnerText;
                                rs.data.selloSAT = selloSATe.InnerText;
                                rs.data.selloCFDI = selloCFDSATe.InnerText;
                                rs.data.fechaTimbrado = FechaTimbradoe.InnerText;
                                rs.data.qrCode = "";

                                ds.Billings[0].CertificateNumberSat = rs.data.noCertificadoSAT;
                                ds.Billings[0].Seal = rs.data.selloCFDI;

                                // DEL NODO TFD
                                ds.Billings[0].VersionSat = versionSATe.InnerText;
                                ds.Billings[0].UUID = rs.data.uuid;
                                ds.Billings[0].DateStamped = DateTime.Parse(rs.data.fechaTimbrado); //TFD
                                ds.Billings[0].RfcProvCertic = rfce.InnerText; //TFD
                                ds.Billings[0].SelloCFD = selloCFDSATe.InnerText; //TFD
                                ds.Billings[0].CertificateNumberSat = NoCertificadoSATe.InnerText;  //TFD
                                ds.Billings[0].SealSat = selloSATe.InnerText;    //TFD

                            }

                        }

                                LogManager.WritePacTrace("Response", rs.data.ToString());

                            //la respuesta viene en Base64
                            string decodedString = rs.data.cfdi;
                            XmlDocument document = new XmlDocument();
                            document.LoadXml(decodedString);

                            LogManager.WritePacTrace("CFDI", decodedString);


                            NameTable nt = new NameTable();
                            XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);
                            nsmgr.AddNamespace("cfdi", "http://www.sat.gob.mx/cfd/4");
                            nsmgr.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");

                            XmlElement root = document.DocumentElement;
                            XmlElement nodeTFD = (XmlElement) root.SelectSingleNode("cfdi:Complemento", nsmgr).SelectSingleNode("tfd:TimbreFiscalDigital", nsmgr);

                            XmlAttribute rfc = nodeTFD.GetAttributeNode("RfcProvCertif");
                            XmlAttribute selloCFDSAT = nodeTFD.GetAttributeNode("SelloCFD");
                            XmlAttribute selloSAT = nodeTFD.GetAttributeNode("SelloSAT");

                            XmlAttribute versionSAT = nodeTFD.GetAttributeNode("Version");
                            XmlAttribute NoCertificadoSAT = nodeTFD.GetAttributeNode("NoCertificadoSAT");

                            // DATOS EXISTENTE: 
                            // Version , Serie, Folio, Fecha, Sello, FormaPago, NoCertififcado, Certificado,

                            // DATOS UNIBILLING
                            ds.Billings[0].CertificateNumberSat = rs.data.noCertificadoSAT;
                            ds.Billings[0].Seal = rs.data.selloCFDI;

                            // DEL NODO TFD
                            ds.Billings[0].VersionSat = versionSAT.InnerText;
                            ds.Billings[0].UUID = rs.data.uuid;
                            ds.Billings[0].DateStamped = DateTime.Parse(rs.data.fechaTimbrado); //TFD
                            ds.Billings[0].RfcProvCertic = rfc.InnerText; //TFD
                            ds.Billings[0].SelloCFD = selloCFDSAT.InnerText; //TFD
                            ds.Billings[0].CertificateNumberSat = NoCertificadoSAT.InnerText;  //TFD
                            ds.Billings[0].SealSat = selloSAT.InnerText;    //TFD
                            ds.Billings[0].StatusSat = "OK";   //CL = cancelado.   , // CP  = cancelacion Pendiente.   esto es interno. 
                            ds.Billings[0].OrignalStringSat = rs.data.cadenaOriginalSAT == "" ?  GenerateOrinalStringTFD(ds) : rs.data.cadenaOriginalSAT;
                            ds.Billings[0].BatchIdSat = "";
                         return true;
                        }
                        else
                        {
                            error.SetCustomError("Stamping002", "Respuesta vacia");
                            LogManager.WritePacTrace("Respuesta vacia", string.Empty);
                        }
                    }
                }
                else
                {
                    error.SetCustomError("Stamping001", "Acceso al PAC no configurado");
                    LogManager.WritePacTrace("PAC no configurado", string.Empty);

                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return false;
        }

        public static string toXml<T>(T obj)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, obj);
                    xml = sww.ToString(); // Your XML
                }
            }
            return xml;
        }
     
        
        // METODO DE PAGOS: 
        public static bool SendToStamping(BillingsDataSet ds, bool forpayment, ref ErrorManager error)
        {
            string userName = string.Empty;
            string password = string.Empty;
            string fileType = string.Empty;
            string url = string.Empty;
            string token = string.Empty;
            bool simulate = false;

            try
            {
                int billerID = ds.BillingsPayments_3[0].BillerID;

                if (LoadConfiguration(billerID, ref url, ref userName, ref password, ref fileType, ref token, ref simulate))
                {
                    if (simulate)
                    {
                        ds.BillingsPayments_3[0].UUID = string.Format("3C65608E-767F-4BBD-Testxxxx-{0}", DateTime.Now.ToString("yyddmmss"));
                        ds.BillingsPayments_3[0].DateStamped = DateTime.Now;
                        ds.BillingsPayments_3[0].CertificateNumberSat = "30001000000100000801";
                        ds.BillingsPayments_3[0].SealSat = "XVMSOakrqNt7cJtGIa3HEYI/otX3J2imZpGP4O8wBk6wKct1zgoTQKKQQMPYAFt64huYe5j6BHbwSpWC8ob7w3tN9tcM3VXVAo7SJmRNwO1EYr16JaBrQwkalt4l6pQxvRgZ6a3gDLIDNDh1RyhxWl4aekl6+PERXpxCZCx+8HQ=";
                        ds.BillingsPayments_3[0].VersionSat = "1.0";
                        ds.BillingsPayments_3[0].BatchIdSat = "0000";
                        ds.BillingsPayments_3[0].StatusSat = "0";
                        ds.BillingsPayments_3[0].RfcProvCertic = "SFE0807172W7";
                        ds.BillingsPayments_3[0].SelloCFD = "kqFJelLV1B99l8DunR98pn5k8uCwi94k+PhZJ2crw5Et2+qSkoROBlOWsoXrv93j5UUPBkxTmlStwTGBHSHWiQr1ewA4/fzseNIaWokVbIr8iFwrZETgZWp6Q+dErqLntDPISWYHmXfnvXi/Om7hCtklZC/msv3ZmmojcZEkjJb0Rk+sVDh+qm3sRbx40k1xKX1xtgLnX4/P/DwZlv+mj31YE4MEpy48xMjBA7dPNE4dF9UL3mTAQhwMV40MCrLpjTn95ov8mnL0ftaxuqzGXhqqNcDk1YF5OtBXuGFKwWAfY53bNxz1GeVY1+/8xZsmUmuukwA5uvIa7ghGt/4twA==";
                        ds.BillingsPayments_3[0].OrignalStringSat = GenerateOrinalStringTFD(ds);

                        LogManager.WritePacTrace("Response", "Simulado por el buzonE es chafa");
                        return true;
                    }

                    else
                    {

                        StampResponseV4 rs = null;
                        string xml = GetXmlCFDI(ds);
                        byte[] cfdiBase64 = System.Text.Encoding.UTF8.GetBytes(xml);
                        LogManager.WritePacTrace("Request", xml);

                        try
                        {

                            Stamp stamp = new Stamp(url, userName, password, 0, null);
                            rs = stamp.TimbrarV4(xml);
                            LogManager.WritePacTrace("Response", toXml(rs));
                        }
                        catch (Exception ex)
                        {
                            error.SetCustomError("500", ex.Message);
                            LogManager.WritePacTrace("Response", ex.Message);
                            throw ex;
                        }
                        if (rs != null)
                        {
                            if (rs.status == "error")//   rs.uuid == null ||rs.uuid == "") 307 ojo. 
                            {

                                // Ya existe el comprobante firmado..  extraerlo.. 
                                if (!rs.message.StartsWith("307.") && rs.messageDetail != "")
                                {
                                    error.SetCustomError("405", rs.message);
                                    LogManager.WritePacTrace("Response", rs.message + " DETAIL  :" + rs.messageDetail);
                                    throw new Exception("405" + rs.message);
                                }
                                else
                                {
                                    string recoveredCFDI = rs.messageDetail;
                                    rs = new StampResponseV4();
                                    rs.data = new Data_Complete();
                                    rs.data.cfdi = recoveredCFDI;
                                    // me faltan los demas datos.. 



                                    XmlDocument documente = new XmlDocument();
                                    documente.LoadXml(recoveredCFDI);

                                    NameTable nte = new NameTable();
                                    XmlNamespaceManager nsmgre = new XmlNamespaceManager(nte);
                                    nsmgre.AddNamespace("cfdi", "http://www.sat.gob.mx/cfd/4");
                                    nsmgre.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");

                                    XmlElement roote = documente.DocumentElement;
                                    XmlElement nodeTFDe = (XmlElement)roote.SelectSingleNode("cfdi:Complemento", nsmgre).SelectSingleNode("tfd:TimbreFiscalDigital", nsmgre);

                                    XmlAttribute rfce = nodeTFDe.GetAttributeNode("RfcProvCertif");
                                    XmlAttribute uuide = nodeTFDe.GetAttributeNode("UUID");
                                    XmlAttribute selloCFDSATe = nodeTFDe.GetAttributeNode("SelloCFD");
                                    XmlAttribute selloSATe = nodeTFDe.GetAttributeNode("SelloSAT");
                                    XmlAttribute FechaTimbradoe = nodeTFDe.GetAttributeNode("FechaTimbrado");

                                    XmlAttribute versionSATe = nodeTFDe.GetAttributeNode("Version");
                                    XmlAttribute NoCertificadoSATe = nodeTFDe.GetAttributeNode("NoCertificadoSAT");


                                    rs.data.cadenaOriginalSAT = "";  // No viene de regreso.  abajo hay una validacion para que la haga.. con el ds
                                    rs.data.noCertificadoSAT = NoCertificadoSATe.InnerText;
                                    rs.data.uuid = uuide.InnerText;
                                    rs.data.selloSAT = selloSATe.InnerText;
                                    rs.data.selloCFDI = selloCFDSATe.InnerText;
                                    rs.data.fechaTimbrado = FechaTimbradoe.InnerText;
                                    rs.data.qrCode = "";

                                    ds.Billings[0].CertificateNumberSat = rs.data.noCertificadoSAT;
                                    ds.Billings[0].Seal = rs.data.selloCFDI;

                                    // DEL NODO TFD
                                    ds.Billings[0].VersionSat = versionSATe.InnerText;
                                    ds.Billings[0].UUID = rs.data.uuid;
                                    ds.Billings[0].DateStamped = DateTime.Parse(rs.data.fechaTimbrado); //TFD
                                    ds.Billings[0].RfcProvCertic = rfce.InnerText; //TFD
                                    ds.Billings[0].SelloCFD = selloCFDSATe.InnerText; //TFD
                                    ds.Billings[0].CertificateNumberSat = NoCertificadoSATe.InnerText;  //TFD
                                    ds.Billings[0].SealSat = selloSATe.InnerText;    //TFD

                                }

                            }

                            LogManager.WritePacTrace("Response", rs.data.ToString());

                            //la respuesta viene en Base64
                            string decodedString = rs.data.cfdi;
                            XmlDocument document = new XmlDocument();
                            document.LoadXml(decodedString);

                            LogManager.WritePacTrace("CFDI", decodedString);


                            NameTable nt = new NameTable();
                            XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);
                            nsmgr.AddNamespace("cfdi", "http://www.sat.gob.mx/cfd/4");
                            nsmgr.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");

                            XmlElement root = document.DocumentElement;
                            XmlElement nodeTFD = (XmlElement)root.SelectSingleNode("cfdi:Complemento", nsmgr).SelectSingleNode("tfd:TimbreFiscalDigital", nsmgr);

                            XmlAttribute rfc = nodeTFD.GetAttributeNode("RfcProvCertif");
                            XmlAttribute selloCFDSAT = nodeTFD.GetAttributeNode("SelloCFD");
                            XmlAttribute selloSAT = nodeTFD.GetAttributeNode("SelloSAT");

                            XmlAttribute versionSAT = nodeTFD.GetAttributeNode("Version");
                            XmlAttribute NoCertificadoSAT = nodeTFD.GetAttributeNode("NoCertificadoSAT");

                            // DATOS EXISTENTE: 
                            // Version , Serie, Folio, Fecha, Sello, FormaPago, NoCertififcado, Certificado,

                            // DATOS UNIBILLING
                            ds.Billings[0].CertificateNumberSat = rs.data.noCertificadoSAT;
                            ds.Billings[0].Seal = rs.data.selloCFDI;

                            // DEL NODO TFD
                            ds.Billings[0].VersionSat = versionSAT.InnerText;
                            ds.Billings[0].UUID = rs.data.uuid;
                            ds.Billings[0].DateStamped = DateTime.Parse(rs.data.fechaTimbrado); //TFD
                            ds.Billings[0].RfcProvCertic = rfc.InnerText; //TFD
                            ds.Billings[0].SelloCFD = selloCFDSAT.InnerText; //TFD
                            ds.Billings[0].CertificateNumberSat = NoCertificadoSAT.InnerText;  //TFD
                            ds.Billings[0].SealSat = selloSAT.InnerText;    //TFD
                            ds.Billings[0].StatusSat = "OK";   //CL = cancelado.   , // CP  = cancelacion Pendiente.   esto es interno. 
                            ds.Billings[0].OrignalStringSat = rs.data.cadenaOriginalSAT == "" ? GenerateOrinalStringTFD(ds) : rs.data.cadenaOriginalSAT;
                            ds.Billings[0].BatchIdSat = "";
                            return true;
                        }
                        else
                        {
                            error.SetCustomError("Stamping002", "Respuesta vacia");
                            LogManager.WritePacTrace("Respuesta vacia", string.Empty);
                        }
                    }

                }
                else
                {
                    error.SetCustomError("Stamping001", "Acceso al PAC no configurado");
                    LogManager.WritePacTrace("PAC no configurado", string.Empty);

                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return false;
        }

        private static bool LoadConfiguration(int billerID, ref string url, ref string user, ref string password, ref string fileType, ref string  token,  ref bool simulate)
        {
            try
            {
                PacConfiguration admin = new PacConfiguration();
                PacConfigurationDataSet.GetByBillerIDDataTable table = admin.SelectByBillerID(billerID);
                simulate = false;
                if (table != null && table.Rows.Count > 0)
                {
                    PacConfigurationDataSet.GetByBillerIDRow dr = table[0];
                    if (dr != null)
                    {
                        url = dr.Url.Trim();
                        user = dr.Username.Trim();
                        password = Cryptography.DecryptUnivisitString(dr.Password.Trim());
                        token = dr.Token;
                        fileType = dr.FileType.Trim();
                        if (!dr.IsSimulateNull())
                            simulate = dr.Simulate;
                        return true;
                    }

                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return false;
        }

        private static void AddSecurityToken(WebServicesClientProtocol wsService, String username, String password)
        {
            SoapContext context = wsService.RequestSoapContext;
            context.Security.Tokens.Add(new UsernameToken(username, password, PasswordOption.SendPlainText));

        }

        private static string GetXmlCFDI(BillingsDataSet ds)
        {
            Billings r = new Billings();
            return r.GetXml(ds);
        }

        private static string GetXmlCFDI(BillingsDataSet ds, bool forpayment)
        {
            BillingsPayments_3 r = new BillingsPayments_3();
            return r.GetXml(ds);
        }

        /*
        private static string GetXmlOfResponce(TimbradoResponse rs)
        {
            try
            {
                if (rs != null)
                {
                    #region XML
                    string xml =
@"<TimbradoResponse>
    <xmlCFDTimbrado><![CDATA[{0}]]></xmlCFDTimbrado>
    <UUID>{1}</UUID>
    <selloSAT>{2}</selloSAT>
    <version>{3}</version>
    <noCertificadoSAT>{4}</noCertificadoSAT>
    <selloCFD>{5}</selloCFD>
    <fechaTimbrado>{6}</fechaTimbrado>
    <datosAdicionales>
        <batchId>{7}</batchId>
        <status>{8}</status>
    </datosAdicionales>
</TimbradoResponse>";
                    #endregion
                    return string.Format(xml
                            , rs.xmlCFDTimbrado != null ? rs.xmlCFDTimbrado : string.Empty
                            , rs.UUID != null ? rs.UUID : string.Empty
                            , rs.selloSAT != null ? rs.selloSAT : ""
                            , rs.version != null ? rs.version : ""
                            , rs.noCertificadoSAT != null ? rs.noCertificadoSAT : ""
                            , rs.selloCFD != null ? rs.selloCFD : ""
                            , rs.fechaTimbrado != null ? rs.fechaTimbrado.ToString("s") : ""
                            , (rs.datosAdicionales != null && rs.datosAdicionales.batchId != null) ? rs.datosAdicionales.batchId : ""
                            , (rs.datosAdicionales != null && rs.datosAdicionales.status != null) ? rs.datosAdicionales.status : "");
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return string.Empty;
        }

        private static string GetXmlOfRequest(TimbradoRequest rq)
        {
            try
            {
                if (rq != null)
                {
                    #region XML
                    string xml =
@"<TimbradoRequest>
    <xmlCFD><![CDATA[{0}]]></xmlCFD>
    <titulo>{1}</titulo>
    <conector>{2}</conector>
    <fileType>{3}</fileType>
    <comentario>{4}</comentario>
</TimbradoRequest>";
                    #endregion
                    return string.Format(xml
                            , rq.xmlCFD != null ? rq.xmlCFD : string.Empty
                            , rq.titulo != null ? rq.titulo : string.Empty
                            , rq.conector != null ? rq.conector : string.Empty
                            , rq.fileType != null ? rq.fileType : string.Empty
                            , rq.comentario != null ? rq.comentario : string.Empty);
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return string.Empty;
        }
        */

        public static string GenerateOrinalStringTFD(DataAccess.BillingsDataSet ds)
        {
            try
            {
                // ACTUALIZASDO A LA VERIOSN 1.1

                return string.Format("||{0}|{1}|{2}|{3}|{4}|{5}||"
                                      , ds.Billings[0].VersionSat
                                      , ds.Billings[0].UUID
                                      , ds.Billings[0].DateStamped.ToString("s")
                                      , ds.Billings[0].SelloCFD
                                      , ds.Billings[0].CertificateNumberSat
                                      , ds.Billings[0].SealSat
                                    );
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return string.Empty;
        }

        public static string GenerateOrinalStringTFD(DataAccess.BillingsDataSet ds, bool forpayment)
        {
            try
            {
                // ACTUALIZASDO A LA VERIOSN 1.1
                return string.Format("||{0}|{1}|{2}|{3}|{4}|{5}||"
                                      , ds.BillingsPayments_3[0].VersionSat
                                      , ds.BillingsPayments_3[0].UUID
                                      , ds.BillingsPayments_3[0].DateStamped.ToString("s")
                                      , ds.BillingsPayments_3[0].SelloCFD //?? sera CFD
                                      , ds.BillingsPayments_3[0].CertificateNumberSat
                                      , ds.Billings[0].SealSat

                                    );
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return string.Empty;
        }
    }
}