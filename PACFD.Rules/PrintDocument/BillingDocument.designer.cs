﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    partial class BillingDocument
    {
        /// <summary>
        /// 
        /// </summary>
        public class OriginalStringInfo
        {
            /// <summary>
            /// 
            /// </summary>
            public int LineWidth { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int X
            {
                get { return this.Location.X; }
                set { this.Location = new System.Drawing.Point(value, this.Location.Y); }
            }
            /// <summary>
            /// 
            /// </summary>
            public int Y
            {
                get { return this.Location.Y; }
                set { this.Location = new System.Drawing.Point(this.Location.X, value); }
            }
            /// <summary>
            /// 
            /// </summary>
            public System.Drawing.Point Location { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class LogoInfo
        {
            /// <summary>
            /// Get or Set an integer value with the image width.
            /// </summary>
            public int Width
            {
                get { return this.Size.Width; }
                set { this.Size = new System.Drawing.Size(value, this.Size.Height); }
            }
            /// <summary>
            /// Get or Set an integer value with the image height.
            /// </summary>
            public int Height
            {
                get { return this.Size.Height; }
                set { this.Size = new System.Drawing.Size(this.Size.Width, value); }
            }
            /// <summary>
            /// Get or Set the logo size.
            /// </summary>
            public System.Drawing.Size Size { get; set; }
            /// <summary>
            /// Get or Set an integer of the y coordenate of the logo.
            /// </summary>
            public int X
            {
                get { return this.Location.X; }
                set { this.Location = new System.Drawing.Point(value, this.Location.Y); }
            }
            /// <summary>
            /// Get or Set an integer of the x coordenate of the logo.
            /// </summary>
            public int Y
            {
                get { return this.Location.Y; }
                set { this.Location = new System.Drawing.Point(this.Location.X, value); }
            }
            /// <summary>
            /// 
            /// </summary>
            public System.Drawing.Point Location { get; set; }
            /// <summary>
            /// Get or set the System.Drawing.Bitmap image used to print the logo of the company.
            /// </summary>
            public System.Drawing.Bitmap Image { get; set; }
        }
        /// <summary>
        /// Class used to encapsulate the seal.
        /// </summary>
        public class SealInfo
        {
            /// <summary>
            /// Get or Set the max chars for line.
            /// </summary>
            public int LineWidth { get; set; }
            /// <summary>
            /// Get or Set an integer of the x coordenate of the seal.
            /// </summary>
            public int X
            {
                get { return this.Location.X; }
                set { this.Location = new System.Drawing.Point(value, this.Location.Y); }
            }
            /// <summary>
            /// Get or Set an integer of the y coordenate of the seal.
            /// </summary>
            public int Y
            {
                get { return this.Location.Y; }
                set { this.Location = new System.Drawing.Point(this.Location.X, value); }
            }
            /// <summary>
            /// Get or Set the location of the seal.
            /// </summary>
            public System.Drawing.Point Location { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class BillingImageInfo
        {
            /// <summary>
            /// 
            /// </summary>
            public System.Drawing.Bitmap BillingImage { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
        }
        public class BillingWaterMark
        {
            public bool Enabled { get; set; }
            public string Text { get; set; }
            public System.Drawing.Color FontColor { get; set; }
            public PdfSharp.Drawing.XFont Font { get; set; }
            public int Alpha { get; set; }

            public bool Load(System.Xml.XmlNode node)
            {
                this.Text = node.Attributes["text"].IsNull() ? "" : node.Attributes["text"].Value;

                this.Font = new PdfSharp.Drawing.XFont(
                    node.Attributes["font"].IsNull() ? "Arial" : node.Attributes["font"].Value,
                    node.Attributes["size"].IsNull() ? 28 : node.Attributes["size"].Value.ToInt32());

                this.Alpha = node.Attributes["alpha-color"].IsNull() ? 255 : node.Attributes["alpha-color"].Value.ToInt32();

                this.FontColor = node.Attributes["color"].IsNull() ?
                System.Drawing.Color.Black : node.Attributes["color"].Value.HexStringToColor();

                return (this.Enabled = true);
            }
        }
    }
}
