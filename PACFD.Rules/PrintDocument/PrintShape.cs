﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    public enum ShapeType
    {
        Line = 0,
        Rectangle,
        RoundRectangle,
    }

    public enum ZonePosition
    {
        Top = 0,
        Left = 1,
        Right = 2,
        Bottom = 3
    }

    public class PrintShape : PrintControl
    {
        public ShapeType ShapeType { get; private set; }
        public override System.Drawing.Point Position { get; set; }
        public System.Drawing.Point Position2 { get; set; }
        public float Size { get; set; }
        public System.Drawing.Color Color { get; set; }
        public System.Drawing.Color BackColor { get; set; }
        public System.Drawing.Color ZoneTitleColor { get; set; }
        public float ZoneTitleSize { get; set; }
        public ZonePosition ZoneTitlePosition { get; set; }

        public PrintShape()
        {
            this.Color = System.Drawing.Color.Black;
            this.Font = new PdfSharp.Drawing.XFont("arial", 0.1);
        }
        
        public override void Draw(PrintArguments e)
        {
            PdfSharp.Drawing.XRect ractangle;
            PdfSharp.Drawing.XPen pen;
            PdfSharp.Drawing.XGraphicsPath pathR;
            PdfSharp.Drawing.XGraphicsPath pathTitle;

            switch (this.ShapeType)
            {
                case ShapeType.Line:
                    e.Graphic.DrawLine(new PdfSharp.Drawing.XPen(this.Color, this.Size),
                        this.Position.X + e.Region.X,
                        this.Position.Y + e.Region.Y,
                        this.Position2.X + e.Region.X,
                        this.Position2.Y + e.Region.Y);
                    break;
                case ShapeType.RoundRectangle:
                    ractangle = new PdfSharp.Drawing.XRect(
                        this.Position.X + e.Region.X,
                        this.Position.Y + e.Region.Y,
                        this.Position2.X,
                        this.Position2.Y
                        );
                    pen = new PdfSharp.Drawing.XPen(this.Color, this.Size);
                    pathR = RoundedRectangle.Create((int)ractangle.X, (int)ractangle.Y, (int)ractangle.Width, (int)ractangle.Height, 10);
                    e.Graphic.DrawPath(new System.Drawing.SolidBrush(BackColor), pathR);

                    if (ZoneTitleSize > 0)
                    {
                        switch (ZoneTitlePosition)
                        {
                            case ZonePosition.Top:
                                {
                                    pathTitle = RoundedRectangle.Create((int)ractangle.X, (int)ractangle.Y, (int)ractangle.Width, (int)ZoneTitleSize, 10, RoundedRectangle.RectangleCorners.TopLeft | RoundedRectangle.RectangleCorners.TopRight);
                                    e.Graphic.DrawPath(new System.Drawing.SolidBrush(ZoneTitleColor), pathTitle);
                                }
                                break;
                            case ZonePosition.Left:
                                {
                                    pathTitle = RoundedRectangle.Create((int)ractangle.X, (int)ractangle.Y, (int)ZoneTitleSize, (int)ractangle.Height, 10, RoundedRectangle.RectangleCorners.TopLeft | RoundedRectangle.RectangleCorners.BottomLeft);
                                    e.Graphic.DrawPath(new System.Drawing.SolidBrush(ZoneTitleColor), pathTitle);
                                }
                                break;
                            case ZonePosition.Right:
                                {
                                    pathTitle = RoundedRectangle.Create(((int)((ractangle.X) + ractangle.Width - ZoneTitleSize)), (int)ractangle.Y, ((int)(ZoneTitleSize)), (int)ractangle.Height, 10, RoundedRectangle.RectangleCorners.TopRight | RoundedRectangle.RectangleCorners.BottomRight);
                                    e.Graphic.DrawPath(new System.Drawing.SolidBrush(ZoneTitleColor), pathTitle);
                                }
                                break;
                            case ZonePosition.Bottom:
                                {
                                    pathTitle = RoundedRectangle.Create((int)ractangle.X, (int)(ractangle.Y + ractangle.Height - ZoneTitleSize), (int)ractangle.Width, (int)ZoneTitleSize, 10, RoundedRectangle.RectangleCorners.BottomLeft | RoundedRectangle.RectangleCorners.BottomRight);
                                    e.Graphic.DrawPath(new System.Drawing.SolidBrush(ZoneTitleColor), pathTitle);
                                }
                                break;
                        }
                    }

                    e.Graphic.DrawPath(pen, pathR);

                    break;
                case ShapeType.Rectangle:

                    ractangle = new PdfSharp.Drawing.XRect(this.Position.X, this.Position.Y, this.Position2.X, this.Position2.Y);
                    pen = new PdfSharp.Drawing.XPen(this.Color, this.Size);

                    //PdfSharp.Drawing.XPoint[] points = new PdfSharp.Drawing.XPoint[4];
                    //e.Graphic.DrawPolygon(pen, points);

                    e.Graphic.DrawLine(pen,
                        this.Position.X + e.Region.X,
                        this.Position.Y + e.Region.Y,
                        this.Position2.X, this.Position.Y);
                    e.Graphic.DrawLine(pen,
                        this.Position.X + e.Region.X,
                        this.Position.Y + e.Region.Y,
                        this.Position.X, this.Position2.Y);
                    e.Graphic.DrawLine(pen,
                        this.Position.X + e.Region.X,
                        this.Position2.Y + e.Region.Y,
                        this.Position2.X, this.Position2.Y);
                    e.Graphic.DrawLine(pen,
                        this.Position2.X + e.Region.X,
                        this.Position2.Y + e.Region.Y,
                        this.Position2.X, this.Position.Y);
                    break;
                default:
                    break;
            }
        }

        public override int GetHeightLinesToUse(PrintArguments e)
        {
            return 0;
        }

        public override void NewPage()
        {
        }

        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            if (root.Attributes["type"].IsNull())
                return;

            base.LoadFromNode(root);

            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Position2 = new System.Drawing.Point(
                root.Attributes["width"].IsNull() ? 0 : root.Attributes["width"].Value.ToInt32(),
                root.Attributes["height"].IsNull() ? 0 : root.Attributes["height"].Value.ToInt32());

            this.Color = root.Attributes["color"].IsNull() ? System.Drawing.Color.Black
                : root.Attributes["color"].Value.HexStringToColor();

            this.Size = root.Attributes["size"].IsNull() ? 0 : root.Attributes["size"].Value.ToFloat();

            switch (root.Attributes["type"].Value.ToLower())
            {
                case "line": this.ShapeType = ShapeType.Line; break;
                case "rectangle": this.ShapeType = ShapeType.Rectangle; break;
                case "round-rectangle": this.ShapeType = ShapeType.RoundRectangle; break;
            }

            this.BackColor = root.Attributes["backcolor"].IsNull() ? System.Drawing.Color.Transparent
                : root.Attributes["backcolor"].Value.HexStringToColor();

            this.ZoneTitleColor = root.Attributes["zonetitle-color"].IsNull() ? System.Drawing.Color.Transparent
                : root.Attributes["zonetitle-color"].Value.HexStringToColor();

            this.ZoneTitleSize = root.Attributes["zonetitle-size"].IsNull() ? 0
                : root.Attributes["zonetitle-size"].Value.ToFloat();

            this.ZoneTitlePosition = ZonePosition.Top;

            if (!root.Attributes["zonetitle-position"].IsNull())
            {
                switch (root.Attributes["zonetitle-position"].Value.ToLower())
                {
                    case "top": this.ZoneTitlePosition = ZonePosition.Top; break;
                    case "bottom": this.ZoneTitlePosition = ZonePosition.Bottom; break;
                    case "left": this.ZoneTitlePosition = ZonePosition.Left; break;
                    case "right": this.ZoneTitlePosition = ZonePosition.Right; break;
                }

            }
        }
    }
}
