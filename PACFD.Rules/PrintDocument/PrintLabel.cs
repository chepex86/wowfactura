﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    public class PrintLabel : TextBaseControl
    {
        /// <summary>
        /// Get or set the data in the data base to look for.
        /// Example: "T1.R1, T2.R1, etc."
        /// </summary>
        /// <example>T1.R1, T2.R1, etc.</example>
        public string Data { get; set; }
        /// <summary>
        /// Thecnique to draw in case of a multi line string.
        /// </summary>
        public InLineTechniqueType InLineTechnique { get; set; }

        /// <summary>
        /// Draw method of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the print document.</param>
        public override void Draw(PrintArguments e)
        {
            string[] s = string.IsNullOrEmpty(this.Data) ? new string[] { } : e.GetStringData(this.Data.Split(','));
            string subtext;
            PdfSharp.Drawing.XStringFormat sformat;
            PdfSharp.Drawing.XSolidBrush brush = new PdfSharp.Drawing.XSolidBrush(this.FontColor);

            this.LinesUsed = 1;
            subtext = this.ParceText(e, s);

            if (this.Width > 0)
            {
                switch (this.InLineTechnique)
                {
                    case InLineTechniqueType.None:
                    case InLineTechniqueType.String:
                        this.DrawInLines(e, subtext);
                        return;
                    case InLineTechniqueType.Character:
                        this.DrawInLinesChar(e, subtext);
                        return;
                }
            }

            if (!this.UseAligment)
            { e.Graphic.DrawString(subtext, this.Font, brush, this.Position.X + e.Region.X, this.Position.Y + e.Region.Y); }
            else
            {
                sformat = new PdfSharp.Drawing.XStringFormat();
                sformat.Alignment = this.Aligment;
                e.Graphic.DrawString(subtext, this.Font, brush, this.Position.X + e.Region.X, this.Position.Y + e.Region.Y, sformat);
            }
        }
        /// <summary>
        /// Draw more than one line at the time, char by char.
        /// </summary>
        /// <param name="e">Print arguments send by PrintDocument or PrintLayer.</param>
        /// <param name="s">Text to be printed.</param>
        public virtual void DrawInLinesChar(PrintArguments e, string text)
        {
            PdfSharp.Drawing.XSolidBrush brush = new PdfSharp.Drawing.XSolidBrush(this.FontColor);
            char[] c;
            string[] sp;
            string aux = string.Empty;
            int x, y;

            text = text.Replace("\n", "").Replace("\r", "");
            c = text.ToCharArray();
            text = string.Empty;

            for (int i = 0; i < c.Length; i++)
            {
                if (e.Graphic.MeasureString(string.Format("{0}{1}", aux, c[i]), this.Font).Width < this.Width)
                {
                    aux += c[i];
                    continue;
                }

                text += string.Format("{0}\n", aux);
                aux = string.Empty;
                i--;
            }

            sp = (text += aux).Split('\n');
            text = null;
            y = Position.Y + e.Region.Y;
            x = Position.X + e.Region.X;

            foreach (string s in sp)
            {
                if (string.IsNullOrEmpty(s.Trim()))
                { continue; }

                e.Graphic.DrawString(s, this.Font, brush, x, y);
                y += this.Font.Height;
            }

            this.LinesUsed = sp.Length;
        }
        /// <summary>
        /// Draw more than one line at the time, word by word.
        /// </summary>
        /// <param name="e">Print arguments send by PrintDocument or PrintLayer.</param>
        /// <param name="s">Text to be printed.</param>
        public virtual void DrawInLines(PrintArguments e, string text)
        {
            PdfSharp.Drawing.XSolidBrush brush = new PdfSharp.Drawing.XSolidBrush(this.FontColor);
            string[] sp;
            string aux = string.Empty;
            int i, x, y;

            //text = text.Replace("\n", "").Replace("\r", "");
            sp = text.Split(' ');
            text = string.Empty;

            for (i = 0; i < sp.Length; i++)
            {
                if (string.IsNullOrEmpty(sp[i]))
                { continue; }

                if (e.Graphic.MeasureString(string.Format("{0} {1}", aux, sp[i]), this.Font).Width < this.Width)
                {
                    aux += (aux.Trim().Length > 0 ? " " : string.Empty) + sp[i];
                    continue;
                }
                else if (e.Graphic.MeasureString(sp[i], this.Font).Width >= this.Width)
                {
                    aux = string.Empty;

                    foreach (char c in sp[i])
                    {
                        if (e.Graphic.MeasureString(aux + c, this.Font).Width < this.Width || this.Width < 1)
                        {
                            aux += c;
                            continue;
                        }

                        text += string.Format("{0}\n", aux);
                        aux = c.ToString();
                    }

                    text += string.Format("{0}", aux);
                    aux = string.Empty;
                    continue;
                }
                else if (e.Graphic.MeasureString(string.Format("{0} {1}", aux, sp[i]), this.Font).Width > this.Width
                    && e.Graphic.MeasureString(sp[i], this.Font).Width < this.Width)
                {
                    text += string.Format("{0}\n", aux);
                    aux = string.Empty;
                    i--;
                    continue;
                }

                text += string.Format("{0}\n", aux);
                aux = string.Empty;
                i--;
            }

            text += aux;
            sp = text.Split('\n');

            /*if (e.Region.Height <= (this.Font.Height * sp.Length) + e.Region.Y)
            {
                i = (e.Region.Y + (this.Font.Height * sp.Length)) - e.Region.Height;
                i = i / this.Font.Height;
                text = string.Join("\n", sp, 0, i) + "...";
                sp = text.Split('\n');
            }*/

            text = null;
            y = Position.Y + e.Region.Y;
            x = Position.X + e.Region.X;

            foreach (string s in sp)
            {
                if (string.IsNullOrEmpty(s.Trim()))
                { continue; }

                e.Graphic.DrawString(s, this.Font, brush, x, y);
                y += this.Font.Height;
            }

            this.LinesUsed = sp.Length;
        }
        /// <summary>
        /// Get an integer value with the max height to use for all the lines.
        /// </summary>
        /// <param name="e">Print arguments send by PrintDocument or PrintLayer.</param>
        /// <returns>Integer.</returns>
        public override int GetHeightLinesToUse(PrintArguments e)
        {
            string[] sp;
            string aux = string.Empty;
            string text;
            int i;

            sp = string.IsNullOrEmpty(this.Data) ? new string[] { } : e.GetStringData(this.Data.Split(','));
            text = this.ParceText(e, sp);
            sp = null;
            //text = text.Replace("\n", string.Empty).Replace("\r", string.Empty);
            sp = text.Split(' ');
            text = string.Empty;

            for (i = 0; i < sp.Length; i++)
            {
                if (string.IsNullOrEmpty(sp[i]))
                { continue; }

                if (e.Graphic.MeasureString(string.Format("{0} {1}", aux, sp[i]), this.Font).Width < this.Width)
                {
                    aux += (aux.Trim().Length > 0 ? " " : string.Empty) + sp[i];
                    continue;
                }
                else if (e.Graphic.MeasureString(sp[i], this.Font).Width >= this.Width)
                {
                    aux = string.Empty;

                    foreach (char c in sp[i])
                    {
                        if (e.Graphic.MeasureString(aux + c, this.Font).Width < this.Width || this.Width < 1)
                        {
                            aux += c;
                            continue;
                        }

                        text += string.Format("{0}\n", aux);
                        aux = c.ToString();
                    }

                    text += string.Format("{0}", aux);
                    aux = string.Empty;
                    continue;
                }
                else if (e.Graphic.MeasureString(string.Format("{0} {1}", aux, sp[i]), this.Font).Width > this.Width
                    && e.Graphic.MeasureString(sp[i], this.Font).Width < this.Width)
                {
                    text += string.Format("{0}\n", aux);
                    aux = string.Empty;
                    i--;
                    continue;
                }

                text += string.Format("{0}\n", aux);
                aux = string.Empty;
                i--;
            }

            text += aux;
            sp = text.Split('\n');

            System.Diagnostics.Debug.WriteLine(text);

            /*if (e.Region.Height <= (this.Font.Height * sp.Length) + e.Region.Y)
            {
                i = (e.Region.Y + (this.Font.Height * sp.Length)) - e.Region.Height;
                i = i / this.Font.Height;
                text = string.Join("\n", sp, 0, i);
                sp = text.Split('\n');
                System.Diagnostics.Debug.WriteLine(text);
            }*/

            text = aux = null;

            return sp.Length;
        }
        /// <summary>
        /// New page method informer of the control.
        /// </summary>
        public override void NewPage()
        {
            //do nothing...
        }
        /// <summary>
        /// Load event of the control.
        /// </summary>
        /// <param name="root">Node with the needed information to load the control.</param>
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            base.LoadFromNode(root);

            this.Text = root.Attributes["text"].IsNull() ? null : root.Attributes["text"].Value;
            this.Data = root.Attributes["data"].IsNull() ? null : root.Attributes["data"].Value;
            this.Width = root.Attributes["width"].IsNull() ? 0 : root.Attributes["width"].Value.ToInt32();

            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Font = new PdfSharp.Drawing.XFont(
                root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());

            this.UseAligment = root.Attributes["aligment"].IsNull() ? false : true;

            this.Aligment = root.Attributes["aligment"].IsNull() ? PdfSharp.Drawing.XStringAlignment.Near
                : (PdfSharp.Drawing.XStringAlignment)Enum.Parse(typeof(PdfSharp.Drawing.XStringAlignment),
                root.Attributes["aligment"].Value);

            this.FontColor = root.Attributes["font-color"].IsNull() ?
                System.Drawing.Color.Black : root.Attributes["font-color"].Value.HexStringToColor();

            if (root.Attributes["inline-technique"].IsNull())
            { this.InLineTechnique = InLineTechniqueType.None; }
            else
            {
                switch (root.Attributes["inline-technique"].Value.ToLower())
                {
                    default:
                    case "string":
                        this.InLineTechnique = InLineTechniqueType.String;
                        break;
                    case "char":
                    case "character":
                        this.InLineTechnique = InLineTechniqueType.Character;
                        break;
                }
            }
        }

        ///// <summary>
        ///// Get a string with the text to be replaced by the data property.
        ///// </summary>
        ///// <param name="e">Event arguments send by the print document.</param>
        ///// <param name="text"></param>
        ///// <returns></returns>
        //protected override string ParceText(PrintArguments e, string[] text)
        //{
        //    //string[] s = string.IsNullOrEmpty(this.Data) ? new string[] { } : e.GetStringData(this.Data.Split(','));

        //    return base.ParceText(e, text);
        //    //return base.ParceText(e, s);
        //    //for (int i = 0; i < s.Length; i++)
        //    //{
        //    //    text = text.Replace("{" + i.ToString() + "}", s[i]);
        //    //}

        //    //return text;
        //}
    }
}
