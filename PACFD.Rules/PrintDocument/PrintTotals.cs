﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// How the taxes will print.
    /// </summary>
    public enum PrintTaxesType
    {
        Grid = 0
    }

    //public enum PrintTotalsBorderStyle
    //{
    //    None = 0,
    //    Line = 1
    //}

    /// <summary>
    /// Class used to print the taxes.
    /// </summary>
    public class PrintTotals : TextBaseControl
    {
        private List<string> textList, valueList;
        private List<PrintShape> printShapeList;

        /// <summary>
        /// Get or set a boolean value with the load state of the control.
        /// </summary>
        private bool IsLoaded { get; set; }
        /// <summary>
        ///  How the taxes will print.
        /// </summary>
        public PrintTaxesType PrintType { get; set; }
        /// <summary>
        /// Get a string value with the format of the totals.
        /// </summary>
        public string Value { get; set; }
        public int TextWidth { get; set; }
        public int ValueWidth { get; set; }
        /// <summary>
        /// Get or set a value with the max decimal used to draw the totals value.
        /// </summary>
        public int Decimals
        {
            get { return this.maxDecimals; }
            set { this.maxDecimals = value < 0 ? 0 : value; }
        }
        private int maxDecimals;
        /// <summary>
        /// Get or set the position where to print the tax values
        /// </summary>
        public System.Drawing.Point ValuePosition { get; set; }
        /// <summary>
        /// Space of the rows.
        /// </summary>
        public int RowSpace { get; set; }
        //public PrintTotalsBorderStyle BorderType { get; set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public PrintTotals()
        {
            this.textList = new List<string>();
            this.valueList = new List<string>();
            this.IsLoaded = false;
            this.NewPage();
        }
        /// <summary>
        /// Drae component to page.
        /// </summary>
        /// <param name="e"></param>
        public override void Draw(PrintArguments e)
        {
            string stmp = string.Empty;
            int iline = 0;
            PdfSharp.Drawing.XStringFormat svalueformat;
           

            // Si es un pago, usar LoadPaymentTaxes para imprimir 0's
            if (e.Billing.BillingsPayments_3.Count > 0)
                this.IsLoaded = this.IsLoaded ? true : this.LoadPaymentTaxes(e);
            else
                this.IsLoaded = this.IsLoaded ? true : this.LoadTaxes(e);

            svalueformat = new PdfSharp.Drawing.XStringFormat();
            svalueformat.Alignment = this.Aligment;

            for (int i = 0; i < this.textList.Count; i++)
            {
                stmp = this.Text.Replace("{0}", this.textList[i]);

                e.Graphic.DrawString(stmp, this.Font, PdfSharp.Drawing.XBrushes.Black,
                    this.Position.X + e.Region.X, this.Position.Y + iline + e.Region.Y);

                stmp = this.Value.Replace("{0}", this.valueList[i]);

                e.Graphic.DrawString(stmp, this.Font, PdfSharp.Drawing.XBrushes.Black,
                    this.ValuePosition.X + e.Region.X, this.ValuePosition.Y + iline + e.Region.Y, svalueformat);

                iline += this.Font.Height * (this.RowSpace > 0 ? this.RowSpace : 1);
            }
        }
        public override int GetHeightLinesToUse(PrintArguments e)
        {
            return this.textList.Count;
        }
        //public override void DrawInLines(PrintArguments e, string s)
        //{
        //throw new NotImplementedException();
        //}
        /// <summary>
        /// Load the taxes from the data set
        /// </summary>
        private Boolean LoadTaxes(PrintArguments e)
        {
            this.textList.Add("Subtotal");
            //this.valueList.Add(this.Decimals > 0 ? Math.Round(e.Billing.Billings[0].SubTotal, this.Decimals).ToString()
            //    : e.Billing.Billings[0].SubTotal.ToString());
            this.valueList.Add(e.Billing.Billings[0].SubTotal.ToString("###,###,###,###,##0.00"));

            if (e.Billing.Billings[0].Discount > 0)
            {
                this.textList.Add("Descuento");
                this.valueList.Add(e.Billing.Billings[0].Discount.ToString("###,###,###,###,##0.00"));
            }

            for (int i = 0; i < e.Billing.TransferTaxes.Count; i++)
            {
                this.textList.Add(string.Format("{0} {1}", e.Billing.TransferTaxes[i].Name, e.Billing.TransferTaxes[i].TaxRatePercentage.ToString("###.##") + "%"));//(e.Billing.TransferTaxes[i].Name);
                this.valueList.Add(e.Billing.TransferTaxes[i].Import.ToString("###,###,###,###,##0.00"));
            }

            for (int i = 0; i < e.Billing.DetainedTaxes.Count; i++)
            {
                this.textList.Add(string.Format("Ret. {0}", e.Billing.DetainedTaxes[i].Name));
                this.valueList.Add(e.Billing.DetainedTaxes[i].Import.ToString("###,###,###,###,##0.00"));
            }

            this.textList.Add("Total");
            this.valueList.Add(e.Billing.Billings[0].Total.ToString("###,###,###,###,##0.00"));

            return true;
        }
        

        private Boolean LoadPaymentTaxes(PrintArguments e)
        {
            decimal pago = 0;
            decimal total = 0;
            this.textList.Add("Subtotal");
            this.valueList.Add(pago.ToString("###,###,###,###,##0.00"));
           
            this.textList.Add("Total");
            this.valueList.Add(total.ToString("###,###,###,###,##0.00"));

            return true;
        }
        /// <summary>
        /// New page initializer.
        /// </summary>
        public override void NewPage()
        {
            //this.CanPrint = true;
            //this.NeedPrint = true;
        }
        /// <summary>
        /// Load total print control from node.
        /// </summary>
        /// <param name="root">XmlNode used to load the control.</param>
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            PrintControl printc;

            if (root.IsNull())
                return;

            base.LoadFromNode(root);

            this.Text = root.Attributes["text"].IsNull() ? null : root.Attributes["text"].Value;
            this.Value = root.Attributes["value"].IsNull() ? null : root.Attributes["value"].Value;

            try
            {
                this.PrintType = root.Attributes["type"].IsNull() ?
                    PrintTaxesType.Grid : (PrintTaxesType)Enum.Parse(typeof(PrintTaxesType), root.Attributes["type"].Value);
            }
            catch (Exception ex)
            {
                this.PrintType = PrintTaxesType.Grid;
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            this.Font = new PdfSharp.Drawing.XFont(
                root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());


            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.ValuePosition = new System.Drawing.Point(
                root.Attributes["x2"].IsNull() ? 0 : root.Attributes["x2"].Value.ToInt32(),
                root.Attributes["y2"].IsNull() ? 0 : root.Attributes["y2"].Value.ToInt32());

            this.TextWidth = root.Attributes["text-width"].IsNull() ? 0 : root.Attributes["text-width"].Value.ToInt32();
            this.ValueWidth = root.Attributes["value-width"].IsNull() ? 0 : root.Attributes["value-width"].Value.ToInt32();
            this.Decimals = root.Attributes["decimals"].IsNull() ? 0 : root.Attributes["decimals"].Value.ToInt32();

            this.RowSpace = root.Attributes["row-space"].IsNull() ? 0 : root.Attributes["row-space"].Value.ToInt32();

            this.Aligment = root.Attributes["value-aligment"].IsNull() ? PdfSharp.Drawing.XStringAlignment.Near
                : (PdfSharp.Drawing.XStringAlignment)Enum.Parse(typeof(PdfSharp.Drawing.XStringAlignment),
                root.Attributes["value-aligment"].Value);

            //this.BorderType = root.Attributes["board-type"].IsNull() ? PrintTotalsBorderStyle.None
            //    : (PrintTotalsBorderStyle)Enum.Parse(typeof(PrintTotalsBorderStyle), root.Attributes["board-type"].Value);

            this.printShapeList = new List<PrintShape>();

            foreach (System.Xml.XmlNode item in root.ChildNodes)
            {
                if (item.Name.ToLower() == typeof(PrintShape).Name.ToLower())
                {
                    printc = new PrintShape();
                    printc.LoadFromNode(item);
                    this.printShapeList.Add(printc as PrintShape);
                }
            }
        }
    }
}
