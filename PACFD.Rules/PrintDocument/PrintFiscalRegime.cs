﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument
{
    public class PrintFiscalRegime : TextBaseControl
    {
        private List<string> fiscallist = new List<string>();
        public int TextWidth { get; set; }
        [System.ComponentModel.Browsable(false), System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public override string Text { get { return string.Empty; } set { } }
        /// <summary>
        /// Space between rows.
        /// </summary>
        public int RowSpace { get; set; }

        public override void Draw(PrintArguments e)
        {
            int iline = 0;
            PdfSharp.Drawing.XStringFormat svalueformat;

            if (e.Billing.BillingFiscalRegime.Count > 0 && fiscallist.Count < 1)
                foreach (var item in e.Billing.BillingFiscalRegime)
                    fiscallist.Add(item.Regime);

            svalueformat = new PdfSharp.Drawing.XStringFormat();

            for (int i = 0; i < this.fiscallist.Count; i++)
            {
                e.Graphic.DrawString(this.fiscallist[i], this.Font, PdfSharp.Drawing.XBrushes.Black,
                    this.Position.X + e.Region.X, this.Position.Y + iline + e.Region.Y);

                iline += this.Font.Height * (this.RowSpace > 0 ? this.RowSpace : 1);
            }
        }
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            base.LoadFromNode(root);

            //this.Text = root.Attributes["text"].IsNull() ? null : root.Attributes["text"].Value;
            this.Font = new PdfSharp.Drawing.XFont(
               root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
               root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());
            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());
            //this.TextWidth = root.Attributes["text-width"].IsNull() ? 0 : root.Attributes["text-width"].Value.ToInt32();
            this.RowSpace = root.Attributes["row-space"].IsNull() ? 0 : root.Attributes["row-space"].Value.ToInt32();
        }

        public override void NewPage()
        {
            //nothing..
        }
    }
}
