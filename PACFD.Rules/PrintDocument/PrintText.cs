﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    public class PrintText : PrintLabel
    {
        public bool Bold { get; set; }
        

        public override void NewPage()
        {
            //do nothing...
        }
        public override void Draw(PrintArguments e)
        {
            PdfSharp.Drawing.XSolidBrush brush = new PdfSharp.Drawing.XSolidBrush(this.FontColor);

            if (this.Width > 0)
            {
                this.DrawInLines(e, this.Text);
                return;
            }
            
            e.Graphic.DrawString(this.Text, this.Font, brush, this.Position.X + e.Region.X, this.Position.Y + e.Region.Y);
            /*PdfSharp.Drawing.Layout.XTextFormatter tf = new PdfSharp.Drawing.Layout.XTextFormatter(e.Graphic);
            PdfSharp.Drawing.
            e.Graphic.DrawRectangle(
            PdfSharp.Drawing.Layout.XTextFormatter
            tf.DrawString(this.Text,this.Font,brush,rect);*/
        }
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            base.LoadFromNode(root);

            this.Text = root.Attributes["text"].IsNull() ? null : root.Attributes["text"].Value;
            this.Width = root.Attributes["width"].IsNull() ? 0 : root.Attributes["width"].Value.ToInt32();

            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Bold = root.Attributes["bold"].IsNull() ? false : root.Attributes["font"].Value.ToBoolean();

            if (!this.Bold)
                this.Font = new PdfSharp.Drawing.XFont(
                    root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                    root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());
            else
                this.Font = new PdfSharp.Drawing.XFont(
                root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32(),
                PdfSharp.Drawing.XFontStyle.Bold);

            this.FontColor = root.Attributes["font-color"].IsNull() ?
                System.Drawing.Color.Black : root.Attributes["font-color"].Value.HexStringToColor();
        }
    }
}