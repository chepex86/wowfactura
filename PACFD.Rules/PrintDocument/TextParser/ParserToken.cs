﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument.TextParser
{
    /// <summary>
    /// Helper Token(methods) class that modify the text.
    /// </summary>
    public class ParserToken
    {
        /// <summary>
        /// Token ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Method to be execute for this token.
        /// </summary>
        public List<string> Methods { get; private set; }
        /// <summary>
        /// Inner text of the string.
        /// </summary>
        public string InnerText { get; set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public ParserToken()
        {
            this.Methods = new List<string>();
        }
    }
}
