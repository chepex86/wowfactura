﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument.TextParser
{
    public class TokensArgs : EventArgs
    {
        public string[] Data { get; private set; }
        public List<ParserToken> Tokens { get; private set; }
        public string Text { get; private set; }

        public TokensArgs(List<ParserToken> tokens, string[] data, string text)
        {
            this.Data = data;
            this.Tokens = tokens;
            this.Text = text;
        }
    }
}
