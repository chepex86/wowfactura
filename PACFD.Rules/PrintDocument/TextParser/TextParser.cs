﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument.TextParser
{
    public delegate void TextBeforeBiuldEventHandler(object sender, TokensArgs e);

    public sealed class TextParser
    {
        public event TextBeforeBiuldEventHandler TextBeforeBuild;
        private List<string> tokenStringList = new List<string>();

        /// <summary>
        /// Get a string parsed. THIS METHOD DO NOT ACCEPT NULL DATA's VARIABLES. Example: 
        /// "texto ejemplo de {0:'{0} variable {0}',up,notempty } personal en impreción. Esto es un breicier \{ esto una diagonal \", "mydata".
        /// Where "{0}" in '{0} variable {0}' will be  substituted for "mydata".
        /// </summary>
        /// <param name="text">Text to be parsed.</param>
        /// <param name="data">Data used as subtitution in te parser.</param>
        /// <remarks>THIS METHOD DO NOT ACCEPT NULL DATA's VARIABLES.</remarks>
        /// <returns></returns>
        public string Parse(string text, string[] data)
        {
            if (data.IsNull())
                return text;

            //---------------------------------------------------------------------
            //                  NOTA IMPORTANTE
            //No se aceptan cadenas datos nullos, envialos vacios (String.empty)
            //Do not accept null data strings, send it empty instead (String.Empty)
            //
            //---------------------------------------------------------------------

            foreach (var item in data)
                if (item == null)
                    return text;

            //first we get the tokens and plain text to write
            string parsedText = this.InitialParse(text);
            List<ParserToken> l = this.CreateTokens();
            ParserToken t;
            string s = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                t = l[i];

                if (t.ID > data.Length - 1)
                    continue;

                s = data[t.ID];

                for (int i2 = 0; i2 < t.Methods.Count; i2++)
                {
                    if (t.Methods[i2] == Tokens.Up && s != null)
                        s = s.ToUpper();
                    else if (t.Methods[i2] == Tokens.Low && s != null)
                        s = s.ToLower();
                    else if (t.Methods[i2] == Tokens.NotEmpty)
                        continue;
                    else
                        s = Tokens.ParseSpecialToken(t.Methods[i2], s);
                }

                if (!string.IsNullOrEmpty(t.InnerText))
                    t.InnerText = t.InnerText.Replace("{0}", s);
                else
                    t.InnerText = string.IsNullOrEmpty(s) ? "" : t.Methods.Contains(Tokens.NotEmpty) ? string.Empty : s;
            }

            if (this.TextBeforeBuild != null)
                this.TextBeforeBuild(this, new TokensArgs(l, data, text));

            for (int i = 0; i < l.Count; i++)
            {
                string tmp = 
                    s = string.Empty;
                t = l[i];
                tmp = t.ID > data.Length - 1 ? string.Empty
                    : data[t.ID].Trim().Length < 1 ? string.Empty
                    : t.Methods.Contains(Tokens.NotEmpty) && data[t.ID].Trim().Length < 1 ? string.Empty
                    : t.InnerText;

                if (!string.IsNullOrEmpty(tmp))
                    s = tmp;

                parsedText = parsedText.Replace("{" + i + "}", s);
            }

            return parsedText;
        }
        /// <summary>
        /// Pre parse the string.
        /// </summary>
        /// <param name="originaltext">Text to be parse.</param>
        private string InitialParse(string originaltext)
        {
            string text = string.Empty;
            string subtext = string.Empty;
            char c;
            bool ischain = false;
            bool isbar = false;
            int index = -1;

            for (int i = 0; i < originaltext.Length; i++)
            {
                c = originaltext[i];

                if (c == '\\' && !isbar)
                {
                    isbar = true;
                    continue;
                }
                else if (c == '\\' && isbar)
                {
                    text += c;
                    isbar = false;
                    continue;
                }

                if (c == '{' && !isbar)
                {
                    index++;
                    subtext = c.ToString();

                    while ((c != '}' || ischain) && i++ < originaltext.Length)
                    {
                        c = originaltext[i];
                        subtext += c.ToString();

                        if (c == "'"[0] && !ischain)
                            ischain = true;
                        else if (c == "'"[0] && ischain)
                            ischain = false;
                    }

                    if (c == '}')
                    {
                        this.tokenStringList.Add(subtext);
                        text += string.Format("{{{0}}}", index);
                        continue;
                    }

                    text += subtext;
                    continue;
                }

                text += c;
            }

            return text;
        }
        /// <summary>
        /// Create tokens/methods that will modify the text.
        /// </summary>
        /// <returns></returns>
        private List<ParserToken> CreateTokens()
        {
            char c;
            List<ParserToken> tokens = new List<ParserToken>();
            string subtext = string.Empty;
            ParserToken tok;
            int id = 0;
            bool success = false;

            foreach (string item in this.tokenStringList)
            {
                success = true;
                tok = new ParserToken();

                for (int i = 0; i < item.Length; i++)
                {
                    c = item[i];

                    if (c == "'"[0])
                    {
                        c = ' ';
                        subtext = string.Empty;

                        while (c != "'"[0] && i++ < item.Length)
                        {
                            c = item[i];

                            if (c != "'"[0])
                                subtext += c.ToString();
                        }

                        tok.InnerText = subtext;
                    }
                    else if (c == '{')
                    {
                        c = ' ';

                        while (c == ' ' && i++ < item.Length)
                        { c = item[i]; }

                        if (!int.TryParse(c.ToString(), out id))
                        {
                            success = false;
                            break;
                        }

                        tok.ID = id;
                    }
                    else if (c == ',')
                    {
                        subtext = string.Empty;

                        while (c != '}' && i++ < item.Length)
                        {
                            c = item[i];

                            if (c == ',')
                            {
                                if (subtext.Length > 0)
                                    tok.Methods.Add(subtext.Trim());

                                subtext = string.Empty;
                                continue;
                            }

                            if (c == '}')
                                continue;

                            subtext += c.ToString();
                        }

                        if (subtext.Length > 0)
                            tok.Methods.Add(subtext.Trim());
                    }
                }

                if (success)
                    tokens.Add(tok);
            }

            return tokens;
        }
    }
}
