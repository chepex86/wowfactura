﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument.TextParser
{
    public static class Tokens
    {
        public const string Up = "up";
        public const string Low = "low";
        public const string NotEmpty = "notempty";
        public const string OnEmpty = "onempty";
        public const string Add = "add";
        public const string AddOnEmpty = "addonempty";
        const string Date = "date";

        public static string[] Default { get { return new string[] { Up, Low, NotEmpty, OnEmpty, Add, AddOnEmpty }; } }

        internal static string ParseSpecialToken(string token, string text)
        {
            DateTime date;
            string[] split = token.Split("'"[0]);
            string s = string.Empty;

            if (split.Length < 1)
                return text;

            s = text; //send defalt text return

            switch (split[0])
            {
                case Tokens.Date:

                    date = DateTime.Now;

                    if (split.Length > 1 && !DateTime.TryParse(text, out date))
                        return text;

                    s = date.ToString(split[1]);
                    break;
                case Tokens.OnEmpty:
                    if (split.Length > 1 && string.IsNullOrEmpty(text))
                        s = split[1];
                    break;
                case Tokens.Add:
                    if (split.Length > 1)
                        s = text + split[1];
                    break;
                case Tokens.AddOnEmpty:
                    if (split.Length > 1 && string.IsNullOrEmpty(text))
                        s = text + split[1];
                    break;
            }

            return s;
        }
    }
}
