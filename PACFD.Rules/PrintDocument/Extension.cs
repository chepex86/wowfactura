﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument
{
    public enum InLineTechniqueType
    {
        None = 0,
        String,
        Character,
    }

    internal enum QrCBBDataType : int
    {
        RFC = 0,
        ApprovalNumber = 1,
        FolioStart = 2,
        FolioEnd = 3,
        Serial = 4,
        ExpeditionDate = 5,
        ExpirationDate = 6,
    }
    /// <summary>
    /// Class extencion for the namespace PACFD.Rules.PrintDocument.
    /// </summary>
    internal static class Extension
    {
        /// <summary>
        /// Get a boolean value indicating if the object is null or not.
        /// </summary>
        /// <param name="value">Object to be evaluated.</param>
        /// <returns>If null return true else false.</returns>
        public static bool IsNull(this object value) { return value == null ? true : false; }
        public static Int32 ToInt32(this string value)
        {
            int rel = 0;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!int.TryParse(value, out rel))
                return 0;

            return rel;
        }
        public static float ToFloat(this string value)
        {
            float rel = 0;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!float.TryParse(value, out rel))
                return 0;

            return rel;
        }
        public static decimal ToDecimal(this string value)
        {
            decimal rel = 0;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!decimal.TryParse(value, out rel))
                return 0;

            return rel;
        }
        public static Boolean ToBoolean(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            switch (value.Trim().ToLower())
            {
                case "true":
                case "yes":
                case "si":
                case "1":
                    return true;
                case "false":
                case "0":
                case "not":
                case "no":
                default:
                    return false;
            }
        }
        public static System.Drawing.Color HexStringToColor(this string hex)
        {
            hex = hex.Replace("#", "");

            if (hex.Length != 6)
                throw new Exception(hex +
                    " is not a valid 6-place hexadecimal color code.");

            string r, g, b;

            r = hex.Substring(0, 2);
            g = hex.Substring(2, 2);
            b = hex.Substring(4, 2);

            return System.Drawing.Color.FromArgb(HexStringToBase10Int(r),
                                                 HexStringToBase10Int(g),
                                                 HexStringToBase10Int(b));

        }
        /// <summary>
        /// Get a string information contained in the Qr image that is in a PACFD.DataAccess.BillingsDataSet.BillingsRow.
        /// </summary>
        /// <param name="value">Value to be search.</param>
        /// <param name="data">Information to look for.</param>
        /// <returns>If success return a string else return empty.</returns>
        public static string GetQrData(this PACFD.DataAccess.BillingsDataSet.BillingsRow value, QrCBBDataType data)
        {
            System.Drawing.Graphics g;
            System.Drawing.Bitmap bitmap, clone;
            PACFD.Rules.QRCodeLib.QRCodeDecoder qr;
            string s;
            string[] split;

            if (value == null)
                return string.Empty;

            if (value.IsQrImageNull())
                return string.Empty;

            if (value.QrImage.Length < 10)
                return string.Empty;

            using (bitmap = System.Drawing.Bitmap.FromStream(new System.IO.MemoryStream(value.QrImage)) as System.Drawing.Bitmap)
            {
                if (bitmap.IsNull())
                    return string.Empty;

                using (clone = new System.Drawing.Bitmap(250, 250))
                {
                    g = System.Drawing.Graphics.FromImage(clone);
                    g.Clear(System.Drawing.Color.Transparent);
                    g.DrawImage(bitmap, 0, 0, 250, 250);
                    qr = new PACFD.Rules.QRCodeLib.QRCodeDecoder();

                    try
                    {
                        s = qr.decode(new PACFD.Rules.QRCodeLib.data.QRCodeBitmapImage(clone));
                    }
                    catch
                    {
                        return string.Empty;
                    }
                }
            }

            if (string.IsNullOrEmpty(s))
                return string.Empty;

            if (s.Length < 1)
                return string.Empty;

            split = s.Split('|');

            if (split.Length < 6)
                return string.Empty;

            return split[((int)data) + 1];
        }

        public static string ToLetters(this decimal value, PACFD.Common.CurrencyType currency)
        {
            PACFD.Common.LetterConverter l = new PACFD.Common.LetterConverter();

            return l.NumberToLetter(value, currency);
        }
        public static int HexStringToBase10Int(string hex)
        {
            int base10value = 0;

            try { base10value = System.Convert.ToInt32(hex, 16); }
            catch { base10value = 0; }

            return base10value;
        }
        public static System.IO.Stream ToStream(this byte[] value)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();

            if (value.Length < 1)
                return stream;

            stream.Write(value, 0, value.Length);
            stream.Seek(0, System.IO.SeekOrigin.Begin);

            return stream;
        }
        public static System.Drawing.Bitmap ToBitmap(this System.IO.Stream stream)
        {
            System.Drawing.Bitmap b = null;

            stream.Seek(0, System.IO.SeekOrigin.Begin);

            try
            {
                b = new System.Drawing.Bitmap(stream);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return b;
        }
    }
}