﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PACFD.Common;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Class as container for the XML data base definition.
    /// </summary>
    public sealed class DataBaseDefinition
    {
        public List<Table> tableList;
        public List<Row> rowList;

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public DataBaseDefinition()
        {
            this.rowList = new List<Row>();
            this.tableList = new List<Table>();
        }
        /// <summary>
        /// Get a string array with the columname and rows values.
        /// </summary>
        /// <param name="table">Table ID name. Default format (TN) example: T1</param>
        /// <param name="row">Row ID name. Default format (RN) exemple: R1</param>
        /// <returns>If success return an array of string else null.</returns>
        public string[] GetColumn(string table, string row)
        {
            for (int i = 0; i < this.tableList.Count; i++)
            {
                if (this.tableList[i].ID == table)
                    if (this.rowList[i].ID == row)
                        return new string[] { this.tableList[i].Name, this.rowList[i].Name };
            }

            return null;
        }
        /// <summary>
        /// Load the data base definition. Default locaiton "Includes\DBDefinition.xml" in the main exe that cerated
        /// the instance of the class.
        /// </summary>
        public void LoadXML()
        {
            Table t;
            Row r;
            System.Xml.XmlNode root, n, n2;
            System.Xml.XmlDocument doc;// = new System.Xml.XmlDocument();
            //            System.IO.MemoryStream stream;
            //            byte[] xmlarray;
            //            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();

            //            using (stream = new System.IO.MemoryStream())
            //            {
            //                xmlarray = encoding.GetBytes(Properties.Resources.DBDefinition);
            //                encoding = null;
            //                stream.Write(xmlarray, 0, xmlarray.Length);
            //                stream.Seek(0, System.IO.SeekOrigin.Begin);

            //                try
            //                {
            //                    //doc.Load(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"Includes\DBDefinition.xml");                
            //                    doc.Load(stream);
            //                }
            //                catch (Exception ex)
            //                {
            //                    LogManager.WriteError(ex);
            //#if DEBUG
            //                    System.Diagnostics.Debug.WriteLine("[ERROR - Reading DBDefinition] " + ex.Message);
            //#endif
            //                    return;
            //                }
            //            }

            doc = PACFD.Rules.PrintTemplates.GetDBDefinition();

            root = doc.DocumentElement;

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                n = root.ChildNodes[i];//.SelectSingleNode("table");

                if (n.IsNull())
                    continue;

                if (n.Name.ToLower() != "table")
                    continue;

                t = new Table(n.Attributes["name"].IsNull() ? null : n.Attributes["name"].Value.Trim(),
                    n.Attributes["id"].IsNull() ? null : n.Attributes["id"].Value.Trim());

                if (t.IsNull())
                    continue;

                for (int y = 0; y < n.ChildNodes.Count; y++)
                {
                    n2 = n.ChildNodes[y];

                    if (n2.NodeType != System.Xml.XmlNodeType.Element)
                        continue;                    

                    r = new Row(n2.Attributes["name"].IsNull() ? null : n2.Attributes["name"].Value.Trim(),
                        n2.Attributes["id"].IsNull() ? null : n2.Attributes["id"].Value.Trim());

                    this.tableList.Add(t);
                    this.rowList.Add(r);
                    //this.DataBaseTable.Add(t, r);
                }
            }
        }


        public sealed class Table
        {
            /// <summary>
            /// Get the table name.
            /// </summary>
            public string Name { get; private set; }
            /// <summary>
            /// Get the table ID.
            /// </summary>
            public string ID { get; private set; }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="id"></param>
            internal Table(string name, string id)
            {
                this.Name = name;
                this.ID = id;
            }
        }

        public sealed class Row
        {
            /// <summary>
            /// Get the row name.
            /// </summary>
            public string Name { get; private set; }
            /// <summary>
            /// Get the row ID.
            /// </summary>
            public string ID { get; private set; }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="id"></param>
            internal Row(string name, string id)
            {
                this.Name = name;
                this.ID = id;
            }
        }
    }
}
