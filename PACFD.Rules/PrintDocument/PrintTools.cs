﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PACFD.Common;

namespace PACFD.Rules.PrintDocument
{
    public class PrintTools : TextBaseControl
    {
        public string Data { get; set; }
        public string Method { get; set; }


        public override void Draw(PrintArguments e)
        {
            bool draw = false;
            string[] s = null, saxu = null;
            string subtext = this.Text;
            string curr = string.Empty;
            List<string> sresult = new List<string>();

            if (string.IsNullOrEmpty(this.Method))
                return;

            foreach (string m in this.Method.Split(','))
            {
                switch (m.ToLower().Trim())
                {
                    case "decimaltoletters":
                        s = e.GetStringData(string.IsNullOrEmpty(this.Data) ? new string[] { } : this.Data.Split(','));

                        if (s.Length < 1)
                        {
                            LogManager.WriteWarning(new Exception("PACFD.Rules.PrintDocument.PrintTools.Draw(PrintArguments e) - Data Base can be null."));
                            return;
                        }

                        saxu = e.GetStringData(new string[] { "T1.C1" }); //   Includes/DBDefinition.xml

                        if (saxu.Length < 1)
                        {
                            LogManager.WriteWarning(new Exception("PACFD.Rules.PrintDocument.PrintTools.Draw(PrintArguments e) - Currency type is empty."));
                            return;
                        }

                        curr = saxu[0];

                        s = new string[] { s[0].ToDecimal().ToLetters(curr.ToLower() == "mxn" ? CurrencyType.MXN : CurrencyType.USD) };
                        sresult.AddRange(s);
                        draw = true;
                        break;
                    case "qr-rfc":
                    case "qr-numeroaprovacion":
                    case "qr-folio-inicio":
                    case "qr-folio-fin":
                    case "qr-serie":
                    case "qr-fecha-expedicion":
                    case "qr-fecha-expiracion":
                        s = m.ToLower().Trim().Split('-');
                        saxu = new string[] { "" };

                        if (s[1].ToLower() == "rfc")
                            saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.RFC);
                        else if (s[1].ToLower() == "numeroaprovacion")
                            saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.ApprovalNumber);
                        else if (s[1].ToLower() == "folio")
                        {
                            if (s[2].ToLower() == "inicio")
                                saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.FolioStart);
                            else if (s[2].ToLower() == "fin")
                                saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.FolioEnd);
                        }
                        else if (s[1].ToLower() == "serie")
                            saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.Serial);
                        else if (s[1].ToLower() == "fecha")
                        {
                            if (s[2].ToLower() == "expedicion")
                                saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.ExpeditionDate);
                            else if (s[2].ToLower() == "expiracion")
                                saxu[0] = e.Billing.Billings[0].GetQrData(QrCBBDataType.ExpirationDate);
                        }

                        s = new string[] { saxu[0] };
                        sresult.AddRange(s);
                        draw = true;

                        break;
                }
            }
            
            s = null;

            if (draw && sresult.Count > 0)
            {
                s = sresult.ToArray();

                for (int i = 0; i < s.Length; i++)
                    subtext = subtext.Replace("{" + i.ToString() + "}", s[i]);

                if (this.Width > 0)
                {
                    this.DrawInLines(e, subtext);
                    return;
                }

                e.Graphic.DrawString(subtext, this.Font, PdfSharp.Drawing.XBrushes.Black,
                    this.Position.X + e.Region.X, this.Position.Y + e.Region.Y);
            }
        }

        public void DrawInLines(PrintArguments e, string s)
        {
            if (s.Length > 0)
                s = s.Replace("\n", "").Replace("\r", "");

            char[] c = s.ToCharArray();
            string auxL = string.Empty;
            string auxS = string.Empty;

            for (int j = 0; j < c.Length; j++)
            {
                if (e.Graphic.MeasureString(string.Format("{0}{1}", auxL, c[j].ToString()), this.Font).Width < this.Width)
                {
                    auxL += c[j].ToString();
                }
                else
                {
                    auxS += string.Format("{0}\n", auxL);
                    auxL = string.Empty;
                    j--;
                }
            }

            s = string.Format("{0}{1}", auxS, auxL);

            int i = 0;
            int al = this.Font.Height;
            int y = Position.Y + e.Region.Y;
            int x = Position.X + e.Region.X;

            foreach (string l in s.Split('\n'))
            {
                if (l.Trim() != string.Empty)
                {
                    if (i > 0)
                    {
                        this.LinesUsed++;
                        y += al;
                    }
                    e.Graphic.DrawString(l, this.Font, PdfSharp.Drawing.XBrushes.Black, x, y);
                    i++;

                }
            }
        }

        public override int GetHeightLinesToUse(PrintArguments e)
        {
            return 0;
        }

        public override void NewPage()
        {
            //do nothing...
        }
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            base.LoadFromNode(root);

            this.Text = root.Attributes["text"].IsNull() ? string.Empty : root.Attributes["text"].Value;
            this.Data = root.Attributes["data"].IsNull() ? null : root.Attributes["data"].Value;
            this.Method = root.Attributes["method"].IsNull() ? null : root.Attributes["method"].Value;
            this.Width = root.Attributes["width"].IsNull() ? 0 : root.Attributes["width"].Value.ToInt32();

            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Font = new PdfSharp.Drawing.XFont(
                root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());
        }
    }
}
