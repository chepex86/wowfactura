﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PACFD.Common;

namespace PACFD.Rules.PrintDocument
{
    public delegate void BillingDocumentBeforePrintEventHandler(BillingDocumentBeforePrintEventArgs e);


    /// <summary>
    /// Create a PDF document based on a billing id.
    /// </summary>
    public partial class BillingDocument
    {
        private PACFD.DataAccess.BillingsDataSet dataSet;
        private DataBaseDefinition dataBaseDefinition;
        private int pageCount = 0;
        private const string PREVIEW = "     PREVIEW     ";
        private const string CANCELED = "     CANCELADA     ";
        private const string PREBILLING = "     PRECOMPROBANTE     ";

        public event EventHandler BeforePrint;
        public event EventHandler BeforeWaterMakPrint;

        /// <summary>
        /// Indicate if the document is a Demo document.
        /// </summary>
        public bool isDemoDoc { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public System.Drawing.Point Size { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OriginalStringInfo OriginalString { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public LogoInfo Logo { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public SealInfo Seal { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public BillingImageInfo Image { get; private set; }
        //public LayerRow RowStyle { get; set; }
        /// <summary>
        /// Layers collection.
        /// </summary>
        public PrintLayerCollection Layers { get; private set; }
        /// <summary>
        /// Print foot data; number of pages, document copy right etc..
        /// </summary>
        public PrintFoot Foot { get; private set; }

        public BillingWaterMark WaterMark { get; private set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public BillingDocument()
        {
            this.OriginalString = new OriginalStringInfo();
            this.Logo = new LogoInfo();
            this.Seal = new SealInfo();
            this.Image = new BillingImageInfo();
            this.Layers = new PrintLayerCollection();

            this.dataBaseDefinition = new DataBaseDefinition();
            this.dataBaseDefinition.LoadXML();
            this.Foot = new PrintFoot();
            this.WaterMark = new BillingWaterMark()
            {
                Enabled = false,
                Text = string.Empty,
                Alpha = 255,
                FontColor = System.Drawing.Color.Black,
                Font = new PdfSharp.Drawing.XFont("Arial", 28),
            };
        }
        /// <summary>
        /// Load a xml documnet for use in a billing print.
        /// </summary>
        public void LoadFromXML(System.IO.Stream printdocument)
        {
            int icount;
            System.Drawing.Bitmap bitmap;
            System.Xml.XmlNode node, n;
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Drawing.Graphics g;

            printdocument.Seek(0, System.IO.SeekOrigin.Begin);

            try
            {
                //doc.Load(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"Billings\XML\Default.xml");
                doc.Load(printdocument);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return;
            }

            node = doc.DocumentElement;

            try
            {
                this.Name = node.SelectSingleNode("name").Attributes["id"].Value;

                this.Size = new System.Drawing.Point(node.SelectSingleNode("size").Attributes["width"].Value.ToInt32(),
                    node.SelectSingleNode("size").Attributes["height"].Value.ToInt32());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return;
            }

            if (!node.SelectSingleNode("pagefoot").IsNull())
                this.Foot.LoadFromXML(node.SelectSingleNode("pagefoot"));

            if (!node.SelectSingleNode("logo").IsNull())
            {
                try
                {
                    this.Logo.X = node.SelectSingleNode("logo").Attributes["x"].Value.ToInt32();
                    this.Logo.Y = node.SelectSingleNode("logo").Attributes["y"].Value.ToInt32();
                    this.Logo.Width = node.SelectSingleNode("logo").Attributes["width"].Value.ToInt32();
                    this.Logo.Height = node.SelectSingleNode("logo").Attributes["height"].Value.ToInt32();
                }
                catch (Exception ex)
                {
                    this.Logo.X = 0;
                    this.Logo.Y = 0;
                    this.Logo.Width = 1;
                    this.Logo.Height = 1;

                    LogManager.WriteError(ex);
                    this.Logo.Image = null;
                }
            }

            if (!(n = node.SelectSingleNode("watermark")).IsNull())
            {
                this.WaterMark.Load(n);
                n = null;
            }

            bitmap = null;

            if (!(n = node.SelectSingleNode("billingimage")).IsNull())
            {
                this.Image.Height = n.Attributes["height"].IsNull() ? 10 : n.Attributes["height"].Value.ToInt32();
                this.Image.Width = n.Attributes["width"].IsNull() ? 10 : n.Attributes["width"].Value.ToInt32();
                n = null;
            }

            if (this.Image.Height < 1)
                this.Image.Height = 10;

            if (this.Image.Width < 1)
                this.Image.Width = 10;

            if (!(n = node.SelectSingleNode("billingimage")).IsNull())
            {
                try
                {
                    if (!n.Attributes["url"].IsNull())
                        bitmap = new System.Drawing.Bitmap(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + n.Attributes["url"].Value);
                }
                catch (Exception ex)
                {
                    bitmap = null;
                    LogManager.WriteWarning(new Exception("Couldn't load billing image file."));
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                finally
                {
                    n = null;
                }
            }

            if (bitmap.IsNull())
            {
                bitmap = new System.Drawing.Bitmap(10, 10);

                using (g = System.Drawing.Graphics.FromImage(bitmap))
                    g.Clear(System.Drawing.Color.Transparent);
            }

            this.Image.BillingImage = bitmap;

            try
            {
                icount = node.ChildNodes.Count;
                //node.SelectSingleNode(typeof(PrintLayer).Name.ToLower()).ChildNodes.Count;
            }
            catch { icount = 0; }

            PrintLayer layer;

            for (int i = 0; i < icount; i++)
            {
                try
                {
                    n = node.ChildNodes[i];

                    if (n.IsNull())
                        continue;

                    if (n.Name.ToLower() != typeof(PrintLayer).Name.ToLower())
                        continue;

                    layer = new PrintLayer();
                    layer.LoadFromNode(n);
                    this.Layers.Add(layer);
                    layer = null;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Print the document.
        /// </summary>
        /// <param name="data"></param>
        public BillingDocumentPrintEventArgs Print(int billingid, int? billingPaymentId = 0)
        {
            BillingDocumentPrintEventArgs e = null;
            System.IO.MemoryStream stream;
            PdfSharp.Pdf.PdfDocument document;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;
            PACFD.DataAccess.BillingsDataSet dsTest = null;

            if (billingPaymentId == null || billingPaymentId == 0)
                this.dataSet = (new PACFD.Rules.Billings()).GetFullBilling(billingid);
            else
            {
                this.dataSet = (new PACFD.Rules.BillingsPayments_3()).GetFullBilling(billingPaymentId.Value);
                dsTest = (new PACFD.Rules.Billings()).GetFullBilling(this.dataSet.Billings[0].BillingID);
            }

            this.isDemoDoc = false;
                // test

            if (this.dataSet.IsNull())
                return e;

            if (this.dataSet.Billings.Count < 1)
                return e;

            billertable = (new PACFD.Rules.Billers()).SelectByID(this.dataSet.Billings[0].BillerID);

            if (billertable.Count > 0)
            {
                if (!billertable[0].IsLogoCFDNull())
                    this.Logo.Image = billertable[0].LogoCFD.ToStream().ToBitmap();
            }

            if (!billertable[0].IsLogoCFDNull())
            {
                if (!billertable[0].LogoCFD.IsNull())
                {
                    if (billertable[0].LogoCFD.Length > 10)
                        this.Logo.Image = System.Drawing.Bitmap.FromStream(new System.IO.MemoryStream(billertable[0].LogoCFD)) as System.Drawing.Bitmap;
                }
            }

            billertable.Dispose();
            billertable = null;

            if (!dataSet.Billings[0].Active)
            {
                this.WaterMark.Enabled = true;
                this.WaterMark.Alpha = 30;
                this.WaterMark.FontColor = System.Drawing.Color.FromArgb(30, 255, 0, 0);
                this.WaterMark.Text = CANCELED;
            }
            else if (this.dataSet.Billings[0].ElectronicBillingType == (int)ElectronicBillingType.CBB && this.dataSet.Billings[0].PreBilling)
            {
                this.WaterMark.Enabled = true;
                this.WaterMark.Alpha = 30;
                this.WaterMark.FontColor = System.Drawing.Color.FromArgb(30, 255, 0, 0);
                this.WaterMark.Text = PREBILLING;
            }

            document = this.BeginPrintDocument();

            stream = new System.IO.MemoryStream();
            document.Save(stream, false);
            e = new BillingDocumentPrintEventArgs(stream);

            return e;
        }
        /// <summary>
        /// These method is used as demo preview and demo report maker only.
        /// </summary>
        /// <param name="dataset">Data set to be printed.</param>
        /// <returns>BillingDocumentPrintEventArgs</returns>
        public BillingDocumentPrintEventArgs PrintDemo(PACFD.DataAccess.BillingsDataSet dataset)
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            BillingDocumentPrintEventArgs e = null;
            System.IO.MemoryStream stream;
            PdfSharp.Pdf.PdfDocument document;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;

            this.dataSet = dataset;
            bill = null;
            this.isDemoDoc = true;
            this.WaterMark = new BillingWaterMark()
            {
                Enabled = true,
                Text = PREVIEW,
                Alpha = 30,
                FontColor = System.Drawing.Color.Black,
                Font = new PdfSharp.Drawing.XFont("Arial", 28),
            };

            if (this.dataSet.IsNull())
                return e;

            if (this.dataSet.Billings.Count < 1)
                return e;

            this.Logo.Image = new System.Drawing.Bitmap(20, 20);

            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(this.Logo.Image))
            {
                g.Clear(System.Drawing.Color.White);
                g.DrawRectangle(System.Drawing.Pens.Black, 0, 0, 19, 19);
                g.DrawLine(System.Drawing.Pens.Blue, 0, 0, 19, 19);
            }

            if (dataSet.Billings.Count > 0)
            {
                using (billertable = (new PACFD.Rules.Billers()).SelectByID(this.dataSet.Billings[0].BillerID))
                {
                    if (billertable.Count > 0 && !billertable[0].IsLogoCFDNull())
                    {
                        stream = new System.IO.MemoryStream(billertable[0].LogoCFD);
                        this.Logo.Image = new System.Drawing.Bitmap(stream);
                    }
                }
            }

            document = this.BeginPrintDocument();
            stream = new System.IO.MemoryStream();
            document.Save(stream, false);
            e = new BillingDocumentPrintEventArgs(stream);

            return e;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="data"></param>
        private PdfSharp.Pdf.PdfDocument BeginPrintDocument()
        {
            bool bneedprint = true;
            Boolean bcanprint = false;
            PdfSharp.Pdf.PdfDocument document = new PdfSharp.Pdf.PdfDocument();
            PdfSharp.Pdf.PdfPage page = null;
            PdfSharp.Drawing.XGraphics g = null;
            PdfSharp.Drawing.XImage image;
            PrintArguments e = null;
            this.pageCount = 0;
            BillingDocumentBeforePrintEventArgs beforeprintargs = new BillingDocumentBeforePrintEventArgs(this.Layers, this.dataSet, this.dataBaseDefinition
                , this.WaterMark, this.isDemoDoc);

            if (!this.BeforePrint.IsNull()) //event trigger before print            
                this.BeforePrint(this, beforeprintargs);

            do
            {
                if (!bcanprint && bneedprint)
                {
                    page = new PdfSharp.Pdf.PdfPage();
                    document.AddPage(page);
                    page.Width = this.Size.X;
                    page.Height = this.Size.Y;

                    if (!g.IsNull())
                        g.Dispose();

                    g = null;
                    g = PdfSharp.Drawing.XGraphics.FromPdfPage(page);

                    if (!this.Image.BillingImage.IsNull())
                        g.DrawImage(PdfSharp.Drawing.XImage.FromGdiPlusImage(this.Image.BillingImage), 0, 0, this.Image.Width, this.Image.Height);

                    e = new PrintArguments(this.pageCount, g, this.dataSet, this.dataBaseDefinition
                        , new PrintArguments.ZoneRegion(0, 0, this.Size.X, this.Size.Y), this.isDemoDoc);
                    bcanprint = true;
                    this.pageCount++;

                    foreach (PrintLayer l in this.Layers)
                    {
                        l.NewPage();
                    }
                }

                bneedprint = false;
                int idx = 0;
                foreach (PrintLayer l in this.Layers)
                {
                    l.idx = idx;
                    l.Draw(e);
                    idx++;
                    if (!bneedprint)
                    {
                        bneedprint = l.NeedPrint;
                        bcanprint = l.CanPrint;
                    }
                }

                if (!this.Logo.Image.IsNull())
                {
                    image = PdfSharp.Drawing.XImage.FromGdiPlusImage(this.Logo.Image);
                    image.Interpolate = false;
                    g.DrawImage(image, this.Logo.X, this.Logo.Y, this.Logo.Width, this.Logo.Height);
                }

            } while (bneedprint);

            if (!g.IsNull())
                g.Dispose();

            g = null;

            if (this.Foot.Enabled)
                this.Foot.DrawFoot(document, this.pageCount);

            if (this.WaterMark.Enabled)
            {
                if (!this.BeforeWaterMakPrint.IsNull())
                    this.BeforeWaterMakPrint(this, beforeprintargs);

                this.WaterMarkDraw(document, System.Drawing.Color.FromArgb(this.WaterMark.Alpha, this.WaterMark.FontColor), this.WaterMark.Text, this.WaterMark.Font);
            }

            return document;
        }
        /// <summary>
        /// Set the text all over the document.
        /// </summary>
        /// <param name="doc">Document to by modify.</param>
        /// <param name="color">Color of the text.</param>
        /// <param name="text">Text to draw verticaly all over the document.</param>
        /// <param name="fontsize">Font size used to draw the text.</param>
        private void WaterMarkDraw(PdfSharp.Pdf.PdfDocument doc, System.Drawing.Color color, string text, double fontsize)
        {
            PdfSharp.Drawing.XFont font = new PdfSharp.Drawing.XFont("Arial", fontsize);
            this.WaterMarkDraw(doc, color, text, font);
        }

        private void WaterMarkDraw(PdfSharp.Pdf.PdfDocument doc, System.Drawing.Color color, string text, PdfSharp.Drawing.XFont font)
        {
            double x = 0, y = 0;
            PdfSharp.Drawing.XSize xsize;
            PdfSharp.Drawing.XSolidBrush solid = new PdfSharp.Drawing.XSolidBrush(color);
            PdfSharp.Drawing.XUnit pw, ph;

            foreach (PdfSharp.Pdf.PdfPage page in doc.Pages)
            {
                pw = page.Width;
                ph = page.Height;

                page.Width += 1500;
                page.Height += 1500;

                using (PdfSharp.Drawing.XGraphics g = PdfSharp.Drawing.XGraphics.FromPdfPage(page))
                {
                    xsize = g.MeasureString(text, font);
                    x = y = 0;

                    g.SmoothingMode = PdfSharp.Drawing.XSmoothingMode.AntiAlias;
                    g.TranslateTransform(0, -200);
                    g.RotateTransform(45);

                    for (double h = 0; h < page.Height; h++)
                    {
                        y = h;

                        for (double w = 0; w < page.Width; w++)
                        {
                            x = w;
                            g.DrawString(text, font, solid, x, y);
                            w += xsize.Width;
                        }

                        h += xsize.Height * 2;
                    }
                }

                page.Width = pw;
                page.Height = ph;
            }
        }
    }
}