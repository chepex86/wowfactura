﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Sealed class used as arguments send by the printing class before print the PDF document.
    /// </summary>
    public sealed class BillingDocumentBeforePrintEventArgs : EventArgs
    {
        /// <summary>
        /// Layers to be print to the PDF.
        /// </summary>
        public PrintLayerCollection Layers { get; private set; }
        public PACFD.DataAccess.BillingsDataSet DataSet { get; private set; }
        public DataBaseDefinition PacfdDataBaseSchema { get; private set; }
        public PACFD.Rules.PrintDocument.BillingDocument.BillingWaterMark WaterMark { get; private set; }
        /// <summary>
        /// Get a boolean indigating if is a demo print
        /// </summary>
        public bool IsDemo { get; private set; }
        /// <summary>
        /// Get the electronict billing type of the print
        /// </summary>
        public ElectronicBillingType BillingElectronicType { get { return (ElectronicBillingType)this.DataSet.Billings[0].ElectronicBillingType; } }
        /// <summary>
        /// Get a boolean value indicating if the billing is active (canceled)
        /// </summary>
        public bool IsBillingActive { get { return this.DataSet.Billings[0].Active; } }

        public BillingDocumentBeforePrintEventArgs(PrintLayerCollection layers, PACFD.DataAccess.BillingsDataSet billing, DataBaseDefinition pacfddbshema
            , PACFD.Rules.PrintDocument.BillingDocument.BillingWaterMark watermark, bool isdemo)
        {
            this.Layers = layers;
            this.DataSet = billing;
            this.PacfdDataBaseSchema = pacfddbshema;
            this.WaterMark = watermark;
            this.IsDemo = isdemo;
        }
    }
}
