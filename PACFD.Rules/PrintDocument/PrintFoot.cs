﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    public class PrintFoot
    {
        public System.Drawing.Point Location { get; set; }
        public int X { get { return this.Location.X; } set { this.Location = new System.Drawing.Point(value, this.Location.Y); } }
        public int Y { get { return this.Location.Y; } set { this.Location = new System.Drawing.Point(this.Location.X, value); } }
        public PdfSharp.Drawing.XFont Font { get; set; }
        public string Text { get; set; }
        public bool Enabled { get; set; }


        public PrintFoot()
        {
            this.Enabled = false;
        }

        public void LoadFromXML(System.Xml.XmlNode root)
        {
            this.Text = root.Attributes["text"].IsNull() ? null : root.Attributes["text"].Value;

            this.Location = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Font = new PdfSharp.Drawing.XFont(
                root.Attributes["font"].IsNull() ? "Arial" : root.Attributes["font"].Value,
                root.Attributes["size"].IsNull() ? 8 : root.Attributes["size"].Value.ToInt32());

            this.Enabled = true;
        }

        public void DrawFoot(PdfSharp.Pdf.PdfDocument document, int pagecount)
        {
            int i = 1;
            string subtext = this.Text;

            foreach (PdfSharp.Pdf.PdfPage page in document.Pages)
            {
                subtext = subtext.Replace("{0}", i.ToString());
                subtext = subtext.Replace("{1}", pagecount.ToString());

                using (PdfSharp.Drawing.XGraphics g = PdfSharp.Drawing.XGraphics.FromPdfPage(page))
                { g.DrawString(subtext, this.Font, PdfSharp.Drawing.XBrushes.Black, this.X, this.Y); }

                subtext = this.Text;
                i++;
            }
        }
    }
}
