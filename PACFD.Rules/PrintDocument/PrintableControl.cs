﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Class used as base for childrens of the print layer class.
    /// </summary>
    public abstract class PrintControl : PrintControlBase
    {
        /// <summary>
        /// Lines used by the control.
        /// </summary>
        public int LinesUsed { get; protected set; }
        /// <summary>
        /// Get an integer value with the recalculated 
        /// total height used by the control.
        /// </summary>
        public virtual int GetHeightLinesToUse(PrintArguments e)
        {
            return 0;
        }
    }
}
