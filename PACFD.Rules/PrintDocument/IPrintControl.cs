﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Minimum interface base class used for print controls.
    /// </summary>
    public interface IPrintControl
    {
        /// <summary>
        /// Relative position of the control in the document.
        /// </summary>
        System.Drawing.Point Position { get; }
        /// <summary>
        /// Id of the control.
        /// </summary>
        string ID { get; set; }

        /// <summary>
        /// Draw method of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the print document.</param>
        void Draw(PrintArguments e);
        
        /// <summary>
        /// New page method informer of the control.
        /// </summary>
        void NewPage();
        /// <summary>
        /// Load event of the control.
        /// </summary>
        /// <param name="root">Node with the needed information to load the control.</param>
        void LoadFromNode(System.Xml.XmlNode root);
    }
}
