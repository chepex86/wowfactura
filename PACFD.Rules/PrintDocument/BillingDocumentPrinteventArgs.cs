﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Result class after process all data in a printed processes
    /// </summary>
    public class BillingDocumentPrintEventArgs : EventArgs
    {
        /// <summary>
        /// Get the PDF result of the print.
        /// </summary>
        public System.IO.MemoryStream PDFDocumnet { get; private set; }

        /// <summary>
        /// Generated a byte array from the PDFDocument.
        /// </summary>
        /// <returns>If faild return null else return an array of bytes.</returns>
        public byte[] PdfToByteArray()
        {
            if (this.PDFDocumnet.IsNull())
                return null;

            byte[] b = new byte[this.PDFDocumnet.Length];
            this.PDFDocumnet.Seek(0, System.IO.SeekOrigin.Begin);
            this.PDFDocumnet.Read(b, 0, b.Length);

            return b;
        }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="pdf">PDF result of the print.</param>
        public BillingDocumentPrintEventArgs(System.IO.MemoryStream pdf)
        {
            this.PDFDocumnet = pdf;
        }
    }
}
