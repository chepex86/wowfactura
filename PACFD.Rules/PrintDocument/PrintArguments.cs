﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Class used as an arguments to print also know as DataBinder.
    /// </summary> 
    public class PrintArguments : EventArgs
    {
        /// <summary>
        /// Name of the addendum table keyword.
        /// </summary>
        public const string ADDENDA = "addenda";

        private string regimeFiscal = string.Empty;
        private bool regimeFiscalStringCalculated = false;

        /// <summary>
        /// Get a integer with the page number.
        /// </summary>
        public int Page { get; private set; }
        /// <summary>
        /// Graphic class ref of the actual page.
        /// </summary>
        public PdfSharp.Drawing.XGraphics Graphic { get; private set; }
        /// <summary>
        /// Get the data set from where the data is retrived.
        /// </summary>
        public PACFD.DataAccess.BillingsDataSet Billing { get; private set; }
        /// <summary>
        /// Get the dictionary definition of the data base.
        /// </summary>
        public DataBaseDefinition DBDefinition { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public int UseIndex { get; set; }
        /// <summary>
        /// Zone in where to print.
        /// </summary>
        public ZoneRegion Region { get; private set; }
        /// <summary>
        /// Get a boolean indicating if the bill is a demo.
        /// </summary>
        public bool IsDemo { get; private set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="page">Number of the page.</param>
        /// <param name="graphic">Graphic class used to draw.</param>
        /// <param name="billing">Billing data set with all the needed informaiton.</param>
        /// <param name="dbdefinition">The data base, data table, data row definition of the  data set. Addendum is not included.</param>
        /// <param name="region">Zone region where to draw.</param>
        public PrintArguments(int page, PdfSharp.Drawing.XGraphics graphic, PACFD.DataAccess.BillingsDataSet billing
            , DataBaseDefinition dbdefinition, ZoneRegion region
            , bool isdemo)
        {
            int i = 0;
            this.Page = page;
            this.Graphic = graphic;
            this.Billing = billing;
            this.DBDefinition = dbdefinition;
            this.Region = region;
            System.Data.DataRow[] r = null;

            this.IsDemo = isdemo;

            //Comprobante de Pagos 
            /*if (billing.BillingsPayments_3.Count > 0)
                using (DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable addtablePayment = (new Rules.BillingsAddendum()).SelectByID(billing.Billings[0].BillingID))
                {
                    if (addtablePayment.Count > 0)
                    {
                        this.Billing.LoadAddendum(addtablePayment[0].Xml);
                    }
                }
            else */
            if (billing != null && billing.Billings.Count > 0 && this.Billing.Addendum.Tables.Count < 1)
                using (DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable addtable = (new Rules.BillingsAddendum()).SelectByID(billing.Billings[0].BillingID))
                    if (addtable.Count > 0)
                        this.Billing.LoadAddendum(addtable[0].Xml);

            //check if already table added and if not demo
            if (!this.IsDemo && billing != null)
            {
                i = 0;
                r = billing.BillingsDetails.Select("BillingsDetailsID < 1");
                //if detail concept not calculable table isn't added then add
                if (!r.Any())
                {
                    foreach (PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow row in billing.BillingsDetailNotes)
                    {
                        --i; //first ID = -1
                        PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow drow = billing.BillingsDetails.NewBillingsDetailsRow();
                        drow.ConceptID = i;
                        drow.BillingsDetailsID = row.BillingDetailNoteID * -1;
                        drow.BillingID = row.BillingID;
                        drow.SetPrintOnlyDescriptionNull();
                        drow.DiscountPercentage = 0;
                        drow.AppliesTax = false;
                        drow.Amount = 0;

                        if (!row.IsDescriptionNull())
                            drow.Description = row.Description;
                        else
                            drow.Description = string.Empty;

                        if (!row.IsPriceNull())
                            drow.UnitValue = row.Price;
                        else
                            drow.UnitValue = 0;

                        if (!row.IsCodeNull())
                            drow.Unit = row.Code;
                        else
                            drow.Unit = string.Empty;

                        if (!row.IsCountNull())
                            drow.Count = row.Count;
                        else
                            drow.Count = 0;

                        drow.IdentificationNumber = string.Empty;
                        billing.BillingsDetails.AddBillingsDetailsRow(drow);
                        drow.AcceptChanges();
                    }
                }
            }
        }
        /// <summary>
        /// Get the row count of a table.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public int TableRowCount(string table)
        {
            switch (table.ToLower())
            {
                case "billings": return this.Billing.Billings.Count;
                case "billingsreceptors": return this.Billing.BillingsReceptors.Count;
                case "taxes": return this.Billing.Taxes.Count;
                case "billingsbillers": return this.Billing.BillingsBillers.Count;
                case "billingsissued": return this.Billing.BillingsIssued.Count;
                case "billingsdetails": return this.Billing.BillingsDetails.Count;
            }

            return 0;
        }
        /// <summary>
        /// Get a string array of data from the data base.
        /// </summary>
        /// <param name="values">Array with the data to reatrive.</param>
        /// <returns>If success return an array of strings.</returns>
        public string[] GetStringData(string[] values)
        {
            string[] split;
            List<string> result = new List<string>();
            string[] table;

            foreach (string item in values)
            {
                split = item.Trim().Split('.');

                if (split.Length < 1)
                    continue;

                if (split.Length > 2 && split[0].ToLower() == ADDENDA)
                {
                    result.Add(this.GetAddendumString(split[1], split[2]));
                    continue;
                }

                if (split.Length < 2)
                    continue;

                //--------------------
                //fusion of printtool with printlabel

                if (split[0] == "tool")
                {
                    result.Add(this.GetQrData(split));
                    continue;
                }

                //-------------------
                //data from database

                table = this.DBDefinition.GetColumn(split[0].Trim(), split[1].Trim());

                if (table.IsNull())
                    continue;

                if (table.Length < 1)
                    continue;

                if (this.UseIndex > -1) //only BillingsDetails use this property (USED WHEN IS A LIST)
                    result.Add(this.GetStringDataFromTable(table[0], table[1], this.UseIndex));
                else
                    result.Add(this.GetStringDataFromTable(table[0], table[1]));
            }

            return result.ToArray();
        }
        /// <summary>
        /// Get an objet type from the data base
        /// </summary>
        /// <param name="values">Array with the data to reatrive.</param>
        /// <returns>If success return an object.</returns>
        public object[] GetObjectData(string[] values)
        {
            List<object> result = new List<object>();
            string[] split;
            string[] table;

            foreach (string item in values)
            {
                split = item.Split('.');

                if (split.Length < 1)
                    continue;

                if (split.Length > 2 && split[0].ToLower() == ADDENDA)
                {
                    result.Add(this.GetAddendumObject(split[1], split[2]));
                    continue;
                }

                if (split.Length < 2)
                    continue;

                table = this.DBDefinition.GetColumn(split[0].Trim(), split[1].Trim());

                if (table.IsNull())
                    continue;

                if (table.Length < 1)
                    continue;

                result.Add(this.GetObjectDataFromTable(table[0], table[1]));
            }

            return result.ToArray();
        }
        /// <summary>
        /// Get a string that is contained in the Qr.
        /// </summary>
        /// <param name="method">
        /// Information type to get. decimaltoletters, qr-rfc, qr-numeroaprovacion,
        /// qr-folio-inicio, qr-folio-fin, qr-serie, qr-fecha-expedicion, qr-fecha-expiracion
        /// </param>
        /// <returns></returns>
        private string GetQrData(string[] method)
        {
            string[] s = null, saxu = null;
            string curr = string.Empty;

            switch (method[1].ToLower().Trim())
            {
                case "decimaltoletters":
                    s = this.GetStringData(string.IsNullOrEmpty(method[2]) ? new string[] { } : method[2].Split(','));

                    if (s.Length < 1)
                    {
                        PACFD.Common.LogManager.WriteWarning(new Exception("PACFD.Rules.PrintDocument.PrintTools.Draw(PrintArguments e) - Data Base can be null."));
                        return string.Empty;
                    }

                    saxu = this.GetStringData(new string[] { "T1.C1" }); //   Includes/DBDefinition.xml

                    if (saxu.Length < 1)
                    {
                        PACFD.Common.LogManager.WriteWarning(new Exception("PACFD.Rules.PrintDocument.PrintTools.Draw(PrintArguments e) - Currency type is empty."));
                        return string.Empty;
                    }

                    curr = saxu[0];

                    s = new string[] { s[0].ToDecimal().ToLetters(curr.ToLower() == "mxn" ? PACFD.Common.CurrencyType.MXN : PACFD.Common.CurrencyType.USD) };
                    break;
                case "qr-rfc":
                case "qr-numeroaprovacion":
                case "qr-folio-inicio":
                case "qr-folio-fin":
                case "qr-serie":
                case "qr-fecha-expedicion":
                case "qr-fecha-expiracion":
                    s = method[1].ToLower().Trim().Split('-');
                    saxu = new string[] { "" };

                    if (s[1].ToLower() == "rfc")
                        saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.RFC);
                    else if (s[1].ToLower() == "numeroaprovacion")
                        saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.ApprovalNumber);
                    else if (s[1].ToLower() == "folio")
                    {
                        if (s[2].ToLower() == "inicio")
                            saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.FolioStart);
                        else if (s[2].ToLower() == "fin")
                            saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.FolioEnd);
                    }
                    else if (s[1].ToLower() == "serie")
                        saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.Serial);
                    else if (s[1].ToLower() == "fecha")
                    {
                        if (s[2].ToLower() == "expedicion")
                            saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.ExpeditionDate);
                        else if (s[2].ToLower() == "expiracion")
                            saxu[0] = this.Billing.Billings[0].GetQrData(QrCBBDataType.ExpirationDate);
                    }

                    s = new string[] { saxu[0] };
                    break;
            }

            return s[0];
        }
        /// <summary>
        /// Get an object from the fake data table addendum.
        /// </summary>
        /// <param name="table">Name of the table.</param>
        /// <param name="col">Name of the column.</param>
        /// <returns>If success return an object else null.</returns>
        private object GetAddendumObject(string table, string col)
        {
            int i = -1;

            if (this.Billing.Addendum.Tables.Count < 1)
            { return null; }

            foreach (System.Data.DataTable t in this.Billing.Addendum.Tables)
            {
                if (t.TableName != table)
                { continue; }

                i = -1;

                foreach (System.Data.DataColumn c in t.Columns)
                {
                    ++i;

                    if (c.ColumnName != col || t.Rows.Count < 1)
                    { continue; }

                    if (i > -1 && i < t.Rows[0].ItemArray.Length)
                    { return t.Rows[0].ItemArray[i]; }
                }
            }

            return null;
        }
        /// <summary>
        /// Get an object from the fake data table addendum.
        /// </summary>
        /// <param name="table">Name of the table.</param>
        /// <param name="col">Name of the column.</param>
        /// <returns>Return a string.</returns>
        private string GetAddendumString(string table, string col)
        {
            string s;
            object o = this.GetAddendumObject(table, col);

            if (o.IsNull())
            { return ""; }

            s = o.ToString();
            return s;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="col"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private string GetStringDataFromTable(string table, string col)
        {
            string s = string.Empty;

            switch (table.ToLower())
            {  // se agrego para los campos personalizados
                case "billingobservations": 
                        s = GetFldStringData(col); break;
                case "billings": s = this.GetBillingStringData(col); break;
                case "billingspayments_3": s = this.GetBillingsPaymentStringData(col); break;
                case "billingsreceptors": s = this.GetReceptorStringData(col); break;
                case "taxes": s = this.GetTaxesStringData(col); break;
                case "billingsbillers":
                    s = this.GetBillersStringData(col);
                    break;
                case "billingsissued":

                    //if (this.Billing.BillingsBillers.Count > 0)
                    //{
                    //    if (!this.Billing.BillingsBillers[0].IssuedInDifferentPlace &&
                    //        (col.ToLower() == "billername" || col.ToLower() == "billerrfc"
                    //        || col.ToLower() == "zipcode"))
                    //    {
                    //s = this.GetBillersStringData(col);
                    //        break;
                    //    }
                    //}

                    //if (col.ToLower() == "billeraddress")
                    //    col = "Address";

                    //s = this.GetIssuedData(col);
                    if (this.Billing.BillingsIssued.Count < 1)//si no hay expedido mandar los del domicilio fiscal
                    {
                        if (col.ToLower() == "address") col = "billeraddress";
                        s = this.GetBillersStringData(col);
                    }
                    else
                    {
                        s = this.GetIssuedStringData(col);
                    }
                    break;
                case "billingfiscalregime": s = this.GetBillingFiscalRegimeStringData(col, 0); break;
            }

            return s;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="col"></param>
        /// <param name="data"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private string GetStringDataFromTable(string table, string col, int index)
        {
            string s = string.Empty;

            switch (table.ToLower())
            {
                case "billingobservations":
                    s = GetFldStringData(col); break;

                case "billings": s = this.GetBillingStringData(col); break;
                case "billingsbillers":
                case "billingsissued":

                    //if (this.Billing.BillingsBillers.Count > 0)
                    //{
                    //    if (!this.Billing.BillingsBillers[0].IssuedInDifferentPlace &&
                    //        (col.ToLower() == "billername" || col.ToLower() == "billerrfc"))
                    //    {
                    s = this.GetBillersStringData(col);
                    //        break;
                    //    }
                    //}

                    //if (col.ToLower() == "billeraddress")
                    //    col = "Address";

                    //s = this.GetIssuedData(col);
                    break;
                case "billingsreceptors": s = this.GetReceptorStringData(col); break;
                case "taxes": s = this.GetTaxesStringData(col); break;
                case "billingsdetails": s = this.GetBillingsDetailsStringData(col, index); break;
                case "billingfiscalregime": s = this.GetBillingFiscalRegimeStringData(col, index); break;
                case "billingspayments_3": s = this.GetBillingsPaymentStringData(col, index); break;
            }

            return s;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private object GetObjectDataFromTable(string table, string col)
        {
            object o = null;

            switch (table.ToLower())
            {
                case "billings": o = this.GetBillingObjectData(col); break;
                default: return null;
            }

            return o;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        private object GetBillingObjectData(string col)
        {
            int index = 0;

            if (this.Billing.Billings.Count < 1)
            { return null; }

            foreach (System.Data.DataColumn item in this.Billing.Billings[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                { break; }

                index++;
            }

            if (index > this.Billing.Billings[0].ItemArray.Length - 1)
            { return null; }

            return this.Billing.Billings[0].ItemArray[index];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetBillingStringData(string col)
        {
            int index = 0;
            decimal d;

            if (this.Billing.Billings.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.Billings[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.Billings[0].ItemArray.Length - 1)
                return string.Empty;

            d = 0;

            if (col.ToLower() == "PlaceDispatch".ToLower())
            {
                return Helperss.GetPlaceDispatch(this.Billing.Billings[0].ItemArray[index].ToString(), this.Billing.Billings[0].BillerID);
            }

            if (col.ToLower() == "PaymentMethod".ToLower())
            {
                return Helperss.GetPaymentMethodByCode(this.Billing.Billings[0].ItemArray[index].ToString());
            }

            if (col.ToLower() == "PaymentForm".ToLower())
            {
                return Helperss.GetPaymentFormByCode(this.Billing.Billings[0].ItemArray[index].ToString());
            }

            if (col.ToLower() != "AccountNumberPayment".ToLower())
            {
                if (decimal.TryParse(this.Billing.Billings[0].ItemArray[index].ToString(), out d))
                    return Math.Round(d, 2).ToString();
            }

            return this.Billing.Billings[0].ItemArray[index].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetBillingsPaymentStringData(string col)
        {
            int index = 0;

            if (this.Billing.BillingsPayments_3.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.BillingsPayments_3[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsPayments_3[0].ItemArray.Length - 1)
                return string.Empty;

            if (col.ToLower() == "paymentform")
                return GetPaymentForm(this.Billing.BillingsPayments_3[0].PaymentForm);

            if (col.ToLower() == "billingdate")
                return this.Billing.BillingsPayments_3[0].BillingDate.ToString();
            if (col.ToLower() == "subtotal")
                return "0.00";
            //Ignore format string
            if (col.ToLower() == "certificatenumbersat" || col.ToLower() == "reference" || col.ToLower() == "partiality" || col.ToLower() == "ctabeneficiario" || col.ToLower() == "ctaordenante")
                return this.Billing.BillingsPayments_3[0].ItemArray[index].ToString();

                decimal d = 0;
            if (decimal.TryParse(this.Billing.BillingsPayments_3[0].ItemArray[index].ToString(), out d))
                return d.ToString("###,###,###,###,##0.00");

            return this.Billing.BillingsPayments_3[0].ItemArray[index].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetBillersStringData(string col)
        {
            int index = 0;

            if (this.Billing.BillingsBillers.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.BillingsBillers[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsBillers[0].ItemArray.Length - 1)
                return "";


            if (col.ToLower() == "TaxSystem".ToLower())
            {
                return Helperss.GetTaxSystemByCode(this.Billing.BillingsBillers[0].ItemArray[index].ToString());
            }

            return this.Billing.BillingsBillers[0].ItemArray[index].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetIssuedStringData(string col)
        {
            int index = 0;

            if (this.Billing.BillingsIssued.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.BillingsIssued[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsIssued[0].ItemArray.Length - 1)
                return "";

            return this.Billing.BillingsIssued[0].ItemArray[index].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetReceptorStringData(string col)
        {
            int index = 0;
            string s;

            if (this.Billing.BillingsReceptors.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.BillingsReceptors[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsReceptors[0].ItemArray.Length - 1)
                return "";

            s = this.Billing.BillingsReceptors[0].ItemArray[index].ToString();

            if (col.ToLower().Trim() == "zipcode")
                if (s.Length > 0 && s.ToInt32() < 1)
                    s = string.Empty;

            if (col.ToLower().Trim() == "taxsystem")
            {
                s = Helperss.GetUsoCFDIByCode(this.Billing.BillingsReceptors[0].ItemArray[index].ToString());
            }

            return s;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private string GetTaxesStringData(string col)
        {
            int index = 0;
            decimal d;

            if (this.Billing.Taxes.Count < 1)
                return string.Empty;

            foreach (System.Data.DataColumn item in this.Billing.Taxes[0].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.Taxes[0].ItemArray.Length - 1)
                return "";

            d = 0;

            if (decimal.TryParse(this.Billing.Taxes[0].ItemArray[index].ToString(), out d))
                return Math.Round(d, 2).ToString();

            return this.Billing.Taxes[0].ItemArray[index].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="col"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private string GetBillingsDetailsStringData(string col, int count)
        {
            int index = 0;
            decimal d;
            string s;
            System.Data.DataRow[] rows;
            PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow rownote;

            if (this.Billing.BillingsDetails.Count < 1)
                return string.Empty;

            //
            //check for negative ID request then 
            //send detail not calculate table if not demo
            //
            if (!this.IsDemo && this.Billing.BillingsDetails[count].BillingsDetailsID < 0)
            {
                //select from ID, multiply for (-1) to get a positive ID
                rows = this.Billing.BillingsDetailNotes.Select("BillingDetailNoteID = "
                    + (this.Billing.BillingsDetails[count].BillingsDetailsID * -1).ToString());

                if (rows.Length < 1)
                    return string.Empty;

                rownote = (PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow)rows[0];
                s = string.Empty;

                switch (col.ToLower())
                {
                    case "unit":
                        if (!rownote.IsCodeNull())
                            s = rownote.Code;
                        break;
                    case "description":
                        if (!rownote.IsDescriptionNull())
                            s = rownote.Description;
                        break;
                    case "unitvalue":
                        if (!rownote.IsPriceNull())
                            s = rownote.Price.ToString("###,###,###,###,##0.00");
                        break;
                    case "count":
                        if (!rownote.IsCountNull())
                            s = rownote.Count.ToString("###,###,###,###,##0.####");
                        break;
                    case "amount":
                        if (!rownote.IsImportNull())
                            s = rownote.Import.ToString("###,###,###,###,##0.00");
                        break;
                    default:
                        //any other column is not sopported (yet!)
                        return s;
                }

                return s;
            }
            //
            //normal detail concept table request variable
            //
            foreach (System.Data.DataColumn item in this.Billing.BillingsDetails[count].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsDetails[count].ItemArray.Length - 1)
                return string.Empty;

            d = 0;

            

            if (col.ToLower() != "description" && !this.Billing.BillingsDetails[count].IsPrintOnlyDescriptionNull() && this.Billing.BillingsDetails[count].PrintOnlyDescription)
                return string.Empty;

            if (col.ToLower() == "countstring" || col.ToLower() == "count")
                return this.Billing.BillingsDetails[count].Count.ToString("###,###,###,###,##0.####");

            if (col.ToLower() == "ClaveProdServ".ToLower())
            {
                return Helperss.GetClaveProdServCode(this.Billing.BillingsDetails[count].ItemArray[index].ToString());
            }

            if (col.ToLower() == "TaxType".ToLower())
            {
                return Helperss.GetImpuesto(this.Billing.BillingsDetails[count].ItemArray[index].ToString());
            }


            if (decimal.TryParse(this.Billing.BillingsDetails[count].ItemArray[index].ToString(), out d))
                return d.ToString("###,###,###,###,##0.00");

            return this.Billing.BillingsDetails[count].ItemArray[index].ToString();
        }

        private string GetBillingsPaymentStringData(string col, int count)
        {
            int index = 0;
            decimal d;
            string s;
            System.Data.DataRow[] rows;
            PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row paymentRow;

            if (this.Billing.BillingsPayments_3.Count < 1)
                return string.Empty;

            //
            //check for negative ID request then 
            //send detail not calculate table if not demo
            //
            if (!this.IsDemo && this.Billing.BillingsPayments_3[count].BillingPaymentId < 0)
            {
                //select from ID, multiply for (-1) to get a positive ID
                rows = this.Billing.BillingsPayments_3.Select("BillingPaymentId = "
                    + (this.Billing.BillingsPayments_3[count].BillingPaymentId * -1).ToString());

                if (rows.Length < 1)
                    return string.Empty;

                paymentRow = (PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row)rows[0];
                s = string.Empty;

                /*switch (col.ToLower())
                {
                    case "reference":
                        if (!paymentRow.IsReferenceNull())
                            return paymentRow.Reference;
                        break;
                   /* case "unit":
                        if (!paymentRow.sIsCodeNull())
                            s = paymentRow.Code;
                        break;
                    case "description":
                        if (!paymentRow.IsDescriptionNull())
                            s = paymentRow.Description;
                        break;
                    case "unitvalue":
                        if (!paymentRow.IsPriceNull())
                            s = paymentRow.Price.ToString("###,###,###,###,##0.00");
                        break;
                    case "count":
                        if (!paymentRow.IsCountNull())
                            s = paymentRow.Count.ToString("###,###,###,###,##0.####");
                        break;
                    case "amount":
                        if (!paymentRow.IsImportNull())
                            s = paymentRow.Import.ToString("###,###,###,###,##0.00");
                        break;
                    default:
                        //any other column is not sopported (yet!)
                        return s;

                } */
            }
            //
            //normal detail concept table request variable
            //
            foreach (System.Data.DataColumn item in this.Billing.BillingsPayments_3[count].Table.Columns)
            {
                if (col.ToLower() == item.Caption.ToLower())
                    break;

                index++;
            }

            if (index > this.Billing.BillingsPayments_3[count].ItemArray.Length - 1)
                return string.Empty;

            d = 0;


            /*
                        if (col.ToLower() != "description")
                            return string.Empty;

                        if (col.ToLower() == "countstring" || col.ToLower() == "count")
                            return this.Billing.BillingsPayments_3[count]..ToString("###,###,###,###,##0.####");
                        */
            

            return this.Billing.BillingsPayments_3[count].ItemArray[index].ToString();
        }
        private string GetPaymentForm(string form)
        {
            switch (form)
            {
                case "01":
                    form += " - Efectivo";
                    break;
                case "02":
                    form += " - Cheque Nominativo";
                    break;
                case "03":
                    form += " - Transferencia electrónica de fondos";
                    break;
                case "04":
                    form += " - Tarjeta de crédito";
                    break;
                case "05":
                    form += " - Monedero electrónico";
                    break;
                case "06":
                    form += " - Dinero electrónico";
                    break;
                case "08":
                    form += " - Vales de despensa";
                    break;
                case "12":
                    form += " - Dación de pago";
                    break;
                case "13":
                    form += " - Pago por subrogación";
                    break;
                case "14":
                    form += " - Pago por consignación";
                    break;
                case "15":
                    form += " - Condonación";
                    break;
                case "17":
                    form += " - Compensación";
                    break;
                case "23":
                    form += " - Novación";
                    break;
                case "24":
                    form += " - Confusión";
                    break;
                case "25":
                    form += " - Remisión de deuda";
                    break;
                case "26":
                    form += " - Prescripción o caducidad";
                    break;
                case "27":
                    form += " - A satisfacción del acreedor";
                    break;
                case "28":
                    form += " - Tarjeta de débito";
                    break;
                case "29":
                    form += " - Tarjeta de servicios";
                    break;
                case "30":
                    form += " - Aplicación de anticipos";
                    break;
            }
            return form;
        }

        private string GetBillingFiscalRegimeStringData(string col, int count)
        {
            //int index = 0;
            ////BillingFiscalRegime
            //if (this.Billing.BillingFiscalRegime.Count < 1)
            //    return string.Empty;

            //foreach (System.Data.DataColumn item in this.Billing.BillingFiscalRegime[count].Table.Columns)
            //{
            //    if (col.ToLower() == item.Caption.ToLower())
            //        break;

            //    index++;
            //}

            //if (index > this.Billing.BillingFiscalRegime[count].ItemArray.Length - 1)
            //    return string.Empty;

            //return this.Billing.BillingFiscalRegime[count].ItemArray[index].ToString();

            if (string.IsNullOrEmpty(this.regimeFiscal) && !regimeFiscalStringCalculated)
            {
                string s = string.Empty;
                List<string> li = (from x in this.Billing.BillingFiscalRegime where !string.IsNullOrEmpty(x.Regime) select x.Regime).ToList<string>();

                if (li.Count > 0)
                    s = string.Join(", ", li.ToArray());

                this.regimeFiscal = s;
                this.regimeFiscalStringCalculated = true;
            }

            return this.regimeFiscal;
        }

        private string GetFldStringData(string col)
        {
        //    int index = 0;

        //    if (this.Billing.BillingFields.Count < 1)
        //        return string.Empty;
        //    foreach (System.Data.DataColumn item in this.Billing.BillingFields[0].Table.Columns)
        //    {
        //        if (col.ToLower() == item.Caption.ToLower())
        //            break;
        //        index++;
        //    }

        //    if (index > this.Billing.BillingFields[0].ItemArray.Length - 1)
        //        return "";

        //    return this.Billing.BillingFields[0].ItemArray[index].ToString();
            string s = string.Empty;
            if (string.IsNullOrEmpty(this.regimeFiscal) && !regimeFiscalStringCalculated)
            {
               
               

            
                    if (col=="FName")
                    {
                        List<string> li = (from x in this.Billing.BillingFields select  x.FName).ToList();
                        s = string.Join("\n", li.ToArray());
                    }
                    else
                    {
                        List<string> li = (from x in this.Billing.BillingFields select x.FValue).ToList();
                        s = string.Join("\n", li.ToArray());
                    }

            }
            return s;
        }


        /// <summary>
        /// Class used as zone definition.
        /// </summary>
        public class ZoneRegion
        {
            /// <summary>
            /// Get or set the location of the zone.
            /// </summary>
            public System.Drawing.Point Location { get; set; }
            /// <summary>
            /// Get or set the size of the zone.
            /// </summary>
            public System.Drawing.Size Size { get; set; }
            /// <summary>
            /// Get or set the x coordenate of the zone.
            /// </summary>
            public int X { get { return this.Location.X; } set { this.Location = new System.Drawing.Point(value, this.Location.Y); } }
            /// <summary>
            /// Get or set the y coordenate of the zone.
            /// </summary>
            public int Y { get { return this.Location.Y; } set { this.Location = new System.Drawing.Point(this.Location.X, value); } }
            /// <summary>
            /// Get or set the width of the zone.
            /// </summary>
            public int Width { get { return this.Size.Width; } set { this.Size = new System.Drawing.Size(value, this.Size.Height); } }
            /// <summary>
            /// Get or set the height of the zone.
            /// </summary>
            public int Height { get { return this.Size.Height; } set { this.Size = new System.Drawing.Size(this.Size.Width, value); } }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="x">X coordenate of the zone.</param>
            /// <param name="y">Y coordenate of the zone.</param>
            /// <param name="width">Width of the zone.</param>
            /// <param name="height">Height of the zone.</param>
            public ZoneRegion(int x, int y, int width, int height)
            {
                this.X = x;
                this.Y = y;
                this.Width = width;
                this.Height = height;
            }
        }
    }
}