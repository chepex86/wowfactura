﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Base class for the childrens controls of the document.
    /// </summary>
    public abstract class PrintControlBase : IPrintControl
    {
        /// <summary>
        /// Get or set the font of the control.
        /// </summary>
        public PdfSharp.Drawing.XFont Font { get; set; }
        /// <summary>
        /// Get or set the position of the control.
        /// </summary>
        public abstract System.Drawing.Point Position { get; set; }
        /// <summary>
        /// Id of the control.
        /// </summary>
        public virtual string ID { get; set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public PrintControlBase()
        {
            this.Position = new System.Drawing.Point(0, 0);
        }
        /// <summary>
        /// Draw method of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the print document.</param>
        public abstract void Draw(PrintArguments e);
        
        /// <summary>
        /// New page method informer of the control.
        /// </summary>
        public abstract void NewPage();
        /// <summary>
        /// Load event of the control.
        /// </summary>
        /// <param name="root">Node with the needed information to load the control.</param>
        public virtual void LoadFromNode(System.Xml.XmlNode root)
        {
            this.ID =  root != null && root.Attributes["id"] != null ?  root.Attributes["id"].Value : string.Empty;
        }
    }
}
