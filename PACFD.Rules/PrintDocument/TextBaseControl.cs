﻿using System;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Base class for draw text controls.
    /// </summary>
    public abstract partial class TextBaseControl : PrintControl
    {
        /// <summary>
        /// Text to by draw.
        /// </summary>
        public virtual string Text { get; set; }
        /// <summary>
        /// Max width of the text.
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Absolute position of the control refered by the layer container.
        /// </summary>
        public override System.Drawing.Point Position { get; set; }
        /// <summary>
        /// Get or set the aligment used.
        /// </summary>
        public virtual PdfSharp.Drawing.XStringAlignment Aligment { get; set; }
        /// <summary>
        /// Get or set the value that indicate if the text use aligment.
        /// </summary>
        public virtual bool UseAligment { get; set; }
        /// <summary>
        /// Get or set the color used to draw the text.
        /// </summary>
        public virtual System.Drawing.Color FontColor { get; set; }

        ///// <summary>
        ///// Draw more than one line at the time.
        ///// </summary>
        ///// <param name="e">Print arguments send by PrintDocument or PrintLayer.</param>
        ///// <param name="s">Text to be printed.</param>
        //public abstract void DrawInLines(PrintArguments e, string s);
        /// <summary>
        /// Parce a text that will be sustituted with a custom one.
        /// </summary>
        /// <param name="e">Print arguments send by PrintDocument or PrintLayer.</param>
        /// <param name="data">Data that is gona be placed in the text field.</param>
        /// <returns>Return a string with the {0}, {1}, et.. sustituted be the data parameter.</returns>
        protected virtual string ParceText(PrintArguments e, string[] data)
        {
            string text = this.Text;

            if (data.IsNull())
            { return text; }

            //for (int i = 0; i < data.Length; i++)
            //{ text = text.Replace("{" + i.ToString() + "}", data[i]); }

            //text = this.Test(text, data);
            text = (new TextParser.TextParser()).Parse(text, data);

            return text;
        }
        ///// <summary>
        ///// This a test for a new type of command the string.format.
        ///// This method must override ParceText.
        ///// </summary>
        ///// <param name="data"></param>
        //private string Test(string text, string[] data)
        //{
        //    PACFD.Rules.PrintDocument.TextParser.TextParser p = new PACFD.Rules.PrintDocument.TextParser.TextParser();

        //    return p.Parse(text, data);
        //}
    }
}
