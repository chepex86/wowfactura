﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.PrintDocument
{
    public class PrintImage : PrintControlBase
    {
        public string Data { get; set; }
        public override System.Drawing.Point Position { get; set; }
        public System.Drawing.Size Size { get; set; }

        private bool isInitialized = false;
        private PdfSharp.Drawing.XImage imge = null;

        public override void Draw(PrintArguments e)
        {
            System.Drawing.Rectangle recta;
            
            object o = null;

            if (imge.IsNull() && !this.isInitialized)
            {
                this.isInitialized = true; //optimization for know if already try to load image

                o = string.IsNullOrEmpty(this.Data) ? string.Empty : (object)e.GetObjectData(this.Data.Split(','));

                if(!o.IsNull() && o.GetType() == typeof(object[])){
                    if (((object[])o).Length > 0)
                        o = ((object[])o)[0];
                    else
                        o = null;
                }

                if (o.IsNull())
                    o = this.GetImageFromBase64(this.Data);

                if (o.IsNull())
                    return;

                if (o.GetType() != typeof(byte[]))
                    return;

                imge = PdfSharp.Drawing.XImage.FromGdiPlusImage(System.Drawing.Image.FromStream(
                    new System.IO.MemoryStream(o as byte[])));
            }

            if (imge.IsNull())
                return;

            recta = new System.Drawing.Rectangle(this.Position, this.Size);
            e.Graphic.DrawImage(imge, recta);
        }

        public override void NewPage()
        {
            //nothing special...
        }

        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            base.LoadFromNode(root);

            this.Data = root.Attributes["data"] == null ? string.Empty : root.Attributes["data"].Value.ToString();
            this.Position = new System.Drawing.Point(
                root.Attributes["x"] == null ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"] == null ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Size = new System.Drawing.Size(
                root.Attributes["width"] == null ? 100 : root.Attributes["width"].Value.ToInt32(),
                root.Attributes["height"] == null ? 100 : root.Attributes["height"].Value.ToInt32());

            if (this.Size.Width < 1)
                this.Size = new System.Drawing.Size(1, this.Size.Height);

            if (this.Size.Height < 1)
                this.Size = new System.Drawing.Size(this.Size.Width, 1);
        }

        private byte[] GetImageFromBase64(string source)
        {
            byte[] b = null;
            System.IO.MemoryStream stream;
            System.Drawing.Bitmap map;

            try
            {
                b = Convert.FromBase64String(source);
                using (stream = new System.IO.MemoryStream())
                {
                    stream.Write(b, 0, b.Length);
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                    using (map = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(stream))
                    {
                        stream = null; //don't use stream.Dispose() will delete the map memory hash table
                        stream = new System.IO.MemoryStream();
                        map.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                    b = stream.ToArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }

            return b;
        }
    }
}
