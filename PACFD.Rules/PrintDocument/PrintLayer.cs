﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules.PrintDocument
{
    /// <summary>
    /// Container of print controls.
    /// </summary>
    public class PrintLayer : PrintControlBase, IPrintLayer
    {
        private int yCoordenate;
        private int dataIndex;

        /// <summary>
        /// The print controls collection
        /// </summary>
        public PrintControlCollection PrintControls { get; private set; }
        /// <summary>
        /// Size of the layer.
        /// </summary>
        public System.Drawing.Point Size { get; set; }
        /// <summary>
        /// Get a value indicating if the layer can print.
        /// </summary>
        public bool CanPrint { get; protected set; }
        /// <summary>
        /// Get a boolean value indicating if the layer need print.
        /// </summary>
        public bool NeedPrint { get; protected set; }
        /// <summary>
        /// Position of the layer.
        /// </summary>
        public override System.Drawing.Point Position { get; set; }
        //public LayerRow RowStyle { get; set; }
        /// <summary>
        /// Get a boolean value indicating if the layer is a list layer.
        /// </summary>
        public Boolean IsList { get; private set; }
        public string TableName { get; private set; }
        /// <summary>
        /// Get or set an integer value indicating the spacing of the rows.
        /// </summary>
        public int RowSpacing { get; set; }
        /// <summary>
        /// Indicate index from concept in page
        /// </summary>
        public int idx { get; set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        

        public PrintLayer()
        {
            this.PrintControls = new PrintControlCollection();
            this.Size = new System.Drawing.Point(120, 120);
            this.yCoordenate = 0;
            this.dataIndex = -1;
            this.NeedPrint = true;
            this.CanPrint = true;
            this.Font = new PdfSharp.Drawing.XFont("Arial", 8);
            //this.isInitialize = false;

        }
        /// <summary>
        /// Draw the layer and his childs.
        /// </summary>
        /// <param name="e">Arguments needed to print the layer and the chinlds.</param>
        public override void Draw(PrintArguments e)
        {
            int h = 0;
            PrintArguments e2;

            System.Diagnostics.Debug.WriteLine(string.Format("writting document layer : {0}", this.NeedPrint));

            if (!this.NeedPrint)
                return;

            System.Diagnostics.Debug.WriteLine(string.Format("writting document layer... table: " + this.TableName));

            this.NeedPrint = false;

            if (this.IsList)
                this.dataIndex++;

            e2 = new PrintArguments(e.Page, e.Graphic, e.Billing, e.DBDefinition,
                new PrintArguments.ZoneRegion(
                    this.Position.X, this.Position.Y + this.yCoordenate,
                    this.Size.X, this.Size.Y - this.yCoordenate), e.IsDemo);

            e2.UseIndex = dataIndex;
            h = this.GetMaxTextHeight(e2) + (this.RowSpacing > 0 ? this.RowSpacing : 0);

            if (this.IsList && !string.IsNullOrEmpty(this.TableName))
            {
                if (!this.NeedPrint)
                    this.NeedPrint = e2.TableRowCount(this.TableName) - 1 > dataIndex ? true : false;

                this.yCoordenate += h;
                this.CanPrint = true;

                //if (this.Position.Y + this.yCoordenate > this.Size.Y) // the variable... this.Size.Y = this.Position.Y + height
                  //  this.CanPrint = false;

                if (!this.CanPrint&&idx>0)
                {
                    this.dataIndex--;
                    this.NeedPrint = true;
                    e2 = null;
                    return;
                }
            }

            foreach (PrintControlBase p in this.PrintControls)
            {
                if (this.IsList)
                    e2.UseIndex = this.dataIndex;

                p.Draw(e2);
            }

            e2 = null;
        }
        /// <summary>
        /// Get the max height used to print a child element.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>Return an integer value.</returns>
        private int GetMaxTextHeight(PrintArguments e)
        {
            int i = 0, x = 0, h = 0;
            PrintDocument.PrintControl p;

            foreach (PrintDocument.PrintControlBase c in this.PrintControls)
            {
                p = c as PrintControl;

                if (p.IsNull())
                    continue;

                h = p.Font.Height;
                x = p.GetHeightLinesToUse(e);
                h *= (x > 0 ? x : 1);
                i = i > h ? i : h;
            }

            return i;
        }
        /// <summary>
        /// Method called when a new page is needed.
        /// </summary>
        public override void NewPage()
        {
            this.yCoordenate = 0;
            this.CanPrint = true;
            this.NeedPrint = true;

            foreach (PrintControlBase item in this.PrintControls)
                item.NewPage();
        }
        /// <summary>
        /// Load the layer from a XML node.
        /// </summary>
        /// <param name="root"></param>
        public override void LoadFromNode(System.Xml.XmlNode root)
        {
            System.Xml.XmlNode n;
            PrintControlBase l = null;

            base.LoadFromNode(root);

            this.Position = new System.Drawing.Point(
                root.Attributes["x"].IsNull() ? 0 : root.Attributes["x"].Value.ToInt32(),
                root.Attributes["y"].IsNull() ? 0 : root.Attributes["y"].Value.ToInt32());

            this.Size = new System.Drawing.Point(
                root.Attributes["width"].IsNull() ? 0 : root.Attributes["width"].Value.ToInt32(),
                root.Attributes["height"].IsNull() ? 0 : root.Attributes["height"].Value.ToInt32());

            this.IsList = root.Attributes["list"].IsNull() ? false : root.Attributes["list"].Value.ToBoolean();
            this.TableName = root.Attributes["table"].IsNull() ? "" : root.Attributes["table"].Value;

            this.RowSpacing = root.Attributes["row-spacing"].IsNull() ? 0 : root.Attributes["row-spacing"].Value.ToInt32();

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                n = root.ChildNodes[i];

                if (n.Name.ToLower() == typeof(PrintLabel).Name.ToLower())
                    l = new PrintLabel();
                else if (n.Name.ToLower() == typeof(PrintText).Name.ToLower())
                    l = new PrintText();
                else if (n.Name.ToLower() == typeof(PrintTools).Name.ToLower())
                    l = new PrintTools();
                else if (n.Name.ToLower() == typeof(PrintShape).Name.ToLower())
                    l = new PrintShape();
                else if (n.Name.ToLower() == typeof(PrintTotals).Name.ToLower())
                    l = new PrintTotals();
                else if (n.Name.ToLower() == typeof(PrintImage).Name.ToLower())
                    l = new PrintImage();
                else if (n.Name.ToLower() == typeof(PrintFiscalRegime).Name.ToLower())
                    l = new PrintFiscalRegime();
                else if (n.Name.ToLower() == typeof(PrintPaymentForm).Name.ToLower())
                    l = new PrintPaymentForm();

                if (!l.IsNull())
                {
                    l.LoadFromNode(n);
                    this.PrintControls.Add(l);
                    l = null;
                }
            }
        }
    }

    /// <summary>
    /// Layer base interface.
    /// </summary>
    public interface IPrintLayer
    {
        /// <summary>
        /// Print controls collection.
        /// </summary>
        PrintControlCollection PrintControls { get; }
        /// <summary>
        /// Size of the layer.
        /// </summary>
        System.Drawing.Point Size { get; set; }
        //LayerRow RowStyle { get; set; }
        /// <summary>
        /// Get a boolean value indicating if the layer is a list layer.
        /// </summary>
        Boolean IsList { get; }
        string TableName { get; }
        /// <summary>
        /// Get or set the row spaceing of the rows.
        /// </summary>
        int RowSpacing { get; set; }
        /// <summary>
        /// Get a boolean value indicating if the layer can print.
        /// </summary>
        bool CanPrint { get; }
        /// <summary>
        /// Get a boolean value indicating if the layer need print.
        /// </summary>
        bool NeedPrint { get; }
    }
    /*
    public class LayerRow
    {
        public int Space { get; set; }
        public int AlternatingColor { get; set; }
    }*/
}
