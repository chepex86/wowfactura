﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using PACFD.Rules.BillingsExceptions;
using System.Web;
using Tool = PACFD.Common.Utilities;
using System.Security.Cryptography;
using System.Configuration;
using QRCoder;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class used as a layer ruler for Billings table.
    /// </summary>
    public partial class Billings
    {
        /// <summary> 
        /// Header of line for concept when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_CONCEPT = "C";
        /// <summary>
        /// Header of line for iva when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_IVA = "V";
        /// <summary>
        /// Header of line for move when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_MOVE = "M";


        public FiscalRegime FiscalRegimeTable { get { return new FiscalRegime(); } }

        /// <summary>
        /// Select all the billing from an external folio.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the bill.</param>
        /// <param name="folio">Folio to look for.</param>        
        public PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable SelectByExternalFolio(int billerid, string folio)
        {
            PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable
                table = new PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable();

            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.GetByExternalFolioTableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.GetByExternalFolioTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, folio);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.BillingsDataSet.BillingsDataTable table with the billing ID.
        /// </summary>
        /// <param name="id">Billing to look for.</param>
        /// <returns>Return a DataAccess.BillingsDataSet.BillingsDataTable table.</returns>
        public DataAccess.BillingsDataSet.BillingsDataTable SelectByID(int id)
        {
            DataAccess.BillingsDataSet.BillingsDataTable table;

            table = new PACFD.DataAccess.BillingsDataSet.BillingsDataTable();
            this.SelectByID(table, id);

            return table;
        }

        public DataAccess.BillingsDataSet.GetByBillingPaymentIdDataTable SelectByBillingPaymentId(int id)
        {
            DataAccess.BillingsDataSet.GetByBillingPaymentIdDataTable table;

            table = new PACFD.DataAccess.BillingsDataSet.GetByBillingPaymentIdDataTable();
            this.SelectByBillingPaymentId(table, id);

            return table;
        }

        public bool SelectByBillingPaymentId(DataAccess.BillingsDataSet.GetByBillingPaymentIdDataTable table, int id)
        {
            DataAccess.BillingsDataSetTableAdapters.GetByBillingPaymentIdTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.GetByBillingPaymentIdTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
            #if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
            #endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        public DataAccess.BillingsDataSet.GetSerialsByBillerIDDataTable GetSerialsByBillerID(int BillerID)
        {
            DataAccess.BillingsDataSet.GetSerialsByBillerIDDataTable table = new PACFD.DataAccess.BillingsDataSet.GetSerialsByBillerIDDataTable();
            this.GetSerialsByBillerID(table, BillerID);

            return table;
        }
        public bool GetSerialsByBillerID(DataAccess.BillingsDataSet.GetSerialsByBillerIDDataTable ta, int BillerID)
        {
            DataAccess.BillingsDataSetTableAdapters.GetSerialsByBillerIDTableAdapter adapt;

            try
            {
                using (adapt = new PACFD.DataAccess.BillingsDataSetTableAdapters.GetSerialsByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapt);
                    adapt.Fill(ta, BillerID);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(DataAccess.BillingsDataSet.BillingsDataTable table, int id)
        {
            DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataAccess.BillingsDataSet.GetBillingsByReceptorIDDataTable SelectByReceptorID(int id)
        {
            DataAccess.BillingsDataSet.GetBillingsByReceptorIDDataTable table = new PACFD.DataAccess.BillingsDataSet.GetBillingsByReceptorIDDataTable();
            DataAccess.BillingsDataSetTableAdapters.GetBillingsByReceptorIDTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.GetBillingsByReceptorIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return table;
            }

            return table;
        }
        /// <summary>
        /// Update, delete or modify a billing. If a Billing is inserted and include serial ID, the serial is marked as used. 
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillingsDataSet dataset)
        {
            DataAccess.Billings_DA adapter = new PACFD.DataAccess.Billings_DA();

            try
            {
                //Tool.RemoveOwnerSqlCommand
                //is used inside the update method.
                adapter.Update(dataset);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }
        /// <summary>
        /// Update, modify, insert or delete only the billing table, if a more complicate operation
        /// is needed use the overload update method that resive a DataAccess.BillingsDataSet.
        /// </summary>
        /// <param name="table">Update, modify, insert or delete only the billing table</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillingsDataSet.BillingsDataTable table)
        {
            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        public bool SetPaymentInfo(DataAccess.BillingsDataSet.BillingsDataTable table, int billingPaymentId)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;
            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetPaymentInfo(table[0].BillingID, table[0].Partiality, table[0].ImpSaldoAnt, table[0].ImpSaldoInsoluto, table[0].ImpPagado, billingPaymentId);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the paid status of a billing.
        /// </summary>
        /// <param name="billingid">Billing ID to change.</param>
        /// <param name="paid">State of the billing.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetPaidTo(int billingid, bool paid)
        {
            if (billingid < 1)
                return false;

            try
            {
                using (DataAccess.BillingsDataSet.BillingsDataTable table = new PACFD.DataAccess.BillingsDataSet.BillingsDataTable())
                {
                    using (DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter adapter =
                        new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);

                        if (adapter.Fill(table, billingid) < 1)
                            return false;

                        table[0].BeginEdit();
                        table[0].IsPaid = paid;
                        table[0].EndEdit();
                        table[0].AcceptChanges();
                        table[0].SetModified();
                        adapter.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.BillingsDataSet.GetBillings_BillingsReceptor_ByBillerIDDataTable table 
        /// with the Billings table and BillingsReceptors table filtered by biller ID
        /// </summary>
        /// <param name="id">Biller ID used to filter.</param>
        /// <returns>
        /// Return a PACFD.DataAccess.BillingsDataSet.GetBillings_BillingsReceptor_ByBillerIDDataTable table
        /// </returns>
        public PACFD.DataAccess.BillingsDataSet.GetBillings_BillingsReceptor_ByBillerIDDataTable SelectBillingAndBillingReceptorByBillerID(int id)
        {
            PACFD.DataAccess.BillingsDataSet.GetBillings_BillingsReceptor_ByBillerIDDataTable table;
            PACFD.DataAccess.BillingsDataSetTableAdapters.GetBillings_BillingsReceptor_ByBillerIDTableAdapter adapter;

            table = new PACFD.DataAccess.BillingsDataSet.GetBillings_BillingsReceptor_ByBillerIDDataTable();

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.GetBillings_BillingsReceptor_ByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Set the active state of a billing.
        /// If billing is a CFDI then is send to PAC's web service (send to pac is disabled at the moment)
        /// , if the cancellation is success the billing is cancel else throw an exception.
        /// </summary>
        /// <param name="billingid">Billing ID to set active.</param>
        /// <param name="active">State of the active.</param>
        /// <exception cref="PACFD.Rules.BillingsExceptions.BillingCancelError">If CFDI and the pac can't cancel the billing then throw cancel exception.</exception>
        /// <returns>If Success return true else false.</returns>
        public bool SetActive(int billingid, bool active)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;
            PACFD.Rules.ElectronicBillingType electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate;
            ErrorManager error;
            bool cancel = false;

            using (PACFD.DataAccess.BillingsDataSet fullbilling = this.GetFullBilling(billingid))
            {
                if (fullbilling.Billings.Count < 1)
                    return false;

                switch (fullbilling.Billings[0].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 2: electronictype = (PACFD.Rules.ElectronicBillingType)fullbilling.Billings[0].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }

                if (electronictype == ElectronicBillingType.Indeterminate)
                    return false;
                //electronictype == ElectronicBillingType.CFDI || electronictype == ElectronicBillingType.CFDI3_2 ||
                if ( electronictype == ElectronicBillingType.CFDI3_3 || electronictype == ElectronicBillingType.CFDI4_0)
                {
                    error = new ErrorManager();
                    bool NotSealed = fullbilling.Billings[0]["SealSat"] == DBNull.Value;

                    if ( NotSealed || CancellationsCFDI.SendToCancellation(fullbilling, ref error))
                    {
                        cancel = true;
                        Common.PacSealErrorTypes currentError = GetCurrentErrorPac(billingid);

                        if (currentError == PacSealErrorTypes.CancelError)
                            SetErrorPAC(billingid, PacSealErrorTypes.CancelNotMailSend);
                        else
                            SetErrorPAC(billingid, PacSealErrorTypes.None);
                    }
                    else if (SetErrorPAC(billingid, PacSealErrorTypes.CancelError))
                    {
                        cancel = true;
                    }
                    else
                    {
                        cancel = false;
                    }
                }
                else
                {
                    cancel = true;
                }
            }

            if (!cancel) //if electronic type is cfdi and an error is detected, don't send to cancel.
                return false;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetActive(billingid, active);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Set the date of a billing
        /// </summary>
        /// <param name="billingid">Billing ID to be modify</param>
        /// <param name="newdate">New date to be set.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetDateTo(int billingid, DateTime newdate)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetDateTo(billingid, newdate);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// SET BILLING PAYMENT
        /// </summary>
        /// <param name="billingid">Billing ID to be modify</param>
        /// <param name="newdate">New date to be set.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetBillingPayment(int billingpaymentid, int id)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetBillingPayment(id, billingpaymentid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        public bool SetBillingPaymentNULL(int billingpaymentid)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetBillingPaymentNULL(billingpaymentid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #region ReSeal

        public bool ReSealCFD(int billingID)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                if (ds != null && ds.Billings.Count > 0 &&
                    (
                      ds.Billings[0].ElectronicBillingType == (int)ElectronicBillingType.CFD ||
                      ds.Billings[0].ElectronicBillingType == (int)ElectronicBillingType.CFD2_2
                    )
                   )
                {

                    PACFD.DataAccess.BillingsDataSet.BillingsRow billing = ds.Billings[0];
                    string orinalString = GenerateOrinalString(ds);
                    string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);
                    if (orinalString.Trim() != "" && seal != "")
                    {
                        SetReSealCFD(billingID, orinalString, seal);
                        return true;
                    }


                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                         + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }

        public bool ReSealCFD()
        {
            try
            {
                DataAccess.BillingsDataSet data = new PACFD.DataAccess.BillingsDataSet();
                DataAccess.BillingsDataSetTableAdapters.GetForReSealCFDTableAdapter adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.GetForReSealCFDTableAdapter();
                adapter.Fill(data.GetForReSealCFD);
                if (data != null && data.GetForReSealCFD.Count > 0)
                {
                    foreach (var dr in data.GetForReSealCFD)
                    {
                        PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(dr.BillingID);
                        if (ds != null && ds.Billings.Count > 0)
                        {

                            PACFD.DataAccess.BillingsDataSet.BillingsRow billing = ds.Billings[0];
                            string orinalString = GenerateOrinalString(ds);
                            string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);
                            if (orinalString.Trim() != "" && seal != "")
                            {
                                SetReSealCFD(dr.BillingID, orinalString, seal);
                            }
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                         + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }

        private bool SetReSealCFD(int billingID, string originalString, string Seal)
        {
            try
            {

                using (DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query =
                        new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_ReSealCFD(billingID, originalString, Seal);
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                             + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }
        #endregion
        /// <summary>
        /// Set an error seal code to a billing.
        /// </summary>
        /// <param name="billingid">Billing ID to set.</param>
        /// <param name="pacerrortype">Error seal code to set.</param>
        /// <returns>If success true else false.</returns>
        public bool SetErrorPAC(int billingid, Common.PacSealErrorTypes pacerrortype)
        {
            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillings_SetErrorPAC(billingid, (int)pacerrortype);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                       + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public Common.PacSealErrorTypes GetCurrentErrorPac(int billingid)
        {
            try
            {
                DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable table =
                 SelectCfdiByErrorPAC(billingid, PacSealErrorTypes.Unknown);
                if (table != null && table.Count > 0)
                {
                    if (!table[0].IsErrorPACNull())
                    {
                        return (PacSealErrorTypes)table[0].ErrorPAC;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                       + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return PacSealErrorTypes.Unknown;
        }
        /// <summary>
        /// Get a DataAccess.BillingsDataSet data set with all the tables used by a billing.
        /// If you only want Billings table without all the child tables, use "SelectByID" method instead.
        /// </summary>
        /// <param name="billingid">Billing ID used to fill the tables.</param>
        /// <returns>Return a DataAccess.BillingsDataSet data set.</returns>
        public DataAccess.BillingsDataSet GetFullBilling(int billingID)
        {
            DataAccess.Billings_DA da;
            try
            {
                da = new PACFD.DataAccess.Billings_DA();
                return da.GetFullBilling(billingID);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            finally
            {
                da = null;
            }
            return null;
        }
        /// <summary>
        /// Get only the necesaries ID of the billing table that have an error on seal and is active
        /// </summary>
        /// <param name="billerid">Optional null biller ID.</param>
        /// <param name="errorsealing">Optional null error seal code.</param>
        /// <returns>DataAccess.BillingsDataSet.GetCFDIBySealingErrorWSDataTable</returns>
        public DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable SelectCfdiByErrorPAC(int? billerid, PacSealErrorTypes pacsealerrortypes)
        {
            DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable table = new PACFD.DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable();

            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.GetCFDIByPACErrorTableAdapter adapter = new
                    PACFD.DataAccess.BillingsDataSetTableAdapters.GetCFDIByPACErrorTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, pacsealerrortypes == PacSealErrorTypes.None ? null : (int?)pacsealerrortypes);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Search a billing.
        /// </summary>
        /// <param name="billerid">Biller ID.</param>
        /// <param name="receptorname">Optional name string of the receptor used as filter.</param>
        /// <param name="serial">Optional serial string used as filter.</param>
        /// <param name="folio">Optional folio number used as filter.</param>
        /// <param name="date">Optional date - time used as filter.</param>
        /// <param name="prebilling">Optional prebilling boolean used as filter.</param>
        /// <param name="ispaid">Optional ispaid boolean used as filter.</param>
        /// <returns>Return a table with all the rows founded.</returns>
        public PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable Search(SearchPetitionFromFlag comesfrom, int billerid, string receptorname, string serial,
            string folio, DateTime? date, bool? prebilling, bool? ispaid, string uuid, int? branchID, string externalfolio, int? billingid, string paymentMethod)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable
                table = new PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable();

            if (comesfrom == SearchPetitionFromFlag.None)
                return table;

            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsSearchTableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsSearchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, (int?)comesfrom, billerid, receptorname, folio, serial, date, prebilling, ispaid, uuid, branchID, externalfolio, billingid, paymentMethod);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Generate original string for a billing.
        /// </summary>
        /// <param name="ds">Billing data set</param>
        /// <param name="pathXlst"></param>
        /// <returns></returns>
        public string GenerateOrinalString(DataAccess.BillingsDataSet ds)
        {
            try
            {
                string xml = GetXml(ds);
                string xlst = "";
                switch ((ElectronicBillingType)ds.Billings[0].ElectronicBillingType)
                {
                    case ElectronicBillingType.CFD:
                        xlst = Properties.Resources.OriginalString_v_2_0;
                        break;
                    case ElectronicBillingType.CFD2_2:
                        xlst = Properties.Resources.OriginalString_v_2_2;
                        break;
                    case ElectronicBillingType.CFDI:
                        xlst = Properties.Resources.OriginalString_v_3_0;
                        break;
                    case ElectronicBillingType.CFDI3_2:
                        xlst = Properties.Resources.OriginalString_v_3_2;
                        break;
                    case ElectronicBillingType.CFDI3_3:
                        xlst = Properties.Resources.OriginalString_v_3_3;
                        break;
                    case ElectronicBillingType.CFDI4_0:
                        xlst = Properties.Resources.OriginalString_v_4_0;
                        break;
                }

                string result = string.Empty;
                using (var ms = new MemoryStream())
                {
                    var xls = new XslCompiledTransform();
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream me = new MemoryStream();
                    byte[] ba = encoding.GetBytes(xlst);
                    me.Write(ba, 0, ba.Length);
                    me.Seek(0, SeekOrigin.Begin);
                    xls.Load(new XmlTextReader(me));
                    XPathDocument xd = new XPathDocument(new XmlTextReader(xml, XmlNodeType.Element, null));
                    var xw = XmlWriter.Create(ms, xls.OutputSettings);
                    xls.Transform(xd, xw);
                    xw.Close();
                    ms.Position = 0;
                    using (var sr = new StreamReader(ms))
                    {
                        result = sr.ReadToEnd().Trim();
                        if (result != string.Empty)
                        {
                            string[] split = result.Split(new Char[] { '\r', '\n' });
                            result = "";
                            foreach (string c in split)
                            {
                                if (c.Trim() != string.Empty)
                                    result += c.Trim();

                            }

                        }
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return string.Empty;
        }
        /// <summary>
        /// Seal a billing.
        /// </summary>
        /// <param name="billingID">Billing ID to be updated.</param>
        /// <param name="pathXlst"></param>
        /// <param name="pathTemp"></param>
        /// <param name="pathOpenSSL"></param>
        /// <returns>If success return true else false.</returns>
        public bool SealCFD(int billingID, ref ErrorManager error)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                return SealCFD(ds, ref error);
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[SealCFD]", ex);
            }
            return true;
        }
        /// <summary>
        /// Seal a billing dataset.
        /// </summary>
        /// <param name="ds">Billing data set with the billing to be updated.</param>
        /// <param name="pathXlst"></param>
        /// <param name="pathTemp"></param>
        /// <param name="pathOpenSSL"></param>
        /// <returns>If succes return true else false.</returns>
        public bool SealCFD(PACFD.DataAccess.BillingsDataSet ds, ref ErrorManager error)
        {
            try
            {
                if (ds == null)
                    return false;

                PACFD.DataAccess.BillingsDataSet.BillingsBillersRow biller = ds.BillingsBillers[0];
                PACFD.DataAccess.BillingsDataSet.BillingsRow billing = ds.Billings[0];

                string orinalString = GenerateOrinalString(ds);
                string filio = string.Format("{0}-{1}", billing.Serial, billing.Folio);
                string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);

                if (string.IsNullOrEmpty(seal))
                    return false;

                PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter q = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter();
                Tool.RemoveOwnerSqlCommand(q);

                ds.Billings[0].Seal = seal;
                ds.Billings[0].OriginalString = orinalString;
                if ((ElectronicBillingType)ds.Billings[0].ElectronicBillingType == ElectronicBillingType.CFDI4_0)
                {
                    if (StampingCFDI.SendToStamping(ds, ref error))
                    {
                        q.spSealCFDI(ds.Billings[0].BillingID
                                     , orinalString
                                     , seal
                                     , ds.Billings[0].UUID
                                     , ds.Billings[0].DateStamped
                                     , ds.Billings[0].SealSat
                                     , ds.Billings[0].CertificateNumberSat
                                     , ds.Billings[0].OrignalStringSat
                                     , ds.Billings[0].BatchIdSat
                                     , ds.Billings[0].StatusSat
                                     , ds.Billings[0].VersionSat
                                     , GetCFDIArray(ds)
                                     , ds.Billings[0].RfcProvCertic
                                     , ds.Billings[0].SelloCFD);
                        return true;

                    }
                    else
                    {
                        this.SetErrorPAC(ds.Billings[0].BillingID, PacSealErrorTypes.SealError);

                        try
                        {
                            //aqui menter el correo

                            PACFD.Rules.Mail.MailSender sender = new PACFD.Rules.Mail.MailSender();
                            String path = HttpContext.Current.Request.MapPath("~/Includes/Mail/EmisorAdded.es.xml");
                            String xmlParse = ds.GetXml().Replace("<", "&#60;").Replace(">", "&#62;");

                            sender.Message = PACFD.Rules.Mail.MailSenderHelper.GetMessageFromXML(path, "005");

                            sender.Parameters.Add("lblError", error.Error.Message);
                            sender.Parameters.Add("lblXML", xmlParse);
                            sender.Send();
                        }
                        catch (Exception ex)
                        {
                            LogManager.WriteCustom("[SealCFD -SendMail]", ex);

                        }
                    }
                }
                else
                {
                    q.spSealCFD(ds.Billings[0].BillingID, orinalString, seal);
                    return true;

                }
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[SealCFD]", ex);
                return false;
            }

            return false;
        }

        public bool HandSealCFD(int billingID, string UUID, string dateStamped, string sealSat, string certificateNumberSat, string orignalStringSat, string versionSat)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                if (ds != null)
                {
                    PACFD.DataAccess.BillingsDataSet.BillingsRow billing = ds.Billings[0];
                    if (string.IsNullOrEmpty(UUID) || string.IsNullOrEmpty(dateStamped) || string.IsNullOrEmpty(sealSat) ||
                        string.IsNullOrEmpty(certificateNumberSat) || string.IsNullOrEmpty(orignalStringSat) || string.IsNullOrEmpty(versionSat))
                    {
                        return false;
                    }
                    if ((ElectronicBillingType)ds.Billings[0].ElectronicBillingType == ElectronicBillingType.CFDI)
                    {
                        string orinalString = GenerateOrinalString(ds);
                        string filio = string.Format("{0}-{1}", billing.Serial, billing.Folio);
                        string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);

                        if (string.IsNullOrEmpty(seal))
                            return false;

                        PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter q = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter();
                        Tool.RemoveOwnerSqlCommand(q);
                        DateTime DateStamped = DateTime.Parse(dateStamped);
                        q.spSealCFDI(ds.Billings[0].BillingID
                                      , orinalString
                                      , seal
                                      , UUID
                                      , DateStamped
                                      , sealSat
                                      , certificateNumberSat
                                      , orignalStringSat
                                      , "-1"
                                      , "7"
                                      , versionSat
                                      , GetCFDIArray(ds)
                                      , "sad"
                                      , "huehue");
                        return true;

                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[HandSealCFD]", ex);

            }
            return false;
        }

        public static string GenerateSeal(string orinalString, int billerID, string CertificateNumber, DateTime date)
        {
            string path = string.Empty;
            string result = string.Empty;
            DateTime d2011 = new DateTime(2011, 01, 01);
            DateTime d2016 = new DateTime(2017, 01, 10);
            try
            {
                DigitalCertificates cer = new DigitalCertificates();
                
                string passPrivateKey = string.Empty;
                //FALTA VALIDAR LA FECHA DEL CETIFICADO
                byte[] privateKey = cer.GetPrivateKey(billerID, CertificateNumber, ref passPrivateKey);
                if (privateKey != null)
                {

                    System.Security.SecureString pass = new System.Security.SecureString();
                    for (int i = 0; i < passPrivateKey.Length; i++)
                    {
                        pass.AppendChar(passPrivateKey.ToCharArray()[i]);
                    }
                    System.Security.Cryptography.RSACryptoServiceProvider key = OpenSSL.DecodeEncryptedPrivateKeyInfo(privateKey, pass);

                    object hash = null;
                    if (date < d2011)
                        hash = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    else if (date < d2016)
                        hash = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                    else
                        hash = CryptoConfig.CreateFromName("SHA256");

                    Byte[] signedBytes = key.SignData(Encoding.UTF8.GetBytes(orinalString), hash);
                    string sello = Convert.ToBase64String(signedBytes);

                    return sello;
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GenerateSeal " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return string.Empty;
        }

        private static void DeleteFile(string file)
        {
            try
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] DeleteFile " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        private static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    File.Delete(path);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] DeleteFile " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        private static string GetMD5(string cadenaOriginal)
        {
            try
            {
                byte[] CadenaUTF8;
                byte[] tmpHash;

                CadenaUTF8 = Encoding.UTF8.GetBytes(cadenaOriginal);
                tmpHash = new System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(CadenaUTF8);
                StringBuilder sOutput = new StringBuilder(tmpHash.Length);
                for (int i = 0; i < tmpHash.Length; i++)
                {
                    sOutput.Append(tmpHash[i].ToString("x2"));
                }
                return sOutput.ToString();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetMD5 " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            return string.Empty;

        }
        public byte[] GetXml(int billingID, ref string fileName)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                if (ds != null)
                {
                    fileName = string.Format("{0}_{1}-{2}.xml", ds.BillingsBillers[0].BillerRFC, ds.Billings[0].Serial, ds.Billings[0].Folio);

                    string xml = GetXml(ds);
                    //using (var ms = new MemoryStream())
                    //{
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    //MemoryStream me = new MemoryStream();
                    byte[] ba = encoding.GetBytes(xml);

                    return ba;
                    //}
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetXml " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return null;
        }
        /// <summary>
        /// Get a billing from a dataset.
        /// </summary>
        /// <param name="ds">Dataset with all billing information.</param>
        /// <param name="pathXlst">Transformation path used to convert the dataset.</param>
        /// <returns>If success return astring with values else null.</returns>
        public string GetXml(PACFD.DataAccess.BillingsDataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    string xmlOrigen = ds.GetXml();
                    bool isCfd = false;
                    bool format = false;
                    if (xmlOrigen.Trim() != string.Empty)
                    {
                        using (var ms = new MemoryStream())
                        {
                            var xls = new XslCompiledTransform();
                            //Obtener la hoja de transformacion desde los recursos
                            string resourceXlst = Properties.Resources.BillingsDataSetToCFD;
                            switch ((PACFD.Rules.ElectronicBillingType)ds.Billings[0].ElectronicBillingType)
                            {
                                case ElectronicBillingType.CFD:
                                    {
                                        resourceXlst = Properties.Resources.BillingsDataSetToCFD;
                                        isCfd = true;
                                        if (ds.Billings.Count > 0)
                                        {
                                            if (!ds.Billings[0].IsEtcNull() && ds.Billings[0].Etc == 0)
                                            {
                                                format = true;
                                            }
                                        }
                                    }
                                    break;
                                case ElectronicBillingType.CFD2_2:
                                    {
                                        resourceXlst = Properties.Resources.BillingsDataSetToCFD_2_2;
                                    }
                                    break;
                                case ElectronicBillingType.CFDI:
                                    {
                                        resourceXlst = Properties.Resources.BillingsDataSetToCFDI;
                                    }
                                    break;
                                case ElectronicBillingType.CFDI3_2:
                                    {
                                        resourceXlst = Properties.Resources.BillingsDataSetToCFDI_3_2;
                                    }
                                    break;
                                case ElectronicBillingType.CFDI3_3:
                                    {
                                        resourceXlst = Properties.Resources.CfdDataSetToCFD33;
                                        break;
                                    }
                                case ElectronicBillingType.CFDI4_0:
                                    {
                                        resourceXlst = Properties.Resources.CfdDataSetToCFD4_0;
                                        break;
                                    }
                                case ElectronicBillingType.CBB:
                                    {
                                        resourceXlst = Properties.Resources.BillingsDataSetToCBB;
                                    }
                                    break;
                            }

                            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                            MemoryStream me = new MemoryStream();
                            byte[] ba = encoding.GetBytes(resourceXlst.Trim());
                            me.Write(ba, 0, ba.Length);
                            me.Seek(0, SeekOrigin.Begin);
                            xls.Load(new XmlTextReader(me));


                            XPathDocument xd = new XPathDocument(new XmlTextReader(xmlOrigen, XmlNodeType.Element, null));
                            var xw = XmlWriter.Create(ms, xls.OutputSettings);
                            xls.Transform(xd, xw);
                            xw.Close();
                            ms.Position = 0;
                            string result = "";
                            using (var sr = new StreamReader(ms))
                            {
                                result = sr.ReadToEnd().Trim();
                            }
                            #region caso especial corregir formateo

                            if (isCfd && format)
                            {
                                FormatingCFD(ref result);
                            }
                            #endregion

                            return result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetXml " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            return null;
        }

        private void FormatingCFD(ref string xml)
        {
            string auxXml = xml;
            try
            {
                if (xml != string.Empty)
                {
                    bool haveTasa = false;
                    int idx1 = -1;
                    int idx2 = -1;
                    int idx3 = -1;
                    int len = -1;
                    string tasa = "";

                    idx1 = xml.IndexOf("<Traslados>");
                    if (idx1 != -1)
                    {
                        do
                        {
                            idx2 = xml.IndexOf("tasa=\"", idx1);
                            if (idx2 != -1)
                            {
                                idx2 += "tasa=\"".Length;
                                idx3 = xml.IndexOf("\"", idx2);
                                if (idx3 != -1)
                                {
                                    len = idx3 - idx2;
                                    tasa = xml.Substring(idx2, len);
                                    xml = xml.Remove(idx2, len);
                                    tasa = decimal.Parse(tasa).ToString("0");
                                    xml = xml.Insert(idx2, tasa);
                                    haveTasa = true;
                                    idx1 = idx2 + tasa.Length;
                                }
                            }
                            else
                            {
                                haveTasa = false;
                            }
                        } while (haveTasa);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                xml = auxXml;
            }

        }

        public void CalculationAmount(ref decimal unitPrice, decimal discountPercentage, bool appliesTax, PACFD.DataAccess.BillingsDataSet ds)
        {
            //variables
            //decimal realSubTotal = 0;
            //decimal subTotal = 0;
            decimal realAmount = 0;

            //decimal discount = 0;
            //decimal iva = 0;
            decimal ivaPercentageUsed = 0;
            //decimal ivaRetained = 0;
            decimal ivaRetainedPercentageUsed = 0;
            //decimal ieps = 0;
            decimal iepsPercentageUSed = 0;
            //decimal restTax = 0;
            //decimal restTaxRetained = 0;

            //Asignaciones
            PACFD.DataAccess.BillingsDataSet.BillingsRow drBilling = ds.Billings[0];
            string taxName = string.Empty;
            PACFD.DataAccess.BillingsDataSet.TransferTaxesRow drIva = null;
            PACFD.DataAccess.BillingsDataSet.TransferTaxesRow drIeps = null;
            PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow drIvaRetained = null;

            //Trasladado
            foreach (PACFD.DataAccess.BillingsDataSet.TransferTaxesRow dr in ds.TransferTaxes)
            {
                taxName = dr.Name.Trim().ToUpper();
                if (taxName == "IVA")
                {
                    drIva = dr;
                    ivaPercentageUsed = drIva.TaxRatePercentage;
                }
                else if (taxName == "IEPS")
                {
                    drIeps = dr;
                    iepsPercentageUSed = drIeps.TaxRatePercentage;
                }
            }
            //Retenido
            foreach (PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow dr in ds.DetainedTaxes)
            {
                taxName = dr.Name.Trim().ToUpper();
                if (taxName == "IVA")
                {
                    drIvaRetained = dr;
                    ivaRetainedPercentageUsed = drIvaRetained.TaxRatePercentage;
                }
            }

            realAmount = unitPrice;

            decimal percentageTaxes = 0;
            decimal taxes = 0;
            if (appliesTax)
            {
                if (ivaPercentageUsed > 0)
                {
                    percentageTaxes += ivaPercentageUsed * 0.01m;
                }
                if (ivaRetainedPercentageUsed > 0)
                {
                    percentageTaxes -= ivaRetainedPercentageUsed * 0.01m; ;
                }

                taxes = realAmount - (realAmount / (percentageTaxes + 1M));

            }

            unitPrice = realAmount - taxes;
            if (discountPercentage > 0)
            {
                discountPercentage = ((discountPercentage * 0.01M) * -1) + 1;
                if (discountPercentage > 0)
                    unitPrice = (unitPrice / discountPercentage);
            }
        }
        public void CalculationTaxesAndTotals(ref PACFD.DataAccess.BillingsDataSet ds)
        {
            if (ds != null)
            {
                if (ds.Billings.Count > 0 && ds.BillingsDetails.Count > 0)
                {
                    //variables
                    decimal realSubTotal = 0;
                    decimal subTotal = 0;
                    decimal realAmount = 0;
                    decimal discountPercentage = 0;
                    decimal discount = 0;
                    decimal iva = 0;
                    decimal ivaPercentageUsed = 0;
                    decimal ivaRetained = 0;
                    decimal ivaRetainedPercentageUsed = 0;
                    decimal ieps = 0;
                    decimal iepsPercentageUSed = 0;
                    decimal restTax = 0;
                    decimal restTaxRetained = 0;

                    bool appliesTax = false;

                    PACFD.DataAccess.BillingsDataSet.TransferTaxesRow drIva = null;
                    PACFD.DataAccess.BillingsDataSet.TransferTaxesRow drIeps = null;

                    PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow drIvaRetained = null;

                    //Asignaciones
                    PACFD.DataAccess.BillingsDataSet.BillingsRow drBilling = ds.Billings[0];
                    string taxName = string.Empty;

                    //Trasladado
                    foreach (PACFD.DataAccess.BillingsDataSet.TransferTaxesRow dr in ds.TransferTaxes)
                    {
                        taxName = dr.Name.Trim().ToUpper();
                        if (taxName == "IVA")
                        {
                            drIva = dr;
                            ivaPercentageUsed = drIva.TaxRatePercentage;
                        }
                        else if (taxName == "IEPS")
                        {
                            drIeps = dr;
                            iepsPercentageUSed = drIeps.TaxRatePercentage;
                        }
                    }
                    //Retenido
                    foreach (PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow dr in ds.DetainedTaxes)
                    {
                        taxName = dr.Name.Trim().ToUpper();
                        if (taxName == "IVA")
                        {
                            drIvaRetained = dr;
                            ivaRetainedPercentageUsed = drIvaRetained.TaxRatePercentage;
                        }
                    }
                    //Calculo de impuestos y subtotales                  
                    foreach (PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow drDetails in ds.BillingsDetails)
                    {
                        realAmount = drDetails.Amount;
                        appliesTax = drDetails.AppliesTax;
                        discountPercentage = 0;

                        if (!drDetails.IsDiscountPercentageNull())
                            discountPercentage = drDetails.DiscountPercentage;

                        if (discountPercentage > 0)
                        {
                            var dis = Math.Round((realAmount * (drDetails.DiscountPercentage * 0.01M)), 2);
                            discount += dis;
                            realAmount = (realAmount - dis);
                        }

                        if (appliesTax)
                        {
                            if (ivaPercentageUsed > 0)
                                iva += drDetails.Tax;
                                //iva += ((realAmount * (ivaPercentageUsed * 0.01M)) + (realAmount * (ieps * 0.01M)));

                            if (ivaRetainedPercentageUsed > 0)
                            {
                                if (ivaRetainedPercentageUsed == 0.6666667M)
                                    ivaRetained += Math.Round((iva * (ivaRetainedPercentageUsed/** 0.01M*/)), 2);
                                else
                                    ivaRetained += Math.Round((realAmount * (ivaRetainedPercentageUsed / 100)), 2);

                            }
                            //ivaRetained += (realAmount * (ivaRetainedPercentageUsed * 0.01M));

                        }

                        realSubTotal += realAmount;
                        subTotal += drDetails.Amount;
                    }

                    if (iepsPercentageUSed > 0)
                        ieps = Math.Round(realSubTotal * (iepsPercentageUSed * 0.01M),2);

                    if (drIva != null)
                        drIva.Import = iva;

                    if (drIeps != null)
                        drIeps.Import = ieps;

                    if (drIvaRetained != null)
                        drIvaRetained.Import = ivaRetained;

                    //Trasladado
                    foreach (PACFD.DataAccess.BillingsDataSet.TransferTaxesRow dr in ds.TransferTaxes)
                    {
                        taxName = dr.Name.Trim().ToUpper();
                        if (taxName != "IVA" && taxName != "IEPS")
                        {
                            if (dr.TaxRatePercentage > 0)
                            {
                                dr.Import = Math.Round(realSubTotal * (dr.TaxRatePercentage * 0.01M), 2);
                                restTax += dr.Import;
                            }
                        }
                    }
                    //Detained
                    foreach (PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow dr in ds.DetainedTaxes)
                    {
                        taxName = dr.Name.Trim().ToUpper();
                        if (taxName != "IVA")
                        {
                            if (dr.TaxRatePercentage > 0)
                            {
                                dr.Import = Math.Round(realSubTotal * (dr.TaxRatePercentage * 0.01M), 2);
                                restTaxRetained += dr.Import;
                            }
                        }
                    }

                    drBilling.Discount = discount;
                    drBilling.SubTotal = subTotal;
                    drBilling.Total = (subTotal + iva + ieps + restTax - discount - ivaRetained - restTaxRetained);



                    if (ds.Taxes.Count > 0)
                    {
                       // ds.Taxes[0].TotalBaseTransfer = 100;

                        ds.Taxes[0].TotalTransfer = (iva + ieps + restTax);
                        ds.Taxes[0].TotalDetained = (ivaRetained + restTaxRetained);
                    }
                }

            }
        }
        public byte[] GetCFDIArray(DataAccess.BillingsDataSet billings)
        {
            byte[] b = null;

            try
            {
                if (billings == null)
                    return null;

                if (billings.Billings.Count < 1)
                    return null;

                if (billings.BillingsBillers.Count < 1)
                    return null;

                if (billings.BillingsReceptors.Count < 1)
                    return null;

                String re = String.Empty;
                String rr = String.Empty;
                String tt = String.Empty;
                String id = String.Empty;
                String fmt = "0000000000.000000";

                if (billings.Billings[0].Total <= 0)
                    return null;
                if (billings.Billings[0].UUID == null || billings.Billings[0].UUID == String.Empty)
                    return null;
                if (billings.BillingsBillers[0].BillerRFC == null || billings.BillingsBillers[0].BillerRFC == String.Empty)
                    return null;
                if (billings.BillingsReceptors[0].ReceptorRFC == null || billings.BillingsReceptors[0].ReceptorRFC == String.Empty)
                    return null;

                re = billings.BillingsBillers[0].BillerRFC;
                rr = billings.BillingsReceptors[0].ReceptorRFC;
                tt = billings.Billings[0].Total.ToString("0.00");
                id = billings.Billings[0].UUID;
                string fe = "";
                if (billings.Billings[0].Seal.Length > 8)
                    fe = billings.Billings[0].Seal.Substring(billings.Billings[0].Seal.Length - 8);
                if (re.Length < 12 || re.Length > 13)
                    return null;
                if (rr.Length < 12 || rr.Length > 13)
                    return null;
                if (id.Length < 36 || id.Length > 36)
                    return null;

                

                String data = String.Empty;
                string url = ConfigurationManager.AppSettings["CFDIVerficatorURL"];
                data = String.Format(url + "id={0}&re={1}&rr={2}&tt={3}&fe={4}", id, re, rr, tt, fe);

                // NUEVO QR
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.L);
                PngByteQRCode qrCode = new PngByteQRCode(qrCodeData);
                byte[] qrCodeAsPngByteArr = qrCode.GetGraphic(20);
                b = qrCodeAsPngByteArr;
                

                /* Se actualizó la librería que hace el QR (QRCoder)
                QRCodeLib.QRCodeEncoder encoder = new QRCodeLib.QRCodeEncoder();

                encoder.QRCodeEncodeMode = QRCodeLib.QRCodeEncoder.ENCODE_MODE.BYTE;
                encoder.QRCodeScale = 3;
                encoder.QRCodeVersion = 8;
                encoder.QRCodeErrorCorrect = QRCodeLib.QRCodeEncoder.ERROR_CORRECTION.L;

                System.Drawing.Image img;

                img = encoder.Encode(data);

                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                b = ms.ToArray();
                */
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return b;

        }

        /// <summary>
        /// Enum used to determinated a search calls come from.
        /// </summary>
        public enum SearchPetitionFromFlag
        {
            None = 0,
            WebForm = 1,
            WebService = 2,
            OpenWebForm = 3,
            //WindowsForm = 4,
            //Ajaxs = 5,
        }

        public class FiscalRegime
        {
            public PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable GetByBillerID(int billerid)
            {
                PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable table =
                    new PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable();

                try
                {
                    using (PACFD.DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter adapter =
                        new PACFD.DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter())
                        adapter.Fill(table, billerid);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                           + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                    LogManager.WriteError(ex);
                }
                return table;
            }
        }
    }
}