﻿using PACFD.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool = PACFD.Common.Utilities;


namespace PACFD.Rules
{
    public class BillingsGeneralPeriod
    {

        public Boolean Update(DataAccess.BillingsDataSet.BillingGeneralPeriodDataTable table)
        {
            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.BillingGeneralPeriodTableAdapter adapter =
                    new DataAccess.BillingsDataSetTableAdapters.BillingGeneralPeriodTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public DataAccess.BillingsDataSet.BillingGeneralPeriodDataTable SelectByBillingID(int billingid)
        {
            DataAccess.BillingsDataSet.BillingGeneralPeriodDataTable table = new DataAccess.BillingsDataSet.BillingGeneralPeriodDataTable();

            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.BillingGeneralPeriodTableAdapter adapter =
                    new DataAccess.BillingsDataSetTableAdapters.BillingGeneralPeriodTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.FillByBillingID(table, billingid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }

        public bool Exist(int billingid)
        {
            using (DataAccess.BillingsDataSet.BillingGeneralPeriodDataTable table = this.SelectByBillingID(billingid))
            { return (table.Count > 0); }
        }
    }


}
