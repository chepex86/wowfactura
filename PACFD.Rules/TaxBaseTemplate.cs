﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class TaxBaseTemplate
    {
        /// <summary>
        /// Select a TaxBaseTemplate by ID.
        /// </summary>
        /// <param name="taxtemplateid">TaxBaseTemplate ID.</param>
        /// <returns>PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesDataTable</returns>
        public PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesDataTable SelectByID(int taxtemplateid)
        {
            PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesDataTable table =
                new PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesDataTable();

            try
            {
                using (PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtemplateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select tax base template for list display.
        /// </summary>
        /// <returns>PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable</returns>
        public PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable SelectForList()
        {
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable table =
                new PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable();

            try
            {
                using (PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.GetNameTaxTypesTableAdapter adapter =
                    new PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.GetNameTaxTypesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select a TaxBaseType that is in or out a list of taxes.
        /// </summary>
        /// <param name="templateid">TaxBaseType ID.</param>
        /// <param name="isin">True for tax in the list else false for tax out of the list.</param>
        /// <returns>PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable</returns>
        public PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable SelectTaxBaseTypeInOrOut(int templateid, bool isin)
        {
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable table =
                new PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable();
            PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.GetTaxBaseTypeInAndOutTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.GetTaxBaseTypeInAndOutTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, templateid, isin);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select tax template base by template ID.
        /// </summary>
        /// <param name="taxbasetemplateid">Tax base template ID.</param>
        /// <returns>PACFD.DataAccess.TaxBaseTemplatesDataSet</returns>
        public PACFD.DataAccess.TaxBaseTemplatesDataSet SelectByTaxBaseTemplateID(int taxbasetemplateid)
        {
            PACFD.DataAccess.TaxBaseTemplatesDataSet table = new PACFD.DataAccess.TaxBaseTemplatesDataSet();

            try
            {
                using (PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    table = adapter.FillTaxBaseTemplate_TaxBaseTemplateID(taxbasetemplateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Update, modify, insert or delete a tax base template.
        /// </summary>
        /// <param name="dataset">Data set with the information to offect.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFD.DataAccess.TaxBaseTemplatesDataSet dataset)
        {
            try
            {
                using (PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.UpdateTaxBaseTemplate_TaxBaseTemplateTaxType(dataset);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
    }
}