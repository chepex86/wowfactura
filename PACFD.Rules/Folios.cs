﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.FoliosDataSetTableAdapters;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    ///  
    /// </summary>
    [System.Obsolete("This class is obsolet use Series instead.")]
    public class Folios
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool Update(FoliosDataSet.FoliosDataTable table)
        {
            try
            {
                using (FoliosTableAdapter taFolios = new FoliosTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taFolios);
                    taFolios.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FoliosDataSet.FoliosDataTable SelectByID(int id)
        {
            FoliosDataSet.FoliosDataTable table;

            table = new FoliosDataSet.FoliosDataTable();
            this.SelectByID(table, id);

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(FoliosDataSet.FoliosDataTable table, int id)
        {
            try
            {
                using (FoliosTableAdapter adapter = new FoliosTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }

            return true;
        }
    }
}
