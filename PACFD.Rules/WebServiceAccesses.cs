﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;

namespace PACFD.Rules
{
    public class WebServiceAccesses
    {
        /// <summary>
        /// Update, edit or delete rows from WebServiceAccessesDataSet table.
        /// </summary>
        /// <param name="table">Table with the WebServiceAccessesDataSet rows.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table)
        {
            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.WebServiceAccessesTableAdapter adapter =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.WebServiceAccessesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Select all the WebServiceAccessesDataSet with compaie names rows.
        /// </summary>
        /// <returns>PACFD.DataAccess.WebServiceAccessesDataSet.GetAllWithCompanyNamesDataTable.</returns>
        public PACFD.DataAccess.WebServiceAccessesDataSet.GetAllWithCompanyNamesDataTable SelectAllWithCompanyName()
        {
            PACFD.DataAccess.WebServiceAccessesDataSet.GetAllWithCompanyNamesDataTable table =
                new PACFD.DataAccess.WebServiceAccessesDataSet.GetAllWithCompanyNamesDataTable();
            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.GetAllWithCompanyNamesTableAdapter adapter =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.GetAllWithCompanyNamesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }

        public PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable SelectByID(int webserviceid)
        {
            PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table =
                new PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable();

            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.WebServiceAccessesTableAdapter adapter =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.WebServiceAccessesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, webserviceid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Set the active state of a row.
        /// </summary>
        /// <param name="webserviceaccessesid">WebServiceAccesses ID </param>
        /// <param name="active">The active state of the row.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetActive(int webserviceaccessesid, bool active)
        {
            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.QueriesTableAdapter query =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spWebServiceAccesses_SetActive(webserviceaccessesid, active);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable SelectByNamePassword(string name, string password)
        {
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table =
                new PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable();

            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.SearchByNamePasswordTableAdapter adapter =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.SearchByNamePasswordTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, name, password);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }

        public int GetPrintID(int billerid)
        {
            try
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.GetByFkBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.WebServiceAccessesDataSetTableAdapters.GetByFkBillerIDTableAdapter())
                {
                    using (PACFD.DataAccess.WebServiceAccessesDataSet.GetByFkBillerIDDataTable table = new
                         PACFD.DataAccess.WebServiceAccessesDataSet.GetByFkBillerIDDataTable())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Fill(table, (int?)billerid);

                        if (table.Count < 1)
                            return 0;

                        if (table[0].IsPrintTemplateIDNull())
                            return 0;

                        return table[0].PrintTemplateID;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return 0;
            }
        }
    }
}
