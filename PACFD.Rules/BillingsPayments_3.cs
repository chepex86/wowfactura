﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using PACFD.Rules.BillingsExceptions;
using System.Web;
using Tool = PACFD.Common.Utilities;
using System.Security.Cryptography;

namespace PACFD.Rules
{
    /// <summary>
    /// Class used as a layer ruler for Billings table.
    /// </summary>
    public partial class BillingsPayments_3
    {
        /// <summary> 
        /// Header of line for concept when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_CONCEPT = "C";
        /// <summary>
        /// Header of line for iva when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_IVA = "V";
        /// <summary>
        /// Header of line for move when insertion from stream.
        /// </summary>
        public const string HEADER_STREAM_MOVE = "M";


        public FiscalRegime FiscalRegimeTable { get { return new FiscalRegime(); } }

        /// <summary>
        /// Select all the billing from an external folio.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the bill.</param>
        /// <param name="folio">Folio to look for.</param>        
        public PACFD.DataAccess.BillingsDataSet.spBillingsPayments_3_GetByExternalFolioDataTable SelectByExternalFolio(int billerid, string folio)
        {
            PACFD.DataAccess.BillingsDataSet.spBillingsPayments_3_GetByExternalFolioDataTable
                table = new PACFD.DataAccess.BillingsDataSet.spBillingsPayments_3_GetByExternalFolioDataTable();

            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetByExternalFolioTableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetByExternalFolioTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, folio);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.BillingsDataSet.BillingsDataTable table with the billing ID.
        /// </summary>
        /// <param name="id">Billing to look for.</param>
        /// <returns>Return a DataAccess.BillingsDataSet.BillingsDataTable table.</returns>
        public DataAccess.BillingsDataSet.BillingsPayments_3DataTable SelectByID(int id)
        {
            DataAccess.BillingsDataSet.BillingsPayments_3DataTable table;

            table = new PACFD.DataAccess.BillingsDataSet.BillingsPayments_3DataTable();
            this.SelectByID(table, id);

            return table;
        }
        public DataAccess.BillingsDataSet.spBillingsPayments_3_GetSerialsByIDDataTable GetSerialsByBillerID(int BillerID)
        {
            DataAccess.BillingsDataSet.spBillingsPayments_3_GetSerialsByIDDataTable table = new PACFD.DataAccess.BillingsDataSet.spBillingsPayments_3_GetSerialsByIDDataTable();
            this.GetSerialsByBillerID(table, BillerID);

            return table;
        }
        public bool GetSerialsByBillerID(DataAccess.BillingsDataSet.spBillingsPayments_3_GetSerialsByIDDataTable ta, int BillerID)
        {
            DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetSerialsByIDTableAdapter adapt;

            try
            {
                using (adapt = new PACFD.DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetSerialsByIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapt);
                    adapt.Fill(ta, BillerID);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(DataAccess.BillingsDataSet.BillingsPayments_3DataTable table, int id)
        {
            DataAccess.BillingsDataSetTableAdapters.BillingsPayments_3TableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsPayments_3TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public bool SelectReceptor(DataAccess.BillingsDataSet.ReceptorsDataTable table, int id)
        {
            DataAccess.BillingsDataSetTableAdapters.ReceptorsTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.ReceptorsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public bool SelectBiller(DataAccess.BillingsDataSet.BillersDataTable table, int id)
        {
            DataAccess.BillingsDataSetTableAdapters.BillersTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.BillersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Update, delete or modify a billing. If a Billing is inserted and include serial ID, the serial is marked as used. 
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillingsDataSet dataset)
        {
            DataAccess.BillingsPayments_DA adapter = new PACFD.DataAccess.BillingsPayments_DA();

            try
            {
                //Tool.RemoveOwnerSqlCommand
                //is used inside the update method.
                adapter.Update(dataset);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        /// <summary>
        /// delete a billing. 
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public bool Delete(DataAccess.BillingsDataSet dataset)
        {
            DataAccess.BillingsPayments_DA adapter = new PACFD.DataAccess.BillingsPayments_DA();

            try
            {
                //Tool.RemoveOwnerSqlCommand
                //is used inside the update method.
                adapter.Delete(dataset);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Update, modify, insert or delete only the billing table, if a more complicate operation
        /// is needed use the overload update method that resive a DataAccess.BillingsDataSet.
        /// </summary>
        /// <param name="table">Update, modify, insert or delete only the billing table</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillingsDataSet.BillingsPayments_3DataTable table)
        {
            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsPayments_3TableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsPayments_3TableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the paid status of a billing.
        /// </summary>
        /// <param name="billingid">Billing ID to change.</param>
        /// <param name="paid">State of the billing.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetPaidTo(int billingpaymentid, int billingid)
        {
            if (billingid < 1)
                return false;

            try
            {
                using (DataAccess.BillingsDataSet.BillingsDataTable table = new PACFD.DataAccess.BillingsDataSet.BillingsDataTable())
                {
                    using (DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter adapter =
                        new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);

                        if (adapter.Fill(table, billingid) < 1)
                            return false;

                        table[0].BeginEdit();
                        table[0].BillingPaymentId = billingpaymentid;
                        table[0].EndEdit();
                        table[0].AcceptChanges();
                        table[0].SetModified();
                        adapter.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the active state of a billing.
        /// If billing is a CFDI then is send to PAC's web service (send to pac is disabled at the moment)
        /// , if the cancellation is success the billing is cancel else throw an exception.
        /// </summary>
        /// <param name="billingpaymentid">Billing ID to set active.</param>
        /// <param name="active">State of the active.</param>
        /// <exception cref="PACFD.Rules.BillingsExceptions.BillingCancelError">If CFDI and the pac can't cancel the billing then throw cancel exception.</exception>
        /// <returns>If Success return true else false.</returns>
        public bool SetActive(int billingpaymentid, bool active)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;
            PACFD.Rules.ElectronicBillingType electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate;
            ErrorManager error = null;
            bool cancel = false;

            using (PACFD.DataAccess.BillingsDataSet fullbilling = this.GetFullBilling(billingpaymentid))
            {
                if (fullbilling.Billings.Count < 1)
                    return false;

                if (CancellationsCFDI.SendToCancellation(fullbilling, ref error))
                {
                    cancel = true;
                    Common.PacSealErrorTypes currentError = GetCurrentErrorPac(billingpaymentid);

                    if (currentError == PacSealErrorTypes.CancelError)
                        SetErrorPAC(billingpaymentid, PacSealErrorTypes.CancelNotMailSend);
                    else
                        SetErrorPAC(billingpaymentid, PacSealErrorTypes.None);
                }
                else if (SetErrorPAC(billingpaymentid, PacSealErrorTypes.CancelError))
                {
                    cancel = true;
                }
                else cancel = true;
            }

            if (!cancel) //if electronic type is cfdi and an error is detected, don't send to cancel.
                return false;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillingsPayments_3_SetActive(billingpaymentid, active);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }


        /// <summary>
        /// Set the date of a billing
        /// </summary>
        /// <param name="billingid">Billing ID to be modify</param>
        /// <param name="newdate">New date to be set.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetDateTo(int billingpaymentid, DateTime newdate)
        {
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query;

            try
            {
                using (query = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillingsPayments_3_SetDateTo(billingpaymentid, newdate);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        #region ReSeal

        public bool ReSealCFD(int bilingpaymentid)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(bilingpaymentid);
                PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billing = ds.BillingsPayments_3[0];
                string orinalString = GenerateOrinalString(ds);
                string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);
                if (orinalString.Trim() != "" && seal != "")
                {
                    SetReSealCFD(bilingpaymentid, orinalString, seal);
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                         + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }

        public bool ReSealCFD()
        {
            try
            {
                DataAccess.BillingsDataSet data = new PACFD.DataAccess.BillingsDataSet();
                DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetForReSealCFDTableAdapter adapter = new PACFD.DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetForReSealCFDTableAdapter();
                adapter.Fill(data.spBillingsPayments_3_GetForReSealCFD);
                if (data != null && data.spBillingsPayments_3_GetForReSealCFD.Count > 0)
                {
                    foreach (var dr in data.spBillingsPayments_3_GetForReSealCFD)
                    {
                        PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(dr.BillingPaymentId);
                        if (ds != null && ds.Billings.Count > 0)
                        {

                            PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billing = ds.BillingsPayments_3[0];
                            string orinalString = GenerateOrinalString(ds);
                            string seal = GenerateSeal(orinalString, billing.BillerID, billing.CertificateNumber, billing.BillingDate);
                            if (orinalString.Trim() != "" && seal != "")
                            {
                                SetReSealCFD(dr.BillingPaymentId, orinalString, seal);
                            }
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                         + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }

        private bool SetReSealCFD(int billingpaymentid, string originalString, string Seal)
        {
            try
            {

                using (DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query =
                        new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillingsPayments_3_ReSealCFD(billingpaymentid, originalString, Seal);
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                             + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }
        #endregion
        /// <summary>
        /// Set an error seal code to a billing.
        /// </summary>
        /// <param name="billingid">Billing ID to set.</param>
        /// <param name="pacerrortype">Error seal code to set.</param>
        /// <returns>If success true else false.</returns>
        public bool SetErrorPAC(int billingpaymentid, Common.PacSealErrorTypes pacerrortype)
        {
            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter query =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBillingsPayments_3_SetErrorPAC(billingpaymentid, (int)pacerrortype);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                       + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public Common.PacSealErrorTypes GetCurrentErrorPac(int billingpaymentid)
        {
            try
            {
                DataAccess.BillingsDataSet.spBillingsPayments_3_GetCFDIByPACErrorDataTable table =
                 SelectCfdiByErrorPAC(billingpaymentid, PacSealErrorTypes.Unknown);
                if (table != null && table.Count > 0)
                {
                    if (!table[0].IsErrorPACNull())
                    {
                        return (PacSealErrorTypes)table[0].ErrorPAC;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                       + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                LogManager.WriteError(ex);
            }
            return PacSealErrorTypes.Unknown;
        }
        /// <summary>
        /// Get a DataAccess.BillingsDataSet data set with all the tables used by a billing.
        /// If you only want Billings table without all the child tables, use "SelectByID" method instead.
        /// </summary>
        /// <param name="billingid">Billing ID used to fill the tables.</param>
        /// <returns>Return a DataAccess.BillingsDataSet data set.</returns>
        public DataAccess.BillingsDataSet GetFullBilling(int billingpaymentid)
        {
            DataAccess.BillingsPayments_DA da;
            try
            {
                da = new PACFD.DataAccess.BillingsPayments_DA();
                return da.GetFullBilling(billingpaymentid);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            finally
            {
                da = null;
            }
            return null;
        }
        /// <summary>
        /// Get only the necesaries ID of the billing table that have an error on seal and is active
        /// </summary>
        /// <param name="billerid">Optional null biller ID.</param>
        /// <param name="errorsealing">Optional null error seal code.</param>
        /// <returns>DataAccess.BillingsDataSet.GetCFDIBySealingErrorWSDataTable</returns>
        public DataAccess.BillingsDataSet.spBillingsPayments_3_GetCFDIByPACErrorDataTable SelectCfdiByErrorPAC(int? billerid, PacSealErrorTypes pacsealerrortypes)
        {
            DataAccess.BillingsDataSet.spBillingsPayments_3_GetCFDIByPACErrorDataTable table = new PACFD.DataAccess.BillingsDataSet.spBillingsPayments_3_GetCFDIByPACErrorDataTable();

            try
            {
                using (DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetCFDIByPACErrorTableAdapter adapter = new
                    PACFD.DataAccess.BillingsDataSetTableAdapters.spBillingsPayments_3_GetCFDIByPACErrorTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, pacsealerrortypes == PacSealErrorTypes.None ? null : (int?)pacsealerrortypes);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Search a billing.
        /// </summary>
        /// <param name="billerid">Biller ID.</param>
        /// <param name="receptorname">Optional name string of the receptor used as filter.</param>
        /// <param name="serial">Optional serial string used as filter.</param>
        /// <param name="folio">Optional folio number used as filter.</param>
        /// <param name="date">Optional date - time used as filter.</param>
        /// <param name="prebilling">Optional prebilling boolean used as filter.</param>
        /// <param name="ispaid">Optional ispaid boolean used as filter.</param>
        /// <returns>Return a table with all the rows founded.</returns>
        public PACFD.DataAccess.BillingsDataSet.BillingsPayment_3SearchDataTable Search(SearchPetitionFromFlag comesfrom, int billerid, string receptorname, string serial,
            string folio, DateTime? date, bool? prebilling, bool? ispaid, string uuid, int? branchID, string externalfolio, int? billingpaymentid, int? billingid)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsPayment_3SearchDataTable
                table = new PACFD.DataAccess.BillingsDataSet.BillingsPayment_3SearchDataTable();

            if (comesfrom == SearchPetitionFromFlag.None)
                return table;

            try
            {
                using (PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsPayment_3SearchTableAdapter adapter =
                    new PACFD.DataAccess.BillingsDataSetTableAdapters.BillingsPayment_3SearchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, (int?)comesfrom, billerid, receptorname, folio, serial, date, prebilling, ispaid, uuid, branchID, externalfolio, billingpaymentid, billingid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Generate original string for a billing.
        /// </summary>
        /// <param name="ds">Billing data set</param>
        /// <param name="pathXlst"></param>
        /// <returns></returns>
        public string GenerateOrinalString(DataAccess.BillingsDataSet ds)
        {
            try
            {
                string xml = GetXml(ds);
                string xlst = xlst = Properties.Resources.OriginalString_v_3_3;
                string result = string.Empty;
                using (var ms = new MemoryStream())
                {
                    var xls = new XslCompiledTransform();
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream me = new MemoryStream();
                    byte[] ba = encoding.GetBytes(xlst);
                    me.Write(ba, 0, ba.Length);
                    me.Seek(0, SeekOrigin.Begin);
                    xls.Load(new XmlTextReader(me));
                    XPathDocument xd = new XPathDocument(new XmlTextReader(xml, XmlNodeType.Element, null));
                    var xw = XmlWriter.Create(ms, xls.OutputSettings);
                    xls.Transform(xd, xw);
                    xw.Close();
                    ms.Position = 0;
                    using (var sr = new StreamReader(ms))
                    {
                        result = sr.ReadToEnd().Trim();
                        if (result != string.Empty)
                        {
                            string[] split = result.Split(new Char[] { '\r', '\n' });
                            result = "";
                            foreach (string c in split)
                            {
                                if (c.Trim() != string.Empty)
                                    result += c.Trim();

                            }

                        }
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return string.Empty;
        }
        /// <summary>
        /// Seal a billing.
        /// </summary>
        /// <param name="billingID">Billing ID to be updated.</param>
        /// <param name="pathXlst"></param>
        /// <param name="pathTemp"></param>
        /// <param name="pathOpenSSL"></param>
        /// <returns>If success return true else false.</returns>
        public bool SealCFD(int billingID, ref ErrorManager error)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                return SealCFD(ds, ref error);
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[SealCFD]", ex);
            }
            return true;
        }

        public PACFD.DataAccess.BillingsDataSet.BillingsRow NewBillingRow(PACFD.DataAccess.BillingsDataSet ds, PACFD.DataAccess.BillingsDataSet.BillingsRow r)
        {
            var row = ds.Billings.NewBillingsRow();
            row.AccountNumberPayment = r.IsAccountNumberPaymentNull() ? "":r.AccountNumberPayment;
            row.Active = r.Active;
            row.ApprovalNumber= r.IsApprovalNumberNull() ? 0: r.ApprovalNumber;
            row.ApprovalYear= r.IsApprovalYearNull() ? 0: r.ApprovalYear;
            row.BatchIdSat= r.IsBatchIdSatNull() ? "" :r.BatchIdSat;
            row.BillerID= r.BillerID;
            row.BillingDate= r.BillingDate;
            row.BillingID= r.BillingID;
            row.BillingPaymentId= r.IsBillingPaymentIdNull() ? 0: r.BillingPaymentId;
            row.BillingType= r.BillingType;
            row.BranchID= r.IsBranchIDNull()? 0 : r.BranchID;
            row.Certificate= r.Certificate;
            row.CertificateNumber= r.CertificateNumber;
            row.CertificateNumberSat= r.IsCertificateNumberSatNull() ? "" : r.CertificateNumberSat;
            row.CurrencyCode=  r.CurrencyCode;
            row.DateReSeal= r.IsDateReSealNull()? new DateTime(): r.DateReSeal;
            row.DateStamped = r.IsDateStampedNull() ? new DateTime(): r.DateStamped;
            row.Discount=  r.Discount;
            row.DiscountDescription= r.DiscountDescription;
            row.ElectronicBillingType= r.ElectronicBillingType;
            row.Etc= r.IsEtcNull() ? 0 : r.Etc;
            row.ExchangeRateMXN= r.IsExchangeRateMXNNull() ? 0 : r.ExchangeRateMXN;
            row.ExternalFolio= r.IsExternalFolioNull() ? "": r.ExternalFolio;
            row.Folio=  r.Folio;
            row.FolioID= r.IsFolioIDNull() ? 0 : r.FolioID;
            row.InternalBillingType= r.IsInternalBillingTypeNull() ? "": r.InternalBillingType;
            row.IsCredit= r.IsCredit;
            row.IsPaid= r.IsIsPaidNull() ? false: r.IsPaid;
            row.Observations= r.IsObservationsNull() ? "": r.Observations;
            row.OriginalString=  r.OriginalString;
            row.OrignalStringSat= r.IsOrignalStringSatNull() ? "" : r.OrignalStringSat;
            row.PaymentForm=  r.PaymentForm;
            row.PaymentMethod= r.PaymentMethod;
            row.PaymentTerms=  r.PaymentTerms;
            row.PlaceDispatch= r.IsPlaceDispatchNull() ? "": r.PlaceDispatch;
            row.PreBilling= r.PreBilling;
            row.PrintTemplateID= r.PrintTemplateID;
            row.QrImage= r.IsQrImageNull() ? new byte[0]: r.QrImage;
            row.ReceptorID= r.ReceptorID;
            row.ReSeal= r.IsReSealNull() ? false: r.ReSeal;
            row.RfcProvCertic= r.IsRfcProvCerticNull() ? "" : r.RfcProvCertic;
            row.Seal=  r.Seal;
            row.SealSat= r.IsSealSatNull() ? "": r.SealSat;
            row.SelloCFD= r.IsSelloCFDNull() ? "" : r.SelloCFD;
            row.Serial=  r.Serial;
            row.SerialID= r.IsSerialIDNull() ? 0 : r.SerialID;
            row.StatusSat= r.IsStatusSatNull() ? "": r.StatusSat;
            row.SubTotal=  r.SubTotal;
            row.TaxTemplateName=  r.TaxTemplateName;
            row.Total=  r.Total;
            row.UUID= r.IsUUIDNull() ? "" : r.UUID;
            row.Version= r.Version;
            row.VersionSat= r.IsVersionSatNull() ? "" : r.VersionSat;
            row.Partiality = r.IsPartialityNull() ? 0 : r.Partiality;
            row.ImpSaldoAnt = r.IsImpSaldoAntNull() ? 0: r.ImpSaldoAnt;
            row.ImpPagado = r.IsImpPagadoNull() ? 0 : r.ImpPagado;
            row.ImpSaldoInsoluto = r.IsImpSaldoInsolutoNull() ? 0 : r.ImpSaldoInsoluto;
            return row;
        }
        /// <summary>
        /// Seal a billing dataset.
        /// </summary>
        /// <param name="ds">Billing data set with the billing to be updated.</param>
        /// <param name="pathXlst"></param>
        /// <param name="pathTemp"></param>
        /// <param name="pathOpenSSL"></param>
        /// <returns>If succes return true else false.</returns>
        public bool SealCFD(PACFD.DataAccess.BillingsDataSet ds, ref ErrorManager error)
        {
            try
            {
                if (ds == null)
                    return false;

                PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billingpayment = ds.BillingsPayments_3[0];
                this.SelectReceptor(ds.Receptors, ds.BillingsPayments_3[0].ReceptorID);
                this.SelectBiller(ds.Billers, ds.BillingsPayments_3[0].BillerID);
                string orinalString = GenerateOrinalString(ds);
                string filio = string.Format("{0}-{1}", billingpayment.Serial, billingpayment.Folio);
                string seal = GenerateSeal(orinalString, billingpayment.BillerID, billingpayment.CertificateNumber, billingpayment.BillingDate);

                if (string.IsNullOrEmpty(seal))
                    return false;

                PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter q = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter();
                Tool.RemoveOwnerSqlCommand(q);

                ds.BillingsPayments_3[0].Seal = seal;
                ds.BillingsPayments_3[0].OriginalString = orinalString;
                if (StampingCFDI.SendToStamping(ds, true, ref error))
                {
                    q.spSealPaymentCFDI(ds.BillingsPayments_3[0].BillingPaymentId
                                 , orinalString
                                 , seal
                                 , ds.BillingsPayments_3[0].UUID
                                 , ds.BillingsPayments_3[0].DateStamped
                                 , ds.BillingsPayments_3[0].SealSat
                                 , ds.BillingsPayments_3[0].CertificateNumberSat
                                 , ds.BillingsPayments_3[0].OrignalStringSat
                                 , ds.BillingsPayments_3[0].BatchIdSat
                                 , ds.BillingsPayments_3[0].StatusSat
                                 , ds.BillingsPayments_3[0].VersionSat
                                 , GetCFDIArray(ds)
                                 , ds.BillingsPayments_3[0].RfcProvCertic
                                 , ds.BillingsPayments_3[0].SelloCFD);
                    return true;

                }
                else
                {
                    this.SetErrorPAC(ds.BillingsPayments_3[0].BillingPaymentId, PacSealErrorTypes.SealError);

                    try
                    {
                        //aqui menter el correo

                        PACFD.Rules.Mail.MailSender sender = new PACFD.Rules.Mail.MailSender();
                        String path = HttpContext.Current.Request.MapPath("~/Includes/Mail/EmisorAdded.es.xml");
                        String xmlParse = ds.GetXml().Replace("<", "&#60;").Replace(">", "&#62;");

                        sender.Message = PACFD.Rules.Mail.MailSenderHelper.GetMessageFromXML(path, "005");

                        sender.Parameters.Add("lblError", error.Error.Message);
                        sender.Parameters.Add("lblXML", xmlParse);
                        sender.Send();
                    }
                    catch (Exception ex)
                    {
                        LogManager.WriteCustom("[SealCFD -SendMail]", ex);

                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[SealCFD]", ex);
                return false;
            }

            return false;
        }

        public bool HandSealCFD(int billingpaymentid, string UUID, string dateStamped, string sealSat, string certificateNumberSat, string orignalStringSat, string versionSat)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingpaymentid);
                if (ds != null)
                {
                    PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billingpayment = ds.BillingsPayments_3[0];
                    if (string.IsNullOrEmpty(UUID) || string.IsNullOrEmpty(dateStamped) || string.IsNullOrEmpty(sealSat) ||
                        string.IsNullOrEmpty(certificateNumberSat) || string.IsNullOrEmpty(orignalStringSat) || string.IsNullOrEmpty(versionSat))
                    {
                        return false;
                    }
                    string orinalString = GenerateOrinalString(ds);
                    string filio = string.Format("{0}-{1}", billingpayment.Serial, billingpayment.Folio);
                    string seal = GenerateSeal(orinalString, billingpayment.BillerID, billingpayment.CertificateNumber, billingpayment.BillingDate);

                    if (string.IsNullOrEmpty(seal))
                        return false;

                    PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter q = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter();
                    Tool.RemoveOwnerSqlCommand(q);
                    DateTime DateStamped = DateTime.Parse(dateStamped);
                    q.spSealPaymentCFDI(ds.BillingsPayments_3[0].BillingPaymentId
                                  , orinalString
                                  , seal
                                  , UUID
                                  , DateStamped
                                  , sealSat
                                  , certificateNumberSat
                                  , orignalStringSat
                                  , "-1"
                                  , "7"
                                  , versionSat
                                  , GetCFDIArray(ds)
                                  , "sad"
                                  , "huehue");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteCustom("[HandSealCFD]", ex);

            }
            return false;
        }

        public static string GenerateSeal(string orinalString, int billerID, string CertificateNumber, DateTime date)
        {
            string path = string.Empty;
            string result = string.Empty;
            DateTime d2011 = new DateTime(2011, 01, 01);
            DateTime d2016 = new DateTime(2017, 01, 10);
            try
            {
                DigitalCertificates cer = new DigitalCertificates();

                string passPrivateKey = string.Empty;
                //FALTA VALIDAR LA FECHA DEL CETIFICADO
                byte[] privateKey = cer.GetPrivateKey(billerID, CertificateNumber, ref passPrivateKey);
                if (privateKey != null)
                {

                    System.Security.SecureString pass = new System.Security.SecureString();
                    for (int i = 0; i < passPrivateKey.Length; i++)
                    {
                        pass.AppendChar(passPrivateKey.ToCharArray()[i]);
                    }
                    System.Security.Cryptography.RSACryptoServiceProvider key = OpenSSL.DecodeEncryptedPrivateKeyInfo(privateKey, pass);

                    object hash = null;
                    if (date < d2011)
                        hash = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    else if (date < d2016)
                        hash = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                    else
                        hash = CryptoConfig.CreateFromName("SHA256");

                    Byte[] signedBytes = key.SignData(Encoding.UTF8.GetBytes(orinalString), hash);
                    string sello = Convert.ToBase64String(signedBytes);

                    return sello;
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GenerateSeal " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return string.Empty;
        }

        private static void DeleteFile(string file)
        {
            try
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] DeleteFile " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        private static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    File.Delete(path);
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] DeleteFile " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        private static string GetMD5(string cadenaOriginal)
        {
            try
            {
                byte[] CadenaUTF8;
                byte[] tmpHash;

                CadenaUTF8 = Encoding.UTF8.GetBytes(cadenaOriginal);
                tmpHash = new System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(CadenaUTF8);
                StringBuilder sOutput = new StringBuilder(tmpHash.Length);
                for (int i = 0; i < tmpHash.Length; i++)
                {
                    sOutput.Append(tmpHash[i].ToString("x2"));
                }
                return sOutput.ToString();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetMD5 " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            return string.Empty;

        }
        public byte[] GetXml(int billingID, ref string fileName)
        {
            try
            {
                PACFD.DataAccess.BillingsDataSet ds = GetFullBilling(billingID);
                if (ds != null)
                {
                    var billerRfc = new Billers().SelectByID(ds.BillingsPayments_3[0].BillerID)[0].RFC;
                    fileName = string.Format("{0}_{1}-{2}.xml", billerRfc, ds.BillingsPayments_3[0].Serial, ds.BillingsPayments_3[0].Folio);

                    string xml = GetXml(ds);
                    //using (var ms = new MemoryStream())
                    //{
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    //MemoryStream me = new MemoryStream();
                    byte[] ba = encoding.GetBytes(xml);

                    return ba;
                    //}
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetXml " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return null;
        }
        /// <summary>
        /// Get a billing from a dataset.
        /// </summary>
        /// <param name="ds">Dataset with all billing information.</param>
        /// <param name="pathXlst">Transformation path used to convert the dataset.</param>
        /// <returns>If success return astring with values else null.</returns>
        public string GetXml(PACFD.DataAccess.BillingsDataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    string xmlOrigen = ds.GetXml();
                    bool isCfd = false;
                    bool format = false;
                    if (xmlOrigen.Trim() != string.Empty)
                    {
                        using (var ms = new MemoryStream())
                        {
                            var xls = new XslCompiledTransform();
                            //Obtener la hoja de transformacion desde los recursos
                            string resourceXlst = Properties.Resources.CfdDataSetToCFDPAGO33;
                            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                            MemoryStream me = new MemoryStream();
                            byte[] ba = encoding.GetBytes(resourceXlst.Trim());
                            me.Write(ba, 0, ba.Length);
                            me.Seek(0, SeekOrigin.Begin);
                            xls.Load(new XmlTextReader(me));

                            XPathDocument xd = new XPathDocument(new XmlTextReader(xmlOrigen, XmlNodeType.Element, null));
                            var xw = XmlWriter.Create(ms, xls.OutputSettings);
                            xls.Transform(xd, xw);
                            xw.Close();
                            ms.Position = 0;
                            string result = "";
                            using (var sr = new StreamReader(ms))
                            {
                                result = sr.ReadToEnd().Trim();
                            }
                            #region caso especial corregir formateo

                            if (isCfd && format)
                            {
                                FormatingCFD(ref result);
                            }
                            #endregion

                            return result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] GetXml " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            return null;
        }

        private void FormatingCFD(ref string xml)
        {
            string auxXml = xml;
            try
            {
                if (xml != string.Empty)
                {
                    bool haveTasa = false;
                    int idx1 = -1;
                    int idx2 = -1;
                    int idx3 = -1;
                    int len = -1;
                    string tasa = "";

                    idx1 = xml.IndexOf("<Traslados>");
                    if (idx1 != -1)
                    {
                        do
                        {
                            idx2 = xml.IndexOf("tasa=\"", idx1);
                            if (idx2 != -1)
                            {
                                idx2 += "tasa=\"".Length;
                                idx3 = xml.IndexOf("\"", idx2);
                                if (idx3 != -1)
                                {
                                    len = idx3 - idx2;
                                    tasa = xml.Substring(idx2, len);
                                    xml = xml.Remove(idx2, len);
                                    tasa = decimal.Parse(tasa).ToString("0");
                                    xml = xml.Insert(idx2, tasa);
                                    haveTasa = true;
                                    idx1 = idx2 + tasa.Length;
                                }
                            }
                            else
                            {
                                haveTasa = false;
                            }
                        } while (haveTasa);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                xml = auxXml;
            }

        }

        public byte[] GetCFDIArray(DataAccess.BillingsDataSet billings)
        {
            byte[] b = null;

            try
            {
                if (billings == null)
                    return null;

                if (billings.BillingsPayments_3.Count < 1)
                    return null;
                String re = String.Empty;
                String rr = String.Empty;
                String tt = String.Empty;
                String id = String.Empty;
                String fmt = "0000000000.000000";

                if (billings.BillingsPayments_3[0].UUID == null || billings.BillingsPayments_3[0].UUID == String.Empty)
                    return null;

                var billers = new Billers().SelectByID(billings.BillingsPayments_3[0].BillerID);
                var receptors = new Receptors().SelectByID(billings.BillingsPayments_3[0].ReceptorID);
                if (billers.Count < 1)
                    return null;
                if (billers[0].RFC == null || billers[0].RFC == String.Empty)
                    return null;

                if (receptors.Count < 1)
                    return null;
                if (receptors[0].RFC == null || receptors[0].RFC == String.Empty)
                    return null;

                re = billers[0].RFC;
                rr = receptors[0].RFC;
                tt = String.Format("{0,22}", billings.BillingsPayments_3[0].Total.ToString(fmt)).Trim();
                id = billings.BillingsPayments_3[0].UUID;

                if (re.Length < 12 || re.Length > 13)
                    return null;
                if (rr.Length < 12 || rr.Length > 13)
                    return null;
                if (id.Length < 36 || id.Length > 36)
                    return null;

                QRCodeLib.QRCodeEncoder encoder = new QRCodeLib.QRCodeEncoder();

                encoder.QRCodeEncodeMode = QRCodeLib.QRCodeEncoder.ENCODE_MODE.BYTE;
                encoder.QRCodeScale = 3;
                encoder.QRCodeVersion = 7;
                encoder.QRCodeErrorCorrect = QRCodeLib.QRCodeEncoder.ERROR_CORRECTION.M;

                System.Drawing.Image img;

                String data = String.Empty;

                data = String.Format("?re={0}&rr={1}&tt={2}&id={3}", re, rr, tt, id);

                img = encoder.Encode(data);

                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                b = ms.ToArray();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return b;

        }

        /// <summary>
        /// Enum used to determinated a search calls come from.
        /// </summary>
        public enum SearchPetitionFromFlag
        {
            None = 0,
            WebForm = 1,
            WebService = 2,
            OpenWebForm = 3,
            //WindowsForm = 4,
            //Ajaxs = 5,
        }

        public class FiscalRegime
        {
            public PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable GetByBillerID(int billerid)
            {
                PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable table =
                    new PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable();

                try
                {
                    using (PACFD.DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter adapter =
                        new PACFD.DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter())
                        adapter.Fill(table, billerid);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                           + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                    LogManager.WriteError(ex);
                }
                return table;
            }
        }
    }
}
