﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Pac configuration layer
    /// </summary>
    public class PacConfiguration
    {
        /// <summary>
        /// Update, insert delete or modify a configuration.
        /// </summary>
        /// <param name="table">Table with files to affect.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.PacConfigurationDataSet.PacConfigurationDataTable table)
        {
            try
            {
                using (DataAccess.PacConfigurationDataSetTableAdapters.PacConfigurationTableAdapter adapter =
                    new PACFD.DataAccess.PacConfigurationDataSetTableAdapters.PacConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Select a pac configuration by ID.
        /// </summary>
        /// <param name="pacid">Pac ID:</param>
        /// <returns>DataAccess.PacConfigurationDataSet.PacConfigurationDataTable</returns>
        public DataAccess.PacConfigurationDataSet.PacConfigurationDataTable SelectByID(int pacid)
        {
            DataAccess.PacConfigurationDataSet.PacConfigurationDataTable table =
                new PACFD.DataAccess.PacConfigurationDataSet.PacConfigurationDataTable();

            try
            {
                using (DataAccess.PacConfigurationDataSetTableAdapters.PacConfigurationTableAdapter adapter =
                    new PACFD.DataAccess.PacConfigurationDataSetTableAdapters.PacConfigurationTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, pacid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select a pac configuration by biller ID.
        /// </summary>
        /// <param name="billerid">Biller ID.</param>
        /// <returns>DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable</returns>
        public DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable SelectByBillerID(int billerid)
        {
            DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable table =
                new PACFD.DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable();

            try
            {
                using (DataAccess.PacConfigurationDataSetTableAdapters.GetByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.PacConfigurationDataSetTableAdapters.GetByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Set active state of a pac configuration.
        /// </summary>
        /// <param name="pacid">Pac ID.</param>
        /// <param name="active">New active state.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetActive(int pacid, bool active)
        {
            using (DataAccess.PacConfigurationDataSet.PacConfigurationDataTable table = this.SelectByID(pacid))
            {
                if (table.Count < 1)
                { return false; }

                table[0].Active = active;
                table[0].AcceptChanges();
                table[0].SetModified();

                this.Update(table);
            }

            return true;
        }
        /// <summary>
        /// Get a boolean value indicating the active state of a pac configuration.
        /// </summary>
        /// <param name="billerid">Biller ID.</param>
        /// <returns>True if active else false.</returns>
        public bool IsActive(int billerid)
        {
            using (DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable table = this.SelectByBillerID(billerid))
            {
                if (table.Count < 1)
                { return false; }

                return table[0].Active;
            }
        }
    }
}
