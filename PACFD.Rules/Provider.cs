﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.ProviderDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class Provider
    {
        #region SelectByProviderID
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ProviderDataSet.ProviderDataTable GetProvider()
        {
            ProviderDataSet.ProviderDataTable table;

            table = new ProviderDataSet.ProviderDataTable();
            this.GetProvider(table, 1);

            return table;
        }
        /// <summary>
        ///  
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GetProvider(ProviderDataSet.ProviderDataTable table, int id)
        {
            try
            {
                using (ProviderTableAdapter adapter = new ProviderTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region UpdateProviders
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool UpdateProviders(ProviderDataSet.ProviderDataTable table)
        {
            try
            {
                using (ProviderTableAdapter taUser = new ProviderTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taUser);
                    taUser.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region UpdateDigitalCertificates
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool UpdateDigitalCertificates(ProviderDataSet.ProviderDigitalCertificatesDataTable table)
        {
            try
            {
                using (ProviderDigitalCertificatesTableAdapter taCertificate = new ProviderDigitalCertificatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taCertificate);
                    taCertificate.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select All Digital Certificates
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable GetAllDigitalCertificatesProvider()
        {
            ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable table;

            table = new ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable();
            this.GetAllDigitalCertificatesProvider(table, 1);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GetAllDigitalCertificatesProvider(ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable table, int id)
        {
            try
            {
                using (ProviderDigitalCertificates_GetByFkProviderIDTableAdapter adapter = new ProviderDigitalCertificates_GetByFkProviderIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select Digital Certificates By ProviderDigitalCertificateID
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ProviderDataSet.ProviderDigitalCertificatesDataTable GetDigitalCertificatesProviderByID(int id)
        {
            ProviderDataSet.ProviderDigitalCertificatesDataTable table;

            table = new ProviderDataSet.ProviderDigitalCertificatesDataTable();
            this.GetDigitalCertificatesProviderByID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GetDigitalCertificatesProviderByID(ProviderDataSet.ProviderDigitalCertificatesDataTable table, int id)
        {
            try
            {
                using (ProviderDigitalCertificatesTableAdapter adapter = new ProviderDigitalCertificatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Get Active
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ProviderDataSet.ProviderDigitalCertificates_GetActiveDataTable GetActive()
        {
            ProviderDataSet.ProviderDigitalCertificates_GetActiveDataTable table;

            table = new ProviderDataSet.ProviderDigitalCertificates_GetActiveDataTable();
            this.GetActive(table, 1);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GetActive(ProviderDataSet.ProviderDigitalCertificates_GetActiveDataTable table, int id)
        {
            try
            {
                using (ProviderDigitalCertificates_GetActiveTableAdapter adapter = new ProviderDigitalCertificates_GetActiveTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion
    }
}
