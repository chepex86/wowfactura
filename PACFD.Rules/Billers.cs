﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class used as a layer for Billers data table.
    /// </summary>
    /// <remarks>
    /// spBillers_GetByClientIDAndName
    /// </remarks>
    public class Billers
    {
        /// <summary>
        /// Default currency code soported by the billing.
        /// </summary>
        public const string DEFAULT_CURRENCYCODE = "MXN";

        // public BillerTaxes BillerTaxesTable { get; private set; }
        /// <summary>
        /// Use this property to update the issued table individual, if an asynchronous is needed
        /// use update(BillerDataSet) instead.
        /// </summary>
        public Issued IssuedTable { get; private set; }
        public FiscalRegime FiscalRegimeTable { get; private set; }


        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public Billers()
        {
            this.IssuedTable = new Issued();
            this.FiscalRegimeTable = new FiscalRegime();
        }
        /// <summary>
        /// Get a DataAccess.BillersDataSet.GetByClientIDAndNameDataTable table with the 
        /// cities found filtered by clientID and city name.
        /// </summary>
        /// <param name="groupid">GroupID used as filter.</param>
        /// <param name="name">Name of the city to look for.</param>
        /// <returns>Return a DataAccess.BillersDataSet.GetByClientIDAndNameDataTable table.</returns>
        public DataAccess.BillersDataSet.GetByClientIDAndNameDataTable SelectByClientIDAndName(int groupid, string name)
        {
            DataAccess.BillersDataSet.GetByClientIDAndNameDataTable table;
            DataAccess.BillersDataSetTableAdapters.GetByClientIDAndNameTableAdapter adapter;

            table = new PACFD.DataAccess.BillersDataSet.GetByClientIDAndNameDataTable();

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.GetByClientIDAndNameTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, name);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get all DataAccess.BillersDataSet.SelectBillersAllDataTable rows from the database.
        /// </summary>
        /// <returns>
        /// Return a DataAccess.BillersDataSet.SelectBillersAllDataTable table with all
        /// the rows from the data base.
        /// </returns>
        public DataAccess.BillersDataSet.SelectBillersAllDataTable SelectAll()
        {
            DataAccess.BillersDataSet.SelectBillersAllDataTable table;
            DataAccess.BillersDataSetTableAdapters.SelectBillersAllTableAdapter adapters;

            table = new PACFD.DataAccess.BillersDataSet.SelectBillersAllDataTable();

            try
            {
                using (adapters = new PACFD.DataAccess.BillersDataSetTableAdapters.SelectBillersAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapters);
                    adapters.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Fill a DataAccess.BillersDataSet.BillersDataTable table filtered by BillerID.
        /// </summary>
        /// <param name="table">DataAccess.BillersDataSet.BillersDataTable table to be filled.</param>
        /// <param name="id">BillersID used to filter.</param>
        /// <returns>Return a boolean value if success else false.</returns>
        public bool SelectByID(DataAccess.BillersDataSet.BillersDataTable table, int id)
        {
            DataAccess.BillersDataSetTableAdapters.BillersTableAdapter adapters;

            try
            {
                using (adapters = new PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapters);
                    adapters.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Get a DataAccess.BillersDataSet.BillersDataTable table with the biller filtered by ID.
        /// </summary>
        /// <param name="id">BillerID to llok for.</param>
        /// <returns>Return a DataAccess.BillersDataSet.BillersDataTable table.</returns>
        public DataAccess.BillersDataSet.BillersDataTable SelectByID(int id)
        {
            DataAccess.BillersDataSet.BillersDataTable table;

            table = new PACFD.DataAccess.BillersDataSet.BillersDataTable();
            this.SelectByID(table, id);

            return table;
        }
        /// <summary>
        /// ?????????????????????????????????
        /// ?????????????????????????????????
        /// ?????????????????????????????????
        /// ?????????????????????????????????...
        /// </summary>
        /// <param name="table">DataAccess.BillersDataSet.Billers_GetForListDataTable table to by filled.</param>
        /// <param name="groupID">GroupID used as a filter.</param>
        /// <returns>If success return true else false.</returns>
        public bool SelectBillersForList(DataAccess.BillersDataSet.Billers_GetForListDataTable table, int groupID, bool? active)
        {
            DataAccess.BillersDataSetTableAdapters.Billers_GetForListTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.Billers_GetForListTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupID, active);
                }
            }
            catch (Exception ex)
            {

                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Get a DataAccess.BillersDataSet.SearchByNameDataTable table filtered by Name
        /// </summary>
        /// <param name="groupid">GroupID used to filter</param>
        /// <param name="name">Name of the biller.</param>
        /// <returns>DataAccess.BillersDataSet.SearchByNameDataTable table</returns>
        public DataAccess.BillersDataSet.SearchByNameDataTable SearchByName(int groupid, string name, bool active)
        {
            DataAccess.BillersDataSet.SearchByNameDataTable table =
                new PACFD.DataAccess.BillersDataSet.SearchByNameDataTable();
            DataAccess.BillersDataSetTableAdapters.SearchByNameTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.SearchByNameTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, name, active);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.BillersDataSet.SearchByRFCDataTable table filtered by RFC
        /// </summary>
        /// <param name="groupid">GroupID used to filter</param>
        /// <param name="rfc">RFC to look for.</param>
        /// <returns>DataAccess.BillersDataSet.SearchByRFCDataTable table </returns>
        public DataAccess.BillersDataSet.SearchByRFCDataTable SearchByRFC(int groupid, string rfc, bool active)
        {
            DataAccess.BillersDataSet.SearchByRFCDataTable table =
                new PACFD.DataAccess.BillersDataSet.SearchByRFCDataTable();
            DataAccess.BillersDataSetTableAdapters.SearchByRFCTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.SearchByRFCTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, rfc, active);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.BillersDataSet.GetRFCsDataTable table with the RFCs only filtered by rfc
        /// </summary>
        /// <param name="groupid">GroupID used to filter</param>
        /// <param name="rfc">RFC to look for.</param>
        /// <returns>DataAccess.BillersDataSet.GetRFCsDataTable table</returns>
        public DataAccess.BillersDataSet.GetRFCsDataTable GetRFCs(int groupid, string rfc)
        {
            DataAccess.BillersDataSet.GetRFCsDataTable table =
               new PACFD.DataAccess.BillersDataSet.GetRFCsDataTable();
            DataAccess.BillersDataSetTableAdapters.GetRFCsTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.GetRFCsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, rfc);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Update a row from the data base from a DataAccess.BillersDataSet.BillersDataTable table.
        /// </summary>
        /// <param name="table">DataAccess.BillersDataSet.BillersDataTable Table with the row to update.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillersDataSet.BillersDataTable table)
        {
            DataAccess.BillersDataSetTableAdapters.BillersTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Update PACFD.DataAccess.BillersDataTable table, 
        /// PACFD.DataAccess.IssuedDataTable table and
        /// PACFD.DataAccess.BillerTaxesDataTable table.
        /// </summary>
        /// <param name="dataset">DataSet with the biller, issued and billertaxes to update.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(PACFD.DataAccess.BillersDataSet dataset)
        {
            PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update_Biller_Issued_BillerTaxes(dataset);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Set Tax System PACFD.DataAccess.BillersDataTable table,
        /// </summary>
        /// <param name="taxSystem">new tax system from catalog</param>
        /// /// <param name="billerId">biller to modify</param>
        /// <returns>If success return true else false.</returns>
        public void SetTaxSystem(string taxSystem, int billerId)
        {
            PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.BillersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.SetTaxSystem(taxSystem,billerId);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        public DataAccess.BillersDataSet.SearchByFilterDataTable SelectByRFCName(int groupid, string rfc, string name, bool active)
        {
            DataAccess.BillersDataSet.SearchByFilterDataTable table;
            PACFD.DataAccess.BillersDataSetTableAdapters.SearchByFilterTableAdapter adapter;

            table = new DataAccess.BillersDataSet.SearchByFilterDataTable();

            try
            {
                using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.SearchByFilterTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, rfc, name, active);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);

            }

            return table;
        }

        public DataAccess.BillersDataSet.SearchByFilterDataTable SelectByRFCName(int groupid, string rfc, string name)
        {
            DataAccess.BillersDataSet.SearchByFilterDataTable table = new DataAccess.BillersDataSet.SearchByFilterDataTable();

            try
            {
                using (PACFD.DataAccess.BillersDataSetTableAdapters.SearchByFilterTableAdapter adapter =
                    new PACFD.DataAccess.BillersDataSetTableAdapters.SearchByFilterTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, rfc, name, (bool?)null);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get the currency code of a biller.
        /// </summary>
        /// <param name="billerid">Biller ID to search for.</param>
        /// <returns>If success return the currency code else the default soported value (MXN).</returns>
        public string GetCurrencyByBillerID(int billerid)
        {
            PACFD.Rules.Billers biller = new Billers();

            using (PACFD.DataAccess.BillersDataSet.BillersDataTable table = biller.SelectByID(billerid))
            {
                if (table.Count < 1)
                    return DEFAULT_CURRENCYCODE;

                return table[0].CurrencyCode;
            }
        }
        /// <summary>
        /// Get billers by group ID.
        /// </summary>
        /// <param name="groupid">Group ID.</param>
        /// <returns>PACFD.DataAccess.BillersDataSet.GetByGroupIDDataTable</returns>
        public PACFD.DataAccess.BillersDataSet.GetByGroupIDDataTable SelectByGroupID(int groupid)
        {
            PACFD.DataAccess.BillersDataSet.GetByGroupIDDataTable table =
                new PACFD.DataAccess.BillersDataSet.GetByGroupIDDataTable();

            try
            {
                using (PACFD.DataAccess.BillersDataSetTableAdapters.GetByGroupIDTableAdapter adapter =
                    new PACFD.DataAccess.BillersDataSetTableAdapters.GetByGroupIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }


        /// <summary>
        /// Class used to modify the Issued table. If an asynchronous update between billers
        /// and issued is needed use the Update(PACFD.DataAccess.BillersDataSet) method
        /// in the parent class instead.
        /// </summary>
        public class Issued
        {
            /// <summary>
            /// Initialize a new instance of the class, constructor is internal 
            /// for restric to create the instance.
            /// </summary>
            internal Issued() { }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="billerid"></param>
            /// <returns></returns>
            public PACFD.DataAccess.BillersDataSet.IssuedDataTable SelectByID(int billerid)
            {
                PACFD.DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter adapter;
                PACFD.DataAccess.BillersDataSet.IssuedDataTable table;

                table = new PACFD.DataAccess.BillersDataSet.IssuedDataTable();

                try
                {
                    using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Fill(table, billerid);
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                    LogManager.WriteError(ex);
                }

                return table;
            }
            public bool Update(PACFD.DataAccess.BillersDataSet.IssuedDataTable table)
            {
                DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter adapter;

                try
                {
                    using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Update(table);
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                    LogManager.WriteError(ex);
                    return false;
                }

                return true;
            }
            public bool Delete(int billerid)
            {
                DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter adapter;

                try
                {
                    using (adapter = new PACFD.DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Delete(billerid);
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                    LogManager.WriteError(ex);
                    return false;
                }

                return true;
            }
        }
        /// <summary>
        /// Class uses as layer rules for FiscalRegime.
        /// </summary>
        public class FiscalRegime
        {
            public PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable GetByBillerID(int billerid)
            {
                PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable table = new PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable();

                try
                {
                    using (DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter adapters =
                        new PACFD.DataAccess.BillersDataSetTableAdapters.FiscalRegimeGetByBillerIDTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapters);
                        adapters.Fill(table, billerid);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                    LogManager.WriteError(ex);
                }

                return table;
            }
        }

        #region Old source code
        /*
        public class BillerTaxes
        {
            public PACFD.DataAccess.TaxTypesDataSet.BillerTaxes_GetByBillerIDDataTable SelectByBillerID(int billerid)
            {
                PACFD.DataAccess.TaxTypesDataSet.BillerTaxes_GetByBillerIDDataTable table =
                    new PACFD.DataAccess.TaxTypesDataSet.BillerTaxes_GetByBillerIDDataTable();
                PACFD.DataAccess.TaxTypesDataSetTableAdapters.BillerTaxes_GetByBillerIDTableAdapter adapter;

                try
                {
                    adapter = new PACFD.DataAccess.TaxTypesDataSetTableAdapters.BillerTaxes_GetByBillerIDTableAdapter();
                    adapter.Fill(table, billerid);
                    adapter.Dispose();
                    adapter = null;
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }

                return table;
            }
        }*/
        #endregion
    }
}
