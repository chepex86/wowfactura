﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.Mail
{
    public class EmailConfiguration
    {
        public string ID { get; private set; }
        public string Description { get; private set; }


        public EmailConfiguration(string id, string description)
        {
            this.ID = id;
            this.Description = description;

        }
    }
}
