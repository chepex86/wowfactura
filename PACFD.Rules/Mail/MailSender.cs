﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace PACFD.Rules.Mail
{
    /// <summary>
    /// Class used to controlate the send of email.
    /// </summary>
    public class MailSender
    {
        /// <summary>
        /// Get a SmtpClient object with the configuration to send an email.
        /// </summary>
        public SmtpClient SmtpClient { get; private set; }
        /// <summary>
        /// Get or set the MailMessage used to send the email.
        /// </summary>
        public MailMessage Message { get; set; }
        public Dictionary<string, string> Parameters { get; private set; }
        public string this[string key] { get { return this.Parameters[key]; } set { this.Parameters[key] = value; } }



        /// <summary>
        /// Create a new instance of the class and load the default configuration.
        /// </summary>
        public MailSender()
            : this(-1)
        {
        }
        /// <summary>
        /// Create a new instance of the class and load the default company configuration.
        /// If can't load the company configuration load the default configuration.
        /// </summary>
        /// <param name="billerid">The biller ID of the company to get the custom smtp configuration. Must be > 0.</param>
        public MailSender(int billerid)
        {
            if (billerid < 1)
                this.SmtpClient = Mail.MailSenderHelper.GetSmtpClientFromDataBase();
            else
                this.SmtpClient = Mail.MailSenderHelper.GetSmtpClientFromCompanyDataBaseByBillerID(billerid);

            Parameters = new Dictionary<string, string>();
        }


        /// <summary>
        /// Send a mail.
        /// </summary>
        /// <returns>Return a MailSendResult object with the result of the operation.</returns>
        public MailSendResult Send()
        {
            if (this.Message == null)
                return new MailSendResult(false, new Exception("Not message waiting."));

            if (this.Parameters != null)
                this.ReplaceParams(this.Parameters);

            try
            {
                this.SmtpClient.Send(this.Message);
            }
            catch (Exception ex)
            {
                return new MailSendResult(false, ex);
            }

            return new MailSendResult(true, new Exception("Enviado o algo raro 19/11/2011"));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        public void ReplaceParams(Dictionary<string, string> dictionary)
        {
            string s;

            s = Message.Body;

            foreach (var item in dictionary)
                s = s.Replace("[" + item.Key + "]", item.Value);

            this.Message.Body = s;
        }
    }
}
