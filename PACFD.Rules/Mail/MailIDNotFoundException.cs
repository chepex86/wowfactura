﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.Mail
{
    public class MailIDNotFoundException : Exception
    {
        public MailIDNotFoundException(string message) : base(message)
        {
        }
    }
}
