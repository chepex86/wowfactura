﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace PACFD.Rules.Mail
{
    public static class MailSenderHelper
    {

        internal static SmtpClient GetSmtpClientFromCompanyDataBaseByBillerID(int billerid)
        {
            SmtpClient smt;
            Rules.BillerSmtpConfiguration dbase = new BillerSmtpConfiguration();

            using (DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable table = dbase.SelectByBillerID(billerid))
            {
                dbase = null;

                if (table.Count < 1)
                    return GetSmtpClientFromDataBase();

                if (table[0].UseBase)
                    return GetSmtpClientFromDataBase();

                smt = new System.Net.Mail.SmtpClient(
                    table[0].Host, table[0].Port);

                smt.Credentials = new System.Net.NetworkCredential(
                    table[0].NameCredential,
                    PACFD.Common.Cryptography.DecryptUnivisitString(table[0].PasswordCredential));

                smt.EnableSsl = table[0].EnableSsl;
            }

            return smt;
        }

        internal static SmtpClient GetSmtpClientFromDataBase()
        {
            SmtpClient smt;
            Rules.SmtpConfiguration dbase = new SmtpConfiguration();

            using (DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table = dbase.Select())
            {
                dbase = null;

                if (table.Count < 1)
                    return new SmtpClient();

                smt = new System.Net.Mail.SmtpClient(
                    table[0].Host, table[0].Port);

                smt.Credentials = new System.Net.NetworkCredential(
                    table[0].NameCredential,
                    PACFD.Common.Cryptography.DecryptUnivisitString(table[0].PasswordCredential));
                smt.EnableSsl = table[0].EnableSsl;
            }

            return smt;
        }

        public static MailMessage GetMessageFromReceptorId(int receptorid)
        {
            MailMessage mail = new MailMessage();
            DataAccess.ReceptorsDataSet.ReceptorsDataTable rectable;
            PACFD.Rules.Receptors receptor;
            PACFD.Rules.SmtpConfiguration smtpconfig;
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable smtptable;

            receptor = new Receptors();
            rectable = receptor.SelectByID(receptorid);
            receptor = null;

            if (rectable.Count < 1)
            {
                return mail;
            }

            smtpconfig = new SmtpConfiguration();
            smtptable = smtpconfig.Select();
            smtpconfig = null;

            mail.To.Add(rectable[0].Email);
            mail.IsBodyHtml = true;
            mail.Body = string.Empty;//"asdcnasdcbh";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.Subject = string.Empty;//"loasjdncajisd";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.From = new MailAddress(smtptable[0].NameCredential, smtptable[0].Name, System.Text.Encoding.UTF8);
            mail.Priority = MailPriority.Normal;
            mail.Bcc.Add(smtptable[0].Bcc);

            smtptable.Dispose();
            smtptable = null;
            rectable.Dispose();
            rectable = null;

            return mail;
        }

        public static MailMessage GetMessageFromUserId(int userid)
        {
            MailMessage mail = new MailMessage();
            DataAccess.UsersDataSet.UsersDataTable usertable;
            PACFD.Rules.Users user;
            PACFD.Rules.SmtpConfiguration smtpconfig;
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable smtptable;

            user = new Users();
            usertable = user.SelectByIDUsers(userid);
            user = null;

            if (usertable.Count < 1)
            {
                return mail;
            }

            smtpconfig = new SmtpConfiguration();
            smtptable = smtpconfig.Select();
            smtpconfig = null;

            mail.To.Add(usertable[0].UserName);
            mail.IsBodyHtml = true;
            mail.Body = "<b>hola mundo</b>";//string.Empty;//"asdcnasdcbh";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.Subject = "Correo Test"; // string.Empty;//"loasjdncajisd";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.From = new MailAddress(smtptable[0].NameCredential, smtptable[0].Name, System.Text.Encoding.UTF8);
            mail.Priority = MailPriority.Normal;
            mail.Bcc.Add(smtptable[0].Bcc);

            smtptable.Dispose();
            smtptable = null;
            usertable.Dispose();
            usertable = null;

            return mail;
        }

        /// <summary>
        /// Create a MailMessage object intance.
        /// </summary>
        /// <param name="dir">Path of the xml to read.</param>
        /// <param name="idname">ID of the mail configuration to use.</param>
        /// <returns>Return a MailMessage object.</returns>
        /// <exception cref="MailFromNotValidException">The From email is not valid.</exception>
        /// <exception cref="MailIDNotFoundException">
        /// Email configuration used to create MailMessage not found.
        /// </exception>
        /// <exception cref="MailToCcBccEmptyException">Email to, cc or bcc all are empty.</exception>
        /// <exception cref="System.Xml.XmlException">
        /// An error is fired on load or in the xml document analisys. The document can be empty.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// path is a zero-length string, contains only white space, or contains one
        /// or more invalid characters as defined by System.IO.Path.InvalidPathChars.
        ///</exception>
        /// <exception cref="System.ArgumentNullException">path is null.</exception>
        /// <exception cref="System.IO.PathTooLongException">
        /// The specified path, file name, or both exceed the system-defined maximum
        /// length. For example, on Windows-based platforms, paths must be less than
        /// 248 characters, and file names must be less than 260 characters.
        /// </exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">
        /// The specified path is invalid, (for example, it is on an unmapped drive).
        /// </exception>
        /// <exception cref="System.IO.IOException:">An I/O error occurred while opening the file.</exception>
        /// <exception cref="System.UnauthorizedAccessException">
        /// path specified a file that is read-only.  -or- This operation is not supported
        /// on the current platform.  -or- path specified a directory.  -or- The caller
        /// does not have the required permission.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">mode specified an invalid value.</exception>
        /// <exception cref="System.IO.FileNotFoundException">The file specified in path was not found.</exception>
        /// <exception cref="System.NotSupportedException">path is in an invalid format.</exception>
        public static MailMessage GetMessageFromXML(string path, string idname)
        {
            string cont = string.Empty;
            System.Xml.XmlDocument doc = null;

            if (System.IO.File.Exists(path))
            {
                try
                {
                    doc = new System.Xml.XmlDocument();
                    doc.LoadXml(System.IO.File.ReadAllText(path));

                }
                catch (Exception ex) { throw ex; }
            }
            else
            {
                throw new Exception("Mail path doesn't exist.");
            }

            return GetMessageFromXML(doc, idname);
        }
        /// <summary>
        /// Create a MailMessage object intance.
        /// </summary>
        /// <param name="dir">Stream of the xml to read.</param>
        /// <param name="idname">ID of the mail configuration to use.</param>
        /// <returns>Return a MailMessage object.</returns>
        /// <exception cref="MailFromNotValidException">The From email is not valid.</exception>
        /// <exception cref="MailIDNotFoundException">
        /// Email configuration used to create MailMessage not found.
        /// </exception>
        /// <exception cref="MailToCcBccEmptyException">Email to, cc or bcc all are empty.</exception>
        /// <exception cref="System.Xml.XmlException">
        /// An error is fired on load or in the xml document analisys. The document can be empty.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// path is a zero-length string, contains only white space, or contains one
        /// or more invalid characters as defined by System.IO.Path.InvalidPathChars.
        ///</exception>
        /// <exception cref="System.ArgumentNullException">path is null.</exception>
        /// <exception cref="System.IO.PathTooLongException">
        /// The specified path, file name, or both exceed the system-defined maximum
        /// length. For example, on Windows-based platforms, paths must be less than
        /// 248 characters, and file names must be less than 260 characters.
        /// </exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">
        /// The specified path is invalid, (for example, it is on an unmapped drive).
        /// </exception>
        /// <exception cref="System.IO.IOException:">An I/O error occurred while opening the file.</exception>
        /// <exception cref="System.UnauthorizedAccessException">
        /// path specified a file that is read-only.  -or- This operation is not supported
        /// on the current platform.  -or- path specified a directory.  -or- The caller
        /// does not have the required permission.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">mode specified an invalid value.</exception>
        /// <exception cref="System.IO.FileNotFoundException">The file specified in path was not found.</exception>
        /// <exception cref="System.NotSupportedException">path is in an invalid format.</exception>
        public static MailMessage GetMessageFromXML(System.Xml.XmlDocument document, string idname)
        {
            MailMessage mail = new MailMessage();

            System.Xml.XmlNode node;
            bool ishtml = false;
            string htmldir = string.Empty;
            bool onemtyerror_sendto = false;
            bool onhtmlnotfound_error = true;
            int priority = 0;
            bool nodefounded = false;

            if (document == null)
                throw new Exception("Document can't be null.");

            node = document.SelectSingleNode("mailbox");

            foreach (System.Xml.XmlNode item in node.ChildNodes)
            {
                if (item.Name.ToLower() != "email")
                    continue;

                if (item.Attributes["id"] == null)
                    continue;

                if (item.Attributes["id"].Value.ToLower() == idname.Trim())
                {
                    node = item;
                    nodefounded = true;
                    break;
                }
            }

            if (!nodefounded)
            {
                document = null;
                throw new MailIDNotFoundException("Email configuration not founded.");
            }

            //node = document.SelectSingleNode("email");

            if (node.SelectSingleNode("mail") != null)
            {
                if (node.SelectSingleNode("mail").Attributes["ishtml"] != null)
                {
                    if (!bool.TryParse(node.SelectSingleNode("mail").Attributes["ishtml"].Value, out ishtml))
                        mail.IsBodyHtml = false;
                    else
                        mail.IsBodyHtml = ishtml;
                }

                if (node.SelectSingleNode("mail").Attributes["htmldir"] != null)
                    htmldir = node.SelectSingleNode("mail").Attributes["htmldir"].Value;

                if (node.SelectSingleNode("mail").Attributes["priority"] != null)
                {
                    int.TryParse(node.SelectSingleNode("mail").Attributes["priority"].Value, out priority);

                    if (priority < 0 || priority > 2)
                        priority = 0;

                    mail.Priority = (MailPriority)Enum.Parse(typeof(MailPriority), priority.ToString());
                }

                if (node.SelectSingleNode("mail").Attributes["onhtmlnotfound-error"] != null)
                    if (!bool.TryParse(node.SelectSingleNode("mail").Attributes["onhtmlnotfound-error"].Value, out onhtmlnotfound_error))
                        onhtmlnotfound_error = true;

            }

            if (node.SelectSingleNode("from") != null)
            {
                if (node.SelectSingleNode("from").Attributes["value"] != null)
                {
                    if (Mail.MailSenderHelper.IsEmailValid(node.SelectSingleNode("from").Attributes["value"].Value))
                        mail.From = new MailAddress(node.SelectSingleNode("from").Attributes["value"].Value);
                    else
                        throw new MailFromNotValidException("Email address is not valid. " + node.SelectSingleNode("from").Attributes["value"].Value);
                }
                else
                {
                    throw new MailFromNotValidException("Email address is empty.");
                }

                if (node.SelectSingleNode("from").Attributes["alias"] != null)
                    mail.From = new MailAddress(mail.From.Address, node.SelectSingleNode("from").Attributes["alias"].Value,
                        System.Text.Encoding.UTF8);
            }

            if (node.SelectSingleNode("sendto") != null)
            {
                if (node.SelectSingleNode("sendto").Attributes["onempty-error"] != null)
                    if (!bool.TryParse(node.SelectSingleNode("sendto").Attributes["onempty-error"].Value, out onemtyerror_sendto))
                        onemtyerror_sendto = false;

                foreach (System.Xml.XmlNode item in node.SelectSingleNode("sendto").ChildNodes)
                {
                    if (item.Attributes["value"] == null)
                        continue;

                    if (!Mail.MailSenderHelper.IsEmailValid(item.Attributes["value"].Value.Trim()))
                        continue;

                    switch (item.Name.ToLower())
                    {
                        case "to": mail.To.Add(item.Attributes["value"].Value.Trim()); break;
                        case "cc": mail.CC.Add(item.Attributes["value"].Value.Trim()); break;
                        case "bcc": mail.Bcc.Add(item.Attributes["value"].Value.Trim()); break;
                    }
                }

                if (onemtyerror_sendto)
                {
                    if (mail.To.Count < 1 && mail.CC.Count < 1 && mail.Bcc.Count < 1)
                        throw new MailToCcBccEmptyException("Address (to, cc and bcc are empty) to send.");
                }
            }

            if (node.SelectSingleNode("subject") != null)
                if (node.SelectSingleNode("subject").Attributes["value"] != null)
                    mail.Subject = node.SelectSingleNode("subject").Attributes["value"].Value;

            mail.Body = string.Empty;//"asdcnasdcbh";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;

            node = null;
            document = null;

            if (htmldir[0] == '/')
                htmldir = htmldir.Remove(0, 1);

            htmldir = htmldir.Replace("/", "\\");

            try
            {
                htmldir = Mail.MailSenderHelper.LoadHTML(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + htmldir);
                //PACFD.Common.LogManager.WriteStackTrace(new Exception("Loading html body from: " + htmldir));
            }
            catch (Exception ex)
            {
                try
                {
                    //PACFD.Common.LogManager.WriteStackTrace(new Exception(System.AppDomain.CurrentDomain.BaseDirectory + htmldir));
                    htmldir = Mail.MailSenderHelper.LoadHTML(System.AppDomain.CurrentDomain.BaseDirectory + htmldir);
                    //PACFD.Common.LogManager.WriteStackTrace(new Exception("Loading html body from: " + htmldir));
                }
                catch (Exception ex2)
                {
                    System.Diagnostics.Debug.WriteLine(ex2.Message);
                    htmldir = string.Empty;

                    if (onhtmlnotfound_error)
                        throw ex;
                }
            }

            mail.Body = htmldir;

            return mail;
        }
        /// <summary>
        /// Load a HTML file.
        /// </summary>
        /// <param name="dir">File path of the html to load</param>
        /// <returns>Return a string value with the content of the file</returns>
        /// <exception cref="System.ArgumentException">
        /// path is a zero-length string, contains only white space, or contains one
        /// or more invalid characters as defined by System.IO.Path.InvalidPathChars.
        ///</exception>
        /// <exception cref="System.ArgumentNullException">path is null.</exception>
        /// <exception cref="System.IO.PathTooLongException">
        /// The specified path, file name, or both exceed the system-defined maximum
        /// length. For example, on Windows-based platforms, paths must be less than
        /// 248 characters, and file names must be less than 260 characters.
        /// </exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">
        /// The specified path is invalid, (for example, it is on an unmapped drive).
        /// </exception>
        /// <exception cref="System.IO.IOException:">An I/O error occurred while opening the file.</exception>
        /// <exception cref="System.UnauthorizedAccessException">
        /// path specified a file that is read-only.  -or- This operation is not supported
        /// on the current platform.  -or- path specified a directory.  -or- The caller
        /// does not have the required permission.
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">mode specified an invalid value.</exception>
        /// <exception cref="System.IO.FileNotFoundException">The file specified in path was not found.</exception>
        /// <exception cref="System.NotSupportedException">path is in an invalid format.</exception>
        internal static string LoadHTML(string dir)
        {
            if (System.IO.File.Exists(dir))
            {
                return System.IO.File.ReadAllText(dir);
            }

            return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        internal static bool IsEmailValid(string email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);

            if (re.IsMatch(email))
                return true;

            return false;
        }
        /// <summary>
        /// Get an EmailConfigurationCollection collection available in a xml file.
        /// </summary>
        /// <param name="dir">Path of the XML to read.</param>
        /// <returns>Return an EmailConfigurationCollection collection.</returns>
        public static EmailConfigurationCollection GetEmailConfigurations(string dir)
        {
            System.IO.Stream stream;

            try { stream = System.IO.File.Open(dir, System.IO.FileMode.Open); }
            catch (Exception ex) { throw ex; }

            return GetEmailConfigurations(stream);
        }
        /// <summary>
        /// Get an EmailConfigurationCollection collection available in a xml file.
        /// </summary>
        /// <param name="dir">XML to read.</param>
        /// <returns>Return an EmailConfigurationCollection collection.</returns>
        public static EmailConfigurationCollection GetEmailConfigurations(System.IO.Stream dir)
        {
            EmailConfigurationCollection c = new EmailConfigurationCollection();
            System.Xml.XmlDocument document;
            System.Xml.XmlNode node;

            dir.Seek(0, System.IO.SeekOrigin.Begin);
            document = new System.Xml.XmlDocument();

            try { document.Load(dir); }
            catch (Exception ex) { throw ex; }

            node = document.SelectSingleNode("mailbox");

            if (node == null)
                return c;

            foreach (System.Xml.XmlNode item in node.ChildNodes)
            {
                if (item.Name.ToLower().Trim() != "email")
                    continue;

                if (node.Attributes["id"] == null)
                    continue;

                if (node.Attributes["description"] == null)
                    c.Add(new EmailConfiguration(node.Attributes["id"].Value, string.Empty));
                else
                    c.Add(new EmailConfiguration(node.Attributes["id"].Value, node.Attributes["description"].Value));
            }

            return c;
        }
    }
}
