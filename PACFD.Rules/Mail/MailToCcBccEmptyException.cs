﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.Mail
{
    public class MailToCcBccEmptyException : Exception
    {
        public MailToCcBccEmptyException(string message) : base(message)
        {
        }
    }
}
