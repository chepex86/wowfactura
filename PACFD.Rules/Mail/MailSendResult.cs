﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.Mail
{
    public class MailSendResult
    {
        public Boolean IsSended { get; private set; }
        public Exception Error { get; private set; }

        public MailSendResult(bool send, Exception ex)
        {
            this.IsSended = send;
            this.Error = ex;
        }
    }
}
