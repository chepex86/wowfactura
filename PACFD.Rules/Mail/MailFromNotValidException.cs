﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.Mail
{
    public class MailFromNotValidException : Exception
    {
        public MailFromNotValidException(string message) : base(message)
        {
        }
    }
}
