﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class used to implement the ruler layer beetwin OzDataBase and PACFD.
    /// </summary> 
    /// <remarks>
    /// sp_OzCities
    /// </remarks>
    public class OzUnivisit
    {
        /// <summary>
        /// Get a DataAccess.OzUnivisitDataSet.SearchOzCitiesDataTable  table with the cities 
        /// filtered by name.
        /// </summary>
        /// <param name="cityname">City name to look for.</param>
        /// <returns>Return a DataAccess.OzUnivisitDataSet.SearchOzCitiesDataTable table.</returns>
        public DataAccess.OzUnivisitDataSet.SearchOzCitiesDataTable SearchCity(string cityname)
        {
            DataAccess.OzUnivisitDataSet.SearchOzCitiesDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.SearchOzCitiesDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.SearchOzCitiesTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.SearchOzCitiesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, cityname);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.OzUnivisitDataSet.OzGetAllCountriesDataTable table with all the 
        /// countries in the data base.
        /// </summary>
        /// <returns>Return a DataAccess.OzUnivisitDataSet.OzGetAllCountriesDataTable.</returns>
        public DataAccess.OzUnivisitDataSet.OzGetAllCountriesDataTable SelectAllCountries()
        {
            DataAccess.OzUnivisitDataSet.OzGetAllCountriesDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetAllCountriesDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCountriesTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCountriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select state by country ID.
        /// </summary>
        /// <param name="countryid">Country ID.</param>
        /// <returns>DataAccess.OzUnivisitDataSet.OzGetAllStateByCountryIDDataTable</returns>
        public DataAccess.OzUnivisitDataSet.OzGetAllStateByCountryIDDataTable SelectStateByCountryID(string countryid)
        {
            DataAccess.OzUnivisitDataSet.OzGetAllStateByCountryIDDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetAllStateByCountryIDDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllStateByCountryIDTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllStateByCountryIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, countryid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select municipality by state ID.
        /// </summary>
        /// <param name="stateid">State ID.</param>
        /// <returns>DataAccess.OzUnivisitDataSet.OzGetAllMunicipalityByStateIDDataTable</returns>
        public DataAccess.OzUnivisitDataSet.OzGetAllMunicipalityByStateIDDataTable SelectMunicipalityByStateID(int stateid)
        {
            DataAccess.OzUnivisitDataSet.OzGetAllMunicipalityByStateIDDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetAllMunicipalityByStateIDDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllMunicipalityByStateIDTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllMunicipalityByStateIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, stateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select city by municipality ID.
        /// </summary>
        /// <param name="municipalityid">Municipality ID.</param>
        /// <returns>DataAccess.OzUnivisitDataSet.OzGetAllCityByMunicipalityIDDataTable</returns>
        public DataAccess.OzUnivisitDataSet.OzGetAllCityByMunicipalityIDDataTable SelectCityByMunicipalityID(int municipalityid)
        {
            DataAccess.OzUnivisitDataSet.OzGetAllCityByMunicipalityIDDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetAllCityByMunicipalityIDDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCityByMunicipalityIDTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCityByMunicipalityIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, municipalityid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a DataAccess.OzUnivisitDataSet.OzGetAllCityByStateCodeDataTable table with the 
        /// city ID, city name, city code, municipality ID, municipality name, state ID state name,
        /// county ID and county name.
        /// </summary>
        /// <param name="code">City ID used to search.</param>
        /// <returns>
        /// return a DataAccess.OzUnivisitDataSet.OzGetAllCityByStateCodeDataTable table.
        /// </returns>
        public DataAccess.OzUnivisitDataSet.OzGetAllCityByStateCodeDataTable SelectCityByStateCode(string code)
        {
            DataAccess.OzUnivisitDataSet.OzGetAllCityByStateCodeDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetAllCityByStateCodeDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCityByStateCodeTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetAllCityByStateCodeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, code);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select city state by city ID.
        /// </summary>
        /// <param name="cityid">City ID.</param>
        /// <returns>DataAccess.OzUnivisitDataSet.OzGetCityStateCountryByCityIDDataTable</returns>
        public DataAccess.OzUnivisitDataSet.OzGetCityStateCountryByCityIDDataTable SelectCityStateCountryByCityID(int cityid)
        {
            DataAccess.OzUnivisitDataSet.OzGetCityStateCountryByCityIDDataTable table = new PACFD.DataAccess.OzUnivisitDataSet.OzGetCityStateCountryByCityIDDataTable();

            try
            {
                using (DataAccess.OzUnivisitDataSetTableAdapters.OzGetCityStateCountryByCityIDTableAdapter adapter =
                    new PACFD.DataAccess.OzUnivisitDataSetTableAdapters.OzGetCityStateCountryByCityIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, cityid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}