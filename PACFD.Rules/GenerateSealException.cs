﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public class SealGenerationException : Exception
    {
        public SealGenerationException(string message)
            : base(message)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EmptySealGenerationException : Exception
    {
        public EmptySealGenerationException(string message)
            : base(message)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LockedSealGenerationException : Exception
    {
        public LockedSealGenerationException(string message)
            : base(message)
        {

        }
    }

    public class CantReadPrivateKeySealGenerationException : Exception
    {
        public CantReadPrivateKeySealGenerationException(string message)
            : base(message)
        {

        }
    }

    public class PrivateKeyDoenstExistSealGenerationException : Exception
    {
        public PrivateKeyDoenstExistSealGenerationException(string message)
            : base(message)
        {

        }
    }
}
