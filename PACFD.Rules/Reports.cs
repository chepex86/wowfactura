﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.ReportsDataSetTableAdapters;
using PACFD.Common;
using System.IO;
using System.Configuration;
using System.Web;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public class Reports
    {
        #region Provider report

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public ReportsDataSet.ProviderMonthlyReportDataTable ProviderMonthlyReport(DateTime date)
        {
            ReportsDataSet.ProviderMonthlyReportDataTable table;

            table = new ReportsDataSet.ProviderMonthlyReportDataTable();
            this.ProviderMonthlyReport(table, date);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public bool ProviderMonthlyReport(ReportsDataSet.ProviderMonthlyReportDataTable table, DateTime d)
        {
            DateTime start = new DateTime(d.Year, d.Month, 1);
            DateTime end = new DateTime(d.Year, d.Month, 1).AddMonths(1).AddDays(-1);

            try
            {
                using (ProviderMonthlyReportTableAdapter adapter = new ProviderMonthlyReportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, start, end);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public bool ProviderMonthlyReportsUpdate(ReportsDataSet.ProviderMonthlyReportsDataTable table)
        {
            try
            {
                using (ProviderMonthlyReportsTableAdapter adapter = new ProviderMonthlyReportsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public ReportsDataSet.ProviderMonthlyReports_GetByDateDataTable GetReportByDate(int Year, byte month)
        {
            ReportsDataSet.ProviderMonthlyReports_GetByDateDataTable table;

            table = new ReportsDataSet.ProviderMonthlyReports_GetByDateDataTable();
            this.GetReportByDate(table, Year, month);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public bool GetReportByDate(ReportsDataSet.ProviderMonthlyReports_GetByDateDataTable table, int Year, byte month)
        {
            try
            {
                using (ProviderMonthlyReports_GetByDateTableAdapter adapter = new ProviderMonthlyReports_GetByDateTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, Year, month);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListDataTable GetReporstForList()
        {
            ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListDataTable table;

            table = new ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListDataTable();
            this.GetReporstForList(table);

            return table;
        }

        public bool GetReporstForList(ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListDataTable table)
        {
            try
            {
                using (ProviderMonthlyReports_GetByProviderIDForListTableAdapter adapter = new ProviderMonthlyReports_GetByProviderIDForListTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, 1);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public ReportsDataSet.ProviderMonthlyReports_GetByReportIDDataTable GetReporstByReportID(int id)
        {
            ReportsDataSet.ProviderMonthlyReports_GetByReportIDDataTable table;

            table = new ReportsDataSet.ProviderMonthlyReports_GetByReportIDDataTable();
            this.GetReporstByReportID(table, id);

            return table;
        }

        public bool GetReporstByReportID(ReportsDataSet.ProviderMonthlyReports_GetByReportIDDataTable table, int id)
        {
            try
            {
                using (ProviderMonthlyReports_GetByReportIDTableAdapter adapter = new ProviderMonthlyReports_GetByReportIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        #endregion

        #region Biller
        #region BillerMonthlyReport by Date
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public ReportsDataSet.BillerMonthlyReportDataTable BillerMonthlyReport(int BillerID, DateTime date)
        {
            ReportsDataSet.BillerMonthlyReportDataTable table;

            table = new ReportsDataSet.BillerMonthlyReportDataTable();
            this.ProviderMonthlyReport(table, BillerID, date);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public bool ProviderMonthlyReport(ReportsDataSet.BillerMonthlyReportDataTable table, int BillerID, DateTime d)
        {
            DateTime start = new DateTime(d.Year, d.Month, 1, 0, 0, 0);
            DateTime end = new DateTime(d.Year, d.Month, 1, 23, 59, 59).AddMonths(1).AddDays(-1);

            try
            {
                using (BillerMonthlyReportTableAdapter adapter = new BillerMonthlyReportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, start, end);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public bool BillerMonthlyReportsUpdate(ReportsDataSet.MonthlyReportsDataTable table)
        {
            try
            {
                using (MonthlyReportsTableAdapter adapter = new MonthlyReportsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <param name="Year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public ReportsDataSet.BillerMonthlyReports_GetByDateDataTable GetReportByDate(int BillerID, int Year, byte month)
        {
            ReportsDataSet.BillerMonthlyReports_GetByDateDataTable table;

            table = new ReportsDataSet.BillerMonthlyReports_GetByDateDataTable();
            this.GetReportByDate(table, BillerID, Year, month);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <param name="Year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public bool GetReportByDate(ReportsDataSet.BillerMonthlyReports_GetByDateDataTable table, int BillerID, int Year, byte month)
        {
            try
            {
                using (BillerMonthlyReports_GetByDateTableAdapter adapter = new BillerMonthlyReports_GetByDateTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, Year, month);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable GetReportsByBillerID(int BillerID)
        {
            ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable table;

            table = new ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable();
            this.GetReportsByBillerID(table, BillerID);

            return table;
        }

        public bool GetReportsByBillerID(ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable table, int BillerID)
        {
            try
            {
                using (MonthlyReports_GetByBillerIDForListTableAdapter adapter = new MonthlyReports_GetByBillerIDForListTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public ReportsDataSet.MonthlyReports_GetByBillerIDForDetailDataTable GetReportsForDetails(int BillerID, int ReportID)
        {
            ReportsDataSet.MonthlyReports_GetByBillerIDForDetailDataTable table;

            table = new ReportsDataSet.MonthlyReports_GetByBillerIDForDetailDataTable();
            this.GetReportsForDetails(table, BillerID, ReportID);

            return table;
        }

        public bool GetReportsForDetails(ReportsDataSet.MonthlyReports_GetByBillerIDForDetailDataTable table, int BillerID, int ReportID)
        {
            try
            {
                using (MonthlyReports_GetByBillerIDForDetailTableAdapter adapter = new MonthlyReports_GetByBillerIDForDetailTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, ReportID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <returns></returns>
        public ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable LastReportGenerated(int BillerID)
        {
            ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable table;

            table = new ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable();
            this.LastReportGenerated(table, BillerID);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <returns></returns>
        public bool LastReportGenerated(ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable table, int BillerID)
        {
            try
            {
                using (BillerMonthlyReports_LastReportGenerated_ByBillerIDTableAdapter adapter = new BillerMonthlyReports_LastReportGenerated_ByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
            return true;
        }
        #endregion

        public String GenerateReport(int CurrentBillerID, int Year, int Month, int UserID, String UserName)
        {
            ReportsDataSet.BillerMonthlyReportDataTable t;

            t = this.BillerMonthlyReport(CurrentBillerID, new DateTime(Year, Month, 1));
            if (t.Count < 0)
                return null;

            String c = "", p = "|", e = "0.00", d = "dd/MM/yyyy hh:mm:ss",
                   r = "||||" + System.Environment.NewLine;

            for (int i = 0; i < t.Count; i += 1)
                c += p + t[i].ReceptorRFC +
                     p + t[i].Serial.Trim() +
                     p + t[i].Folio +
                     p + t[i].ApprovalNumber +
                     p + t[i].BillingDate.ToString(d) +
                     p + t[i].Total.ToString(e) +
                     p + t[i].AmountTax.ToString(e) +
                     p + "1" +
                     p + t[i].BillingType.Substring(0, 1).ToUpper() +
                     (t[i].Active ? r : r +
                     p + t[i].ReceptorRFC +
                     p + t[i].Serial.Trim() +
                     p + t[i].Folio +
                     p + t[i].ApprovalNumber +
                     p + t[i].BillingDate.ToString(d) +
                     p + t[i].Total.ToString(e) +
                     p + t[i].AmountTax.ToString(e) +
                     p + "0" +
                     p + t[i].BillingType.Substring(0, 1).ToUpper() + r);

            ReportsDataSet ds = new ReportsDataSet();
            ReportsDataSet.MonthlyReportsRow drReport = ds.MonthlyReports.NewMonthlyReportsRow();

            try
            {
                drReport.BillerID = CurrentBillerID;
                drReport.Report = c;
                drReport.CreationDate = DateTime.Now;
                drReport.Month = Convert.ToByte(Month);
                drReport.Year = Year;

                ds.MonthlyReports.AddMonthlyReportsRow(drReport);
                this.BillerMonthlyReportsUpdate(ds.MonthlyReports);

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(UserID, String.Format("El usuario {0} genero un reporte mensual para el SAT de la fecha {1}-{2}",
                    UserName, Month, Year), CurrentBillerID, null, ds.GetXml());
                #endregion

                return c + "_" + t.Count;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return null;
            }
        }

        public ReportsDataSet.MonthlyReportsDataTable SelectByReportID(int ReportID)
        {
            ReportsDataSet.MonthlyReportsDataTable ta;
            ta = new ReportsDataSet.MonthlyReportsDataTable();

            this.SelectByReportID(ta, ReportID);

            return ta;
        }
        public bool SelectByReportID(ReportsDataSet.MonthlyReportsDataTable ta, int ReportID)
        {
            try
            {
                using (MonthlyReportsTableAdapter adapter = new MonthlyReportsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(ta, ReportID);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        public ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable SelectFirstBillingByBillerID(int BillerID)
        {
            ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable table;

            table = new ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable();
            this.SelectFirstBillingByBillerID(table, BillerID);

            return table;
        }
        public bool SelectFirstBillingByBillerID(ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable table, int BillerID)
        {
            try
            {
                using (MonthlyReports_GetFirstBillingByBillerIDTableAdapter adapter = new MonthlyReports_GetFirstBillingByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        public ReportsDataSet.MonthlyReports_GetAlertSentReportDataTable SelectAlertSent(int BillerID, int StartMonth, int StartYear, int EndMonth, int EndYear)
        {
            ReportsDataSet.MonthlyReports_GetAlertSentReportDataTable table;

            table = new ReportsDataSet.MonthlyReports_GetAlertSentReportDataTable();
            this.SelectAlertSent(table, BillerID, StartMonth, StartYear, EndMonth, EndYear);

            return table;
        }
        public bool SelectAlertSent(ReportsDataSet.MonthlyReports_GetAlertSentReportDataTable ta, int BillerID, int StartMonth, int StartYear, int EndMonth, int EndYear)
        {
            try
            {
                using (MonthlyReports_GetAlertSentReportTableAdapter adapter = new MonthlyReports_GetAlertSentReportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(ta, BillerID, StartMonth, StartYear, EndMonth, EndYear);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        public ReportsDataSet.MonthlyReports_GetByDateDataTable SelectAlertGenerate(int BillerID, int StartMonth, int StartYear, int EndMonth, int EndYear)
        {
            ReportsDataSet.MonthlyReports_GetByDateDataTable table;

            table = new ReportsDataSet.MonthlyReports_GetByDateDataTable();
            this.SelectAlertGenerate(table, BillerID, StartMonth, StartYear, EndMonth, EndYear);

            return table;
        }
        public bool SelectAlertGenerate(ReportsDataSet.MonthlyReports_GetByDateDataTable t, int id, int sMonth, int sYear, int eMonth, int eYear)
        {
            try
            {
                using (MonthlyReports_GetByDateTableAdapter adapter = new MonthlyReports_GetByDateTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(t, id, sMonth, sYear, eMonth, eYear);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Get a list of billings active or inactive by group and date.
        /// </summary>
        /// <returns></returns>
        public DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable SelectBillingByGroupAndDate(int? groupid, DateTime? start, DateTime? end)
        {
            DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable table = new ReportsDataSet.Reports_InvoiceCutDataTable();
            
            try
            {
                using (DataAccess.ReportsDataSetTableAdapters.Reports_InvoiceCutTableAdapter adapter = new Reports_InvoiceCutTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, groupid, start, end);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}