﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{

    public class BillersOthersConfigurations
    {

        public bool SelectByBillerID(BillersDataSet.BillersOthersConfigurationsDataTable table, int billerId)
        {
            if (table != null)
                table = new BillersDataSet.BillersOthersConfigurationsDataTable();

            try
            {
                using (DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter adapter =
                    new DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.FillByBillerID(table, billerId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return false;
        }

        public bool SelectByID(BillersDataSet.BillersOthersConfigurationsDataTable table, int id)
        {
            if (table != null)
                table = new BillersDataSet.BillersOthersConfigurationsDataTable();

            try
            {
                using (DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter adapter =
               new DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return false;
        }

        public bool Update(BillersDataSet.BillersOthersConfigurationsDataTable table)
        {
            try
            {
                using (DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter adapter =
                    new DataAccess.BillersDataSetTableAdapters.BillersOthersConfigurationsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }


    }
}
