﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities; 
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Ruler layer class for BillingPayments
    /// </summary>
    public class BillingPayments
    {
        /// <summary>
        /// Search a BilllingPayment
        /// </summary>
        /// <param name="billerid">Biller ID owner of the billing.</param>
        /// <param name="datetime">Nullable date time used as filter.</param>
        /// <param name="reference">Reference used as filter.</param>
        /// <param name="ispaid">Nullable ispaid used as filter.</param>
        /// <returns>Return a table with the numer of rows founded.</returns>
        public DataAccess.BillingPaymentsDataSet.SearchDataTable GetByBillingID(int billingid, DateTime? datetime, string reference, bool? ispaid)
        {
            DataAccess.BillingPaymentsDataSet.SearchDataTable table =
                new PACFD.DataAccess.BillingPaymentsDataSet.SearchDataTable();

            try
            {
                using (DataAccess.BillingPaymentsDataSetTableAdapters.SearchTableAdapter adapter =
                    new PACFD.DataAccess.BillingPaymentsDataSetTableAdapters.SearchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billingid, datetime, reference, ispaid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Update, delet and modify method.
        /// </summary>
        /// <param name="table">Table with the rows affected.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.BillingPaymentsDataSet.BillingPaymentsDataTable table)
        {
            try
            {
                using (DataAccess.BillingPaymentsDataSetTableAdapters.BillingPaymentsTableAdapter adapter =
                    new PACFD.DataAccess.BillingPaymentsDataSetTableAdapters.BillingPaymentsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
    }
}
