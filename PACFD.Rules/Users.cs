﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.UsersDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public class Users
    {

        #region UpdateUsers
        public bool UpdateUsers(UsersDataSet.UsersDataTable dt)
        {
            try
            {
                using (UsersTableAdapter adapter = new UsersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(dt);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public int UpdateUser(UsersDataSet.UsersDataTable dt)
        {
            int UserID = -1;

            try
            {
                using (UsersTableAdapter adapter = new UsersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(dt);
                    UserID = dt[0].UserID;
                }

                return UserID;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return -1;
            }
        }
        #endregion
        #region SelectByIDUsers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UsersDataSet.UsersDataTable SelectByIDUsers(int id)
        {
            UsersDataSet.UsersDataTable table;

            table = new UsersDataSet.UsersDataTable();
            this.SelectByIDUsers(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByIDUsers(UsersDataSet.UsersDataTable table, int id)
        {
            try
            {
                using (UsersTableAdapter adapter = new UsersTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select User By Searching
        /// <summary>
        /// Search user by role and email.
        /// </summary>
        /// <param name="role">Role of the user.</param>
        /// <param name="email">Email to search.</param>        
        /// <returns></returns>
        public UsersDataSet.Users_GetBySearchingDataTable SelectUserBySearching(int? role, string email)
        {
            UsersDataSet.Users_GetBySearchingDataTable table;

            table = new UsersDataSet.Users_GetBySearchingDataTable();
            this.SelectUserBySearching(table, role, email);

            return table;
        }
        /// <summary>
        /// Search user by role and email.
        /// </summary>
        /// <param name="table">Table to be fill.</param>
        /// <param name="role">Role of the user.</param>
        /// <param name="email">Email to search.</param>        
        /// <returns></returns>
        public bool SelectUserBySearching(UsersDataSet.Users_GetBySearchingDataTable table, int? role, string email)
        {
            try
            {
                using (Users_GetBySearchingTableAdapter adapter = new Users_GetBySearchingTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, role, email);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion
        #region Select Client By Searching
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public UsersDataSet.Clients_GetBySearchingDataTable SelectClientBySearching(string email, string name)
        {
            UsersDataSet.Clients_GetBySearchingDataTable table;

            table = new UsersDataSet.Clients_GetBySearchingDataTable();
            this.SelectClientBySearching(table, email, name);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SelectClientBySearching(UsersDataSet.Clients_GetBySearchingDataTable table, string email, string name)
        {
            try
            {
                using (Clients_GetBySearchingTableAdapter adapter = new Clients_GetBySearchingTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, email, name);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select By UserName
        public UsersDataSet.Users_GetByUserNameDataTable SelectByUserName(string username)
        {
            UsersDataSet.Users_GetByUserNameDataTable table;

            table = new UsersDataSet.Users_GetByUserNameDataTable();
            this.SelectByIDUsers(table, username);

            return table;
        }

        public bool SelectByIDUsers(UsersDataSet.Users_GetByUserNameDataTable table, string username)
        {
            try
            {
                using (Users_GetByUserNameTableAdapter adapter = new Users_GetByUserNameTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, username);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Login
        public bool SelectUserforLogin(UsersDataSet.LoginDataTable table, string userName, string password)
        {
            try
            {
                using (LoginTableAdapter adapter = new LoginTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, userName, password);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Select a user for login
        /// </summary>
        /// <param name="userName">User to look for.</param>
        /// <param name="password">password to match the user.</param>
        /// <returns>Return a table with the user founded</returns>
        public UsersDataSet.LoginDataTable SelectUserforLogin(string userName, string password)
        {
            UsersDataSet.LoginDataTable table = new UsersDataSet.LoginDataTable();

            try
            {
                using (LoginTableAdapter adapter = new LoginTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, userName, password);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        #endregion

        #region Unlock
        public bool SelectUserforUnlock(UsersDataSet.LoginDataTable table, string userName)
        {
            try
            {
                using (LoginTableAdapter adapter = new LoginTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.FillByUserName(table, userName);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region UserBiller
        public bool Update_UserBiller(UsersDataSet.UserBillerDataTable table)
        {
            try
            {
                using (UserBillerTableAdapter adapter = new UserBillerTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public UsersDataSet.UserBillerDataTable GetUserBillerByUserID(int userid)
        {
            UsersDataSet.UserBillerDataTable table = new UsersDataSet.UserBillerDataTable();
            try
            {
                using (UserBillerTableAdapter adapter = new UserBillerTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, userid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public UsersDataSet.GetUserBillerSelectedDataTable GetUserBillerSelected(int userid)
        {
            UsersDataSet.GetUserBillerSelectedDataTable table;

            table = new UsersDataSet.GetUserBillerSelectedDataTable();
            this.GetUserBillerSelected(table, userid);

            return table;
        }

        private bool GetUserBillerSelected(UsersDataSet.GetUserBillerSelectedDataTable table, int userid)
        {
            try
            {
                using (GetUserBillerSelectedTableAdapter adapter = new GetUserBillerSelectedTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, userid, true);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool DeleteAllUserBiller(int userid)
        {
            try
            {
                using (QueriesTableAdapter adapter = new QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.spUserBiller_DeleteAllByUserID(userid);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion
    }
}