﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.SerialDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public class Series
    {
        #region Update Folios
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool UpdateFolios(SerialDataSet ds)
        {
            try
            {
                using (SerialTableAdapter taSerial = new SerialTableAdapter())
                {
                    using (FoliosTableAdapter taFolios = new FoliosTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(taFolios);
                        Tool.RemoveOwnerSqlCommand(taSerial);

                        taSerial.Update(ds.Serial);
                        taFolios.Update(ds.Folios);
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        public bool UpdateFolios(SerialDataSet.FoliosDataTable table)
        {
            try
            {
                using (FoliosTableAdapter taFolios = new FoliosTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taFolios);
                    taFolios.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion
        #region Update Serial
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool UpdateSerial(SerialDataSet.SerialDataTable table)
        {
            try
            {
                using (SerialTableAdapter taSerial = new SerialTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taSerial);
                    taSerial.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion
        #region Select By IDSerial
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.SerialDataTable SelectByIDSerial(int id)
        {
            SerialDataSet.SerialDataTable table;

            table = new SerialDataSet.SerialDataTable();
            this.SelectByIDSerial(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByIDSerial(SerialDataSet.SerialDataTable table, int id)
        {
            try
            {
                using (SerialTableAdapter adapter = new SerialTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Get Folios Used
        private bool GetFoliosUsed(PACFD.DataAccess.SerialDataSet.Folios_GetFoliosUsedDataTable table, int SerialID)
        {
            try
            {
                using (Folios_GetFoliosUsedTableAdapter adapter = new Folios_GetFoliosUsedTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, SerialID);
                }
                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public PACFD.DataAccess.SerialDataSet.Folios_GetFoliosUsedDataTable GetFoliosUsed(int SerialID)
        {
            PACFD.DataAccess.SerialDataSet.Folios_GetFoliosUsedDataTable ta;

            ta = new SerialDataSet.Folios_GetFoliosUsedDataTable();
            this.GetFoliosUsed(ta, SerialID);

            return ta;
        }
        #endregion

        #region Set Use By Folio
        public bool SetFolioToUsed(int FolioID)
        {
            Series series = new Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.FoliosDataTable ta = SelectByIDFolio(FolioID);

            if (ta.Count < 1)
                return false;

            ta[0].Used = true;
            ta[0].FilledDate = DateTime.Now;
            series.UpdateFolios(ta);

            return true;
        }
        #endregion

        #region Select By IDFolio
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.FoliosDataTable SelectByIDFolio(int id)
        {
            SerialDataSet.FoliosDataTable table;

            table = new SerialDataSet.FoliosDataTable();
            this.SelectByIDFolio(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByIDFolio(SerialDataSet.FoliosDataTable table, int id)
        {
            try
            {
                using (FoliosTableAdapter adapter = new FoliosTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Searching Last SerialID
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SerialDataSet.Serial_GetLastIDDataTable SearchingLastSerialID(int BillerID, int? BranchID, String serial)
        {
            SerialDataSet.Serial_GetLastIDDataTable table;

            table = new SerialDataSet.Serial_GetLastIDDataTable();
            this.SearchingLastSerialID(table, BillerID, BranchID, serial);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="role"></param>
        /// <param name="email"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SearchingLastSerialID(SerialDataSet.Serial_GetLastIDDataTable table, int BillerID, int? BranchID, String serial)
        {
            try
            {
                if (BranchID == -1)
                    BranchID = null;

                using (Serial_GetLastIDTableAdapter adapter = new Serial_GetLastIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, serial, BranchID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select By IDBiller
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.Serial_GetByFkBillerIDCustomDataTable SelectByIDBiller(int billerid, int branchid)
        {
            SerialDataSet.Serial_GetByFkBillerIDCustomDataTable table;

            table = new SerialDataSet.Serial_GetByFkBillerIDCustomDataTable();
            this.SelectByIDBiller(table, billerid, branchid);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByIDBiller(SerialDataSet.Serial_GetByFkBillerIDCustomDataTable table, int billerid, int branchid)
        {
            try
            {
                using (Serial_GetByFkBillerIDCustomTableAdapter adapter = new Serial_GetByFkBillerIDCustomTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, branchid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Get Active By IDBiller
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.Serial_GetActiveDataTable SerialGetActiveByIDBiller(int BillerID, int BranchID)
        {
            SerialDataSet.Serial_GetActiveDataTable table;

            table = new SerialDataSet.Serial_GetActiveDataTable();
            this.SerialGetActiveByIDBiller(table, BillerID, BranchID);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SerialGetActiveByIDBiller(SerialDataSet.Serial_GetActiveDataTable table, int BillerID, int BranchID)
        {
            try
            {
                using (Serial_GetActiveTableAdapter adapter = new Serial_GetActiveTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, BranchID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Select By Searching
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <param name="year"></param>
        /// <param name="serial"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public SerialDataSet.Serial_GetBySearchingDataTable SelectBySearching(int BillerID, int BranchID, int? year, string serial, int? active)
        {
            SerialDataSet.Serial_GetBySearchingDataTable table;

            table = new SerialDataSet.Serial_GetBySearchingDataTable();
            this.SelectBySearching(table, BillerID, BranchID, year, serial, active);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectBySearching(SerialDataSet.Serial_GetBySearchingDataTable table, int BillerID, int BranchID, int? year, string serial, int? active)
        {
            try
            {
                using (Serial_GetBySearchingTableAdapter adapter = new Serial_GetBySearchingTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, BranchID, year, serial, active);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Serial-Folios Get by IDSerial
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.Serial_DetailsDataTable Serial_Details(int id)
        {
            SerialDataSet.Serial_DetailsDataTable table;

            table = new SerialDataSet.Serial_DetailsDataTable();
            this.Serial_Details(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Serial_Details(SerialDataSet.Serial_DetailsDataTable table, int id)
        {
            try
            {
                using (Serial_DetailsTableAdapter adapter = new Serial_DetailsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Un used Serial-Folios Get by IDBiller
        /// <summary>
        /// Get an unused serai number and folio.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the serial.</param>
        /// <returns>Return a table with the forst non used serial and folio numbre.</returns>
        public SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable GetUnusedSerialByBillerID(int billerID, int? branchID, bool payment = false)
        {
            SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table;

            table = new SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable();
            if (branchID != null && branchID == -1) branchID = null;

            this.GetUnusedSerialByBillerID(table, billerID, branchID,payment);

            return table;
        }
        /// <summary>
        /// Get an unused serial number and folio.
        /// </summary>
        /// <param name="table">Table to be filled.</param>
        /// <param name="id">Biller ID owner of the serial.</param>
        /// <returns>Return true if succes else false.</returns>
        public bool GetUnusedSerialByBillerID(SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table, int billerID, int? branchID, bool payment = false)
        {
            try
            {
                if (branchID != null && branchID == -1)
                    branchID = null;

                using (Serial_GetUnusedFolioByBillerIDTableAdapter adapter = new Serial_GetUnusedFolioByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerID, branchID,payment);
                }
            }
            catch (Exception ex)
            { 
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Un used Serial-Folios Get by IDBiller FOR PPD
        /// <summary>
        /// Get an unused serai number and folio.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the serial.</param>
        /// <returns>Return a table with the forst non used serial and folio numbre.</returns>
        public SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable GetUnusedSerialByBillerIDPayments(int billerID, int? branchID, bool payment)
        {
            SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table;

            table = new SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable();
            if (branchID != null && branchID == -1) branchID = null;

            this.GetUnusedSerialByBillerIDPayments(table, billerID, branchID,payment);

            return table;
        }
        /// <summary>
        /// Get an unused serial number and folio.
        /// </summary>
        /// <param name="table">Table to be filled.</param>
        /// <param name="id">Biller ID owner of the serial.</param>
        /// <returns>Return true if succes else false.</returns>
        public bool GetUnusedSerialByBillerIDPayments(SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table, int billerID, int? branchID,bool payment)
        {
            try
            {
                if (branchID != null && branchID == -1)
                    branchID = null;

                using (Serial_GetUnusedFolioByBillerIDTableAdapter adapter = new Serial_GetUnusedFolioByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerID, branchID, payment);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Get Info By BillerID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SerialDataSet.Serial_GetInfoByBillerIDDataTable SerialGetInfoByBillerID(int billerID, int? branchID)
        {
            SerialDataSet.Serial_GetInfoByBillerIDDataTable table;

            table = new SerialDataSet.Serial_GetInfoByBillerIDDataTable();
            this.SerialGetInfoByBillerID(table, billerID, branchID);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SerialGetInfoByBillerID(SerialDataSet.Serial_GetInfoByBillerIDDataTable table, int billerID, int? branchID)
        {
            try
            {
                using (Serial_GetInfoByBillerIDTableAdapter adapter = new Serial_GetInfoByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerID, branchID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion
    }
}
