﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.Rules
{
    /// <summary>
    /// CFD version types
    /// </summary>
    public enum ElectronicBillingType : int
    {
        /// <summary>
        /// Default billing type 2.0.
        /// </summary>
        CFD = 0,
        /// <summary>
        /// Billing type. CFDI Version 3.0.
        /// </summary>
        CFDI = 1,
        /// <summary>
        /// Qr billing type 1.0.
        /// </summary>
        CBB = 2,
        /// <summary>
        /// Billing type. CFD Version 2.2.
        /// </summary>
        CFD2_2 = 3,
        /// <summary>
        /// Billing type. CFDI Version 3.2.
        /// </summary>
        CFDI3_2 = 4,
        /// <summary>
        /// Billing type. CFDI Version 3.3.
        /// </summary>
        CFDI3_3 = 5,
        /// <summary>
        /// Billing type. CFDI Version 4.0.
        /// </summary>
        /// 
        CFDI4_0 = 6,
        /// <summary>
        /// Indeterminate.
        /// </summary>
        /// 
        Indeterminate = 99

    }
}
