﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules
{
    /// <summary>
    /// Flag used for generate the original string
    /// </summary>
    public enum OriginalStringVersionFlag
    {
        Unknown = 0,
        v20 = 20,
        v30 = 30,
        v40 = 40
    }
}
