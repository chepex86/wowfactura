﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class layer of branches
    /// </summary>
    public class Branches
    {
        /// <summary>
        /// Select branch by biller ID.
        /// </summary>
        /// <param name="billerid">Biller ID used as filter.</param>
        /// <returns>BranchesDataSet.GetByBillerIDDataTable </returns>
        public BranchesDataSet.GetByBillerIDDataTable SelectByBillerID(int billerid)
        {
            BranchesDataSet.GetByBillerIDDataTable table = new BranchesDataSet.GetByBillerIDDataTable();

            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.GetByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.BranchesDataSetTableAdapters.GetByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select a branch by ID.
        /// </summary>
        /// <param name="branchid">Branch ID.</param>
        /// <returns>BranchesDataSet.BranchDataTable</returns>
        public BranchesDataSet.BranchDataTable SelectByID(int branchid)
        {
            BranchesDataSet.BranchDataTable table = new BranchesDataSet.BranchDataTable();

            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.BranchTableAdapter adapter =
                    new PACFD.DataAccess.BranchesDataSetTableAdapters.BranchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, branchid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Update, modify, insert or delete a branch(es).
        /// </summary>
        /// <param name="table">Table with the branch(es) to modify.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(BranchesDataSet.BranchDataTable table)
        {
            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.BranchTableAdapter adapter =
                    new PACFD.DataAccess.BranchesDataSetTableAdapters.BranchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Search a branch(es).
        /// </summary>
        /// <param name="branchid">Branch ID.</param>
        /// <param name="code">Code of the branch.</param>
        /// <param name="billerid">Billing ID owner of the branch.</param>
        /// <param name="address">Address of the branch.</param>
        /// <param name="externalNumber">External branch number.</param>
        /// <param name="internalNumber">Internal branch number.</param>
        /// <param name="colony">Colony of the branch.</param>
        /// <param name="location">Locaiton of the branch.</param>
        /// <param name="reference">Reference of the brach.</param>
        /// <param name="municipality">Municipality of the branch.</param>
        /// <param name="state">State of the branch.</param>
        /// <param name="country">Country of the branch.</param>
        /// <param name="zipcode">Zipcode of the branch.</param>
        /// <param name="name">Name of the branch.</param>
        /// <param name="active">Active state of the branch.</param>
        /// <returns>BranchesDataSet.SearchDataTable</returns>
        public BranchesDataSet.SearchDataTable Search(global::System.Nullable<int> branchid, string code, global::System.Nullable<int> billerid,
                    string address, string externalNumber, string internalNumber, string colony, string location, string reference, string municipality,
                     string state, string country, string zipcode, string name, global::System.Nullable<bool> active)
        {
            BranchesDataSet.SearchDataTable table = new BranchesDataSet.SearchDataTable();

            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.SearchTableAdapter adapter =
                    new PACFD.DataAccess.BranchesDataSetTableAdapters.SearchTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, branchid, code, billerid, address, externalNumber, internalNumber, colony, location, reference, municipality, state
                      , country, zipcode, name, active);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Set the active state of a branch.
        /// </summary>
        /// <param name="branchid">Branch ID to be modify.</param>
        /// <param name="state">New state of the branch.</param>
        /// <returns>If success return true else false.</returns>
        public bool SetActive(int branchid, bool state)
        {
            if (branchid < 1)
            { return false; }

            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.QueriesTableAdapter query = new PACFD.DataAccess.BranchesDataSetTableAdapters.QueriesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(query);
                    query.spBranches_SetActive(branchid, state);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Select a branch by code.
        /// </summary>
        /// <param name="billerid">Biller ID owner of the branch.</param>
        /// <param name="branchid">Optional branch ID.</param>
        /// <param name="code">Code of the branch.</param>
        /// <returns>BranchesDataSet.GetByCodeDataTable</returns>
        public BranchesDataSet.GetByCodeDataTable SelectByCode(int billerid, int? branchid, string code)
        {
            BranchesDataSet.GetByCodeDataTable table = new BranchesDataSet.GetByCodeDataTable();

            try
            {
                using (DataAccess.BranchesDataSetTableAdapters.GetByCodeTableAdapter adapter =
                    new PACFD.DataAccess.BranchesDataSetTableAdapters.GetByCodeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, branchid, code, billerid);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return table;
        }

    }
}
