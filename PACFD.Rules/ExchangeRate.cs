﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Rules.mx.org.banxico.www;
using PACFD.Common;
using System.Data;
using System.Xml;
#endregion

namespace PACFD.Rules
{
    public class ExchangeRate
    {
        private DgieWS ExchangeRateWS { set; get; }

        public ExchangeRate()
        {
            this.ExchangeRateWS = new DgieWS();
        }

        public DataTable ExchangeRate1USDtoMXN()
        {
            String exchangerate = String.Empty;
            DataSet ds = new DataSet();

            try
            {
                exchangerate = this.ExchangeRateWS.tiposDeCambioBanxico();
                ds = this.ReadXML(exchangerate);

                return ds.Tables["Obs"];
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return null;
            }
        }

        #region Read XML
        private DataSet ReadXML(String strXML)
        {
            DataSet ds = new DataSet();
            XmlDocument xmldoc = new XmlDocument();

            try
            {
                xmldoc.LoadXml(strXML);

                using (XmlReader reader = new XmlNodeReader(xmldoc))
                {
                    ds.ReadXml(reader);
                    reader.Close();
                }

                return ds;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return null;
            }
        }
        #endregion
    }
}