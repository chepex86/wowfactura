﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.QRCodeLib.data
{
    public interface QRCodeImage
    {
        int Width { get; }
        int Height { get; }
        int getPixel(int x, int y);
    }
}