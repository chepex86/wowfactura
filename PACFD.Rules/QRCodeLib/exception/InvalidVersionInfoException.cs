﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.QRCodeLib.exception
{
    [Serializable]
    public class InvalidVersionInfoException : VersionInformationException
    {
        internal String message = null;
        public override String Message
        {
            get
            {
                return message;
            }

        }

        public InvalidVersionInfoException(String message)
        {
            this.message = message;
        }
    }
}
