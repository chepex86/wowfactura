﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.QRCodeLib.exception
{
    [Serializable]
    public class SymbolNotFoundException : System.ArgumentException
    {
        internal String message = null;

        public override String Message
        {
            get
            {
                return message;
            }

        }

        public SymbolNotFoundException(String message)
        {
            this.message = message;
        }
    }
}
