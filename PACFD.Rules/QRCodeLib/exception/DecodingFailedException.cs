﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PACFD.Rules.QRCodeLib.exception
{
    [Serializable]
    public class DecodingFailedException : System.ArgumentException
    {
        internal String message = null;

        public override String Message
        {
            get
            {
                return message;
            }

        }

        public DecodingFailedException(String message)
        {
            this.message = message;
        }
    }
}
