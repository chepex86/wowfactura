﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Line = PACFD.Rules.QRCodeLib.geom.Line;
using Point = PACFD.Rules.QRCodeLib.geom.Point;

namespace PACFD.Rules.QRCodeLib.util
{
    /* 
	* This class must be a "edition independent" class for debug information controll.
	* I think it's good idea to modify this class with a adapter pattern
	*/
    public class DebugCanvasAdapter : DebugCanvas
    {
        public virtual void println(String string_Renamed)
        {
        }

        public virtual void drawPoint(Point point, int color)
        {
        }

        public virtual void drawCross(Point point, int color)
        {
        }

        public virtual void drawPoints(Point[] points, int color)
        {
        }

        public virtual void drawLine(Line line, int color)
        {
        }

        public virtual void drawLines(Line[] lines, int color)
        {
        }

        public virtual void drawPolygon(Point[] points, int color)
        {
        }

        public virtual void drawMatrix(bool[][] matrix)
        {
        }

    }
}
