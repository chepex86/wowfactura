﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Line = PACFD.Rules.QRCodeLib.geom.Line;
using Point = PACFD.Rules.QRCodeLib.geom.Point;

namespace PACFD.Rules.QRCodeLib.util
{
    public class ConsoleCanvas : DebugCanvas
    {

        public void println(String str)
        {
            Console.WriteLine(str);
        }

        public void drawPoint(Point point, int color)
        {
        }

        public void drawCross(Point point, int color)
        {

        }

        public void drawPoints(Point[] points, int color)
        {
        }

        public void drawLine(Line line, int color)
        {
        }

        public void drawLines(Line[] lines, int color)
        {
        }

        public void drawPolygon(Point[] points, int color)
        {
        }

        public void drawMatrix(bool[][] matrix)
        {

        }

    }
}
