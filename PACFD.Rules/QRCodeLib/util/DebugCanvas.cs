﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Line = PACFD.Rules.QRCodeLib.geom.Line;
using Point = PACFD.Rules.QRCodeLib.geom.Point;

namespace PACFD.Rules.QRCodeLib.util
{
    public interface DebugCanvas
    {
        void println(String str);
        void drawPoint(Point point, int color);
        void drawCross(Point point, int color);
        void drawPoints(Point[] points, int color);
        void drawLine(Line line, int color);
        void drawLines(Line[] lines, int color);
        void drawPolygon(Point[] points, int color);
        void drawMatrix(bool[][] matrix);
    }
}
