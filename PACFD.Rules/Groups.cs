﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.GroupsDataSetTableAdapters;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class Groups
    {
        #region Update Groups
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool Update(GroupsDataSet.GroupsDataTable table)
        {
            GroupsTableAdapter taGroups;

            try
            {
                using (taGroups = new GroupsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taGroups);
                    taGroups.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                return false;
            }

            return true;
        }
        #endregion

        #region Select By ConceptsID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ConceptsDataSet.ConceptsDataTable SelectByID(int id)
        {
            ConceptsDataSet.ConceptsDataTable table;

            table = new ConceptsDataSet.ConceptsDataTable();
            //this.SelectByID(table, id);

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(GroupsDataSet.GroupsDataTable table, int id)
        {
            GroupsTableAdapter adapter;

            try
            {
                using (adapter = new GroupsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                return false;
            }

            return true;
        }
        #endregion

        #region Select Concepts By Searching


        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SelectBySearching(GroupsDataSet.GroupsDataTable table, string name, bool? active)
        {
            try
            {
                using (GroupsTableAdapter adapter = new GroupsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.FillBySearching(table, name, active);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public GroupsDataSet.GroupsDataTable SelectBySearching(string name, bool? active)
        {
            GroupsDataSet.GroupsDataTable ta;

            ta = new GroupsDataSet.GroupsDataTable();
            this.SelectBySearching(ta, name, active);

            return ta;
        }
        #endregion

        /// <summary>
        /// Select all groups from the data base.
        /// </summary>
        /// <returns></returns>
        public DataAccess.GroupsDataSet.GetAllDataTable SelectAll()
        {
            DataAccess.GroupsDataSet.GetAllDataTable table = new GroupsDataSet.GetAllDataTable();

            try
            {
                using (DataAccess.GroupsDataSetTableAdapters.GetAllTableAdapter adapter = new GetAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {

            }

            return table;
        }
    }
}
