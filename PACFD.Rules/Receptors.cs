﻿#region Usings
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using PACFD.DataAccess.ReceptorsDataSetTableAdapters;
using System.Data;
using PACFD.DataAccess;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    ///  
    /// </summary>
    /// <remarks>
    /// Store Procedures added:
    /// - spReceptors_GetByBillersID
    /// </remarks>        
    public class Receptors
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByBillersID(DataAccess.ReceptorsDataSet.ReceptorsGetByBillersIDDataTable table, int id)
        {
            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.ReceptorsGetByBillersIDTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.ReceptorsGetByBillersIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataAccess.ReceptorsDataSet.ReceptorsGetByBillersIDDataTable SelectByBillersID(int id)
        {
            DataAccess.ReceptorsDataSet.ReceptorsGetByBillersIDDataTable table;

            table = new PACFD.DataAccess.ReceptorsDataSet.ReceptorsGetByBillersIDDataTable();
            this.SelectByBillersID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByID(DataAccess.ReceptorsDataSet.ReceptorsDataTable table, int id)
        {
            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.ReceptorsTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.ReceptorsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataAccess.ReceptorsDataSet.ReceptorsDataTable SelectByID(int id)
        {
            DataAccess.ReceptorsDataSet.ReceptorsDataTable table;

            table = new PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable();
            this.SelectByID(table, id);

            return table;
        }
        /// <summary>
        /// Update a receptor(s).
        /// </summary>
        ///<param name="table">Table with the receptors to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(DataAccess.ReceptorsDataSet.ReceptorsDataTable table)
        {
            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.ReceptorsTableAdapter adapter = new PACFD.DataAccess.ReceptorsDataSetTableAdapters.ReceptorsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Get a DataAccess.ReceptorsDataSet.SearchByNameDataTable table filtered by name.
        /// </summary>
        /// <param name="billersid">Biller ID used to filter.</param>
        /// <param name="name">Name to look for.</param>
        /// <returns>DataAccess.ReceptorsDataSet.SearchByNameDataTable table.</returns>
        public DataAccess.ReceptorsDataSet.SearchByNameDataTable SelectByName(int billersid, string name)
        {
            DataAccess.ReceptorsDataSet.SearchByNameDataTable table = new PACFD.DataAccess.ReceptorsDataSet.SearchByNameDataTable();

            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.SearchByNameTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.SearchByNameTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billersid, name);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a receptor by the biller ID and receptor RFC.
        /// </summary>
        /// <param name="billersid">Biller ID.</param>
        /// <param name="rfc">RFC to llok for.</param>
        /// <returns>Return a table with the receptor founded.</returns>
        public DataAccess.ReceptorsDataSet.SearchByRFCDataTable SelectByRFC(int billersid, string rfc)
        {
            DataAccess.ReceptorsDataSet.SearchByRFCDataTable table =
                new PACFD.DataAccess.ReceptorsDataSet.SearchByRFCDataTable();

            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.SearchByRFCTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.SearchByRFCTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billersid, rfc);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get the receptors RFC from a biller ID.
        /// </summary>
        /// <param name="billersid">Biller ID used as filter.</param>
        /// <param name="rfc">RFC to look for.</param>
        /// <returns></returns>
        public DataAccess.ReceptorsDataSet.GetRFCsDataTable GetRFCs(int billersid, string rfc)
        {
            DataAccess.ReceptorsDataSet.GetRFCsDataTable table =
                new PACFD.DataAccess.ReceptorsDataSet.GetRFCsDataTable();

            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.GetRFCsTableAdapter adapter = new PACFD.DataAccess.ReceptorsDataSetTableAdapters.GetRFCsTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billersid, rfc);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billersid"></param>
        /// <param name="rfc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataAccess.ReceptorsDataSet.SearchByFilterDataTable SelectByRFCName(int billersid, string rfc, string name)
        {
            DataAccess.ReceptorsDataSet.SearchByFilterDataTable table =
                new PACFD.DataAccess.ReceptorsDataSet.SearchByFilterDataTable();

            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.SearchByFilterTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.SearchByFilterTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billersid, rfc, name);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billerid"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataAccess.ReceptorsDataSet.GetByNameAndBillersIDDataTable SelectByNameAndBillerID(int billerid, string name)
        {
            DataAccess.ReceptorsDataSet.GetByNameAndBillersIDDataTable table = new PACFD.DataAccess.ReceptorsDataSet.GetByNameAndBillersIDDataTable();

            try
            {
                using (DataAccess.ReceptorsDataSetTableAdapters.GetByNameAndBillersIDTableAdapter adapter =
                    new PACFD.DataAccess.ReceptorsDataSetTableAdapters.GetByNameAndBillersIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, name);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        #region Import Receptors

        public bool Update(ReceptorsDataSet.Receptors_ImportDataTable table)
        {
            try
            {
                using (Receptors_ImportTableAdapter adapter = new Receptors_ImportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool Import_InsertRow(int BillerID, DataRow row)
        {
            ReceptorsDataSet ds = new ReceptorsDataSet();
            ReceptorsDataSet.ReceptorsRow drReceptor;

            try
            {
                drReceptor = ds.Receptors.NewReceptorsRow();

                drReceptor.BillerID = BillerID;
                drReceptor.RFC = row["RFC"].ToString();
                drReceptor.Name = row["Name"].ToString();
                drReceptor.Address = row["Address"].ToString();
                drReceptor.Phone = row["Phone"].ToString();
                drReceptor.ExternalNumber = row["ExternalNumber"].ToString();
                drReceptor.InternalNumber = row["InternalNumber"].ToString();
                drReceptor.Colony = row["Colony"].ToString();
                drReceptor.Location = row["City"].ToString();
                drReceptor.Reference = row["Reference"].ToString();
                drReceptor.Municipality = row["Municipality"].ToString();
                drReceptor.State = row["State"].ToString();
                drReceptor.Country = row["Country"].ToString();
                drReceptor.Zipcode = row["ZipCode"].ToString();
                drReceptor.Email = row["Email"].ToString();

                ds.Receptors.AddReceptorsRow(drReceptor);
                this.Update(ds.Receptors);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool SelectByRFC(ReceptorsDataSet.Receptors_ImportDataTable table, int BillerID, String RFC)
        {
            try
            {
                using (Receptors_ImportTableAdapter adapter = new Receptors_ImportTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, RFC);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool Import_ModifyRow(int BillerID, DataRow row)
        {
            ReceptorsDataSet ds = new ReceptorsDataSet();
            ReceptorsDataSet.Receptors_ImportRow drImport;

            try
            {
                this.SelectByRFC(ds.Receptors_Import, BillerID, row["RFC"].ToString());
                drImport = ds.Receptors_Import[0];

                drImport.BillerID = BillerID;
                drImport.RFC = row["RFC"].ToString();
                drImport.Name = row["Name"].ToString();
                drImport.Address = row["Address"].ToString();
                drImport.Phone = row["Phone"].ToString();
                drImport.ExternalNumber = row["ExternalNumber"].ToString();
                drImport.InternalNumber = row["InternalNumber"].ToString();
                drImport.Colony = row["Colony"].ToString();
                drImport.Location = row["City"].ToString();
                drImport.Reference = row["Reference"].ToString();
                drImport.Municipality = row["Municipality"].ToString();
                drImport.State = row["State"].ToString();
                drImport.Country = row["Country"].ToString();
                drImport.Zipcode = row["ZipCode"].ToString();
                drImport.Email = row["Email"].ToString();

                this.Update(ds.Receptors_Import);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        public String ReadFile(ref DataTable table, int BillerID, String ImportFile)
        {
            ReceptorsDataSet.SearchByRFCDataTable dsReceptor;
            table = new DataTable();
            DataRow row;
            int cont = 0;
            String rfcAux = String.Empty;

            try
            {

                int Lines = ImportFile.Split(Environment.NewLine[1]).Length;

                String[] clients = new String[Lines];
                Array.Copy(ImportFile.Split(Environment.NewLine[1]), clients, Lines);

                for (int i = 0; i < clients.Length; i++)
                {
                    rfcAux = clients[i].Split('|')[1];

                    for (int j = clients.Length - 1; j > i; j -= 1)
                        if (rfcAux == clients[j].Split('|')[1])
                            return ((int)TypesErrorsImport.Twice).ToString() + "_" + (i + 1) + "_" + (j + 1)
                                + "_" + clients[i] + "_" + clients[j];
                }

                rfcAux = String.Empty;

                table.Columns.Add("RFC");
                table.Columns.Add("Name");
                table.Columns.Add("Address");
                table.Columns.Add("ExternalNumber");
                table.Columns.Add("InternalNumber");
                table.Columns.Add("Colony");
                table.Columns.Add("Phone");
                table.Columns.Add("Country");
                table.Columns.Add("State");
                table.Columns.Add("Municipality");
                table.Columns.Add("City");
                table.Columns.Add("Reference");
                table.Columns.Add("ZipCode");
                table.Columns.Add("Email");
                table.Columns.Add("RFCExist");
                table.Columns.Add("Status");
                table.Columns.Add("UsoCFDI");
                for (int i = 0; i < clients.Length; i += 1)
                {
                    row = table.NewRow();

                    if (clients[i].Split('|')[cont].ToUpper() != "C")
                        return ((int)TypesErrorsImport.MissingHeader).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i];

                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 13)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;

                    rfcAux = clients[i].Split('|')[cont];
                    dsReceptor = this.SelectByRFC(BillerID, rfcAux);

                    row["RFCExist"] = dsReceptor.Count < 1 ? false : true;
                    row["Status"] = 0;

                    if (rfcAux == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["RFC"] = rfcAux;
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Name"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Address"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 50)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["ExternalNumber"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 50)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["InternalNumber"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;

                    row["Colony"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 50)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Phone"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + cont;
                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont] == "Mexico" || clients[i].Split('|')[cont] == "México") { }
                    else return ((int)TypesErrorsImport.CountryNotFound).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i];
                    row["Country"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["State"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Municipality"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont] == String.Empty)
                        return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["City"] = clients[i].Split('|')[cont];
                    cont += 1;

                    if (clients[i].Split('|')[cont].Length > 255)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Reference"] = clients[i].Split('|')[cont];
                    cont += 1;

                    try
                    {
                        if (clients[i].Split('|')[cont] == String.Empty)
                            return ((int)TypesErrorsImport.StringEmpty).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                        row["ZipCode"] = Convert.ToInt32(clients[i].Split('|')[cont]);
                        cont += 1;
                    }
                    catch (Exception ex) { return ((int)TypesErrorsImport.ErrorConversion).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + cont; }

                    if (clients[i].Split('|')[cont].Length > 50)
                        return ((int)TypesErrorsImport.MaxLength).ToString() + "_" + (i + 1) + "_" + (cont + 1) + "_" + clients[i] + "_" + cont;
                    row["Email"] = clients[i].Split('|')[cont];
                    cont = 0;
                    row["UsoCFDI"] = " ";
                    table.Rows.Add(row);
                }

                table.AcceptChanges();
                return ((int)TypesErrorsImport.Success).ToString();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return ((int)TypesErrorsImport.FatalError).ToString();
            }
        }

        public DataTable Import(int BillerID, DataTable table)
        {
            if (table == null)
                return table;

            if (table.Rows.Count < 1)
                return null;

            DataRow row;
            ArrayList array = new ArrayList();

            try
            {
                for (int i = 0; i < table.Rows.Count; i += 1)
                {
                    row = table.Rows[i];

                    switch (Convert.ToInt32(row["Status"]))
                    {
                        case 0: //Insert
                            table.Rows[i]["RFCExist"] = this.Import_InsertRow(BillerID, row);
                            break;
                        case 1: //Ignorar
                            array.Add(table.Rows[i]);
                            break;
                        case 2: //Sobreescribir
                            table.Rows[i]["RFCExist"] = this.Import_ModifyRow(BillerID, row);
                            break;
                    }

                    table.AcceptChanges();
                }

                this.DeleteRowsInTable(ref table, array);

                return table;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return null;
            }
        }

        public void DeleteRowsInTable(ref DataTable dt, ArrayList array)
        {
            if (array == null || array.Count < 1)
                return;

            foreach (DataRow dr in array)
                dt.Rows.Remove(dr);
        }

        #endregion
    }
}