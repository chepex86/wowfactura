﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PACFD.Rules.mx.com.buzone.prepws {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="TimbradoCFDSoapBinding", Namespace="http://timbrado.buzone.com.mx/TimbradoCFD")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TimbradoError))]
    public partial class TimbradoCFDService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback timbradoCFDOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public TimbradoCFDService() {
            this.Url = global::PACFD.Rules.Properties.Settings.Default.PACFD_Rules_mx_com_buzone_prepws_TimbradoCFDService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event timbradoCFDCompletedEventHandler timbradoCFDCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("timbradoCFDReturn", Namespace="http://timbrado.buzone.com.mx/TimbradoCFD")]
        public TimbradoResponse timbradoCFD([System.Xml.Serialization.XmlElementAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD")] TimbradoRequest in0) {
            object[] results = this.Invoke("timbradoCFD", new object[] {
                        in0});
            return ((TimbradoResponse)(results[0]));
        }
        
        /// <remarks/>
        public void timbradoCFDAsync(TimbradoRequest in0) {
            this.timbradoCFDAsync(in0, null);
        }
        
        /// <remarks/>
        public void timbradoCFDAsync(TimbradoRequest in0, object userState) {
            if ((this.timbradoCFDOperationCompleted == null)) {
                this.timbradoCFDOperationCompleted = new System.Threading.SendOrPostCallback(this.OntimbradoCFDOperationCompleted);
            }
            this.InvokeAsync("timbradoCFD", new object[] {
                        in0}, this.timbradoCFDOperationCompleted, userState);
        }
        
        private void OntimbradoCFDOperationCompleted(object arg) {
            if ((this.timbradoCFDCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.timbradoCFDCompleted(this, new timbradoCFDCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2102.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD/Schema")]
    public partial class TimbradoRequest {
        
        private string usuarioField;
        
        private string passwordField;
        
        private object xmlCFDField;
        
        private string tituloField;
        
        private string conectorField;
        
        private string fileTypeField;
        
        private int returnCFDTimbradoField;
        
        private string comentarioField;
        
        /// <remarks/>
        public string usuario {
            get {
                return this.usuarioField;
            }
            set {
                this.usuarioField = value;
            }
        }
        
        /// <remarks/>
        public string password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
            }
        }
        
        /// <remarks/>
        public object xmlCFD {
            get {
                return this.xmlCFDField;
            }
            set {
                this.xmlCFDField = value;
            }
        }
        
        /// <remarks/>
        public string titulo {
            get {
                return this.tituloField;
            }
            set {
                this.tituloField = value;
            }
        }
        
        /// <remarks/>
        public string conector {
            get {
                return this.conectorField;
            }
            set {
                this.conectorField = value;
            }
        }
        
        /// <remarks/>
        public string fileType {
            get {
                return this.fileTypeField;
            }
            set {
                this.fileTypeField = value;
            }
        }
        
        /// <remarks/>
        public int returnCFDTimbrado {
            get {
                return this.returnCFDTimbradoField;
            }
            set {
                this.returnCFDTimbradoField = value;
            }
        }
        
        /// <remarks/>
        public string comentario {
            get {
                return this.comentarioField;
            }
            set {
                this.comentarioField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2102.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD/Schema")]
    public partial class TimbradoError {
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2102.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD/Schema")]
    public partial class TimbradoResponse {
        
        private object xmlCFDTimbradoField;
        
        private string uUIDField;
        
        private string selloSATField;
        
        private string versionField;
        
        private string noCertificadoSATField;
        
        private string selloCFDField;
        
        private System.DateTime fechaTimbradoField;
        
        private bool fechaTimbradoFieldSpecified;
        
        private string rfcProvCertifField;
        
        private string leyendaField;
        
        private TimbradoDatosAdicionales datosAdicionalesField;
        
        /// <remarks/>
        public object xmlCFDTimbrado {
            get {
                return this.xmlCFDTimbradoField;
            }
            set {
                this.xmlCFDTimbradoField = value;
            }
        }
        
        /// <remarks/>
        public string UUID {
            get {
                return this.uUIDField;
            }
            set {
                this.uUIDField = value;
            }
        }
        
        /// <remarks/>
        public string selloSAT {
            get {
                return this.selloSATField;
            }
            set {
                this.selloSATField = value;
            }
        }
        
        /// <remarks/>
        public string version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
        
        /// <remarks/>
        public string noCertificadoSAT {
            get {
                return this.noCertificadoSATField;
            }
            set {
                this.noCertificadoSATField = value;
            }
        }
        
        /// <remarks/>
        public string selloCFD {
            get {
                return this.selloCFDField;
            }
            set {
                this.selloCFDField = value;
            }
        }
        
        /// <remarks/>
        public System.DateTime fechaTimbrado {
            get {
                return this.fechaTimbradoField;
            }
            set {
                this.fechaTimbradoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool fechaTimbradoSpecified {
            get {
                return this.fechaTimbradoFieldSpecified;
            }
            set {
                this.fechaTimbradoFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string rfcProvCertif {
            get {
                return this.rfcProvCertifField;
            }
            set {
                this.rfcProvCertifField = value;
            }
        }
        
        /// <remarks/>
        public string leyenda {
            get {
                return this.leyendaField;
            }
            set {
                this.leyendaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public TimbradoDatosAdicionales datosAdicionales {
            get {
                return this.datosAdicionalesField;
            }
            set {
                this.datosAdicionalesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2102.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD/Schema")]
    public partial class TimbradoDatosAdicionales {
        
        private string batchIdField;
        
        private string statusField;
        
        private error[] errorListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string batchId {
            get {
                return this.batchIdField;
            }
            set {
                this.batchIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public error[] errorList {
            get {
                return this.errorListField;
            }
            set {
                this.errorListField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2102.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://timbrado.buzone.com.mx/TimbradoCFD/Schema")]
    public partial class error {
        
        private string codeField;
        
        private string messageField;
        
        private string detailField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string detail {
            get {
                return this.detailField;
            }
            set {
                this.detailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void timbradoCFDCompletedEventHandler(object sender, timbradoCFDCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class timbradoCFDCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal timbradoCFDCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public TimbradoResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((TimbradoResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591