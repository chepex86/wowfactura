﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.DataAccess;
using PACFD.DataAccess.DigitalCertificatesDataSetTableAdapters;
using PACFD.Common;
using System.IO;
using System.Configuration;
using System.Web;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// 
    /// </summary>
    public enum TypeFile
    {
        DigitalCertificate = 0,
        PrivateKey = 1
    }
    /// <summary>
    /// 
    /// </summary>
    public class DigitalCertificates
    {
        /// <summary>
        /// Updates the table DigitalCertificatesDataSet.DigitalCertificatesDataTable
        /// </summary>
        /// <param name="table">DigitalCertificatesDataSet.DigitalCertificatesDataTable table</param>
        /// <returns>Returns true if the process completes successfully, otherwise returns false.</returns>
        public bool Update(DigitalCertificatesDataSet.DigitalCertificatesDataTable table)
        {
            try
            {
                using (DigitalCertificatesTableAdapter taCertificate = new DigitalCertificatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(taCertificate);
                    taCertificate.Update(table);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        #region Select By DigitalCertificateID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DigitalCertificatesDataSet.DigitalCertificatesDataTable SelectByDigitalCertificateID(int id)
        {
            DigitalCertificatesDataSet.DigitalCertificatesDataTable table;

            table = new DigitalCertificatesDataSet.DigitalCertificatesDataTable();
            this.SelectByDigitalCertificateID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByDigitalCertificateID(DigitalCertificatesDataSet.DigitalCertificatesDataTable table, int id)
        {
            try
            {
                using (DigitalCertificatesTableAdapter adapter = new DigitalCertificatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion

        #region Select By BillerID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable SelectByBillerID(int id)
        {
            DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable table;

            table = new DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable();
            this.SelectByBillerID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool SelectByBillerID(DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable table, int id)
        {
            try
            {
                using (DigitalCertificates_GetByFkBillerIDTableAdapter adapter = new DigitalCertificates_GetByFkBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        #endregion

        #region Get Active By BillerID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable GetActiveByBillerID(int id)
        {
            DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table;

            table = new DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable();
            this.GetActiveByBillerID(table, id);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GetActiveByBillerID(DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table, int id)
        {
            try
            {
                using (DigitalCertificates_GetActiveTableAdapter adapter = new DigitalCertificates_GetActiveTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region Seek File By BillerID & DigitalCertificateID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <param name="DigCertID"></param>
        /// <returns></returns>
        public DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable SeekFile(int BillerID, int DigCertID)
        {
            DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable table;

            table = new DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable();
            this.SeekFile(table, BillerID, DigCertID);

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="BillerID"></param>
        /// <param name="DigCertID"></param>
        /// <returns></returns>
        public bool SeekFile(DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable table, int BillerID, int DigCertID)
        {
            try
            {
                using (DigitalCertificates_SeekFileTableAdapter adapter = new DigitalCertificates_SeekFileTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, BillerID, DigCertID);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        #endregion

        public byte[] GetPrivateKey(int BillerID, string CertificateNumber, ref string cpp)
        {
            byte[] arr = null;
            try
            {
                using (DigitalCertificatesDataSet.DigitalCertificates_ByCertificateNumberDataTable table = new DigitalCertificatesDataSet.DigitalCertificates_ByCertificateNumberDataTable())
                {
                    using (DigitalCertificates_ByCertificateNumberTableAdapter adapter = new DigitalCertificates_ByCertificateNumberTableAdapter())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Fill(table, BillerID, CertificateNumber);
                    }

                    if (table != null && table.Count > 0 && table[0].PrivtaeKey != string.Empty)
                    {
                        arr = Convert.FromBase64String(table[0].PrivtaeKey);
                        cpp = PACFD.Common.Cryptography.DecryptUnivisitString(table[0].Ccp);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return arr;
        }

        #region Get Path of the Digital Certificate and Private Key by BillerID - DigitalCertificateID or by separate
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BillerID"></param>
        /// <returns></returns>
        protected int GetCertificateActiveByBillerID(int BillerID)
        {
            DigitalCertificates certificate = new DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable ta;

            ta = certificate.GetActiveByBillerID(BillerID);

            return ta.Count < 1 ? -1 : ta[0].DigitalCertificateID;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billerID"></param>
        /// <returns></returns>
        public string GetPublickey(int billerID)
        {
            DigitalCertificates certificate = new DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable ta;

            ta = certificate.GetActiveByBillerID(billerID);

            return ta.Count < 1 ? String.Empty : ta[0].Ccp;
        }
        /// <summary>
        /// Generates a random number between 0 and 1000, taken as seed the milliseconds.  
        /// </summary>
        /// <returns>String number</returns>
        protected String RandomNumber()
        {
            Random r = new Random(DateTime.Now.Millisecond);
            return r.Next(0, 1000).ToString();
        }
        #endregion

        public static int ValidatePrivateKeyAndIsCouple(String c, String k, String PublicKey)
        {
            byte[] arrCer = Convert.FromBase64String(c);
            byte[] arrKey = Convert.FromBase64String(k);

            System.Security.Cryptography.X509Certificates.X509Certificate2 X509Cert;
            X509Cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(arrCer);

            if (X509Cert == null)
                return 1;

            
            System.Security.SecureString cpp = new System.Security.SecureString();

            for (int i = 0; i < PublicKey.Length; i++)
            {
                cpp.AppendChar(PublicKey.ToCharArray()[i]);
            }

            System.Security.Cryptography.RSACryptoServiceProvider key = OpenSSL.DecodeEncryptedPrivateKeyInfo(arrKey, cpp);

            if (key == null)
                return 99;

            if (!OpenSSL.CompareByteArrays(key.ExportParameters(false).Modulus, OpenSSL.CertToXMLKey(X509Cert).ExportParameters(false).Modulus))
                return 2;

            return 0;
        }

        public bool ValidateRFC(String c)
        {
            byte[] arrCert = Convert.FromBase64String(c);
            System.Security.Cryptography.X509Certificates.X509Certificate2 X509Cert;

            System.Text.RegularExpressions.Match rfc;
            String re = @"(?i)OID\.2\.5\.4\.45=(?<value>[^/,]+)";

            try
            {
                X509Cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(arrCert);

                if (X509Cert == null)
                    return false;

                rfc = System.Text.RegularExpressions.Regex.Match(X509Cert.Subject, re);

                return rfc.Success;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
    }
}