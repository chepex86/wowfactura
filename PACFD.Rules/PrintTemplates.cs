﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    public class PrintTemplates
    {
        /// <summary>
        /// Get the data base rows indexs definition.
        /// </summary>
        /// <returns></returns>
        public static System.Xml.XmlDocument GetDBDefinition()
        {
            System.Xml.XmlDocument doc = null;
            System.IO.MemoryStream stream;
            byte[] xmlarray;
            System.Text.UTF8Encoding encoding = null;

            using (stream = new System.IO.MemoryStream())
            {
                encoding = new System.Text.UTF8Encoding();
                xmlarray = encoding.GetBytes(Properties.Resources.DBDefinition);
                stream.Write(xmlarray, 0, xmlarray.Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                xmlarray = null;
                encoding = null;
                doc = new System.Xml.XmlDocument();

                try
                {
                    doc.Load(stream);
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    System.Diagnostics.Debug.WriteLine("[ERROR - Reading DBDefinition] " + ex.Message);
                    return null;
                }
            }

            return doc;
        }
        public static System.Xml.XmlDocument GetXmlDocumentDefaultCBB()
        {
            System.Xml.XmlDocument doc = null;
            System.IO.MemoryStream stream;
            byte[] xmlarray;
            System.Text.UTF8Encoding encoding = null;

            using (stream = new System.IO.MemoryStream())
            {
                encoding = new System.Text.UTF8Encoding();
                xmlarray = encoding.GetBytes(Properties.Resources.DefaultCBB);
                stream.Write(xmlarray, 0, xmlarray.Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                xmlarray = null;
                encoding = null;
                doc = new System.Xml.XmlDocument();

                try
                {
                    doc.Load(stream);
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    System.Diagnostics.Debug.WriteLine("[ERROR - Reading DefaultCBB] " + ex.Message);
                    return null;
                }
            }

            return doc;
        }
        public static System.Xml.XmlDocument GetXmlDocumentDefaultCFDI()
        {
            System.Xml.XmlDocument doc = null;
            System.IO.MemoryStream stream;
            byte[] xmlarray;
            System.Text.UTF8Encoding encoding = null;

            using (stream = new System.IO.MemoryStream())
            {
                encoding = new System.Text.UTF8Encoding();
                xmlarray = encoding.GetBytes(Properties.Resources.DefaultCFDI);
                stream.Write(xmlarray, 0, xmlarray.Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                xmlarray = null;
                encoding = null;
                doc = new System.Xml.XmlDocument();

                try
                {
                    doc.Load(stream);
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    System.Diagnostics.Debug.WriteLine("[ERROR - Reading DefaultCFDI] " + ex.Message);
                    return null;
                }
            }

            return doc;
        }
        public static System.Xml.XmlDocument GetXmlDocumentDefaultCFD()
        {
            System.Xml.XmlDocument doc = null;
            System.IO.MemoryStream stream;
            byte[] xmlarray;
            System.Text.UTF8Encoding encoding = null;

            using (stream = new System.IO.MemoryStream())
            {
                encoding = new System.Text.UTF8Encoding();
                xmlarray = encoding.GetBytes(Properties.Resources.DefaultCFD);
                stream.Write(xmlarray, 0, xmlarray.Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                xmlarray = null;
                encoding = null;
                doc = new System.Xml.XmlDocument();

                try
                {
                    doc.Load(stream);
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    System.Diagnostics.Debug.WriteLine("[ERROR - Reading DefaultCFD] " + ex.Message);
                    return null;
                }
            }

            return doc;
        }

        /// <summary>
        /// Get the default template of the CBB.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCBB()
        {
            return Properties.Resources.DefaultCBB;
        }
        /// <summary>
        /// Get the default template of the CFDI.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCFDI()
        {
            return Properties.Resources.DefaultCFDI;
        }
        /// <summary>
        /// Get the default template of the CFD.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCFD()
        {
            return Properties.Resources.DefaultCFD;
        }
        /// <summary>
        /// Get the default template of the CFDI 3.2.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCFDI3_2()
        {
            return Properties.Resources.DefaultCFDI3_2;
        }
        /// <summary>
        /// Get the default template of the CFDI 3.2.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCFDI3_3()
        {
            return Properties.Resources.DefaultCFDI3_3;
        }
        /// <summary>
        /// Get the default template of the CFDI 3.2.
        /// </summary>
        /// <returns>String.</returns>
        public static string GetStringDefaultCFDI4_0()
        {
            return Properties.Resources.DefaultCFDI4_0;
        }

        /// <summary>
        /// Get all the print templates rows.
        /// </summary>
        /// <returns></returns>
        public PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesAllDataTable SelectAll()
        {
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesAllDataTable table =
                new PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesAllDataTable();

            try
            {
                using (PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesAllTableAdapter adapter =
                    new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesAllTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }


            return table;
        }
        /// <summary>
        /// Update, delete and insert a print template table.
        /// </summary>
        /// <param name="table">Table with the rows to be update, deleted or insert.</param>
        /// <returns></returns>
        public bool Update(PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table)
        {
            PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Update(table);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Select a template by ID.
        /// </summary>
        /// <param name="id">Template ID.</param>
        /// <returns>PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable</returns>
        public PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable SelectByID(int id)
        {
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table =
                new PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable();
            PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter adapter;

            try
            {
                using (adapter = new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, id);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get the print template rows by biller ID.
        /// </summary>
        /// <param name="billerid">Biller ID used as filter.</param>
        /// <returns> PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable</returns>
        public PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable SelectByBillerID(int billerid)
        {
            PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable table =
               new PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable();

            try
            {
                using (PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter adapter =
                    new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, (int?)null);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Select all the print template by the electronic billing type and biller ID (optional). If the biller ID  or 
        /// electronic billing type is not send  in the search, the default print template is returned. 
        /// Use also the get SelectDefaultPrintTemplate method to get the default values.
        /// </summary>
        /// <param name="billerid">Optional biller ID.</param>
        /// <param name="electronictype">Optional electronic billing type.</param>
        /// <returns>PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable</returns>
        public PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable SelectByBillerIDAndElectronicBillingType(
            int? billerid, ElectronicBillingType? electronictype)
        {
            PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable table =
                new PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable();

            try
            {
                using (PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter adapter =
                    new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid, electronictype == null ? (int?)null : (int?)electronictype);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get the default values of the print template table.
        /// </summary>
        /// <returns> PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable</returns>
        public PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable SelectDefaultPrintTemplate()
        {
            PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable table =
               new PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable();

            try
            {
                using (PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter adapter =
                    new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.GetByBillerIDAndElectronicBillingTypeTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, (int?)null, (int?)null);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a string xml template used for print.
        /// </summary>
        /// <param name="printtemplateid">Print ID to look for.</param>
        /// <returns>If success return a string with the print template else empty.</returns>
        public string GetPrintTemplateTextByID(int printtemplateid)
        {
            try
            {
                using (PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.PrintTemplatesDataSetTableAdapters.PrintTemplatesTableAdapter())
                {
                    using (PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table =
                        new PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable())
                    {
                        Tool.RemoveOwnerSqlCommand(adapter);
                        adapter.Fill(table, printtemplateid);

                        if (table.Count > 0)
                            return table[0].Text;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            return string.Empty;
        }
    }
}
