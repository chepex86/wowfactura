﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PACFD.Common;
using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFD.Rules
{
    /// <summary>
    /// Class used as ruler layer of the TaxTemplate and TaxTemplateTaxType tables.
    /// </summary>
    public class TaxTemplate
    {
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable table, with the 
        /// template "Name", template "BillingType" and taxtype Name as "TaxName".
        /// </summary>
        /// <param name="taxtemplateid">Id of the template used to saerch.</param>
        /// <returns>PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable</returns>
        public PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable SelectForListByTaxTemplateID(int taxtemplateid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetNameTaxTypesByTemplateIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetNameTaxTypesByTemplateIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtemplateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable table, with the 
        /// template "Name", template "BillingType" and taxtype Name as "TaxName".
        /// </summary>
        /// <param name="taxtemplateid">Id of the template used to saerch.</param>
        /// <returns>PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByTemplateIDDataTable</returns>
        public PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByBillerIDDataTable SelectForListByBillerID(int billerid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByBillerIDDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByBillerIDDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetNameTaxTypesByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetNameTaxTypesByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table.
        /// </summary>
        /// <param name="billerid">Billir ID used to search.</param>
        /// <returns>PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable</returns>
        public PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable SelectByID(int taxtemplateid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtemplateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public PACFD.DataAccess.TaxTemplatesDataSet SelectByTaxTemplateID(int taxtemplateid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet table =
                new PACFD.DataAccess.TaxTemplatesDataSet();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    table = adapter.FillTaxTemplate_TaxTemplateID(taxtemplateid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable table with the 
        /// TaxTypes values.
        /// </summary>
        /// <param name="templateid">TaxTemplate ID to search.</param>
        /// <param name="isin">If true get taxtype selected else get taxtype not selected.</param>
        /// <returns>PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable</returns>
        public PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable SelectTaxTypeInOrOut(int templateid, bool isin)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetTaxTypeInAndOutTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.GetTaxTypeInAndOutTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, templateid, isin);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplateGetTaxTypeByBillerIDDataTable SelectTaxTypeByBilleID(int billerid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplateGetTaxTypeByBillerIDDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplateGetTaxTypeByBillerIDDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplateGetTaxTypeByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplateGetTaxTypeByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public bool Update(PACFD.DataAccess.TaxTemplatesDataSet dataset)
        {
            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.UpdateTaxTemplate_TaxTemplateTaxType(dataset);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        public DataAccess.TaxTemplatesDataSet.IsTaxTypeUsedDataTable SelectTaxTypesInUse(int taxtypeid)
        {
            DataAccess.TaxTemplatesDataSet.IsTaxTypeUsedDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.IsTaxTypeUsedDataTable();

            try
            {
                using (DataAccess.TaxTemplatesDataSetTableAdapters.IsTaxTypeUsedTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.IsTaxTypeUsedTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, taxtypeid);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            return table;
        }

        public PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable TaxTemplatesGetByBillerID(int BillerID)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable ta;

            ta = new PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable();
            this.TaxTemplatesGetByBillerID(ta, BillerID);

            return ta;
        }

        public bool TaxTemplatesGetByBillerID(PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable ta, int BillerID)
        {
            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplates_GetByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplates_GetByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(ta, BillerID);
                }

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }


        public PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesGetByBillerIDDataTable SelectByBillerID(int billerid)
        {
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesGetByBillerIDDataTable table =
                new PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesGetByBillerIDDataTable();

            try
            {
                using (PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesGetByBillerIDTableAdapter adapter =
                    new PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesGetByBillerIDTableAdapter())
                {
                    Tool.RemoveOwnerSqlCommand(adapter);
                    adapter.Fill(table, billerid);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return table;
        }
    }
}
