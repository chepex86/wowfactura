﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using PACFD.Common;
using System.Linq;

namespace PACFD.WindowServices
{
    partial class PACFDBillingService : ServiceBase
    {
        private Thread threadCfdiCancelled;
        private Thread threadCfdiToSeal;

        /// <summary>
        /// Sleep main loop. Default 1 minute. appSettings key = Pacfd-MainLoop-Sleep value = 1
        /// </summary>
        public int MainLoopSleep
        {
            get
            {
                int i = 1;
                string s = System.Configuration.ConfigurationManager.AppSettings["Pacfd-MainLoop-Sleep"];
                return string.IsNullOrEmpty(s) || !int.TryParse(s, out i) ? 60000 : i * 60000;
            }
        }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public PACFDBillingService() { InitializeComponent(); }
        /// <summary>
        /// Windows service main start method.
        /// </summary>
        protected override void OnStart(string[] args)
        {
            try
            {
                LogManager.WriteStackTrace(new Exception("Starting cancel thread.."));
                this.Start_CfdiCancelledThread();
                LogManager.WriteStackTrace(new Exception("Service cancel thread stop."));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }

            try
            {
                LogManager.WriteStackTrace(new Exception("Starting to seal thread.."));
                this.Start_CfdiToSealThread();
                LogManager.WriteStackTrace(new Exception("Service to seal thread stop."));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                LogManager.WriteError(ex);
            }
        }
        /// <summary>
        /// Windows service main stop method.
        /// </summary>
        protected override void OnStop()
        {
            LogManager.WriteStackTrace(new Exception("Stopping cancel thread.."));
            this.Stop_CfdiCancelledThread();
            LogManager.WriteStackTrace(new Exception("Service cancel thread stop."));

            LogManager.WriteStackTrace(new Exception("Stopping to seal thread.."));
            this.Stop_CfdiToSealThread();
            LogManager.WriteStackTrace(new Exception("Service to seal thread stop."));
        }
        
        #region Send to seal the billings that failed on the webservice and webpage application
        /// <summary>
        /// Stop the cfdi to seal thread.
        /// </summary>
        private void Stop_CfdiToSealThread()
        {
            try
            {
                if (this.threadCfdiToSeal != null)
                    this.threadCfdiToSeal.Abort();
            }
            catch (Exception ex)
            {
                LogManager.WriteStackTrace(new Exception("Stopping to seal thread failed.."));
                LogManager.WriteError(ex);
            }
            finally
            {
                this.threadCfdiToSeal = null;
            }
        }
        /// <summary>
        /// Start the to seal thread.
        /// </summary>
        private void Start_CfdiToSealThread()
        {
            LogManager.WriteStackTrace(new Exception("Starting to seal thread..."));

            if (this.threadCfdiToSeal == null)
                this.threadCfdiToSeal = new Thread(this.CfdiToSealThread_MainMethod);

            this.threadCfdiToSeal.Start();
            LogManager.WriteStackTrace(new Exception("Start to seal thread success."));
        }
        /// <summary>
        /// Main method of the to seal thread.
        /// </summary>
        private void CfdiToSealThread_MainMethod()
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();

            while (true)
            {
                System.Threading.Thread.Sleep(this.MainLoopSleep); //default sleep

                try
                {
                    using (DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable table = bill.SelectCfdiByErrorPAC(null, PacSealErrorTypes.SealError))
                    {
                        LogManager.WriteStackTrace(new Exception("Seal error request: " + table.Count.ToString()));

                        foreach (var row in table)
                        {
                            ErrorManager err = new ErrorManager();

                            if (!bill.SealCFD(row.BillingID, ref err))
                                bill.SetErrorPAC(row.BillingID, PacSealErrorTypes.SealError);
                            else
                                bill.SetErrorPAC(row.BillingID, PacSealErrorTypes.SealNotMailSend);

                            System.Threading.Thread.Sleep(5000); // sleep for 5 sec.
                        }
                    }
                }
                catch(Exception ex)
                {
                    LogManager.WriteError(ex);
                }
            }
        } 
        #endregion

        #region Set cancelled billings to deactive and send email notifications
        private void Stop_CfdiCancelledThread()
        {
            try
            {
                if (this.threadCfdiCancelled != null)
                    this.threadCfdiCancelled.Abort();
            }
            catch (Exception ex)
            {
                LogManager.WriteStackTrace(new Exception("Stopping cancel thread failed.."));
                LogManager.WriteError(ex);
            }
            finally
            {
                this.threadCfdiCancelled = null;
            }
        }
        /// <summary>
        /// Start the seal thread.
        /// </summary>
        private void Start_CfdiCancelledThread()
        {
            LogManager.WriteStackTrace(new Exception("Starting cancel thread..."));

            if (this.threadCfdiCancelled == null)
                this.threadCfdiCancelled = new Thread(this.CfdiCancelledThread_MainMethod);

            this.threadCfdiCancelled.Start();
            LogManager.WriteStackTrace(new Exception("Cancel thread start success."));
        }
        /// <summary>
        /// Method used to set active = 0 on cancelled billings and send the notification mail.
        /// </summary>
        private void CfdiCancelledThread_MainMethod()
        {
            Rules.Mail.MailSender mail = new PACFD.Rules.Mail.MailSender();
            PACFD.Rules.Mail.MailSendResult resultmail;

            while (true)
            {
                try
                {
                    System.Threading.Thread.Sleep(this.MainLoopSleep); //default sleep

                    using (DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable table = (new Rules.Billings()).SelectCfdiByErrorPAC(null, PacSealErrorTypes.CancelError))
                    {
                        LogManager.WriteStackTrace(new Exception("Cancelled request: " + table.Count.ToString()));

                        foreach (DataAccess.BillingsDataSet.GetCFDIByPACErrorRow row in table)
                            (new Rules.Billings()).SetActive(row.BillingID, false);
                    }

                    System.Threading.Thread.Sleep(5000); // sleep for 5 sec.

                    using (DataAccess.BillingsDataSet.GetCFDIByPACErrorDataTable table = (new Rules.Billings()).SelectCfdiByErrorPAC(null, PacSealErrorTypes.CancelNotMailSend))
                    {
                        LogManager.WriteStackTrace(new Exception("Cancelled mail request: " + table.Count.ToString()));

                        foreach (DataAccess.BillingsDataSet.GetCFDIByPACErrorRow row in table)
                        {
                            try
                            {
                                mail.Message = PACFD.Rules.Mail.MailSenderHelper.GetMessageFromXML(System.Windows.Forms.Application.StartupPath + @"\Includes\Mail\Mail.es.xml", "002");  //GetMessageFromBillerId(row.ReceptorID);
                            }
                            catch (Exception ex)
                            {
                                LogManager.WriteError(new Exception("Can't load email file " + System.Windows.Forms.Application.StartupPath + @"\Includes\Mail\Mail.es.xml. " + ex.Message + " " + ex.StackTrace));
                                continue;
                            }

                            this.SetMailParameters(mail, row);
                            mail.Message.Bcc.Add("francisco@univisit.com.mx");
                            resultmail = mail.Send();

                            if (!resultmail.IsSended)
                                LogManager.WriteError(new Exception("Can't send email. BillingID: " + row.BillingID.ToString() + ". Error: " + resultmail.Error));
                            else
                            {
                                (new Rules.Billings()).SetErrorPAC(row.BillingID, PacSealErrorTypes.CancelMailSend);
                                LogManager.WriteStackTrace(new Exception("Set pac error to PacSealErrorTypes.CancelMailSend. BillingID: " + row.BillingID.ToString()));
                                System.Threading.Thread.Sleep(5000); // sleep for 5 sec.
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
            }
        }
        /// <summary>
        /// Set the default mail configuration.
        /// </summary>
        /// <param name="mail">Mail to configure.</param>
        /// <param name="row">Data used to configure the mail.</param>
        private void SetMailParameters(Rules.Mail.MailSender mail, DataAccess.BillingsDataSet.GetCFDIByPACErrorRow row)
        {
            const string lblBillerName = "lblBillerName";
            const string lblDate = "lblDate";
            const string lblSerie = "lblSerie";
            const string lblFolio = "lblFolio";
            const string lblUUID = "lblUUID";
            const string lblReceptorName = "lblReceptorName";
            const string lblReceptorRFC = "lblReceptorRFC";
            const string lblBranch = "lblBranch";

            using (DataAccess.BillingsDataSet dataset = (new PACFD.Rules.Billings()).GetFullBilling(row.BillingID))
            {
                mail.Parameters[lblBillerName] = dataset.BillingsBillers[0].BillerName;
                mail.Parameters[lblDate] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                mail.Parameters[lblSerie] = dataset.Billings[0].Serial;
                mail.Parameters[lblFolio] = dataset.Billings[0].Folio;
                mail.Parameters[lblUUID] = dataset.Billings[0].IsUUIDNull() ? string.Empty : dataset.Billings[0].UUID;
                mail.Parameters[lblReceptorName] = dataset.BillingsReceptors[0].ReceptorName;
                mail.Parameters[lblReceptorRFC] = dataset.BillingsReceptors[0].ReceptorRFC;

                if (!dataset.Billings[0].IsBranchIDNull())
                    using (PACFD.DataAccess.BranchesDataSet.BranchDataTable btable = (new PACFD.Rules.Branches()).SelectByID(dataset.Billings[0].BranchID))
                        mail.Parameters[lblBranch] = string.Format("Sucursal {0},", btable[0].Name);
                else
                    mail.Parameters[lblBranch] = string.Empty;

                using (DataAccess.BillersDataSet.BillersDataTable table = (new PACFD.Rules.Billers()).SelectByID(row.BillerID))
                    mail.Message.To.Add(table[0].IsEmailNull() ? "francisco@univisit.com.mx" : table[0].Email);
            }
        }
        #endregion
    }
}
