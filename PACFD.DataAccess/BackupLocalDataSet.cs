﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PACFD.Common;
using System;

namespace PACFD.DataAccess
{


    public partial class BackupLocalDataSet
    {
    }



    public class BackupLocal_DA
    {
        private string conecctionStringData;

        public BackupLocal_DA()
        {

            if (ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"] == null)
            {

            }

            this.conecctionStringData = ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"].ConnectionString;

        }
        /// <summary>
        /// Update, delete or modify a billing.
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public string Backup(string Path)
        {
            SqlConnection conecction = new SqlConnection(this.conecctionStringData);
            SqlCommand command = null;

            String BDDName = "PACFD"; //Nombre de la base de datos

            String scriptBackup = "BACKUP DATABASE " + BDDName +
                                  " TO DISK = N'" + Path +
                                  "' WITH NOFORMAT, NOINIT, NAME =N'" + BDDName +
                                  "', SKIP, STATS = 10";

            try
            {
                command = new SqlCommand();
                command.CommandText = scriptBackup;
                command.Connection = conecction;

                conecction.Open();

                command.ExecuteNonQuery();
                return true.ToString() + "_" + String.Empty;
            }
            catch (System.Exception ex)
            {
                return false.ToString() + "_" + ex.ToString();
                //throw ex;
            }
            finally
            {
                if (conecction.State != ConnectionState.Closed)
                    conecction.Close();
            }
        }

        /*public BillingsDataSet GetFullBilling(int billingID)
        {


            BillingsDataSet ds = new BillingsDataSet();
            using (SqlDataAdapter sqlAdpater = new SqlDataAdapter("spBillingsAllCustomer_GetByID", conecctionStringData))
            {
                sqlAdpater.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@BillingID", billingID));

                sqlAdpater.TableMappings.Add("Table", ds.Billings.TableName);
                sqlAdpater.TableMappings.Add("Table1", ds.BillingsBillers.TableName);
                sqlAdpater.TableMappings.Add("Table2", ds.BillingsDetails.TableName);
                sqlAdpater.TableMappings.Add("Table3", ds.BillingsIssued.TableName);
                sqlAdpater.TableMappings.Add("Table4", ds.BillingsReceptors.TableName);
                sqlAdpater.TableMappings.Add("Table5", ds.Taxes.TableName);
                sqlAdpater.TableMappings.Add("Table6", ds.TransferTaxes.TableName);
                sqlAdpater.TableMappings.Add("Table7", ds.DetainedTaxes.TableName);

                sqlAdpater.Fill(ds);
            }
            return ds;

        }*/


    }

}

