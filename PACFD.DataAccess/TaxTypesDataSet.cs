﻿using System.Data.SqlClient;

namespace PACFD.DataAccess.TaxTypesDataSetTableAdapters
{
    partial class TaxTypesTableAdapter
    {
        public DataAccess.TaxTypesDataSet SelectTaxByID(DataAccess.TaxTypesDataSet dataset, int taxtypeid)
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter("spTaxTypesValues_GetByID", this.Connection))
            {
                adapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.Add(new SqlParameter("@TaxTypeID", taxtypeid));

                adapter.TableMappings.Add("Table", dataset.TaxTypes.TableName);
                adapter.TableMappings.Add("Table1", dataset.TaxValues.TableName);
                adapter.Fill(dataset);
            }

            return dataset;
        }

        /// <summary>
        /// Update or insert a Taxtype and Taxvalue data.
        /// </summary>
        /// <param name="dataset">DataSet with the taxtype and taxvalue to update or insert.</param>
        /// <returns>If success return true else false.</returns>
        public bool UpdateTax(DataAccess.TaxTypesDataSet dataset)
        {
            SqlTransaction transaction = null;
            DataAccess.TaxTypesDataSetTableAdapters.TaxTypesTableAdapter taxtypeadapter = new TaxTypesTableAdapter();
            DataAccess.TaxTypesDataSetTableAdapters.TaxValuesTableAdapter taxvalueadapter = new TaxValuesTableAdapter();

            try
            {
                taxtypeadapter.Connection =
                    taxvalueadapter.Connection = this.Connection;
                this.Connection.Open();

                transaction = this.Connection.BeginTransaction();

                taxvalueadapter.Transaction =
                taxtypeadapter.Transaction =
                     transaction;

                taxtypeadapter.Update(dataset.TaxTypes);
                taxvalueadapter.Update(dataset.TaxValues);

                transaction.Commit();
            }
            catch (System.Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                throw ex;
            }
            finally
            {
                if (this.Connection.State != System.Data.ConnectionState.Closed)
                    this.Connection.Close();
            }

            return true;
        }
    }
}

namespace PACFD.DataAccess
{ public partial class TaxTypesDataSet { } }
