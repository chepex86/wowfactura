﻿namespace PACFD.DataAccess
{
    public partial class SmtpConfigurationDataSet
    {

        public partial class SmtpConfigurationRow
        {
            /// <summary>
            /// Get the enabled state of the configuration.
            /// </summary>
            public bool IsSmtpEnabled
            {
                get
                {
                    if (string.IsNullOrEmpty(this.Host))
                        return false;

                    if (string.IsNullOrEmpty(this.Name))
                        return false;

                    if (string.IsNullOrEmpty(this.NameCredential))
                        return false;

                    if (string.IsNullOrEmpty(this.PasswordCredential))
                        return false;

                    return true;
                }
            }
        }
    }
}
