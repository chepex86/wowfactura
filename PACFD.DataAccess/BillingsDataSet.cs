﻿#region Usings
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PACFD.DataAccess.BillingsDataSetTableAdapters;
using PACFD.Common;
using System;
using Tool = PACFD.Common.Utilities;
using static PACFD.DataAccess.BillingsDataSet;
#endregion

namespace PACFD.DataAccess
{
    public partial class BillingsDataSet
    {
        partial class BillingCancelationDetailDataTable
        {
        }

        partial class BillingsDetailsGetByBillingIDDataTable
        {
        }

        partial class BillingsReceptorsDataTable
        {
        }

        partial class PaymentBillingsDataTable
        {
        }

        partial class BillersDataTable
        {
        }

        partial class ReceptorsDataTable
        {
        }

        partial class BillingsPayments_3DataTable
        {
        }

        partial class BillingsDetailNotesDataTable
        {
        }

        partial class BillingsDataTable
        {
        }

        private AddendumDataSet addendumDataSet = new AddendumDataSet();

        /// <summary>
        /// Addendum fake table. Delete if the real deal is need.
        /// </summary>        
        [global::System.ComponentModel.Browsable(false)]
        [global::System.ComponentModel.DesignerSerializationVisibility(global::System.ComponentModel.DesignerSerializationVisibility.Content)]
        public AddendumDataSet Addendum { get { return addendumDataSet; } }

        /// <summary>
        /// Billing example, not for real use. Used for print an example in the PDF report.
        /// </summary>
        /// <param name="countconcepts">Number of concepts to display.</param>
        /// <returns></returns>
        public static BillingsDataSet GetExampleDataSet(int countconcepts)
        {
            BillingsDataSet b = new BillingsDataSet();

            b.Billings.AddBillingsRow(0, 0, 0, 0, true, DateTime.Now, "v2.0", "1AB", "123",
                "SEAL TEST uyg|nuy|gukyb|gyg|fby|f|yf|fv|yt|d|vtrh|dvrt|vrt|crth|dh|crd|cthsd|hrd|hrt|r|sgr|csrg|cs|s|ers|e|cs||c|es|er|sr|cse|ljkb|jhb|jhb|jhb|jh|bjh|bjh|bjh|bj|hbj|h|jhb||jhb|jhb|jhb|b|b|hbj|b|jhb|jhbhj|bjh|hjbjh|bjh|bh|bjh|bh|bvhg|vg|gh|cgh|c|chg|chgc|hgc|hg|kjjhnhnh",
                9999999, DateTime.Now.Year, "Tarjeta Credito Ejemplo", "123", "123", "Terminos de pago", 0.00m, 10.00m, string.Empty, 0.00m, "Pagado en una sola exhibición", "Prueba", "MXN",
                "CADENA ORIGINAL ekfrcnwc_ d||csd||cwe||rfvwe|ign|jhg|uyng|uyg|uavsxyv satvxtdre1ssr1vzvx1erxv1 rex1re xre|xer|xvre|xve|rxv|erx|vryex|erx|re|xvyre|x|ryex|yrex|ryex|vre|xrv|xvre|x|reyn|uyn|g|uyn guy|u|g|u|u|gu|gnu|ku|gnu|g|y|gu|gu|g|iug|uyg|uyg|uyg|uy|uy gu|nuy|gu|gnu|gn|uygn|uyg|yfiyt|trsr|ese|sygb|ijoñ|jmyf|td|jh|jng|jg|yfb|d|rex|e|esx|es cet|s|e|ct|cw|c|cw|||e|wzewr|zcwr|zc|zce|cze|cze|czr|zwe|zc||ew|z|ewzc|ecz|wez|ez|e|zew|ras|era|rs|rtfc|ft|dcr|cet|axer|axe|ae|sr fc||sdc||skld||cksd||isd||fklvc||sdifjh vijs dfh||vfhdvij||jhjhghb|hg kfbh|gfb|hfb|hgbf|f|hf|f|bf||f|k||u|ygu|yuk|yfgsdf",
                0, "ingreso", false, "prueba", false, 1, false, "0001", "text de observación de ejemplo", null, 0, 1, "123456789a123456789b123456789c123456", DateTime.Now,
                "SEAL_SAT|bi|uyb|b|uy|b|yub|uyb|hsd|hrd|hrt|r|sgr|csrg|cs|s|ers|e|cs||c|es|er|sr|cse|ljkb|jhb|jhb|jhb|jh hsd|hrd|hrt|r|sgr|csrg|cs|s|ers|e|cs||c|es|er|sr|cse|ljkb|jhb|jhb|jhb|jhyi|b|", "12345678901234567890", "SAT_ORIGINALSTRING|khb|khb|h|bh|b|hjb|hb|hb|hjyg|fby|f|yf|fv|yt|d|vtrh|dvrt|vrt|crth|dh|crd|cthsd|hrd|hrt|r|sgr|csrg|cs|s|ers|e|cs||c|es|er|sr|cse|ljkb|jhb|jhb|jhb|jh|bjh|bjh|bjh|bj|hbj|h|jhb||jhb|jhb|jhb|b|b|hbj|b|jhb|jhbhj|bjh|hjb jh|bjh|bh|bjh|bh|bv hg|vg|gh|cgh|c|chg|chgc|hgc|hg|kjj hn hnh|bjh|b|hjb|jh|b|"
                , string.Empty, string.Empty, string.Empty, 1, 1, 0, 0, "Lugar de expedición de ejemplo.", false, DateTime.Now, "4488", "SFE0807172W7", "kqFJelLV1B99l8DunR98pn5k8uCwi94k+PhZJ2crw5Et2+qSkoROBlOWsoXrv93j5UUPBkxTmlStwTGBHSHWiQr1ewA4/fzseNIaWokVbIr8iFwrZETgZWp6Q+dErqLntDPISWYHmXfnvXi/Om7hCtklZC/msv3ZmmojcZEkjJb0Rk+sVDh+qm3sRbx40k1xKX1xtgLnX4/P/DwZlv+mj31YE4MEpy48xMjBA7dPNE4dF9UL3mTAQhwMV40MCrLpjTn95ov8mnL0ftaxuqzGXhqqNcDk1YF5OtBXuGFKwWAfY53bNxz1GeVY1+/8xZsmUmuukwA5uvIa7ghGt/4twA==", null, 0, 0, 0, 0, "01");

            b.BillingsBillers.AddBillingsBillersRow(b.Billings[0], "ACBD123456ABC", "My prueba dirección", "Josue Test Ramirez Probador", "123",
                "123", "Guerrero", "La Paz", string.Empty, "La Paz", "Baja California Norte del Oeste", "México", false, "123", "correo@ejemplo.mx", "Persona fisica");

            for (int i = 1; i < (countconcepts < 1 ? 15 : countconcepts); ++i)
            {
                b.BillingsDetails.AddBillingsDetailsRow(b.Billings[0], i * -1, i * 105020.35m, "Prueba de concepto adscasd casdcas dcasdcasdc dc xxx xx xxx xxxx xxx xxxx",
                    101073.50m, string.Format("Prueba {0} concepto xxx ", i), "123", i, true, 150030.00m, false, i.ToString("0.####"), "02", "1", ".16", 0, "601", "Kilo", 1, 1, "0.4", "", "", "", "", "02");
                b.Billings[0].Total += b.BillingsDetails[i - 1].Amount;
                b.Billings[0].SubTotal += b.BillingsDetails[i - 1].Amount;
            }

            b.BillingsIssued.AddBillingsIssuedRow(b.Billings[0], "Prueba issued", "123", "123", "Guerrero", "La Paz", string.Empty,
                "La Paz", "Baja California Sur en el Este", "Mexico", "123");
            b.BillingsReceptors.AddBillingsReceptorsRow(b.Billings[0], "Cliente dirección", "EFGA123456ABC", "Berto Test Antonio H.",
                "123", "123", "Guerrero", "La Paz", string.Empty, "La Paz", "Baja California Norte del Oeste", "México", "123", "A", "G03");
            b.Taxes.AddTaxesRow(b.Billings[0], 1000.00m, 500.00m);
            b.DetainedTaxes.AddDetainedTaxesRow(10.00m, "IPTEST", b.Taxes[0], 1000.00m, true, "tasa", 1);
            b.TransferTaxes.AddTransferTaxesRow(10.00m, "IVATEST", b.Taxes[0], 4100.00m, true, "cuota", 1);
            b.BillingFiscalRegime.AddBillingFiscalRegimeRow("Régimen fiscal de ejemplo.", b.Billings[0]);
            b.Billings[0].QrImage = GetFakeQr();

            return b;
        }

        public static BillingsDetailNotesDataTable GetBillingsDetailNotesDataTable(BillingsDataSet dataset, int count)
        {
            BillingsDetailNotesDataTable table = new BillingsDetailNotesDataTable();

            for (int i = 0; i < count; i++)
                table.AddBillingsDetailNotesRow(dataset.Billings[0], i.ToString() + count.ToString()
                    , "Ejemplo de concepto no calculable", (decimal)(i * 0.5), (decimal)(count * i * 0.5), i);

            dataset.BillingsDetailNotes.Merge(table, true);
            return table;
        }
        /// <summary>
        /// Get a byte array of a Qr fake image.
        /// </summary>
        /// <returns>byte[] array</returns>
        public static byte[] GetFakeQr()
        {
            byte[] b;

            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                Properties.Resources.QrFake.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                stream.Seek(0, System.IO.SeekOrigin.Begin);
                b = new byte[stream.Length];
                stream.Read(b, 0, b.Length);
            }

            return b;
        }
        /// <summary>
        /// Load the Addendum from a string xml.
        /// </summary>
        /// <param name="s"></param>
        public void LoadAddendum(string s)
        {
            if (this.addendumDataSet != null)
                this.addendumDataSet.Dispose();

            this.addendumDataSet = new AddendumDataSet();

            try
            {
                this.addendumDataSet.ReadXml(new System.Xml.XmlTextReader(new System.IO.StringReader(s)));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Class used as fake data table from the data base, reprecent the addendum.
        /// </summary>
        public sealed class AddendumDataSet : System.Data.DataSet
        {
        }
    }

    public class Billings_DA
    {
        private string conecctionStringData;

        public Billings_DA()
        {
            //if (ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"] == null)
            //{
            //}
            this.conecctionStringData = ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"].ConnectionString;
        }
        /// <summary>
        /// Update, delete or modify a billing.
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>

        public bool Update(BillingsDataSet dataset)
        {
            SqlConnection conecction = new SqlConnection(this.conecctionStringData);
            SqlTransaction trans = null;

            try
            {
                BillingsDetailsTableAdapter detailsadapter = new BillingsDetailsTableAdapter();
                TaxesTableAdapter taxeadapter = new TaxesTableAdapter();
                BillingsReceptorsTableAdapter receptoradapter = new BillingsReceptorsTableAdapter();
                BillingsBillersTableAdapter billersadapter = new BillingsBillersTableAdapter();
                BillingsTableAdapter billinsadapter = new BillingsTableAdapter();
                BillingsIssuedTableAdapter billinissue = new BillingsIssuedTableAdapter();
                QueriesTableAdapter query = new QueriesTableAdapter();
                DetainedTaxesTableAdapter detailtaxesadapter = new DetainedTaxesTableAdapter();
                TransferTaxesTableAdapter transfertaxeadapter = new TransferTaxesTableAdapter();
                BillingFiscalRegimeTableAdapter fiscalregimeadapter = new BillingFiscalRegimeTableAdapter();
                BillingsDetailNotesTableAdapter detailnoteadapter = new BillingsDetailNotesTableAdapter();
                BillingFieldsTableAdapter billingfieldta = new BillingFieldsTableAdapter();
                BillingGeneralPeriodTableAdapter billingGeneral = new BillingGeneralPeriodTableAdapter();
                BillingCancelationDetailTableAdapter billingCancelation = new BillingCancelationDetailTableAdapter();


                detailsadapter.Connection = conecction;
                taxeadapter.Connection = conecction;
                receptoradapter.Connection = conecction;
                billinsadapter.Connection = conecction;
                billersadapter.Connection = conecction;
                billinissue.Connection = conecction;
                query.Connection = conecction;
                detailtaxesadapter.Connection = conecction;
                transfertaxeadapter.Connection = conecction;
                detailnoteadapter.Connection = conecction;
                fiscalregimeadapter.Connection = conecction;
                billingfieldta.Connection = conecction;
                billingGeneral.Connection = conecction;
                billingCancelation.Connection = conecction;

                conecction.Open();
                trans = conecction.BeginTransaction();

                detailsadapter.Transaction =
                    taxeadapter.Transaction =
                    receptoradapter.Transaction =
                    billinsadapter.Transaction =
                    billersadapter.Transaction =
                    billinissue.Transaction =
                    query.Transaction =
                    detailtaxesadapter.Transaction =
                    transfertaxeadapter.Transaction =
                    fiscalregimeadapter.Transaction =
                    detailnoteadapter.Transaction =
                    billingfieldta.Transaction =
                    billingGeneral.Transaction =
                    billingCancelation.Transaction =
                    trans;

                Tool.RemoveOwnerSqlCommand(billinsadapter);
                Tool.RemoveOwnerSqlCommand(detailsadapter);
                Tool.RemoveOwnerSqlCommand(taxeadapter);
                Tool.RemoveOwnerSqlCommand(receptoradapter);
                Tool.RemoveOwnerSqlCommand(billersadapter);
                Tool.RemoveOwnerSqlCommand(billinissue);
                Tool.RemoveOwnerSqlCommand(detailtaxesadapter);
                Tool.RemoveOwnerSqlCommand(transfertaxeadapter);
                Tool.RemoveOwnerSqlCommand(fiscalregimeadapter);
                Tool.RemoveOwnerSqlCommand(detailnoteadapter);
                Tool.RemoveOwnerSqlCommand(billingfieldta);
                Tool.RemoveOwnerSqlCommand(billingGeneral);
                Tool.RemoveOwnerSqlCommand(billingCancelation);
                Tool.RemoveOwnerSqlCommand(query);



                billinsadapter.Update(dataset.Billings);
                // se agrego para los nuevos datos para desplegar en la factur
                billingfieldta.Update(dataset.BillingFields);
                detailsadapter.Update(dataset.BillingsDetails);
                taxeadapter.Update(dataset.Taxes);

                // quien sabe por que se cambia el dataset en la tabla de receptores. 
                receptoradapter.Update(dataset.BillingsReceptors);

                billersadapter.Update(dataset.BillingsBillers);
                billinissue.Update(dataset.BillingsIssued);
                detailtaxesadapter.Update(dataset.DetainedTaxes);
                transfertaxeadapter.Update(dataset.TransferTaxes);
                fiscalregimeadapter.Update(dataset.BillingFiscalRegime);
                detailnoteadapter.Update(dataset.BillingsDetailNotes);
                billingGeneral.Update(dataset.BillingGeneralPeriod);
                billingCancelation.Update(dataset.BillingCancelationDetail);
                //query.spSerial_SetActive(dataset.Billings[0].FolioID, false);

                if (!dataset.Billings[0].IsFolioIDNull())
                    query.spFolio_SetActive(dataset.Billings[0].FolioID, true, DateTime.Now);

                trans.Commit();
            }
            catch (System.Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                if (conecction.State != ConnectionState.Closed)
                    conecction.Close();
            }

            return true;
        }

        public BillingsDataSet GetFullBilling(int billingID)
        {
            BillingsDataSet ds = new BillingsDataSet();
            using (SqlDataAdapter sqlAdapter = new SqlDataAdapter("spBillingsAllCustomer_GetByID", conecctionStringData))
            {
                sqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlAdapter.SelectCommand.Parameters.Add(new SqlParameter("@BillingID", billingID));

                sqlAdapter.TableMappings.Add("Table", ds.Billings.TableName);
                sqlAdapter.TableMappings.Add("Table1", ds.BillingsBillers.TableName);
                sqlAdapter.TableMappings.Add("Table2", ds.BillingsDetails.TableName);
                sqlAdapter.TableMappings.Add("Table3", ds.BillingsIssued.TableName);
                sqlAdapter.TableMappings.Add("Table4", ds.BillingsReceptors.TableName);
                sqlAdapter.TableMappings.Add("Table5", ds.Taxes.TableName);
                sqlAdapter.TableMappings.Add("Table6", ds.TransferTaxes.TableName);
                sqlAdapter.TableMappings.Add("Table7", ds.DetainedTaxes.TableName);
                sqlAdapter.TableMappings.Add("Table8", ds.BillingFiscalRegime.TableName);
                sqlAdapter.TableMappings.Add("Table9", ds.BillingsDetailNotes.TableName);

                // para los campos personalizados de los datos exptras u observaciones
                sqlAdapter.TableMappings.Add("Table10", ds.BillingFields.TableName);
                sqlAdapter.TableMappings.Add("Table11", ds.BillingGeneralPeriod.TableName);
                sqlAdapter.TableMappings.Add("Table12", ds.BillingCancelationDetail.TableName);



                Tool.RemoveOwnerSqlCommand(sqlAdapter);

                sqlAdapter.Fill(ds);

                if (ds != null && ds.BillingsDetails.Count > 0)
                {
                    foreach (var dr in ds.BillingsDetails)
                        dr.CountString = dr.Count.ToString("0.####");

                    ds.AcceptChanges();
                }
            }
            return ds;
        }
    }

    public class BillingsPayments_DA
    {
        private string conecctionStringData;

        public BillingsPayments_DA()
        {
            //if (ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"] == null)
            //{
            //}
            this.conecctionStringData = ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"].ConnectionString;
        }
        /// <summary>
        /// Update, delete or modify a billing.
        /// </summary>
        /// <param name="dataset">Billing to be updated.</param>
        /// <returns>If success return true else false.</returns>
        public bool Update(BillingsDataSet dataset)
        {
            SqlConnection conecction = new SqlConnection(this.conecctionStringData);
            SqlTransaction trans = null;

            try
            {
                BillingsPayments_3TableAdapter billingspaymentsadapter = new BillingsPayments_3TableAdapter();
                PaymentBillingsTableAdapter paymentbillings = new PaymentBillingsTableAdapter();
                QueriesTableAdapter query = new QueriesTableAdapter();



                billingspaymentsadapter.Connection =
                    query.Connection =
                    paymentbillings.Connection =
                    conecction;

                conecction.Open();
                trans = conecction.BeginTransaction();

                paymentbillings.Transaction =
                    billingspaymentsadapter.Transaction =
                    query.Transaction =
                    trans;

                Tool.RemoveOwnerSqlCommand(billingspaymentsadapter);
                Tool.RemoveOwnerSqlCommand(paymentbillings);
                Tool.RemoveOwnerSqlCommand(query);




                billingspaymentsadapter.Update(dataset.BillingsPayments_3);

                if (!dataset.BillingsPayments_3[0].IsFolioIDNull())
                    query.spFolio_SetActive(dataset.BillingsPayments_3[0].FolioID, true, DateTime.Now);

                foreach (var b in dataset.PaymentBillings)
                {
                    b.BillingPaymentID = dataset.BillingsPayments_3[0].BillingPaymentId;
                    paymentbillings.Update(b);
                }
                //// se agrego para los nuevos datos para desplegar en la factur



                ////query.spSerial_SetActive(dataset.Billings[0].FolioID, false);

                //if (!dataset.BillingsPayments_3[0].IsFolioIDNull())
                //    query.spFolio_SetActive(dataset.BillingsPayments_3[0].FolioID, true, DateTime.Now);

                trans.Commit();
            }
            catch (System.Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                if (conecction.State != ConnectionState.Closed)
                    conecction.Close();
            }

            return true;
        }

        public bool Delete(BillingsDataSet dataset)
        {
            SqlConnection conecction = new SqlConnection(this.conecctionStringData);
            SqlTransaction trans = null;

            try
            {
                BillingsPayments_3TableAdapter billingspaymentsadapter = new BillingsPayments_3TableAdapter();
                PaymentBillingsTableAdapter paymentbillings = new PaymentBillingsTableAdapter();

                billingspaymentsadapter.Connection = conecction;
                paymentbillings.Connection = conecction;

                conecction.Open();
                trans = conecction.BeginTransaction();

                paymentbillings.Transaction =
                    billingspaymentsadapter.Transaction =
                    trans;

                Tool.RemoveOwnerSqlCommand(billingspaymentsadapter);
                Tool.RemoveOwnerSqlCommand(paymentbillings);

                billingspaymentsadapter.Delete(dataset.BillingsPayments_3[0].BillingPaymentId);

                foreach (var b in dataset.Billings)
                {
                    paymentbillings.Delete(dataset.BillingsPayments_3[0].BillingPaymentId, b.BillingID);
                }
                //// se agrego para los nuevos datos para desplegar en la factur



                ////query.spSerial_SetActive(dataset.Billings[0].FolioID, false);

                //if (!dataset.BillingsPayments_3[0].IsFolioIDNull())
                //    query.spFolio_SetActive(dataset.BillingsPayments_3[0].FolioID, true, DateTime.Now);

                trans.Commit();
            }
            catch (System.Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
            finally
            {
                if (conecction.State != ConnectionState.Closed)
                    conecction.Close();
            }

            return true;
        }

        public BillingsDataSet GetFullBilling(int billingPaymentId)
        {
            BillingsDataSet ds = new BillingsDataSet();
            using (SqlDataAdapter sqlAdapter = new SqlDataAdapter("spBillingsPaymentAllCustomer_GetByID", conecctionStringData))
            {
                sqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlAdapter.SelectCommand.Parameters.Add(new SqlParameter("@BillingPaymentId", billingPaymentId));


                sqlAdapter.TableMappings.Add("Table", ds.Billings.TableName);
                sqlAdapter.TableMappings.Add("Table1", ds.BillingsBillers.TableName);
                sqlAdapter.TableMappings.Add("Table2", ds.BillingsDetails.TableName);
                sqlAdapter.TableMappings.Add("Table3", ds.BillingsIssued.TableName);
                sqlAdapter.TableMappings.Add("Table4", ds.BillingsReceptors.TableName);
                sqlAdapter.TableMappings.Add("Table5", ds.Taxes.TableName);
                sqlAdapter.TableMappings.Add("Table6", ds.TransferTaxes.TableName);
                sqlAdapter.TableMappings.Add("Table7", ds.DetainedTaxes.TableName);
                sqlAdapter.TableMappings.Add("Table8", ds.BillingFiscalRegime.TableName);
                sqlAdapter.TableMappings.Add("Table9", ds.BillingsDetailNotes.TableName);
                sqlAdapter.TableMappings.Add("Table10", ds.BillingsPayments_3.TableName);


                Tool.RemoveOwnerSqlCommand(sqlAdapter);

                sqlAdapter.Fill(ds);

                if (ds != null && ds.BillingsDetails.Count > 0)
                {
                    foreach (var dr in ds.BillingsDetails)
                        dr.CountString = dr.Count.ToString("0.####");

                    ds.AcceptChanges();
                }
            }
            return ds;
        }
    }
}

namespace PACFD.DataAccess.BillingsDataSetTableAdapters
{
    partial class BillingsSearchTableAdapter
    {
    }

    partial class BillingsPayment_3SearchTableAdapter
    {
    }

    public partial class QueriesTableAdapter
    {
        public SqlConnection Connection
        {
            set
            {
                this.InitCommandCollection();

                if (this._commandCollection == null)
                    throw new Exception("Billings.Query->commandcollection is null.");

                for (int i = 0; i < this._commandCollection.Length; ++i)
                {
                    if (_commandCollection[i] != null)
                        this._commandCollection[i].Connection = value;
                }
            }
        }

        public SqlTransaction Transaction
        {
            set
            {
                for (int i = 0; i < this._commandCollection.Length; ++i)
                    this._commandCollection[i].Transaction = value;
            }
        }
    }
}