﻿namespace PACFD.DataAccess
{


    public partial class BillerSmtpConfigurationDataSet
    {
        public partial class BillerSmtpConfigurationRow
        {
            /// <summary>
            /// Get the enabled state of the configuration.
            /// </summary>
            public bool IsSmtpEnabled
            {
                get
                {
                    if (this.UseBase)
                        return true;

                    if (string.IsNullOrEmpty(this.Host))
                        return false;

                    if (string.IsNullOrEmpty(this.Name))
                        return false;

                    if (string.IsNullOrEmpty(this.NameCredential))
                        return false;

                    if (string.IsNullOrEmpty(this.PasswordCredential))
                        return false;

                    return true;
                }
            }
        }
    }
}
