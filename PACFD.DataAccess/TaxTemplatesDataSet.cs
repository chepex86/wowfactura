﻿using System.Data.SqlClient;

namespace PACFD.DataAccess
{ public partial class TaxTemplatesDataSet { partial class IsTaxTypeUsedDataTable { } } }

namespace PACFD.DataAccess.TaxTemplatesDataSetTableAdapters
{
    partial class TaxTemplatesTableAdapter
    {
        public bool UpdateTaxTemplate_TaxTemplateTaxType(PACFD.DataAccess.TaxTemplatesDataSet dataset)
        {
            SqlTransaction transaction = null;
            PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplatesTableAdapter templateadapter = new TaxTemplatesTableAdapter();
            PACFD.DataAccess.TaxTemplatesDataSetTableAdapters.TaxTemplateTaxTypeTableAdapter taxtypeadapter = new TaxTemplateTaxTypeTableAdapter();

            try
            {
                templateadapter.Connection =
                    taxtypeadapter.Connection =
                    this.Connection;
                this.Connection.Open();

                transaction = this.Connection.BeginTransaction();

                templateadapter.Transaction =
                    taxtypeadapter.Transaction = transaction;

                templateadapter.Update(dataset.TaxTemplates);
                taxtypeadapter.Update(dataset.TaxTemplateTaxType);

                transaction.Commit();
            }
            catch (System.Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                throw ex;
            }
            finally
            {
                if (this.Connection.State != System.Data.ConnectionState.Closed)
                    this.Connection.Close();
            }

            return true;
        }

        public DataAccess.TaxTemplatesDataSet FillTaxTemplate_TaxTemplateID(int taxtemplateid)
        {
            DataAccess.TaxTemplatesDataSet dataset = new TaxTemplatesDataSet();

            using (SqlDataAdapter sqlAdpater = new SqlDataAdapter("spTaxTemplate_GetTaxTemplate_TaxTemplateTaxType_ByTaxTemplateID",
                this.Connection))
            {
                sqlAdpater.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@TaxTemplateID", taxtemplateid));

                sqlAdpater.TableMappings.Add("Table", dataset.TaxTemplates.TableName);
                sqlAdpater.TableMappings.Add("Table1", dataset.TaxTemplateTaxType.TableName);

                sqlAdpater.Fill(dataset);
            }

            return dataset;
        }
    }
}