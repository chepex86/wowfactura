﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PACFD.Common;
using System;

namespace PACFD.DataAccess
{
    public partial class BillersDataSet
    {
        partial class SearchByFilterDataTable
        {
        }
    }
}

namespace PACFD.DataAccess.BillersDataSetTableAdapters
{
    partial class BillersTableAdapter
    {
        /// <summary>
        /// Update a biller and a issued dataset.
        /// </summary>
        /// <param name="dataset">DataSet with the biller and issued data to update.</param>
        /// <returns>If success return true.</returns>
        /// <exception cref="System.Exception">Return a exception generic class.</exception>
        public bool Update_Biller_Issued_BillerTaxes(PACFD.DataAccess.BillersDataSet dataset)
        {
            SqlTransaction transaction = null;
            DataAccess.BillersDataSetTableAdapters.BillersTableAdapter billeradapter = new BillersTableAdapter();
            DataAccess.BillersDataSetTableAdapters.IssuedTableAdapter issuedadapter = new IssuedTableAdapter();
            DataAccess.BillersDataSetTableAdapters.FiscalRegimeTableAdapter fiscaladapter = new FiscalRegimeTableAdapter();
            DataAccess.BillersDataSetTableAdapters.PlaceDispatchTableAdapter placeadapter = new PlaceDispatchTableAdapter();

            try
            {
                issuedadapter.Connection =
                    billeradapter.Connection =
                    fiscaladapter.Connection =
                    placeadapter.Connection =
                    this.Connection;
                this.Connection.Open();

                transaction = this.Connection.BeginTransaction();

                issuedadapter.Transaction =
                    billeradapter.Transaction =
                    fiscaladapter.Transaction =
                    placeadapter.Transaction =
                    transaction;

                ///the dataset update order must be:
                ///---------------------------------
                ///Biller
                ///(all others...)

                billeradapter.Update(dataset.Billers);
                issuedadapter.Update(dataset.Issued);
                fiscaladapter.Update(dataset.FiscalRegime);
                placeadapter.Update(dataset.PlaceDispatch);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                throw ex;
            }
            finally
            {
                if (this.Connection.State != ConnectionState.Closed)
                    this.Connection.Close();
            }

            return true;
        }
    }
}