﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PACFD.DataAccess.BillingsDataSetTableAdapters;
using PACFD.Common;
using System;
namespace PACFD.DataAccess
{


    public partial class ExportContpaqDataSet
    {
        partial class ExportInfoDataTable
        {
        }

        partial class ExportInfoDetailsDataTable
        {
        }

        partial class MasterDataTable
        {
        }
    }


    public class ExportContpaq_DA
    {
        private string conecctionStringData;

        public ExportContpaq_DA()
        {

            if (ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"] == null)
            {

            }

            this.conecctionStringData = ConfigurationManager.ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"].ConnectionString;

        }

        public ExportContpaqDataSet GetExportData(int billingID, DateTime day, decimal exchangeRate)
        {


            ExportContpaqDataSet ds = new ExportContpaqDataSet();
            using (SqlDataAdapter sqlAdpater = new SqlDataAdapter("spExportContpaq", conecctionStringData))
            {
                sqlAdpater.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@BillerID", billingID));
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@Day", day));
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@ExchangeRate", exchangeRate));

                sqlAdpater.TableMappings.Add("Table", ds.Master.TableName);
                sqlAdpater.TableMappings.Add("Table1", ds.Details.TableName);

                sqlAdpater.Fill(ds);
            }
            return ds;

        }

        public ExportContpaqDataSet FillTables(int BillerID, DateTime Date, decimal exchangeRate)
        {
            PACFD.DataAccess.ExportContpaq_DA export = new PACFD.DataAccess.ExportContpaq_DA();
            ExportContpaqDataSet ds = new ExportContpaqDataSet();
            ExportContpaqDataSet.ExportInfoDetailsRow row = null;
            ExportContpaqDataSet.ExportInfoRow rowExportInfo = null;

            ds = export.GetExportData(BillerID, Date, exchangeRate);
            row = ds.ExportInfoDetails.NewExportInfoDetailsRow();
            rowExportInfo = ds.ExportInfo.NewExportInfoRow();

            if (ds.Master.Count < 1 || ds.Details.Count < 1)
                return null;

            //Header
            rowExportInfo.Number = Date.ToString("yyyyMMdd") + " 1 00000001 1 000";
            rowExportInfo.Date = Date;
            rowExportInfo.Reference = ds.Master[0].BillerName;
            rowExportInfo.EchangeRate = exchangeRate;
            ds.ExportInfo.AddExportInfoRow(rowExportInfo);

            for (int i = 0; i < ds.Master.Count; i += 1)
            {
                //Bank
                row = ds.ExportInfoDetails.NewExportInfoDetailsRow();
                row.Type = 1;
                row.Account = ds.Master[i].BankAccount;
                row.Reference = ds.Master[i].Reference;
                row.Description = ds.Master[i].BankAccountReference;
                row.ChargeMXN = ds.Master[i].BankAmountMXN;
                row.ChargeUSD = ds.Master[i].BankAmountUSD;
                row.AmountMXN = ds.Master[i].BankAmountMXN;
                row.AmountUSD = ds.Master[i].BankAmountUSD;
                ds.ExportInfoDetails.AddExportInfoDetailsRow(row);

                //IVA
                row = ds.ExportInfoDetails.NewExportInfoDetailsRow();
                row.Type = 2;
                row.Account = ds.Master[i].IvaAccount;
                row.Reference = ds.Master[i].Reference;
                row.Description = ds.Master[i].IvaAccountReference;
                row.DepositMXN = ds.Master[i].IvaAmountMXN;
                row.DepositUSD = ds.Master[i].IvaAmountUSD;
                row.AmountMXN = ds.Master[i].IvaAmountMXN;
                row.AmountUSD = ds.Master[i].IvaAmountUSD;
                ds.ExportInfoDetails.AddExportInfoDetailsRow(row);

                for (int j = 0; j < ds.Details.Count; j += 1)
                {
                    if (ds.Details[j].BillingID == ds.Master[i].BillingID)
                    {
                        //Rows
                        row = ds.ExportInfoDetails.NewExportInfoDetailsRow();
                        row.Type = 2;
                        row.Account = ds.Details[j].Account;
                        row.Reference = ds.Master[i].Reference;
                        row.Description = ds.Details[j].AccountReference;
                        row.DepositMXN = ds.Details[j].AmountMXN;
                        row.DepositUSD = ds.Details[j].AmountUSD;
                        row.AmountMXN = ds.Details[j].AmountMXN;
                        row.AmountUSD = ds.Details[j].AmountUSD;
                        ds.ExportInfoDetails.AddExportInfoDetailsRow(row);
                    }
                }
            }
            return ds;
        }
    }
}