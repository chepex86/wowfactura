﻿using System.Data.SqlClient;

namespace PACFD.DataAccess
{ public partial class TaxBaseTemplatesDataSet { partial class IsTaxBaseTypeUsedDataTable { } } }

namespace PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters
{
    partial class TaxBaseTemplatesTableAdapter
    {
        public bool UpdateTaxBaseTemplate_TaxBaseTemplateTaxType(PACFD.DataAccess.TaxBaseTemplatesDataSet dataset)
        {
            SqlTransaction transaction = null;
            PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplatesTableAdapter templateadapter = new TaxBaseTemplatesTableAdapter();
            PACFD.DataAccess.TaxBaseTemplatesDataSetTableAdapters.TaxBaseTemplateTaxTypeTableAdapter taxtypeadapter = new TaxBaseTemplateTaxTypeTableAdapter();

            try
            {
                templateadapter.Connection =
                    taxtypeadapter.Connection =
                    this.Connection;
                this.Connection.Open();

                transaction = this.Connection.BeginTransaction();

                templateadapter.Transaction =
                    taxtypeadapter.Transaction = transaction;

                templateadapter.Update(dataset.TaxBaseTemplates);
                taxtypeadapter.Update(dataset.TaxBaseTemplateTaxType);

                transaction.Commit();
            }
            catch (System.Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                throw ex;
            }
            finally
            {
                if (this.Connection.State != System.Data.ConnectionState.Closed)
                    this.Connection.Close();
            }

            return true;
        }

        public DataAccess.TaxBaseTemplatesDataSet FillTaxBaseTemplate_TaxBaseTemplateID(int taxbasetemplateid)
        {
            DataAccess.TaxBaseTemplatesDataSet dataset = new TaxBaseTemplatesDataSet();

            using (SqlDataAdapter sqlAdpater = new SqlDataAdapter("spTaxBaseTemplate_GetTaxBaseTemplate_TaxBaseTemplateTaxType_ByTaxBaseTemplateID",
                this.Connection))
            {
                sqlAdpater.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlAdpater.SelectCommand.Parameters.Add(new SqlParameter("@TaxBaseTemplateID", taxbasetemplateid));

                sqlAdpater.TableMappings.Add("Table", dataset.TaxBaseTemplates.TableName);
                sqlAdpater.TableMappings.Add("Table1", dataset.TaxBaseTemplateTaxType.TableName);

                sqlAdpater.Fill(dataset);
            }

            return dataset;
        }
    }
}