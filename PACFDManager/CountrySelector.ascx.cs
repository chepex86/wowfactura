﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess.CountrySelectorDataSetTableAdapters;
using PACFD.Rules;

namespace PACFDManager
{
    public partial class CountrySelector : PACFDManager.BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            //this.lblCountry.Width =
            //    this.lblState.Width =
            //    this.lblCity.Width = this.ColumnWidth == 0 ? 100 : this.ColumnWidth;

            //this.LoadCountryDropDownList(false);
        }

        
        public String GetStateName { get { return this.ddlState.Visible ? this.ddlState.SelectedValue : this.txtState.Text; } }
        public String GetCountryName { get { return this.ddlCountry.SelectedIndex == 0 ? this.ddlCountry.Items[0].Text : this.txtCountry.Text; } }
        public string SelectCountry
        {
            get { return this.ddlCountry.Visible ? this.ddlCountry.SelectedValue : this.txtCountry.Text; }
            set
            {
                if (value != "México")
                {
                    this.ddlCountry.ClearSelection();
                    this.ddlCountry.SelectedIndex = 1;
                    this.txtCountry.Text = value;
                    this.ddlCountry_SelectedIndexChanged(this.ddlCountry, EventArgs.Empty);
                }

                if (this.ddlCountry.SelectedIndex == 0)
                    this.ddlCountry.SelectedIndex = this.ddlCountry.Items.IndexOf(new ListItem(value));
                else
                    this.txtCountry.Text = value;
            }
        }
        public String SelectState
        {
            set
            {
                if (this.ddlState.Visible)
                {

                    if (this.ddlState.Items.IndexOf(this.ddlState.Items.FindByText(value)) < 0)
                        this.txtState.Text = value;
                    else
                        this.ddlState.SelectedIndex = this.ddlState.Items.IndexOf(this.ddlState.Items.FindByText(value));
                }
                else
                    this.txtState.Text = value;
            }
        }
        /// <summary>
        /// Sets the property tooltip. 
        /// </summary>
        public String ToolTipState { set { this.spnState.Attributes["title"] = value; } }
        /// <summary>
        /// Sets the property tooltip. 
        /// </summary>
        public String ToolTipCountry { set { this.spnCountry.Attributes["title"] = value; } }
        public Boolean Enabled { set { this.ddlState.Enabled = value; } }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = 0;
            int.TryParse(this.ddlCountry.SelectedItem.Value, out i);
            this.ddlState.Visible = !(this.txtCountry_RequiredFieldValidator.Visible = this.txtCountry.Visible = this.txtState.Visible = (i != 0));

            //switch (i)
            //{
            //    default:
            //    case 0:
            //        this.txtCountry_RequiredFieldValidator.Visible =
            //            this.txtCountry.Visible =
            //            this.txtState.Visible = false;
            //        this.ddlState.Visible = true;
            //        break;
            //    case 1:
            //        this.txtCountry_RequiredFieldValidator.Visible =
            //        this.txtCountry.Visible =
            //            this.txtState.Visible = true;
            //        this.ddlState.Visible = false;
            //        break;
            //}
        }

        #region ...
        /* public ListItem City
        {
            get
            {
                if (!this.ddlCity.SelectedItem.IsNull())
                    return this.ddlCity.SelectedItem;

                if (this.ddlCity.Items.Count > 0)
                    return this.ddlCity.Items[0];

                return null;
            }
        }
        public ListItemCollection CityItems { get { return this.ddlCity.Items; } }
        public ListItem State
        {
            get
            {
                if (!this.ddlState.SelectedItem.IsNull())
                    return this.ddlState.SelectedItem;

                if (this.ddlState.Items.Count > 0)
                    return this.ddlState.Items[0];

                return null;
            }
            set
            {

                foreach (ListItem item in this.ddlState.Items)
                {
                    if (item.Text == value.Text && item.Value == value.Value)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
        }
        public ListItemCollection StateItems { get { return this.ddlState.Items; } }
        public ListItem Country
        {
            get
            {
                if (!this.ddlCountry.SelectedItem.IsNull())
                    return this.ddlCountry.SelectedItem;

                if (this.ddlCountry.Items.Count > 0)
                    return this.ddlCountry.Items[0];

                return null;
            }
        }
        public ListItemCollection CountryItems { get { return this.ddlCity.Items; } }
        public string DefaultCountry
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return "";

                return o.ToString();
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        public Boolean Enabled
        {
            get { return this.ddlCity.Enabled; }
            set
            {
                this.ddlCity.Enabled =
                    this.ddlCountry.Enabled =
                    this.ddlState.Enabled = value;
            }
        }
        public int ColumnWidth
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlState.Items.Clear();
            PACFD.Rules.CountrySelectors select = new PACFD.Rules.CountrySelectors();

            this.ddlState.DataSource = select.SelectStateByCountryID(this.ddlCountry.SelectedValue);
            this.ddlState.DataBind();
            this.ddlState.Enabled = this.Enabled;

            if (this.ddlState.Items.Count > 0)
                this.ddlState_SelectedIndexChanged(this, EventArgs.Empty);
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            PACFD.Rules.CountrySelectors select = new PACFD.Rules.CountrySelectors();

            this.ddlCity.Items.Clear();
            this.ddlCity.DataSource = select.SelectCityByStateCode(this.ddlState.SelectedValue);
            this.ddlCity.DataBind();
            this.ddlCity.Enabled = this.Enabled;
        }
        public void SetCountry(string country, string state, string city)
        {
            int index;

            this.LoadCountryDropDownList(true);
            index = 0;

            foreach (ListItem item in this.ddlState.Items)
            {
                if (item.Value != state)
                {
                    index++;
                    continue;
                }
                

                
                this.ddlState.SelectedIndex = index;
                this.ddlState_SelectedIndexChanged(null, EventArgs.Empty);
                break;
            }

            index = 0;
            foreach (ListItem item in this.ddlCity.Items)
            {
                if (item.Value != city)
                {
                    index++;
                    continue;
                }

                this.ddlCity.SelectedIndex = index;
                break;
            }
        }
        private void LoadCountryDropDownList(bool forceload)
        {
            PACFD.Rules.CountrySelectors select;
            ListItem ditem;

            if (!forceload && this.ddlCity.Items.Count > 0)
                return;

            select = new PACFD.Rules.CountrySelectors();
            this.ddlCountry.DataSource = select.SelectAllCountries();
            this.ddlCountry.DataBind();

            foreach (ListItem item in this.ddlCountry.Items)
            {
                if (item.Value == "MX")
                {
                    ditem = item;
                    this.ddlCountry.Items.Clear();
                    this.ddlCountry.Items.Add(ditem);
                    ditem.Selected = true;
                    break;
                }
            }

            if (this.ddlCountry.Items.Count > 0)
                this.ddlCountry_SelectedIndexChanged(this, EventArgs.Empty);
        }
        /// <summary>
        /// Sets the property tooltip. 
        /// </summary>
        public String ToolTipCountry { set { this.spnCountry.Attributes["title"] = value; } }
        /// <summary>
        /// Sets the property tooltip. 
        /// </summary>
        public String ToolTipState { set { this.spnState.Attributes["title"] = value; } }
        /// <summary>
        /// Sets the property tooltip. 
        /// </summary>
        public String ToolTipCity { set { this.spnCity.Attributes["title"] = value; } }*/
        #endregion
    }
}