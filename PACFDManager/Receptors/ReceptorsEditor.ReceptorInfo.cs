﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Receptors
{
    partial class ReceptorsEditor
    {
        public class ReceptorsInfo
        {
            public ReceptorsEditor Parent { get; private set; }
            public string Name
            {
                get { return this.Parent.txtName.Text; }
                set { this.Parent.txtName.Text = value; }
            }
            public string RFC
            {
                get { return this.Parent.txtRFC.Text; }
                set { this.Parent.txtRFC.Text = value; }
            }
            public string Address
            {
                get { return this.Parent.txtAddress.Text; }
                set { this.Parent.txtAddress.Text = value; }
            }
            public string Phone
            {
                get { return this.Parent.txtPhone.Text; }
                set { this.Parent.txtPhone.Text = value; }
            }
            public string ExternalNumber
            {
                get { return this.Parent.txtExternalNumber.Text; }
                set { this.Parent.txtExternalNumber.Text = value; }
            }
            public string InternalNumber
            {
                get { return this.Parent.txtInternalNumber.Text; }
                set { this.Parent.txtInternalNumber.Text = value; }
            }
            public string Colony
            {
                get { return this.Parent.txtColony.Text; }
                set { this.Parent.txtColony.Text = value; }
            }
            //public string Location
            //{
            //    get { return this.Parent.txtLocation.Text; }
            //    set { this.Parent.txtLocation.Text = value; }
            //}
            public string Reference
            {
                get { return this.Parent.txtReference.Text; }
                set { this.Parent.txtReference.Text = value; }
            }
            public String City
            {
                get { return this.Parent.txtCity.Text; }
                set { this.Parent.txtCity.Text = value; }
            }
            public string Municipality
            {
                get { return this.Parent.txtMunicipality.Text; }
                set { this.Parent.txtMunicipality.Text = value; }
            }
            //public string State
            //{
            //    get { return this.Parent.txtEstate.Text; }
            //    set { this.Parent.txtEstate.Text = value; }
            //}
            //public string Country
            //{
            //    get { return this.Parent.txtCountry.Text; }
            //    set { this.Parent.txtCountry.Text = value; }
            //}
            public String Zipcode
            {
                get
                {
                    /*
                    int i;

                    if (string.IsNullOrEmpty(this.Parent.txtZipcode.Text))
                        return -1;

                    if (!int.TryParse(this.Parent.txtZipcode.Text, out i))
                        i = -1;

                    return i;*/
                    return this.Parent.txtZipcode.Text;
                }
                set { this.Parent.txtZipcode.Text = value.ToString() == "-1" ? String.Empty : value.ToString(); }
            }
            public string Email
            {
                get { return this.Parent.txtEmail.Text; }
                set { this.Parent.txtEmail.Text = value; }
            }

            public ReceptorsInfo(ReceptorsEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
