﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceptorsEditor.ascx.cs"
    Inherits="PACFDManager.Receptors.ReceptorsEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../CountrySelector.ascx" TagName="CountrySelector" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc3" %>
<div id="container" class="wframe90">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitle" runat="server" Text="Cliente"></asp:Label>
                </h2>
            </div>
            <ul>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblName" runat="server" Text="Nombre:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el Nombre ó la razón social con la cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="255" Width="450px" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblRFC" runat="server" Text="RFC:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el RFC con el cual el cliente se encuentra registrado<br />en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).<br /><br />Ej. <b>ABC123456ABC</b><br /><b>ABCD123456ABC</b>">
                                    <asp:TextBox ID="txtRFC" runat="server" MaxLength="13" Width="300px" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg" style="width: 330px">
                                    <asp:RequiredFieldValidator ID="rfvRFC" runat="server" ControlToValidate="txtRFC"
                                        Display="Dynamic" ErrorMessage="Este campo es requerido" />
                                    <asp:RegularExpressionValidator ID="revRFC" runat="server" Display="Dynamic" ErrorMessage="RFC no valido. XXX[X]999999NNN"
                                        ControlToValidate="txtRFC" ValidationExpression="([A-Z|a-z]|&amp;){3,4}\d{6}\w{3}$" />
                                    <!--
                                        ([A-Z|a-z]|&amp;){3,4}\d{6}\w{3}$
                                        [A-Z,Ñ,&amp;]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?
                                        -->
                                </div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblAddress" runat="server" Text="Dirección:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce la Dirección fisica con la cual el cliente se encuentra registrado<br />en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtAddress" runat="server" Width="450px" MaxLength="255" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblExternalNumber" runat="server" Text="Número externo:" /></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el número exterior del lugar (local, edificio, terreno), con el cual el cliente<br />se encuentra registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtExternalNumber" runat="server" Width="120px" MaxLength="50" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="right">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblInternalNumber" runat="server" Text="Número interno:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el número interior del lugar (local, edificio, terreno), con el cual el cliente<br />se encuentra registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtInternalNumber" runat="server" Width="120px" MaxLength="50" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblColony" runat="server" Text="Colonia:"></asp:Label>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el nombre de la Colonia con la cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtColony" runat="server" MaxLength="255" Width="450px" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblPhone" runat="server" Text="Teléfono:"></asp:Label>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el número de teléfono fijo del cliente.<br />Ej. <b>01 612 12 2 22 22</b>">
                                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" Width="200px" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblCity" runat="server" Text="Ciudad:"></asp:Label>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el nombre de la Ciudad con la cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtCity" runat="server" MaxLength="20" Width="180px"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblMunicipality" runat="server" Text="Municipio:"></asp:Label>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el nombre del Municipio con el cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtMunicipality" runat="server" MaxLength="40" Width="180px"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    </table>
                    <%-- <div class="validator-msg" style="width: 180px">
                        <asp:RequiredFieldValidator ID="rfvMunicipality" runat="server" ControlToValidate="txtMunicipality"
                            Display="Dynamic" ErrorMessage="Este campo es requerido"></asp:RequiredFieldValidator></div>--%>
                </li>
                <uc1:CountrySelector ID="CountrySelector1" runat="server" DefaultCountry="Mexico"
                    ToolTipCountry="Muestra el nombre del País con el cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público)."
                    ToolTipState="Selecciona el nombre del Estado con el cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público)." />
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblReference" runat="server" Text="Referencia:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce alguna referencia que ayude a ubicar<br />más fácilmente la dirección del cliente.<br />Ej. <b>Una calle, un edificio, un monumento, etc.</b> ">
                                    <asp:TextBox ID="txtReference" runat="server" MaxLength="255" Width="450px" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblZipcode" runat="server" Text="Código postal:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce el Código postal con el cual el cliente se encuentra<br />registrado en la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).">
                                    <asp:TextBox ID="txtZipcode" runat="server" MaxLength="5" Width="200px" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg">
                                    <%--<asp:RequiredFieldValidator ID="rfvZipcode" runat="server" ControlToValidate="txtZipcode"
                            Display="Dynamic" ErrorMessage="Este campo es requerido"></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="revZopcode" runat="server" Display="Dynamic"
                                        ErrorMessage="Solo numeros (0 - 9)." ControlToValidate="txtZipcode" ValidationExpression="^\d+$" /></div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="clear">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblEmail" runat="server" Text="Correo Electrónico:" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Introduce una dirección de correo electrónico valida.<br />Ej. <b>nombre@mail.com</b>">
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="200px" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg">
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email incorrecto (ejempl@oz.com)"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" /></div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="left" id="liTaxSystem" runat="server" style="height: auto;">
                    <div>
                        <table>
                            <tr>
                                <td style="width:400px">
                                    <label class="desc">
                                        <asp:Label ID="lblTaxSystem" runat="server" Text="Uso CFDI" />

                                    </label>
                                    <div class="validator-msg" runat="server" visible="false" id="divErrorTaxSystem"
                                        style="color: #ff1818; background-color: #feeaea;">
                                        Uso CFDI es necesario.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td  style="width:400px">
                                    <span class="vtip" title="Uso CFDI.">
                                        <asp:DropDownList ID="ddlUsoCFDI" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>

                 <li class="clear">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                     <label class="desc">
                                        <asp:Label ID="Label1" runat="server" Text="Regimen Fiscal Obligatorio" />

                                    </label>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Régimen fiscal. CFDI 4.0 Obligatorio">
                                    <asp:DropDownList ID="ddlTaxSystem" runat="server">
                                        </asp:DropDownList>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                  <div class="validator-msg" runat="server" visible="false" id="divErrorFiscalRegimen"
                                        style="color: #ff1818; background-color: #feeaea;">
                                        Régimen fiscal. CFDI 4.0 Obligatorio.
                                    </div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="buttons">
                    <!--Command button layout-->
                    <div>
                        <asp:Button CssClass="Button" ID="btnAcept" runat="server" Text="Aceptar" OnClick="btnAcept_Click"
                            Height="26px" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
                            CausesValidation="false" CssClass="Button" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
