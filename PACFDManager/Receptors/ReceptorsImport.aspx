﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="ReceptorsImport.aspx.cs"
    Inherits="PACFDManager.Receptors.ReceptorsImport" %>

<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Importar Clientes"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblSelectPath" runat="server" Text="Selecciona la ruta del archivo:"></asp:Label>
                                </label>
                                <span class="vtip" title="Selecciona la ruta del archivo [<b>*.txt</b>]<br />que se va importar al sistema.">
                                    <asp:FileUpload ID="fuReceptorsImport" runat="server" /></span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvReceptorsImport" runat="server" ControlToValidate="fuReceptorsImport"
                                        Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator></div>
                            </li>
                            <li class="buttons clear">
                                <div>
                                    <asp:Button ID="btnLoadFile" runat="server" CssClass="Button" Text="Cargar Archivo"
                                        ValidationGroup="Validators" OnClick="btnLoadFile_Click" /></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="txt_left" id="dvList" runat="server" visible="false">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleList" runat="server" Text="Listado de Clientes a Importar" />
                    </h2>
                </div>
                <span>
                    <asp:GridView ID="gvReceptorsTemplate" runat="server" AutoGenerateColumns="False"
                        OnRowDataBound="gvReceptorsTemplate_RowDataBound" Visible="False">
                        <Columns>
                            <asp:TemplateField HeaderText="RFC">
                                <ItemTemplate>
                                    <span id="spnRFC1" runat="server" visible="false" class="vtip" style="cursor: help;"
                                        title='<%#"Ya existe en el sistema un cliente con el RFC: <b>" + Eval("RFC") + "</b>.<br />Selecciona una de las 2 opciones para resolver el conflicto."%>'>
                                        <asp:Label ID="lblRFC1" runat="server" Text='<%#Eval("RFC")%>'></asp:Label>
                                    </span><span id="spnRFC2" runat="server">
                                        <asp:Label ID="lblRFC" runat="server" Text='<%#Eval("RFC")%>'></asp:Label>
                                    </span>
                                    <asp:HiddenField ID="hdState" runat="server" Value='<%#Eval("RFCExist")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre" DataField="Name" />
                            <asp:BoundField HeaderText="Dirección" DataField="Address" />
                            <asp:BoundField HeaderText="Ciudad" DataField="City" />
                            <asp:BoundField DataField="Municipality" HeaderText="Municipio" />
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <span class="vtip" title='No se encontro el Estado asignado.<br />Selecciona un Estado de la lista.'>
                                        <asp:DropDownList ID="ddlStates" runat="server" Visible="False">
                                            <asp:ListItem>Aguascalientes</asp:ListItem>
                                            <asp:ListItem>Baja California</asp:ListItem>
                                            <asp:ListItem>Baja California Sur</asp:ListItem>
                                            <asp:ListItem>Chihuahua</asp:ListItem>
                                            <asp:ListItem>Colima</asp:ListItem>
                                            <asp:ListItem>Campeche</asp:ListItem>
                                            <asp:ListItem>Chiapas</asp:ListItem>
                                            <asp:ListItem>Coahuila</asp:ListItem>
                                            <asp:ListItem>Distrito Federal</asp:ListItem>
                                            <asp:ListItem>Durango</asp:ListItem>
                                            <asp:ListItem>Guanajuato</asp:ListItem>
                                            <asp:ListItem>Guerrero</asp:ListItem>
                                            <asp:ListItem>Hidalgo</asp:ListItem>
                                            <asp:ListItem>Jalisco</asp:ListItem>
                                            <asp:ListItem>Michoacan</asp:ListItem>
                                            <asp:ListItem>Morelos</asp:ListItem>
                                            <asp:ListItem>Estado Mexico</asp:ListItem>
                                            <asp:ListItem>Nayarit</asp:ListItem>
                                            <asp:ListItem>Nuevo Leon</asp:ListItem>
                                            <asp:ListItem>Oaxaca</asp:ListItem>
                                            <asp:ListItem>Puebla</asp:ListItem>
                                            <asp:ListItem>Quintana Roo</asp:ListItem>
                                            <asp:ListItem>Queretaro</asp:ListItem>
                                            <asp:ListItem>Sinaloa</asp:ListItem>
                                            <asp:ListItem>Sonora</asp:ListItem>
                                            <asp:ListItem>San Luis Potosi</asp:ListItem>
                                            <asp:ListItem>Tabasco</asp:ListItem>
                                            <asp:ListItem>Tlaxcala</asp:ListItem>
                                            <asp:ListItem>Tamaulipas</asp:ListItem>
                                            <asp:ListItem>Veracruz</asp:ListItem>
                                            <asp:ListItem>Yucatan</asp:ListItem>
                                            <asp:ListItem>Zacatecas</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <asp:Label ID="lblState" runat="server" Text='<%#Eval("State")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Pais" DataField="Country" />
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <span id="spnState1" runat="server" class="vtip" title="Selecciona una de las 2 acciones a<br />realizar con un cliente en conflicto:<br/><br /><b>- Ignorar.</b> El cliente sera descartado<br />y no se agregara al sistema.<br /><b>- Sobreescribir.</b> El cliente sera agregado<br />al sistema, reemplazando al cliente<br />ya existente."
                                        visible="true">
                                        <uc1:Tooltip ID="ttState1" runat="server" />
                                    </span><span id="spnState2" runat="server" class="vtip" title="La imagen <img src='../Includes/Images/png/apply.png' alt='Agregado' /> significa que el cliente<br />fue importado correctamente al sistema.<br /><br />La imagen <img src='../Includes/Images/png/deleteIcon.png' alt='No agregado' /> significa que el cliente<br />no pudo ser importado al sistema."
                                        visible="false">
                                        <uc1:Tooltip ID="ttState2" runat="server" />
                                    </span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:DropDownList ID="ddlState" runat="server" Visible="false">
                                            <asp:ListItem>Ignorar</asp:ListItem>
                                            <asp:ListItem>Sobreescribir</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Image ID="imgState" Visible="false" runat="server" ImageUrl="~/Includes/Images/png/apply.png" />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div>
                        <asp:Label ID="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                </span>
                <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="Button" Visible="false"
                    OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button" Visible="false"
                    OnClick="btnCancel_Click" />
                <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" Visible="false"
                    CausesValidation="false" PostBackUrl="~/Receptors/ReceptorsList.aspx" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoadFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
