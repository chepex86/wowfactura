﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Receptors
{
    public class ReceptorsSearchEventArgs : EventArgs
    {
        public string Name { get; private set; }
        public string RFC { get; private set; }
        public ReceptorsSearchMode SearchMode { get; private set; }

        public ReceptorsSearchEventArgs(string rfc, string name, ReceptorsSearchMode editmode)
        {
            this.SearchMode = editmode;
            this.RFC = rfc;
            this.Name = name;
        }
    }
}
