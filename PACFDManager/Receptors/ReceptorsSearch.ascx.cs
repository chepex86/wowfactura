﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Receptors
{
    public partial class ReceptorsSearch : System.Web.UI.UserControl
    {
        public delegate void SearchEventHandler(object sender, ReceptorsSearchEventArgs e);
        public event SearchEventHandler Search;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterScriptsForProgressBar();
        }
        private void RegisterScriptsForProgressBar()
        {
            if (this.Page.ClientScript.IsStartupScriptRegistered(String.Format("LoadBindControls_{0}", this.Page.ClientID)))
                return;

            String scriptToRegister = String.Empty;
            String uProgressByName = String.Empty;
            String uProgressByRFC = String.Empty;

            uProgressByName = String.Format("jsReceptor.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtName_AutoCompleteExtender.BehaviorID, this.spnByName.ClientID, true);

            uProgressByRFC = String.Format("jsReceptor.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtRFC_AutoCompleteExtender.BehaviorID, this.spnByRFC.ClientID, true);

            scriptToRegister = "Sys.Application.add_load(function() {";
            scriptToRegister += String.Format("{2}{0}{2}{1}{2}", uProgressByName, uProgressByRFC, System.Environment.NewLine);
            scriptToRegister += "});";

            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), String.Format("LoadBindControls_{0}", this.Page.ClientID), scriptToRegister, true);
        }
        protected void OnSearch(ReceptorsSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ReceptorsSearchMode mode = ReceptorsSearchMode.None;
            ReceptorsSearchEventArgs a;
            Button b = sender as Button;

            if (b.IsNull())
                return;

            //if (this.btnClean == b)
            //{
            //    this.txtName.Text =
            //        this.txtRFC.Text = string.Empty;
            //    return;
            //}

            if (this.txtName.Text.Trim().Length > 0 && this.txtRFC.Text.Trim().Length > 0)
                mode = ReceptorsSearchMode.Both;
            else if (this.txtName.Text.Trim().Length > 0)
                mode = ReceptorsSearchMode.Name;
            else if (this.txtRFC.Text.Trim().Length > 0)
                mode = ReceptorsSearchMode.RFC;

            a = new ReceptorsSearchEventArgs(
                mode == ReceptorsSearchMode.Both || mode == ReceptorsSearchMode.RFC ? this.txtRFC.Text.Trim() : null,
                mode == ReceptorsSearchMode.Both || mode == ReceptorsSearchMode.Name ? this.txtName.Text.Trim() : null,
                mode);

            this.OnSearch(a);
        }
    }
}