﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;
using PACFDManager.Billings;
#endregion

namespace PACFDManager.Receptors
{
    /// <summary>
    /// Control class used to edit or add a new receptor row.
    /// </summary>
    public partial class ReceptorsEditor : BaseUserControl
    {
        /// <summary>
        /// Event handler delegate of the Applychanges event.
        /// </summary>
        /// <param name="sender">Object that send the event.</param>
        /// <param name="e">Event arguments send by the object.</param>
        public delegate void ApplychangesEventHandler(object sender, ReceptorsEditorEventArgs e);
        /// <summary>
        /// Event that fire the ApplychangesEventHandler delegate.
        /// </summary>
        public event ApplychangesEventHandler ApplyChanges;
        /// <summary>
        /// Event that fire the CancelClick delegate.
        /// </summary>
        public event EventHandler CancelClick;
        /// <summary>
        /// Get or Set a ReceptorsEditorMode value with the control mode.
        /// </summary>
        public ReceptorsEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return ReceptorsEditorMode.Add;

                return o.GetType() == typeof(ReceptorsEditorMode) ? (ReceptorsEditorMode)o : ReceptorsEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get a ReceptorsInfo class with the main information of the control.
        /// </summary>
        public ReceptorsInfo ReceptorsInformation { get; private set; }
        /// <summary>
        /// Get or Set an int value with the ID of the Receptor to edit.
        /// </summary>
        public int ReceptorID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get or Set an int value with the ID of the Biller owner of the receptor to edit.
        /// </summary>
        public int BillerID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Enabled
        {
            get { return this.txtZipcode.Enabled; }
            set
            {
                this.txtAddress.ReadOnly =
                    this.txtColony.ReadOnly =
                    //this.txtCountry.Enabled =
                    //this.txtEstate.Enabled =
                    this.txtExternalNumber.ReadOnly =
                    this.txtInternalNumber.ReadOnly =
                    //this.txtLocation.Enabled =
                    this.txtMunicipality.ReadOnly =
                    this.txtName.ReadOnly =
                    this.txtPhone.ReadOnly =
                    this.txtReference.ReadOnly =
                    this.txtRFC.ReadOnly =
                    this.txtZipcode.ReadOnly =
                    this.txtEmail.ReadOnly =
                    this.txtCity.ReadOnly = value ? false : true;
                this.btnCancel.Enabled =
                this.btnAcept.Enabled =
                this.CountrySelector1.Enabled =
                value;
            }
        }
        /// <summary>
        /// Get or set a boolean value indicating if the accept button is visible.
        /// </summary>
        public bool VisibleAcceptButton
        {
            get { return this.btnAcept.Visible; }
            set
            {
                if (!this.Visible)
                    value = false;

                this.btnAcept.Visible = value;
            }
        }
        /// <summary>
        /// Get or set a boolean value indicating if the cancel button is visible.
        /// </summary>
        public bool VisibleCancelButton
        {
            get { return this.btnCancel.Visible; }
            set
            {
                if (!this.Visible)
                    value = false;

                this.btnCancel.Visible = value;
            }
        }
        /// <summary>
        /// Get or set a boolean value indicating if the accept button is enabled.
        /// </summary>
        public bool EnabledAcceptButton { get { return this.btnAcept.Enabled; } set { this.btnAcept.Enabled = value; } }
        /// <summary>
        /// Get or set a boolean value indicating if the cancel button is enabled.
        /// </summary>
        public bool EnabledCancelButton { get { return this.btnCancel.Enabled; } set { this.btnCancel.Enabled = value; } }
        public String TitleMode { set { this.lblTitle.Text = value; } }

        public bool selected { get; set; }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public ReceptorsEditor()
        {
            this.ReceptorsInformation = this.OnGenerateReceptorsInformation();
        }
        /// <summary>
        /// Load send by the control. 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            if (!selected)
            {
                this.ddlUsoCFDI.Items.Clear();
                this.ddlUsoCFDI.DataTextField = "Descripcion";
                this.ddlUsoCFDI.DataValueField = "c_UsoCFDI";
                this.ddlUsoCFDI.DataSource = Helpers.GetUsoCFDI();
                this.ddlUsoCFDI.DataBind();

              
                this.ddlTaxSystem.Items.Clear();
                this.ddlTaxSystem.DataTextField = "Descripcion";
                this.ddlTaxSystem.DataValueField = "c_RegimenFiscal";
                this.ddlTaxSystem.DataSource = Helpers.GetTaxSystem();
                this.ddlTaxSystem.DataBind();
               
            }
            //this.CountrySelector1.DefaultCountry = "Mexico";
        }
        /// <summary>
        /// Fire the ApplyChanges event of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected void OnApplyChanges(ReceptorsEditorEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// Fire the CancelClick event of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Click method of the acept button.
        /// </summary>
        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.OnApplyChanges(this.OnGenerateAcceptArguments());
        }
        /// <summary>
        /// Cancel method of the cancel button.
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
        /// <summary>
        /// Generate a new instance of the class of type ReceptorsInfo to control the fields of the control.
        /// </summary>
        /// <returns>Returns a ReceptorsInfo class.</returns>
        protected virtual ReceptorsInfo OnGenerateReceptorsInformation()
        {
            return new ReceptorsInfo(this);
        }
        /// <summary>
        /// Generate a new instance of the class ReceptorsEditorEventArgs.
        /// </summary>
        /// <returns>
        /// Return a ReceptorsEditorEventArgs containing a 
        /// PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table with
        /// one PACFD.DataAccess.ReceptorsDataSet.ReceptorsRow row and 
        /// the editor mode.
        /// </returns>
        protected ReceptorsEditorEventArgs OnGenerateAcceptArguments()
        {
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsRow row;

            table = new PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable();

            row = table.NewReceptorsRow();
            row.BeginEdit();
            row.Name = this.ReceptorsInformation.Name;
            row.RFC = this.ReceptorsInformation.RFC;
            row.Address = this.ReceptorsInformation.Address;
            row.Phone = this.ReceptorsInformation.Phone;
            row.ExternalNumber = this.ReceptorsInformation.ExternalNumber;
            row.InternalNumber = this.ReceptorsInformation.InternalNumber;
            row.Colony = this.ReceptorsInformation.Colony;
            //row.Location = this.ReceptorsInformation.Location;
            row.Reference = this.ReceptorsInformation.Reference;
            //row.State = this.ReceptorsInformation.State;
            //row.Country = this.ReceptorsInformation.Country;

            row.Location = this.ReceptorsInformation.City;
            row.Municipality = this.ReceptorsInformation.Municipality;
            row.State = this.CountrySelector1.GetStateName;
            row.Country = this.CountrySelector1.GetCountryName;

            row.Zipcode = this.ReceptorsInformation.Zipcode;
            row.BillerID = this.BillerID;
            row.Email = this.ReceptorsInformation.Email;
            row.ReceptorID = this.EditorMode == ReceptorsEditorMode.Edit ? this.ReceptorID : 1;
            row.CFDIUse = this.ddlUsoCFDI.SelectedItem.Value;

            row.FiscalRegime = this.ddlTaxSystem.SelectedItem.Value;

            row.EndEdit();

            table.AddReceptorsRow(row);

            row.AcceptChanges();

            if (this.EditorMode == ReceptorsEditorMode.Edit)
                row.SetModified();
            else if (this.EditorMode == ReceptorsEditorMode.Add)
                row.SetAdded();

            return new ReceptorsEditorEventArgs(table, this.EditorMode);
        }

        public void DataBindReceptorID()
        {
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;

            if (this.ReceptorID < 1)
                return;

            receptor = new PACFD.Rules.Receptors();
            table = receptor.SelectByID(this.ReceptorID);

            if (table.Count < 1)
                return;

            this.ReceptorsInformation.Address = table[0].Address;
            this.ReceptorsInformation.Colony = table[0].Colony;
            this.ReceptorsInformation.ExternalNumber = table[0].ExternalNumber;
            this.ReceptorsInformation.InternalNumber = table[0].InternalNumber;
            this.ReceptorsInformation.Name = table[0].Name;
            this.ReceptorsInformation.Phone = table[0].Phone;
            this.ReceptorsInformation.Reference = table[0].IsReferenceNull() ? string.Empty : table[0].Reference; ;
            this.ReceptorsInformation.RFC = table[0].RFC;
            this.ReceptorsInformation.Zipcode = table[0].Zipcode;
            this.ReceptorsInformation.Email = table[0].IsEmailNull() ? string.Empty : table[0].Email;

            this.txtCity.Text = table[0].Location;
            this.txtMunicipality.Text = table[0].Municipality;
            this.CountrySelector1.SelectState = table[0].State;

           // this.CountrySelector1.SetCountry(table[0].Country, table[0].State, table[0].Location);
            this.CountrySelector1.SelectCountry = table[0].Country;

            this.revRFC.Validate();
            this.rfvRFC.Validate();

            this.ddlUsoCFDI.Items.Clear();
            this.ddlUsoCFDI.DataTextField = "Descripcion";
            this.ddlUsoCFDI.DataValueField = "c_UsoCFDI";
            this.ddlUsoCFDI.DataSource = Helpers.GetUsoCFDI();
            this.ddlUsoCFDI.DataBind();

            this.ddlTaxSystem.Items.Clear();
            this.ddlTaxSystem.DataTextField = "Descripcion";
            this.ddlTaxSystem.DataValueField = "c_RegimenFiscal";
            this.ddlTaxSystem.DataSource = Helpers.GetTaxSystem();
            this.ddlTaxSystem.DataBind();

            var fiscalSelected =  ddlTaxSystem.Items.FindByValue(table[0].IsFiscalRegimeNull() ? string.Empty : table[0].FiscalRegime);
            if (fiscalSelected != null)
                fiscalSelected.Selected = true;

            selected = true;

            var itemselected = ddlUsoCFDI.Items.FindByValue(table[0].IsCFDIUseNull() ? string.Empty : table[0].CFDIUse);

            if (itemselected != null)
                itemselected.Selected = true;
        }
    }
}