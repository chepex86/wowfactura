﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Receptors
{
    public partial class ReceptorsAdd : PACFDManager.BasePage
    {
        const string MESSAGE_QUESTION_ADDNEW = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReceptorsEditor1.BillerID = this.CurrentBillerID;
            this.ReceptorsEditor1.Enabled = true;
        }

        protected void ReceptorsEditor1_ApplyChanges(object sender, ReceptorsEditorEventArgs e)
        {
            PACFD.Rules.Receptors receptos;

            if (e.EditorMode != ReceptorsEditorMode.Add)
                return;

            receptos = new PACFD.Rules.Receptors();

            if (!receptos.Update(e.Table))
            {
                //error...
                LogManager.WriteError(new Exception("Error al registrar cliente."));
                this.WebMessageBox1.ShowMessage("Error al registrar cliente.",
                    System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo cliente [{1}] al sistema.",
                this.UserName, e.Table[0].Name), this.CurrentBillerID, null, null);
            #endregion

            LogManager.WriteStackTrace(new Exception("Exito al registrar cliente."));
            this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Yes && e.CommandArguments == MESSAGE_QUESTION_ADDNEW)
            {
                this.Response.Redirect(typeof(ReceptorsAdd).Name + ".aspx");
                return;
            }

            this.ReceptorsEditor1_CancelClick(this, EventArgs.Empty);
        }

        protected void ReceptorsEditor1_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
        }
    }
}
