﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Receptors
{
    /// <summary>
    /// Class uses to pass as parameter for the receptor editor control.
    /// </summary>
    public class ReceptorsEditorEventArgs : EventArgs
    {
        /// <summary>
        /// Get a ReceptorsEditorMode with the actual editor mode.
        /// </summary>
        public ReceptorsEditorMode EditorMode { get; private set; }
        /// <summary>
        /// Get a PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table 
        /// with the row modified or new.
        /// </summary>
        public PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable Table { get; private set; }



        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="table">
        /// PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table
        /// with the row modified or new.
        /// </param>
        /// <param name="mode">ReceptorsEditorMode value with the editor mode.</param>
        public ReceptorsEditorEventArgs(PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table,
            ReceptorsEditorMode mode)
        {
            this.EditorMode = mode;
            this.Table = table;
        }
    }
}
