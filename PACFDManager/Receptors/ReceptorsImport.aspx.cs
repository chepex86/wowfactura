﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Receptors
{
    public partial class ReceptorsImport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblError.Visible = false;
        }

        private bool LoadData()
        {
            Byte[] stream;
            String file = String.Empty;

            try
            {
                if (!this.fuReceptorsImport.HasFile)
                    return false;

                stream = this.fuReceptorsImport.FileBytes;

                file = Encoding.UTF8.GetString(stream);

                this.ReadFile(file);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        private void ReadFile(String file)
        {
            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            DataTable table = new DataTable();

            if (file == String.Empty)
            {
                this.lblError.Text = "El archivo [" + this.fuReceptorsImport.FileName + "] esta vacio.";
                this.dvList.Visible =
                this.lblError.Visible = true;
                return;
            }

            String err = receptor.ReadFile(ref table, this.CurrentBillerID, file);
            if ((TypesErrorsImport)Convert.ToInt32(err.Split('_')[0]) != TypesErrorsImport.Success)
            {
                this.ErrorMessage(err);
                return;
            }

            if (table.Rows.Count < 1)
            {
                this.lblError.Text = "No se encontro ningun cliente para importar.";
                this.dvList.Visible =
                this.lblError.Visible = true;
                return;
            }

            this.btnSave.Visible =
            this.btnCancel.Visible =
            this.dvList.Visible =
            this.gvReceptorsTemplate.Visible = true;
            this.btnAccept.Visible = false;
            this.lblTitleList.Text = "Listado de Clientes a Importar";

            this.ViewState["ResultMode"] = null;

            String State = "Aguascalientes,Baja California,Baja California Sur,Chihuahua,Colima,Campeche,Chiapas,Coahuila,Distrito Federal,Durango," +
                           "Guanajuato,Guerrero,Hidalgo,Jalisco,Michoacan,Morelos,Estado Mexico,Nayarit,Nuevo Leon,Oaxaca,Puebla,Quintana Roo,Queretaro," +
                           "Sinaloa,Sonora,San Luis Potosi,Tabasco,Tlaxcala,Tamaulipas,Veracruz,Yucatan,Zacatecas";

            bool flag = false;
            int cont = 0;

            for (int i = 0; i < table.Rows.Count; i += 1)
            {
                for (int j = 0; j < 32; j += 1)
                {
                    if (table.Rows[i]["State"].ToString().ToLower().Trim() == State.Split(',')[j].ToLower().Trim())
                    {
                        flag = true;
                        break;
                    }
                    cont += 1;
                }

                if (flag)
                    table.Rows[i]["State"] = State.Split(',')[cont];
                else
                    table.Rows[i]["State"] = "-1";

                cont = 0;
                flag = false;
            }

            this.gvReceptorsTemplate.DataSource = table;
            this.gvReceptorsTemplate.DataBind();
            this.ViewState["tempTable"] = table;
        }

        private void ErrorMessage(String error)
        {
            String Message = String.Empty;
            String error1, error2, x, y, c;
            int pos = 0;

            switch ((TypesErrorsImport)Convert.ToInt32(error.Split('_')[0]))
            {
                case TypesErrorsImport.Twice:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    c = error.Split('_')[3].Split('|')[1];

                    error1 = error.Split('_')[3];
                    error1 = error1.Replace(c, "<b style='color:red'>" + c + "</b>");
                    error2 = error.Split('_')[4];
                    error2 = error2.Replace(c, "<b style='color:red'>" + c + "</b>");

                    Message = "Se detecto duplicado de registros en la fila [" + x + "] y en la fila [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span><br />" + "[" + y + "]---> <span style='color:#305389'>" + error2 + "</span>";
                    break;
                case TypesErrorsImport.MissingHeader:
                    Message = "Ausencia de la cabecera [C] en la fila [" + error.Split('_')[1] + "].<br /><br />" +
                              "[" + error.Split('_')[1] + "]---> <span style='color:#305389'>" + error.Split('_')[3] + "</span>";
                    break;
                case TypesErrorsImport.MaxLength:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    pos = Convert.ToInt32(error.Split('_')[4]);
                    c = error.Split('_')[3].Split('|')[pos];

                    error1 = error.Split('_')[3];
                    Message = "El valor supero la longitud maxima permitida: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + error.Split('_')[1] + "]---> <span style='color:#305389'>" + error.Split('_')[3] + "</span>";
                    break;
                case TypesErrorsImport.StringEmpty:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    pos = Convert.ToInt32(error.Split('_')[4]);
                    c = error.Split('_')[3].Split('|')[pos];

                    error1 = error.Split('_')[3];

                    Message = "El valor no puede estar vacio: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span>";
                    break;
                case TypesErrorsImport.ErrorConversion:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    c = error.Split('_')[3].Split('|')[13];

                    error1 = error.Split('_')[3];
                    error1 = error1.Replace(c, "<b style='color:red'>" + c + "</b>");

                    Message = "Error de tipo de dato numerico en el Codigo Postal, el valor contiene caracteres no validos: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span>";
                    break;
                case TypesErrorsImport.FatalError:
                    Message = "Ocurrio un problema mientras se leia el archivo.<br />Revise si el archivo tiene el formato correcto o si esta dañado.";
                    break;
                case TypesErrorsImport.CountryNotFound:

                    break;
                default:
                    break;
            }

            this.lblError.Text = Message;//"El contenido del archivo [" + this.fuReceptorsImport.FileName + "] no tiene el formato correcto y no es posible leerlo.";
            this.dvList.Visible =
            this.lblError.Visible = true;
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            if (!this.LoadData())
                return;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            DataTable table = new DataTable();
            DataTable temp;
            DataRow row;
            DropDownList ddlS;
            DropDownList error;
            Label lblS;
            Label lblRFC;
            HiddenField hd;

            if (this.ViewState["tempTable"].IsNull())
                return; //Error...

            temp = this.ViewState["tempTable"] as DataTable;

            if (temp.Rows.Count != this.gvReceptorsTemplate.Rows.Count)
                return; //Error...

            table.Columns.Add("RFC");
            table.Columns.Add("Name");
            table.Columns.Add("Address");
            table.Columns.Add("ExternalNumber");
            table.Columns.Add("InternalNumber");
            table.Columns.Add("Colony");
            table.Columns.Add("Phone");
            table.Columns.Add("Country");
            table.Columns.Add("State");
            table.Columns.Add("Municipality");
            table.Columns.Add("City");
            table.Columns.Add("Reference");
            table.Columns.Add("ZipCode");
            table.Columns.Add("Email");
            table.Columns.Add("RFCExist");
            table.Columns.Add("Status");

            for (int i = 0; i < this.gvReceptorsTemplate.Rows.Count; i += 1)
            {
                ddlS = this.gvReceptorsTemplate.Rows[i].FindControl("ddlStates") as DropDownList;
                lblS = this.gvReceptorsTemplate.Rows[i].FindControl("lblState") as Label;
                lblRFC = this.gvReceptorsTemplate.Rows[i].FindControl("lblRFC") as Label;
                hd = this.gvReceptorsTemplate.Rows[i].FindControl("hdState") as HiddenField;
                error = this.gvReceptorsTemplate.Rows[i].FindControl("ddlState") as DropDownList;

                row = table.NewRow();

                row["Status"] = Convert.ToBoolean(hd.Value) ? error.SelectedIndex + 1 : 0;
                row["RFC"] = lblRFC.Text;
                row["Name"] = temp.Rows[i]["Name"].ToString();
                row["Address"] = temp.Rows[i]["Address"].ToString();
                row["ExternalNumber"] = temp.Rows[i]["ExternalNumber"].ToString();
                row["InternalNumber"] = temp.Rows[i]["InternalNumber"].ToString();
                row["Colony"] = temp.Rows[i]["Colony"].ToString();
                row["Phone"] = temp.Rows[i]["Phone"].ToString();
                row["Country"] = temp.Rows[i]["Country"].ToString();
                row["State"] = ddlS.Visible ? ddlS.SelectedValue : lblS.Text;
                row["Municipality"] = temp.Rows[i]["Municipality"].ToString();
                row["City"] = temp.Rows[i]["City"].ToString();
                row["Reference"] = temp.Rows[i]["Reference"].ToString();
                row["ZipCode"] = temp.Rows[i]["ZipCode"].ToString();
                row["Email"] = temp.Rows[i]["Email"].ToString();

                table.Rows.Add(row);
            }

            table = receptor.Import(this.CurrentBillerID, table);

            if (!table.IsNull())
            {
                this.ViewState["ResultMode"] = true;
                this.gvReceptorsTemplate.DataSource = table;
                this.gvReceptorsTemplate.DataBind();
                this.lblTitleList.Text = "Reporte de Resultados";
            }

            this.gvReceptorsTemplate.HeaderRow.Cells[6].FindControl("spnState1").Visible = false;
            this.gvReceptorsTemplate.HeaderRow.Cells[6].FindControl("spnState2").Visible = true;
            this.btnCancel.Visible =
            this.btnSave.Visible = false;
            this.btnAccept.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.dvList.Visible =
            this.gvReceptorsTemplate.Visible =
            this.btnSave.Visible =
            this.btnCancel.Visible = false;
        }

        public bool ToolTipMode()
        {
            return this.ViewState["ResultMode"].IsNull() ? true : false;
        }

        protected void gvReceptorsTemplate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowIndex > -1)
            {
                HiddenField hdstate = row.FindControl("hdState") as HiddenField;
                Image img = row.FindControl("ImgState") as Image;
                DropDownList ddls = row.FindControl("ddlState") as DropDownList;

                if (!this.ViewState["ResultMode"].IsNull())
                {
                    img.ImageUrl = Convert.ToBoolean(hdstate.Value) ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/deleteIcon.png";
                    img.Visible = true;
                    ddls.Visible = false;
                }
                else
                {
                    Label lblState = row.FindControl("lblState") as Label;

                    if (lblState.Text == "-1")
                    {
                        DropDownList ddlState = row.FindControl("ddlStates") as DropDownList;

                        ddlState.Visible = true;
                        lblState.Visible = false;
                    }

                    HiddenField hd = row.FindControl("hdState") as HiddenField;
                    Label RFC = row.FindControl("lblRFC1") as Label;
                    DropDownList ddl = row.FindControl("ddlState") as DropDownList;

                    if (Convert.ToBoolean(hd.Value))
                    {
                        row.FindControl("spnRFC1").Visible = true;
                        row.FindControl("spnRFC2").Visible = false;
                        RFC.ForeColor = System.Drawing.Color.Crimson;
                        ddl.Visible = true;
                    }
                }
            }
        }
    }
}