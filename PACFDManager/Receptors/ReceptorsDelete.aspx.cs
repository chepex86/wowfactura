﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Receptors
{
    public partial class ReceptorsDelete : PACFDManager.BasePage
    {
        /// <summary>
        /// Get an int value with the id to edit from the session.
        /// </summary>
        private int ReceptorID
        {
            get
            {
                object o;
                int r;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReceptorsEditor1.Enabled =
                this.ReceptorsEditor1.VisibleAcceptButton =
                this.ReceptorsEditor1.VisibleCancelButton = false;

            if (this.IsPostBack)
                return;

            this.ReceptorsEditor1.ReceptorID = this.ReceptorID;
            this.ReceptorsEditor1.DataBindReceptorID();
            this.ReceptorsEditor1.EditorMode = ReceptorsEditorMode.View;

            if (this.ReceptorID < 1)
                this.btnAcept.Enabled = false;
                
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.WebMessageBox1.ShowMessage("¿Esta seguro que quiere borrar el cliente?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
        }
    }
}
