﻿<%@ Page Title="Agregar Cliente" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ReceptorsAdd.aspx.cs" Inherits="PACFDManager.Receptors.ReceptorsAdd" %>

<%@ Register Src="ReceptorsEditor.ascx" TagName="ReceptorsEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:ReceptorsEditor ID="ReceptorsEditor1" runat="server" OnCancelClick="ReceptorsEditor1_CancelClick"
                OnApplyChanges="ReceptorsEditor1_ApplyChanges" TitleMode="Agregar Cliente" />
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
