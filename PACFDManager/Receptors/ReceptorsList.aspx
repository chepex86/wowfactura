﻿<%@ Page Title="Listado de Clientes" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ReceptorsList.aspx.cs" Inherits="PACFDManager.Receptors.ReceptorsList" %>

<%@ Register Src="ReceptorsSearch.ascx" TagName="ReceptorsSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc1:ReceptorsSearch ID="ReceptorsSearch1" runat="server" OnSearch="ReceptorsSearch1_Search" />
            </div>
            <br />
            <br />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de Clientes" />
                    </h2>
                </div>
                <asp:Button CssClass="Button" ID="btnAddReceptor" runat="server" Text="Agregar cliente"
                    OnClick="btnAddReceptor_Click" /><span>
                        <asp:GridView ID="gdvReceptors" runat="server" CssClass="DataGrid" AutoGenerateColumns="False"
                            EmptyDataText="No hay facturas." AllowPaging="true" PageSize="10" PagerSettings-Position="Top"
                            OnPageIndexChanging="gdvReceptors_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="ReceptorID" />
                                <asp:BoundField HeaderText="Nombre" DataField="Name" ItemStyle-HorizontalAlign="Left">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="RFC">
                                    <HeaderTemplate>
                                        <span class="vtip" style="cursor: help" title='<b>RFC</b>. Registro Federal de Contribuyentes'>
                                            <asp:Label ID="lblTitleRFC" Text="RFC" runat="server" /></span>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRFC" Text='<%#Eval("RFC")%>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Dirección" DataField="Address" ItemStyle-HorizontalAlign="Left">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="País de origen del receptor." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("Country")%>
                                        <br />
                                        <%#Eval("State")%>,<%#Eval("Location")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detalles">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="ttpHeaderDetailsReceptor" runat="server" ToolTip="Haga click en la imagen de <img src='../Includes/Images/png/kfind.png' /> para ver<br />los detalles del cliente." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='<%# string.Format("Ver detalles del cliente <br/><b>{0}</b>", Eval("Name"))%>'>
                                            <asp:ImageButton ID="imbDetails" CommandArgument='<%#Eval("ReceptorID")%>' runat="server"
                                                OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/kfind.png" AlternateText="Detalles" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Editar">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />modificar el cliente seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Modificar el cliente<br /><b><%#Eval("Name")%></b>'>
                                            <asp:ImageButton ID="imbEdit" CommandArgument='<%#Eval("ReceptorID")%>' runat="server"
                                                OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editIcon.png" AlternateText="Editar" /></span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Borrar">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar el cliente seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Eliminar el cliente<br /><b><%#Eval("Name")%></b>'>
                                            <asp:ImageButton ID="imbDelete" CommandArgument='<%#Eval("ReceptorID")%>' runat="server"
                                                OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png" AlternateText="Eliminar" /></span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="dgHeader" />
                            <RowStyle CssClass="dgItem" />
                            <AlternatingRowStyle CssClass="dgAlternate" />
                        </asp:GridView>
                    </span>
                <br />
                <div style="text-align: right">
                    <span>
                        <asp:Label ID="lblTitleNumberReceptors" runat="server" Text="Número de Registros:"
                            Font-Bold="True" /></span> <span>
                                <asp:Label ID="lblNumberReceptors" runat="server" Text="" />
                            </span>
                </div>
                <asp:Button CssClass="Button" ID="btnAddreceptor2" runat="server" Text="Agregar cliente"
                    OnClick="btnAddReceptor_Click" />
            </div>
            <div>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
