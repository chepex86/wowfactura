﻿<%@ Page Title="Elimar cliente." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ReceptorsDelete.aspx.cs" Inherits="PACFDManager.Receptors.ReceptorsDelete" %>

<%@ Register Src="ReceptorsEditor.ascx" TagName="ReceptorsEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblTitle" runat="server" Text="Eliminar cliente" 
                            Font-Size="24px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
                        <div style="border-style: solid; border-width: 1px; border-color: #286fc0; background-color: #f2f5fb;">
                            <uc1:ReceptorsEditor ID="ReceptorsEditor1" runat="server" EditorMode="View" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button CssClass="Button" ID="btnAcept" runat="server" Text="Eliminar" OnClick="btnAcept_Click" />
                                </td>
                                <td>
                                    <asp:Button CssClass="Button" ID="btncancel" runat="server" Text="Cancelar" OnClick="btncancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
