﻿<%@ Page Title="Modificar cliente." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ReceptorsModify.aspx.cs" Inherits="PACFDManager.Receptors.ReceptorsModify" %>

<%@ Register Src="ReceptorsEditor.ascx" TagName="ReceptorsEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            <uc1:ReceptorsEditor ID="ReceptorsEditor1" runat="server" OnCancelClick="ReceptorsEditor1_CancelClick"
                OnApplyChanges="ReceptorsEditor1_ApplyChanges" TitleMode="Editar Cliente" />
                
                
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
