﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Receptors
{
    public enum ReceptorsSearchMode
    {
        None = 0,
        RFC = 1,
        Name = 2,
        Both
    }
}
