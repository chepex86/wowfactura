﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Receptors
{
    public enum ReceptorsEditorMode
    {
        Add = 0,
        Edit = 1,
        View = 2,
    }
}
