﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
#endregion

namespace PACFDManager.Receptors
{
    public partial class ReceptorsModify : PACFDManager.BasePage
    {
        private const string MESSAGE_ERROR_REDIRECT = "0";
        private const string MESSAGE_MODIFY_CLIENT = "1";

        /// <summary>
        /// Get a boolean value with modify behavior of the web page.
        /// </summary>
        private bool ModifyMode
        {
            get
            {
                object o;
                bool r;

                o = this.Request.QueryString["e"];

                if (o.IsNull())
                    return true;

                if (!Boolean.TryParse(o.ToString(), out r))
                    return true;

                return r;
            }
        }
        /// <summary>
        /// Get an int value with the id to edit from the session.
        /// </summary>
        private int ReceptorID
        {
            get
            {
                object o;
                int r;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }

        private PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable TemporalDataTable
        {
            get
            {
                PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable))
                    return null;

                return (PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.ReceptorsEditor1.Enabled = this.ModifyMode;
            this.ReceptorsEditor1.ReceptorID = this.ReceptorID;
            this.ReceptorsEditor1.BillerID = this.CurrentBillerID;
            this.ReceptorsEditor1.DataBindReceptorID();
            this.ReceptorsEditor1.EditorMode =
                this.ModifyMode ? ReceptorsEditorMode.Edit : ReceptorsEditorMode.View;

            this.ReceptorsEditor1.EnabledCancelButton = true;
        }

        protected void ReceptorsEditor1_ApplyChanges(object sender, ReceptorsEditorEventArgs e)
        {
            if (e.EditorMode != ReceptorsEditorMode.Edit)
                return;

            this.TemporalDataTable = e.Table;
            this.WebMessageBox1.CommandArguments = MESSAGE_MODIFY_CLIENT;
            this.WebMessageBox1.ShowMessage("¿Desea modificar al cliente?", false, WebMessageBoxButtonType.YesNo);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_MODIFY_CLIENT:
                    if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
                    {
                        if (!(new PACFD.Rules.Receptors()).Update(this.TemporalDataTable))
                        {
                            //error ...     
                            LogManager.WriteError(new Exception("Error al modificar cliente."));
                            this.WebMessageBox1.CommandArguments = MESSAGE_ERROR_REDIRECT;
                            this.WebMessageBox1.ShowMessage("Error al modificar cliente.", true);
                            this.TemporalDataTable = null;
                            break;
                        }

                        #region Add new entry to log system
                        PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico información del cliente {1}.",
                            this.UserName, this.TemporalDataTable[0].Name), this.CurrentBillerID, null, null);
                        #endregion

                        this.TemporalDataTable = null;
                        this.WebMessageBox1.CommandArguments = MESSAGE_ERROR_REDIRECT;
                        LogManager.WriteStackTrace(new Exception("Exito al modificar cliente."));
                        this.WebMessageBox1.ShowMessage("Exito al modificar cliente", false, WebMessageBoxButtonType.Accept);
                    }
                    else
                    {
                        this.WebMessageBox1.CommandArguments = string.Empty;
                        this.TemporalDataTable = null;
                    }
                    break;
                case MESSAGE_ERROR_REDIRECT:
                default:
                    this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
                    break;
            }
        }

        protected void ReceptorsEditor1_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
        }
    }
}