﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Billings
{
    public partial class BillingsDelete : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.BillingsEditor1.VisibleAcceptButton =
            //    this.BillingsEditor1.VisibleCancelButton =
                this.BillingsEditor1.Enabled = false;

            if (this.IsPostBack)
                return;
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            LogManager.WriteStackTrace(new Exception("Exito al borrar factura."));
            this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.WebMessageBox1.ShowMessage("¿Desea borrar el comprobante?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
        }
    }
}
