﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    /// <summary>
    /// 
    /// </summary>
    public partial class BillingsArticleSearch : System.Web.UI.UserControl
    {
        public event SearchBillingArticleEventHandler Search;
        public event ArticlesSelectedEventHandler ArticlesSelected;
        public event EventHandler CancelClick;



        /// <summary>
        /// Get or Set a string value with the text to search.
        /// </summary>
        public string SearchText
        {
            get { return this.txtArticle.Text; }
            set { this.txtArticle.Text = value; }
        }
        /// <summary>
        /// Get or Set a BillingsArticleSearchSelectMode with the selection mode 
        /// of the control. If selection mode is BillingsArticleSearchSelectMode.Individual,
        /// only the firts selected item is add to the selected list of items, 
        /// excluding the rest.
        /// </summary>
        public BillingsArticleSearchSelectMode SelectionMode
        {
            get
            {
                object o;
                BillingsArticleSearchSelectMode e;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return BillingsArticleSearchSelectMode.Individual;


                try
                {
                    e = (BillingsArticleSearchSelectMode)Enum.Parse(typeof(BillingsArticleSearchSelectMode), o.ToString());
                }
                catch
                {
                    return BillingsArticleSearchSelectMode.Individual;
                }

                return e;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        [System.ComponentModel.Browsable(false)]
        public int BillerID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void OnSearch(BillingsArticleSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        protected virtual void OnArticlesSelected(BillingsArticleSearchSelectedEventArgs e)
        {
            if (!this.ArticlesSelected.IsNull())
                this.ArticlesSelected(this, e);
        }
        protected virtual void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        public virtual void PerformSearch()
        {
            this.btnSearch_Click(null, EventArgs.Empty);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BillingsArticleSearchEventArgs a;
            PACFD.Rules.Concepts concept;
            PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table;

            concept = new PACFD.Rules.Concepts();
            table = concept.SelectByBillerIDAndDescription(this.BillerID/*Security.Security.CurrentBillerID*/, this.txtArticle.Text);
            a = new BillingsArticleSearchEventArgs(this.txtArticle.Text, table, this.SelectionMode);
            this.gdvArticles.DataSource = table;
            this.gdvArticles.DataBind();

            this.OnSearch(a);
        }
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            BillingsArticleSearchSelectedEventArgs a;

            a = new BillingsArticleSearchSelectedEventArgs(
                this.OnArticlesGenerate(),
                this.SelectionMode);

            this.OnArticlesSelected(a);
        }
        protected BillingsArticleSearchSelectedCollection OnArticlesGenerate()
        {
            HiddenField hidden;
            CheckBox check;
            TextBox text;
            //int selected = this.gdvArticles.Columns.Count - 1;
            BillingsArticleSearchSelectedCollection list;

            list = new BillingsArticleSearchSelectedCollection();

            foreach (GridViewRow row in this.gdvArticles.Rows)
            {
                hidden = row./*Cells[selected].*/FindControl("hflID") as HiddenField;
                text = row./*Cells[selected - 1].*/FindControl("txtAmount") as TextBox;
                check = row./*Cells[selected].*/FindControl("ckbSelect") as CheckBox;

                if (check.IsNull() || text.IsNull())
                    continue;

                if (!check.Checked)
                    continue;

                //list.Add(new BillingsArticleSearchSelected(
                //    hidden.Value.ToInt32(),
                //    row.Cells[0].Text,
                //    row.Cells[2].Text.Remove(0, 1).ToDecimal(),
                //    text.Text.ToInt32() == 0 ? 1 : text.Text.ToInt32()
                //    ));

                if (this.SelectionMode == BillingsArticleSearchSelectMode.Individual)
                    break;
            }

            return list;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
    }
}