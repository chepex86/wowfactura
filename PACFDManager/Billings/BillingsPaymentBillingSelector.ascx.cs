﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class BillingsPaymentBillingSelector : PACFDManager.BaseUserControl
    {
        public event BillingsPaymentBillingSelectedEventHandler Selected;

        private int TemporalBillingID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        public int ReceptorID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        private PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable SearchBillingTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable))
                    return null;

                return (PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Billings billing;

            if (this.IsPostBack)
                return;
        }

        public void Search()
        {
            PACFD.Rules.Billings billing;
            billing = new PACFD.Rules.Billings();
            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();

            int? branchID = null;

            if (CurrentBranchID != -1)
                branchID = CurrentBranchID;

            var r = receptor.SelectByID(this.ReceptorID);
            if (r.Count > 0)
            {
                this.SearchBillingTable = billing.Search(
                PACFD.Rules.Billings.SearchPetitionFromFlag.WebForm
                , this.CurrentBillerID
                , r[0].Name
                , null
                , null
                , null
                , false
                , false
                , null
                , branchID
                , null
                , null
                , "PPD"
                );

                this.gdvBillings.DataSource = this.SearchBillingTable;
                this.gdvBillings.DataBind();
            }
        }
        protected virtual void OnSelected(BillingsPaymentBillingSelectorEventArgs e)
        {
            if (!this.Selected.IsNull())
                this.Selected(this, e);
        }

        protected void ImageButton_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            int r;

            if (b.IsNull())
                return;

            List<int> ids = new List<int>();
            foreach (GridViewRow row in this.gdvBillings.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string id = ((Label)row.Cells[0].FindControl("lblBillingID")).Text;
                        ids.Add(int.Parse(id));
                    }
                }
            }

            if (ids.Count < 1)
            {
                return;
            }
            BillingsPaymentBillingSelectorEventArgs a = new BillingsPaymentBillingSelectorEventArgs(ids);
            this.OnSelected(a);
        }


        protected void gdvBillings_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvBillings.PageIndex = e.NewPageIndex;
            this.gdvBillings.DataSource = this.SearchBillingTable;
            this.gdvBillings.DataBind();
        }
    }
}