﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Collection class for the BillingsArticleSearchSelected object.
    /// </summary>
    public class BillingsArticleSearchSelectedCollection : List<BillingsArticleSearchSelected>
    {
    }
}
