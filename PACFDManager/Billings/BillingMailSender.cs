﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    ///// <summary>
    ///// Class used to send an email from the billing layer.
    ///// </summary>
    //public class BillingMailSender : PACFD.Rules.Mail.MailSender
    //{
    //    /// <summary>
    //    /// Mail send date.
    //    /// </summary>
    //    public const string LBL_MAIL_DATE = "lblMailDate";
    //    /// <summary>
    //    /// Folio of the billing.
    //    /// </summary>
    //    public const string LBL_FOLIO = "lblFolio";
    //    /// <summary>
    //    /// Billing creation date.
    //    /// </summary>
    //    public const string LBL_DATE = "lblDate";
    //    /// <summary>
    //    /// Payment method.
    //    /// </summary>
    //    public const string LBL_PAYMENT_METHOD = "lblPaymentMethod";
    //    /// <summary>
    //    /// If is a prebilling.
    //    /// </summary>
    //    public const string LBL_PREBILLING = "lblPreBilling";
    //    /// <summary>
    //    /// Biller (corporation) name.
    //    /// </summary>
    //    public const string LBL_BILLER_NAME = "lblBillerName";
    //    /// <summary>
    //    /// Biller (corporation) address.
    //    /// </summary>
    //    public const string LBL_BILLER_ADDRESS = "lblBillerAddress";
    //    /// <summary>
    //    /// Receptor (client) name.
    //    /// </summary>
    //    public const string LBL_RECEPTOR_ADDRESS = "lblReceptorAddress";
    //    /// <summary>
    //    /// Total billing tax trasfered.
    //    /// </summary>
    //    public const string LBL_TOTAL_TRANSFERED = "lblTotalTransfer";
    //    /// <summary>
    //    /// Total billing tax detained.
    //    /// </summary>
    //    public const string LBL_TOTAL_DETAINED = "lblTotalDetained";
    //    /// <summary>
    //    /// Sub total billing value.
    //    /// </summary>
    //    public const string LBL_SUBTOTAL = "lblSubTotal";
    //    /// <summary>
    //    /// Total billing value.
    //    /// </summary>
    //    public const string LBL_TOTAL = "lblTotal";
    //    /// <summary>
    //    /// The PDF download url.
    //    /// </summary>
    //    public const string LBL_DOWNLOAD_ADDRESS = "lblDownloadAddress";
    //    /// <summary>
    //    /// The XML download url.
    //    /// </summary>
    //    public const string LBL_DOWNLOAD_ADDRESS_2 = "lblDownloadAddress2";



    //    /// <summary>
    //    /// Default parameters in the param dictionary.
    //    /// </summary>
    //    public virtual string DefaultParams
    //    {
    //        get
    //        {
    //            return "lblMailDate,lblFolio,lblDate,lblPaymentMethod,lblPreBilling,lblBillerName,lblBillerAddress," +
    //                "lblReceptorAddress,lblTotalTransfer,lblTotalDetained,lblSubTotal,lblTotal, lblDownloadAddress";
    //        }
    //    }



    //    /// <summary>
    //    /// Create a new instance of the class.
    //    /// </summary>
    //    /// <param name="dir">XML file with the configuration to load.</param>
    //    /// <param name="id">Id of the mail configuration to load.</param>
    //    public BillingMailSender(string dir, string id)
    //    {
    //        this.Message = PACFD.Rules.Mail.MailSenderHelper.GetMessageFromXML(dir, id);
    //        string[] sp;

    //        sp = this.DefaultParams.Split(',');

    //        for (int i = 0; i < sp.Length; i++)
    //            this.Parameters.Add(sp[i].Trim(), string.Empty);
    //    }
    //    /// <summary>
    //    /// Set the data of the body message
    //    /// </summary>
    //    /// <param name="mail">Mail to be configured.</param>
    //    /// <param name="e">DataSet used to fill the mail.</param>
    //    /// <param name="billerid">Biller ID used to identify the receptor and billing.</param>
    //    public static void SetBillingMail(BillingMailSender mail, ref PACFD.DataAccess.BillingsDataSet e, int billerid)
    //    {
    //        PACFD.Rules.SmtpConfiguration smt;
    //        PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable table;

    //        mail[BillingMailSender.LBL_BILLER_ADDRESS] = e.BillingsBillers[0].IssuedInDifferentPlace ?
    //           e.BillingsIssued[0].Address : e.BillingsBillers[0].BillerAddress;
    //        mail[BillingMailSender.LBL_BILLER_NAME] = e.BillingsBillers[0].BillerName;
    //        mail[BillingMailSender.LBL_DATE] = e.Billings[0].BillingDate.ToString("yyyy/MM/dd");
    //        mail[BillingMailSender.LBL_FOLIO] = e.Billings[0].Serial.ToString() + "-" + e.Billings[0].Folio.ToString();
    //        mail[BillingMailSender.LBL_MAIL_DATE] = DateTime.Now.ToString("yyyy/MM/dd");
    //        mail[BillingMailSender.LBL_PAYMENT_METHOD] = e.Billings[0].PaymentMethod;
    //        mail[BillingMailSender.LBL_PREBILLING] = e.Billings[0].PreBilling ? "No sellado" : "Sellado";
    //        mail[BillingMailSender.LBL_RECEPTOR_ADDRESS] = e.BillingsReceptors[0].ReceptorAddress;

    //        mail[BillingMailSender.LBL_SUBTOTAL] =
    //            string.IsNullOrEmpty(e.Billings[0].SubTotal.ToString()) ? "$0.00" : string.Format("{0:c}", e.Billings[0].SubTotal);
    //        mail[BillingMailSender.LBL_TOTAL] =
    //            //e.Billings[0].Total.ToString();
    //            string.IsNullOrEmpty(e.Billings[0].Total.ToString()) ? "$0.00" : string.Format("{0:c}", e.Billings[0].SubTotal);
    //        mail[BillingMailSender.LBL_TOTAL_DETAINED] =
    //            //e.Taxes[0].TotalDetained.ToString();
    //            string.IsNullOrEmpty(e.Taxes[0].TotalDetained.ToString()) ? "$0.00" : string.Format("{0:c}", e.Taxes[0].TotalTransfer);
    //        mail[BillingMailSender.LBL_TOTAL_TRANSFERED] =
    //            //e.Taxes[0].TotalTransfer.ToString();
    //            string.IsNullOrEmpty(e.Taxes[0].TotalTransfer.ToString()) ? "$0.00" : string.Format("{0:c}", e.Taxes[0].TotalTransfer);

    //        mail[BillingMailSender.LBL_DOWNLOAD_ADDRESS] =
    //            string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
    //            + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=pdf&a={0}&b={1}&o=true",
    //            PACFD.Common.Cryptography.EncryptUnivisitString(billerid.ToString()),
    //            PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
    //            );
    //        mail[BillingMailSender.LBL_DOWNLOAD_ADDRESS_2] =
    //            string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
    //            + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=xml&a={0}&b={1}&o=true",
    //            PACFD.Common.Cryptography.EncryptUnivisitString(billerid.ToString()),
    //            PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
    //            );

    //        if (mail.Message.From.DisplayName == string.Empty)
    //        {
    //            smt = new PACFD.Rules.SmtpConfiguration();
    //            table = smt.Select();

    //            if (table.Count > 0)
    //                mail.Message.From = new System.Net.Mail.MailAddress(mail.Message.From.Address, table[0].Name);

    //            smt = null;
    //            table.Dispose();
    //            table = null;
    //        }
    //    }
    //}
}
