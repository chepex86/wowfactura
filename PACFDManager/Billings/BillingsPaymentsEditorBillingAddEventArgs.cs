﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsPaymentsEditorBillingAddEventArgs
    {
        public BillingsEditorMode EditorMode { get; private set; }
        public System.Data.DataRow DataRow { get; private set; }

        public BillingsPaymentsEditorBillingAddEventArgs(System.Data.DataRow row, BillingsEditorMode mode)
        {
            this.DataRow = row;
            this.EditorMode = mode;
        }
    }
}