﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsEditorArticleAddEventArgs : EventArgs
    {
        public BillingsEditorMode EditorMode { get; private set; }
        public System.Data.DataRow DataRow { get; private set; }

        public BillingsEditorArticleAddEventArgs(System.Data.DataRow row, BillingsEditorMode mode)
        {
            this.DataRow = row;
            this.EditorMode = mode;
        }
    }
}
