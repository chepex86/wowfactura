﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PACFD.Common; 
#endregion

namespace PACFDManager.Billings
{
    partial class BillingsEditor
    {
        /// <summary>
        /// Class used to group the main properties and methods of PACFDManager.Billings.BillingsEditor control.
        /// </summary>
        public class BillingsEditorInfo
        {
            /// <summary>
            /// Get or Set the parent control of PACFDManager.Billings.BillingsEditor.BillingsEditorInfo
            /// </summary>
            protected BillingsEditor Parent { get; private set; }
            /// <summary>
            /// Get or Set a string value with the conditions.
            /// </summary>
            public string Conditions
            {
                get { return this.Parent.txtReceptorConditions.Text; }
                set { this.Parent.txtReceptorConditions.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public ListItem PaymentMethod { get { return this.Parent.ddlPaymentMethod.SelectedItem; } }
            public int PaymentMethodIndex
            {
                get { return this.Parent.ddlPaymentMethod.SelectedIndex; }
                set
                {
                    if (value > this.Parent.ddlPaymentMethod.Items.Count - 1)
                        value = 0;

                    this.Parent.ddlPaymentMethod.SelectedIndex = value;
                }
            }
            public ListItemCollection PaymentMethodItems { get { return this.Parent.ddlPaymentMethod.Items; } }
            public ListItem IsCredit { get { return this.Parent.ddlIsCredit.SelectedItem; } }
            public int IsCreditIndex
            {
                get { return this.Parent.ddlIsCredit.SelectedIndex; }
                set
                {
                    if (value > this.Parent.ddlIsCredit.Items.Count - 1)
                        value = 0;

                    this.Parent.ddlIsCredit.SelectedIndex = value;
                }
            }
            public List<string> billings { get; private set; }
            public ListItemCollection IsCreditItems { get { return this.Parent.ddlIsCredit.Items; } }
            /// <summary>
            /// Get or set a decimal value with the discount in the concept search section.
            /// </summary>
            public decimal Discount
            {
                get
                {
                    decimal r;

                    if (!decimal.TryParse(this.Parent.txtAddConceptDiscount.Text, out r))
                        r = 0;

                    return r;
                }
                set
                {
                    if (value < 0)
                        value = 0;

                    this.Parent.txtAddConceptDiscount.Text = value.ToString();
                }
            }
            ///// <summary>
            ///// Get a decimal value with the total discount in the concepts (articles).
            ///// </summary>
            //public decimal DiscountTotal
            //{
            //    get
            //    {
            //        decimal r = 0;

            //        foreach (System.Data.DataRow row in this.ArticlesGrid.Rows)
            //            r += row.ItemArray[4].ToString().ToDecimal();
            //        //r += row.ItemArray[3].ToString().ToDecimal();

            //        this.Parent.lblDescountTotalText.Text = "%" + r.ToString();

            //        return r;
            //    }
            //}
            /// <summary>
            /// 
            /// </summary>
            public int PaymentTypeIndex
            {
                get { return this.Parent.ddlPaymentMethod.SelectedIndex; }
                set { this.Parent.ddlPaymentMethod.SelectedIndex = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public ListItemCollection PaymentTypeItems { get { return this.Parent.ddlPaymentMethod.Items; } }
            /// <summary>
            /// Get or set a string with the total in letters.
            /// </summary>
            public string TotalInLetters
            {
                get { return this.Parent.txtTotalByHand.Text; }
                set { this.Parent.txtTotalByHand.Text = value; }
            }
            /// <summary>
            /// Get a ArticlesGridInf class used to get data to the articles data grid view.
            /// </summary>
            public ArticlesGridInf ArticlesGrid { get; private set; }
            /// <summary>
            /// Control the tax grid data bind and the totals tax grid.
            /// </summary>
            public Billings.BillingsEditor.BillerTaxesGrid TaxesGrid { get; private set; }
            ///// <summary>
            ///// Controls the totals grid.
            ///// </summary>
            //public Billings.BillingsEditor.BillingTaxesTotals TotalsTaxes { get { return this.TaxesGrid.Totals; } }

            ///// <summary>
            ///// 
            ///// </summary>
            //public decimal Total
            //{
            //    get
            //    {
            //        decimal t;

            //        if (!decimal.TryParse(this.Parent.lblTotalText.Text.Remove(0, 1), out t))
            //            return 0.0m;

            //        return t;
            //    }
            //    set { this.Parent.lblTotalText.Text = value.ToString("c"); }
            //}
            ///// <summary>
            ///// Get or set a decimal value with the subtotal.
            ///// </summary>
            //public decimal SubTotal
            //{
            //    get
            //    {
            //        decimal s;

            //        if (!decimal.TryParse(this.Parent.lblSubTotalText.Text.Remove(0, 1), out s))
            //            return 0.0m;

            //        return s;
            //    }
            //    set { this.Parent.lblSubTotalText.Text = value.ToString("C"); }
            //}
            /// <summary>
            /// Get a string value with the seal.
            /// </summary>
            public string Seal { get { return this.Parent.BillingSeal; } }
            /// <summary>
            /// Get a string value with the original string.
            /// </summary>
            public string OriginalString { get { return this.Parent.BillingOriginalString; } }
            /// <summary>
            /// Get a boolean value with the visible state
            /// of the receptor drop down list.
            /// </summary>
            public bool IsReceptorDropDownVisible { get { return this.Parent.ddlReceptorSearch.Visible; } }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Parent of the class.</param>
            internal BillingsEditorInfo(BillingsEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
                this.ArticlesGrid = new ArticlesGridInf(owner);
                this.TaxesGrid = new BillerTaxesGrid(owner);
                this.billings = new List<string>();
            }

            /// <summary>
            /// Recalculate the totals and display it converted in letters.
            /// </summary>
            public decimal CalculateTotal()
            {
                PACFD.Rules.Billings billings = new PACFD.Rules.Billings();
                PACFD.DataAccess.BillingsDataSet b = null;

                try
                {
                    b = this.Parent.OnGenerateBillingsEditorEventArgs(true).DataSet;
                }
                catch (Exception ex)
                { 
                    LogManager.WriteWarning(ex);

                    //totalsrowlist = this.TotalsTaxes.Rows;
                    //this.Total = 0;
                    //this.SubTotal = 0;

                    //foreach (TaxTotalsRow r in totalsrowlist)
                    //    r.Amount = 0;

                    b = new PACFD.DataAccess.BillingsDataSet();
                    this.UpdateTotalsTable(ref b);

                    this.TotalInLetters = "0".ToDecimal().ToLetters(this.Parent.CurrencyBillerType);// this.Total.ToLetters();

                    return 0;
                }

                if (b.IsNull())
                    return 0;

                billings.CalculationTaxesAndTotals(ref b);
                billings = null;

                //this.SubTotal =
                //    this.Total = 0;
                this.TotalInLetters = b.Billings[0].Total.ToLetters(this.Parent.CurrencyBillerType);
                //this.Total.ToLetters();

                //if (b.Billings.Count < 1)
                //    return 0;

                //this.SubTotal = b.Billings[0].SubTotal;
                //totalsrowlist = this.TaxesGrid.Totals.Rows;

                //foreach (PACFD.DataAccess.BillingsDataSet.TransferTaxesRow t in b.TransferTaxes)
                //{
                //    foreach (Billings.BillingsEditor.TaxTotalsRow l in totalsrowlist)
                //    {
                //        if (l.Name != t.Name)
                //            continue;

                //        l.Amount = t.Import;
                //        break;
                //    }
                //}

                //foreach (PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow t in b.DetainedTaxes)
                //{
                //    foreach (Billings.BillingsEditor.TaxTotalsRow l in totalsrowlist)
                //    {
                //        if (l.Name != t.Name)
                //            continue;

                //        l.Amount = t.Import;
                //        break;
                //    }
                //}

                //this.Total = b.Billings[0].Total;
                //this.TotalInLetters = "0".ToDecimal().ToLetters(); // this.Total.ToLetters();

                this.UpdateTotalsTable(ref b);

                return b.Billings[0].Total;// this.Total;
            }
            /// <summary>
            /// If the drop down list is visible set a receptor in the drop down list.
            /// </summary>
            /// <param name="receptorid">ID of the receptor to select.</param>
            public void SetReceptorSelected(int receptorid)
            {
                int i = -1;

                if (!this.Parent.ddlReceptorSearch.Visible)
                    return;

                foreach (ListItem item in this.Parent.ddlReceptorSearch.Items)
                {
                    i++;

                    if (item.Value != receptorid.ToString())
                        continue;

                    this.Parent.ddlReceptorSearch.SelectedIndex = i;
                    break;
                }

                this.Parent.btnAceptReceptorSearch_Click(this.Parent.btnAceptReceptorSearch, EventArgs.Empty);
            }
            /// <summary>
            /// If the drop down list is not visible set and search 
            /// a receptor in the text field.
            /// </summary>
            /// <param name="receptorname"></param>
            public void SetReceptorSelected(string receptorname)
            {
                if (this.Parent.ddlReceptorSearch.Visible)
                    return;

                this.Parent.txtReceptorSearch.Text = receptorname;
                this.Parent.btnAceptReceptorSearch_Click(this.Parent.btnAceptReceptorSearch, EventArgs.Empty);
            }
            /// <summary>
            /// Set a consept in the search field.
            /// </summary>
            /// <param name="name">Name of the concept.</param>
            public void SetConceptSelected(string name)
            {
                this.Parent.hflArticleSearch_ValueChanged(this.Parent.hflArticleSearch, EventArgs.Empty);
            }

            public void UpdateTotalsTable(ref PACFD.DataAccess.BillingsDataSet dataset)
            {
                System.Text.StringBuilder sbuilder;

                sbuilder = new System.Text.StringBuilder();
                sbuilder.Append("<table border='0' cellpadding='2' cellspacing='2'>");

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Subtotal:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td  style='text-align: right;'>");
                //sbuilder.Append("$");
                sbuilder.Append(string.Format("{0:c}", dataset.Billings.Count < 1 ? 0 : Math.Round(dataset.Billings[0].SubTotal, 2)));
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Descuento:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td  style='text-align: right;'>");
                //sbuilder.Append("$");
                sbuilder.Append(string.Format("{0:c}", dataset.Billings.Count < 1 ? 0 : Math.Round(dataset.Billings[0].Discount, 2)));
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                foreach (PACFD.DataAccess.BillingsDataSet.TransferTaxesRow item in dataset.TransferTaxes)
                {
                    sbuilder.Append("<tr>");
                    sbuilder.Append("<td>");
                    sbuilder.Append(item.Name);
                    sbuilder.Append(":");
                    sbuilder.Append("</td>");
                    sbuilder.Append("<td style='text-align: right;'>");
                    //sbuilder.Append("$");
                    sbuilder.Append(string.Format("{0:c}", Math.Round(item.Import, 2)));
                    sbuilder.Append("</td>");
                    sbuilder.Append("</tr>");
                }

                foreach (PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow item in dataset.DetainedTaxes)
                {
                    sbuilder.Append("<tr>");
                    sbuilder.Append("<td>");
                    sbuilder.Append("Ret. ");
                    sbuilder.Append(item.Name);
                    sbuilder.Append(":");
                    sbuilder.Append("</td>");
                    sbuilder.Append("<td style='text-align: right;'>");
                    //sbuilder.Append("$");
                    sbuilder.Append(string.Format("{0:c}", Math.Round(item.Import, 2)));
                    sbuilder.Append("</td>");
                    sbuilder.Append("</tr>");
                }

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Total:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td style='text-align: right;'>");
                //sbuilder.Append("$");
                sbuilder.Append(string.Format("{0:c}", dataset.Billings.Count < 1 ? 0 : Math.Round(dataset.Billings[0].Total, 2)));
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                sbuilder.Append("</table>");
                this.Parent.ltrTotalsDetailView.Text = sbuilder.ToString();
            }

            public void UpdateTotalsTable(ref PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable table)
            {
                System.Text.StringBuilder sbuilder;

                sbuilder = new System.Text.StringBuilder();
                sbuilder.Append("<table border='0' cellpadding='2' cellspacing='2'>");

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Subtotal:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td style='text-align: right;'>");
                sbuilder.Append("$0.00");
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Descuento:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td  style='text-align: right;'>");
                sbuilder.Append("$0.00");
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                foreach (PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutRow item in table)
                {
                    sbuilder.Append("<tr>");
                    sbuilder.Append("<td>");

                    if (item.Transfer)
                        sbuilder.Append("Ret. ");

                    sbuilder.Append(item.Name);
                    sbuilder.Append(":");
                    sbuilder.Append("</td>");
                    sbuilder.Append("<td style='text-align: right;'>");
                    sbuilder.Append("$0.00");
                    sbuilder.Append("</td>");
                    sbuilder.Append("</tr>");
                }

                sbuilder.Append("<tr>");
                sbuilder.Append("<td>");
                sbuilder.Append("Total:");
                sbuilder.Append("</td>");
                sbuilder.Append("<td style='text-align: right;'>");
                sbuilder.Append("$0.00");
                sbuilder.Append("</td>");
                sbuilder.Append("</tr>");

                sbuilder.Append("</table>");
                this.Parent.ltrTotalsDetailView.Text = sbuilder.ToString();
            }
        }
    }
}