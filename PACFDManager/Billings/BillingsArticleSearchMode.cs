﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Represent the type of selection in the BillingsArticleSearch control.
    /// </summary>
    public enum BillingsArticleSearchSelectMode
    {
        Individual = 0,
        Multiple = 1
    }
}
