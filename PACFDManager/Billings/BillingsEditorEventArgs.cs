﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsEditorEventArgs
    {
        public PACFD.DataAccess.BillingsDataSet DataSet { get; private set; }
        public Billings.BillingsEditorMode EditorMode { get; private set; }

        public BillingsEditorEventArgs(PACFD.DataAccess.BillingsDataSet dataset, BillingsEditorMode mode)
        {
            this.DataSet = dataset;
            this.EditorMode = mode;
        }
    }
}
