﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// 
    /// </summary>
    public class BillingsEditorTaxCheckBoxEventArgs : EventArgs
    {
        public bool Checked { get; private set; }

        public BillingsEditorTaxCheckBoxEventArgs(bool ischecked)
        {
            this.Checked = ischecked;
        }
    }
}
