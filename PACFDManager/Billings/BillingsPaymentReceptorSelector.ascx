﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsPaymentReceptorSelector.ascx.cs"
    Inherits="PACFDManager.Billings.BillingsPaymentReceptorSelector" %>
<%@ Register Src="../Receptors/ReceptorsSearch.ascx" TagName="ReceptorsSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
    <div>
        <uc1:ReceptorsSearch ID="ReceptorsSearch1" runat="server" OnSearch="ReceptorsSearch1_Search" />
    </div>
    <br />
    <br />
    <div class="txt_left">
        <div class="info">
            <h2>
                <asp:Label ID="lblTitle" runat="server" Text="Listado de Clientes" />
            </h2>
        </div>
        <span>
                <asp:GridView ID="gdvReceptors" runat="server" CssClass="DataGrid" AutoGenerateColumns="False"
                    EmptyDataText="No hay facturas." AllowPaging="true" PageSize="10" PagerSettings-Position="Top"
                    OnPageIndexChanging="gdvReceptors_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="ReceptorID" />
                        <asp:BoundField HeaderText="Nombre" DataField="Name" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="RFC">
                            <HeaderTemplate>
                                <span class="vtip" style="cursor: help" title='<b>RFC</b>. Registro Federal de Contribuyentes'>
                                    <asp:Label ID="lblTitleRFC" Text="RFC" runat="server" /></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRFC" Text='<%#Eval("RFC")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Dirección" DataField="Address" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="País de origen del receptor." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Country")%>
                                <br />
                                <%#Eval("State")%>,<%#Eval("Location")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Detalles">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="ttpHeaderDetailsReceptor" runat="server" ToolTip="Clic en botón para seleccionar receptor" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='<%# string.Format("Seleccionar el cliente <br/><b>{0}</b>", Eval("Name"))%>'>
                                    <asp:Button ID="imbDetails" CommandArgument='<%#Eval("ReceptorID")%>' runat="server" Text="Seleccionar"
                                        OnClick="ImageButton_Click"  AlternateText="Detalles" />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                </asp:GridView>
            </span>
        <br />
        <div style="text-align: right">
            <span>
                <asp:Label ID="lblTitleNumberReceptors" runat="server" Text="Número de Registros:"
                    Font-Bold="True" /></span> <span>
                        <asp:Label ID="lblNumberReceptors" runat="server" Text="" />
                    </span>
        </div>
    </div>
  <%--          <div>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            </div>--%>