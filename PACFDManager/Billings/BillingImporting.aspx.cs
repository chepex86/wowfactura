﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;

namespace PACFDManager.Billings
{
    public partial class BillingImporting : PACFDManager.BasePage
    {
        const string MESSAGE_REDIRECT = "MESSAGE_REDIRECT";



        /// <summary>
        /// Temporal billing data set.
        /// </summary>
        protected PACFD.DataAccess.BillingsDataSet BillingTemporal
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.BillingsDataSet))
                    return null;

                return (PACFD.DataAccess.BillingsDataSet)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }



        public override void DataBind()
        {
            base.DataBind();

            System.IO.StreamReader reader = new System.IO.StreamReader(fuFile.PostedFile.InputStream);
            PACFD.Rules.Billings b = new PACFD.Rules.Billings();
            this.btnImport.Visible =
                this.btnImport2.Visible = false;
            this.fuFile.Visible =
                this.btnSendFile.Visible = true;

            if (fuFile.PostedFile.InputStream.Length < 1)
            {
                this.WebMessageBox1.ShowMessage("El archivo esta vacio.", true);
                return;
            }


            try
            {
                BillingTemporal = b.GetBillingFromStream(fuFile.PostedFile.InputStream, this.CurrentBillerID, this.CurrentElectronicBillingType);
            }
            catch (PACFD.Rules.BillingsExceptions.BillingExceptionBase ex)
            {
                PACFD.Common.LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage(string.Format("Un valor no pudo ser leido. Comprobante no. {0}.", ex.RowIndex + 1), true);
                return;
            }

            if (this.gdvMain.Rows.Count  < 1)
            {
                this.WebMessageBox1.ShowMessage("No se encontro ningun registro.", true);
                return;
            }

            this.btnSendFile.Visible =
                this.fuFile.Visible = false;
            this.btnImport.Visible =
                this.btnImport2.Visible = true;
            this.gdvMain.DataSource = BillingTemporal.Billings;
            this.gdvMain.DataBind();
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_REDIRECT:
                    this.Response.Redirect("~/Default.aspx");
                    return;
                default:
                    break;
            }
        }

        protected void gdvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int billingid;
            GridView grid;

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            billingid = this.BillingTemporal.Billings[e.Row.RowIndex].BillingID;

            this.gdvMain_RowDataBound_SetCompanyData(billingid, e);

            ///--------------------------------------
            ///concept's configuration.
            grid = e.Row.FindControl("gdvConcepts") as GridView;
            grid.DataSource = this.BillingTemporal.BillingsDetails.Select(
                string.Format("BillingID={0}", billingid));
            grid.DataBind();

            this.gdvMain_RowDataBound_SetConceptData(billingid, e);
        }

        private void gdvMain_RowDataBound_SetCompanyData(int billingid, GridViewRowEventArgs e)
        {
            Label l;

            l = e.Row.FindControl("lblCompanyName") as Label;
            l.Text = string.Format("{0} - {1}, {2}, {3}",
                this.BillingTemporal.BillingsBillers[e.Row.RowIndex].BillerName,
                this.BillingTemporal.BillingsBillers[e.Row.RowIndex].Location,
                this.BillingTemporal.BillingsBillers[e.Row.RowIndex].State,
                this.BillingTemporal.BillingsBillers[e.Row.RowIndex].Country
                );
            l = e.Row.FindControl("lblCompanyAddress") as Label;
            l.Text = this.BillingTemporal.BillingsBillers[e.Row.RowIndex].BillerAddress;
            l = e.Row.FindControl("lblCompanyRFC") as Label;
            l.Text = this.BillingTemporal.BillingsBillers[e.Row.RowIndex].BillerRFC;

            l = e.Row.FindControl("lblClientName") as Label;
            l.Text = string.Format("{0} - {1}, {2}, {3}",
                this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].ReceptorName,
                this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].Location,
                this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].State,
                this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].Country
            );
            l = e.Row.FindControl("lblClientAddress") as Label;
            l.Text = this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].ReceptorAddress;
            l = e.Row.FindControl("lblClientRFC") as Label;
            l.Text = this.BillingTemporal.BillingsReceptors[e.Row.RowIndex].ReceptorRFC;
        }

        private void gdvMain_RowDataBound_SetConceptData(int billingid, GridViewRowEventArgs e)
        {
            GridView grid;
            System.Data.DataRow row;
            System.Data.DataRow[] row2, row3;
            System.Data.DataTable table;
            object[] itemarray;

            ///-----------------
            ///Iva's configuration.
            row2 = this.BillingTemporal.DetainedTaxes.Select(string.Format("BillingID={0}", billingid));
            row3 = this.BillingTemporal.TransferTaxes.Select(string.Format("BillingID={0}", billingid));

            table = new System.Data.DataTable();
            table.Merge(this.BillingTemporal.DetainedTaxes, row2);
            table.Merge(this.BillingTemporal.TransferTaxes, row3);

            itemarray = new object[table.Rows[0].ItemArray.Length];
            itemarray = table.Rows[0].ItemArray;
            itemarray[table.ColumnNameIndex("BillingID")] = this.BillingTemporal.Billings[e.Row.RowIndex].BillingID;
            itemarray[table.ColumnNameIndex("Name")] = "Total";
            itemarray[table.ColumnNameIndex("Import")] = this.BillingTemporal.Billings[e.Row.RowIndex].Total;
            row = table.NewRow();
            row.ItemArray = itemarray;
            table.Rows.InsertAt(row, 0);

            itemarray = null;
            itemarray = new object[table.Rows[0].ItemArray.Length];
            itemarray = table.Rows[0].ItemArray;
            itemarray[table.ColumnNameIndex("BillingID")] = this.BillingTemporal.Billings[e.Row.RowIndex].BillingID;
            itemarray[table.ColumnNameIndex("Name")] = "SubTotal";
            itemarray[table.ColumnNameIndex("Import")] = this.BillingTemporal.Billings[e.Row.RowIndex].SubTotal;
            row = table.NewRow();
            row.ItemArray = itemarray;
            table.Rows.Add(row);

            grid = e.Row.FindControl("gdvIVAs") as GridView;
            grid.DataSource = table;
            grid.DataBind();
        }

        protected void btnSendFile_Click(object sender, EventArgs e)
        {
            this.DataBind();


            //PACFD.WebServices.Billing billing = new PACFD.WebServices.Billing();
            //System.Xml.XmlDocument doc = new System.Xml.XmlDocument(); 
            //doc.Load(@"C:\Users\Pablo\Desktop\XML PACFD ejemplo.xml");
            //billing.InsertBilling(doc);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            System.Data.DataRow[] rows;
            int indexbill = 0;
            this.WebMessageBox1.CommandArguments = MESSAGE_REDIRECT;

            if (this.BillingTemporal.IsNull() || this.gdvMain.Rows.Count < 1)
            {
                this.WebMessageBox1.ShowMessage("No se pudo guardar el comprobante.", true);
                return;
            }

            rows = null;

            foreach (PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow in this.BillingTemporal.Billings)
            {
                indexbill++;

                using (PACFD.DataAccess.BillingsDataSet billingclone = new PACFD.DataAccess.BillingsDataSet())
                {
                    billingclone.Billings.ImportRow(billingrow);

                    rows = this.BillingTemporal.Taxes.Select(string.Format("BillingID={0}", billingrow.BillingID));
                    billingclone.Taxes.ImportRow(rows[0]);

                    rows = this.BillingTemporal.BillingsBillers.Select(string.Format("BillingID={0}", billingrow.BillingID));
                    billingclone.BillingsBillers.ImportRow(rows[0]);

                    //rows = billingclone.BillingsIssued.Select(string.Format("BillingID={0}", billingrow.BillingID));
                    //billingclone.BillingsIssued.ImportRow(rows[0]);

                    rows = this.BillingTemporal.BillingsReceptors.Select(string.Format("BillingID={0}", billingrow.BillingID));
                    billingclone.BillingsReceptors.ImportRow(rows[0]);

                    rows = this.BillingTemporal.DetainedTaxes.Select(string.Format("BillingID={0}", billingrow.BillingID));

                    for (int i = 0; i < rows.Length; i++)
                        billingclone.DetainedTaxes.ImportRow(rows[i]);

                    rows = this.BillingTemporal.TransferTaxes.Select(string.Format("BillingID={0}", billingrow.BillingID));

                    for (int i = 0; i < rows.Length; i++)
                        billingclone.TransferTaxes.ImportRow(rows[i]);

                    rows = this.BillingTemporal.BillingsDetails.Select(string.Format("BillingID={0}", billingrow.BillingID));

                    for (int i = 0; i < rows.Length; i++)
                        billingclone.BillingsDetails.ImportRow(rows[i]);

                    using (PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable serietable = series.GetUnusedSerialByBillerID(this.CurrentBillerID,this.CurrentBranchID))
                    {
                        if (serietable.Count < 1)
                        {
                            this.WebMessageBox1.ShowMessage("No se pudo encontrar una serie o folio libre. La importación no puede continuar.", true);
                            return;
                        }

                        billingclone.Billings[0].ApprovalNumber = serietable[0].AprovationNumber;
                        billingclone.Billings[0].ApprovalYear = serietable[0].AprovationYear;
                        billingclone.Billings[0].Serial = serietable[0].Serial;
                        billingclone.Billings[0].Folio = serietable[0].Folio;
                        billingclone.Billings[0].FolioID = serietable[0].FolioID;
                        billingclone.Billings[0].SerialID = serietable[0].SerialID;
                    }

                    if (!bill.Update(billingclone))
                    {
                        this.BillingTemporal.Dispose();
                        this.BillingTemporal = null;
                        this.WebMessageBox1.ShowMessage(string.Format("No se pudo guardar el comprobante numero {0}. La importación no puede continuar.", indexbill), true);
                        return;
                    }

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} importo un comprobante numero {1}.",
                        this.UserName, indexbill), this.CurrentBillerID, null, billingclone.GetXml());
                    #endregion

                    if (!billingclone.Billings[0].IsIsCreditNull() && !billingclone.Billings[0].IsCredit)
                    {
                        PACFD.Common.ErrorManager error = new PACFD.Common.ErrorManager();
                        bill.SealCFD(billingclone.Billings[0].BillingID,ref error);
                    }

                    if (!billingclone.Billings[0].IsIsCreditNull() && !billingclone.Billings[0].IsCredit)
                        bill.SetPaidTo(billingclone.Billings[0].BillingID, true);
                    else
                        bill.SetPaidTo(billingclone.Billings[0].BillingID, false);
                }
            }

            this.BillingTemporal.Dispose();
            this.BillingTemporal = null;
            
            this.WebMessageBox1.ShowMessage("Comprobante guardado con éxito.", true);
        }
    }
}