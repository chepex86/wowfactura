﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Billings
{
    public partial class SendBillingMail : PACFDManager.BasePage
    {
        const string BILLINGS_NOT_FOUND = "0";
        const string BILLINGS_INCORRECT_MAIL = "1";


        public int BillingID
        {
            get
            {
                int r;
                object o;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Billings billing;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable billingtable;
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable receptortable;

            if (this.IsPostBack)
                return;

            this.btnCancel.CommandArgument = this.Request.UrlReferrer.ToString();

            billing = new PACFD.Rules.Billings();
            billingtable = billing.SelectByID(this.BillingID);
            billing = null;

            if (billingtable.Count < 1)
            {
                billingtable.Dispose();
                billingtable = null;
                this.WebMessageBox1.CommandArguments = BILLINGS_NOT_FOUND;
                this.WebMessageBox1.ShowMessage("No se puede encontrar el comprobante.", true);
                return;
            }

            receptor = new PACFD.Rules.Receptors();
            receptortable = receptor.SelectByID(billingtable[0].ReceptorID);
            receptor = null;

            if (receptortable.Count < 1)
            {
                receptortable.Dispose();
                receptortable = null;
                return;
            }

            this.txtTo.Text = receptortable[0].Email;
            this.txtDescription.Text = string.Format("Aviso de {0}comprobante: {1} - {2}",
                billingtable[0].PreBilling ? "pre" : string.Empty,
                billingtable[0].Serial,
                billingtable[0].Folio);

            billingtable.Dispose();
            billingtable = null;
            receptortable.Dispose();
            receptortable = null;
        }

        protected void btnAcept_Click(object sender, EventArgs ea)
        {
            PACFD.Rules.BillingMailSender mail;
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            PACFD.DataAccess.BillingsDataSet e;
            PACFD.Rules.Mail.MailSendResult errormail;
            System.Text.StringBuilder sb;

            this.txtTo_RegularExpressionValidator.ValidationExpression =
                "^(\\s*,?\\s*[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})+\\s*$";
            this.txtTo_RegularExpressionValidator.Validate();

            if (!this.txtTo_RegularExpressionValidator.IsValid)
            {
                this.WebMessageBox1.CommandArguments = BILLINGS_INCORRECT_MAIL;
                this.WebMessageBox1.ShowMessage("El campo de correo electronico es incorrecto.", true);
                return;
            }

            e = bill.GetFullBilling(this.BillingID);

            if (e.Billings.Count < 1)
            {
                this.WebMessageBox1.ShowMessage("No se pudo encontrar el comprobante.", true);
                return;
            }

            mail = new PACFD.Rules.BillingMailSender(this.Request.PhysicalApplicationPath + @"Includes\Mail\EmisorAdded.es.xml", "004", this.CurrentBillerID);

            #region Old source code to send mail
            //mail[BillingMailSender.LBL_BILLER_ADDRESS] = e.BillingsBillers[0].IssuedInDifferentPlace ?
            //    e.BillingsIssued[0].Address : e.BillingsBillers[0].BillerAddress;
            //mail[BillingMailSender.LBL_BILLER_NAME] = e.BillingsBillers[0].BillerName;
            //mail[BillingMailSender.LBL_DATE] = e.Billings[0].BillingDate.ToString("yyyy/MM/dd");
            //mail[BillingMailSender.LBL_FOLIO] = e.Billings[0].Folio;
            //mail[BillingMailSender.LBL_MAIL_DATE] = DateTime.Now.ToString("yyyy/MM/dd");
            //mail[BillingMailSender.LBL_PAYMENT_METHOD] = e.Billings[0].PaymentMethod;
            //mail[BillingMailSender.LBL_PREBILLING] = e.Billings[0].PreBilling ? "No sellado" : "Sellado";
            //mail[BillingMailSender.LBL_RECEPTOR_ADDRESS] = e.BillingsReceptors[0].ReceptorAddress;
            //mail[BillingMailSender.LBL_SUBTOTAL] = e.Billings[0].SubTotal.ToString(); 
            //mail[BillingMailSender.LBL_TOTAL] = e.Billings[0].Total.ToString();
            //mail[BillingMailSender.LBL_TOTAL_DETAINED] = e.Taxes[0].TotalDetained.ToString();
            //mail[BillingMailSender.LBL_TOTAL_TRANSFERED] = e.Taxes[0].TotalTransfer.ToString();
            //mail[BillingMailSender.LBL_DOWNLOAD_ADDRESS] =
            //    string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Requezst.Url.Authority
            //    + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=pdf&a={0}&b={1}&o=true",
            //    PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString()),
            //    PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
            //    );
            //mail[BillingMailSender.LBL_DOWNLOAD_ADDRESS_2] = string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
            //    + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=xml&a={0}&b={1}&o=true",
            //    PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString()),
            //    PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
            //    ); 
            #endregion

            PACFD.Rules.BillingMailSender.SetBillingMail(mail, ref e, this.CurrentBillerID);

            if (this.ckbDownloadButtons.Checked)
            {
                sb = new System.Text.StringBuilder();

                sb.AppendLine("<ul class=\"ticket-detail-nav\">");
                sb.AppendLine("<li><a class=\"pdf-icon\" href='[lblDownloadAddress]'>Descarga de archivo pdf haz click aqui.</a></li>");

                mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS] =
                    string.Format("{0}://{1}{2}/Billings/XML/{3}",
                    HttpContext.Current.Request.Url.Scheme,
                    HttpContext.Current.Request.Url.Authority,
                    HttpContext.Current.Request.ApplicationPath,
                    mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS]);

                if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
                //{
                //mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS_2] = string.Empty;
                //}
                // else
                {
                    mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS_2] =
                    string.Format("{0}://{1}{2}/Billings/XML/{3}",
                    HttpContext.Current.Request.Url.Scheme,
                    HttpContext.Current.Request.Url.Authority,
                    HttpContext.Current.Request.ApplicationPath,
                    mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS_2]);

                    sb.AppendLine("<li><a class=\"xml-icon\" href='[lblDownloadAddress2]'>Descarga de archivo xml haz click aqui.</a></li>");
                }

                sb.AppendLine("</ul>");

                mail.Parameters[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_HTML] = sb.ToString();
            }

            mail.Message.To.Clear();
            mail.Message.To.Add(this.txtTo.Text.Trim());
            mail.Message.Subject = this.txtDescription.Text.Trim();

#if DEBUG
            System.Diagnostics.Debug.WriteLine(mail[PACFD.Rules.BillingMailSender.LBL_DOWNLOAD_ADDRESS]);
#endif

            if (this.ckbIncludeFiles.Checked)
                this.AddFilesToMail(mail);

            if (!(errormail = mail.Send()).IsSended)
            {
                LogManager.WriteError(new Exception(errormail.Error.Message));
                this.WebMessageBox1.ShowMessage("No se pudo enviar el correo.", true);
                return;
            }

            this.WebMessageBox1.ShowMessage("Correo enviado con exito.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);

            //mail.Message.Attachments.Add(new System.Net.Mail.Attachment(
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case BILLINGS_INCORRECT_MAIL: this.WebMessageBox1.CommandArguments = string.Empty; return;
            }

            this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect((sender as Button).CommandArgument);
        }

        private void AddFilesToMail(PACFD.Rules.BillingMailSender mail)
        {
            System.Net.Mail.Attachment at;
            System.IO.Stream stream = this.GetPDF();
            System.Net.Mime.ContentDisposition comp;

            if (!stream.IsNull())
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                at = new System.Net.Mail.Attachment(stream, System.Net.Mime.MediaTypeNames.Application.Pdf);
                comp = at.ContentDisposition;
                comp.CreationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                at.Name = string.Format("{0} {1}.pdf", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"));
                mail.Message.Attachments.Add(at);
            }

            stream = null;
            stream = this.GetXml();
            comp = null;

            if (!stream.IsNull() && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                at = new System.Net.Mail.Attachment(stream, "text/xml");
                comp = at.ContentDisposition;
                comp.CreationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                at.Name = string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"));
                mail.Message.Attachments.Add(at);
            }
        }

        private System.IO.Stream GetPDF()
        {
            PACFD.Rules.PrintDocument.BillingDocumentPrintEventArgs r;
            PACFD.Rules.PrintDocument.BillingDocument doc = new PACFD.Rules.PrintDocument.BillingDocument();
            System.IO.Stream stream;

            stream = this.GetPrintConfigStream();

            if (stream.IsNull())
                return null;

            doc.LoadFromXML(stream);

            r = doc.Print(this.BillingID);

            if (r.IsNull())
            {
                LogManager.WriteError(new Exception("document is null."));
                return null;
            }

            return r.PDFDocumnet;
        }

        private System.IO.Stream GetXml()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            string fileName = string.Empty;

            byte[] b = billing.GetXml(BillingID, ref fileName);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();

            if (b.IsNull() || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                return null;

            stream.Write(b, 0, b.Length);

            return stream;
        }

        protected System.IO.Stream GetPrintConfigStream()
        {
            string s;
            System.IO.MemoryStream stream = null;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable;
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;
            PACFD.Rules.PrintTemplates p = new PACFD.Rules.PrintTemplates();
            System.Text.UTF8Encoding ut8;
            byte[] b;

            billtable = billing.SelectByID(this.BillingID);
            billing = null;

            if (billtable.Count < 1)
            {
                billtable.Dispose();
                billtable = null;
                return null;
            }

            table = p.SelectByID(billtable[0].PrintTemplateID);
            p = null;
            stream = new System.IO.MemoryStream();

            if (table.Count < 1)
                table = p.SelectByID(1); // <---------- default ID PrintTemplate             

            if (table.Count < 1)
            {
                table.Dispose();
                table = null;

                s = this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ?
                        PACFD.Rules.PrintTemplates.GetStringDefaultCBB() :
                           this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD ?
                            PACFD.Rules.PrintTemplates.GetStringDefaultCFD() :
                                   this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ?
                                       PACFD.Rules.PrintTemplates.GetStringDefaultCFDI() :
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2||
                                             this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3? PACFD.Rules.PrintTemplates.GetStringDefaultCFDI3_2() :
                                                string.Empty;
            }
            else
            {
                s = table[0].Text;
                table.Dispose();
                table = null;
            }

            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the posistion
            ut8 = new System.Text.UTF8Encoding(); //avoid text without UTF8 Encoding format
            b = ut8.GetBytes(s); //temporal bytes array nedeed
            stream.Write(b, 0, b.Length); //write array
            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the position

            return stream;
        }
    }
}
