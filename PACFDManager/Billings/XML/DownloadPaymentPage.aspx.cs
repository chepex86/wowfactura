﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules.PrintDocument;

namespace PACFDManager.Billings.XML
{
    /// <summary>
    /// Class used as a dummy to download files.
    /// The html header of the pages is override in run time.    
    /// </summary>
    public partial class DownloadPaymentPage : System.Web.UI.Page
    {
        public const string FILETYPE_PDF = "PDF";
        public const string FILETYPE_XML = "XML";
        public const string FILETYPE_PREVIEW = "PREVIEW";


        bool IsCopy
        {
            get
            {
                string s = this.Request.QueryString["iscopy"];

                if (string.IsNullOrEmpty(s))
                    return false;

                return s.ToBoolean();
            }
        }
        /// <summary>
        /// Get a string with the reques file type.
        /// </summary>
        string FileType
        {
            get
            {
                string s = this.Request.QueryString["type"];

                if (string.IsNullOrEmpty(s))
                    return string.Empty;

                return s.ToUpper();
            }
        }
        /// <summary>
        /// Get the billing ID to download.
        /// </summary>
        int BillingPaymentID
        {
            get
            {
                string s = this.Request.QueryString["b"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                return s.ToInt32();
            }
        }
        /// <summary>
        /// Get an integer with the biller ID to owner of the bill.
        /// </summary>
        int BillerID
        {
            get
            {
                string s = this.Request.QueryString["a"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                return s.ToInt32();
            }
        }
        /// <summary>
        /// Get a boolean value indicating if the download comes from out side.
        /// </summary>
        public bool IsOut
        {
            get
            {
                string s = this.Request.QueryString["out"];

                if (string.IsNullOrEmpty(s))
                    return false;

                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                return s.ToBoolean();
            }
        }
        /// <summary>
        /// Set a temporal data set to be downloaded.
        /// </summary>
        public PACFD.DataAccess.BillingsDataSet PreviewDataSet
        {
            private get
            {
                object o = this.Session["Download_temporal_preview_pdf"];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.BillingsDataSet))
                    return null;

                return (PACFD.DataAccess.BillingsDataSet)o;
            }
            set { this.Session["Download_temporal_preview_pdf"] = value; }
        }

        public string PrintXML
        {
            get
            {
                object s = this.Session["temporal_xml_download"];

                if (s.IsNull())
                    return string.Empty;

                return s.ToString();
            }
            set { this.Session["temporal_xml_download"] = value; }
        }

        //public System.Web.HttpContext OpenContext
        //{
        //    get
        //    {
        //        return this.Context;
        //    }
        //}

        /// <summary>
        /// Page load event.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.BillingPaymentID < 1)
                return;

            switch (this.FileType)
            {
                case FILETYPE_PREVIEW:

                    if (this.PreviewDataSet.IsNull())
                    {
                        this.Response.Write("<b>No se pudo generar la vista previa.</b>");
                        return;
                    }

                    PDFPaymentCreator p = new PDFPaymentCreator();
                    p.BillingPaymentID = this.BillingPaymentID;
                    p.Context = this.Context;
                    p.GetPDFTest(this.PreviewDataSet, this.PrintXML);
                    //return;
                    break;
                case FILETYPE_PDF: this.GetPDF(); break;
                case FILETYPE_XML: this.GetXML(); break;
            }

            if (Request.UrlReferrer != null)
                this.Response.Redirect(Request.UrlReferrer.ToString());
        }
        /// <summary>
        /// Download the PDF.
        /// </summary>
        private void GetPDF()
        {
            PDFPaymentCreator p = new PDFPaymentCreator();
            p.BillingPaymentID = this.BillingPaymentID;
            p.Context = this.Context;

            if (this.IsCopy)
            {
                p.BeforePdfPrints += (se1, e1) =>
                {
                    ((BillingDocument)se1).BeforePrint += (sender, e) =>
                    {
                        BillingDocument doc = (BillingDocument)sender;
                        doc.WaterMark.Enabled = true;
                        doc.WaterMark.Alpha = 30;
                        doc.WaterMark.Text = string.Format("   Copia de Comprobante   ", doc.WaterMark.Text.Trim());
                    };
                };
            }

            p.GetPDF();
        }
        /// <summary>
        /// Download the XML.
        /// </summary>
        private void GetXML()
        {
            XmlPaymentCreator x = new XmlPaymentCreator();
            x.BillingsPaymentID = this.BillingPaymentID;
            x.Context = this.Context;
            x.GetXml();
        }
    }
}