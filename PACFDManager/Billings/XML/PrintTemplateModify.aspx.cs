﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using PACFD.Common;

namespace PACFDManager.Billings.XML
{
    public partial class PrintTemplateModify : PACFDManager.BasePage
    {
        const string MESSAGE_ERROR = "1";
        const string MESSAGE_REDIRECT = "2";


        public int TemporalReportID
        {
            get
            {
                int r;
                object o;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session["id"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.PrintTemplateEditor1.EditorMode = PrintTemplateEditorMode.Edit;

            if (this.IsPostBack)
                return;

            this.PrintTemplateEditor1.PrintTemplateID = this.TemporalReportID;
            this.PrintTemplateEditor1.DataBindPrintTemplate();

            this.SetJavaScript();
        }

        private void SetJavaScript()
        {
            StringBuilder sb = new StringBuilder();
            System.Xml.XmlDocument document;
            string[] scolor = { "#cad8e2", "#ffd4d4", "#d9ffd4" };
            int indexcolor = -1;

            sb.AppendLine("<script type='text/javascript'>");
            sb.AppendLine(string.Format("function {0}_ShowHideHelp()", this.ClientID));
            sb.AppendLine("{");
            sb.AppendLine(string.Format("var x = document.getElementById(\"{0}\")", this.divXML.ClientID));
            sb.AppendLine(string.Format("var h = document.getElementById(\"{0}\")", this.divHelp.ClientID));
            sb.AppendLine(string.Format("var b = document.getElementById(\"{0}\")", this.btnXML.ClientID));
            sb.AppendLine("");
            sb.AppendLine("if(x.style.display == \"block\"){");
            sb.AppendLine("x.style.display = \"none\";");
            sb.AppendLine("h.style.display = \"block\";");
            sb.AppendLine("b.value = \"Mostrar xml\";");
            sb.AppendLine("}else{");
            sb.AppendLine("x.style.display = \"block\";");
            sb.AppendLine("h.style.display = \"none\";");
            sb.AppendLine("b.value = \"Mostrar palabras clave\";");
            sb.AppendLine("}");
            sb.AppendLine("}</script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showhidehelp", sb.ToString());
            this.btnXML.Attributes["onclick"] = string.Format("javascript:{0}_ShowHideHelp();", this.ClientID);

            sb = null;
            sb = new StringBuilder();

            document = PACFD.Rules.PrintTemplates.GetDBDefinition();

            if (document.IsNull())
            {
                this.WebMessageBox1.CommandArguments = string.Empty;
                this.WebMessageBox1.ShowMessage("Hay un error en la configuración de impreciones, las palabras claves no se cargarón. Informe a soporte técnico.",
                    System.Drawing.Color.Blue, WebMessageBoxButtonType.Accept);
                return;
            }

            sb.AppendLine("<table cellpadding='0' cellspacing='1'>");

            foreach (System.Xml.XmlNode list in document.ChildNodes)
            {
                if (list.Name != "database")
                    continue;

                foreach (System.Xml.XmlNode database in list)
                {
                    if (database.Name != "table")
                        continue;

                    indexcolor = indexcolor > scolor.Length - 2 ? 0 : indexcolor + 1;

                    foreach (System.Xml.XmlNode n in database)
                    {
                        if (n.Name != "row")
                            continue;

                        sb.AppendLine(string.Format("<tr style='background-color:{3};'><td style='text-align:left'>{0}</td><td>{1}.{2}</td></tr>"
                            , n.Attributes["alias"].IsNull() ? "[error de nombre]" : n.Attributes["alias"].Value
                            , database.Attributes["id"].IsNull() ? "[error de tabla]" : database.Attributes["id"].Value
                            , n.Attributes["id"].IsNull() ? "[error de id]" : n.Attributes["id"].Value
                            , scolor[indexcolor]
                            ));
                    }
                }
            }

            sb.AppendLine("</table>");
            this.ltrHelp.Text = sb.ToString();
        }

        protected void PrintTemplateEditor1_ApplyChanges(object sender, PrintTemplateApplyEventArgs e)
        {
            PACFD.Rules.PrintTemplates template;

            template = new PACFD.Rules.PrintTemplates();

            if (!template.Update(e.DataTable))
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("No se pudo modificar configuración.", true);
                return;
            }

            this.WebMessageBox1.CommandArguments = MESSAGE_REDIRECT;
            this.WebMessageBox1.ShowMessage("Éxito al modificar configuración.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            this.WebMessageBox1.CommandArguments = string.Empty;

            switch (e.CommandArguments)
            {
                case MESSAGE_ERROR:
                    break;
                case MESSAGE_REDIRECT:
                    this.btnCancel_Click(this, EventArgs.Empty);
                    break;
            }
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            if (!this.PrintTemplateEditor1.IsXMLValid)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("XML no valido.", true);
                return;
            }

            try
            {
                this.PrintTemplateEditor1.CreatePrintTemplate();
            }
            catch (Exception ex)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("No se pudo modificar configuración.", true);
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(PrintTemplateList).Name + ".aspx"); }

        protected void btnTest_Click(object sender, EventArgs e)
        {
            PDFCreator p = new PDFCreator();
            int i = 15;

            if (!this.PrintTemplateEditor1.IsXMLValid)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("XML no valido.", true);
            }

            if (!int.TryParse(this.txtTestCount.Text, out i))
            { i = 15; }
            else
            { i++; }

            if (i < 1 || i > 50)
            { i = 15; }

            p.Context = this.Context;
            p.GetPDFTest(PACFD.DataAccess.BillingsDataSet.GetExampleDataSet(i), this.PrintTemplateEditor1.XML);
        }
    }
}
