﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PACFDManager.Billings.XML
{
    /// <summary>
    /// Descripción breve de $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://www.univisit.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class XmlCreator : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        internal HttpContext Context { get; set; }
        internal int BillingID { get; set; }

        public void ProcessRequest(HttpContext context)
        {
            int i;

            ///----------------------------------------------
            ///security validations
            if (Security.Security.CurrentBillerID < 1)
                return;

            if (context.Request.QueryString["id"].IsNull())
                return;

            this.Context = context;

            if (this.Context.Request.QueryString["id"].IsNull())
                return;

            i = this.Context.Request.QueryString["id"].ToInt32();

            if (i < 1)
                return;

            this.BillingID = i;

            this.Context = context;
            this.GetXml();
        }

        internal void GetXml()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            string fileName = string.Empty;
            
            byte[] b = billing.GetXml(BillingID,  ref fileName);

            if (b.IsNull())
                return;

            this.Context.Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
            this.Context.Response.ContentType = "application/octet-stream";
            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
