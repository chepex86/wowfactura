﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings.XML
{
    public class PrintTemplateApplyEventArgs : EventArgs
    {
        public PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable DataTable { get; private set; }

        public PrintTemplateApplyEventArgs(PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable e)
        {
            this.DataTable = e;
        }
    }
}
