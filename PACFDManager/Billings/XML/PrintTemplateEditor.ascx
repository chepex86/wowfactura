﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintTemplateEditor.ascx.cs"
    Inherits="PACFDManager.Billings.XML.PrintTemplateEditor" %>
<ul>
    <li>
        <label class="desc">
            <asp:RequiredFieldValidator ID="txtName_RequiredFieldValidator" runat="server" ErrorMessage="*"
                ControlToValidate="txtName" />
            <asp:Label ID="lblName" runat="server" Text="Nombre" />
        </label>
        <span>
            <asp:TextBox ID="txtName" runat="server" Width="90%" />
        </span></li>
    <li>
        <label class="desc">
            <asp:Label ID="lblXML" runat="server" Text="Codigo" />
        </label>
    </li>
</ul>
<span>
    <asp:TextBox ID="txtXML" runat="server" Height="700px" Width="98%" TextMode="MultiLine" />
</span>