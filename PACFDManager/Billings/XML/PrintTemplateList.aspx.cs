﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings.XML
{
    public partial class PrintTemplateList : PACFDManager.BasePage
    {
        /// <summary>
        /// Message box delete question.
        /// </summary>
        const string MESSAGE_DELETE = "1";



        /// <summary>
        /// Get or set the temporal report ID.
        /// </summary>
        public int TemporalReportID
        {
            get
            {
                int r;
                object o;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session["id"] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.DataBindPrintTemplateGridView();
        }

        private void DataBindPrintTemplateGridView()
        {
            PACFD.Rules.PrintTemplates print;

            print = new PACFD.Rules.PrintTemplates();

            this.gdvPrintTemplate.DataSource = print.SelectByBillerID(this.CurrentBillerID);
            this.gdvPrintTemplate.DataBind();
            print = null;
        }

        protected void ImageButton_Click(object sender, EventArgs e)
        {
            int id;
            ImageButton b = sender as ImageButton;
            PDFCreator pdf;
            PACFD.Rules.PrintTemplates printtemplate;
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable printtemplatetable;
            string s;


            if (b.IsNull())
                return;

            if (!int.TryParse(b.CommandArgument, out id))
                return;

            if (id < 1)
                return;

            this.TemporalReportID = id;

            switch (b.ID)
            {
                case "imbEdit":
                    this.Response.Redirect(typeof(PrintTemplateModify).Name + ".aspx");
                    break;
                case "imbDelete":
                    this.WebMessageBox1.CommandArguments = MESSAGE_DELETE;
                    this.WebMessageBox1.ShowMessage("¿Desea eliminar la configuración?", System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
                case "imbTest":
                    pdf = new PDFCreator();
                    pdf.Context = this.Context;

                    if (this.TemporalReportID < 1)
                        return;

                    printtemplate = new PACFD.Rules.PrintTemplates();

                    using (printtemplatetable = printtemplate.SelectByID(this.TemporalReportID))
                    {
                        printtemplate = null;

                        if (printtemplatetable.Count < 1)                        
                            return;                        

                        s = printtemplatetable[0].Text;
                    }

                    pdf.GetPDFTest(PACFD.DataAccess.BillingsDataSet.GetExampleDataSet(15), s);
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;
            PACFD.Rules.PrintTemplates template;

            if (e.CommandArguments == MESSAGE_DELETE)
            {
                this.WebMessageBox1.CommandArguments = string.Empty;
                template = new PACFD.Rules.PrintTemplates();
                table = template.SelectByID(this.TemporalReportID);

                if (table.Count < 1)
                {
                    template = null;
                    table.Dispose();
                    table = null;
                    this.WebMessageBox1.ShowMessage("Error inesperado al intentar eliminar.", true);
                    return;
                }

                table[0].Delete();
                template.Update(table);
                template = null;
                table.Dispose();
                table = null;

                this.DataBindPrintTemplateGridView();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(typeof(PrintTemplateAdd).Name + ".aspx");
        }
    }
}
