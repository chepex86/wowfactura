﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="PrintTemplateAdd.aspx.cs" Inherits="PACFDManager.Billings.XML.PrintTemplateAdd"
    ValidateRequest="false" %>

<%@ Register Src="PrintTemplateEditor.ascx" TagName="PrintTemplateEditor" TagPrefix="uc1" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Editor de PDF" />
                    </h2>
                </div>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <input type="button" id="btnXML" value="Mostrar palabras clave" runat="server" class="button" />
                        </td>
                        <td>
                            <asp:Button ID="btnTest" runat="server" Text="Prueba" OnClick="btnTest_Click" CausesValidation="false" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtTestCount" runat="server" MaxLength="2" Text="15" Width="30px" />
                        </td>
                    </tr>
                </table>
                <div id="divXML" runat="server" style="display: block; width: 100%">
                    <uc1:PrintTemplateEditor ID="PrintTemplateEditor1" runat="server" OnApplyChanges="PrintTemplateEditor1_ApplyChanges" />
                    <div>
                        <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click" />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                            OnClick="btnCancel_Click" CausesValidation="false" />
                    </div>
                </div>
                <div id="divHelp" runat="server" style="display: none; width: 100%">
                    <br />
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Literal ID="ltrHelp" runat="server" />
                            </td>
                            <td style="width: 50px;">
                                <p>
                                </p>
                            </td>
                            <td valign="top">
                                S = Texto o numero tipo cadena.
                                <br />
                                I = Objeto tipo imagen.
                                <br />
                                <br />
                                Tag <b>"printlabel"</b>, la propiedad "text" admite usar texto con llaves "ejemplo:
                                {n} aqui."
                                <br />
                                junto con la propiedad "data", para imprimir texto con datos, por cada
                                <br />
                                identificador "{n}" tiene que existir un identificador "Tx.Rx" separado por comas
                                ",".
                                <br />
                                Las propiedades "x" e "y" permiten modificar las coordenadas de dibujado del texto.
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    &#60;printlabel text="{0}-{1}" data = "T1.R4, T1.R5" x ="500" y="35" font="Arial"
                                    size="12"
                                    <br />
                                    width="310"/&#62;
                                </div>
                                <p>
                                </p>
                                Para definir como se debe calcular el dibujado multilinea, el tag cuenta con un
                                tributo llamado " inline-technique="char" ". Este atributo cuenta con dos opciones
                                posibles:
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    inline-technique="char" (["char"/"character"]).
                                    <br />
                                    inline-technique="string"
                                </div>
                                <p>
                                </p>
                                La primera opción, le indica al tag que debe dibujar la cadena caracter por caracter,
                                hasta que termine la linea y continua en el siguiente renglón.
                                <p>
                                </p>
                                La segunda opción, le indica al tag que debe dibujar la cadena usando palabras completas,
                                de tal forma que, si la palabra excede el espacio asignado para el renglón actual,
                                debe continuar la palabra en el siguiente renglon.
                                <p>
                                </p>
                                La configuración del atributo "inline-technique" es "string" de forma predeterminada,
                                pero para imprimir la cadena original y el sello se usa la configuración de "char".
                                <p>
                                </p>
                                Existen mas formas complejas de envio de datos a print label
                                <br />
                                <br />
                                <div style="background-color: lightyellow;">
                                    &#60;printlabel text="{0}{1:', {0}',notempty}{2:', {0}',up}{3:', {0}',low}{3:'{0}',onempty'esta
                                    vacio'}" data = "T2.R3, T2.R4, T2.R5, T2.R6" x="10" y="795" font="Arial" size="10"
                                    /&#62;
                                </div>
                                <br />
                                <br />
                                up = Fuerza el texto a mayusculas
                                <br />
                                low = fuerza el texto a minusculas
                                <br />
                                notempty = si el datos en la tabla esta vacio, lo excluye del texto
                                <br />
                                onempty = se agregan simbolos '' para sustituir el texto cuando el valor en la tabla
                                esta vacia.
                                <div style="background-color: lightyellow;">
                                    onempty'esta vacio'</div>
                                <br />
                                addonempty = agrega texto a la cadena cuando el token esta vacio.
                                <div style="background-color: lightyellow;">
                                    addonempty'esta vacio'</div>
                                <br />
                                <br />
                                add = agrega texto extra a la cadena cuando el token es leido.
                                <div style="background-color: lightyellow;">
                                    add'texto agregado'</div>
                                <br />
                                <br />
                                date = sustituye el texto por la fecha (servidor).
                                <br />
                                <p>
                                </p>
                                Tag <b>"printtext"</b> permite imprimir texto sin datos.
                                <br />
                                Las propiedades "x" e "y" permiten modificar las coordenadas de dibujado del texto.
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    &#60;printtext text="Importe en Letra" x="20" y="497" font="Arial" size="11" bold="true"
                                    <br />
                                    width="770"/&#62;
                                </div>
                                <p>
                                </p>
                                Tag <b>"printlayer"</b>, el tag permite definir zonas de impreción de controles.
                                <br />
                                Las propiedades "x" e "y" permiten modificar las coordenadas de dibujado del texto.
                                <br />
                                La propiedad "list" se usa en conjunto con la propiedad " table="BillingsDetails"
                                ",
                                <br />
                                para repetir datos disponibles por ejemplo la cantidad de articulos o conceptos.
                                <br />
                                Ejemplo de zona de imprecion sin listado:
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    &#60;printlayer x="20" y="225" width="611" height="470" &#62;
                                    <br />
                                    &#60;printtext text="Letra" x="20" y="47" font="Arial" bold="true" width="70"/&#62;
                                    <br />
                                    &#60;printlabel text="Datos:{0}" data = "T1.R4" x ="50" y="35" font="Arial" width="30"/&#62;
                                    <br />
                                    &#60;/printlayer&#62;
                                </div>
                                <p>
                                </p>
                                O en forma de lista, note como los tag <b>"printtext"</b> no son incluidos en el
                                <b>"printlayer"</b>.
                                <br />
                                Los <b>"printlabel"</b> se repitaran "n" numeros de veses dependiendo el alto y
                                ancho del
                                <br />
                                <b>"printlayer"</b>, de la cantidad de datos a imprimir y el ancho del <b>"printlabel"</b>
                                .
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    &#60;printlayer x="20" y="225" width="611" height="470" list="true" table="BillingsDetails"&#62;
                                    <br />
                                    &#60;printlabel text="Datos:{0}" data = "T1.R4" x ="50" y="35" font="Arial" width="30"/&#62;
                                    <br />
                                    &#60;/printlayer&#62;
                                </div>
                                <p>
                                </p>
                                Para agregar una <b>marca de agua</b> en la hoja, agregar el tag <b>"watermark"</b>
                                en la cabezera del archivo dentro del tag <b>"billingdoc"</b>. Donde <b>"text"</b>
                                es la cadena a imprimir, <b>"font"</b> es la fuente del text, <b>"size"</b> es el
                                tamaño del texto, <b>"color"</b> es el color del texto y <b>"alpha-color"</b> es
                                el nivel de transparencia.
                                <p>
                                </p>
                                <div style="background-color: lightyellow;">
                                    &#60;watermark text="Original" font="Arial" size="28" color="#000000" alpha-color="30"/&#62;
                                </div>
                                <p>
                                </p>
                                Para agregar el metodo de pago y Numero de cuenta en una sola linea, existe el control
                                de <b>"printpaymentform"</b>, este control une las dos tablas y regresa una sola
                                linea de codigo
                                <br />
                                ejemplo:
                                <br />
                                Cadena en tabla: Efectivo, Tarjeta, Cheque<br />
                                Cadena en tabla (numero de cuenta): 2345, 223<br />
                                Resutado: Efectivo(2345), Tarjeta(223), Cheque
                                <div style="background-color: lightyellow;">
                                    &#60;printpaymentform text="{0}" x ="20" y="576" font="Arial" size="8" width="400"
                                    inline-technique="char" /&#62;
                                </div>
                                <p>
                                </p>
                                Para imprimir el regimen fiscar, cuando es mas de uno, se usa el tag de <b>printfiscalregime</b>,
                                este tag regresara una fila por cada cadena regimen fiscal.
                                <div style="background-color: lightyellow;">
                                    &#60;printfiscalregime row-space="0" x ="20" y="576" font="Arial" size="8" /&#62;
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br />
    <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
