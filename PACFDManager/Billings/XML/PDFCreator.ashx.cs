﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PACFD.Common;
using PACFD.Rules.PrintDocument;

namespace PACFDManager.Billings.XML
{
    /// <summary>
    /// Descripción breve de $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://www.univisit.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class PDFCreator : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        internal event EventHandler BeforePdfPrints;

        internal HttpContext Context { get; set; }
        internal int BillingID { get; set; }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private PACFD.Rules.ElectronicBillingType ElectronicBillingType
        {
            get
            {
                PACFD.Rules.ElectronicBillingType e;

                e = Security.Security.CurrentElectronicBillingType;

                if (e == PACFD.Rules.ElectronicBillingType.Indeterminate)
                    e = PACFD.Rules.ElectronicBillingType.CFD;

                return e;
            }
        }

        [WebMethod(true)]
        public void ProcessRequest(HttpContext context)
        {
            int i;
            ///----------------------------------------------
            ///security validations
            if (Security.Security.CurrentBillerID < 1)
                return;

            if (context.Request.QueryString["id"].IsNull())
                return;

            this.Context = context;

            if (this.Context.IsNull())
                return;

            if (this.Context.Request.QueryString["id"].IsNull())
                return;

            i = this.Context.Request.QueryString["id"].ToInt32();

            if (i < 1)
                return;

            this.BillingID = i;

            LogManager.WriteStackTrace(new Exception("Creating PDF"));
            //all is O.K. send pdf
            this.GetPDF();
        }

        internal void GetPDF()
        {
            BillingDocumentPrintEventArgs r;
            BillingDocument doc = new BillingDocument();
            byte[] b;
            PACFD.Rules.Billings bill;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table;
            System.IO.Stream stream;

            stream = this.GetPrintConfigStream();

            if (stream.IsNull())
                return;

            doc.LoadFromXML(stream);

            if (!this.BeforePdfPrints.IsNull())
                this.BeforePdfPrints(doc, EventArgs.Empty);

            r = doc.Print(this.BillingID);

            if (r.IsNull())
            {
                LogManager.WriteError(new Exception("document is null."));
                return;
            }

            //LogManager.WriteStackTrace(new Exception("Creating byte array to send."));
            b = r.PdfToByteArray();

            bill = new PACFD.Rules.Billings();

            using (table = bill.SelectByID(this.BillingID))
            {
                bill = null;

                this.Context.Response.ClearContent();
                this.Context.Response.ClearHeaders();
                this.Context.Response.ContentType = "application/octet-stream";
                this.Context.Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename={0}.pdf",

                    this.ElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI ?
                    table.Count < 1 ? "pdfreport" : table[0].Serial + "_" + table[0].Folio
                    : table.Count < 1 ? "pdfreport" : table[0].IsUUIDNull() ? "pdfreport" : table[0].UUID)

                    );
            }

            if (b.IsNull())
                return;

            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.End();
        }

        internal void GetPDFTest(PACFD.DataAccess.BillingsDataSet dataset, string testxml)
        {
            BillingDocumentPrintEventArgs r;
            BillingDocument doc = new BillingDocument();
            byte[] b;
            PACFD.Rules.Billings bill;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table;
            System.IO.Stream stream;
            System.Text.UTF8Encoding ut8;

            ut8 = new System.Text.UTF8Encoding();
            b = ut8.GetBytes(testxml);
            stream = new System.IO.MemoryStream();
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            stream.Write(b, 0, b.Length);
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            b = null;

            if (stream.IsNull())
                return;

            doc.LoadFromXML(stream);
            r = doc.PrintDemo(dataset);

            if (r.IsNull())
            {
                LogManager.WriteError(new Exception("document is null."));
                return;
            }

            //LogManager.WriteStackTrace(new Exception("Creating byte array to send."));
            b = r.PdfToByteArray();

            bill = new PACFD.Rules.Billings();

            //using (table = bill.SelectByID(this.BillingID))
            using (table = dataset.Billings)
            {
                if (table.Count < 1)
                    throw new Exception("Billing table is empty");

                bill = null;

                if (this.Context.IsNull())
                    throw new Exception("Context is null");

                this.Context.Response.ClearContent();
                this.Context.Response.ClearHeaders();
                this.Context.Response.ContentType = "application/octet-stream";
                this.Context.Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename={0}.pdf",
                    table.Count < 1 ? "pdfreport" : table[0].Serial + "_" + table[0].Folio)
                    );
            }

            if (b.IsNull())
                return;

            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.End();
        }
        /// <summary>
        /// Get the stream from the data base if it fail read the default 
        /// template from the data base pos 1 if fail read the file template
        /// in the website path directory, if all fail the return null.
        /// </summary>
        /// <returns>If success read return a stream else null.</returns>
        protected System.IO.Stream GetPrintConfigStream()
        {
            string s;
            System.IO.MemoryStream stream = null;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable;
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;
            PACFD.Rules.PrintTemplates p = new PACFD.Rules.PrintTemplates();
            System.Text.UTF8Encoding ut8;
            byte[] b;

            billtable = billing.SelectByID(this.BillingID);
            billing = null;

            if (billtable.Count < 1)
            {
                billtable.Dispose();
                billtable = null;
                return null;
            }

            table = p.SelectByID(billtable[0].PrintTemplateID);
            p = null;
            stream = new System.IO.MemoryStream();

            if (table.Count < 1)
            {
                if (this.ElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
                    table = p.SelectByID(1); // <---------- default ID PrintTemplate  
                else
                    table = p.SelectByID(2); // <---------- default CBB ID PrintTemplate  
            }

            if (table.Count < 1)
            {
                table.Dispose();
                table = null;
                s = this.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ?
                        PACFD.Rules.PrintTemplates.GetStringDefaultCBB() :
                            this.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD ?
                                PACFD.Rules.PrintTemplates.GetStringDefaultCFD() :
                                    this.ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ?
                                        PACFD.Rules.PrintTemplates.GetStringDefaultCFDI() :
                                            string.Empty;
            }
            else
            {
                s = table[0].Text;
                table.Dispose();
                table = null;
            }

            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the posistion
            ut8 = new System.Text.UTF8Encoding(); //avoid text without UTF8 Encoding format
            b = ut8.GetBytes(s); //temporal bytes array nedeed
            stream.Write(b, 0, b.Length); //write array
            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the position

            return stream;
        }
    }
}
