﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings.XML
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void PrintTemplateApplyChangesEventHandler(object sender, PrintTemplateApplyEventArgs e);



    /// <summary>
    /// 
    /// </summary>
    public partial class PrintTemplateEditor : PACFDManager.BaseUserControl
    {
        public event PrintTemplateApplyChangesEventHandler ApplyChanges;


        public PrintTemplateEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return PrintTemplateEditorMode.Add;

                return o.GetType() == typeof(PrintTemplateEditorMode) ? (PrintTemplateEditorMode)o : PrintTemplateEditorMode.Add;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        public int PrintTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        protected Boolean IsBase
        {
            get
            {
                bool r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return false;

                if (!bool.TryParse(o.ToString(), out r))
                    return false;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        public string XML
        {
            get { return this.txtXML.Text; }
            set { this.txtXML.Text = value; }
        }
        public string Name
        {
            get { return this.txtName.Text; }
            set { this.txtName.Text = value; }
        }
        public bool IsXMLValid
        {
            get
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                System.IO.Stream stream;
                System.Text.UTF8Encoding ut8;
                byte[] b;

                try
                {
                    ut8 = new System.Text.UTF8Encoding();
                    b = ut8.GetBytes(this.XML);
                    stream = new System.IO.MemoryStream();
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                    stream.Write(b, 0, b.Length);
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                    b = null;
                    doc.Load(stream);
                }
                catch
                {
                    return false;
                }

                return true;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void CreatePrintTemplate()
        {
            PrintTemplateApplyEventArgs e;

            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table =
                new PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable();
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesRow row;

            if (this.txtName.Text.Trim().Length < 1)
                throw new Exception("Name text is not valid.");

            row = table.NewPrintTemplatesRow();
            row.Name = this.txtName.Text.Trim();
            row.Text = this.txtXML.Text.Trim();
            row.PrintTemplateID = this.EditorMode == PrintTemplateEditorMode.Add ? -1 : this.PrintTemplateID;
            row.IsBase = this.IsBase;
            row.ElectronicBillingType = (int)this.CurrentElectronicBillingType;
            row.BillerID = this.CurrentBillerID;

            table.AddPrintTemplatesRow(row);
            row.AcceptChanges();

            if (this.EditorMode == PrintTemplateEditorMode.Add)
                row.SetAdded();
            else
                row.SetModified();

            e = new PrintTemplateApplyEventArgs(table);

            this.OnApplyChanges(e);
        }

        protected virtual void OnApplyChanges(PrintTemplateApplyEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }

        public void DataBindPrintTemplate()
        {
            PACFD.Rules.PrintTemplates p;
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;

            if (this.PrintTemplateID < 1)
                return;

            p = new PACFD.Rules.PrintTemplates();
            table = p.SelectByID(this.PrintTemplateID);
            p = null;

            if (table.Count < 1)
            {
                table.Dispose();
                table = null;
                return;
            }

            this.txtName.Text = table[0].Name;
            this.txtXML.Text = table[0].Text;
            this.IsBase = table[0].IsBase;
            table.Dispose();
            table = null;
        }
    }
}