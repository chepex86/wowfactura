﻿using PACFD.Rules;
using PACFDManager.Receptors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class BillingsPaymentReceptorSelector : PACFDManager.BaseUserControl
    {
        public event BillingsPaymentReceptorSelectedEventHandler Selected;

        private int TemporalReceptorID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }


        private PACFD.DataAccess.ReceptorsDataSet.SearchByFilterDataTable SearchReceptorTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.ReceptorsDataSet.SearchByFilterDataTable))
                    return null;

                return (PACFD.DataAccess.ReceptorsDataSet.SearchByFilterDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Receptors receptor;

            if (this.IsPostBack)
                return;

            receptor = new PACFD.Rules.Receptors();
            this.SearchReceptorTable = receptor.SelectByRFCName(this.CurrentBillerID, null, null);
            this.lblNumberReceptors.Text = this.SearchReceptorTable.Rows.Count.ToString();
            this.gdvReceptors.DataSource = this.SearchReceptorTable; //receptor.SelectByBillersID(this.CurrentBillerID);
            this.gdvReceptors.DataBind();
        }

        protected virtual void OnSelected(BillingsPaymentReceptorSelectorEventArgs e)
        {
            if (!this.Selected.IsNull())
                this.Selected(this, e);
        }

        protected void ReceptorsSearch1_Search(object sender, ReceptorsSearchEventArgs e)
        {
            PACFD.Rules.Receptors receptors = new PACFD.Rules.Receptors();

            this.SearchReceptorTable = receptors.SelectByRFCName(this.CurrentBillerID, e.RFC, e.Name);
            this.lblNumberReceptors.Text = this.SearchReceptorTable.Rows.Count.ToString();
            this.gdvReceptors.DataSource = this.SearchReceptorTable;
            this.gdvReceptors.DataBind();
            receptors = null;
        }


        protected void ImageButton_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            int r;

            if (b.IsNull())
                return;

            if (!int.TryParse(b.CommandArgument, out r))
                return;

            this.TemporalReceptorID = r;

            switch (b.ID)
            {
                case "imbDetails":
                    BillingsPaymentReceptorSelectorEventArgs a = new BillingsPaymentReceptorSelectorEventArgs(this.TemporalReceptorID);
                    this.OnSelected(a);
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            switch (e.CommandArguments)
            {
                case "delete":
                    receptor = new PACFD.Rules.Receptors();
                    table = receptor.SelectByID(this.TemporalReceptorID);

                    if (table.Count < 1)
                        return;

                    String name = table[0].Name;

                    table[0].Delete();

                    if (!receptor.Update(table))
                    {
                        receptor = null;
                        break;
                    }

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino el cliente {1} del sistema.",
                        this.UserName, name), this.CurrentBillerID, null, null);
                    #endregion

                    receptor = null;
                    this.Response.Redirect(typeof(ReceptorsList).Name + ".aspx");
                    break;
            }


        }

        protected void gdvReceptors_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvReceptors.PageIndex = e.NewPageIndex;
            this.gdvReceptors.DataSource = this.SearchReceptorTable;
            this.gdvReceptors.DataBind();
        }
    }
}