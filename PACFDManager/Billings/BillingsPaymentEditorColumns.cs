﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsPaymentEditorColumns
    {
        public static string UUID { get { return "UUID"; } }
        public static string Total { get { return "Total"; } }
        public static string BillingDate { get { return "BillingDate"; } }
        public static string BillingID { get { return "BillingID"; } }

        public static System.Data.DataColumn[] GetColumns()
        {
            System.Data.DataColumn[] col;

            col = new System.Data.DataColumn[4];
            col[0] = new System.Data.DataColumn(BillingsPaymentEditorColumns.UUID, typeof(string));
            col[1] = new System.Data.DataColumn(BillingsPaymentEditorColumns.Total, typeof(decimal));
            col[2] = new System.Data.DataColumn(BillingsPaymentEditorColumns.BillingDate, typeof(DateTime));
            col[3] = new System.Data.DataColumn(BillingsPaymentEditorColumns.BillingID, typeof(int));
            return col;
        }
    }
}