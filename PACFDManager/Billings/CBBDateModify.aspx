﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="CBBDateModify.aspx.cs" Inherits="PACFDManager.Billings.CBBDateModify" %>

<%@ PreviousPageType VirtualPath="~/Billings/BillingsList.aspx" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <link href="../Includes/jQuery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="../Includes/jQuery/css/jquery-ui-timepicker-addon.css" rel="stylesheet"
        type="text/css" />

    <script src="../Includes/jQuery/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>

    <script src="../Includes/jQuery/jquery-ui-timepicker-addon.js" type="text/javascript"></script>

    <script type="text/javascript">
        var jcCBBDateModify;
        function CBBDateModify() {
            var isPostBack = '<%=this.IsPostBack %>';
            this.setCalendar = function() { jQuery('[name$="txtNewDate"]').datetimepicker({ dateFormat: 'dd/mm/yy', ampm: true, timeFormat: 'hh:mm tt' }); }
        }
        jQuery(document).ready(function() {
            if (jcCBBDateModify == undefined && jcCBBDateModify == null) { jcCBBDateModify = new CBBDateModify(); }
            jcCBBDateModify.setCalendar();
            if (Sys != undefined && Sys != null) { Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() { jcCBBDateModify.setCalendar(); }); }
        });
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe70">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <label class="desc">
                                <h2>
                                    <asp:Label ID="lblDateEditor" runat="server" Text="Editar fecha" />
                                </h2>
                            </label>
                        </div>
                        <div>
                            <asp:DetailsView ID="dvBilling" runat="server" Height="50px" Width="125px" AutoGenerateRows="false">
                                <Fields>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>Fecha Actual:</b>
                                                    </td>
                                                    <td>
                                                        <b>Nueva Fecha:</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%#Eval("BillingDate")%>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtNewDate" runat="server" Text='<%#DateTime.Now.ToString("dd/MM/yyyy hh:mm tt").Replace(".", "") %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div style="border-bottom-width: 1px; border-bottom-style: solid;">
                                            </div>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>Serie - Folio:</b>
                                                    </td>
                                                    <td>
                                                        <b>Total:</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%#Eval("Serial").ToString() + "-" + Eval("Folio").ToString()%>
                                                    </td>
                                                    <td>
                                                        <%#string.Format("{0:c}", Eval("Total"))%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </div>
                        <ul>
                            <li class="buttons">
                                <asp:Button ID="btnAcept" runat="server" CssClass="Button" Text="Aceptar" OnClick="btnAcept_Click" />
                                <asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                    Text="Cancelar" PostBackUrl="~/Billings/BillingsList.aspx" />
                            </li>
                        </ul>
                    </div>
                </div>
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
