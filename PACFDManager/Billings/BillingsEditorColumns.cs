﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Class used as article row column layer.
    /// </summary>
    /// <remarks>
    /// Use static class instead a const variable, have a better performance over
    /// cross class usage, because only one static class of the same type is allowed 
    /// in all the project. Constants must autocopy to the class that use it.
    /// </remarks>
    public static class BillingsEditorColumns
    {
        /// <summary>
        /// Concept ID.
        /// </summary>
        public static string ArticleID { get { return "ArticleID"; } }
        /// <summary>
        /// Concept name.
        /// </summary>
        public static string Name { get { return "Name"; } }
        /// <summary>
        /// Concept count.
        /// </summary>
        public static string Count { get { return "Count"; } }
        /// <summary>
        /// Concept discount.
        /// </summary>
        public static string Discount { get { return "Discount"; } }
        /// <summary>
        /// Concept amount.
        /// </summary>
        public static string Amount { get { return "Amount"; } }
        /// <summary>
        /// Concept Taxe ID
        /// </summary>
        public static string TaxeID { get { return "TaxeID"; } }
        /// <summary>
        /// Concept details ID
        /// </summary>
        public static string BillingsDetailsID { get { return "BillingsDetailsID"; } }
        /// <summary>
        /// Apply taxes to concept.
        /// </summary>
        public static string ApplyTaxes { get { return "ApplyTaxes"; } }
        /// <summary>
        /// 
        /// </summary>
        public static string TaxRatePercentage { get { return "TaxRatePercentage"; } }
        /// <summary>
        /// Concept total (concept count * concept amount).
        /// </summary>
        public static string ImporteTotal { get { return "ImporteTotal"; } }
        /// <summary>
        /// Concept description.
        /// </summary>
        public static string Description { get { return "Description"; } }

        /// <summary>
        /// Generate an array of System.Data.DataColumn used in the Article (consep) table.
        /// </summary>
        /// <returns>Return a System.Data.DataColumn array.</returns>
        public static System.Data.DataColumn[] GetColumns()
        {
            System.Data.DataColumn[] col;

            col = new System.Data.DataColumn[11];
            col[0] = new System.Data.DataColumn(BillingsEditorColumns.ArticleID, typeof(int));
            col[1] = new System.Data.DataColumn(BillingsEditorColumns.Name, typeof(string));
            col[2] = new System.Data.DataColumn(BillingsEditorColumns.ApplyTaxes, typeof(bool));
            col[3] = new System.Data.DataColumn(BillingsEditorColumns.Count, typeof(decimal));
            col[4] = new System.Data.DataColumn(BillingsEditorColumns.Discount, typeof(decimal));
            col[5] = new System.Data.DataColumn(BillingsEditorColumns.Amount, typeof(decimal));
            col[6] = new System.Data.DataColumn(BillingsEditorColumns.TaxeID, typeof(int));
            col[7] = new System.Data.DataColumn(BillingsEditorColumns.BillingsDetailsID, typeof(int));
            col[8] = new System.Data.DataColumn(BillingsEditorColumns.TaxRatePercentage, typeof(decimal));
            col[9] = new System.Data.DataColumn(BillingsEditorColumns.ImporteTotal, typeof(decimal));
            col[10] = new System.Data.DataColumn(BillingsEditorColumns.Description, typeof(string));

            return col;
        }
    }
}
