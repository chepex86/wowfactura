﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Text;

namespace PACFDManager.Billings
{
    public partial class BillingsAdd : PACFDManager.BasePage
    {
        private const int VIEW_BILLING_EDITOR = 0;
        private const int VIEW_RECEPTOR = 1;
        private const int VIEW_CONCEPT = 2;
        private const int VIEW_SELECT_TAXTYPE = 3;
        private const string MESSAGE_REDIRECT = "1";
        private const string MESSAGE_ISPREBILLING = "MESSAGE_ISPREBILLING";
        private bool isPrebilling = false;

        /// <summary>
        /// Get a boolean value indicating when a serial is active or not.
        /// </summary>
        private bool IsSerialActive
        {
            get
            {
                PACFD.Rules.Series serie = new PACFD.Rules.Series();

                if (this.CurrentBillerID < 1)
                    return false;

                return serie.SerialGetActiveByIDBiller(this.CurrentBillerID, this.CurrentBranchID).Count > 0 ? true : false;
            }
        }
        /// <summary>
        /// Get a boolean value validating if the certification number is found and
        /// if there's a digital certification.
        /// </summary>
        public bool IsCertificateActive
        {
            get
            {
                PACFD.Rules.DigitalCertificates digital;
                PACFD.DataAccess.DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table;

                digital = new PACFD.Rules.DigitalCertificates();
                table = digital.GetActiveByBillerID(this.CurrentBillerID);
                digital = null;

                if (table.Count < 1)
                {
                    table.Dispose();
                    table = null;
                    return false;
                }

                if (table[0].IsCertificateNumberNull())
                {
                    table.Dispose();
                    table = null;
                    return false;
                }

                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsAvailableSerialFolio
        {
            get
            {
                PACFD.Rules.Series serial;
                PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table;

                serial = new PACFD.Rules.Series();
                table = serial.GetUnusedSerialByBillerID(this.CurrentBillerID, this.CurrentBranchID);
                serial = null;

                if (table.Count < 1)
                {
                    table.Dispose();
                    table = null;
                    return false;
                }

                table.Dispose();
                table = null;
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCertificateValid
        {
            get
            {
                PACFD.Rules.DigitalCertificates digital;
                PACFD.DataAccess.DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table;

                digital = new PACFD.Rules.DigitalCertificates();
                table = digital.GetActiveByBillerID(this.CurrentBillerID);
                digital = null;

                if (table.Count < 1)
                {
                    table.Dispose();
                    table = null;
                    return false;
                }

                if (table[0].CertificateValidityBegin <= DateTime.Now && DateTime.Now <= table[0].CertificateValidityEnd)
                {
                    table.Dispose();
                    table = null;
                    return true;
                }

                table.Dispose();
                table = null;
                return false;
            }
        }
        /// <summary>
        /// Get or set an ID of the template used to print.
        /// </summary>
        private int PrintTemplateID
        {
            get
            {
                object o;
                int result;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out result))
                    return 0;

                return result;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        /// <summary>
        /// Load of the page.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BillingsEditor1.EditorMode = BillingsEditorMode.Add;

            this.btnPreview.Attributes["onclick"] =
                string.Format("javascript:jcPACFD.jcBillingsAdd.OpenPreviewWindow(\"../Billings/XML/DownloadPage.aspx?type=preview&out=true&b={0}\");",
                    PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString()));

            if (this.IsPostBack || !this.ConfigurationValidation())
                return;


            ScriptManager m = ScriptManager.GetCurrent(this);
            m.Scripts.Add(new ScriptReference("~/Billings/BillingsAdd.js"));

            this.btnGenerateBilling.OnClientClick = "javascript:jcPACFD.jcBillingsAdd.onInitializeRequest();";

            this.BillingsEditor1.IsPreBilling = PrebillingType.None;
            this.lblTitle.Text = "Comprobante";
           
            if (!this.Request.QueryString["pre"].IsNull())
            {
                this.BillingsEditor1.IsPreBilling = this.Request.QueryString["pre"] == "1" ? PrebillingType.Prebilling :
                    this.BillingsEditor1.IsPreBilling = this.Request.QueryString["pre"] == "2" ? PrebillingType.PrebillingNotSerialFolio : PrebillingType.None;
                this.lblTitle.Text = this.Request.QueryString["pre"] == "1" ? "Precomprobante" :
                    this.Request.QueryString["pre"] == "2" ? "Precomprobante no foliado" : "Comprobante";
            }

            this.MultiView1.ActiveViewIndex = VIEW_SELECT_TAXTYPE;
            (new XML.DownloadPage()).PreviewDataSet = null;
            this.BillingsEditor1.BillerID = this.CurrentBillerID;
            this.btnCancelBilling.Visible =
            this.btnGenerateBilling.Visible = false;
        }
        /// <summary>
        /// Check if the needed configuration is all right
        /// </summary>
        /// <returns>If configuration is ok return true else false</returns>
        protected bool ConfigurationValidation()
        {
            bool flag = true;
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ul>");

            if (!this.IsSerialActive)
            {
                flag = false;
                sb.AppendLine("<li>No hay una <b>serie</b> activa.</li>");
            }

            if (!this.IsCertificateActive && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
            {
                flag = false;
                sb.AppendLine("<li>No hay <b>certificado</b> o no se encuentra el numero del certificado.</li>");
            }

            if (!this.IsAvailableSerialFolio)
            {
                flag = false;
                sb.AppendLine("<li>No hay <b>serie</b> o <b>folios</b> disponibles. O no hay una <b>serie activada.</b></li>");
            }

            if (!this.IsCertificateValid && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
            {
                flag = false;
                sb.AppendLine("<li>El <b>certificado</b> no esta disponible debido a que la fecha esta caducada.</li>");
            }

            if ((this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                && !(new PACFD.Rules.PacConfiguration()).IsActive(this.CurrentBillerID))
            {
                flag = false;
                sb.AppendLine("<li>El <b>Pac</b> no esta configurado.</li>");
            }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
            {
                using (PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable table = (new PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID))
                {
                    if (table.Count < 1 || table.Select("Active=1").Length < 1)
                    {
                        flag = false;
                        sb.AppendLine("<li>No hay <b>Lugar de expedición</b> activo.</li>");
                    } else
                    {

                        int i = 0;
                        foreach (var r in table)
                        {
                            if (r.IsZipCodeNull() || r.ZipCode == "")
                                i++;
                        }
                        if (i == table.Count)
                        {
                            flag = false;
                            sb.AppendLine("<li>No se ha configurado<b> Código Postal</b> a los lugares de expedición</li>");
                        }
                            
                    }

                }

                using (PACFD.DataAccess.BillersDataSet.BillersDataTable table = (new PACFD.Rules.Billers()).SelectByID(this.CurrentBillerID))
                {
                    if (table.Count < 1)
                    {
                        flag = false;
                        sb.AppendLine("<li>No hay listado de <b>Régimen Fiscal</b> activo.</li>");
                    }
                    else if (table[0].IsTaxSystemNull() ||table[0].TaxSystem == "")
                    {
                        flag = false;
                        sb.AppendLine("<li>No se ha configurado<b>Régimen Fiscal</b></li>");
                    }
                }
            }

            sb.AppendLine("</ul>");

            if (!flag)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_REDIRECT;
                this.WebMessageBox1.ShowMessage(
                    string.Format("Requesito(s) necesario(s) para crear un comprobante.<br /><br />{0}", sb.ToString()),
                        System.Drawing.Color.Green,
                        WebMessageBoxButtonType.Accept);
            }

            return flag;
        }
        protected void wmbMessage_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ISPREBILLING:
                case MESSAGE_REDIRECT:
                    this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
                    break;
            }
        }
        protected void wmbCancel_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
                Response.Redirect(typeof(PACFDManager.Billings.BillingsList).Name + ".aspx");
        }
        /// <summary>
        /// Call to this.BillingsEditor1.CreateBilling() to generate the bill.
        /// </summary>

       
        protected void btnGenerateBilling_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("-----------> " + DateTime.Now.ToString("hh:MM:ss") + " Generando evento CreateBilling [inicio de benchmark]");
                this.BillingsEditor1.CreateBilling();
                System.Diagnostics.Debug.WriteLine("----------->  " + DateTime.Now.ToString("hh:MM:ss") + " todo terminado [fin fr benchmark]");
            }
            catch (CantParseBillingDate ex)
            {
                this.WebMessageBox1.ShowMessage("La fecha del comprobante no es correcta.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                LogManager.WriteError(ex);
                return;
            }
            catch (NotAccountNumberFound ex)
            {
                this.WebMessageBox1.ShowMessage("Información adicional a forma de pago tiene un minimo de cuatro (4) caracteres.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                LogManager.WriteError(ex);
                return;
            }
            catch (Exception ex)
            {
                this.WebMessageBox1.ShowMessage("No se pudo generar el comprobante.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                LogManager.WriteError(ex);
                return;
            }

            if (this.isPrebilling)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ISPREBILLING;
                this.WebMessageBox1.ShowMessage("El comprobante no pudo ser sellado. El comprobante se a marcado como precomprobate."
                    + "Para sellarlo use el listado de comprobantes. Tiempo maximo para sellar comprobante 72 horas.",
                    System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            if (!this.Session["id"].IsNull())
                this.Response.Redirect(typeof(Billings.BillingsDetails).Name + ".aspx");
            else
                this.Response.Redirect(typeof(Billings.BillingsList).Name + ".aspx");
        }
        protected void btnCancelBilling_Click(object sender, EventArgs e)
        {
            this.wmbCancel.ShowMessage("Los datos no guardados se perderan.<br />¿Desea regresar a la lista de facturas?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void BillingTaxTypeSelector1_Selected(object sender, BillingTaxTypeSelectorEventArgs e)
        {
            this.MultiView1.ActiveViewIndex = VIEW_BILLING_EDITOR;
            this.BillingsEditor1.TaxTemplateID = e.TaxTemplateID;
            this.BillingsEditor1.TaxTemplateName = e.Name;
            this.BillingsEditor1.TaxTemplateType = e.BillingType?.Trim().ToLower() == "ingreso" ? "I" : "E"; // Chicanada aka no hay tiempo 
            this.lblBillingEditorTitle.Text = string.Format("{0} - {1}", e.Name, e.Description);
            this.PrintTemplateID = e.DataTable[0].PrintTemplateID;
        }
        protected void BillingTaxTypeSelector1_CancelSelection(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(Billings.BillingsList).Name + ".aspx");
        }

        protected void BillingsEditor1_CancelClick(object sender, EventArgs e)
        {
            //this.pnlReceptors.Visible = !(
            //this.pnlEditor.Visible =
            //this.BillingsEditor1.Visible = false;
            this.Response.Redirect("..\\Default.aspx");
        }
        protected void BillingsEditor1_ReceptorDataBind(object sender, EventArgs e)
        {
            this.btnCancelBilling.Visible =
            this.btnGenerateBilling.Visible = true;
        }
        protected void BillingsEditor1_ReceptorInvalidDataBind(object sender, EventArgs e)
        {
            this.btnCancelBilling.Visible =
            this.btnGenerateBilling.Visible = false;
        }
        protected void BillingsEditor1_GeneratedBilling(object sender, BillingsEditorEventArgs e)
        {
            PACFD.Rules.Billings bill;

            if (e.EditorMode != BillingsEditorMode.Add)
                return;

            System.Diagnostics.Debug.WriteLine("----------->  " + DateTime.Now.ToString("hh:MM:ss") + " Inicio del Update. BillingAdd.aspx");

            bill = new PACFD.Rules.Billings();

            if (!bill.Update(e.DataSet))
            {
                bill = null;
                throw new Exception(string.Format("Error inesperado, al tratar de crear una factura tipo {0} biller {1}.", (int)this.CurrentElectronicBillingType, this.CurrentBillerID));
            }

            System.Diagnostics.Debug.WriteLine("----------->  " + DateTime.Now.ToString("hh:MM:ss") + " Billing Update exitoso");

            if (this.BillingsEditor1.IsPreBilling == PrebillingType.None && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB
                && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.Indeterminate)
            {
                System.Diagnostics.Debug.WriteLine("----------->  " + DateTime.Now.ToString("hh:MM:ss") + " Es precomprobante, inicio de sellando");
                ErrorManager err = new ErrorManager();

                if (!bill.SealCFD(e.DataSet, ref err))
                {
                    this.isPrebilling = e.DataSet.Billings[0].PreBilling = true;
                    bill.Update(e.DataSet);

                }

                if (string.IsNullOrEmpty(e.DataSet.Billings[0].Seal) || string.IsNullOrEmpty(e.DataSet.Billings[0].OriginalString))
                {
                    //this.WebMessageBox1.CommandArguments = MESSAGE_REDIRECT;
                    //this.WebMessageBox1.ShowMessage("El sello o cadena original no se generarón correctamente. El comprobante será guardado como precomprobante.", true);

                    this.isPrebilling =
                        e.DataSet.Billings[0].PreBilling = true;
                    e.DataSet.Billings[0].OriginalString =
                    e.DataSet.Billings[0].Seal = string.Empty;
                    bill.Update(e.DataSet);
                    if (err != null)
                        this.WebMessageBox1.ShowMessage(string.Format("Error: {0}", err.Error.Message));
                    //throw new Exception("El sello o cadena original no se generarón correctamente. El comprobante será guardado como precomprobante.");
                }

                System.Diagnostics.Debug.WriteLine("----------->  " + DateTime.Now.ToString("hh:MM:ss") + " Sellado terminado");
            }

            bill = null;
            this.Session["id"] = e.DataSet.Billings[0].BillingID;
            (new XML.DownloadPage()).PreviewDataSet = null;
        }
        protected void BillingsEditor1_ConceptAfterAddToBill(object sender, BillingsEditorEventArgs e)
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            PACFD.DataAccess.BillingsDataSet ds;

            using (PACFDManager.Billings.XML.DownloadPage d = new PACFDManager.Billings.XML.DownloadPage())
            {
                ds = e.DataSet;
                bill.CalculationTaxesAndTotals(ref ds);
                d.PreviewDataSet = ds;
                d.PrintXML = (new PACFD.Rules.PrintTemplates()).GetPrintTemplateTextByID(this.PrintTemplateID);
                ds = null;
            }
        }
        protected void BillingsEditor1_AddNewReceptor(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = VIEW_RECEPTOR;
            this.ReceptorsEditor1.EditorMode = PACFDManager.Receptors.ReceptorsEditorMode.Add;
            this.ReceptorsEditor1.BillerID = this.CurrentBillerID;
            this.ReceptorsEditor1.Enabled = true;
        }
        protected void BillingsEditor1_AddNewConcept(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = VIEW_CONCEPT;
            this.ConceptsEditor1.EditorMode = PACFDManager.Concepts.ConceptsEditorMode.Add;
            this.ConceptsEditor1.AddMode = true;
        }
        protected void BillingsEditor1_ClientModify(object sender, Billings.BillingsClientEditEventArgs e)
        {
            this.MultiView1.ActiveViewIndex = VIEW_RECEPTOR;
            this.ReceptorsEditor1.EditorMode = PACFDManager.Receptors.ReceptorsEditorMode.Edit;
            this.ReceptorsEditor1.ReceptorID = e.ReceptorID;
            this.ReceptorsEditor1.BillerID = this.CurrentBillerID;
            this.ReceptorsEditor1.DataBindReceptorID();
        }

        protected void ReceptorsEditor1_ApplyChanges(object sender, PACFDManager.Receptors.ReceptorsEditorEventArgs e)
        {
            PACFD.Rules.Receptors receptos;

            //if (e.EditorMode != PACFDManager.Receptors.ReceptorsEditorMode.Add)
            //    return;

            receptos = new PACFD.Rules.Receptors();

            if (!receptos.Update(e.Table))
            {
                if (e.EditorMode == PACFDManager.Receptors.ReceptorsEditorMode.Add)
                    this.WebMessageBox1.ShowMessage("No se pudo registrar cliente.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                else
                    this.WebMessageBox1.ShowMessage("No se pudo modificar al cliente.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
            }

            receptos = null;

            this.ReceptorsEditor1.ReceptorsInformation.Colony =
                this.ReceptorsEditor1.ReceptorsInformation.Email =
                this.ReceptorsEditor1.ReceptorsInformation.ExternalNumber =
                this.ReceptorsEditor1.ReceptorsInformation.InternalNumber =
                this.ReceptorsEditor1.ReceptorsInformation.Name =
                this.ReceptorsEditor1.ReceptorsInformation.Phone =
                this.ReceptorsEditor1.ReceptorsInformation.Reference =
                this.ReceptorsEditor1.ReceptorsInformation.RFC =
            this.ReceptorsEditor1.ReceptorsInformation.Address = string.Empty;
            this.ReceptorsEditor1.ReceptorsInformation.Zipcode = "1000";

            if (e.EditorMode == PACFDManager.Receptors.ReceptorsEditorMode.Add)
            {
                this.BillingsEditor1.DataBindBillerID();
            }

            this.ReceptorsEditor1_CancelClick(sender, EventArgs.Empty);

            if (e.EditorMode == PACFDManager.Receptors.ReceptorsEditorMode.Add)
            {
                if (this.BillingsEditor1.BillingInformation.IsReceptorDropDownVisible)
                    this.BillingsEditor1.BillingInformation.SetReceptorSelected(e.Table[0].ReceptorID);
                else
                    this.BillingsEditor1.BillingInformation.SetReceptorSelected(e.Table[0].Name);
            }
            else if (e.EditorMode == PACFDManager.Receptors.ReceptorsEditorMode.Edit)
            {
                //this.BillingsEditor1.DataBind();
                this.BillingsEditor1.DataBindReceptorID();
            }

            this.BillingsEditor1.InvokeConceptAddedToBill(this.BillingsEditor1.BillingInformation.ArticlesGrid.Rows.Count > 0 ? true : false);
        }
        protected void ReceptorsEditor1_CancelClick(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = VIEW_BILLING_EDITOR;
        }

        protected void ConceptsEditor1_ApplyChanges(object sender, PACFDManager.Concepts.ConceptsEventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            PACFD.DataAccess.ConceptsDataSet ds = new PACFD.DataAccess.ConceptsDataSet();
            PACFD.DataAccess.ConceptsDataSet.ConceptsRow drConcept = ds.Concepts.NewConceptsRow();

            drConcept.Code = e.Code;
            drConcept.BillerID = e.BillerID;
            drConcept.UnitType = e.Type;
            drConcept.Description = e.Description;
            drConcept.UnitPrice = Convert.ToDecimal(e.UnitPrice);
            drConcept.TaxRatePercentage = Convert.ToDecimal(e.Tax);
            drConcept.Active = true;
            drConcept.TaxType = 0;
            drConcept.AppliesTax = e.AppliesTax;
            drConcept.Account = e.Account;
            drConcept.ClavProdServ = e.Prod;
            ds.Concepts.AddConceptsRow(drConcept);

            if (!concept.Update(ds.Concepts))
                this.WebMessageBox1.ShowMessage("No se pudo agregar concepto.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);

            this.ConceptsEditor1.CleaningForm = true;
            this.ReceptorsEditor1_CancelClick(sender, EventArgs.Empty);
            this.BillingsEditor1.BillingInformation.SetConceptSelected(e.Description);
        }
    }
}