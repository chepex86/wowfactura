﻿<%@ Page Title="Detalles del comprobante." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillingsPaymentDetails.aspx.cs" Inherits="PACFDManager.Billings.BillingsPaymentDetails" %>

<%@ Register Src="BillingsPaymentEditor.ascx" TagName="BillingsPaymentEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div style="margin: 0;">
        <ul class="ticket-detail-nav">
            <li><a class="pdf-icon" title="Descargar PDF" rel="_self" href='<%="XML/DownloadPaymentPage.aspx?type=pdf&b=" + this.EncryptBillingAndBillerID("b") + "&a=" + this.EncryptBillingAndBillerID("b") + "o=false"%>'>
                Descargar PDF </a></li>
            <li id="aXMLDownload" runat="server"><a class="xml-icon" title="Descargar XML" rel="_self"
                href='<%="XML/DownloadPaymentPage.aspx?type=xml&b=" + this.EncryptBillingAndBillerID("b") + "&a=" + this.EncryptBillingAndBillerID("b") + "o=false"%>'>
                Descargar XML </a></li>
            <li><a class="email-icon" title="Enviar correo electrónico" rel="_self" href="SendBillingMail.aspx">
                Enviar correo electrónico </a></li>
        </ul>
        <h2>
            <asp:Label ID="lblTitle" runat="server" Text=" Detalle del comprobante" />
        </h2>
        <div style="clear: both">
        </div>
        <%--   <a title='Descarga PDF' rel="_self" href='<%="XML/DownloadPage.aspx?type=pdf&b=" + this.EncryptBillingAndBillerID("b") + "&a=" + this.EncryptBillingAndBillerID("b") + "o=false"%>'>
            Descarga PDF
            <img alt="Descarga PDF" title="Descarga PDF" src="../Includes/Images/png/pdf.png" />
        </a>&nbsp; <a title='Descarga XML' rel="_self" href='<%="XML/DownloadPage.aspx?type=xml&b=" + this.EncryptBillingAndBillerID("b") + "&a=" + this.EncryptBillingAndBillerID("b") + "o=false"%>'>
            Descarga XML
            <img alt="Descarga XML" title="Descarga XML" src="../Includes/Images/png/xml.png" />
        </a>&nbsp; <a title='Enviar correo email.' rel="_self" href='SendBillingMail.aspx'>Enviar
            correo electronico
            <img alt="Enviar correo electronico." title="Enviar correo electronico." src="../Includes/Images/png/sendMail.png" />
        </a>--%>
        <uc1:BillingsPaymentEditor ID="BillingsEditor1" runat="server" OnDataBindBillingID="BillingsEditor1_DataBindBillingID" />
        <div class="facturaDivMain" id="divSeal" runat="server" visible="true">
            <div class="facturaDivContent">
                <div class="title">
                    <asp:Label ID="lblOriginalStringTitle" runat="server" Text="Cadena original" />
                </div>
                <asp:Label ID="lblOriginalStringText" runat="server" Text="" Width="100%" />
            </div>
            <div class="facturaDivContent">
                <div class="title">
                    <asp:Label ID="lblSealTitle" runat="server" Text="Sello." />
                </div>
                <asp:Label ID="lblSealText" runat="server" Text="" Width="100%" />
            </div>
            <div id="divSATOriginalString" class="facturaDivContent" runat="server">
                <div class="title">
                    <asp:Label ID="lblSATOriginalStringTitle" runat="server" Text="Cadena original del complemento de certificación del SAT." />
                </div>
                <asp:Label ID="lblSATOriginalString" runat="server" Text="" Width="100%" />
            </div>
            <div id="divSATSeal" class="facturaDivContent" runat="server">
                <div class="title">
                    <asp:Label ID="lblSATSealTitle" runat="server" Text="Sello digital del SAT." />
                </div>
                <asp:Label ID="lblSATSeal" runat="server" Text="" Width="100%" />
            </div>
        </div>
        <div id="divQr" runat="server" visible="false" class="facturaDivMain">
            <div class="facturaDivContent">
                <table cellpadding="0px" cellspacing="0px" style="border-spacing: 0px; width: 450px;
                    float: left">
                    <tr valign="top">
                        <td>
                            <asp:Image ID="imgQr" runat="server" />
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="border-spacing: 0px; width: 200px"
                                id="tableQrData" runat="server" >
                                <tr>
                                    <td>
                                        RFC:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrRFC" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Numero de aprovación:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrAprovalNumber" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Folio:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrFolio" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Serie:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrSerial" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fecha de Expedición:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrExpeditionDate" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fecha de Expiración:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblQrExpirationDate" runat="server" Text="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="text-align: center">
        <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        <asp:Button ID="btnAcept" runat="server" Text="Aceptar" class="Button" OnClick="btnAcept_Click" />
    </div>
</asp:Content>
