﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Drawing;

namespace PACFDManager.Billings
{
    public partial class BillingsDetails : PACFDManager.BasePage
    {
        protected int BillingID
        {
            get
            {
                object o = this.Session["id"];
                int r;
                return o.IsNull() || !int.TryParse(o.ToString(), out r) ? 0 : r;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BillingsEditor1.Enabled = false;

            switch (this.CurrentElectronicBillingType)
            {
                default:
                case PACFD.Rules.ElectronicBillingType.Indeterminate:
                case PACFD.Rules.ElectronicBillingType.CFD:
                    break;
                case PACFD.Rules.ElectronicBillingType.CFDI:
                    break;
                case PACFD.Rules.ElectronicBillingType.CBB:
                    this.aXMLDownload.Visible = false;
                    break;
            }

            if (this.IsPostBack)
                return;

            this.divQr.Visible =
                this.divSATOriginalString.Visible =
                this.divSATSeal.Visible = false;
            this.BillingsEditor1.BillingID = this.BillingID;
            this.BillingsEditor1.EditorMode = BillingsEditorMode.View;
            this.BillingsEditor1.DataBindFromBillingID();
        }

        protected void BillingsEditor1_DataBindBillingID(object sender, BillingsEditorEventArgs e)
        {
            this.lblOriginalStringText.Text = InsertJump(e.DataSet.Billings[0].OriginalString, 110);
            this.lblSealText.Text = InsertJump(e.DataSet.Billings[0].Seal, 110);

            if (this.BillingsEditor1.IsPreBilling == PrebillingType.Prebilling)
            { this.lblTitle.Text = "Detalles de precomprobante"; }
            else if (this.BillingsEditor1.IsPreBilling == PrebillingType.PrebillingNotSerialFolio)
            { this.lblTitle.Text = "Detalles de precomprobante no foliado"; }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
            {
                this.DataBindQr();
                this.divQr.Visible = true;
                this.divSeal.Visible = false;
                //this.aPDF.Visible = false;
                //this.aMail.Visible = false;
            }
            else if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
            {
                //tableQrData.Visible = false;
                this.tableQrData.Visible = false;
                this.imgQr.ImageUrl = "~/" + typeof(ImageHandler).Name + ".ashx?type=3&BillerID=" + this.BillingID;
                this.divQr.Visible =
                    this.divSATOriginalString.Visible =
                    this.divSATSeal.Visible = true;
                this.lblSATOriginalString.Text = e.DataSet.Billings[0].IsOrignalStringSatNull() ? string.Empty :
                    InsertJump(e.DataSet.Billings[0].OrignalStringSat, 110);
                this.lblSATSeal.Text = e.DataSet.Billings[0].IsSealSatNull() ? string.Empty :
                    InsertJump(e.DataSet.Billings[0].SealSat, 110);
            }
        }
        protected void DataBindQr()
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table;
            PACFD.Rules.QRCodeLib.QRCodeDecoder qr = new PACFD.Rules.QRCodeLib.QRCodeDecoder();
            System.Drawing.Bitmap bimap;
            System.IO.MemoryStream stream;
            string s;
            string[] split;

            this.imgQr.ImageUrl = "~/" + typeof(ImageHandler).Name + ".ashx?type=3&BillerID=" + this.BillingID;

            using (table = bill.SelectByID(this.BillingID))
            {
                if (table.Count < 1)
                {
                    this.WebMessageBox1.ShowMessage("Comprobante no encontrado.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }

                if (table[0].IsQrImageNull())
                {
                    this.WebMessageBox1.ShowMessage("Qr no generado, debido a que es un precomprobante.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }

                stream = new System.IO.MemoryStream(table[0].QrImage);
            }

            try
            {
                bimap = new System.Drawing.Bitmap(stream);
                bimap = this.ValidateQrImageSize(bimap);
                s = qr.decode(new PACFD.Rules.QRCodeLib.data.QRCodeBitmapImage(bimap));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage("No se pudo cargar el CBB (Qr).", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            split = s.Split('|');

            if (split.Length < 2)
            {
                this.WebMessageBox1.ShowMessage("Los datos en el CBB (Qr) son incorrectos.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            this.lblQrRFC.Text = split[1];
            this.lblQrAprovalNumber.Text = split[2];
            this.lblQrFolio.Text = string.Format("{0} - {1}", split[3], split[4]);
            this.lblQrSerial.Text = split.Length < 5 ? string.Format("Error leyendo CBB") : split[5];
            this.lblQrExpeditionDate.Text = split[6];
            this.lblQrExpirationDate.Text = split[7];
        }
        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {

        }
        protected string InsertJump(string cad, int max)
        {
            string result = string.Empty;

            int ant = 0;
            int rest = 0;

            while (ant <= cad.Length)
            {
                rest = cad.Length - (ant);

                if (rest <= 0)
                    break;

                if (rest > max)
                    rest = max;

                result += string.Format("{0}<br />", cad.Substring(ant, rest));
                ant += max;
            }

            return result;
        }
        protected string EncryptBillingAndBillerID(string id)
        {
            return PACFD.Common.Cryptography.EncryptUnivisitString(id == "b" ? this.BillingID.ToString() : this.CurrentBillerID.ToString());
        }
        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
        }
        private System.Drawing.Bitmap ValidateQrImageSize(System.Drawing.Bitmap qr)
        {
            System.Drawing.Bitmap result = new Bitmap(250, 250);

            using (Graphics g = Graphics.FromImage(result))
            {
                g.Clear(Color.White);
                g.DrawImage(qr, 0, 0, 250, 250);
            }

            return result;
        }
    }
}