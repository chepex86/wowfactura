﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// 
    /// </summary>
    public class NotValidDigitalCertificatesException : Exception
    {
        public int BillerID { get; private set; }
        public int DigitalCertificateID { get; private set; }




        public NotValidDigitalCertificatesException(string message, int billerid) :
            this(message, billerid, 0) { }

        public NotValidDigitalCertificatesException(string message, int billerid, int digitalid)
            : base(message)
        {
            this.BillerID = billerid;
            this.DigitalCertificateID = digitalid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotBillerFoundException : Exception
    {
        public int BillerID { get; private set; }

        public NotBillerFoundException(string message, int billerid)
            : base(message)
        {
            this.BillerID = billerid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotReceptorFoundException : Exception
    {
        public int ReceptorID { get; private set; }

        public NotReceptorFoundException(string message, int receptorid)
            : base(message)
        {
            this.ReceptorID = receptorid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotReceptorConfiguredException : Exception
    {
        public int ReceptorID { get; private set; }

        public NotReceptorConfiguredException(string message, int receptorid)
            : base(message)
        {
            this.ReceptorID = receptorid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotConceptFoundException : Exception
    {
        public int ConceptID { get; private set; }

        public NotConceptFoundException(string message, int receptorid)
            : base(message)
        {
            this.ConceptID = receptorid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotConceptConfiguredException : Exception
    {
        public int ConceptID { get; private set; }

        public NotConceptConfiguredException(string message, int receptorid)
            : base(message)
        {
            this.ConceptID = receptorid;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotValidConceptIDException : Exception
    {
        public NotValidConceptIDException(string message)
            : base(message)
        {
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotValidSeriesOrFolios : Exception
    {
        public NotValidSeriesOrFolios(string message)
            :base(message)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NotPlaceDispatchSelected : Exception
    {
        public NotPlaceDispatchSelected(string message)
            : base(message)
        {
        }
    }

    public class CantParseBillingDate : Exception
    {
        public CantParseBillingDate(string message)
            : base(message)
        {
        }
    }

    public class NotAccountNumberFound : Exception
    {
        public NotAccountNumberFound(string message)
            : base(message)
        {
        }
    }
    public class NotGlobalInvoiceData : Exception
    {
        public NotGlobalInvoiceData(string message)
            : base(message)
        {
        }
    }
}
