﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsPaymentReceptorSelectorEventArgs : EventArgs
    {
        /// <summary>
        /// Get the ID of the tax template selected
        /// </summary>
        public int ReceptorID { get; set; }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        /// <param name="taxtempid">TaxTemplate ID selected.</param>
        /// <param name="name">Name of the Billing Type name.</param>
        /// <param name="billingtype">Internal Biilling Type.</param>
        public BillingsPaymentReceptorSelectorEventArgs(int receptorID)
        {
            this.ReceptorID = receptorID;
        }
    }
}