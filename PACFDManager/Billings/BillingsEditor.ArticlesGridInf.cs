﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    partial class BillingsEditor
    {
        /// <summary>
        /// Class used to group the article data grid view control main properties an methods.
        /// </summary>
        public class ArticlesGridInf
        {
            /// <summary>
            /// Get a BillingsEditor parent owner of the class.
            /// </summary>
            protected BillingsEditor Parent { get; set; }
            /// <summary>
            /// Get a System.Data.DataRowCollection collection with the rows in the data grid view.
            /// </summary>
            public System.Data.DataRowCollection Rows { get { return this.Parent.DataTableGridView.Rows; } }
            
            /// <summary>
            /// Add a row to articles data grid view.
            /// </summary>
            /// <param name="row">System.Data.DataRow row to add.</param>
            public virtual void AddRow(System.Data.DataRow row)
            {
                this.Parent.DataTableGridView.Rows.Add(row);
            }
            /// <summary>
            /// Create a new System.Data.DataRow from articles data grid.
            /// </summary>
            /// <returns>Return a System.Data.DataRow row.</returns>
            public System.Data.DataRow NewRow()
            {
                return this.Parent.OnCreateNewRow();
            }
            /// <summary>
            /// Data bind the data table used to store the articles.
            /// </summary>
            public void DataBind()
            {
                this.Parent.gdvArticles.DataSource = this.Parent.DataTableGridView;
                this.Parent.gdvArticles.DataBind();
            }
            /// <summary>
            /// Get a decimal value with the total cost of articles.
            /// </summary>
            /// <returns>Return a decimal value.</returns>
            public decimal Total()
            {
                decimal total = 0.0m;

                foreach (System.Data.DataRow row in this.Rows)
                {
                    total += row[BillingsEditorColumns.Amount].ToString().ToDecimal();
                }

                return total;
            }
            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Owner of the class.</param>
            public ArticlesGridInf(BillingsEditor owner)
            {
                this.Parent = owner;
            }
        }
    }
}
