﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Class that represent the articles selected from BillingsArticleSearch class.
    /// </summary>
    public class BillingsArticleSearchSelected
    {
        /// <summary>
        /// Get an integer with the ID of the article selected.
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Get a decimal with the cost of the article.
        /// </summary>
        public decimal Cost { get; private set; }
        /// <summary>
        /// Get a string with the name of the article selected.
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Get an integer with the amount of articles.
        /// </summary>
        public decimal Amount { get; private set; }

        public bool ApplyTaxes { get; private set; }

        public bool IncludedTaxes { get; private set; }



        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="id">Concept (article) ID.</param>
        /// <param name="name">Name of the concept (article).</param>
        /// <param name="cost">Cost of the concept (article).</param>
        /// <param name="amount">Amount of concepts (articles).</param>
        /// <param name="apptax">If apply taxes.</param>
        public BillingsArticleSearchSelected(int id, string name, decimal cost, decimal amount, bool apptax,bool inctax)
        {
            this.ID = id;
            this.Name = name;
            this.Cost = cost;
            this.Amount = amount;
            this.ApplyTaxes = apptax;
            this.IncludedTaxes = inctax;
        }
    }
}
