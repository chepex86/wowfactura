﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class BillingPDFXMLList : System.Web.UI.Page
    {
        private string billerRFC;

        public int GetCurrentBillerID
        {
            get
            {
                int r;
                string s = this.Request.QueryString["id"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                if (!int.TryParse(s, out r))
                    return 0;

                return r;
            }
        }

        public int CurrentBillerID
        {
            get
            {
                try
                {
                    return (int)HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")];
                }
                catch { }
                return -1;
            }
        }
        protected PACFD.Rules.ElectronicBillingType ElectronicBillingType { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;

            this.divListado.Visible =
                !(this.divQuestion.Visible = this.GetCurrentBillerID < 1 ? true : false);

            if (this.GetCurrentBillerID < 1)
                return;

            using (table = biller.SelectByID(this.GetCurrentBillerID))
            {

                if (table.Count < 1)
                    return;

                this.billerRFC = table[0].RFC;
                this.ElectronicBillingType = table[0].ElectronicBillingType.ToElectronicBillingType();
            }

            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();

            this.gdvFiles.DataSource = bill.Search(
                PACFD.Rules.Billings.SearchPetitionFromFlag.OpenWebForm,                 
                this.GetCurrentBillerID,
                null,
                null,
                null,
                (DateTime?)null,
                (Boolean?)null,
                (bool?)null
                , null
                , null
                , null
                , null
                , null
                );
            this.gdvFiles.DataBind();
        }

        protected string DataBindEncryptBillingAndBillerID(string id)
        {
            return PACFD.Common.Cryptography.EncryptUnivisitString(id);
        }

        protected bool IsFormatVisible(string value)
        {
            if (value == "0" && this.ElectronicBillingType != PACFD.Rules.ElectronicBillingType.Indeterminate)
                return true;
            else if (value == "1" && this.ElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB &&
                this.ElectronicBillingType != PACFD.Rules.ElectronicBillingType.Indeterminate)
                return true;

            return false;
        }

        protected string GetBillerRFC()
        {
            return this.billerRFC;
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(
                string.Format("{0}.aspx?id={1}",
                typeof(BillingPDFXMLList).Name,
                PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString())));
        }
    }
}
