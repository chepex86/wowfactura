﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    ValidateRequest="false" CodeBehind="AddendumModify.aspx.cs" Inherits="PACFDManager.Billings.Addendum.AddendumModify" %>

<%@ Register Src="AddendumEditor.ascx" TagName="AddendumEditor" TagPrefix="uc1" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container" class="wframe80">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <label class="desc">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Modificar configuración de addenda." />
                        </h2>
                    </label>
                </div>
                <uc1:AddendumEditor ID="AddendumEditor1" runat="server" OnApplyChanges="AddendumEditor1_ApplyChanges" />
                <br />
                <asp:Button ID="btnAdd" runat="server" Text="Aceptar" OnClick="btnAdd_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CausesValidation="false"
                    PostBackUrl="~/Billings/Addendum/AddendumList.aspx" />
                <br />
            </div>
        </div>
    </div>
    <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
