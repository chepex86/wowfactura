﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings.Addendum
{
    public partial class AddendumEditor : PACFDManager.BaseUserControl
    {
        public event ApplyChangesEventHandler ApplyChanges;

        public AddendumEditorInformation AddendumInformation { get { return new AddendumEditorInformation(this); } }
        public int AddendumID
        {
            get
            {
                object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                return o.IsNull() || o.GetType() != typeof(int) ? 0 : (int)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        public AddendumEditMode EditorMode
        {
            get
            {
                object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                return o.IsNull() || o.GetType() != typeof(AddendumEditMode) ? 0 : (AddendumEditMode)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void InvokeApplyChanges()
        {
            PACFD.DataAccess.AddendumDataSet.AddendumDataTable table = new PACFD.DataAccess.AddendumDataSet.AddendumDataTable();
            AddendumEditorApplyChangesEventArgs e;
            PACFD.DataAccess.AddendumDataSet.AddendumRow row;

            row = table.NewAddendumRow();
            row.BeginEdit();
            row.AddendumID = this.EditorMode == AddendumEditMode.Edit ? this.AddendumID : 0;
            row.Description = this.AddendumInformation.Description;
            row.Name = this.AddendumInformation.Name;
            row.Xsd = this.AddendumInformation.Xsd;
            row.EndEdit();
            table.AddAddendumRow(row);
            row.AcceptChanges();

            if (this.EditorMode == AddendumEditMode.Add)
            { row.SetAdded(); }
            else if (this.EditorMode == AddendumEditMode.Edit)
            { row.SetModified(); }

            e = new AddendumEditorApplyChangesEventArgs(this.EditorMode, table);
            this.OnApplyChanges(e);
        }
        protected virtual void OnApplyChanges(AddendumEditorApplyChangesEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
            { this.ApplyChanges(this, e); }
        }
        public override void DataBind()
        {
            PACFD.DataAccess.AddendumDataSet.AddendumDataTable table;
            base.DataBind();

            if (this.AddendumID < 1)
            { return; }

            using(table = (new PACFD.Rules.Addendum()).SelectByID(this.AddendumID))
            {
                if (table.Count < 1)
                { return; }

                this.AddendumInformation.Description = table[0].Description;
                this.AddendumInformation.Name = table[0].Name;
                this.AddendumInformation.Xsd = table[0].Xsd;
            }
        }
        public class AddendumEditorInformation
        {
            public AddendumEditorInformation(AddendumEditor owner) { this.Parent = owner; }

            private AddendumEditor Parent { get; set; }
            public int AddendumID { get { return this.Parent.AddendumID; } set { this.Parent.AddendumID = value; } }
            public string Xsd { get { return this.Parent.txtXsd.Text; } set { this.Parent.txtXsd.Text = value; } }
            public string Name { get { return this.Parent.txtName.Text; } set { this.Parent.txtName.Text = value; } }
            public string Description { get { return this.Parent.txtDescription.Text; } set { this.Parent.txtDescription.Text = value; } }
        }
    }

    public enum AddendumEditMode
    {
        Add = 0,
        Edit = 1,
        View = 2,
    }

    public delegate void ApplyChangesEventHandler(object sender, AddendumEditorApplyChangesEventArgs e);

    public class AddendumEditorApplyChangesEventArgs : EventArgs
    {
        public AddendumEditMode EditorMode { get; private set; }
        public PACFD.DataAccess.AddendumDataSet.AddendumDataTable Table { get; private set; }

        public AddendumEditorApplyChangesEventArgs(AddendumEditMode editormode, PACFD.DataAccess.AddendumDataSet.AddendumDataTable table)
        {
            this.EditorMode = editormode;
            this.Table = table;
        }
    }
}