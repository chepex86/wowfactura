﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace PACFDManager.Billings.Addendum
{
    public partial class AddendumApply : BasePage
    {
        const string MESSAGE_ADDENDUM_APPLY = "MESSAGE_ADDENDUM_APPLY";
        const string MESSAGE_ADDENDUM_READ_ERROR = "MESSAGE_ADDENDUM_READ_ERROR";
        const string MESSAGE_ADDENDUM_ALREADY_EXCIST = "MESSAGE_ADDENDUM_ALREADY_EXCIST";
        const string MESSAGE_ADDENDUM_ERROR = "MESSAGE_ADDENDUM_ERROR";

        private int BillingID
        {
            get
            {
                int i = 0;
                object o = this.Session["id"];
                return o.IsNull() || !int.TryParse(o.ToString(), out i) ? 0 : i;
            }
        }
        private int SelectedAddendumID
        {
            get
            {
                //object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                int i = 0;
                //return o.IsNull() || !int.TryParse(o.ToString(), out i) ? 0 : i;
                object o = this.Request.QueryString["adden"];
                return o.IsNull() || !int.TryParse(o.ToString(), out i) ? 0 : i;
            }
            //set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.SelectedAddendumID > 0)
            { this.LoadDinamycControlls(); }

            if (this.IsPostBack | this.BillingID < 1)
            { return; }

            this.gdvAddendum.DataSource = (new PACFD.Rules.Addendum()).SelectAll();
            this.gdvAddendum.DataBind();

            using (PACFD.DataAccess.BillingsDataSet dataset = (new PACFD.Rules.Billings()).GetFullBilling(this.BillingID))
            {
                this.lblBillerName.Text = dataset.BillingsBillers[0].BillerName;
                this.lblReceptorName.Text = dataset.BillingsReceptors[0].ReceptorName;
                this.lblSerialFolio.Text = string.Format("{0}{1}", string.IsNullOrEmpty(dataset.Billings[0].Serial) ? "" : dataset.Billings[0].Serial + "-",
                    dataset.Billings[0].Folio);
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ADDENDUM_READ_ERROR:
                    break;
                case MESSAGE_ADDENDUM_APPLY:
                default:
                    this.Response.Redirect("~/" + typeof(Default).Name + ".aspx");
                    return;
            }
        }

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int i;
            ImageButton b = sender as ImageButton;

            if (b.IsNull() || !int.TryParse(b.CommandArgument, out i) || i < 1)
            { return; }

            this.MultiView1.ActiveViewIndex = 1;
            this.Response.Redirect(typeof(Addendum.AddendumApply).Name + ".aspx?adden=" + i.ToString());
        }

        private void LoadDinamycControlls()
        {
            Panel panel;
            TextBox text;
            Label label;
            System.Data.DataSet dataset;
            RequiredFieldValidator rfvalidator;
            RangeValidator rangevalidator;

            using (PACFD.DataAccess.AddendumDataSet.AddendumDataTable Addendumtable = (new PACFD.Rules.Addendum()).SelectByID(this.SelectedAddendumID))
            {
                dataset = new System.Data.DataSet();

                try
                {
                    dataset.ReadXmlSchema(new System.Xml.XmlTextReader(new System.IO.StringReader(Addendumtable[0].Xsd)));
                }
                catch (Exception ex)
                {
                    this.MultiView1.ActiveViewIndex = 0;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    return;
                }

                foreach (System.Data.DataTable table in dataset.Tables)
                {
                    label = this.LoadControl(typeof(Label), null) as Label;
                    label.ID += "_label" + table.TableName;
                    label.Text = table.TableName;
                    label.Font.Bold = true;
                    this.divData.Controls.Add(label);
                    this.divData.Controls.Add(new System.Web.UI.LiteralControl("<br />"));

                    panel = this.LoadControl(typeof(Panel), null) as Panel;
                    panel.ID += "_panel_" + table.TableName;
                    this.divData.Controls.Add(panel);

                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        label = this.LoadControl(typeof(Label), null) as Label;
                        label.ID += string.Format("_{0}_label_{1}", panel.ID, column.ColumnName);
                        label.Text = column.ColumnName;
                        panel.Controls.Add(label);
                        panel.Controls.Add(new System.Web.UI.LiteralControl("<br />"));

                        panel.Controls.Add(new System.Web.UI.LiteralControl(
                            string.Format("<span class='vtip' title='Ingrese un valor de tipo <b>{0}</b>.<br />El valor <b>{1}</b>.'>"
                                , column.DataType == typeof(Int32) ? "entero"
                                    : column.DataType == typeof(DateTime) ? "fecha"
                                    : column.DataType == typeof(decimal) ? "moneda"
                                    : column.DataType == typeof(double) ? "decimal" : "cadena"
                                , column.AllowDBNull ? "no es necesario" : "es necesario"
                            )));

                        text = this.LoadControl(typeof(TextBox), null) as TextBox;
                        text.ID += string.Format("_{0}_textBox_{1}", panel.ID, column.ColumnName);
                        panel.Controls.Add(text);

                        panel.Controls.Add(new System.Web.UI.LiteralControl("</span>"));
                        panel.Controls.Add(new System.Web.UI.LiteralControl("<br />"));

                        text.Text = column.DataType == typeof(Int32) || column.DataType == typeof(decimal) || column.DataType == typeof(double) ? "0"
                            : column.DataType == typeof(DateTime) ? DateTime.Now.ToString("dd/MMMM/yyyy") : "";

                        panel.Controls.Add(new System.Web.UI.LiteralControl("<div class=\"validator-msg\">"));

                        if (!column.AllowDBNull)
                        {
                            rfvalidator = this.LoadControl(typeof(RequiredFieldValidator), null) as RequiredFieldValidator;
                            rfvalidator.ID += string.Format("_{0}_requiredfieldvalidator_{1}", panel.ID, column.ColumnName);
                            rfvalidator.ControlToValidate = text.ID;
                            rfvalidator.ErrorMessage = "Campo requerido.";
                            rfvalidator.Display = ValidatorDisplay.Dynamic;
                            panel.Controls.Add(rfvalidator);
                        }

                        rangevalidator = this.LoadControl(typeof(RangeValidator), null) as RangeValidator;
                        rangevalidator.ID += string.Format("_{0}_rangevalidator_{1}", panel.ID, column.ColumnName);
                        rangevalidator.ControlToValidate = text.ID;
                        rangevalidator.Type =
                            column.DataType == typeof(Int32) ? ValidationDataType.Integer
                            : column.DataType == typeof(DateTime) ? ValidationDataType.Date
                            : column.DataType == typeof(decimal) ? ValidationDataType.Currency
                            : column.DataType == typeof(double) ? ValidationDataType.Double : ValidationDataType.String;
                        rangevalidator.Display = ValidatorDisplay.Dynamic;

                        panel.Controls.Add(new System.Web.UI.LiteralControl("</div>"));
                    }

                    panel.Controls.Add(new System.Web.UI.LiteralControl("<br />"));
                }
            }

            this.MultiView1.ActiveViewIndex = 1;
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            string s;
            string[] names;
            object[] itemarray;
            TextBox textbox = null;
            System.Data.DataTable table;
            System.Data.DataRow row;
            PACFD.Rules.BillingsAddendum addendumrules = new PACFD.Rules.BillingsAddendum();
            PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable addendumtable;

            using (System.Data.DataSet dataset = new System.Data.DataSet())
            {
                using (PACFD.DataAccess.AddendumDataSet.AddendumDataTable Addendumtable = (new PACFD.Rules.Addendum()).SelectByID(this.SelectedAddendumID))
                {
                    try
                    { dataset.ReadXmlSchema(new System.Xml.XmlTextReader(new System.IO.StringReader(Addendumtable[0].Xsd))); }
                    catch (Exception ex)
                    { System.Diagnostics.Debug.WriteLine(ex.Message); }
                }

                if (dataset.Tables.Count < 1)
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_ADDENDUM_READ_ERROR;
                    this.WebMessageBox1.ShowMessage("Error al leer addenda", WebMessageBoxButtonType.Accept);
                    return;
                }

                dataset.Relations.Clear();
                dataset.EnforceConstraints = false;

                var onlypanels = from c in this.divData.Controls.OfType<Panel>()
                                 select c;

                foreach (Panel p in onlypanels)
                {
                    var onlytextbox = from c in p.Controls.OfType<TextBox>()
                                      select c;

                    names = p.ID.Split('_');
                    table = dataset.Tables[names[names.Length - 1]];

                    row = table.NewRow();
                    itemarray = row.ItemArray;//new object[table.Columns.Count];

                    for (int i = 0; i < row.ItemArray.Length; i++)
                    {
                        s = table.Columns[i].ColumnName;

                        foreach (TextBox box in onlytextbox)
                        {
                            names = box.ID.Split('_');

                            if (names[names.Length - 1] == s)
                            {
                                textbox = box;
                                break;
                            }
                        }

                        if (table.Columns[i].DataType == typeof(Int32))
                        {
                            try { itemarray[i] = Convert.ToInt32(textbox.Text); }
                            catch { itemarray[i] = 0; }
                        }
                        else if (table.Columns[i].DataType == typeof(Int64))
                        {
                            try { itemarray[i] = Convert.ToInt64(textbox.Text); }
                            catch { itemarray[i] = 0; }
                        }
                        else if (table.Columns[i].DataType == typeof(DateTime))
                        {
                            try { itemarray[i] = DateTime.Parse(textbox.Text); }
                            catch { itemarray[i] = DateTime.Now; }
                        }
                        else if (table.Columns[i].DataType == typeof(decimal))
                        {
                            try { itemarray[i] = Convert.ToDecimal(textbox.Text); }
                            catch { itemarray[i] = 0; }
                        }
                        else if (table.Columns[i].DataType == typeof(double))
                        {
                            try { itemarray[i] = Convert.ToDouble(textbox.Text); }
                            catch { itemarray[i] = 0; }
                        }
                        else
                        {
                            try { itemarray[i] = textbox.Text; }
                            catch
                            {
                                try { itemarray[i] = ""; }
                                catch
                                {
                                    if (table.Columns[i].AllowDBNull)
                                    { itemarray[i] = System.DBNull.Value; }
                                }
                            }
                        }
                    }

                    this.ClearRelationships(dataset, table.TableName);
                    table.PrimaryKey = new System.Data.DataColumn[0];
                    table.ChildRelations.Clear();
                    table.ParentRelations.Clear();
                    table.Constraints.Clear();
                    row.ItemArray = itemarray;
                    table.Rows.Add(row);
                    table.AcceptChanges();
                }

                s = dataset.GetXml();
                names = null;
                itemarray = null;
                textbox = null;
                table = null;
                row = null;
                onlypanels = null;
            }

            using (addendumtable = new PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable())
            {
                addendumtable.AddBillingsAddendumRow(this.BillingID, this.SelectedAddendumID, s);
                addendumtable.AcceptChanges();
                addendumtable.Rows[0].SetAdded();

                if (addendumrules.Exist(this.BillingID))
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_ADDENDUM_ALREADY_EXCIST;
                    this.WebMessageBox1.ShowMessage("El comprobante ya tiene aplicada una addenda.", WebMessageBoxButtonType.Accept);
                    return;
                }
                else if (!addendumrules.Update(addendumtable))
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_ADDENDUM_ERROR;
                    this.WebMessageBox1.ShowMessage("No se pudo aplicar la addenda al comprobante.", WebMessageBoxButtonType.Accept);
                    return;
                }
            }

            this.WebMessageBox1.CommandArguments = MESSAGE_ADDENDUM_APPLY;
            this.WebMessageBox1.ShowMessage("Addenda aplicada a comprobante.", WebMessageBoxButtonType.Accept);
        }
        /// <summary>
        /// Clear all restrictions of FOREING KEY types. This must be do at foot.
        /// </summary>
        /// <param name="dataset">DataSet with the tables.</param>
        /// <param name="tablename">Table name to clear.</param>
        private void ClearRelationships(System.Data.DataSet dataset, string tablename)
        {
            foreach (System.Data.DataTable table in dataset.Tables)
            {
                if (table.TableName == tablename)
                { continue; }

                for (int i = table.Constraints.Count - 1; i >= 0; i--)
                {
                    if (table.Constraints[i].ConstraintName.IndexOf(tablename) > -1)
                    { table.Constraints.RemoveAt(i); }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.MultiView1.ActiveViewIndex = 0;
            this.Response.Redirect(typeof(Addendum.AddendumApply).Name + ".aspx");
        }
    }
}
