﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddendumEditor.ascx.cs"
    Inherits="PACFDManager.Billings.Addendum.AddendumEditor" %>
<ul>
    <li>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Nombre" />
                    </label>
                    <asp:TextBox ID="txtName" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblDescription" runat="server" Text="Descripción" />
                    </label>
                    <asp:TextBox ID="txtDescription" runat="server"  />
                </td>
            </tr>
        </table>
    </li>
    <li style="height: 100%; width: 98%;">
        <label class="desc">
            <asp:Label ID="lblXsd" runat="server" Text="Xsd" />
        </label>
        <asp:TextBox ID="txtXsd" runat="server" TextMode="MultiLine" Width="98%" Height="400px" />
    </li>
</ul>
