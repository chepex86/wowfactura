﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings.Addendum
{
    public partial class AddendumList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            { return; }

            this.gdvAddendum.DataSource = (new PACFD.Rules.Addendum()).SelectAll();
            this.gdvAddendum.DataBind();
        }

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int id;
            ImageButton b = sender as ImageButton;

            if (b.IsNull())
            { return; }

            int.TryParse(b.CommandArgument, out id);

            switch (b.ID)
            {
                case "imgbEdit":
                    if (id < 1)
                    { return; }

                    this.Session["id"] = id;
                    this.Response.Redirect(typeof(AddendumModify).Name + ".aspx");
                    break;
                case "imgbDelete":
                    break;
            }
        }
    }
}
