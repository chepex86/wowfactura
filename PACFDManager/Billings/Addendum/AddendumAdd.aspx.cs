﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings.Addendum
{
    public partial class AddendumAdd : BasePage
    {
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddendumEditor1.InvokeApplyChanges();
        }

        protected void AddendumEditor1_ApplyChanges(object sender, AddendumEditorApplyChangesEventArgs e)
        {
            if ((new PACFD.Rules.Addendum()).Update(e.Table))
            {
                this.WebMessageBox1.ShowMessage("Configuración de addenda agregada con éxito.", WebMessageBoxButtonType.Accept);
                this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
                return;
            }
            
            this.WebMessageBox1.ShowMessage("Error al agregar configuración de addenda agregada.", WebMessageBoxButtonType.Accept);
            this.WebMessageBox1.CommandArguments = "";
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_SUCCESS:
                    this.Response.Redirect(typeof(AddendumList).Name + ".aspx");
                    break;
                default:
                    break;
            }
        }
    }
}
