﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class BillingTaxTypeSelector : PACFDManager.BaseUserControl
    {
        public event BillingTaxTypeSelectorSelectedEventHandler Selected;
        public event EventHandler CancelSelection;



        /// <summary>
        /// Get or set a string value with the CssClass property for the accept button.
        /// </summary>
        public string AcceptCss
        {
            get { return this.btnAcept.CssClass; }
            set { this.btnAcept.CssClass = value; }
        }
        /// <summary>
        /// Get or set a string value with the CssClass property  for the cancel button.
        /// </summary>
        public string Cancel
        {
            get { return this.btnCancel.CssClass; }
            set { this.btnCancel.CssClass = value; }
        }



        /// <summary>
        /// Load of the web page.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.TaxTemplate tem;
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table;
            PACFD.DataAccess.TaxTemplatesDataSet.GetNameTaxTypesByBillerIDDataTable seltable;
            System.Text.StringBuilder sbuild;

            if (this.IsPostBack)
                return;

            table = null;
            tem = new PACFD.Rules.TaxTemplate();
            seltable = tem.SelectForListByBillerID(this.CurrentBillerID);

            
            ddlFiscalRegime.Visible = false;
            lblFiscalRegime.Visible = false;

            PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable ds;
            ds = new PACFD.Rules.Billers().FiscalRegimeTable.GetByBillerID(this.CurrentBillerID);

            if (ds.Rows.Count == 0)
            {
                // Si no está en la tabla FiscalRegime quiere decir que no tiene un régimen fiscal fijo...
                ddlFiscalRegime.Visible = true;
                lblFiscalRegime.Visible = true;

                PACFD.Rules.Billers billers;
                PACFD.DataAccess.BillersDataSet.BillersDataTable billersTable;
                billers = new PACFD.Rules.Billers();
                billersTable = billers.SelectByID(this.CurrentBillerID);

                this.ddlFiscalRegime.DataSource = Helpers.GetTaxSystem();
                this.ddlFiscalRegime.DataTextField = "Descripcion";
                this.ddlFiscalRegime.DataValueField = "c_RegimenFiscal";
                this.ddlFiscalRegime.DataBind();
                if (billersTable.Rows.Count > 0)
                {
                    this.ddlFiscalRegime.SelectedValue = billersTable.Rows[0]["TaxSystem"].ToString();
                }
            }

            this.ddlBillingTaxTemplate.DataSource = seltable;
            this.ddlBillingTaxTemplate.DataTextField = "Name";
            this.ddlBillingTaxTemplate.DataValueField = "TaxTemplateID";
            this.ddlBillingTaxTemplate.DataBind();
            this.ddlBillingTaxTemplate.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlBillingTaxTemplate.SelectedIndex = 0;
            this.ddlBillingTaxTemplate.Attributes["onchange"] = string.Format("javascript:{0}_ShowHideDescription(this);", this.ClientID);

            sbuild = new System.Text.StringBuilder();

            sbuild.Append(string.Format("<br/><div id='{0}_Div_Description_{1}' style='display:block'>...</div>",
                this.ClientID, 0));

            foreach (ListItem item in this.ddlBillingTaxTemplate.Items)
            {
                if (item.Value.ToInt32() == 0) //is the [Seleccionar] text continue.
                    continue;

                table = tem.SelectByID(item.Value.ToInt32());
                item.Attributes.Add("title", table[0].Description);

                sbuild.AppendLine(string.Format("<div id='{0}_Div_Description_{1}' style='display:none'>",
                    this.ClientID, table[0].TaxTemplateID));
                sbuild.Append("<br />");
                sbuild.Append(string.Format("<b>Titulo del comprobate:</b> {0}", table[0].Name));
                sbuild.Append("<br /><br />");
                sbuild.Append("<b>Tipo de comprobante:</b> ");
                sbuild.AppendLine(table[0].BillingType);
                sbuild.Append("<br /><br />");
                sbuild.AppendLine("<b>Descripción:</b>");
                sbuild.Append("<br />");
                sbuild.AppendLine(table[0].Description);
                sbuild.AppendLine("</div>");

                item.Text = string.Format("{0} - {1}", item.Text.Trim(), table[0].Description);
            }

            this.ltrDescription.Text = sbuild.ToString();

            ///-------------------------------------------------------------------------------
            ///java script code registration

            sbuild = new System.Text.StringBuilder();
            sbuild.AppendLine("<script type='text/javascript'>");
            sbuild.Append(string.Format("function {0}_ShowHideDescription(d)", this.ClientID));
            sbuild.AppendLine("{");
            sbuild.AppendLine("var i = d.options[d.selectedIndex].value;");

            foreach (ListItem item in this.ddlBillingTaxTemplate.Items)
            {
                sbuild.AppendLine(string.Format("document.getElementById('{0}_Div_Description_{1}').style.display='none';",
                    this.ClientID, item.Value));
            }

            sbuild.AppendLine(string.Format("document.getElementById('{0}_Div_Description_' + i).style.display='block';", this.ClientID));
            sbuild.Append("}</script>");
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showHideDescription", sbuild.ToString());
        }
        protected void btnAcept_Click(object sender, EventArgs e)
        {
            int temid;
            BillingTaxTypeSelectorEventArgs a;
            PACFD.Rules.TaxTemplate tem;
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table;

            if (this.ddlBillingTaxTemplate.SelectedItem.IsNull())
                temid = 0;
            else
                int.TryParse(this.ddlBillingTaxTemplate.SelectedItem.Value, out temid);

            if (this.ddlBillingTaxTemplate.SelectedIndex < 1)
                return;

            tem = new PACFD.Rules.TaxTemplate();
            table = tem.SelectByID(temid);
            tem = null;            

            if (table.Count < 1)
                return;

            a = new BillingTaxTypeSelectorEventArgs(table);

            this.OnSelected(a);
        }
        /// <summary>
        /// Fire the select event of the tax template.
        /// </summary>
        /// <param name="e">Arguments send by the control.</param>
        protected virtual void OnSelected(BillingTaxTypeSelectorEventArgs e)
        {
            if (!this.Selected.IsNull())
                this.Selected(this, e);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelSelection(e);
        }
        /// <summary>
        /// Fire the cancel event of the control.
        /// </summary>
        protected virtual void OnCancelSelection(EventArgs e)
        {
            if (!this.CancelSelection.IsNull())
                this.CancelSelection(this, e);
        }
        protected void ddlFiscalRegime_OnChange(object sender, EventArgs e) {
            if(ddlFiscalRegime.Visible)
                new PACFD.Rules.Billers().SetTaxSystem(this.ddlFiscalRegime.SelectedValue, this.CurrentBillerID);
        }
    }
}