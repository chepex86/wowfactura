﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class BillingsModify : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BillingsEditor1.Enabled = false;
        }

        protected void BillingsArticleSearch1_CancelClick(object sender, EventArgs e)
        {
            this.BillingsArticleSearch1.Visible =
            !(this.BillingsEditor1.Visible = true);
        }

        protected void BillingsEditor1_SearchArticle(object sender, EventArgs e)
        {
            this.BillingsArticleSearch1.Visible =
            !(this.BillingsEditor1.Visible = false);
        }

        protected void BillingsArticleSearch1_ArticlesSelected(object sender, BillingsArticleSearchSelectedEventArgs e)
        {
            this.BillingsArticleSearch1.Visible =
            !(this.BillingsEditor1.Visible = true);
        }
    }
}
