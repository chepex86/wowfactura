﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    partial class BillingsEditor
    {
        /// <summary>
        /// Class used as an abstraction of the rows in the TaxGridView control.
        /// </summary>
        public class TaxesGridRow
        {
            public int TaxTypeID { get; set; }
            public bool Editable { get; private set; }
            public int BillingTaxesID { get; private set; }
            public Boolean TaxTransfer { get; private set; }
            public Boolean IvaAffected { get; private set; }
            public string Text { get; private set; }
            public bool Required { get; private set; }
            public TaxRowType RowType { get; set; }
            public string Value { get; private set; }
            public string Name { get; private set; }

            // agregado para copiarlos a la bd.  // este valor es seleccionable 
            public string FactorType { get; private set; }
            // aun no se si este es un valor que deba ir.. 
            public decimal AmountBase { get; private set; }

            internal TaxesGridRow(int taxtypeid, string name, bool editable, int billtaxid, bool txtransfer,
                bool ivaaffected, string text, string value, bool required, TaxRowType rowtype, string factorType, decimal amountBase)
            {
                this.Required = required;
                this.TaxTypeID = taxtypeid;
                this.Editable = editable;
                this.BillingTaxesID = billtaxid;
                this.TaxTransfer = txtransfer;
                this.IvaAffected = ivaaffected;
                this.Text = value;
                this.RowType = rowtype;
                this.Value = value;
                this.Name = name;
                this.FactorType = factorType;
                this.AmountBase = amountBase;
            }
        }
    }
}
