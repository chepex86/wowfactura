﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Billings
{
    public partial class SendBillingPaymentMail : PACFDManager.BasePage
    {
        const string BILLINGS_NOT_FOUND = "0";
        const string BILLINGS_INCORRECT_MAIL = "1";


        public int BillingPaymentId
        {
            get
            {
                int r;
                object o;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.BillingsPayments_3 billingp;
            PACFD.DataAccess.BillingsDataSet.BillingsPayments_3DataTable billingptable;
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable receptortable;

            if (this.IsPostBack)
                return;

            this.btnCancel.CommandArgument = this.Request.UrlReferrer.ToString();

            billingp = new PACFD.Rules.BillingsPayments_3();
            billingptable = billingp.SelectByID(this.BillingPaymentId);
            billingp = null;

            if (billingptable.Count < 1)
            {
                billingptable.Dispose();
                billingptable = null;
                this.WebMessageBox1.CommandArguments = BILLINGS_NOT_FOUND;
                this.WebMessageBox1.ShowMessage("No se puede encontrar el comprobante.", true);
                return;
            }

            receptor = new PACFD.Rules.Receptors();
            receptortable = receptor.SelectByID(billingptable[0].ReceptorID);
            receptor = null;

            if (receptortable.Count < 1)
            {
                receptortable.Dispose();
                receptortable = null;
                return;
            }

            this.txtTo.Text = receptortable[0].Email;
            this.txtDescription.Text = string.Format("Aviso de {0}comprobante: {1} - {2}",
                billingptable[0].PreBilling ? "pre" : string.Empty,
                billingptable[0].Serial,
                billingptable[0].Folio);

            billingptable.Dispose();
            billingptable = null;
            receptortable.Dispose();
            receptortable = null;
        }

        protected void btnAcept_Click(object sender, EventArgs ea)
        {
            PACFD.Rules.BillingPaymentMailSender mail;
            PACFD.Rules.BillingsPayments_3 billp = new PACFD.Rules.BillingsPayments_3();
            PACFD.DataAccess.BillingsDataSet e;
            PACFD.Rules.Mail.MailSendResult errormail;
            System.Text.StringBuilder sb;

            this.txtTo_RegularExpressionValidator.ValidationExpression =
                "^(\\s*,?\\s*[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})+\\s*$";
            this.txtTo_RegularExpressionValidator.Validate();

            if (!this.txtTo_RegularExpressionValidator.IsValid)
            {
                this.WebMessageBox1.CommandArguments = BILLINGS_INCORRECT_MAIL;
                this.WebMessageBox1.ShowMessage("El campo de correo electronico es incorrecto.", true);
                return;
            }

            e = billp.GetFullBilling(this.BillingPaymentId);

            if (e.BillingsPayments_3.Count < 1)
            {
                this.WebMessageBox1.ShowMessage("No se pudo encontrar el comprobante.", true);
                return;
            }

            mail = new PACFD.Rules.BillingPaymentMailSender(this.Request.PhysicalApplicationPath + @"Includes\Mail\EmisorAdded.es.xml", "004", this.CurrentBillerID);

            #region Old source code to send mail
            //mail[BillingPaymentMailSender.LBL_BILLER_ADDRESS] = e.BillingsBillers[0].IssuedInDifferentPlace ?
            //    e.BillingsIssued[0].Address : e.BillingsBillers[0].BillerAddress;
            //mail[BillingPaymentMailSender.LBL_BILLER_NAME] = e.BillingsBillers[0].BillerName;
            //mail[BillingPaymentMailSender.LBL_DATE] = e.Billings[0].BillingDate.ToString("yyyy/MM/dd");
            //mail[BillingPaymentMailSender.LBL_FOLIO] = e.Billings[0].Folio;
            //mail[BillingPaymentMailSender.LBL_MAIL_DATE] = DateTime.Now.ToString("yyyy/MM/dd");
            //mail[BillingPaymentMailSender.LBL_PAYMENT_METHOD] = e.Billings[0].PaymentMethod;
            //mail[BillingPaymentMailSender.LBL_PREBILLING] = e.Billings[0].PreBilling ? "No sellado" : "Sellado";
            //mail[BillingPaymentMailSender.LBL_RECEPTOR_ADDRESS] = e.BillingsReceptors[0].ReceptorAddress;
            //mail[BillingPaymentMailSender.LBL_SUBTOTAL] = e.Billings[0].SubTotal.ToString(); 
            //mail[BillingPaymentMailSender.LBL_TOTAL] = e.Billings[0].Total.ToString();
            //mail[BillingPaymentMailSender.LBL_TOTAL_DETAINED] = e.Taxes[0].TotalDetained.ToString();
            //mail[BillingPaymentMailSender.LBL_TOTAL_TRANSFERED] = e.Taxes[0].TotalTransfer.ToString();
            //mail[BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS] =
            //    string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Requezst.Url.Authority
            //    + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=pdf&a={0}&b={1}&o=true",
            //    PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString()),
            //    PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
            //    );
            //mail[BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS_2] = string.Format(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
            //    + HttpContext.Current.Request.ApplicationPath + @"/Billings/XML/DownloadPage.aspx?type=xml&a={0}&b={1}&o=true",
            //    PACFD.Common.Cryptography.EncryptUnivisitString(this.CurrentBillerID.ToString()),
            //    PACFD.Common.Cryptography.EncryptUnivisitString(e.Billings[0].BillingID.ToString())
            //    ); 
            #endregion

            PACFD.Rules.BillingPaymentMailSender.SetBillingMail(mail, ref e, this.CurrentBillerID);

            if (this.ckbDownloadButtons.Checked)
            {
                sb = new System.Text.StringBuilder();

                sb.AppendLine("<ul class=\"ticket-detail-nav\">");
                mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS] =
                     string.Format("{0}://{1}{2}/Billings/XML/{3}",
                     HttpContext.Current.Request.Url.Scheme,
                     HttpContext.Current.Request.Url.Authority,
                     HttpContext.Current.Request.ApplicationPath,
                     mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS_2].Replace("xml", "pdf"));
                mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS_2] =
                     string.Format("{0}://{1}{2}/Billings/XML/{3}",
                     HttpContext.Current.Request.Url.Scheme,
                     HttpContext.Current.Request.Url.Authority,
                     HttpContext.Current.Request.ApplicationPath,
                     mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS_2]);
                

                sb.AppendLine("<li><a class=\"xml-icon\" href='[lblDownloadAddress]'>Descarga de archivo PDF haz click aqui.</a></li>");
                sb.AppendLine("<li><a class=\"xml-icon\" href='[lblDownloadAddress2]'>Descarga de archivo XML haz click aqui.</a></li>");
                sb.AppendLine("</ul>");

                mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_HTML] = sb.ToString();
            }

            mail.Message.To.Clear();
            mail.Message.To.Add(this.txtTo.Text.Trim());
            mail.Message.Subject = this.txtDescription.Text.Trim();

#if DEBUG
            System.Diagnostics.Debug.WriteLine(mail[PACFD.Rules.BillingPaymentMailSender.LBL_DOWNLOAD_ADDRESS]);
#endif

            if (this.ckbIncludeFiles.Checked)
                this.AddFilesToMail(mail);

            if (!(errormail = mail.Send()).IsSended)
            {
                LogManager.WriteError(new Exception(errormail.Error.Message));
                this.WebMessageBox1.ShowMessage("No se pudo enviar el correo.", true);
                return;
            }

            this.WebMessageBox1.ShowMessage("Correo enviado con exito.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);

            //mail.Message.Attachments.Add(new System.Net.Mail.Attachment(
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case BILLINGS_INCORRECT_MAIL: this.WebMessageBox1.CommandArguments = string.Empty; return;
            }

            this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect((sender as Button).CommandArgument);
        }

        private void AddFilesToMail(PACFD.Rules.BillingPaymentMailSender mail)
        {
            System.Net.Mail.Attachment at;
            System.IO.Stream stream = this.GetXml();
            System.Net.Mime.ContentDisposition comp;
            comp = null;

            if (!stream.IsNull() && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB)
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                at = new System.Net.Mail.Attachment(stream, "text/xml");
                comp = at.ContentDisposition;
                comp.CreationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                at.Name = string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingPaymentMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"));
                mail.Message.Attachments.Add(at);
            }
        }

        private System.IO.Stream GetXml()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            string fileName = string.Empty;

            byte[] b = billing.GetXml(BillingPaymentId, ref fileName);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();

            if (b.IsNull() || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                return null;

            stream.Write(b, 0, b.Length);

            return stream;
        }
    }
}
