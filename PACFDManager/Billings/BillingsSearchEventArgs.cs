﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsSearchEventArgs : EventArgs
    {
        public DateTime? Date { get; private set; }
        public SearchByFolioEventArgs Folio { get; private set; }
        public string Receptor { get; private set; }
        public bool? PreBilling { get; private set; }
        public bool? IsPaid { get; private set; }
        public string UUID { get; private set; }
        public string ExternalFolio { get; private set; }
        public int? BillingID { get; set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="date">DateTime of the billing. This value can be null.</param>
        /// <param name="serialtext"></param>
        /// <param name="serialvalue"></param>
        /// <param name="folionumber">Folio number of the billing. This value can be null.</param>
        /// <param name="receptorname">Name of the receptor.</param>
        /// <param name="prebilling">Prebilling state of the billing. This value can be null.</param>
        /// <param name="ispaid">Indicate is a billing is paid.</param>
        /// <param name="externalfolio">External folio used by the biller.</param>
        /// <param name="billingid">Billing ID used internal for search and identification</param>
        public BillingsSearchEventArgs(DateTime? date, string serialtext, string serialvalue,
            int? folionumber, string receptorname, bool? prebilling, bool? ispaid, string uuid, string externalfolio, int? billingid)
        {
            this.Date = date;
            this.Receptor = receptorname;
            this.Folio = new SearchByFolioEventArgs(serialtext, serialvalue, folionumber);
            this.PreBilling = prebilling;
            this.IsPaid = ispaid;
            this.UUID = uuid;
            this.ExternalFolio = externalfolio.IsNull() ? null : externalfolio.Trim().Length < 1 ? null : externalfolio;
            this.BillingID = billingid;
        }
    }
}