﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class SearchByFolioEventArgs : EventArgs
    {
        public string SerialText { get; private set; }
        public string SerialValue { get; private set; }
        public int? FolioNumber { get; private set; }



        /// <summary>
        /// Create a enw instance of the class.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <param name="folionumber"></param>
        public SearchByFolioEventArgs(string serialtext, string serialvalue, int? folionumber)
        {
            this.SerialText = serialtext;
            this.SerialValue = serialvalue;
            this.FolioNumber = folionumber;
        }
    }
}
