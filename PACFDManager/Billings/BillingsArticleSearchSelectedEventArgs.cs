﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Class used as arguments send by the BillingsArticleSearch control.
    /// </summary>
    public class BillingsArticleSearchSelectedEventArgs : EventArgs
    {
        /// <summary>
        /// Get a BillingsArticleSearchSelectMode with the select mode of the control.
        /// </summary>
        public BillingsArticleSearchSelectMode SelectMode { get; private set; }
        /// <summary>
        /// Get a BillingsArticleSearchSelectedCollection with the articles selected.
        /// </summary>
        public BillingsArticleSearchSelectedCollection Articles { get; private set; }



        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="articles"></param>
        /// <param name="selectmode">Selecction mode of the control.</param>
        public BillingsArticleSearchSelectedEventArgs(BillingsArticleSearchSelectedCollection articles, BillingsArticleSearchSelectMode selectmode)
        {
            this.Articles = articles;
            this.SelectMode = selectmode;
        }
    }
}
