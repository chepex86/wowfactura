﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    partial class BillingsPaymentEditor
    {
        /// <summary>
        /// Class used to group the main properties and methods of PACFDManager.Billings.BillingsEditor control.
        /// </summary>
        public class BillingsPaymentEditorInfo
        {
            /// <summary>
            /// Get or Set the parent control of PACFDManager.Billings.BillingsEditor.BillingsEditorInfo
            /// </summary>
            protected BillingsPaymentEditor Parent { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            public ListItemCollection PaymentTypeItems { get { return this.Parent.ddlPaymentMethod.Items; } }

            public ListItem PaymentMethod { get { return this.Parent.ddlPaymentMethod.SelectedItem; } }
            public BillingsGridInf BillingsGrid { get; private set; }
            /// <summary>
            /// Control the tax grid data bind and the totals tax grid.
            /// </summary>

            public string Seal { get { return this.Parent.BillingSeal; } }
            /// <summary>
            /// Get a string value with the original string.
            /// </summary>
            public string OriginalString { get { return this.Parent.BillingOriginalString; } }
            /// <summary>
            /// Get a boolean value with the visible state
            /// of the receptor drop down list.
            /// </summary>
            public bool IsReceptorDropDownVisible { get { return this.Parent.ddlReceptorSearch.Visible; } }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Parent of the class.</param>
            internal BillingsPaymentEditorInfo(BillingsPaymentEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
                this.BillingsGrid = new BillingsGridInf(owner);
            }
            /// <summary>
            /// If the drop down list is visible set a receptor in the drop down list.
            /// </summary>
            /// <param name="receptorid">ID of the receptor to select.</param>
            public void SetReceptorSelected(int receptorid)
            {
                int i = -1;

                if (!this.Parent.ddlReceptorSearch.Visible)
                    return;

                foreach (ListItem item in this.Parent.ddlReceptorSearch.Items)
                {
                    i++;

                    if (item.Value != receptorid.ToString())
                        continue;

                    this.Parent.ddlReceptorSearch.SelectedIndex = i;
                    break;
                }

                this.Parent.btnAceptReceptorSearch_Click(this.Parent.btnAceptReceptorSearch, EventArgs.Empty);
            }
            /// <summary>
            /// If the drop down list is not visible set and search 
            /// a receptor in the text field.
            /// </summary>
            /// <param name="receptorname"></param>
            public void SetReceptorSelected(string receptorname)
            {
                if (this.Parent.ddlReceptorSearch.Visible)
                    return;

                this.Parent.txtReceptorSearch.Text = receptorname;
                this.Parent.btnAceptReceptorSearch_Click(this.Parent.btnAceptReceptorSearch, EventArgs.Empty);
            }
        }
    }
}