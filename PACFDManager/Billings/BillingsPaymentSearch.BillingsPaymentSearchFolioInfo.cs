﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    partial class BillingsPaymentSearch
    {
        public class BillingsPaymentSearchFolioInfo
        {
            public BillingsPaymentSearch Parent { get; private set; }
            public System.Web.UI.WebControls.ListItem Serie { get { return this.Parent.ddlSerie.SelectedItem; } }
            public int SerieIndex
            {
                get { return this.Parent.ddlSerie.SelectedIndex; }
                set { this.Parent.ddlSerie.SelectedIndex = value; }
            }
            public System.Web.UI.WebControls.ListItemCollection SerieItems { get { return this.Parent.ddlSerie.Items; } }
            public int FolioNumber
            {
                get
                {
                    int r;

                    if (!int.TryParse(this.Parent.txtFolio.Text, out r))
                        r = 0;

                    return r;
                }
                set
                {
                    this.Parent.txtFolio.Text = value.ToString();
                }
            }

            public BillingsPaymentSearchFolioInfo(BillingsPaymentSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner parameter can't be null.");

                this.Parent = owner;
            }
        }
    }
}