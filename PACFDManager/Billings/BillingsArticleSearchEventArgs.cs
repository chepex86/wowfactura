﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Class used as arguments send by the BillingsArticleSearch control in a search.
    /// </summary>
    public class BillingsArticleSearchEventArgs : EventArgs
    {
        /// <summary>
        /// Get a BillingsArticleSearchSelectMode with the selection mode of the BillingsArticleSearch control.
        /// </summary>
        public BillingsArticleSearchSelectMode SelectMode { get; private set; }
        /// <summary>
        /// Get a string value with the article to look for.
        /// </summary>
        public string Article { get; private set; }
        /// <summary>
        /// Get a PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table with
        /// the search result.
        /// </summary>
        public PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable Table { get; set; }



        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="article">Article to look for.</param>
        /// <param name="table">
        /// PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table with
        /// the search result.
        /// </param>
        /// <param name="selectmode">Select mode of the control.</param>
        public BillingsArticleSearchEventArgs(string article,
            PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table,
            BillingsArticleSearchSelectMode selectmode)
        {
            this.SelectMode = selectmode;
            this.Article = article;
            this.Table = table;
        }
    }
}
