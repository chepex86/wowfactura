﻿<%@ Page Title="Eliminar comprobante." Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillingsDelete.aspx.cs" Inherits="PACFDManager.Billings.BillingsDelete" %>

<%@ Register Src="BillingsEditor.ascx" TagName="BillingsEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblTitle" runat="server" Text="¿Desea eliminar el comprobante?" Font-Size="24px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
                <uc1:BillingsEditor ID="BillingsEditor1" runat="server" EditorMode="View" />
            </td>
        </tr>
        <tr>
            <td>
                <table style="text-align: center">
                    <tr>
                        <td>
                            <asp:Button ID="btnAcept" runat="server" Text="Eliminar" OnClick="btnAcept_Click" />
                        </td>
                        <td style="text-align: center">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
                                Style="height: 26px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
