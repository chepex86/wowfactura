<%@ Page Title="Agregar Comprobante" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillingsAdd.aspx.cs" Inherits="PACFDManager.Billings.BillingsAdd" %>

<%@ Register Src="BillingsEditor.ascx" TagName="BillingsEditor" TagPrefix="uc1" %>
<%@ Register Src="BillingsArticleSearch.ascx" TagName="BillingsArticleSearch" TagPrefix="uc2" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc3" %>
<%@ Register Src="../Receptors/ReceptorsEditor.ascx" TagName="ReceptorsEditor" TagPrefix="uc5" %>
<%@ Register Src="../Concepts/ConceptsEditor.ascx" TagName="ConceptsEditor" TagPrefix="uc4" %>
<%@ Register Src="BillingTaxTypeSelector.ascx" TagName="BillingTaxTypeSelector" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

    <script type="text/javascript">

        Number.prototype.decimal = function(n) {
            pot = Math.pow(10, parseInt(n));
            return parseInt(this * pot) / pot;
        }

        function CalculateTax(total, unitPrice, tax, chkBoxTax) {

            total = document.getElementById(total);
            unitPrice = document.getElementById(unitPrice);
            tax = document.getElementById(tax);
            chkBoxTax = document.getElementById(chkBoxTax);

            var sum;
            var result = 0;

            if (total == null || unitPrice == null || tax == null || chkBoxTax == null) {
                return;
            }

            result = chkBoxTax.checked ? unitPrice.value * tax.value / 100 : unitPrice.value;

            if (chkBoxTax.checked == false) {
                total.value = result;
            }
            else {
                sum = parseFloat(unitPrice.value) + parseFloat(result);
                total.value = sum.decimal(2);
            }
            if (total.value == 'NaN') {
                total.value = '';
            }
        }

        function OnlyNumbers() {

            if (event != null)
                return false;

            if (event.keyCode < 45 || event.keyCode > 57) {
                event.returnValue = false;
            }
            else {
                return true;
            }
        }

       
    </script>

    <div id="divValidationsKeysIsNotValid" style="position: fixed; top: 80%; left: 70%;
        color: #ff1818; background-color: #feeaea; border-width: 1px; border-color: Black;
        border-style: solid; width: 200px; display: none;">
        Uno o m�s campos son invalidos
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlEditor" runat="server">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="viewBillingEditor" runat="server">
                        <div class="formStyles">
                            <div class="info">
                                <label class="desc">
                                    <h2>
                                        Tipo de comprobante:
                                        <asp:Label ID="lblBillingEditorTitle" runat="server" Text="Titulo" />
                                    </h2>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="btnPreview" runat="server" ImageUrl="~/Includes/Images/png/kfind.png"
                                            Text="Vista previa" AlternateText="Vista previa" CssClass="Button" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <br />
                        <uc1:BillingsEditor ID="BillingsEditor1" runat="server" OnGeneratedBilling="BillingsEditor1_GeneratedBilling"
                            OnReceptorDataBind="BillingsEditor1_ReceptorDataBind" OnReceptorInvalidDataBind="BillingsEditor1_ReceptorInvalidDataBind" 
                            OnAddNewReceptor="BillingsEditor1_AddNewReceptor"
                            OnAddNewConcept="BillingsEditor1_AddNewConcept" OnConceptAfterAddToBill="BillingsEditor1_ConceptAfterAddToBill"
                            OnClientModify="BillingsEditor1_ClientModify" />
                        <div class="btnInvoice">
                            <%--<asp:Button ID="btnGenerateBilling" runat="server" CssClass="Button" Text="Aceptar"
                                OnClick="btnGenerateBilling_Click" />
                            &nbsp;<asp:Button ID="btnCancelBilling" runat="server" Text="Cancelar" CausesValidation="false"
                                OnClick="btnCancelBilling_Click" CssClass="Button" />--%>
                            <asp:LinkButton ID="btnGenerateBilling" runat="server" CssClass="Button" Text="Aceptar"
                                OnClick="btnGenerateBilling_Click" />
                            <asp:LinkButton ID="btnCancelBilling" runat="server" Text="Cancelar" CausesValidation="false"
                                OnClick="btnCancelBilling_Click" CssClass="Button" />
                        </div>
                    </asp:View>
                    <asp:View ID="viewAddClient" runat="server">
                        <uc5:ReceptorsEditor ID="ReceptorsEditor1" runat="server" OnApplyChanges="ReceptorsEditor1_ApplyChanges"
                            OnCancelClick="ReceptorsEditor1_CancelClick" />
                    </asp:View>
                    <asp:View ID="viewAddConcept" runat="server">
                        <uc4:ConceptsEditor ID="ConceptsEditor1" runat="server" OnApplyChanges="ConceptsEditor1_ApplyChanges"
                            OnCancelClick="ReceptorsEditor1_CancelClick" />
                    </asp:View>
                    <asp:View ID="viewSelectBillingType" runat="server">
                        <div id="container" class="wframe80">
                            <div class="form">
                                <div class="formStyles">
                                    <div class="info">
                                        <label class="desc">
                                            <h2>
                                                <asp:Label ID="lblTitle" runat="server" Text="Comprobante" />
                                            </h2>
                                        </label>
                                    </div>
                                    <uc6:BillingTaxTypeSelector ID="BillingTaxTypeSelector1" runat="server" OnSelected="BillingTaxTypeSelector1_Selected"
                                        OnCancelSelection="BillingTaxTypeSelector1_CancelSelection" />
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
            <uc3:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="wmbMessage_Click" />
            <uc3:WebMessageBox ID="wmbCancel" runat="server" OnClick="wmbCancel_Click" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
