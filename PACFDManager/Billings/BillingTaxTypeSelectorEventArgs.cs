﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Event arguments used to pass information from BillingTaxtypeSelector.
    /// </summary>
    public class BillingTaxTypeSelectorEventArgs : EventArgs
    {
        /// <summary>
        /// Get the ID of the tax template selected
        /// </summary>
        public int TaxTemplateID { get { return this.DataTable[0].TaxTemplateID; } }
        public string Name { get { return this.DataTable[0].Name; } }
        public string BillingType { get { return this.DataTable[0].BillingType; } }
        public string Description { get { return this.DataTable[0].Description; } }
        public PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable DataTable { get; private set; }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        /// <param name="taxtempid">TaxTemplate ID selected.</param>
        /// <param name="name">Name of the Billing Type name.</param>
        /// <param name="billingtype">Internal Biilling Type.</param>
        public BillingTaxTypeSelectorEventArgs(PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table)
        {
            this.DataTable = table;
        }
    }
}
