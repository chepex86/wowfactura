﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillingImporting.aspx.cs" Inherits="PACFDManager.Billings.BillingImporting" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <%--<script type="text/javascript" src="BillingImport.js"></script>--%>
    <div id="container" class="wframe80">
        <div class="form">
            <div class="formStyles">
                <div>
                    <div class="info">
                        <label class="desc">
                            <h2>
                                Importación de archivos de comprobante.
                            </h2>
                        </label>
                    </div>
                    <asp:FileUpload ID="fuFile" runat="server" />
                    <asp:Button ID="btnSendFile" runat="server" Text="Enviar archivo" CssClass="Button"
                        OnClick="btnSendFile_Click" />
                    <div style="text-align: right;">
                        <asp:Button ID="btnImport" runat="server" Text="Importar" CssClass="Button" Visible="false"
                            OnClick="btnImport_Click" />
                    </div>
                </div>
                <br />
                <div>
                    <asp:GridView ID="gdvMain" runat="server" AutoGenerateColumns="false" OnRowDataBound="gdvMain_RowDataBound"
                        ShowFooter="false" ShowHeader="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div id="divBilling" runat="server">
                                        <span><b>Empresa: </b></span><span>
                                            <asp:Label ID="lblCompanyName" runat="server" Text="" EnableViewState="false" /></span>                                            
                                        <br />
                                        <span><b>Dirección: </b></span><span>
                                            <asp:Label ID="lblCompanyAddress" runat="server" Text="" EnableViewState="false" /></span>
                                        <br />
                                        <span><b>RFC: </b></span><span>
                                            <asp:Label ID="lblCompanyRFC" runat="server" Text="" EnableViewState="false" />
                                        </span>
                                    </div>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span><b>Serial y Folio: </b></span><span>
                                                    <%# string.Format("{0}-{1}", this.Eval("Serial"), this.Eval("Folio"))%></span>
                                                <br />
                                                <span><b>Cliente: </b></span><span>
                                                    <asp:Label ID="lblClientName" runat="server" Text="" EnableViewState="false" /></span>
                                                <br />
                                                <span><b>Dirección: </b></span><span>
                                                    <asp:Label ID="lblClientAddress" runat="server" Text="" EnableViewState="false" /></span>
                                                <br />
                                                <span><b>Terminos de pago: </b></span><span>
                                                    <%# string.Format("{0}", this.Eval("PaymentTerms"))%>
                                                </span>
                                                <br />
                                                <span><b>RFC: </b></span><span>
                                                    <asp:Label ID="lblClientRFC" runat="server" Text="" EnableViewState="false" />
                                                </span><span><b>A credito: </b></span><span>
                                                    <%# this.Eval("IsCredit").ToString() == "True" ? "Si" : "No"%>
                                                </span>
                                                <asp:GridView ID="gdvConcepts" runat="server" AutoGenerateColumns="false" ShowFooter="false">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Cantidad" DataField="Count" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="Tipo" DataField="Unit" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="Descripción" DataField="Description" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="Descuento" DataField="DiscountPercentage" DataFormatString="%{0}"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="P. Unitario" DataField="Amount" DataFormatString="{0:C2}"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="Importe" DataField="UnitValue" DataFormatString="{0:C2}"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                Apl. Imp.
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div style="text-align: center">
                                                                    <%# this.Eval("AppliesTax").ToString() == "True" ? "Si" : "No" %>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="dgHeader" />
                                                    <RowStyle CssClass="dgItem" />
                                                    <AlternatingRowStyle CssClass="dgAlternate" />
                                                    <EmptyDataTemplate>
                                                        No hay productos en el comprobante.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <br />
                                                <div style="text-align: right;">
                                                    <asp:GridView ID="gdvIVAs" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                                        ShowFooter="false" Style="width: 50%">
                                                        <HeaderStyle CssClass="dgHeader" />
                                                        <RowStyle CssClass="dgItem" />
                                                        <AlternatingRowStyle CssClass="dgAlternate" />
                                                        <EmptyDataTemplate>
                                                            No hay productos en el comprobante.
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ControlStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <%#this.Eval("Name")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%# string.Format("{0:c}", this.Eval("Import"))%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <br />
                <div style="text-align: right;">
                    <asp:Button ID="btnImport2" runat="server" Text="Importar" CssClass="Button" Visible="false"
                        OnClick="btnImport_Click" />
                </div>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
