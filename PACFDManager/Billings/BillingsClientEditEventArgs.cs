﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public class BillingsClientEditEventArgs : EventArgs
    {
        public int ReceptorID { get; private set; }
        public Billings.BillingsEditorMode EditorMode { get; private set; }

        public BillingsClientEditEventArgs(int receptorid, Billings.BillingsEditorMode mode)
        {
            this.ReceptorID = receptorid;
            this.EditorMode = mode;
        }
    }
}
