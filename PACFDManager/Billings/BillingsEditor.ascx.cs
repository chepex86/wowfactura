﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Text;
using System.Data;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Control used to edit the billings.
    /// </summary>
    public partial class BillingsEditor : PACFDManager.BaseUserControl
    {
        private int NUMBER_OF_CLIENT_BEFORE_SEARCH = 100; //modify this to vary count of receptor display on search
        
        /// <summary>
        /// Fires the ApplyChanges event.
        /// </summary>
        public event GeneratedBillingEventHandler GeneratedBilling;
       
        /// <summary>
        /// 
        /// </summary>
        public event ArticleBeforeAddEventHandler ArticleBeforeAdd;
        
        /// <summary>
        /// 
        /// </summary>
        public event ArticleAfterAddEventHandler ArticleAfterAdd;
        
        /// <summary>
        /// Fired when the receptor has been loaded.
        /// </summary>
        public event EventHandler ReceptorDataBind;

        /// <summary>
        /// Fired when the receptor has been loaded with no Valid Data.
        /// </summary>
        public event EventHandler ReceptorInvalidDataBind;

        /// <summary>
        /// Fired when a new receptor is needed.
        /// </summary>
        public event EventHandler AddNewReceptor;
        
        /// <summary>
        /// Fired when a new concept is needed.
        /// </summary>
        public event EventHandler AddNewConcept;
        
        /// <summary>
        /// Event fired when a concept is added to the list of the bill.
        /// </summary>
        public event Billings.ConceptAfterAddToBillEventHandler ConceptAfterAddToBill;
        
        /// <summary>
        /// Event fired when a client request to be modify.
        /// </summary>
        public event ClientModifyEventHandler ClientModify;
        
        /// <summary>
        /// Event fired when a billing is binding by ID.
        /// </summary>
        public event DataBindFromBillingIDEventHandler DataBindBillingID;

        /// <summary>
        /// Get or set a BillingsDetailNotesDataTable that is used to add ghost concepts
        /// </summary>
        protected PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesDataTable DataTableGridViewNoCalculableConcept
        {
            get
            {
                object o;
                PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesDataTable table;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull() || o.GetType() != typeof(PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesDataTable))
                {
                    table = new PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesDataTable();
                    this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = table;
                    o = table;
                }

                return (PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or Set a System.Data.DataTable temporal table with the current
        /// selected articles for the billing.
        /// </summary> 
        protected System.Data.DataTable DataTableGridView
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull() || o.GetType() != typeof(System.Data.DataTable))
                {
                    this.OnCreateGridDataTable();
                    o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                }

                return (System.Data.DataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get the main information of the control.
        /// </summary>        
        [System.ComponentModel.Browsable(false)]
        public BillingsEditorInfo BillingInformation { get; private set; }
        
        /// <summary>
        /// Get or Set a BillingsEditorMode value with the editor mode.
        /// </summary>
        public BillingsEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return BillingsEditorMode.Add;

                return o.GetType() == typeof(BillingsEditorMode) ? (BillingsEditorMode)o : BillingsEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;

                if (value == BillingsEditorMode.View)
                    this.Enabled = false;
            }
        }
        
        /// <summary>
        /// Get or Set a boolean value with enable state of the control.
        /// </summary>
        public bool Enabled
        {
            get { return this.gdvArticles.Enabled; }
            set
            {
                //this.txtTotalByHand.Enabled =
                this.txtReceptorConditions.Enabled =
                    this.ddlPaymentMethod.Enabled =
                    this.txtExchangeRate.Enabled =
                    this.ddlCurrencyType.Enabled =
                    //this.ddlReceptorInternalBillingType.Enabled =
                    this.gdvArticles.Enabled =
                    this.divSearchArticle.Visible =
                    this.gdvTaxes.Enabled =
                    this.ddlIsCredit.Enabled =
                    this.gdvNotCalculableConceps.Enabled =
                    this.ddlPaymentForm.Enabled =
                    this.EditorMode == BillingsEditorMode.View ? false : value;
            }
        }
        
        /// <summary>
        /// Get or Set an integer value with the receptor ID to edit.
        /// </summary>        
        protected int ReceptorID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or Set an integer value with the billing ID to edit.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public int BillingID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or Set an integer value with the biller ID to edit.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public int BillerID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get a string value with the seal of a billing loaded.
        /// </summary>
        protected string BillingSeal
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get a string with the original string of a billings loaded.
        /// </summary>
        protected string BillingOriginalString
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;


                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        [System.ComponentModel.Browsable(false)]
        
        
        public int TaxTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or set a string value with the BillingTypeName
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string TaxTemplateName
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or set a string value with the BillingType
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string TaxTemplateType
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;


                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
      
        /// <summary>
        /// Get or set a bool value if the billing is a prebilling or a sealed billing
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public PrebillingType IsPreBilling
        {
            get
            {
                object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                return o.IsNull() || o.GetType() != typeof(PrebillingType) ? PrebillingType.None : (PrebillingType)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        [System.ComponentModel.Browsable(false)]
        protected bool ConceptsIncludeTaxes
        {
            get
            {
                object o;
                bool r;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return true;

                if (!bool.TryParse(o.ToString(), out r))
                    return true;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// Get or set the currency type used.
        /// </summary>
        public PACFD.Common.CurrencyType CurrencyBillerType
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return CurrencyType.None;

                return o.GetType() == typeof(CurrencyType) ? (CurrencyType)o : CurrencyType.None;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        public BillingsEditor()
        {
            this.BillingInformation = this.OnCreateBillingsEditorInfo();
        }
        protected void imgbtnDeleteNotCalculableConcept_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton b;
            int i;
            System.Data.DataRow[] rows;

            if (sender.IsNull() || (b = sender.GetType() != typeof(ImageButton) ? null : (ImageButton)sender).IsNull() || !int.TryParse(b.CommandArgument, out i))
                return;

            rows = this.DataTableGridViewNoCalculableConcept.Select(string.Format("BillingDetailNoteID={0}", i));

            if (rows.Length < 1)
                return;

            this.DataTableGridViewNoCalculableConcept.RemoveBillingsDetailNotesRow((PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow)rows[0]);
            this.gdvNotCalculableConceps.DataSource = this.DataTableGridViewNoCalculableConcept;
            this.gdvNotCalculableConceps.DataBind();
        }
        
        /// <summary>
        /// Load method of the control.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            //((PACFDManager.Default)this.Page.Master).RegistryAutoCompletExtenderOnSelectedItem(this.hflArticleSearch,
            //    this.txtAddConceptDescription_AutoCompleteExtender);

            this.SetJavaScript();

            if (this.txtDateSelector.Visible)
            {
                DateTime sdate;
                try
                {
                    if (!DateTime.TryParseExact(this.txtDateSelector.Text, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out sdate))
                        sdate = DateTime.Now;
                }
                catch
                {
                    sdate = DateTime.Now;
                }

                this.txtDateSelector_CalendarExtender.SelectedDate = sdate;
            }
            ddlPlaceDispatch.DataTextField = "PlaceDispatch";
            ddlPlaceDispatch.DataValueField = "ZipCode";

            dblGlobalInvoicePeriod.DataTextField = "Descripcion";
            dblGlobalInvoicePeriod.DataValueField = "c_Periodicidad";

            dblGlobalInvoiceMonth.DataTextField = "Descripcion";
            dblGlobalInvoiceMonth.DataValueField = "c_Meses";

            dblGlobalInvoiceYear.DataTextField = "Descripcion";
            dblGlobalInvoiceYear.DataValueField = "Year";


            txtAddConceptDescription_AutoCompleteExtender.DelimiterCharacters = "\n\r";

            if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID < 1)
            {
                this.Enabled = false;
                this.divReceptorSearch.Visible = true;
            }

            if (this.ddlCurrencyType.Visible)
            {
                this.txtExchangeRate_RangeValidator.Validate();

                if (this.ddlCurrencyType.SelectedIndex == 1)
                {
                    this.spanExchangeRate.Style["display"] = "block";
                    this.txtExchangeRate_RangeValidator.Enabled = true;
                }
                else
                {
                    this.spanExchangeRate.Style["display"] = "none";
                    this.txtExchangeRate_RangeValidator.Style["display"] = "none";
                    this.txtExchangeRate_RangeValidator.Enabled = false;
                }
            }

            if (this.IsPostBack) //exit method            
                return;

            if (this.EditorMode != BillingsEditorMode.View)
                this.txtExchangeRate.Text = string.Format("{0}", this.CurrentExchangeRate);

            this.InitializeJavaScript_DropDownListReceptorSearch();

            if (this.EditorMode != BillingsEditorMode.View)
            {
                this.DataBindBillerID();
                this.lblBillingExternalFolioText.Visible = false;
                this.ddlPlaceDispatch.Enabled = false;
            }
            else
            {
                this.txtBillingExternalFolioText.Visible = !(
                    this.lblBillingExternalFolioText.Visible =
                    this.txtBillingObservation.ReadOnly = true);
                txtStudentName.Visible = false;
                txtCurp.Visible = false;
                ddAcademyLvl.Visible = false;
                txtAutRVOE.Visible = false;
                lblStudentName.Visible = false;
                lblCurp.Visible = false;
                lblAcademyLvl.Visible = false;
                lblAutRVOE.Visible = false;
            }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
            {
                this.ddlPaymentMethod_RequiredFieldValidator.Visible = true;
                this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                this.ddlPlaceDispatch.Enabled = true;
            }
            else
            {
                this.ddlPaymentMethod_RequiredFieldValidator.Visible = false;
                this.tablePlaceDispatch.Visible = false;
            }

        }
        
        /// <summary>
        /// Add the needed javascript used be the control to the page.
        /// </summary>
        private void SetJavaScript()
        {
            List<string> slist;
            string s;

            if (this.EditorMode == BillingsEditorMode.View)
            {
                this.lblAccountNumberPayment.Visible = true;
                this.txtAccountNumberPayment.Enabled = false;
                this.txtAccountNumberPayment.Visible = true;
            }
            else
            {
                this.txtAccountNumberPayment.Style["display"] =
                    this.lblAccountNumberPayment.Style["display"] = this.ddlPaymentMethod.SelectedIndex > 2 ? "block" : "none";
            }

            if (this.IsPostBack)
                return;

            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "BillingEditor.js", "../Billings/BillingsEditor.js");

            slist = new List<string>();
            //slist.Add(String.Format("ClientID                                       ='{0}'", this.ClientID));
            slist.Add(String.Format("txtAddConceptDescription                       :'{0}'", this.txtAddConceptDescription.ClientID));
            slist.Add(String.Format("imgConceptNotFound                             :'{0}'", this.imgConceptNotFound.ClientID));
            slist.Add(String.Format("hflArticleSearch                               :'{0}'", this.hflArticleSearch.ClientID));
            slist.Add(String.Format("txtAddConceptCode                              :'{0}'", this.txtAddConceptCode.ClientID));
            slist.Add(String.Format("lblCurrencyType                                :'{0}'", this.lblCurrencyType.ClientID));
            slist.Add(String.Format("ddlCurrencyType                                :'{0}'", this.ddlCurrencyType.ClientID));
            slist.Add(String.Format("spanExchangeRate                               :'{0}'", this.spanExchangeRate.ClientID));
            slist.Add(String.Format("txtExchangeRate_RangeValidator                 :'{0}'", this.txtExchangeRate_RangeValidator.ClientID));
            slist.Add(String.Format("txtAddConceptDescription_AutoCompleteExtender  :'{0}'", this.txtAddConceptDescription_AutoCompleteExtender.ClientID));
            slist.Add(String.Format("spanNotConceptFound                            :'{0}'", this.spanNotConceptFound.ClientID));
            slist.Add(String.Format("ckbDateSelector                                :'{0}'", this.ckbDateSelector.ClientID));
            slist.Add(String.Format("txtDateSelector                                :'{0}'", this.txtDateSelector.ClientID));
            slist.Add(String.Format("txtAccountNumberPayment                        :'{0}'", this.txtAccountNumberPayment.ClientID));
            slist.Add(String.Format("ddlPaymentMethod                               :'{0}'", this.ddlPaymentMethod.ClientID));
            slist.Add(String.Format("lblAccountNumberPayment                        :'{0}'", this.lblAccountNumberPayment.ClientID));
            s = string.Format("{{ {0} }}", string.Join(",", slist.ToArray()));

            s = string.Format(" var {0}_jcBillingEditor = new jclassBillingsEditor({1});",
                    this.ClientID, s);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "BillingEditorClassCreator.js", s, true);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "hflArticleSearch.js"
           , string.Format("var hflArticleSearchID = '{0}'", hflArticleSearch.ClientID), true);

            //this.txtAddConceptDescription_AutoCompleteExtender.OnClientItemSelected =
            //    string.Format("function() {{ {0}_jcBillingEditor.AutoCompletSelectedItem('{1}'); }}"
            //    , this.ClientID
            //    , this.hflArticleSearch.ClientID);

            this.txtAddConceptDescription_AutoCompleteExtender.OnClientShown = string.Format("function() {{ {0}_jcBillingEditor.OnClientShow(); }}",
                this.ClientID);
            this.txtAddConceptDescription_AutoCompleteExtender.OnClientHidden = string.Format("function() {{ {0}_jcBillingEditor.OnClientHidde(); }}",
                this.ClientID);

            this.ddlCurrencyType.Attributes["onchange"] = string.Format("javascript:{0}_jcBillingEditor.OnCurrencyTypeChanged()", this.ClientID);
            this.ddlCurrencyType.Attributes["change"] = string.Format("javascript:{0}_jcBillingEditor.OnCurrencyTypeChanged()", this.ClientID);

            this.ddlPaymentMethod.Attributes["onchange"] = string.Format("javascript:{0}_jcBillingEditor.OnddlPaymentMethodChanged()", this.ClientID);
            this.ddlPaymentMethod.Attributes["change"] = string.Format("javascript:{0}_jcBillingEditor.OnddlPaymentMethodChanged()", this.ClientID);

            if (this.EditorMode != BillingsEditorMode.View)
            {
                this.txtAccountNumberPayment.Style["display"] = "none";
                this.lblAccountNumberPayment.Style["display"] = "none";
            }
        }
        
        /// <summary>
        /// Initialize and set the javascript code used 
        /// to do click on the search button.
        /// </summary>
        protected virtual void InitializeJavaScript_DropDownListReceptorSearch()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(string.Format("<script type='text/javascript'>function {0}_SelectReceptor()", this.ClientID));
            sb.AppendLine("{");
            sb.AppendLine(string.Format("var btn = document.getElementById('{0}');", this.btnAceptReceptorSearch.ClientID));
            sb.AppendLine("if(btn == null)return;");
            sb.AppendLine("btn.click();");
            sb.AppendLine("}");
            sb.AppendLine("</script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "SelecReceptorOnDropDownChanged", sb.ToString());
            this.ddlReceptorSearch.Attributes["onchange"] = string.Format("javascript:{0}_SelectReceptor();", this.ClientID);
        }
        
        /// <summary>
        /// Generate a BillingsEditorInfo class.
        /// </summary>
        /// <returns>Return a BillingsEditorInfo.</returns>
        protected virtual BillingsEditorInfo OnCreateBillingsEditorInfo() { return new BillingsEditorInfo(this); }
        
        /// <summary>
        /// Generate a Billing.
        /// </summary>
        /// <exception cref="PACFDManager.Billings.NotDigitalCertificatesAvailableException">
        /// Throw when not a digital certificate are available in for the current biller.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotBillerFoundException">
        /// Throw when not biller found.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotReceptorFoundException">
        /// Throw when not receptor found.
        /// </exception>
        public void CreateBilling()
        {
            BillingsEditorEventArgs e;
            PACFD.Rules.Billings billing;
            PACFD.DataAccess.BillingsDataSet data;

            e = this.OnGenerateBillingsEditorEventArgs(true);
            data = e.DataSet;
            billing = new PACFD.Rules.Billings();
            billing.CalculationTaxesAndTotals(ref data);

            this.OnGeneratedBilling(e);
        }
        
        /// <summary>
        /// Bind the control by a Billing ID.
        /// </summary>
        public void DataBindFromBillingID()
        {
            ListItem l;
            PACFD.DataAccess.BillingsDataSet da;
            System.Data.DataRow row;
            int i;
            PACFD.Rules.ElectronicBillingType el;
            BillingsEditorEventArgs args;

            if (this.BillingID < 1)
                return;

            using (da = (new PACFD.Rules.Billings()).GetFullBilling(this.BillingID))
            {
                if (da.IsNull())
                    return;

                if (da.Billings.Count < 1)
                    return;

                this.BillerID = da.Billings[0].BillerID;
                this.DataBindFromBillingID_Biller(da.BillingsBillers);
                //this.DataBindBillerID();
                this.ReceptorID = da.Billings[0].ReceptorID;
                this.DataBindFromBillingID_Receptor(da.BillingsReceptors);
                //this.InitializeLoadReceptor();

                foreach (var d in da.BillingsDetails)
                {
                    row = this.BillingInformation.ArticlesGrid.NewRow();
                    row[BillingsEditorColumns.Amount] = d.UnitValue;
                    row[BillingsEditorColumns.ApplyTaxes] = d.AppliesTax;
                    row[BillingsEditorColumns.ArticleID] = d.ConceptID;
                    row[BillingsEditorColumns.BillingsDetailsID] = d.BillingsDetailsID;
                    row[BillingsEditorColumns.Count] = d.Count.ToString("0.####");
                    row[BillingsEditorColumns.Description] = d.Description;
                    row[BillingsEditorColumns.Discount] = d.DiscountPercentage;
                    row[BillingsEditorColumns.ImporteTotal] = d.Amount;
                    row[BillingsEditorColumns.Name] = d.Unit;
                    row[BillingsEditorColumns.TaxeID] = d.BillingID;
                    row[BillingsEditorColumns.TaxRatePercentage] = 0;
                    this.BillingInformation.ArticlesGrid.AddRow(row);
                    row = null;
                }

                this.BillingInformation.ArticlesGrid.DataBind();
                i = -1;

                foreach (ListItem item in this.BillingInformation.PaymentMethodItems)
                {
                    i++;

                    if (item.Text != da.Billings[0].PaymentMethod)
                        continue;

                    this.BillingInformation.PaymentMethodIndex = i;
                    break;
                }

                this.lblCompanyYear.Text = da.Billings[0].IsApprovalYearNull() ? "(No aplica)" : da.Billings[0].ApprovalYear.ToString();
                this.lblCompanyAprovalNumberText.Text = da.Billings[0].IsApprovalNumberNull() ? "(No aplica)" : da.Billings[0].ApprovalNumber.ToString();

                this.BillingInformation.IsCreditIndex = da.Billings[0].IsCredit ? 1 : 0;
                this.BillingInformation.Conditions = da.Billings[0].PaymentTerms;
                this.BillingOriginalString = da.Billings[0].OriginalString;
                this.BillingSeal = da.Billings[0].Seal;

                this.IsPreBilling = da.Billings[0].PreBilling && da.Billings[0].IsFolioIDNull() ? PrebillingType.PrebillingNotSerialFolio :
                    da.Billings[0].PreBilling && !da.Billings[0].IsFolioIDNull() ? PrebillingType.Prebilling : PrebillingType.None;

                this.lblCompanySerieFolio.Text = string.Format("{0} - {1}", da.Billings[0].Serial, da.Billings[0].Folio);
                this.txtBillingObservation.Text = da.Billings[0].IsObservationsNull() ? string.Empty : da.Billings[0].Observations;
                this.lblBillingExternalFolioText.Text = da.Billings[0].IsExternalFolioNull() ? string.Empty : da.Billings[0].ExternalFolio;

                el = (PACFD.Rules.ElectronicBillingType)da.Billings[0].ElectronicBillingType;

                //el == PACFD.Rules.ElectronicBillingType.CFD || el == PACFD.Rules.ElectronicBillingType.CFDI ||
                //    el == PACFD.Rules.ElectronicBillingType.CFD2_2 ||

                if (el == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                {
                    this.lblCertificateNumber.Visible = this.lblCertificateNumberTitle.Visible = true;
                    this.lblCertificateNumber.Text = da.Billings[0].CertificateNumber;
                //}

                //if (el == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                //{
                    this.lblUUID.Visible = this.lblUUIDTitle.Visible = true;
                    this.lblUUID.Text = da.Billings[0].IsUUIDNull() ? string.Empty : da.Billings[0].UUID;
                    this.lblStampedDate.Visible = this.lblStampedDateTitle.Visible = true;
                    this.lblStampedDate.Text = string.Format("{0:dd/MM/yyyy hh:mm}", da.Billings[0].IsDateStampedNull() ? (DateTime?)null : da.Billings[0].DateStamped);
                    this.lblCertificateNumberSAT.Visible = this.lblCertificateNumberSATTitle.Visible = true;
                    this.lblCertificateNumberSAT.Text = da.Billings[0].IsCertificateNumberSatNull() ? string.Empty : da.Billings[0].CertificateNumberSat;
                }

                switch (da.Billings[0].CurrencyCode.Trim().ToLower())
                {
                    case "mxn": this.CurrencyBillerType = CurrencyType.MXN; break;
                    case "usd": this.CurrencyBillerType = CurrencyType.USD; break;
                }

                ///Create table for totals in literal control.            
                this.BillingInformation.UpdateTotalsTable(ref da);
                this.BillingInformation.TotalInLetters = da.Billings[0].Total.ToLetters(this.CurrencyBillerType);

                this.ddlCurrencyType.ClearSelection();
                l = this.ddlCurrencyType.Items.FindByText(da.Billings[0].CurrencyCode);

                if (!l.IsNull())
                    l.Selected = true;

                this.txtExchangeRate.Text = da.Billings[0].ExchangeRateMXN.ToString();
                this.txtExchangeRate_RangeValidator.Visible = false;

                this.lblPlaceDispatch.Visible =
                this.ddlPlaceDispatch.Visible =
                this.gdvFiscalRegime.Visible = false;

                //if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                //    this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2)
                if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0 ||
                    this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
                {
                    this.gdvFiscalRegime.DataSource = (new PACFD.Rules.Billings()).FiscalRegimeTable.GetByBillerID(this.CurrentBillerID);
                    this.gdvFiscalRegime.DataBind();

                    this.dblGlobalInvoicePeriod.DataSource = (new PACFD.Rules.BillingsGeneralPeriod()).SelectByBillingID(this.BillingID);
                    this.dblGlobalInvoicePeriod.DataBind();



                    this.lblPlaceDispatch.Visible =
                        this.ddlPlaceDispatch.Visible =
                        this.gdvFiscalRegime.Visible = true;

                    this.ddlPlaceDispatch.ClearSelection();
                    this.ddlPlaceDispatch.Items.Clear();
                    this.ddlPlaceDispatch.SelectedIndex = -1;
                    this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
                    this.ddlPlaceDispatch.DataBind();
                    this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                    this.ddlPlaceDispatch.SelectedIndex = 0;

                    if (this.ddlPlaceDispatch.Items.Count > 1)
                        this.ddlPlaceDispatch.SelectedIndex = 1;
                }

                this.gdvNotCalculableConceps.DataSource = da.BillingsDetailNotes;
                this.gdvNotCalculableConceps.DataBind();
            }

            if (this.EditorMode == BillingsEditorMode.View)
            {
                this.gdvNotCalculableConceps.Columns[this.gdvNotCalculableConceps.Columns.Count - 1].Visible =
                    this.tableNotCalculableConceps.Visible =
                    this.divReceptorSearch.Visible = false;
                this.Enabled = false;

                this.divNotCalculableConcepts.Visible = this.gdvNotCalculableConceps.Rows.Count > 0;
                this.ddlPlaceDispatch.Enabled = false;
            }

            if (!da.Billings[0].IsAccountNumberPaymentNull())
            {
                this.lblAccountNumberPayment.Visible = true;
                this.txtAccountNumberPayment.Enabled = false;
                this.txtAccountNumberPayment.Text = da.Billings[0].AccountNumberPayment;
            }

            args = new BillingsEditorEventArgs(da, this.EditorMode);
            this.OnDataBindBillingID(args);
        }
        protected virtual void OnDataBindBillingID(BillingsEditorEventArgs e)
        {
            if (!this.DataBindBillingID.IsNull())
                this.DataBindBillingID(this, e);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        protected void DataBindFromBillingID_Biller(PACFD.DataAccess.BillingsDataSet.BillingsBillersDataTable table)
        {
            if (table.Count < 1)
                return;

            this.lblCompanyMainTitle.Text = table[0].BillerName;
            this.lblCompanySubTitle.Text = table[0].Reference;
            this.lblCompanyRFCText.Text = table[0].BillerRFC;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        protected void DataBindFromBillingID_Receptor(PACFD.DataAccess.BillingsDataSet.BillingsReceptorsDataTable table)
        {

            if (table.Count < 1)
                return;

            this.lblReceptorNameText.Text = table[0].ReceptorName;
            this.lblReceptorRFCText.Text = table[0].ReceptorRFC;
            this.lblReceptorAddressText.Text = table[0].ReceptorAddress;
            this.lblReceptorCPText.Text = table[0].Zipcode.ToString() == "-1" ? String.Empty : table[0].Zipcode.ToString();
            this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}", table[0].Location, table[0].State, table[0].Country);
            this.lblReceptorFiscalRegimeText.Text = "No Definido.. ";

            //taxsystem == fiscalRegime.
            if (table[0]["TaxSystem"] != null && table[0]["TaxSystem"].ToString() != string.Empty)
            {
                PACFD.DataAccess.BillingCatalogs.CatRegimenFiscalRow  row;
                row = (PACFD.DataAccess.BillingCatalogs.CatRegimenFiscalRow) Helpers.GetTaxSystem().Select("c_RegimenFiscal = '" + table[0].TaxSystem + "'").FirstOrDefault();
                if (row != null)
                lblReceptorFiscalRegimeText.Text = row.Descripcion ;
            }
           
            btnClientEdit.Visible = 
            btnSelectAnotherReceptor.Visible = false;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnReceptorDataBind(EventArgs e)
        {
            if (!this.ReceptorDataBind.IsNull())
                this.ReceptorDataBind(this, e);
        }

        protected void OnReceptorInvalidDataBind(EventArgs e)
        {
            if (!this.ReceptorInvalidDataBind.IsNull())
                this.ReceptorInvalidDataBind(this, e);
        }


        /// <summary>
        /// Initialize biller data info and the receptor search box.
        /// </summary>
        ///<returns>If success return true else false.</returns>
        public bool DataBindBillerID()
        {
            PACFD.Rules.Billers biller;
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;
            PACFD.Rules.Receptors receptor;
            ListItem l;

            biller = new PACFD.Rules.Billers();

            using (table = biller.SelectByID(this.BillerID))
            {
                if (table.Count < 1)
                    return false;

                this.lblCompanyMainTitle.Text = table[0].Name;
                this.lblCompanySubTitle.Text = table[0].Reference;
                this.lblCompanyRFCText.Text = table[0].RFC;

                this.lblCurrencyType.Visible =
                    this.ddlCurrencyType.Visible =
                    this.txtExchangeRate.Visible =
                    this.txtExchangeRate_RangeValidator.Visible =
                    this.spanExchangeRate.Visible = table[0].IsIsDictaminatedNull() ? false : table[0].IsDictaminated;

                if (this.ddlCurrencyType.Visible)
                {
                    this.ddlCurrencyType.ClearSelection();
                    l = this.ddlCurrencyType.Items.FindByText(table[0].CurrencyCode);

                    if (!l.IsNull())
                        l.Selected = true;
                    else
                        this.ddlCurrencyType.Items[0].Selected = true;
                }

                switch (table[0].CurrencyCode.Trim().ToLower())
                {
                    case "mxn":
                        this.CurrencyBillerType = CurrencyType.MXN;
                        this.spanExchangeRate.Style["display"] = "none";
                        break;
                    case "usd":
                        this.ddlCurrencyType.ClearSelection();
                        this.ddlCurrencyType.SelectedIndex = 1;
                        this.CurrencyBillerType = CurrencyType.USD;
                        break;
                    default: this.CurrencyBillerType = CurrencyType.None; break;
                }

                if (this.EditorMode == BillingsEditorMode.Add)
                {
                    this.lblCompanySerieFolio.Text =
                        this.lblCompanyYear.Text =
                        this.lblCompanyAprovalNumberText.Text = "(pendiente)";
                }
            }

            receptor = new PACFD.Rules.Receptors();
            this.ddlReceptorSearch.DataSource = receptor.SelectByBillersID(this.BillerID);
            this.ddlReceptorSearch.DataTextField = "Name";
            this.ddlReceptorSearch.DataValueField = "ReceptorID";
            this.ddlReceptorSearch.DataBind();
            this.ddlReceptorSearch.Items.Insert(0, new ListItem("[Seleccionar]", "-1"));
            receptor = null;

            if (this.ddlReceptorSearch.Items.Count > NUMBER_OF_CLIENT_BEFORE_SEARCH) //modify this to vary count of receptor display on search
            {
                this.ddlReceptorSearch.Items.Clear();
                this.ddlReceptorSearch.Visible = false;
                this.btnAceptReceptorSearch.Attributes["style"] = "display:block";
                //this.ttpAceptReceptorSearch.Visible =
                this.txtReceptorSearch.Visible = true;
            }
            else
            {
                this.btnAceptReceptorSearch.Attributes["style"] = "display:none";
                //this.ttpAceptReceptorSearch.Visible =
                this.txtReceptorSearch.Visible = false;
            }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
            {
                this.gdvFiscalRegime.Visible = true;
                this.gdvFiscalRegime.DataSource = (new PACFD.Rules.Billers()).FiscalRegimeTable.GetByBillerID(this.CurrentBillerID);
                this.gdvFiscalRegime.DataBind();
            }

            return true;
        }
        
        /// <summary>
        /// Data bind a receptor from receptor ID
        /// </summary>
        public void DataBindReceptorID()
        {
            if (this.ReceptorID < 0)
                return;

            this.InitializeLoadReceptor();
        }
        
        /// <summary>
        /// Initialize the receptor of the billing (client). Use this to initialize any control
        /// </summary>
        protected void InitializeLoadReceptor()
        {
            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
            //PACFD.Rules.CountrySelectors select;
            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable countrytable;

            table = receptor.SelectByID(this.ReceptorID);// .SelectByBillersID(this.ReceptorID);//this.BillerID);

            if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID < 1)
            {
                this.Enabled = false;
                this.divReceptorSearch.Visible = true;
            }
            else if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID > 0)
            {
                this.Enabled = true;
                this.divSearchArticle.Visible = true;
            }

            if (table.Count < 1)
                return;

            this.lblReceptorNameText.Text = table[0].Name;
            this.lblReceptorRFCText.Text = table[0].RFC;
            this.lblReceptorAddressText.Text = table[0].Address;
            this.lblReceptorCPText.Text = table[0].Zipcode.ToString() == "-1" ? String.Empty : table[0].Zipcode.ToString();
            this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}", table[0].Location, table[0].State, table[0].Country);
            lblReceptorFiscalRegimeText.Text = "No Definido";
            //taxsystem == fiscalRegime.
            if (table[0]["FiscalRegime"] != null && table[0]["FiscalRegime"].ToString() != string.Empty)
            {
                PACFD.DataAccess.BillingCatalogs.CatRegimenFiscalRow row;
                row = (PACFD.DataAccess.BillingCatalogs.CatRegimenFiscalRow)Helpers.GetTaxSystem().Select("c_RegimenFiscal = '" + table[0].FiscalRegime + "'").FirstOrDefault();
                if (row != null)
                    lblReceptorFiscalRegimeText.Text = row.Descripcion;
            }

            table.Dispose();

            if (this.EditorMode == BillingsEditorMode.Add)
                this.btnClientEdit.Visible =
                    this.tableNotCalculableConceps.Visible =
                    this.btnSelectAnotherReceptor.Visible = true;
            else
                this.btnClientEdit.Visible =
                    this.tableNotCalculableConceps.Visible =
                    this.btnSelectAnotherReceptor.Visible = false;

            this.ddlPlaceDispatch.ClearSelection();
            this.ddlPlaceDispatch.Items.Clear();
            this.ddlPlaceDispatch.SelectedIndex = -1;
            this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
            this.ddlPlaceDispatch.DataBind();
            this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlPlaceDispatch.SelectedIndex = 0;

            if (this.ddlPlaceDispatch.Items.Count > 1)
                this.ddlPlaceDispatch.SelectedIndex = 1;

            this.ddlPlaceDispatch.Enabled = this.ddlPlaceDispatch.Items.Count > 0 && this.EditorMode == BillingsEditorMode.Add &&
                (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                ;

            // crea la periodicidad aunque no se ocupe..

            this.dblGlobalInvoicePeriod.ClearSelection();
            this.dblGlobalInvoicePeriod.Items.Clear();
            this.dblGlobalInvoicePeriod.SelectedIndex = -1;
            this.dblGlobalInvoicePeriod.DataSource = Helpers.GetPeriodsSystem();
            this.dblGlobalInvoicePeriod.DataBind();
            this.dblGlobalInvoicePeriod.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.dblGlobalInvoicePeriod.SelectedIndex = 0;

            this.dblGlobalInvoiceMonth.ClearSelection();
            this.dblGlobalInvoiceMonth.Items.Clear();
            this.dblGlobalInvoiceMonth.SelectedIndex = -1;
            this.dblGlobalInvoiceMonth.DataSource = Helpers.GetMonthsSystem();
            this.dblGlobalInvoiceMonth.DataBind();
            this.dblGlobalInvoiceMonth.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.dblGlobalInvoiceMonth.SelectedIndex = 0;

            this.dblGlobalInvoiceYear.ClearSelection();
            this.dblGlobalInvoiceYear.Items.Clear();
            this.dblGlobalInvoiceYear.SelectedIndex = -1;
            //this.dblGlobalInvoiceYear.DataSource = Helpers.GetPeriodsSystem();
            //this.dblGlobalInvoiceYear.DataBind();
            this.dblGlobalInvoiceYear.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.dblGlobalInvoiceYear.Items.Insert(1, new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
            this.dblGlobalInvoiceYear.Items.Insert(2, new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));

            this.dblGlobalInvoiceYear.SelectedIndex = 0;

            this.divDateSelector.Visible =
                this.EditorMode == BillingsEditorMode.Add && (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2
              || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD);
            this.txtDateSelector_CalendarExtender.SelectedDate = DateTime.UtcNow;
            this.txtDateSelector_CalendarExtender.Format = "dd/MM/yyyy hh:mm tt";

            // Aunque el receptor este cargado enviaremos un mensaje y desactivaremos el boton de generar o timbra: 
            // if (!IsReceptorValidToCFDI4_0())helpe

            if (table[0].IsFiscalRegimeNull() || int.Parse(table[0].Zipcode) == 0 || table[0].IsCFDIUseNull())
            {
                this.WebMessageBox1.ShowMessage("Receptor no cumple con CFDI4.0, CodigoPostal y Regimen Fiscal Requeridos, Actualice los datos antes de timbrar ", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                this.OnReceptorInvalidDataBind(EventArgs.Empty);
            }
            else
            {
                // si y solo si cumple con este dato. 
                this.OnReceptorDataBind(EventArgs.Empty);
            }

            // SI EL RECEPTOR ES PUBLICO EN GENERAL:: 
            // this.lblReceptorRFCText.Text = table[0].RFC;
            if (( this.lblReceptorRFCText.Text == "XAXX010101000" || this.lblReceptorRFCText.Text == "XEXX010101000 ") && 
                this.lblReceptorNameText.Text.ToUpper() != "CLIENTE")
            {
                this.divGlobalInvoiceData.Visible = true;
            }



        }
        
        /// <summary>
        /// Fire the ApplyChange event of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected void OnGeneratedBilling(BillingsEditorEventArgs e)
        {
            if (!this.GeneratedBilling.IsNull())
                this.GeneratedBilling(this, e);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnArticleBeforeAdd(BillingsEditorArticleAddEventArgs e)
        {
            if (!this.ArticleBeforeAdd.IsNull())
                this.ArticleBeforeAdd(this, e);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnArticleAfterAdd(BillingsEditorArticleAddEventArgs e)
        {
            if (!this.ArticleAfterAdd.IsNull())
                this.ArticleAfterAdd(this, e);
        }
        
        /// <summary>
        /// Generate a BillingsEditorEventArgs class used as arguments by the control.
        /// </summary>
        /// <param name="validateconcepts">Add concepts to data set.</param>
        /// <returns>Return a BillingsEditorEventArgs class.</returns>
        /// <exception cref="PACFDManager.Billings.NotDigitalCertificatesAvailableException">
        /// Throw when not a digital certificate are available in for the current biller.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotBillerFoundException">
        /// Throw when not biller found.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotReceptorFoundException">
        /// Throw when not receptor found.
        /// </exception> 
        protected BillingsEditorEventArgs OnGenerateBillingsEditorEventArgs(bool validateconcepts)
        {
            DateTime outtime = DateTime.Now;
            decimal exchangerate = this.CurrentExchangeRate;
            //declaration...
            PACFD.DataAccess.BillingsDataSet.BillingsRow rbill;
            PACFD.DataAccess.BillingsDataSet data = new PACFD.DataAccess.BillingsDataSet();

            ///Billings table general data
            rbill = data.Billings.NewBillingsRow(); //create new row
            rbill.BeginEdit();
            rbill.Active = true;

            if (this.BillerID < 1)
                throw new Billings.NotBillerFoundException("Biller not found.", this.BillerID);

            //if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI3_3)
            //    throw new Billings.NotPlaceDispatchSelected("no se permite");
            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
            {
                if (this.ddlPlaceDispatch.SelectedIndex < 1 && this.ddlPlaceDispatch.Visible)
                    throw new Billings.NotPlaceDispatchSelected("Not place dispatch selected.");

                if ( (this.lblReceptorRFCText.Text == "XAXX010101000" || this.lblReceptorRFCText.Text == "XEXX010101000 ") && 
                    this.lblReceptorNameText.Text.ToUpper() != "CLIENTE") 
                {
                    // debe tener los datos.
                    if (dblGlobalInvoiceMonth.SelectedValue=="0" || dblGlobalInvoicePeriod.SelectedValue=="0" || dblGlobalInvoiceYear.SelectedValue == "0")
                    {
                        throw new Billings.NotGlobalInvoiceData("Faltan los Datos Globales Publico General");
                    }
                }
            }



            rbill.BillerID = this.BillerID;

            if (this.divDateSelector.Visible && this.ckbDateSelector.Checked && this.txtDateSelector.Visible == true)
            {
                try
                {
                    if (!DateTime.TryParseExact(this.txtDateSelector.Text, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out outtime))
                        throw new CantParseBillingDate("Can't parse date, when trying to add billing CFD. Date: " + this.txtDateSelector.Text);
                }
                catch
                {
                    throw new CantParseBillingDate("Can't parse date, when trying to add billing CFD. Date: " + this.txtDateSelector.Text);
                }

                if ((new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day)) < (new DateTime(outtime.Year, outtime.Month, outtime.Day)))
                    throw new CantParseBillingDate("Date is set on the future :o Date: " + this.txtDateSelector.Text + " != " + DateTime.UtcNow.ToString("dd/MM/yyyy"));
            }
            else
                outtime = DateTime.Now;

            rbill.BillingDate = outtime;
            rbill.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
            rbill.BillingType = this.TaxTemplateType;

            if (this.BillerID < 1)
                throw new Billings.NotReceptorFoundException("Receptor not found.", this.ReceptorID);

            rbill.ReceptorID = this.ReceptorID;
            //Se cambiaron los valores ahora paymentmethod es paymentform LUL sat pls
            rbill.PaymentMethod = this.ddlPaymentForm.SelectedItem.Value;
            rbill.PaymentForm = this.BillingInformation.PaymentMethod == null ? null :
                this.BillingInformation.PaymentMethod.Text.Trim() == this.ddlPaymentMethod.Items[0].Text.Trim() ?
                string.Empty : this.BillingInformation.PaymentMethod.Value;
            rbill.PaymentTerms = string.Format("{0}{1}", this.BillingInformation.Conditions,
                this.CurrencyBillerType == CurrencyType.USD ?
                (this.BillingInformation.Conditions.Trim().Length > 0 ? ", " : string.Empty) + "tipo de cambio " + this.txtExchangeRate.Text : string.Empty
                );
            rbill.IsCredit = this.ddlIsCredit.SelectedIndex > 0 ? true : false;
            rbill.SubTotal = 0;
            rbill.Total = 0;
            rbill.Discount = 0;
            rbill.InternalBillingType = this.TaxTemplateName;
            //rbill.CurrencyCode = this.CurrencyBillerType.ToLetterString();
            rbill.CurrencyCode = this.ddlCurrencyType.SelectedItem.Text;
            rbill.DiscountDescription = string.Empty;
            rbill.TaxTemplateName = this.TaxTemplateName;
            rbill.ElectronicBillingType = (int)this.CurrentElectronicBillingType; //electronic billing type 
            rbill.InsertOrigin = 1;
            rbill.PlaceDispatch = //----------lugar de expedición
                (( this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                && this.ddlPlaceDispatch.SelectedIndex > 0 && this.ddlPlaceDispatch.Visible) ? this.ddlPlaceDispatch.SelectedItem.Value : null;
            rbill.ExchangeRateMXN = 1;

            this.txtAccountNumberPayment.Text = this.txtAccountNumberPayment.Text.Trim();

            if ((this.txtAccountNumberPayment.Text.Length > 0 && this.txtAccountNumberPayment.Text.Length < 4) || this.txtAccountNumberPayment.Text.Length > 250)
                throw new NotAccountNumberFound("Account number must be empty or minimum 4 digits max 250");

            rbill.AccountNumberPayment = this.txtAccountNumberPayment.Text.Length >= 4 ?
                this.txtAccountNumberPayment.Text : string.Empty;

            if (rbill.CurrencyCode == "USD")
            {
                if (!decimal.TryParse(this.txtExchangeRate.Text, out exchangerate))
                    throw new Exception("Can't convert exchange rate");

                rbill.ExchangeRateMXN = exchangerate;
            }

            if (this.CurrencyBillerType == CurrencyType.None)
                throw new Billings.NotReceptorFoundException("Currency type is not valid.", this.ReceptorID);

            //ignore IsPreBilling property to secure accurate CFD or CBB usage when sealling
            rbill.PreBilling = this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ?
                this.IsPreBilling == PrebillingType.Prebilling || this.IsPreBilling == PrebillingType.PrebillingNotSerialFolio ? true : false
                : true;
            //this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? this.IsPreBilling : true;
            //this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? false : true;

            if (!string.IsNullOrEmpty(this.txtBillingExternalFolioText.Text))
                rbill.ExternalFolio = this.txtBillingExternalFolioText.Text;
            else rbill.SetExternalFolioNull();

            if (!string.IsNullOrEmpty(this.txtBillingObservation.Text))
                rbill.Observations = this.txtBillingObservation.Text;
            else rbill.SetObservationsNull();

            rbill.PrintTemplateID = this.GetPrintTemplateID();
            rbill.Certificate =
                rbill.CertificateNumber =
                string.Empty;

            this.FillBillings_SerialRowDataSet(ref rbill); //fill serial table values

            if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.Indeterminate)
                this.FillBillings_DigitalCertificateRowDataSet(ref rbill); //fill digital certificates values

            rbill.Version =
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0 ? "4.0" :
                 string.Empty;

            rbill.OriginalString = string.Empty; //tempo value for original string
            rbill.Seal = string.Empty;
            rbill.EndEdit();
            data.Billings.AddBillingsRow(rbill); //add row to table

            //this order must be respected.
            this.FillBillings_BillersDataSet(ref data); //fill billers data set
            this.FillBillings_ReceptorDataSet(ref data); //fill receptor data set

            // agregamos el agrùpador padre para los impuestos
            this.FillBillings_TaxesDataSet(ref data);

            // Agregamos los conceptos. 
            if (validateconcepts)
                this.FillBillings_DetailsDataSet(ref data);  //fill detials data set


            // calculamos los impuestos trasladados de todos lo conceptos. 
            this.FillBillings_TransferTaxesDataSet(ref data);

            
            // calculamos la retencion de impuestos si el concepto aplica retencion. 
            // dependera tambien del regimen fiscal.. por lo que ese dato debe estar presente en el data..
            this.FillBillings_DetainedTaxesDataSet(ref data);


            this.FillBillings_IssuedDataSet(ref data);

            // insertemos los datos de el nodo Facturacion Global. SI ES: 
            if ((this.lblReceptorRFCText.Text == "XAXX010101000" || this.lblReceptorRFCText.Text == "XEXX010101000 ") && 
                this.lblReceptorNameText.Text.ToUpper() != "CLIENTE")  // si es solo una cliente y no es global. no lleva el dato.  
            {
                this.FillBillings_GneralInvoiceDataSet(ref data);
            }

                //----
                //uncomment this line for not calculable concept update
                //----
                //this.FillBillings_BillingsDetailNotes(ref data);

            foreach (var r in (new PACFD.Rules.Billers()).FiscalRegimeTable.GetByBillerID(this.CurrentBillerID))
            {
                data.BillingFiscalRegime.AddBillingFiscalRegimeRow(r.Regime, rbill);
                data.BillingFiscalRegime[data.BillingFiscalRegime.Count - 1].AcceptChanges();
                data.BillingFiscalRegime[data.BillingFiscalRegime.Count - 1].SetParentRow(rbill);
                data.BillingFiscalRegime[data.BillingFiscalRegime.Count - 1].SetAdded();
            }

            //data = e.DataSet;
            //var billing = new PACFD.Rules.Billings();
            //billing.CalculationTaxesAndTotals(ref data);

            rbill.BeginEdit();
            rbill.OriginalString = string.Empty;
            rbill.Seal = string.Empty;
            rbill.EndEdit();
            rbill.AcceptChanges(); //apply changes to row.

            if (this.EditorMode == BillingsEditorMode.Add)
                rbill.SetAdded();

            return new BillingsEditorEventArgs(data, this.EditorMode);
        }
        
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsRow row with the serial and folio 
        /// values needed.
        /// </summary>
        /// <param name="billingrow">Billings row to be filled.</param>
        protected void FillBillings_SerialRowDataSet(ref PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow)
        {
            PACFD.Rules.Series serial = new PACFD.Rules.Series();
            PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table;

            using (table = serial.GetUnusedSerialByBillerID(this.BillerID, this.CurrentBranchID))
            {
                serial = null;

                if (table.Count < 1 && this.IsPreBilling != PrebillingType.PrebillingNotSerialFolio)
                    throw new NotValidSeriesOrFolios("Series or folios not available.");

                if (this.IsPreBilling == PrebillingType.PrebillingNotSerialFolio)
                {
                    billingrow.SetApprovalNumberNull();
                    billingrow.SetApprovalYearNull();
                    //TODO: Campo exportacion cambiar... 
                    billingrow.Exportacion = "01";
                    billingrow.Serial = string.Empty;
                    billingrow.Folio = string.Empty;
                    billingrow.SetFolioIDNull();
                    billingrow.SetSerialIDNull();
                    return;
                }

                if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                {
                    billingrow.SetApprovalNumberNull();
                    billingrow.SetApprovalYearNull();
                }
                else
                {
                    billingrow.ApprovalNumber = table[0].AprovationNumber;
                    billingrow.ApprovalYear = table[0].AprovationYear;
                }

                //TODO: Campo exportacion cambiar... 

                billingrow.Exportacion = "01";
                billingrow.Serial = table[0].Serial;
                billingrow.Folio = table[0].Folio;
                billingrow.FolioID = table[0].FolioID;
                billingrow.SerialID = table[0].SerialID;

                if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                {
                    if (table[0].QRImage.IsNull())
                        throw new Exception("Qr image is null.");

                    billingrow.QrImage = table[0].QRImage;
                }
            }
        }
        
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsRow row with the digital certificates values needed.
        /// </summary>
        /// <param name="billingrow">Billings row to be filled.</param>
        protected void FillBillings_DigitalCertificateRowDataSet(ref PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow)
        {
            PACFD.Rules.DigitalCertificates digital = new PACFD.Rules.DigitalCertificates();
            PACFD.DataAccess.DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table;

            using (table = digital.GetActiveByBillerID(this.BillerID))
            {
                digital = null;

                if (table.Count < 1)
                    throw new NotValidDigitalCertificatesException("Not digital certificate available for emisor.", this.BillerID);

                if (table[0].IsCertificateNumberNull())
                    throw new NotValidDigitalCertificatesException("Not valid digital certificate: CertificateNumber is null or not valid.",
                        this.BillerID, table[0].DigitalCertificateID);

                billingrow.CertificateNumber = table[0].CertificateNumber;
                billingrow.Certificate = table[0].Certificate;
                billingrow.Seal = table[0].Seal;
            }
        }
        
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsBillersRow row and add it to 
        /// a PACFD.DataAccess.BillingsDataSet data set BillingsBillers table.
        /// </summary>
        /// <param name="billingsdataset">
        /// PACFD.DataAccess.BillingsDataSet data set where the 
        /// PACFD.DataAccess.BillingsDataSet.BillingsBillers will be filled.
        /// </param>
        protected void FillBillings_BillersDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsBillersRow row;
            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow cityrow;

            using (PACFD.DataAccess.BillersDataSet.BillersDataTable table = (new PACFD.Rules.Billers()).SelectByID(this.BillerID))
            {
                if (table.Count < 1)
                    throw new Billings.NotBillerFoundException("Biller not found.", this.BillerID);

                //cityrow = this.GetCityStateCountry(table[0].Location.ToInt32());

                row = billingsdataset.BillingsBillers.NewBillingsBillersRow();
                row.BeginEdit();
                row.BillerAddress = table[0].Address;
                row.BillerName = table[0].Name;
                row.BillerRFC = table[0].RFC;
                row.BillingID = this.EditorMode == BillingsEditorMode.Edit ? this.BillerID : 0;
                row.BillingsRow = billingsdataset.Billings[0];
                row.Colony = table[0].Colony;
                row.Country = table[0].Country;//cityrow.Country;
                row.ExternalNumber = table[0].ExternalNumber;
                row.InternalNumber = table[0].InternalNumber;
                row.Location = table[0].Location;//cityrow.City;
                row.Municipality = table[0].Municipality;//cityrow.Municipality;
                row.Reference = table[0].Reference;
                row.State = table[0].State;//cityrow.State;
                row.Zipcode = table[0].Zipcode;
                row.IssuedInDifferentPlace = table[0].IssuedInDifferentPlace;
                row.TaxSystem = table[0].TaxSystem;
         
                row.EndEdit();

                billingsdataset.BillingsBillers.AddBillingsBillersRow(row);
                row.AcceptChanges();

                if (this.EditorMode == BillingsEditorMode.Add)
                    row.SetAdded();

                row.SetParentRow(billingsdataset.Billings[0]);
            }
        }
        
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsReceptorsRow row and add it to
        /// a PACFD.DataAccess.BillingsDataSet data set BillingsReceptors table.
        /// </summary>
        /// <param name="billingsdataset">
        /// PACFD.DataAccess.ReceptorsDataSet.Receptors data set where the 
        /// PACFD.DataAccess.BillingsDataSet.BillingsReceptors will be filled.
        /// </param>
        protected void FillBillings_ReceptorDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.Rules.Receptors receptor;
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
            PACFD.DataAccess.BillingsDataSet.BillingsReceptorsRow row;

            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow cityrow;


            receptor = new PACFD.Rules.Receptors();
            table = receptor.SelectByID(this.ReceptorID);
            receptor = null;

            if (table.Count < 1)
            {
                table.Dispose();
                table = null;
                throw new Billings.NotReceptorFoundException("Receptor not found.", this.ReceptorID);
            }

            //cityrow = this.GetCityStateCountry(table[0].Location.ToInt32());

            row = billingsdataset.BillingsReceptors.NewBillingsReceptorsRow();
            row.BeginEdit();
            row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
            row.BillingsRow = billingsdataset.Billings[0];
            row.Colony = table[0].Colony;
            row.Country = table[0].Country;//cityrow.Country;
            row.ExternalNumber = table[0].ExternalNumber;
            row.InternalNumber = table[0].InternalNumber;
            row.Location = table[0].Location;//cityrow.City;
            row.Municipality = table[0].Municipality;//cityrow.Municipality;
            row.ReceptorAddress = table[0].Address;
            row.ReceptorName = table[0].Name;
            row.ReceptorRFC = table[0].RFC;

            row.Reference = table[0].Reference;

            row.State = table[0].State;//cityrow.State;

            row.Zipcode = table[0].Zipcode;
            row.TaxSystem = table[0].FiscalRegime;
            row.CFDIUse = table[0].CFDIUse;

            row.EndEdit();

            billingsdataset.BillingsReceptors.AddBillingsReceptorsRow(row);
            row.AcceptChanges();

            if (this.EditorMode == BillingsEditorMode.Add)
                row.SetAdded();

            row.SetParentRow(billingsdataset.Billings[0]);
            table.Dispose();
            table = null;
        }
        
        /// <summary>
        /// Fill the BillingDetail Table with the concepts in the bill.
        /// </summary>
        /// <param name="billingsdataset">DataSet to be fill.</param>
        protected void FillBillings_DetailsDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            int tempoid = 0;
            int i;
            PACFD.Rules.Concepts concep;
            PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow row;
            PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable table;

            PACFD.Rules.Billers billers = new PACFD.Rules.Billers();
            PACFD.DataAccess.BillersDataSet.BillersDataTable billerstable;

            billerstable = billers.SelectByID(this.BillerID);
            billers = null;
            concep = new PACFD.Rules.Concepts();

            if (this.DataTableGridView.Rows.Count < 1)
                throw new Exception("Not articles added to list.");

            foreach (System.Data.DataRow r in this.DataTableGridView.Rows)
            {
                i = r[BillingsEditorColumns.ArticleID].ToString().ToInt32();

                if (i < 1)
                {
                    concep = null;
                    throw new NotValidConceptIDException("Concept (article) ID is not valid: " + i.ToString());
                }

                table = concep.SelectByID(i);

                if (table.Count < 1)
                {
                    table.Dispose();
                    table = null;
                    concep = null;
                    throw new NotConceptFoundException("Concept (article) not found.", i);
                }

                row = billingsdataset.BillingsDetails.NewBillingsDetailsRow();
                row.BeginEdit();
                row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
                row.BillingsDetailsID = this.EditorMode == BillingsEditorMode.Add ? tempoid-- :
                    r[BillingsEditorColumns.BillingsDetailsID].ToString().ToInt32();
                row.BillingsRow = billingsdataset.Billings[0];
                row.ConceptID = table[0].ConceptID;   //r[BillingsEditorColumns.ArticleID].ToString().ToInt32();
                row.UnitValue = Math.Round(r[BillingsEditorColumns.Amount].ToString().ToDecimal(), 2); /*table[0].UnitPrice;*/
                row.Count = r[BillingsEditorColumns.Count].ToString().ToDecimal().ToString("0.####").ToDecimal();
                row.Unit = table[0].UnitType.Trim().ToLower() == "[sin asignar]" || string.IsNullOrEmpty(table[0].UnitType) ?
                    ((this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0) ? "Sin Asignar" : string.Empty) : table[0].UnitType;
                row.Amount = row.UnitValue * row.Count;

                row.AppliesTax = r[BillingsEditorColumns.ApplyTaxes].ToString().ToBoolean();
                
                row.CountString = r[BillingsEditorColumns.Count].ToString().ToDecimal().ToString("0.####");
                row.Description = table[0].Description;   //r[BillingsEditorColumns.Name].ToString();
                row.DiscountPercentage = r[BillingsEditorColumns.Discount].ToString().ToDecimal();
                row.IdentificationNumber = table[0].Code;
                // impuesto aplicable al concepto. 
                row.TaxType = ddlTaxType.SelectedItem.Value;
                row.FactorType = ddlFactor.SelectedItem.Text;  // tasa o Exento

                // datos para estudiantes
                row.StudentName = txtStudentName.Text;
                row.StudentCurp = txtCurp.Text;
                row.EducationLevel = (txtStudentName.Text != String.Empty) ? ddAcademyLvl.SelectedItem.Text : "";
                row.AutrVOE = txtAutRVOE.Text;

                // by default... falta considerar cuando no lleva desglose..como el caso ·03· clientes en general. 
                row.TaxObject = row.AppliesTax ? "02" : "01";

                var defaulttax = this.BillingInformation.TaxesGrid.GetRows.FirstOrDefault(x => x.Name == "IVA");
                
                var taxrate = ddlTax.SelectedItem.Value == "0" || ddlTax.SelectedItem.Value == null ? decimal.Parse(defaulttax.Value) / 100: decimal.Parse(ddlTax.SelectedItem.Value);
                
                row.Tax = row.AppliesTax ? Math.Round((row.UnitValue * row.Count) * taxrate, 2) : 0;
                row.TasaOCuota = taxrate.ToString();
                row.ClaveProdServ = table[0].IsClavProdServNull() || string.IsNullOrEmpty(table[0].ClavProdServ) ? "01010101" : table[0].ClavProdServ;
                row.UnitType = table[0].UnitType.Trim().GetClaveUnidadName();

                var detinedrate = detainedConcept();

                row.Retained = detinedrate > 0 && row.AppliesTax ? Math.Round(row.Amount * (detinedrate / 100), 2) : 0;
                row.RetainedTasaOCuota = (detinedrate / 100).ToString();

                row.EndEdit();
                billingsdataset.BillingsDetails.AddBillingsDetailsRow(row);
                row.AcceptChanges();

                if (this.EditorMode == BillingsEditorMode.Add)
                    row.SetAdded();

                row.SetParentRow(billingsdataset.Billings[0]);
                table.Dispose();
                table = null;
            }


            billerstable.Dispose();
            billerstable = null;
            concep = null;
        }

        public decimal detainedConcept()
        {
            //PACFD.DataAccess.BillingsDataSet.TransferTaxesRow row = null;
            List<TaxesGridRow> list;

            list = this.BillingInformation.TaxesGrid.GetRows;

            //list = (
            //    from r in this.BillingInformation.TaxesGrid.GetRows
            //    where r.TaxTransfer == true
            //    select r
            //    ).ToList<TaxesGridRow>();
            decimal d = 0;
            foreach (TaxesGridRow taxrow in list)
            {
                if (taxrow.TaxTransfer)
                    continue;
                return taxrow.Text.ToDecimal();
            }
            return 0;
        }
       
        /// <summary>
        /// Fill the unique Taxes table of the data set.
        /// </summary>
        /// <param name="billingsdataset">DataSet with the Taxes table to be filled.</param>
        protected void FillBillings_TaxesDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.DataAccess.BillingsDataSet.TaxesRow row;

            row = billingsdataset.Taxes.NewTaxesRow();
            row.BeginEdit();
            row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
            row.BillingsRow = billingsdataset.Billings[0];
            row.TotalTransfer = 0;
            row.TotalDetained = 0;
            row.EndEdit();

            billingsdataset.Taxes.AddTaxesRow(row);
            row.AcceptChanges();

            if (this.EditorMode == BillingsEditorMode.Add)
                row.SetAdded();

            row.SetParentRow(billingsdataset.Billings[0]);
        }
      
        /// <summary>
        /// Fill and recalculate the TransferTaxesDataSet
        /// </summary>
        /// <param name="billingsdataset">BillingsDataSet used to get the data.</param>
        /// <param name="concepts">Consept used to search for taxes.</param>
        /// <param name="billers">Biller owner of the taxes.</param>
        protected void FillBillings_TransferTaxesDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            //foreach (PACFD.DataAccess.BillingsDataSet.BillingsDetailsRow detailRow in billingsdataset.BillingsDetails.Rows)
            //{
            //    // aplica impuesto? ademas no es ISR.
            //    if (detailRow.AppliesTax && detailRow.TaxType!="001")   
            //    {
            //        PACFD.DataAccess.BillingsDataSet.TransferTaxesRow row = billingsdataset.TransferTaxes.NewTransferTaxesRow();
            //        row.BeginEdit();
            //            row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
            //            //nombre del impuesto. solo hay 3 tipos.  ??? 
            //            row.Name = detailRow.TaxType;
            //            decimal tasa = detailRow.TasaOCuota != "" ? decimal.Parse(detailRow.TasaOCuota) : 0;

            //            row.TaxRatePercentage = tasa * 100;
            //            row.IvaAffected = detailRow.AppliesTax;
            //            row.FactorType = detailRow.FactorType;
            //            row.Base = detailRow.Amount;
            //            row.Import = detailRow.AppliesTax ? detailRow.Amount * tasa : 0;
            //        row.EndEdit();
            //        billingsdataset.TransferTaxes.AddTransferTaxesRow(row);
            //        row.AcceptChanges();

            //        if (this.EditorMode == BillingsEditorMode.Add)
            //            row.SetAdded();

            //        row.SetParentRow(billingsdataset.Taxes[0]);
            //    }
            //}


            PACFD.DataAccess.BillingsDataSet.TransferTaxesRow row = null;
            List<TaxesGridRow> list;

            list = this.BillingInformation.TaxesGrid.GetRows;

            foreach (TaxesGridRow taxrow in list)
            {
                if (!taxrow.TaxTransfer)
                    continue;

                if (!taxrow.Required && !(taxrow.RowType == TaxRowType.Editable ?
                    (string.IsNullOrEmpty(taxrow.Text) ? false : true) : (taxrow.Value == "-1" ? false : true)))
                    continue;

                row = billingsdataset.TransferTaxes.NewTransferTaxesRow();
                row.BeginEdit();
                row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
                row.Name = taxrow.Name;
                row.TaxRatePercentage = taxrow.Text.ToDecimal();
                row.IvaAffected = taxrow.IvaAffected;
                //no funcionaron
                //row.FactorType = taxrow.FactorType;
                //row.Base = taxrow.AmountBase;

                row.Import = 0;
                row.EndEdit();
                billingsdataset.TransferTaxes.AddTransferTaxesRow(row);
                row.AcceptChanges();

                if (this.EditorMode == BillingsEditorMode.Add)
                    row.SetAdded();

                row.SetParentRow(billingsdataset.Taxes[0]);
            }
        }
       
        /// <summary>
        /// Fill and recalculate the DetainedTaxesDataSet
        /// </summary>
        /// <param name="billingsdataset">BillingsDataSet used to get the data.</param>
        /// <param name="concepts">Consept used to search for taxes.</param>
        /// <param name="billers">Biller owner of the taxes.</param>
        protected void FillBillings_DetainedTaxesDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.DataAccess.BillingsDataSet.DetainedTaxesRow row = null;
            List<TaxesGridRow> taxrowlist;// = new List<TaxesGridRow>();

            //taxrowlist = (
            //    from r in this.BillingInformation.TaxesGrid.GetRows
            //    where r.TaxTransfer == false
            //    select r
            //    ).ToList<TaxesGridRow>();

            taxrowlist = this.BillingInformation.TaxesGrid.GetRows;

            foreach (TaxesGridRow taxrow in taxrowlist)
            {
                if (taxrow.TaxTransfer)
                    continue;

                if (!taxrow.Required && !(taxrow.RowType == TaxRowType.Editable ?
                    (string.IsNullOrEmpty(taxrow.Text) ? false : true) : (taxrow.Value == "-1" ? false : true)))
                    continue;

                row = billingsdataset.DetainedTaxes.NewDetainedTaxesRow();
                row.BeginEdit();
                row.BillingID = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingID;
                row.Name = string.IsNullOrEmpty(taxrow.Name) ? string.Empty : taxrow.Name;
                row.TaxRatePercentage = taxrow.Text.ToDecimal();
                row.IvaAffected = taxrow.IvaAffected;
                row.Import = 0;

                row.FactorType = taxrow.FactorType;
                row.Base = taxrow.AmountBase;

                row.EndEdit();
                billingsdataset.DetainedTaxes.Rows.Add(row);
                row.AcceptChanges();

                if (this.EditorMode == BillingsEditorMode.Add)
                    row.SetAdded();

                row.SetParentRow(billingsdataset.Taxes[0]);
            }
        }
       
        /// <summary>
        /// This method is obsolete and will be removed.
        /// </summary>
        protected void FillBillings_IssuedDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsIssuedRow row = billingsdataset.BillingsIssued.NewBillingsIssuedRow();

            if (this.CurrentBranchID < 1)
                return;

            using (PACFD.DataAccess.BranchesDataSet.BranchDataTable table = (new PACFD.Rules.Branches()).SelectByID(this.CurrentBranchID))
            {
                if (table.Count < 1)
                    return;

                row.Address = table[0].Address;
                row.BillingID = 0;
                row.Colony = table[0].Colony;
                row.Country = table[0].Country;
                row.ExternalNumber = table[0].ExternalNumber;
                row.InternalNumber = table[0].InternalNumber;
                row.Location = table[0].Location;
                row.Municipality = table[0].Municipality;
                row.Reference = table[0].Reference;
                row.State = table[0].State;
                row.Zipcode = table[0].Zipcode;
                billingsdataset.BillingsIssued.AddBillingsIssuedRow(row);
                row.AcceptChanges();
                row.SetAdded();
                billingsdataset.Billings[0].BranchID = table[0].BranchID;
            }

            row.SetParentRow(billingsdataset.Billings[0]);
        }
       
        /// <summary>
        /// Set the BillingsDetailNotes table of the Billing used as dummy concepts.
        /// </summary>
        /// <param name="billingsdataset">DataSet to be fill.</param>
        protected void FillBillings_GneralInvoiceDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            PACFD.DataAccess.BillingsDataSet.BillingGeneralPeriodRow row = billingsdataset.BillingGeneralPeriod.NewBillingGeneralPeriodRow();

            row.BillingID = this.BillingID;
            row.Period = this.dblGlobalInvoicePeriod.SelectedValue;
            row.Month = this.dblGlobalInvoiceMonth.SelectedValue;
            row.Year = int.Parse(this.dblGlobalInvoiceYear.SelectedValue);

            billingsdataset.BillingGeneralPeriod.AddBillingGeneralPeriodRow(row);
            row.AcceptChanges();
            row.SetAdded();
          
            row.SetParentRow(billingsdataset.Billings[0]);
        }
       
        /// <summary>
        /// Set the BillingsDetailNotes table of the Billing used as dummy concepts.
        /// </summary>
        /// <param name="billingsdataset">DataSet to be fill.</param>
        protected void FillBillings_BillingsDetailNotes(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            billingsdataset.BillingsDetailNotes.Merge(this.DataTableGridViewNoCalculableConcept);
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityid"></param>
        /// <returns></returns>
        protected PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow GetCityStateCountry(int cityid)
        {
            PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow row;
            PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable table = new PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable();
            PACFD.Rules.CountrySelectors select = new PACFD.Rules.CountrySelectors();

            table = select.SelectCityStateCountryByCityID(cityid);
            row = table[0];
            table.Dispose();
            table = null;
            select = null;

            return row;
        }
        
        /// <summary>
        /// Get the print ID used to print the PDF.
        /// </summary>
        /// <returns>Return an integer value.</returns>
        protected int GetPrintTemplateID()
        {
            int i;
            PACFD.Rules.TaxTemplate p = new PACFD.Rules.TaxTemplate();

            using (PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table = p.SelectByID(this.TaxTemplateID))
            {
                p = null;

                if (table.Count < 1)
                    return 1; //defaut PDF report               

                i = table[0].PrintTemplateID;
            }

            return i;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton_Click(object sender, EventArgs e)
        {
            int id;
            ImageButton b = sender as ImageButton;

            if (b.IsNull() || this.EditorMode == BillingsEditorMode.View)
                return;

            if (!int.TryParse(b.CommandArgument, out id))
                return;

            if (id - 1 < 0 || this.DataTableGridView.Rows.Count < 1)
                return;

            this.DataTableGridView.Rows[id - 1].Delete();

            if (this.DataTableGridView.Rows.Count > 0 && this.DataTableGridView.Rows.Count > (id - 1))
                this.DataTableGridView.Rows[id - 1].AcceptChanges();

            this.BillingInformation.ArticlesGrid.DataBind();
            this.BillingInformation.CalculateTotal();
        }
     
        /// <summary>
        /// Create a new System.Data.DataRow row.
        /// </summary>
        /// <returns>Return a System.Data.DataRow.</returns>
        protected System.Data.DataRow OnCreateNewRow()
        {
            ///DataTableGridView = property in this class, create a
            ///custom data table with a custom datacolumn.
            return this.DataTableGridView.NewRow();
        }
        
        /// <summary>
        /// Create a new System.Data.DataTable table an set to 
        /// DataTableGridView property.
        /// </summary>
        protected void OnCreateGridDataTable()
        {
            System.Data.DataTable table = new System.Data.DataTable();
            table.Columns.AddRange(BillingsEditorColumns.GetColumns()); //generate a custom column.
            this.DataTableGridView = table;
        }
        
        /// <summary>
        /// Add an article to the data grid.
        /// </summary>
        /// <param name="a">Selected article (concept) item.</param>
        private void AddArticleToList(BillingsArticleSearchSelected a)
        {
            PACFD.Rules.Concepts concep;
            PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable table, datasource;
            PACFD.DataAccess.ConceptsDataSet.ConceptsRow conceptrow;
            System.Data.DataRow row;

            concep = new PACFD.Rules.Concepts();
            datasource = new PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable();
            table = concep.SelectByID(a.ID);

            if (table.Count < 1)
                return;

            conceptrow = table[0];
            table.Dispose();
            table = null;

            decimal cost = a.Cost;

            if (a.IncludedTaxes)
            {
                PACFD.Rules.Billings r = new PACFD.Rules.Billings();
                r.CalculationAmount(ref cost, this.BillingInformation.Discount, a.ApplyTaxes, this.OnGenerateBillingsEditorEventArgs(false).DataSet);
            }

            row = this.BillingInformation.ArticlesGrid.NewRow();
            row[Billings.BillingsEditorColumns.ArticleID] = conceptrow.ConceptID;
            row[Billings.BillingsEditorColumns.Name] = conceptrow.UnitType.Trim().ToLower() == "[sin asignar]" ? string.Empty : conceptrow.UnitType;
            row[Billings.BillingsEditorColumns.Description] = conceptrow.Description;
            row[Billings.BillingsEditorColumns.Count] = a.Amount.ToString("0.####");
            row[Billings.BillingsEditorColumns.Discount] = this.BillingInformation.Discount;
            row[Billings.BillingsEditorColumns.Amount] = cost;
            row[Billings.BillingsEditorColumns.TaxeID] = 1;
            row[Billings.BillingsEditorColumns.BillingsDetailsID] = 0;
            row[Billings.BillingsEditorColumns.ApplyTaxes] = a.ApplyTaxes;

            row[Billings.BillingsEditorColumns.TaxRatePercentage] = conceptrow.TaxRatePercentage;
            row[Billings.BillingsEditorColumns.ImporteTotal] =
                Math.Round(((a.Amount * cost)) - (a.Amount * cost) * (this.BillingInformation.Discount / 100), 2);

            this.BillingInformation.ArticlesGrid.AddRow(row);
            row = null;

            this.BillingInformation.ArticlesGrid.DataBind();
            //this.BillingInformation.billings.Add("asdasd");
            this.BillingInformation.CalculateTotal();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public bool IsReceptorValidToCFDI4_0()
        //{
        //    PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
        //    PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
        //    table = receptor.SelectByID(this.ReceptorID);
        //    // if ( table.Count < 0 ) EN TEORIA ESTO NO PASARA:: 
          
        //    // Valida que cumpla con cfdi 4.0 codigo postal nombre y regimen fiscal. 
        //    if (table[0].IsFiscalRegimeNull() || int.Parse(table[0].Zipcode) == 0 || table[0].IsCFDIUseNull())
        //    {
        //        table.Dispose();
        //        return false ;
        //    }

        //    return true;
        //}
         
        protected void btnAceptReceptorSearch_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Receptors r;

            if (this.EditorMode == BillingsEditorMode.View)
                return;

            if (!this.txtReceptorSearch.Visible)
            {
                if (this.ddlReceptorSearch.Items.Count < 1 || this.ddlReceptorSearch.SelectedIndex == 0)
                    return;

                this.ReceptorID = this.ddlReceptorSearch.SelectedItem.Value.ToInt32();

                //// ANTES DE ACEPTAR EL REEPTOR HAY QUE VALIDAR DATOS REQUERIDOS:: 
                //if (!IsReceptorValidToCFDI4_0())
                //{
                //    this.WebMessageBox1.ShowMessage("Receptor no cumple con CFDI4.0, CodigoPostal y Regimen Fiscal Requeridos, Actualice los datos antes de timbrar ", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                //}

                this.InitializeLoadReceptor();
                this.divReceptorSearch.Visible = false;
                this.BillingInformation.TaxesGrid.DataBindTaxes();
                this.txtAddConceptDescription.Focus();

                if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                    {
                    this.ddlPlaceDispatch.ClearSelection();
                    this.ddlPlaceDispatch.Items.Clear();
                    this.ddlPlaceDispatch.SelectedIndex = -1;
                    this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
                    this.ddlPlaceDispatch.DataBind();

                    this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                    this.ddlPlaceDispatch_RequiredFieldValidator.Enabled = true;
                    this.ddlPlaceDispatch_RequiredFieldValidator.EnableClientScript = true;
                    this.ddlPlaceDispatch.Enabled = true;

                    if (this.ddlPlaceDispatch.Items.Count > 1)
                    {
                        this.ddlPlaceDispatch.ClearSelection();
                        this.ddlPlaceDispatch.SelectedIndex = 1;
                    }
                }

                return;
            }

            r = new PACFD.Rules.Receptors();
            this.gdvReceptor.Visible = true;
            this.gdvReceptor.DataSource = r.SelectByNameAndBillerID(this.BillerID, this.txtReceptorSearch.Text);
            this.gdvReceptor.DataBind();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReceptorSelected_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b.IsNull())
                return;
            
            this.ReceptorID = b.CommandArgument.ToInt32();

            this.divReceptorSearch.Visible = false;
            this.InitializeLoadReceptor();

            this.BillingInformation.TaxesGrid.DataBindTaxes();
        }
        
        /// <summary>
        /// Event used to know when an autocomplet is called.
        /// para validar la informacion necesara del estudiante. 
        /// </summary>
        /// 
        string[] iedu = new string[15] {"86121500", "86121501" , "86121502"  , "86121503"  , "86121504"  , "86121600"  , "86121601"  , "86121602" ,
            "86121700", "86121701", "86121702", "86121800", "86121802", "86121803", "86121804"};
        
        protected void hflArticleSearch_ValueChanged(object sender, EventArgs e)
        {
            string s = this.txtAddConceptDescription.Text;
            int id = -1;
            if (int.TryParse(hflArticleSearch.Value, out id))
            {
                using (PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable table =
                    (new PACFD.Rules.Concepts()).SelectByID(id))
                {
                    if (table.Count < 1)
                    {
                        this.btnAddConcept_Click(this.btnAddConceptCancel, EventArgs.Empty);
                        return;
                    }

                    if (table[0].IsClavProdServNull() || string.IsNullOrEmpty(table[0].UnitType))
                    {
                        this.WebMessageBox1.ShowMessage("Concepto no configurado");
                        table.Dispose();
                        return;
                        //throw new Billings.NotReceptorConfiguredException("Receptor needs UsoCFDI.", this.BillerID);
                    }

                    this.EnableConceptSearch(false);
                    this.spanNotConceptFound.Style["display"] = "none";

                    this.txtAddConceptCode.Text = table[0].Code;
                    this.txtAddConceptAmount.Text = "1";
                    this.txtAddConceptPrice.Text = Math.Round(table[0].UnitPrice, 2).ToString();
                    this.ckbAddConceptApplyTaxes.Checked = table[0].AppliesTax;
                    this.txtAddConceptType.Text = table[0].UnitType.Trim().ToLower() == "[sin asignar]" ? string.Empty : table[0].UnitType;

                    this.hflArticleSearch.Value = table[0].ConceptID.ToString();

                    if (iedu.Contains(table[0].ClavProdServ))
                    {
                        txtStudentName.Visible = true;
                        txtCurp.Visible = true;
                        ddAcademyLvl.Visible = true;
                        txtAutRVOE.Visible = true;

                        //Set Required
                        txtStudentName.Attributes["required"] = "true";
                        txtCurp.Attributes["required"] = "true";
                        txtAutRVOE.Attributes["required"] = "true";

                        lblStudentName.Visible = true;
                        lblCurp.Visible = true;
                        lblAcademyLvl.Visible = true;
                        lblAutRVOE.Visible = true;
                    }
                }
            }
            else
            {
                this.btnAddConcept_Click(this.btnAddConceptCancel, EventArgs.Empty);
                return;
            }
        }
        
        /// <summary>
        /// Set the state of the controls for search concepts
        /// </summary>
        /// <param name="enable">State of the controls.</param>
        private void EnableConceptSearch(bool enable)
        {
            this.revAddConceptPrice.EnableClientScript =
                this.txtAddConceptDiscount.Enabled =
                this.txtAddConceptPrice.Enabled =
                this.txtAddConceptAmount.Enabled =
                this.ckbAddConceptApplyTaxes.Enabled =
                this.btnAddConceptCancel.Enabled =
                this.btnAddConcept.Enabled =
                !(
                this.txtAddConceptDescription.Enabled = enable
                );
        }
        
        /// <summary>
        /// Add concept to the list of concept and clear the search of concepts.
        /// </summary>
        protected void btnAddConcept_Click(object sender, EventArgs e)
        {
            BillingsEditorEventArgs arg;

            if (sender.IsNull())
                return;

            if (sender.GetType() != typeof(Button) && sender.GetType() != typeof(LinkButton))
                return;

            bool ValidIEDU = true;
            string ErrorIEDU = "";
            if (txtStudentName.Visible && txtStudentName.Text == "") {
                ErrorIEDU += "<li>Falta nombre del estudiante</li>";
                ValidIEDU = false;
            }
            if (txtCurp.Visible && txtCurp.Text == ""){
                ErrorIEDU += "<li>Falta CURP del estudiante</li>";
                ValidIEDU = false;
            }
            if (txtAutRVOE.Visible && txtAutRVOE.Text == ""){
                ErrorIEDU += "<li>Clave del centro de trabajo</li>";
                ValidIEDU = false;
            }
            if (!ValidIEDU) { 
                this.WebMessageBox1.ShowMessage("Datos para IEDU requeridos:<ul>" + ErrorIEDU + "</ul>", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            if (sender == this.btnAddConcept)
                this.AddArticleToList(new BillingsArticleSearchSelected(
                    this.hflArticleSearch.Value.ToInt32(),
                    this.txtAddConceptType.Text,
                    this.txtAddConceptPrice.Text.ToDecimal(),
                    this.txtAddConceptAmount.Text.ToDecimal(),
                    this.ckbAddConceptApplyTaxes.Checked,
                    this.ConceptsIncludeTaxes));

            this.EnableConceptSearch(true);
            this.spanNotConceptFound.Style["display"] = "block";
            this.txtAddConceptCode.Text = string.Empty;
            this.txtAddConceptAmount.Text = "1";
            this.txtAddConceptPrice.Text = "0.0";
            this.hflArticleSearch.Value = "0";
            this.txtAddConceptDescription.Text = string.Empty;
            this.txtAddConceptType.Text = string.Empty;
            this.txtAddConceptDiscount.Text = string.Empty;
            this.revAddConceptPrice.Validate();
            txtStudentName.Visible = false;
            txtCurp.Visible = false;
            ddAcademyLvl.Visible = false;
            txtAutRVOE.Visible = false;
            lblStudentName.Visible = false;
            lblCurp.Visible = false;
            lblAcademyLvl.Visible = false;
            lblAutRVOE.Visible = false;
            if (sender == this.btnAddConceptCancel)
                return;

            try
            {
                arg = this.OnGenerateBillingsEditorEventArgs(true);
                this.OnConceptAddedToBill(arg);
            }
            catch (Exception)
            {

            }
        }
        
        /// <summary>
        /// Invoke a concept added to list event, to recalculate preview and concept updates.
        /// </summary>
        /// <param name="validateconcepts">
        /// True to vaidate conceps (articles) in the list else false.
        /// If true and not concept has been added to the list then an exception empty concepts is fired.
        /// </param>
        /// <exception cref="">System.Exception "Not articles added to list."</exception>
        public void InvokeConceptAddedToBill(bool validateconcepts)
        {
            BillingsEditorEventArgs arg = this.OnGenerateBillingsEditorEventArgs(validateconcepts);
            this.OnConceptAddedToBill(arg);
        }

        protected void OnConceptAddedToBill(Billings.BillingsEditorEventArgs e)
        {
            if (!this.ConceptAfterAddToBill.IsNull())
                this.ConceptAfterAddToBill(this, e);
        }
        
        /// <summary>
        /// The user has request a new receptor.
        /// </summary>
        protected void btnReceptorAddNew_Click(object sender, EventArgs e)
        {
            this.OnAddNewReceptor(e);
        }
        
        /// <summary>
        /// The user has request a new receptor.
        /// </summary>
        /// <param name="e">Events arguments send by the control.</param>
        protected virtual void OnAddNewReceptor(EventArgs e)
        {
            if (!this.AddNewReceptor.IsNull())
                this.AddNewReceptor(this, e);
        }
        
        /// <summary>
        /// The user has reuqest a new concept to the data base.
        /// </summary>
        protected void btnConceptAddNewArticleToDadaBase_Click(object sender, EventArgs e)
        {
            this.OnAddNewConcept(e);
        }
        
        /// <summary>
        /// The user has reuqest a new concept to the data base.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected virtual void OnAddNewConcept(EventArgs e)
        {
            if (!this.AddNewConcept.IsNull())
                this.AddNewConcept(this, e);
        }
        
        /// <summary>
        /// Select another client and reinitialize the fields.
        /// </summary>
        protected void btnSelectAnotherReceptor_Click(object sender, EventArgs e)
        {
            this.txtAddConceptCode.Text =
                this.txtAddConceptDescription.Text =
                this.txtAddConceptType.Text =
                this.txtAddConceptDiscount.Text =
                this.txtTotalByHand.Text =
                this.txtReceptorConditions.Text =
                this.ltrTotalsDetailView.Text =
                this.lblReceptorAddressText.Text =
                this.lblReceptorCityText.Text =
                this.lblReceptorCPText.Text =
                this.lblReceptorNameText.Text =
                this.lblReceptorFiscalRegimeText.Text =
                this.lblReceptorRFCText.Text = string.Empty;

            this.gdvTaxes.DataSource =
                this.gdvArticles.DataSource =
                this.gdvNotCalculableConceps.DataSource =
                this.gdvReceptor.DataSource = null;

            this.gdvArticles.DataBind();
            this.gdvTaxes.DataBind();
            this.gdvReceptor.DataBind();
            this.gdvNotCalculableConceps.DataBind();

            this.txtAddConceptAmount.Text = "1";
            this.txtAddConceptPrice.Text = "0.0";
            this.hflArticleSearch.Value = "0";
            this.revAddConceptPrice.Validate();

            if (this.ddlReceptorSearch.Items.Count > 0)
                this.ddlReceptorSearch.SelectedIndex = 0;

            this.ddlPaymentMethod.SelectedIndex = 0;
            this.ddlIsCredit.SelectedIndex = 0;
            this.gdvReceptor.Visible = false;

            this.divGlobalInvoiceData.Visible = false;

            this.divReceptorSearch.Visible = true;

            this.divSearchArticle.Visible =
                //this.ttpSelectAnotherReceptor.Visible =
                this.btnClientEdit.Visible =
                this.btnSelectAnotherReceptor.Visible =
                this.ddlPaymentMethod.Enabled =
                this.ddlIsCredit.Enabled =
                this.txtExchangeRate.Enabled =
                this.ddlCurrencyType.Enabled =
                this.txtReceptorConditions.Enabled = false;

            this.DataTableGridView = null;
            this.EnableConceptSearch(true);
        }
        
        /// <summary>
        /// Calculate tax totals when the dinamyc textbox and dropdownlist are changed.
        /// </summary>
        protected void Taxes_CalculateTotals(object sender, EventArgs e)
        {
            this.BillingInformation.CalculateTotal();
        }

        protected void btnClientEdit_Click(object sender, EventArgs e)
        {
            BillingsClientEditEventArgs a = new BillingsClientEditEventArgs(this.ReceptorID, this.EditorMode);
            this.OnClientModify(a);
        }
        
        /// <summary>
        /// Request a client modification.
        /// </summary>
        protected void OnClientModify(BillingsClientEditEventArgs e)
        {
            if (!this.ClientModify.IsNull())
                this.ClientModify(this, e);
        }
        
        /// <summary>
        /// Recalculate totals with the actual currency 
        /// </summary>
        protected void ddlCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.ddlCurrencyType.SelectedItem.Text.Trim().ToLower())
            {
                default:
                case "mxn":
                    this.CurrencyBillerType = CurrencyType.MXN;
                    this.txtExchangeRate_RangeValidator.Enabled = false;
                    break;
                case "usd":
                    this.CurrencyBillerType = CurrencyType.USD;
                    this.txtExchangeRate_RangeValidator.Enabled = true;
                    break;
            }

            this.BillingInformation.CalculateTotal();
        }

        protected void ckbDateSelector_CheckedChanged(object sender, EventArgs e)
        {
            this.txtDateSelector.Visible = !this.txtDateSelector.Visible;
        }

        protected void btnNotCalculableConcept_Click(object sender, EventArgs e)
        {
            PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow row = this.DataTableGridViewNoCalculableConcept.NewBillingsDetailNotesRow();
            row.BillingID = 0;

            row.BillingDetailNoteID = System.Environment.TickCount * -1;

            if (!string.IsNullOrEmpty(this.txtCodeNotCalculableConcept.Text))
                row.Code = this.txtCodeNotCalculableConcept.Text;
            else row.SetCodeNull();

            if (!string.IsNullOrEmpty(this.txtCountNotCalculableConcept.Text))
                row.Count = this.txtCountNotCalculableConcept.Text.ToDecimal();
            else row.SetCountNull();

            if (!string.IsNullOrEmpty(this.txtDescriptionNotCalculableConcept.Text))
                row.Description = this.txtDescriptionNotCalculableConcept.Text;
            else row.SetDescriptionNull();

            if (!string.IsNullOrEmpty(this.txtImportNotCalculableConcept.Text))
                row.Import = this.txtImportNotCalculableConcept.Text.ToDecimal();
            else row.SetImportNull();

            if (!string.IsNullOrEmpty(this.txtPriceNotCalculableConcept.Text))
                row.Price = this.txtPriceNotCalculableConcept.Text.ToDecimal();
            else row.SetPriceNull();

            this.DataTableGridViewNoCalculableConcept.AddBillingsDetailNotesRow(row);
            this.gdvNotCalculableConceps.DataSource = this.DataTableGridViewNoCalculableConcept;
            this.gdvNotCalculableConceps.DataBind();
        }
    }
}