﻿using PACFD.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public static class Helpers
    {
        public static string GetPlaceDispatch(string zipcode, int billerid)
        {
            PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable table = (new PACFD.Rules.PlaceDispatch()).SelectByBillerID(billerid);
            var rows = table.Select("Zipcode = '" + zipcode + "'");
            if (rows.Count() > 0)
                return rows[0]["PlaceDispatch"].ToString() + "-" + zipcode;
            return zipcode;
        }

        public static BillingCatalogs4.CatMesesDataTable GetMonthsSystem()
        {
            BillingCatalogs4 catalogs = new BillingCatalogs4();
            PACFD.DataAccess.BillingCatalogs4TableAdapters.CatMesesTableAdapter fiscal = new PACFD.DataAccess.BillingCatalogs4TableAdapters.CatMesesTableAdapter();
            fiscal.Fill(catalogs.CatMeses);
            return catalogs.CatMeses;
        }

        public static BillingCatalogs4.CatPeriodicidadDataTable GetPeriodsSystem()
        {
            BillingCatalogs4 catalogs = new BillingCatalogs4();
            PACFD.DataAccess.BillingCatalogs4TableAdapters.CatPeriodicidadTableAdapter fiscal = new PACFD.DataAccess.BillingCatalogs4TableAdapters.CatPeriodicidadTableAdapter();
            fiscal.Fill(catalogs.CatPeriodicidad);
            return catalogs.CatPeriodicidad;
        }

        public static BillingCatalogs.CatRegimenFiscalDataTable GetTaxSystem()
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter fiscal = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatRegimenFiscalTableAdapter();
            fiscal.Fill(catalogs.CatRegimenFiscal);
            return catalogs.CatRegimenFiscal;
        }

        public static BillingCatalogs.CatUsoCFDIDataTable GetUsoCFDI()
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatUsoCFDITableAdapter();
            cfdi.Fill(catalogs.CatUsoCFDI);
            return catalogs.CatUsoCFDI;

        }

        public static bool IsValidZipCode(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatCodigoPostalTableAdapter codigo = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatCodigoPostalTableAdapter();
            codigo.Fill(catalogs.CatCodigoPostal);
            var cp = catalogs.CatCodigoPostal.Select("c_CodigoPostal = " + source);
            return cp != null && cp.Count() > 0;
        }

        public static string GetClaveUnidadName(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter();
            cfdi.FillByClave(catalogs.CatClaveUnidad, source);
            var cp = catalogs.CatClaveUnidad;
            string result = "";
            if (cp != null && cp.Count() > 0)
                result = cp[0].Nombre.ToString();
            return result;
        }

        public static string GetClaveProdServName(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter();
            cfdi.FillByClave(catalogs.CatClaveProdServ, source);
            var cp = catalogs.CatClaveProdServ ;
            string result = "";
            if (cp != null && cp.Count() > 0)
                result = cp[0].Descripcion.ToString();
            return result;
        }

        public static string GetClaveUnidadCode(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter();
            cfdi.FillByName(catalogs.CatClaveUnidad, source);
            var cp = catalogs.CatClaveUnidad;
            string result = "";
            if (cp != null && cp.Count() > 0)
                result = cp[0].c_ClaveUnidad.ToString();
            return result;
        }

        public static List<string> FindClaveUnidad(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveUnidadTableAdapter();
            cfdi.Fill(catalogs.CatClaveUnidad, source);
            var cp = catalogs.CatClaveUnidad;
            List<string> result = new List<string>();
            foreach(var row in cp)
            {
                result.Add(row.Nombre.ToString());
            }

            return result;
        }

        public static List<string> FindClaveProdServ(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter();
            cfdi.Fill(catalogs.CatClaveProdServ, source);
            List<string> result = new List<string>();
            foreach (var row in catalogs.CatClaveProdServ)
            {
                result.Add(row.Descripcion.ToString());
            }

            return result;
        }

        public static string GetClaveProdServCode(this string source)
        {
            BillingCatalogs catalogs = new BillingCatalogs();
            PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter cfdi = new PACFD.DataAccess.BillingCatalogsTableAdapters.CatClaveProdServTableAdapter();
            cfdi.FillByName(catalogs.CatClaveProdServ, source);
            var cp = catalogs.CatClaveProdServ;
            string result = "";
            if (cp != null && cp.Count() > 0)
                result = cp[0].c_ClaveProdServ.ToString();
            return result;
        }
    }
}