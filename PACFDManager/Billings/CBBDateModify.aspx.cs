﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billings
{
    public partial class CBBDateModify : PACFDManager.BasePage
    {
        const string MESSAGE_DATETIMEERROR = "MESSAGE_DATETIMEERROR";
        const string MESSAGE_UPDATEERROR = "MESSAGE_UPDATEERROR";
        const string MESSAGE_UPDATESUCCESS = "MESSAGE_UPDATESUCCESS";
        const string MESSAGE_ISNOTCBB = "MESSAGE_ISNOTCBB";

        private int CBBID
        {
            get
            {
                int r;
                string s = this.Request.QueryString["cbb"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                if (!int.TryParse(s, out r))
                    return 0;

                return r;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB || this.CBBID < 1)
            {

                this.WebMessageBox1.CommandArguments = MESSAGE_ISNOTCBB;
                this.WebMessageBox1.ShowMessage("Comprobante no valido o empresa no es CBB.", WebMessageBoxButtonType.Accept);
                return;
            }

            if (this.IsPostBack)
                return;

            using (PACFD.DataAccess.BillingsDataSet.BillingsDataTable table = (new PACFD.Rules.Billings()).SelectByID(this.CBBID))
            {
                if (table.Count < 1 || this.CurrentBillerID != table[0].BillerID)
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_ISNOTCBB;
                    this.WebMessageBox1.ShowMessage("No se puede cambiar la fecha de este compobante.", WebMessageBoxButtonType.Accept);
                    PACFD.Common.LogManager.WriteError(new Exception("Intento de modificar comprobante desde un billerid diferente."));
                    return;
                }
            }

            PACFD.Rules.Billings b = new PACFD.Rules.Billings();
            this.dvBilling.DataSource = b.SelectByID(this.CBBID);
            this.dvBilling.DataBind();
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ISNOTCBB:
                case MESSAGE_UPDATESUCCESS:
                    this.Response.Redirect("BillingsList.aspx");
                    break;
            }
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            DateTime d;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table;
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();

            //DateTime.ParseExact(((TextBox)this.dvBilling.Rows[0].FindControl("txtNewDate")).Text, "dd/MM/yyyy hh:mm tt", null);

            if (!DateTime.TryParse(((TextBox)this.dvBilling.Rows[0].FindControl("txtNewDate")).Text, out d) || this.CBBID < 1)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_DATETIMEERROR;
                this.WebMessageBox1.ShowMessage("La fecha ingresada no es valida.", WebMessageBoxButtonType.Accept);
                return;
            }

            using (table = bill.SelectByID(this.CBBID))
            {
                if (table.Count < 1 || this.CurrentBillerID != table[0].BillerID)
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_DATETIMEERROR;
                    this.WebMessageBox1.ShowMessage("No se puede cambiar la fecha.", WebMessageBoxButtonType.Accept);
                    return;
                }
            }

            if (bill.SetDateTo(this.CBBID, d))
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_UPDATESUCCESS;
                this.WebMessageBox1.ShowMessage("Comprobante actualizado correctamente.", WebMessageBoxButtonType.Accept);
            }
            else
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_UPDATEERROR;
                this.WebMessageBox1.ShowMessage("No se pudo actualizar el comprobante.", WebMessageBoxButtonType.Accept);
            }
        }
    }
}
