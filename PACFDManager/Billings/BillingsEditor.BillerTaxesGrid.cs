﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PACFDManager.Billers;
#endregion

namespace PACFDManager.Billings
{
    partial class BillingsEditor
    {
        /// <summary>
        /// Class used to edit the Taxes gridview control.
        /// </summary>
        public partial class BillerTaxesGrid
        {
            /// <summary>
            /// Parent control of the class.
            /// </summary>
            protected Billings.BillingsEditor Parent { get; private set; }
            /// <summary>
            /// GridView control.
            /// </summary>
            private GridView TaxesGridView
            {
                get
                {
                    if (this.Parent.IsNull())
                    { throw new Exception("Owner can't be null."); }

                    return this.Parent.gdvTaxes;
                }
            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Parent control of the class.</param>
            internal BillerTaxesGrid(Billings.BillingsEditor owner)
            {
                if (owner.IsNull())
                { throw new Exception("Owner can't be null."); }

                this.Parent = owner;
            }
            /// <summary>
            /// Data bind the selected taxes by the parent biller ID.
            /// </summary>
            public void DataBindTaxes()
            {
                PACFD.Rules.TaxTemplate template;
                PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable table;

                if (this.Parent.BillerID < 1)
                    return;

                template = new PACFD.Rules.TaxTemplate();
                table = template.SelectTaxTypeInOrOut(this.Parent.TaxTemplateID, true);
                this.TaxesGridView.DataSource = table;
                this.TaxesGridView.DataBind();
                this.DataBindInitializeRowsTaxes();

                ///---------------------------------------------------------
                ///Update totals taxes
                this.Parent.BillingInformation.UpdateTotalsTable(ref table);

                table.Dispose();
                table = null;
                template = null;
                this.GetConceptsIncludeTaxes();
            }
            private void GetConceptsIncludeTaxes()
            {
                PACFD.Rules.TaxTemplate template;
                PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table;

                if (this.Parent.BillerID < 1)
                    return;

                template = new PACFD.Rules.TaxTemplate();

                using (table = template.SelectByID(this.Parent.TaxTemplateID))
                    this.Parent.ConceptsIncludeTaxes = table[0].IsConceptIncludeTaxNull() ? false : table[0].ConceptIncludeTax;

            }
            /// <summary>
            /// Initialize the tax controls of the gridview.
            /// </summary>
            private void DataBindInitializeRowsTaxes()
            {
                PACFD.Rules.TaxTypes tax = new PACFD.Rules.TaxTypes();
                HiddenField hidden,
                    heditable,
                    hiddenvalue,hFactorType,hAmountBase;
                DropDownList drop;
                TextBox textbox;
                decimal value = 0;
                //RangeValidator range;
                //RequiredFieldValidator required;

                foreach (GridViewRow row in this.TaxesGridView.Rows)
                {
                    if (row.RowType != DataControlRowType.DataRow)
                    { continue; }

                    hidden = row.FindControl("hdfID") as HiddenField;
                    drop = row.FindControl("ddlGridTax") as DropDownList;
                    heditable = row.FindControl("hdfEditable") as HiddenField;
                    textbox = row.FindControl("txtGridTax") as TextBox;
                    hiddenvalue = row.FindControl("hdfTaxValue") as HiddenField;
                    hFactorType = row.FindControl("hdfFactorType") as HiddenField;
                    hAmountBase = row.FindControl("hdfAmountBase") as HiddenField;
                    //range = row.FindControl("rnvGridTaxValue") as RangeValidator;
                    //required = row.FindControl("rfvGridTaxNeed") as RequiredFieldValidator;
                    drop.Visible = !(textbox.Visible = heditable.Value.ToBoolean());

                    if (drop.Visible)
                    {
                        drop.DataSource = tax.SelectTaxValueByTaxTypeID(hidden.Value.ToInt32());
                        drop.DataBind();
                        decimal.TryParse(hiddenvalue.Value, out value);

                        for (int i = 0; i < drop.Items.Count; i++)
                        {
                            if (drop.Items[i].Text != value.ToString("0.00#######"))
                            { continue; }

                            drop.SelectedIndex = i;
                            break;
                        }

                        continue;
                    }

                    textbox.Text = hiddenvalue.Value;
                }

                tax = null;
            }
            /// <summary>
            /// Get a list of TaxGridRow's with values in the TaxGridView.
            /// </summary>
            /// <param name="index">Index of the row.</param>
            /// <returns>If transfer return true else false</returns>
            public List<TaxesGridRow> GetRows
            {
                get
                {
                    TaxesGridRow taxrow;
                    List<TaxesGridRow> list = new List<TaxesGridRow>();
                    HiddenField hid;
                    HiddenField heditable;
                    HiddenField hbilltaxid;
                    HiddenField htaxtranfer;
                    HiddenField htaxivaaffected;
                    HiddenField htaxrequired;
                    HiddenField hFactorType;
                    HiddenField hAmountBase;
                    DropDownList drop;
                    TextBox text;
                    Label name;
                    string s;

                    foreach (GridViewRow row in this.TaxesGridView.Rows)
                    {
                        hid = row.FindControl("hdfID") as HiddenField;
                        heditable = row.FindControl("hdfEditable") as HiddenField;
                        hbilltaxid = row.FindControl("hdfBillTaxID") as HiddenField;
                        htaxtranfer = row.FindControl("hdfTaxTranfer") as HiddenField;
                        htaxivaaffected = row.FindControl("hdfTaxIvaAffected") as HiddenField;
                        htaxrequired = row.FindControl("hdfTaxRequired") as HiddenField;
                        drop = row.FindControl("ddlGridTax") as DropDownList;
                        text = row.FindControl("txtGridTax") as TextBox;
                        name = row.FindControl("lblGridTaxName") as Label;

                        hFactorType = row.FindControl("hdfFactorType") as HiddenField;
                        hAmountBase = row.FindControl("hdfAmountBase") as HiddenField;


                        s = name.Text.Replace(":", "").Trim();

                        if (htaxtranfer.Value != "True")
                            s = s.Remove(0, "Ret. ".Length);

                        if (!drop.Visible)
                            taxrow = new TaxesGridRow(
                                hid.Value.ToInt32(),
                                s,
                                heditable.Value == "1" ? true : false,
                                hbilltaxid.IsNull() ? 0 : hbilltaxid.Value.ToInt32(),
                                htaxtranfer.Value == "True" ? true : false,
                                htaxivaaffected.Value == "1" ? true : false,
                                text.Text.Trim(),
                                text.Text.Trim(),
                                htaxrequired.Value == "1" ? true : false,
                                TaxRowType.Editable, hFactorType.Value, 
                                hAmountBase.Value == "" ? 0 : decimal.Parse(hAmountBase.Value)
                                ); 
                        else
                            taxrow = new TaxesGridRow(
                                hid.Value.ToInt32(),
                                s,
                                heditable.Value == "1" ? true : false,
                                hbilltaxid.IsNull() ? 0 : hbilltaxid.Value.ToInt32(),
                                htaxtranfer.Value == "True" ? true : false,
                                htaxivaaffected.Value == "1" ? true : false,
                                drop.SelectedItem.Text,
                                drop.SelectedItem.Value,
                                htaxrequired.Value == "1" ? true : false,
                                TaxRowType.List, hFactorType.Value,
                                hAmountBase.Value == "" ? 0 : decimal.Parse(hAmountBase.Value)

                                );

                        list.Add(taxrow);

                        hid = null;
                        heditable = null;
                        hbilltaxid = null;
                        htaxtranfer = null;
                        htaxivaaffected = null;
                        htaxrequired = null;
                        drop = null;
                        text = null;
                        name = null;
                        }

                    return list;
                }
            }
        }
    }
}
