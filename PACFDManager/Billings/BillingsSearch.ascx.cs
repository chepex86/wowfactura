﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace PACFDManager.Billings
{
    public partial class BillingsSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Fired when the user do a search.
        /// </summary>
        public event SearchEventHandler Search;
        
        /// <summary>
        /// Get or set a DateTime value with the date selected.
        /// </summary>
        public DateTime Date
        {
            get
            {
                DateTime t;

                t = new DateTime(this.PeriodChooser1.Year, this.PeriodChooser1.Month, 1);

                return t;
            }
            set
            {
                this.PeriodChooser1.Year = value.ToString("yyyy").ToInt32();
                this.PeriodChooser1.Month = value.ToString("MM").ToInt32();
            }
        }
        public bool DateChecked
        {
            get { return this.ckbSearchDate.Checked; }
            set { this.ckbSearchDate.Checked = value; }
        }
        /// <summary>
        /// Get or Set a string value with the receptor name.
        /// </summary>
        public string ReceptorName
        {
            get { return this.txtReceptor.Text; }
            set { this.txtReceptor.Text = value; }
        }
        /// <summary>
        /// Get the main information about folio.
        /// </summary>
        public BillingsSearchFolioInfo Folio { get; private set; }
        
        /// <summary>
        /// Create a new instance of the control.
        /// </summary>
        public BillingsSearch()
        {
            this.Folio = new BillingsSearchFolioInfo(this);
        }
        /// <summary>
        /// Load method of the control.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb;
            PACFD.Rules.Series serie;

            if (this.IsPostBack)
                return;

            if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI)
            {
                this.liUUID.Visible = false;
            }

            serie = new PACFD.Rules.Series();
            this.ddlSerie.DataSource = serie.SelectByIDBiller(Security.Security.CurrentBillerID, Security.Security.CurrentBranchID);
            this.ddlSerie.DataTextField = "Serial";
            this.ddlSerie.DataBind();
            this.ddlSerie.Items.Insert(0, new ListItem("[Todos]", "0"));

            sb = new StringBuilder();
            sb.AppendLine(string.Format("<script type='text/javascript'>\nfunction {0}_ShowHideFolioField(sender)", this.ClientID));
            sb.AppendLine("{");
            sb.AppendLine(string.Format("var mdiv = document.getElementById('{0}');", this.divFolioField.ClientID));
            sb.AppendLine(string.Format("var txt = document.getElementById('{0}');", this.txtFolio.ClientID));
            sb.AppendLine("");
            sb.AppendLine("if(sender == null || mdiv == null) return;");
            sb.AppendLine("");
            sb.AppendLine("if(txt != null) txt.value = '1';");
            sb.AppendLine("");
            sb.AppendLine("mdiv.style.display = sender.selectedIndex == 0 ?  'none' : 'block'");
            sb.AppendLine("");
            sb.AppendLine("}</script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "HidenShowFolioField", sb.ToString());
            this.ddlSerie.Attributes["onchange"] = string.Format("javascript:{0}_ShowHideFolioField(this)", this.ClientID);

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                this.lblPrebilling.Visible =
                    this.ddlPreBillingType.Visible = false;
        }
        /// <summary>
        /// Fire the search event method.
        /// </summary>
        /// <param name="e"></param>
        protected void OnSearch(BillingsSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// User do click in the search button.
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int id;
            Button b = sender as Button;
            BillingsSearchEventArgs a = null;

            if (b.IsNull())
                return;

            if (this.btnClean == b)
            {
                this.txtReceptor.Text =
                    this.txtBillingID.Text =
                    this.txtExternalFolio.Text =
                    this.txtUUID.Text =
                    this.txtFolio.Text = string.Empty;

                this.Date = DateTime.Now;
                this.ddlPreBillingType.SelectedIndex =
                    this.ddlSerie.SelectedIndex = 0;
                return;
            }

            if (this.ddlSerie.SelectedIndex > 0)
                int.TryParse(this.ddlSerie.SelectedItem.Value, out id);

            a = new BillingsSearchEventArgs(
                this.ckbSearchDate.Checked ? (DateTime?)this.Date : null
                , this.ddlSerie.SelectedIndex < 1 ? null : this.ddlSerie.SelectedItem.Text
                , this.ddlSerie.SelectedIndex < 1 ? null : this.ddlSerie.SelectedItem.Value
                , this.ddlSerie.SelectedIndex < 1 ? null : (int?)this.Folio.FolioNumber
                , this.txtReceptor.Text.Trim().Length > 0 ? this.txtReceptor.Text.Trim() : null
                , this.ddlPreBillingType.SelectedIndex == 0 ? null : this.ddlPreBillingType.SelectedIndex == 1 ? (bool?)true : (bool?)false
                , this.ddlIsPaid.SelectedIndex == 0 ? null : this.ddlIsPaid.SelectedIndex == 1 ? (bool?)true : (bool?)false
                , this.liUUID.Visible && this.txtUUID.Text.Trim().Length > 0 ? this.txtUUID.Text : null
                , this.txtExternalFolio.Text.Trim().Length < 1 ? null : this.txtExternalFolio.Text
                , string.IsNullOrEmpty(this.txtBillingID.Text) ? (int?)null : this.txtBillingID.Text.ToInt32()
                );

            if (this.ddlSerie.SelectedIndex > 0)
                this.divFolioField.Style["display"] = "block";

            this.OnSearch(a);
        }

        public void InvokeSearch()
        {
            this.btnSearch_Click(this.btnSearchByReceptor, EventArgs.Empty);
        }
    }
}