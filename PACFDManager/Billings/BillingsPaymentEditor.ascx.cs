﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Text;
using System.Data;
using static PACFDManager.Billings.BillingsEditor;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Control used to edit the billings.
    /// </summary>
    public partial class BillingsPaymentEditor : PACFDManager.BaseUserControl
    {
        private int NUMBER_OF_CLIENT_BEFORE_SEARCH = 100; //modify this to vary count of receptor display on search

        public int BillingID
        {
            get
            {
                int id;
                string s;

                s = this.Request.QueryString["pid"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                if (!int.TryParse(s, out id))
                    return 0;

                return id;
            }
        }
        public PACFD.DataAccess.BillingsDataSet PreDataSet { get; set; }
        public bool IsPaid
        {
            get
            {
                if (this.BillingID > 0)
                {
                    var total = this.GetBillingPaidAmount();
                    return (Math.Round(total, 2).ToString() == lblTotalAmountText.Text.Replace("$", "").Replace(",", ""));
                }
                return false;
            }
        }
        public BillingsPaymentEditor()
        {
            this.BillingInformation = this.OnCreateBillingsEditorInfo();
        }
        protected virtual BillingsPaymentEditorInfo OnCreateBillingsEditorInfo() { return new BillingsPaymentEditorInfo(this); }
        /// <summary>
        /// Fires the ApplyChanges event.
        /// </summary>
        public event GeneratedBillingEventHandler GeneratedBilling;
        /// <summary>
        /// 
        /// </summary>
        public event BillingBeforeAddEventHandler BillingBeforeAdd;
        /// <summary>
        /// 
        /// </summary>
        public event BillingAfterAddEventHandler BillingAfterAdd;
        /// <summary>
        /// Fired when the receptor has been loaded.
        /// </summary>
        public event EventHandler ReceptorDataBind;
        /// <summary>
        /// Fired when a new receptor is needed.
        /// </summary>
        public event EventHandler AddNewReceptor;
        /// <summary>
        /// Fired when a new concept is needed.
        /// </summary>
        public event EventHandler AddNewBilling;
        /// <summary>
        /// Event fired when a concept is added to the list of the bill.
        /// </summary>
        public event Billings.BillingAfterAddEventHandler BillingAfterAddToBill;
        /// <summary>
        /// Event fired when a client request to be modify.
        /// </summary>
        public event ClientModifyEventHandler ClientModify;
        /// <summary>
        /// Event fired when a billing is binding by ID.
        /// </summary>
        public event DataBindFromBillingIDEventHandler DataBindBillingID;
        /// <summary>
        /// Get or Set a System.Data.DataTable temporal table with the current
        /// selected articles for the billing.
        /// </summary> 
        protected System.Data.DataTable DataTableGridView
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull() || o.GetType() != typeof(System.Data.DataTable))
                {
                    this.OnCreateGridDataTable();
                    o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                }

                return (System.Data.DataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get the main information of the control.
        /// </summary>        
        [System.ComponentModel.Browsable(false)]
        public BillingsPaymentEditorInfo BillingInformation { get; private set; }
        /// <summary>
        /// Get or Set a BillingsEditorMode value with the editor mode.
        /// </summary>
        public BillingsEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return BillingsEditorMode.Add;

                return o.GetType() == typeof(BillingsEditorMode) ? (BillingsEditorMode)o : BillingsEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;

                //if (value == BillingsEditorMode.View)
                    //this.Enabled = false;
            }
        }
        /// <summary>
        /// Get or Set a boolean value with enable state of the control.
        /// </summary>

        /// <summary>
        /// Get or Set an integer value with the receptor ID to edit.
        /// </summary>        
        public int ReceptorID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        /// <summary>
        /// Get or Set an integer value with the billing ID to edit.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public int BillingPaymentID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or Set an integer value with the biller ID to edit.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public int BillerID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get a string value with the seal of a billing loaded.
        /// </summary>
        protected string BillingSeal
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get a string with the original string of a billings loaded.
        /// </summary>
        protected string BillingOriginalString
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;


                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        [System.ComponentModel.Browsable(false)]
        public int TaxTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set a string value with the BillingTypeName
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string TaxTemplateName
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set a string value with the BillingType
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string TaxTemplateType
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;


                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set a bool value if the billing is a prebilling or a sealed billing
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public PrebillingType IsPreBilling
        {
            get
            {
                object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];
                return o.IsNull() || o.GetType() != typeof(PrebillingType) ? PrebillingType.None : (PrebillingType)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set the currency type used.
        /// </summary>
        public PACFD.Common.CurrencyType CurrencyBillerType
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return CurrencyType.None;

                return o.GetType() == typeof(CurrencyType) ? (CurrencyType)o : CurrencyType.None;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        /// <summary>
        /// Load method of the control.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            //((PACFDManager.Default)this.Page.Master).RegistryAutoCompletExtenderOnSelectedItem(this.hflArticleSearch,
            //    this.txtAddConceptDescription_AutoCompleteExtender);
            
            this.SetJavaScript();

            if (this.txtDateSelector.Visible)
            {
                DateTime sdate;
                try
                {
                    if (!DateTime.TryParseExact(this.txtDateSelector.Text, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out sdate))
                        sdate = DateTime.Now;
                }
                catch
                {
                    sdate = DateTime.Now;
                }

                this.txtDateSelector_CalendarExtender.SelectedDate = sdate;
            }
            ddlPlaceDispatch.DataTextField = "PlaceDispatch";
            ddlPlaceDispatch.DataValueField = "ZipCode";

            if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID < 1)
            {
                //this.Enabled = false;
                this.divReceptorSearch.Visible = true;
            }

            if (this.ddlCurrencyType.Visible)
            {
                this.txtExchangeRate_RangeValidator.Validate();

                if (this.ddlCurrencyType.SelectedIndex == 1)
                {
                    this.spanExchangeRate.Style["display"] = "block";
                    this.txtExchangeRate_RangeValidator.Enabled = true;
                }
                else
                {
                    this.spanExchangeRate.Style["display"] = "none";
                    this.txtExchangeRate_RangeValidator.Style["display"] = "none";
                    this.txtExchangeRate_RangeValidator.Enabled = false;
                }
            }

            if (this.IsPostBack) //exit method            
                return;

            if (this.BillingID > 0)
            {
                this.ReceptorID = GetReceptor();
                this.divReceptorSearch.Visible = false;
                this.InitializeLoadReceptor();
                this.LoadTotals();
                this.AddBillingToList(this.BillingID);
            } 
            if (this.EditorMode != BillingsEditorMode.View)
                this.txtExchangeRate.Text = string.Format("{0}", this.CurrentExchangeRate);

            this.InitializeJavaScript_DropDownListReceptorSearch();

            if (this.EditorMode != BillingsEditorMode.View)
            {
                this.DataBindBillerID();
                this.lblBillingExternalFolioText.Visible = false;
                this.ddlPlaceDispatch.Enabled = false;
            }
            else
            {
                this.txtBillingExternalFolioText.Visible = !(
                    this.lblBillingExternalFolioText.Visible =
                    this.txtBillingObservation.ReadOnly = true);
            }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
            {
                this.ddlPaymentMethod_RequiredFieldValidator.Visible = true;
                this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                this.ddlPlaceDispatch.Enabled = true;
            }
            else
            {
                this.ddlPaymentMethod_RequiredFieldValidator.Visible = false;
                //this.tablePlaceDispatch.Visible = false;
            }
        }

        private void LoadTotals(PACFD.DataAccess.BillingsDataSet dataset)
        {
            decimal total, paid;

            total = this.GetBillingTotal(dataset);
            this.lblTotalAmountText.Text = total.ToString("C");

            paid = this.GetBillingPaidAmount(total, dataset);
            this.lblSubAmountText.Text = paid.ToString("C");

            this.lblTotalMinusText.Text = (total - paid).ToString("C");
        }
        protected void hflInstSearch_ValueChanged(object sender, EventArgs e)
        {
            //string s = this.txtAddConceptDescription.Text;
            //int id = -1;
            //if (int.TryParse(hflArticleSearch.Value, out id))
            //{
            //    using (PACFD.DataAccess.ConceptsDataSet.ConceptsDataTable table =
            //        (new PACFD.Rules.Concepts()).SelectByID(id))
            //    {
            //        if (table.Count < 1)
            //        {
            //            this.btnAddConcept_Click(this.btnAddConceptCancel, EventArgs.Empty);
            //            return;
            //        }

            //        if (table[0].IsClavProdServNull() || string.IsNullOrEmpty(table[0].UnitType))
            //        {
            //            this.WebMessageBox1.ShowMessage("Concepto no configurado");
            //            table.Dispose();
            //            return;
            //            //throw new Billings.NotReceptorConfiguredException("Receptor needs UsoCFDI.", this.BillerID);
            //        }

            //        this.EnableConceptSearch(false);
            //        this.spanNotConceptFound.Style["display"] = "none";

            //        this.txtAddConceptCode.Text = table[0].Code;
            //        this.txtAddConceptAmount.Text = "1";
            //        this.txtAddConceptPrice.Text = Math.Round(table[0].UnitPrice, 2).ToString();
            //        this.ckbAddConceptApplyTaxes.Checked = table[0].AppliesTax;
            //        this.txtAddConceptType.Text = table[0].UnitType.Trim().ToLower() == "[sin asignar]" ? string.Empty : table[0].UnitType;

            //        this.hflArticleSearch.Value = table[0].ConceptID.ToString();

            //        if (iedu.Contains(table[0].ClavProdServ))
            //        {
            //            txtStudentName.Visible = true;
            //            txtCurp.Visible = true;
            //            ddAcademyLvl.Visible = true;
            //            txtAutRVOE.Visible = true;
            //            lblStudentName.Visible = true;
            //            lblCurp.Visible = true;
            //            lblAcademyLvl.Visible = true;
            //            lblAutRVOE.Visible = true;
            //        }
            //    }
            //}
            //else
            //{
            //    this.btnAddConcept_Click(this.btnAddConceptCancel, EventArgs.Empty);
            //    return;
            //}
        }
        private decimal GetBillingPaidAmount(decimal totalbill, PACFD.DataAccess.BillingsDataSet dataset)
        {
            decimal totalglobal = 0
                //, paidtotal = 0
                ;
            //System.Data.DataRow[] rows;

            totalglobal = 0;

            foreach (PACFD.DataAccess.BillingsDataSet.BillingsRow row in dataset.Billings)
            {
                totalglobal += totalbill - row.ImpSaldoInsoluto;
            }

            return totalglobal;
        }

        private decimal GetBillingTotal(PACFD.DataAccess.BillingsDataSet dataset)
        {
            decimal totalglobal = 0;
            foreach (PACFD.DataAccess.BillingsDataSet.BillingsRow row in dataset.Billings)
            {
                if (row.CurrencyCode != "MXN")
                    totalglobal += row.Total * row.ExchangeRateMXN;
                else
                    totalglobal += row.Total;
            }
            return totalglobal;
        }

        private void LoadTotals()
        {
            decimal total, paid;

            total = this.GetBillingTotal();
            this.lblTotalAmountText.Text = total.ToString("C");

            paid = this.GetBillingPaidAmount();
            this.lblSubAmountText.Text = paid.ToString("C");

            this.lblTotalMinusText.Text = (total - paid).ToString("C");
        }

        private decimal GetBillingPaidAmount()
        {
            decimal totalglobal = 0
                //, paidtotal = 0
                ;
            //System.Data.DataRow[] rows;
            PACFD.Rules.BillingsPayments_3 billp = new PACFD.Rules.BillingsPayments_3();
            int? branchID = null;

            if (CurrentBranchID != -1)
                branchID = CurrentBranchID;

            int? billingid = this.BillingID;
            if (this.BillingID < 1)
                billingid = null;

            var payments = billp.Search(
                PACFD.Rules.BillingsPayments_3.SearchPetitionFromFlag.WebForm
                , this.CurrentBillerID
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , branchID
                , null
                , null
                , billingid
                );

            totalglobal = 0;

            foreach (PACFD.DataAccess.BillingsDataSet.BillingsPayment_3SearchRow row in payments)
            {
                if (row.CurrencyCode != "MXN")
                    totalglobal += row.Total * row.ExchangeRateMXN;
                else
                    totalglobal += row.Total;

                //if (row.PreBilling || row.BillingType.Trim().ToLower() == "egreso" || !row.Active)
                //    continue;

                //if (!row.IsIsCreditNull() && row.IsCredit)
                //    if (row.IsIsPaidNull() || !row.IsPaid)
                //        continue;

                //if (row.CurrencyCode != "MXN")
                //    paidtotal += row.Total * row.ExchangeRateMXN;
                //else
                //    paidtotal += row.Total;
            }

            return totalglobal;
        }

        private decimal GetBillingTotal()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable = billing.SelectByID(this.BillingID))
            {
                if (billtable.Count < 0)
                    return 0;

                return billtable[0].Total;
            }
        }

        public void CreateDataset(List<int> ids)
        {
            this.PreDataSet = new PACFD.DataAccess.BillingsDataSet();
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            foreach(int id in ids)
            {
                var billing = bill.SelectByID(id);
                if (billing.Count < 1)
                    continue;
                this.PreDataSet.Billings.Rows.Add(billing[0].ItemArray);
            }
        }
        private int GetReceptor()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable = billing.SelectByID(this.BillingID))
            {
                if (billtable.Count < 0)
                    return 0;

                return billtable[0].ReceptorID;
            }
        }
        /// <summary>
        /// Add the needed javascript used be the control to the page.
        /// </summary>
        private void SetJavaScript()
        {
            List<string> slist;
            string s;

            if (this.EditorMode == BillingsEditorMode.View)
            {
                //this.lblBankAccount.Visible = true;
                //this.txtBankAccount.Enabled = false;
                this.txtReference.Enabled = false;
                this.txtReference.Visible = true;
                this.txtBankName.Visible = true;
                this.txtBankName.Enabled = false;
                this.txtDateSelector.Enabled = false;
                this.txtDateSelector.Visible = false;
                this.txtTotal.Enabled = false;
                this.txtTotal.Visible = true;
                this.ddlPaymentMethod.Enabled = false;
                this.ddlPlaceDispatch.Enabled = false;
            }
            else
            {
                //this.txtBankAccount.Style["display"] =
                  //  this.lblBankAccount.Style["display"] = this.ddlPaymentMethod.SelectedIndex > 2 ? "block" : "none";
                this.txtReference.Style["display"] = "block";
                this.txtReference.Style["display"] = "block";
                this.txtBankName.Style["display"] = "block";
                this.txtBankName.Style["display"] = "block";
                this.txtDateSelector.Style["display"] = "block";
                this.txtDateSelector.Style["display"] = "block";
            }

            if (this.IsPostBack)
                return;

            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "BillingEditor.js", "../Billings/BillingsEditor.js");

            slist = new List<string>();
            //slist.Add(String.Format("ClientID                                       ='{0}'", this.ClientID));
            //slist.Add(String.Format("txtAddConceptDescription                       :'{0}'", this.txtAddConceptDescription.ClientID));
            //slist.Add(String.Format("imgConceptNotFound                             :'{0}'", this.imgConceptNotFound.ClientID));
            //slist.Add(String.Format("hflArticleSearch                               :'{0}'", this.hflArticleSearch.ClientID));
            //slist.Add(String.Format("txtAddConceptCode                              :'{0}'", this.txtAddConceptCode.ClientID));
            slist.Add(String.Format("lblCurrencyType                                :'{0}'", this.lblCurrencyType.ClientID));
            slist.Add(String.Format("ddlCurrencyType                                :'{0}'", this.ddlCurrencyType.ClientID));
            slist.Add(String.Format("spanExchangeRate                               :'{0}'", this.spanExchangeRate.ClientID));
            slist.Add(String.Format("txtExchangeRate_RangeValidator                 :'{0}'", this.txtExchangeRate_RangeValidator.ClientID));
            //slist.Add(String.Format("txtAddConceptDescription_AutoCompleteExtender  :'{0}'", this.txtInstRecepSearch.ClientID));
            //slist.Add(String.Format("spanNotConceptFound                            :'{0}'", this.txtInstRecepSearch.ClientID));
            slist.Add(String.Format("txtDateSelector                                :'{0}'", this.txtDateSelector.ClientID));
            //slist.Add(String.Format("txtAccountNumberPayment                        :'{0}'", this.txtBankAccount.ClientID));
            slist.Add(String.Format("ddlPaymentMethod                               :'{0}'", this.ddlPaymentMethod.ClientID));
            //slist.Add(String.Format("lblAccountNumberPayment                        :'{0}'", this.lblBankAccount.ClientID));
            s = string.Format("{{ {0} }}", string.Join(",", slist.ToArray()));

            s = string.Format(" var {0}_jcBillingEditor = new jclassBillingsEditor({1});",
                    this.ClientID, s);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "BillingEditorClassCreator.js", s, true);

            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "hflArticleSearch.js"
            //, string.Format("var hflArticleSearchID = '{0}'", hflArticleSearch.ClientID), true);

            //this.txtAddConceptDescription_AutoCompleteExtender.OnClientItemSelected =
            //    string.Format("function() {{ {0}_jcBillingEditor.AutoCompletSelectedItem('{1}'); }}"
            //    , this.ClientID
            //    , this.hflArticleSearch.ClientID);

            //this.txtInstRecepSearch.OnClientShown = string.Format("function() {{ {0}_jcBillingEditor.OnClientShow(); }}",
            //    this.ClientID);
            //this.txtInstRecepSearch.OnClientHidden = string.Format("function() {{ {0}_jcBillingEditor.OnClientHidde(); }}",
            //    this.ClientID);

            this.ddlCurrencyType.Attributes["onchange"] = string.Format("javascript:{0}_jcBillingEditor.OnCurrencyTypeChanged()", this.ClientID);
            this.ddlCurrencyType.Attributes["change"] = string.Format("javascript:{0}_jcBillingEditor.OnCurrencyTypeChanged()", this.ClientID);

            this.ddlPaymentMethod.Attributes["onchange"] = string.Format("javascript:{0}_jcBillingEditor.OnddlPaymentMethodChanged()", this.ClientID);
            this.ddlPaymentMethod.Attributes["change"] = string.Format("javascript:{0}_jcBillingEditor.OnddlPaymentMethodChanged()", this.ClientID);

            if (this.EditorMode != BillingsEditorMode.View)
            {
                //this.txtBankAccount.Style["display"] = "none";
                ////this.lblBankAccount.Style["display"] = "none";
                //this.txtBankName.Style["display"] = "none";
                //this.lblBankName.Style["display"] = "none";
            }
        }
        /// <summary>
        /// Initialize and set the javascript code used 
        /// to do click on the search button.
        /// </summary>
        protected virtual void InitializeJavaScript_DropDownListReceptorSearch()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(string.Format("<script type='text/javascript'>function {0}_SelectReceptor()", this.ClientID));
            sb.AppendLine("{");
            sb.AppendLine(string.Format("var btn = document.getElementById('{0}');", this.btnAceptReceptorSearch.ClientID));
            sb.AppendLine("if(btn == null)return;");
            sb.AppendLine("btn.click();");
            sb.AppendLine("}");
            sb.AppendLine("</script>");

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "SelecReceptorOnDropDownChanged", sb.ToString());
            this.ddlReceptorSearch.Attributes["onchange"] = string.Format("javascript:{0}_SelectReceptor();", this.ClientID);
        }
        /// <summary>
        /// Generate a Billing.
        /// </summary>
        /// <exception cref="PACFDManager.Billings.NotDigitalCertificatesAvailableException">
        /// Throw when not a digital certificate are available in for the current biller.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotBillerFoundException">
        /// Throw when not biller found.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotReceptorFoundException">
        /// Throw when not receptor found.
        /// </exception>
        public void CreateBilling()
        {
            BillingsEditorEventArgs e;
            PACFD.Rules.BillingsPayments_3 billingpayment;
            PACFD.DataAccess.BillingsDataSet data;

            e = this.OnGenerateBillingsEditorEventArgs();
            data = e.DataSet;
            billingpayment = new PACFD.Rules.BillingsPayments_3();

            this.OnGeneratedBilling(e);
        }
        /// <summary>
        /// Bind the control by a Billing ID.
        /// </summary>
        public void DataBindFromBillingID()
        {
            ListItem l;
            PACFD.DataAccess.BillingsDataSet da;
            System.Data.DataRow row;
            int i;
            PACFD.Rules.ElectronicBillingType el;
            BillingsEditorEventArgs args;

            if (this.BillingPaymentID < 1)
                return;

            using (da = (new PACFD.Rules.BillingsPayments_3()).GetFullBilling(this.BillingPaymentID))
            {
                if (da.IsNull())
                    return;

                if (da.BillingsPayments_3.Count < 1)
                    return;

                this.BillerID = da.BillingsPayments_3[0].BillerID;
                this.DataBindFromBillingID_Biller(da.BillingsPayments_3[0].BillerID);
                this.DataBindBillerID();
                this.ReceptorID = da.BillingsPayments_3[0].ReceptorID;
                this.DataBindFromBillingID_Receptor(da.BillingsPayments_3[0].ReceptorID);
                this.InitializeLoadReceptor();

                var billings = new PACFD.Rules.Billings().SelectByBillingPaymentId(this.BillingPaymentID);
                foreach (var d in billings)
                {
                    row = this.BillingInformation.BillingsGrid.NewRow();
                    row[Billings.BillingsPaymentEditorColumns.UUID] = d.UUID;
                    row[Billings.BillingsPaymentEditorColumns.Total] = d.Total;
                    row[Billings.BillingsPaymentEditorColumns.BillingDate] = d.BillingDate;
                    row[Billings.BillingsPaymentEditorColumns.BillingID] = d.BillingID;
                    this.BillingInformation.BillingsGrid.AddRow(row);
                    row = null;
                }
                this.BillingInformation.BillingsGrid.DataBind();
                this.lblCompanyYear.Text = da.BillingsPayments_3[0].IsApprovalYearNull() ? "(No aplica)" : da.BillingsPayments_3[0].ApprovalYear.ToString();
                this.lblCompanyAprovalNumberText.Text = da.BillingsPayments_3[0].IsApprovalNumberNull() ? "(No aplica)" : da.BillingsPayments_3[0].ApprovalNumber.ToString();
                this.BillingOriginalString = da.BillingsPayments_3[0].OriginalString;
                this.BillingSeal = da.BillingsPayments_3[0].Seal;

                this.IsPreBilling = da.BillingsPayments_3[0].PreBilling && da.BillingsPayments_3[0].IsFolioIDNull() ? PrebillingType.PrebillingNotSerialFolio :
                    da.BillingsPayments_3[0].PreBilling && !da.BillingsPayments_3[0].IsFolioIDNull() ? PrebillingType.Prebilling : PrebillingType.None;

                this.lblCompanySerieFolio.Text = string.Format("{0} - {1}", da.BillingsPayments_3[0].Serial, da.BillingsPayments_3[0].Folio);
                this.txtBillingObservation.Text = da.BillingsPayments_3[0].IsObservationsNull() ? string.Empty : da.BillingsPayments_3[0].Observations;
                this.lblBillingExternalFolioText.Text = da.BillingsPayments_3[0].IsExternalFolioNull() ? string.Empty : da.BillingsPayments_3[0].ExternalFolio;

                el = (PACFD.Rules.ElectronicBillingType)da.BillingsPayments_3[0].ElectronicBillingType;

                this.lblCertificateNumber.Visible = this.lblCertificateNumberTitle.Visible = true;
                this.lblCertificateNumber.Text = da.BillingsPayments_3[0].CertificateNumber;
                this.lblUUID.Visible = this.lblUUIDTitle.Visible = true;
                this.lblUUID.Text = da.BillingsPayments_3[0].IsUUIDNull() ? string.Empty : da.BillingsPayments_3[0].UUID;
                this.lblStampedDate.Visible = this.lblStampedDateTitle.Visible = true;
                this.lblStampedDate.Text = string.Format("{0:dd/MM/yyyy hh:mm}", da.BillingsPayments_3[0].IsDateStampedNull() ? (DateTime?)null : da.BillingsPayments_3[0].DateStamped);
                this.lblCertificateNumberSAT.Visible = this.lblCertificateNumberSATTitle.Visible = true;
                this.lblCertificateNumberSAT.Text = da.BillingsPayments_3[0].IsCertificateNumberSatNull() ? string.Empty : da.BillingsPayments_3[0].CertificateNumberSat;

                switch (da.BillingsPayments_3[0].CurrencyCode.Trim().ToLower())
                {
                    case "mxn": this.CurrencyBillerType = CurrencyType.MXN; break;
                    case "usd": this.CurrencyBillerType = CurrencyType.USD; break;
                }


                this.ddlCurrencyType.ClearSelection();
                l = this.ddlCurrencyType.Items.FindByText(da.BillingsPayments_3[0].CurrencyCode);

                if (!l.IsNull())
                    l.Selected = true;

                this.txtExchangeRate.Text = da.BillingsPayments_3[0].ExchangeRateMXN.ToString();
                this.txtExchangeRate_RangeValidator.Visible = false;

                this.lblPlaceDispatch.Visible =
               this.ddlPlaceDispatch.Visible =
               this.gdvFiscalRegime.Visible = false;

                this.gdvFiscalRegime.DataSource = (new PACFD.Rules.Billings()).FiscalRegimeTable.GetByBillerID(this.CurrentBillerID);
                this.gdvFiscalRegime.DataBind();
                this.lblPlaceDispatch.Visible =
                    this.ddlPlaceDispatch.Visible =
                    this.gdvFiscalRegime.Visible = true;

                this.ddlPlaceDispatch.ClearSelection();
                this.ddlPlaceDispatch.Items.Clear();
                this.ddlPlaceDispatch.SelectedIndex = -1;
                this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
                this.ddlPlaceDispatch.DataBind();
                this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                this.ddlPlaceDispatch.SelectedIndex = 0;

                if (this.ddlPlaceDispatch.Items.Count > 1)
                {
                    var value = this.ddlPlaceDispatch.Items.FindByValue(da.BillingsPayments_3[0].PlaceDispatch);
                    if (value != null)
                        this.ddlPlaceDispatch.Items.FindByValue(da.BillingsPayments_3[0].PlaceDispatch).Selected = true;
                    else
                        this.ddlPlaceDispatch.SelectedIndex = 1;
                }

                var asd = this.ddlPaymentMethod.Items.FindByValue(da.BillingsPayments_3[0].PaymentForm);
                if (asd != null)
                    this.ddlPaymentMethod.Items.FindByValue(da.BillingsPayments_3[0].PaymentForm).Selected = true;
                else
                    this.ddlPaymentMethod.SelectedIndex = 1;
            }

            if (this.EditorMode == BillingsEditorMode.View)
            {
                
                this.ddlPlaceDispatch.Enabled = false;
            }

            if (!da.BillingsPayments_3[0].IsRfcEmisorCtaBenNull())
            {
                /*this.lblBankAccount.Visible = true;
                this.txtBankAccount.Enabled = false;
                this.txtBankAccount.Text = da.BillingsPayments_3[0].BankAccount;*/

                this.lblBankName.Visible = true;
                this.txtBankName.Enabled = false;
                this.txtBankName.Text = da.BillingsPayments_3[0].IsBankNameNull() ? "":da.BillingsPayments_3[0].BankName;
                this.txtRfcEmisorBen.Text = da.BillingsPayments_3[0].IsRfcEmisorCtaBenNull() ? "" : da.BillingsPayments_3[0].RfcEmisorCtaBen;
                this.txtRfcEmisorOrd.Text = da.BillingsPayments_3[0].IsRfcEmisorCtaOrdNull() ? "" : da.BillingsPayments_3[0].RfcEmisorCtaOrd;
                this.txtNoCuentaBen.Text = da.BillingsPayments_3[0].IsCtaBeneficiarioNull() ? "" : da.BillingsPayments_3[0].CtaBeneficiario.ToString();
                this.txtNoCuentaOrd.Text = da.BillingsPayments_3[0].IsCtaOrdenanteNull() ? "" : da.BillingsPayments_3[0].CtaOrdenante.ToString();
            }

            if (!da.BillingsPayments_3[0].IsReferenceNull())
            {
                this.lblReference.Visible = true;
                this.txtReference.Enabled = false;
                this.txtReference.Text = da.BillingsPayments_3[0].Reference;
            }

            LoadTotals(da);
            this.txtTotal.Visible = true;
            this.txtTotal.Enabled = false;
            this.txtTotal.Text = da.BillingsPayments_3[0].Total.ToString();
            args = new BillingsEditorEventArgs(da, this.EditorMode);
            this.OnDataBindBillingID(args);
        }
        protected virtual void OnDataBindBillingID(BillingsEditorEventArgs e)
        {
            if (!this.DataBindBillingID.IsNull())
                this.DataBindBillingID(this, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        protected void DataBindFromBillingID_Biller(int id)
        {
            var table = new PACFD.Rules.Billers().SelectByID(id);
            if (table.Count < 1)
                return;

            this.lblCompanyMainTitle.Text = table[0].Name;
            this.lblCompanySubTitle.Text = table[0].Reference;
            this.lblCompanyRFCText.Text = table[0].RFC;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        protected void DataBindFromBillingID_Receptor(int id)
        {
            //PACFD.Rules.CountrySelectors select;
            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable countrytable;
            var table = new PACFD.Rules.Receptors().SelectByID(id);
            if (table.Count < 1)
                return;

            //select = new PACFD.Rules.CountrySelectors();
            //countrytable = select.SelectCityStateCountryByCityID(table[0].Location.ToInt32());

            //if (countrytable.Count > 0)
            //    this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}",
            //    countrytable[0].City, countrytable[0].State, countrytable[0].Country);
            //else
            //    this.lblReceptorCityText.Text = "(No disponible)";

            //countrytable.Dispose();

            this.lblReceptorNameText.Text = table[0].Name;
            this.lblReceptorRFCText.Text = table[0].RFC;
            this.lblReceptorAddressText.Text = table[0].Address;
            this.lblReceptorCPText.Text = table[0].Zipcode.ToString() == "-1" ? String.Empty : table[0].Zipcode.ToString();
            this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}", table[0].Location, table[0].State, table[0].Country);

            this.btnClientEdit.Visible =
                this.btnSelectAnotherReceptor.Visible = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnReceptorDataBind(EventArgs e)
        {
            if (!this.ReceptorDataBind.IsNull())
                this.ReceptorDataBind(this, e);
        }
        /// <summary>
        /// Initialize biller data info and the receptor search box.
        /// </summary>
        ///<returns>If success return true else false.</returns>
        public bool DataBindBillerID()
        {
            PACFD.Rules.Billers biller;
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;
            PACFD.Rules.Receptors receptor;
            ListItem l;

            biller = new PACFD.Rules.Billers();

            using (table = biller.SelectByID(this.BillerID))
            {
                if (table.Count < 1)
                    return false;

                this.lblCompanyMainTitle.Text = table[0].Name;
                this.lblCompanySubTitle.Text = table[0].Reference;
                this.lblCompanyRFCText.Text = table[0].RFC;

                this.lblCurrencyType.Visible =
                    this.ddlCurrencyType.Visible =
                    this.txtExchangeRate.Visible =
                    this.txtExchangeRate_RangeValidator.Visible =
                    this.spanExchangeRate.Visible = true;

                if (this.ddlCurrencyType.Visible)
                {
                    this.ddlCurrencyType.ClearSelection();
                    l = this.ddlCurrencyType.Items.FindByText(table[0].CurrencyCode);

                    if (!l.IsNull())
                        l.Selected = true;
                    else
                        this.ddlCurrencyType.Items[0].Selected = true;
                }

                switch (table[0].CurrencyCode.Trim().ToLower())
                {
                    case "mxn":
                        this.CurrencyBillerType = CurrencyType.MXN;
                        this.spanExchangeRate.Style["display"] = "none";
                        break;
                    case "usd":
                        this.ddlCurrencyType.ClearSelection();
                        this.ddlCurrencyType.SelectedIndex = 1;
                        this.CurrencyBillerType = CurrencyType.USD;
                        break;
                    default: this.CurrencyBillerType = CurrencyType.None; break;
                }

                if (this.EditorMode == BillingsEditorMode.Add)
                {
                    this.lblCompanySerieFolio.Text =
                        this.lblCompanyYear.Text =
                        this.lblCompanyAprovalNumberText.Text = "(pendiente)";
                }
            }

            receptor = new PACFD.Rules.Receptors();
            this.ddlReceptorSearch.DataSource = receptor.SelectByBillersID(this.BillerID);
            this.ddlReceptorSearch.DataTextField = "Name";
            this.ddlReceptorSearch.DataValueField = "ReceptorID";
            this.ddlReceptorSearch.DataBind();
            this.ddlReceptorSearch.Items.Insert(0, new ListItem("[Seleccionar]", "-1"));
            receptor = null;

            if (this.ddlReceptorSearch.Items.Count > NUMBER_OF_CLIENT_BEFORE_SEARCH) //modify this to vary count of receptor display on search
            {
                this.ddlReceptorSearch.Items.Clear();
                this.ddlReceptorSearch.Visible = false;
                this.btnAceptReceptorSearch.Attributes["style"] = "display:block";
                //this.ttpAceptReceptorSearch.Visible =
                this.txtReceptorSearch.Visible = true;
            }
            else
            {
                this.btnAceptReceptorSearch.Attributes["style"] = "display:none";
                //this.ttpAceptReceptorSearch.Visible =
                this.txtReceptorSearch.Visible = false;
            }

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
            {
                this.gdvFiscalRegime.Visible = true;
                this.gdvFiscalRegime.DataSource = (new PACFD.Rules.Billers()).FiscalRegimeTable.GetByBillerID(this.CurrentBillerID);
                this.gdvFiscalRegime.DataBind();
            }

            return true;
        }
        /// <summary>
        /// Data bind a receptor from receptor ID
        /// </summary>
        public void DataBindReceptorID()
        {
            if (this.ReceptorID < 0)
                return;

            this.InitializeLoadReceptor();
        }
        /// <summary>
        /// Initialize the receptor of the billing (client). Use this to initialize any control
        /// </summary>
        public void InitializeLoadReceptor()
        {
            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table;
            //PACFD.Rules.CountrySelectors select;
            //PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable countrytable;

            table = receptor.SelectByID(this.ReceptorID);// .SelectByBillersID(this.ReceptorID);//this.BillerID);

            if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID < 1)
            {
                //this.Enabled = false;
                this.divReceptorSearch.Visible = true;
            }
            else if (this.EditorMode == BillingsEditorMode.Add && this.ReceptorID > 0)
            {
                this.divReceptorSearch.Visible = false;
                //this.Enabled = true;
                //this.divSearchArticle.Visible = true;
            }

            if (table.Count < 1)
                return;

            //select = new PACFD.Rules.CountrySelectors();
            //countrytable = select.SelectCityStateCountryByCityID(table[0].Location.ToInt32());

            //if (countrytable.Count > 0)
            //    this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}",
            //    countrytable[0].City, countrytable[0].State, countrytable[0].Country);
            //else
            //    this.lblReceptorCityText.Text = "(No disponible)";

            //countrytable.Dispose();

            this.lblReceptorNameText.Text = table[0].Name;
            this.lblReceptorRFCText.Text = table[0].RFC;
            this.lblReceptorAddressText.Text = table[0].Address;
            this.lblReceptorCPText.Text = table[0].Zipcode.ToString() == "-1" ? String.Empty : table[0].Zipcode.ToString();
            this.lblReceptorCityText.Text = string.Format("{0}, {1}, {2}", table[0].Location, table[0].State, table[0].Country);
            table.Dispose();

            if (this.EditorMode == BillingsEditorMode.Add)
                this.btnClientEdit.Visible =
                    //this.tableNotCalculableConceps.Visible =
                    this.btnSelectAnotherReceptor.Visible = true;
            else
                this.btnClientEdit.Visible =
                    //this.tableNotCalculableConceps.Visible =
                    this.btnSelectAnotherReceptor.Visible = false;

            this.ddlPlaceDispatch.ClearSelection();
            this.ddlPlaceDispatch.Items.Clear();
            this.ddlPlaceDispatch.SelectedIndex = -1;
            this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
            this.ddlPlaceDispatch.DataBind();
            this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlPlaceDispatch.SelectedIndex = 0;

            if (this.ddlPlaceDispatch.Items.Count > 1)
                this.ddlPlaceDispatch.SelectedIndex = 1;

            this.ddlPlaceDispatch.Enabled = this.ddlPlaceDispatch.Items.Count > 0 && this.EditorMode == BillingsEditorMode.Add &&
                (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
                ;
            this.divDateSelector.Visible =
                this.EditorMode == BillingsEditorMode.Add && (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2
              || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD ||this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3);
            this.txtDateSelector_CalendarExtender.SelectedDate = DateTime.UtcNow;
            this.txtDateSelector_CalendarExtender.Format = "dd/MM/yyyy hh:mm tt";

            this.OnReceptorDataBind(EventArgs.Empty);
        }
        /// <summary>
        /// Fire the ApplyChange event of the control.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected void OnGeneratedBilling(BillingsEditorEventArgs e)
        {
            if (!this.GeneratedBilling.IsNull())
                this.GeneratedBilling(this, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnBillingBeforeAdd(BillingsEditorEventArgs e)
        {
            if (!this.BillingBeforeAdd.IsNull())
                this.BillingBeforeAdd(this, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnBillingfterAdd(BillingsEditorEventArgs e)
        {
            if (!this.BillingAfterAdd.IsNull())
                this.BillingAfterAdd(this, e);
        }
        /// <summary>
        /// Generate a BillingsEditorEventArgs class used as arguments by the control.
        /// </summary>
        /// <param name="validateconcepts">Add concepts to data set.</param>
        /// <returns>Return a BillingsEditorEventArgs class.</returns>
        /// <exception cref="PACFDManager.Billings.NotDigitalCertificatesAvailableException">
        /// Throw when not a digital certificate are available in for the current biller.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotBillerFoundException">
        /// Throw when not biller found.
        /// </exception>
        /// <exception cref="PACFDManager.Billings.NotReceptorFoundException">
        /// Throw when not receptor found.
        /// </exception> 
        protected BillingsEditorEventArgs OnGenerateBillingsEditorEventArgs()
        {
            DateTime outtime = DateTime.Now;
            decimal exchangerate = this.CurrentExchangeRate;
            //declaration...
            PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row rbillpayment;
            PACFD.DataAccess.BillingsDataSet data = new PACFD.DataAccess.BillingsDataSet();

            ///Billings table general data
            rbillpayment = data.BillingsPayments_3.NewBillingsPayments_3Row(); //create new row
            rbillpayment.BeginEdit();
            rbillpayment.Active = true;

            if (this.BillerID < 1)
                throw new Billings.NotBillerFoundException("Biller not found.", this.BillerID);

            //if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI3_3)
            //    throw new Billings.NotPlaceDispatchSelected("no se permite");
            if (this.ddlPlaceDispatch.SelectedIndex < 1 && this.ddlPlaceDispatch.Visible)
                throw new Billings.NotPlaceDispatchSelected("Not place dispatch selected.");

            rbillpayment.BillerID = this.BillerID;

            if (this.divDateSelector.Visible && this.txtDateSelector.Visible == true)
            {
                try
                {
                    if (!DateTime.TryParseExact(this.txtDateSelector.Text, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out outtime))
                        throw new CantParseBillingDate("Can't parse date, when trying to add billing CFD. Date: " + this.txtDateSelector.Text);
                }
                catch
                {
                    throw new CantParseBillingDate("Can't parse date, when trying to add billing CFD. Date: " + this.txtDateSelector.Text);
                }

                if ((new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day)) < (new DateTime(outtime.Year, outtime.Month, outtime.Day)))
                    throw new CantParseBillingDate("Date is set on the future :o Date: " + this.txtDateSelector.Text + " != " + DateTime.UtcNow.ToString("dd/MM/yyyy"));
            }
            else
                outtime = DateTime.Now;
            rbillpayment.PaymentDate = outtime;
            rbillpayment.BillingDate = DateTime.Now;

            rbillpayment.BillingPaymentId = this.EditorMode == BillingsEditorMode.Add ? 0 : this.BillingPaymentID;
            rbillpayment.BillingType = "P";

            if (this.ReceptorID < 1)
                throw new Billings.NotReceptorFoundException("Receptor not found.", this.ReceptorID);

            rbillpayment.ReceptorID = this.ReceptorID;

            rbillpayment.PaymentForm = this.BillingInformation.PaymentMethod == null ? null :
                this.BillingInformation.PaymentMethod.Text.Trim() == this.ddlPaymentMethod.Items[0].Text.Trim() ?
                string.Empty : this.BillingInformation.PaymentMethod.Value;
            rbillpayment.PaymentMethod = "";
            rbillpayment.SubTotal = 0;
            rbillpayment.Total = 0;
            rbillpayment.Discount = 0;
            rbillpayment.PaymentTerms = "";
            rbillpayment.InternalBillingType = "Pago";
            //rbill.CurrencyCode = this.CurrencyBillerType.ToLetterString();
            rbillpayment.CurrencyCode = this.ddlCurrencyType.SelectedItem.Text;
            rbillpayment.DiscountDescription = string.Empty;
            rbillpayment.TaxTemplateName = this.TaxTemplateName;
            rbillpayment.ElectronicBillingType = (int)this.CurrentElectronicBillingType; //electronic billing type 
            rbillpayment.InsertOrigin = 1;
            rbillpayment.PlaceDispatch = this.ddlPlaceDispatch.SelectedItem.Value;
            //TODO: REVISAR SI CAMBIA CUANDO SE ELIJE OTRA MONEDA
            rbillpayment.ExchangeRateMXN = 1;

            //this.txtBankAccount.Text = this.txtBankAccount.Text.Trim();
            this.txtReference.Text = this.txtReference.Text.Trim();

            /*if ((this.txtBankAccount.Text.Length > 0 && this.txtBankAccount.Text.Trim().ToUpper() != "XEXX010101000"))
                throw new NotAccountNumberFound("Este campo solo es necesario si el banco ordenante es extranjero y debe ser XEXX010101000, este campo es obligatorio.");
            rbillpayment.BankAccount = this.txtBankAccount.Text.Length >= 4 ?
                this.txtBankAccount.Text : string.Empty;*/

            if (this.txtReference.Text.Length > 100)
                throw new NotAccountNumberFound("Referencia debe tener  máximo 100 digitos");

            rbillpayment.Reference = this.txtReference.Text;

            this.txtBankName.Text = this.txtBankName.Text.Trim();

            if ((this.txtBankName.Text.Length > 0 && this.txtBankName.Text.Length < 4) || this.txtBankName.Text.Length > 250)
                throw new NotAccountNumberFound("Account number must be empty or minimum 4 digits max 250");

            rbillpayment.BankName = this.txtBankName.Text.Length >= 4 ?
                this.txtBankName.Text : string.Empty;

            if (rbillpayment.CurrencyCode == "USD")
            {
                if (!decimal.TryParse(this.txtExchangeRate.Text, out exchangerate))
                    throw new Exception("Can't convert exchange rate");

                rbillpayment.ExchangeRateMXN = exchangerate;
            }

            if (this.CurrencyBillerType == CurrencyType.None)
                throw new Billings.NotReceptorFoundException("Currency type is not valid.", this.ReceptorID);

            if (this.BillingID > 0)
            {
                decimal total = 0;
                if (!decimal.TryParse(this.txtTotal.Text.Trim(), out total) || total < 0.01m)
                    throw new NotAccountNumberFound("La cantidad no es válida");

                if (total + this.GetBillingPaidAmount() > Math.Round(this.GetBillingTotal(), 2))
                {
                    throw new NotAccountNumberFound("La cantidad abonada sobrepasa a la cantidad debida.");
                }
                rbillpayment.Total = total;
            }
            //ignore IsPreBilling property to secure accurate CFD or CBB usage when sealling
            rbillpayment.PreBilling = false;
            //this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? this.IsPreBilling : true;
            //this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? false : true;

            if (!string.IsNullOrEmpty(this.txtBillingExternalFolioText.Text))
                rbillpayment.ExternalFolio = this.txtBillingExternalFolioText.Text;
            else rbillpayment.SetExternalFolioNull();

            if (!string.IsNullOrEmpty(this.txtBillingObservation.Text))
                rbillpayment.Observations = this.txtBillingObservation.Text;
            else rbillpayment.SetObservationsNull();

            rbillpayment.PrintTemplateID = 95; // Se le va a poner el template 95   this.GetPrintTemplateID();
            rbillpayment.Certificate =
                rbillpayment.CertificateNumber =
                string.Empty;

            //TODO Cambiar para el serial y folio.....
            this.FillBillings_SerialRowDataSet(ref rbillpayment); //fill serial table values

            if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CBB && this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.Indeterminate)
                this.FillBillings_DigitalCertificateRowDataSet(ref rbillpayment); //fill digital certificates values

            rbillpayment.Version = this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ? "3.3" : "4.0";
            rbillpayment.OriginalString = string.Empty; //tempo value for original string
            rbillpayment.Seal = string.Empty;
            rbillpayment.EndEdit();
            rbillpayment.UUID = lblUUID.Text;
            rbillpayment.PaymentForm = ddlPaymentMethod.SelectedValue;

            rbillpayment.RfcEmisorCtaBen = txtRfcEmisorBen.Text.Split('-')[0].Trim();
            rbillpayment.RfcEmisorCtaOrd = txtRfcEmisorOrd.Text.Split('-')[0].Trim();

            rbillpayment.CtaBeneficiario = txtNoCuentaBen.Text;
            rbillpayment.CtaOrdenante = txtNoCuentaOrd.Text;

            data.BillingsPayments_3.AddBillingsPayments_3Row(rbillpayment); //add row to table
            this.FillBillings_BillingsDataSet(ref data);
            //----
            //uncomment this line for not calculable concept update
            //----
            //this.FillBillings_BillingsDetailNotes(ref data);

            rbillpayment.BeginEdit();
            rbillpayment.OriginalString = string.Empty;
            rbillpayment.Seal = string.Empty;
            rbillpayment.EndEdit();
            rbillpayment.AcceptChanges(); //apply changes to row.

            if (this.EditorMode == BillingsEditorMode.Add)
                rbillpayment.SetAdded();

            return new BillingsEditorEventArgs(data, this.EditorMode);
        }
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsRow row with the serial and folio 
        /// values needed.
        /// </summary>
        /// <param name="billingrow">Billings row to be filled.</param>
        protected void FillBillings_SerialRowDataSet(ref PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billingrow)
        {
            PACFD.Rules.Series serial = new PACFD.Rules.Series();
            PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable table;

            using (table = serial.GetUnusedSerialByBillerID(this.BillerID, this.CurrentBranchID))
            {
                serial = null;

                if (table.Count < 1 && this.IsPreBilling != PrebillingType.PrebillingNotSerialFolio)
                    throw new NotValidSeriesOrFolios("Series or folios not available.");

                billingrow.SetApprovalNumberNull();
                billingrow.SetApprovalYearNull();
                billingrow.Serial = string.Empty;
                billingrow.Folio = string.Empty;
                billingrow.SetFolioIDNull();
                billingrow.SetSerialIDNull();
                billingrow.SetApprovalNumberNull();
                billingrow.SetApprovalYearNull();
                billingrow.Serial = table[0].Serial;
                billingrow.Folio = table[0].Folio;
                billingrow.FolioID = table[0].FolioID;
                billingrow.SerialID = table[0].SerialID;

            }
        }
        /// <summary>
        /// Fill a PACFD.DataAccess.BillingsDataSet.BillingsRow row with the digital certificates values needed.
        /// </summary>
        /// <param name="billingrow">Billings row to be filled.</param>
        protected void FillBillings_DigitalCertificateRowDataSet(ref PACFD.DataAccess.BillingsDataSet.BillingsPayments_3Row billingrow)
        {
            PACFD.Rules.DigitalCertificates digital = new PACFD.Rules.DigitalCertificates();
            PACFD.DataAccess.DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable table;

            using (table = digital.GetActiveByBillerID(this.BillerID))
            {
                digital = null;

                if (table.Count < 1)
                    throw new NotValidDigitalCertificatesException("Not digital certificate available for emisor.", this.BillerID);

                if (table[0].IsCertificateNumberNull())
                    throw new NotValidDigitalCertificatesException("Not valid digital certificate: CertificateNumber is null or not valid.",
                        this.BillerID, table[0].DigitalCertificateID);

                billingrow.CertificateNumber = table[0].CertificateNumber;
                billingrow.Certificate = table[0].Certificate;
                billingrow.Seal = table[0].Seal;
            }
        }
        protected void FillBillings_BillingsDataSet(ref PACFD.DataAccess.BillingsDataSet billingsdataset)
        {
            int tempoid = 0;
            int i;
            PACFD.DataAccess.BillingsDataSet.PaymentBillingsRow row;

            if (this.DataTableGridView.Rows.Count < 1)
                throw new Exception("Not articles added to list.");

            if (this.BillingID > 0)
            {
                row = billingsdataset.PaymentBillings.NewPaymentBillingsRow();
                row.BeginEdit();
                row.BillingID = this.BillingID;
                row.BillingPaymentID = 0;
                row.EndEdit();
                billingsdataset.PaymentBillings.AddPaymentBillingsRow(row);
                row.AcceptChanges();

                if (this.EditorMode == BillingsEditorMode.Add)
                    row.SetAdded();

                row.SetParentRow(billingsdataset.BillingsPayments_3[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityid"></param>
        /// <returns></returns>
        protected PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow GetCityStateCountry(int cityid)
        {
            PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDRow row;
            PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable table = new PACFD.DataAccess.CountrySelectorDataSet.Cities_GetCityStateCountryByCityIDDataTable();
            PACFD.Rules.CountrySelectors select = new PACFD.Rules.CountrySelectors();

            table = select.SelectCityStateCountryByCityID(cityid);
            row = table[0];
            table.Dispose();
            table = null;
            select = null;

            return row;
        }
        /// <summary>
        /// Get the print ID used to print the PDF.
        /// </summary>
        /// <returns>Return an integer value.</returns>
        protected int GetPrintTemplateID()
        {
            int i;
            PACFD.Rules.TaxTemplate p = new PACFD.Rules.TaxTemplate();

            using (PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplatesDataTable table = p.SelectByID(this.TaxTemplateID))
            {
                p = null;

                if (table.Count < 1)
                    return 1; //defaut PDF report               

                i = table[0].PrintTemplateID;
            }

            return i;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ImageButton_Click(object sender, EventArgs e)
        //{
        //    int id;
        //    ImageButton b = sender as ImageButton;

        //    if (b.IsNull() || this.EditorMode == BillingsEditorMode.View)
        //        return;
        //    var commandArgument = b.CommandArgument.Split(',');
        //    if (!int.TryParse(commandArgument[0], out id))
        //        return;

        //    if (id - 1 < 0 || this.gvSelectedBillings.Rows.Count < 1)
        //        return;
        //    var row = this.DataTableGridView.Rows[id - 1];
        //    this.ddlFacturas.Items.Add(new ListItem(row[BillingsPaymentEditorColumns.UUID].ToString(), row[BillingsPaymentEditorColumns.BillingID].ToString()));
        //    this.gvFacturas.DataBind();

        //    this.DataTableGridView.Rows[id - 1].Delete();

        //    if (this.DataTableGridView.Rows.Count > 0 && this.DataTableGridView.Rows.Count > (id - 1))
        //        this.DataTableGridView.Rows[id - 1].AcceptChanges();

        //    this.BillingInformation.BillingsGrid.DataBind();
        //}
        /// <summary>
        /// Create a new System.Data.DataRow row.
        /// </summary>
        /// <returns>Return a System.Data.DataRow.</returns>
        protected System.Data.DataRow OnCreateNewRow()
        {
            ///DataTableGridView = property in this class, create a
            ///custom data table with a custom datacolumn.
            return this.DataTableGridView.NewRow();
        }
        /// <summary>
        /// Create a new System.Data.DataTable table an set to 
        /// DataTableGridView property.
        /// </summary>
        protected void OnCreateGridDataTable()
        {
            System.Data.DataTable table = new System.Data.DataTable();
            table.Columns.AddRange(BillingsPaymentEditorColumns.GetColumns()); //generate a custom column.
            this.DataTableGridView = table;
        }
        /// <summary>
        /// Add an article to the data grid.
        /// </summary>
        /// <param name="a">Selected article (concept) item.</param>
        private void AddBillingToList(int id)
        {
            PACFD.Rules.Billings billing;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table, datasource;
            PACFD.DataAccess.BillingsDataSet.BillingsRow billingrow;
            System.Data.DataRow row;

            billing = new PACFD.Rules.Billings();
            datasource = new PACFD.DataAccess.BillingsDataSet.BillingsDataTable();
            table = billing.SelectByID(id);

            if (table.Count < 1)
                return;

            billingrow = table[0];
            table.Dispose();
            table = null;
            row = this.BillingInformation.BillingsGrid.NewRow();
            string asd = billingrow.UUID;
            row[Billings.BillingsPaymentEditorColumns.UUID] = billingrow["UUID"];
            row[Billings.BillingsPaymentEditorColumns.Total] = billingrow.Total;
            row[Billings.BillingsPaymentEditorColumns.BillingDate] = billingrow.BillingDate;
            row[Billings.BillingsPaymentEditorColumns.BillingID] = billingrow.BillingID;
            this.BillingInformation.BillingsGrid.AddRow(row);
            row = null;

            this.BillingInformation.BillingsGrid.DataBind();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAceptReceptorSearch_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Receptors r;

            if (this.EditorMode == BillingsEditorMode.View)
                return;

            if (!this.txtReceptorSearch.Visible)
            {
                if (this.ddlReceptorSearch.Items.Count < 1 || this.ddlReceptorSearch.SelectedIndex == 0)
                    return;

                this.ReceptorID = this.ddlReceptorSearch.SelectedItem.Value.ToInt32();
                this.InitializeLoadReceptor();
                this.divReceptorSearch.Visible = false;


                if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                    this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                    this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
                {
                    this.ddlPlaceDispatch.ClearSelection();
                    this.ddlPlaceDispatch.Items.Clear();
                    this.ddlPlaceDispatch.SelectedIndex = -1;
                    this.ddlPlaceDispatch.DataSource = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
                    this.ddlPlaceDispatch.DataBind();
                    this.ddlPlaceDispatch.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
                    this.ddlPlaceDispatch_RequiredFieldValidator.Enabled = true;
                    this.ddlPlaceDispatch_RequiredFieldValidator.EnableClientScript = true;
                    this.ddlPlaceDispatch.Enabled = true;

                    if (this.ddlPlaceDispatch.Items.Count > 1)
                    {
                        this.ddlPlaceDispatch.ClearSelection();
                        this.ddlPlaceDispatch.SelectedIndex = 1;
                    }
                }

                return;
            }

            r = new PACFD.Rules.Receptors();
            this.gdvReceptor.Visible = true;
            this.gdvReceptor.DataSource = r.SelectByNameAndBillerID(this.BillerID, this.txtReceptorSearch.Text);
            this.gdvReceptor.DataBind();
        }

        //protected void BillingSelected(object sender, EventArgs e)
        //{
        //    PACFD.Rules.Billings b;

        //    if (this.EditorMode == BillingsEditorMode.View)
        //        return;

        //    this.BillingID = this.ddlFacturas.SelectedItem.Value.ToInt32();
        //    if (this.BillingID < 1) return;
        //    this.InitializeLoadReceptor();

        //    b = new PACFD.Rules.Billings();
           
        //    this.gvFacturas.Visible = true;
        //    this.gvFacturas.DataSource = b.SelectByID(this.BillingID);
        //    this.gvFacturas.DataBind();
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnBillingSelected_Click(object sender, EventArgs e)
        //{
        //    Button b = sender as Button;

        //    if (b.IsNull())
        //        return;

        //    this.BillingID = b.CommandArgument.ToInt32();
        //    if (this.BillingID < 1) return;
        //    BillingsEditorEventArgs arg;

        //    this.AddBillingToList(this.BillingID);
        //    this.ddlFacturas.Items.Remove(this.ddlFacturas.SelectedItem);
        //    this.gvFacturas.DataBind();
        //    try
        //    {
        //        arg = this.OnGenerateBillingsEditorEventArgs();
        //        this.OnBillingAddedToBill(arg);
        //    }
        //    catch (Exception zxce)
        //    {

        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBankReceptorSelected_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b.IsNull())
                return;

            this.ReceptorID = b.CommandArgument.ToInt32();
            this.divReceptorSearch.Visible = false;
            this.gdvReceptor.DataBind();
            //this.ddlFacturas.Visible = true;
            //this.ddlFacturas.DataValueField = "BillingID";
            //this.ddlFacturas.DataTextField = "UUID";
            //this.ddlFacturas.DataSource = new PACFD.Rules.Billings().SelectByReceptorID(this.ReceptorID)
            //    .Select("BillingPaymentId IS NULL AND UUID IS NOT NULL AND UUID <> '' AND PaymentMethod LIKE 'PPD'");
            //this.ddlFacturas.DataBind();
            //this.ddlFacturas.Items.Insert(0, new ListItem("Seleccionar", "-1"));


            //this.InitializeLoadBankReceptor();
        }
    protected void btnReceptorSelected_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b.IsNull())
                return;

            this.ReceptorID = b.CommandArgument.ToInt32();
            this.divReceptorSearch.Visible = false;
            this.gdvReceptor.DataBind();
            //this.ddlFacturas.Visible = true;
            //this.ddlFacturas.DataValueField = "BillingID";
            //this.ddlFacturas.DataTextField = "UUID";
            //this.ddlFacturas.DataSource = new PACFD.Rules.Billings().SelectByReceptorID(this.ReceptorID)
            //    .Select("BillingPaymentId IS NULL AND UUID IS NOT NULL AND UUID <> '' AND PaymentMethod LIKE 'PPD'");
            //this.ddlFacturas.DataBind();
            //this.ddlFacturas.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            this.InitializeLoadReceptor();
        }
        /// <summary>
        /// Event used to know when an autocomplet is called.
        /// </summary>

        /// <summary>
        /// Set the state of the controls for search concepts
        /// </summary>
        /// <param name="enable">State of the controls.</param>

        /// <summary>
        /// Add concept to the list of concept and clear the search of concepts.
        /// </summary>

        /// <summary>
        /// Invoke a concept added to list event, to recalculate preview and concept updates.
        /// </summary>
        /// <param name="validateconcepts">
        /// True to vaidate conceps (articles) in the list else false.
        /// If true and not concept has been added to the list then an exception empty concepts is fired.
        /// </param>
        /// <exception cref="">System.Exception "Not articles added to list."</exception>
        public void InvokeBillingAddedToBill(bool validateconcepts)
        {
            BillingsEditorEventArgs arg = this.OnGenerateBillingsEditorEventArgs();
            this.OnBillingAddedToBill(arg);
        }

        protected void OnBillingAddedToBill(Billings.BillingsEditorEventArgs e)
        {
            if (!this.BillingAfterAddToBill.IsNull())
                this.BillingAfterAddToBill(this, e);
        }
        /// <summary>
        /// The user has request a new receptor.
        /// </summary>
        protected void btnReceptorAddNew_Click(object sender, EventArgs e)
        {
            this.OnAddNewReceptor(e);
        }
        /// <summary>
        /// The user has request a new receptor.
        /// </summary>
        /// <param name="e">Events arguments send by the control.</param>
        protected virtual void OnAddNewReceptor(EventArgs e)
        {
            if (!this.AddNewReceptor.IsNull())
                this.AddNewReceptor(this, e);
        }
        /// <summary>
        /// The user has reuqest a new concept to the data base.
        /// </summary>

        /// <summary>
        /// The user has reuqest a new concept to the data base.
        /// </summary>
        /// <param name="e">Event arguments send by the control.</param>
        protected virtual void OnAddNewBilling(EventArgs e)
        {
            if (!this.AddNewBilling.IsNull())
                this.AddNewBilling(this, e);
        }
        /// <summary>
        /// Select another client and reinitialize the fields.
        /// </summary>
        protected void btnSelectAnotherReceptor_Click(object sender, EventArgs e)
        {
           
                this.lblReceptorAddressText.Text =
                this.lblReceptorCityText.Text =
                this.lblReceptorCPText.Text =
                this.lblReceptorNameText.Text =
                this.lblReceptorRFCText.Text = string.Empty;

            //this.gvSelectedBillings.DataBind();
            //this.gvFacturas.DataBind();
            this.gdvReceptor.DataBind();

            //this.txtAddConceptAmount.Text = "1";
            //this.txtAddConceptPrice.Text = "0.0";
            //this.hflArticleSearch.Value = "0";
            //this.revAddConceptPrice.Validate();

            if (this.ddlReceptorSearch.Items.Count > 0)
                this.ddlReceptorSearch.SelectedIndex = 0;

            this.ddlPaymentMethod.SelectedIndex = 0;
            this.gdvReceptor.Visible = false;

            this.divReceptorSearch.Visible = true;

         
                //this.ttpSelectAnotherReceptor.Visible =
                this.btnClientEdit.Visible =
                this.btnSelectAnotherReceptor.Visible =
                this.ddlPaymentMethod.Enabled =
                this.txtExchangeRate.Enabled =
                this.ddlCurrencyType.Enabled = true;

            this.DataTableGridView = null;
            //this.EnableConceptSearch(true);
        }
        /// <summary>
        /// Calculate tax totals when the dinamyc textbox and dropdownlist are changed.
        /// </summary>

        protected void btnClientEdit_Click(object sender, EventArgs e)
        {
            BillingsClientEditEventArgs a = new BillingsClientEditEventArgs(this.ReceptorID, this.EditorMode);
            this.OnClientModify(a);
        }
        /// <summary>
        /// Request a client modification.
        /// </summary>
        protected void OnClientModify(BillingsClientEditEventArgs e)
        {
            if (!this.ClientModify.IsNull())
                this.ClientModify(this, e);
        }
        /// <summary>
        /// Recalculate totals with the actual currency 
        /// </summary>
        protected void ddlCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.ddlCurrencyType.SelectedItem.Text.Trim().ToLower())
            {
                default:
                case "mxn":
                    this.CurrencyBillerType = CurrencyType.MXN;
                    this.txtExchangeRate_RangeValidator.Enabled = false;
                    break;
                case "usd":
                    this.CurrencyBillerType = CurrencyType.USD;
                    this.txtExchangeRate_RangeValidator.Enabled = true;
                    break;
            }

            //this.BillingInformation.CalculateTotal();
        }

        protected void ckbDateSelector_CheckedChanged(object sender, EventArgs e)
        {
            this.txtDateSelector.Visible = !this.txtDateSelector.Visible;
        }

        protected void btnNotCalculableConcept_Click(object sender, EventArgs e)
        {
            //PACFD.DataAccess.BillingsDataSet.BillingsDetailNotesRow row = this.DataTableGridViewNoCalculableConcept.NewBillingsDetailNotesRow();
            //row.BillingID = 0;

            //row.BillingDetailNoteID = System.Environment.TickCount * -1;

            //if (!string.IsNullOrEmpty(this.txtCodeNotCalculableConcept.Text))
            //    row.Code = this.txtCodeNotCalculableConcept.Text;
            //else row.SetCodeNull();

            //if (!string.IsNullOrEmpty(this.txtCountNotCalculableConcept.Text))
            //    row.Count = this.txtCountNotCalculableConcept.Text.ToDecimal();
            //else row.SetCountNull();

            //if (!string.IsNullOrEmpty(this.txtDescriptionNotCalculableConcept.Text))
            //    row.Description = this.txtDescriptionNotCalculableConcept.Text;
            //else row.SetDescriptionNull();

            //if (!string.IsNullOrEmpty(this.txtImportNotCalculableConcept.Text))
            //    row.Import = this.txtImportNotCalculableConcept.Text.ToDecimal();
            //else row.SetImportNull();

            //if (!string.IsNullOrEmpty(this.txtPriceNotCalculableConcept.Text))
            //    row.Price = this.txtPriceNotCalculableConcept.Text.ToDecimal();
            //else row.SetPriceNull();

            //this.DataTableGridViewNoCalculableConcept.AddBillingsDetailNotesRow(row);
            //this.gdvNotCalculableConceps.DataSource = this.DataTableGridViewNoCalculableConcept;
            //this.gdvNotCalculableConceps.DataBind();
        }
    }

    public enum PrebillingType
    {
        None = 0,
        Prebilling = 1,
        PrebillingNotSerialFolio
    }
}