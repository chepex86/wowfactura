﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    /// <summary>
    /// Extencion for System.Data.DataTable
    /// </summary>
    internal static class ExtensionTable
    {
        /// <summary>
        /// Marger tables with diferents columns names.
        /// </summary>
        /// <param name="table">Table destiny.</param>
        /// <param name="from">Table source to be marge.</param>
        /// <param name="rows">Rows to be added.</param>
        public static void Merge(this System.Data.DataTable table, System.Data.DataTable from, System.Data.DataRow[] rows)
        {
            System.Data.DataRow row;
            System.Data.DataColumn[] columns = new System.Data.DataColumn[from.Columns.Count];

            for (int i = 0; i < columns.Length; i++)
            {
                if (!table.ColumnNameExist(from.Columns[i].ColumnName))
                    columns[i] = new System.Data.DataColumn(from.Columns[i].ColumnName, from.Columns[i].DataType);
            }

            table.Columns.AddRange(columns);

            for (int i = 0; i < rows.Length; i++)
            {
                row = table.NewRow();
                row.ItemArray = rows[i].ItemArray;                
                table.Rows.Add(row);
            }
        }
        /// <summary>
        /// Get a booblean value if a columname excist or not.
        /// </summary>
        /// <param name="table">Table with the columns to look into.</param>
        /// <param name="name">Column name to search.</param>
        /// <returns>If founded return true else false.</returns>
        private static bool ColumnNameExist(this System.Data.DataTable table, string name)
        {
            for (int i = 0; i < table.Columns.Count; i++)
                if (table.Columns[i].ColumnName == name)
                    return true;

            return false;
        }

        public static int ColumnNameIndex(this System.Data.DataTable table, string name)
        {
            for (int i = 0; i < table.Columns.Count; i++)
                if (table.Columns[i].ColumnName == name)
                    return i;

            return -1;
        }
    }
}
