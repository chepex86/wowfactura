﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Linq;
#endregion

namespace PACFDManager.Billings
{
    public partial class BillingsList : PACFDManager.BasePage
    {
        private const string MESSAGE_CANCEL = "MESSAGE_CANCEL";
        private const string MESSAGE_CANCEL_SUCCESS = "MESSAGE_CANCEL_SUCCESS";
        private const string MESSAGE_SEAL = "MESSAGE_SEAL";
        private const string MESSAGE_CBB_TO_BILLING = "MESSAGE_CBB_TO_BILLING";
        private const string MESSAGE_SEAL_SUCCESS = "MESSAGE_SEAL_SUCCESS";
        private const string MESSAGE_SEND_EMAIL = "MESSAGE_SEND_EMAIL";
        private const string MESSAGE_CANCEL_ERROR = "MESSAGE_CANCEL_ERROR";
        private const string MESSAGE_NOAVAILABLE = "MESSAGE_NOAVAILABLE";

        // cuando solo se envio a cancelar pero no se ha cancelado. 
        private const string MESSAGE_CANCEL_SENT = "MESSAGE_CANCEL_SENT";




        /// <summary>
        /// Billing to be cancel.
        /// </summary>
        private int TemporalBillingID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        private PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable SearchDataTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                return o.GetType() == typeof(PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable) ?
                    (PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable)o : null;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        private string SortColumn
        {
            get
            {
                object result;

                result = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (result.IsNull())
                    return "ASC";

                return result.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        private SortDirection SortDirectionUsed
        {
            get
            {
                object o = this.ViewState[string.Format("{0}_{1}", this.ClientID,
                   System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1])];

                if (o.IsNull() || o.GetType() != typeof(SortDirection))
                    return SortDirection.Ascending;

                return (SortDirection)Enum.Parse(typeof(SortDirection), o.ToString());
            }
            set { this.ViewState[string.Format("{0}_{1}", this.ClientID, System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1])] = value; }
        }

        /// <summary>
        /// Lod of the page.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.ProgressBar.Visible = false;

            if (this.IsPostBack)
            {
                return;
            }

            ScriptManager m = ScriptManager.GetCurrent(this.Page);
            m.Scripts.Add(new ScriptReference(string.Format("~/Billings/BillingList.js?p={0};p2={1}"
                , this.Master.ProgressBar.UniqueID
                , this.Master.ProgressBar.ClientID)));


            btnPreBillingNotSerialFolio.Visible = btnPreBillingNotSerialFolio2.Visible = false;

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                btnPreBillingNotSerialFolio.Visible = btnPreBillingNotSerialFolio2.Visible = true;

            this.Master.ProgressBar.AssociatedUpdatePanelID = this.UpdatePanel1.ID;
        }
        /// <summary>
        /// Load completed of the page.
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.BillingsSearch1.DateChecked = true;
            this.BillingsSearch1.Date = DateTime.Now;
            this.BillingsSearch1.InvokeSearch();
        }
        /// <summary>
        /// Search event of the search control.
        /// </summary>
        /// <param name="sender">Object sender of the event.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void BillingsSearch1_Search(object sender, BillingsSearchEventArgs e)
        {
            decimal totalglobal = 0
                //, paidtotal = 0
                ;
            //System.Data.DataRow[] rows;
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            int? branchID = null;

            if (CurrentBranchID != -1)
                branchID = CurrentBranchID;

            this.SearchDataTable = bill.Search(
                PACFD.Rules.Billings.SearchPetitionFromFlag.WebForm
                , this.CurrentBillerID
                , e.Receptor
                , e.Folio.SerialText
                , e.Folio.FolioNumber.IsNull() ? null : e.Folio.FolioNumber.ToString()
                , e.Date
                , e.PreBilling
                , e.IsPaid
                , e.UUID
                , branchID
                , e.ExternalFolio
                , e.BillingID
                , null
                );

            this.lblNumberBillings.Text = this.SearchDataTable.Count.ToString();

            //if (this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI)            
            //    this.gdvBillings.Columns[1].Visible = false;            
            this.gdvBillings.Columns[1].Visible = (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3);

            this.gdvBillings.DataSource = this.SearchDataTable;
            this.gdvBillings.DataBind();
            this.GridViewBillingsValidateHeaders();

            totalglobal = 0;

            foreach (PACFD.DataAccess.BillingsDataSet.BillingsSearchRow row in this.SearchDataTable)
            {
                if (row.CurrencyCode != "MXN")
                    totalglobal += row.Total * row.ExchangeRateMXN;
                else
                    totalglobal += row.Total;

                //if (row.PreBilling || row.BillingType.Trim().ToLower() == "egreso" || !row.Active)
                //    continue;

                //if (!row.IsIsCreditNull() && row.IsCredit)
                //    if (row.IsIsPaidNull() || !row.IsPaid)
                //        continue;

                //if (row.CurrencyCode != "MXN")
                //    paidtotal += row.Total * row.ExchangeRateMXN;
                //else
                //    paidtotal += row.Total;
            }

            this.lblGlobalTotals.Text = string.Format("{0:c} MXN", totalglobal);
            //this.lblGlobalPaidTotal.Text = string.Format("{0:c} MXN", paidtotal);

            //rows = this.SearchDataTable.Select("IsCredit=false and prebilling=false and Active=true");
            //this.lblGlobalNumberPay.Text = rows.Length.ToString();

            //rows = this.SearchDataTable.Select("BillingType='%egreso%'");
            //this.lblGlobalEgresos.Text = string.Format("{0}", rows.Length);

            //this.spanGlobalEgresos.Attributes["title"] = "Numero de egresos.";

            //if (rows.Length > 0)
            //{
            //    this.spanGlobalEgresos.Attributes["title"] += "<br />IDs:";

            //    foreach (System.Data.DataRow r in rows)
            //    {
            //        this.spanGlobalEgresos.Attributes["title"] += ((PACFD.DataAccess.BillingsDataSet.BillingsSearchRow)r).BillingID.ToString() + ",";
            //    }
            //}

        }
        /// <summary>
        /// 
        /// </summary>
        private void GridViewBillingsValidateHeaders()
        {
            //Label lhader;
            //IQueryable q;

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
            {
                for (int i = 0; i < this.gdvBillings.Columns.Count; i++)
                {
                    //this.gdvBillings.Columns[i].HeaderText.Trim().ToLower() != "sellado" &&
                    //this.gdvBillings.Columns[i].HeaderText.Trim().ToLower() != "pre." &&

                    if (this.gdvBillings.Columns[i].HeaderText.Trim().ToLower() == "addenda")
                        this.gdvBillings.Columns[i].Visible = false;

                    if (this.gdvBillings.Columns[i].HeaderText.Trim().ToLower() != "xml")
                        continue;

                    this.gdvBillings.Columns[i].Visible = false;
                }
            }
        }
        /// <summary>
        /// Add new bill method.
        /// </summary>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int i = 0;
            Button b = sender as Button;

            if (b.IsNull())
                return;

            int.TryParse(b.CommandArgument, out i);
            i = i > 2 ? 2 : i < 0 ? 0 : i;
            this.Response.Redirect(string.Format("{0}.aspx{1}", typeof(BillingsAdd).Name, i > 0 ? "?pre=" + i.ToString() : string.Empty));
        }
        /// <summary>
        /// Image button commands.
        /// </summary>
        /// <param name="sender">Image buttond sendser of the event.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImageButton_Click(object sender, EventArgs e)
        {
            ImageButton b = sender as ImageButton;

            if (b.IsNull())
                return;

            this.Master.ProgressBar.Visible = false;

            switch (b.ID)
            {
                case "imbDetails":
                    this.Session["id"] = b.CommandArgument;
                    this.Response.Redirect(string.Format("{0}{1}",
                        typeof(BillingsDetails).Name,
                        b.ID == "imbDetails" ? ".aspx?e=false" : ".aspx"));
                    break;
                case "imbCancel":
                    this.TemporalBillingID = b.CommandArgument.ToInt32();
                    this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL;
                    this.wmbCancelBilling.ShowMessage(
                        this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI ?
                        "¿Está seguro de cancelar el comprobante? Reactivar el comprobante podria requerir de un proceso de validación ante el \"SAT\"."
                        : "¿Está seguro de la cancelación? este movimiento se roportara al SAT."
                        , System.Drawing.Color.Red, WebMessageBoxButtonType.YesNo);
                    //this.wmbCancelBilling.ShowMessage(
                    //   this.CurrentElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFDI ?
                    //   "Estamos trabajando para habilitar esta opcion, por el momento no esta disponible."
                    //   : "Aun no se ha habilitado esta opcion "
                    //   , System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    break;
                //case "imbDownloadPDF":
                //    //this.Response.Redirect(@"~\Billings\XML\PDFCreator.ashx?id=" + b.CommandArgument);
                //    this.Response.Redirect(string.Format(@"~\Billings\XML\DownloadPage.aspx?type=pdf&id={0}", b.CommandArgument));
                //    break;
                //case "imbDownloadXML":
                //    //this.Response.Redirect(@"~\Billings\XML\XmlCreator.ashx?id=" + b.CommandArgument);
                //    this.Response.Redirect(string.Format(@"~\Billings\XML\DownloadPage.aspx?type=xml&id={0}", b.CommandArgument));
                //    break;
                case "imbSeal":

                    this.TemporalBillingID = b.CommandArgument.ToInt32();

                    if (this.TemporalBillingID < 1)
                        return;

                    if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)

                    {
                        this.wmbCancelBilling.CommandArguments = MESSAGE_SEAL;
                        this.wmbCancelBilling.ShowMessage("¿Desea sellar el comprobante? Esto tardará unos segundos.", System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    }
                    else if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                    {
                        this.wmbCancelBilling.CommandArguments = MESSAGE_CBB_TO_BILLING;
                        this.wmbCancelBilling.ShowMessage("¿ya no es posible timbrar este comprobante?", System.Drawing.Color.Blue, WebMessageBoxButtonType.Accept);
                    }

                    break;
                case "imbEmailBilling":
                    this.Session["id"] = b.CommandArgument;
                    this.Response.Redirect(typeof(SendBillingMail).Name + ".aspx");
                    break;
                case "imbAddendum":
                    this.Session["id"] = b.CommandArgument;
                    this.Response.Redirect("~/Billings/Addendum/" + typeof(Addendum.AddendumApply).Name + ".aspx");
                    break;
            }
        }
        /// <summary>
        /// Seal a bill from ID.
        /// </summary>
        /// <param name="billingid">Billing ID to seal.</param>
        private void SealBilling(int billingid)
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            PACFD.Common.ErrorManager error = new PACFD.Common.ErrorManager();
            Action changesealimage = () =>
            {
                GridViewRow row = null;
                ImageButton ibutt;

                foreach (GridViewRow e in this.gdvBillings.Rows)
                {
                    ibutt = (ImageButton)e.FindControl("imbDetails");

                    if (ibutt.CommandArgument.ToInt32() == billingid)
                    {
                        row = e;
                        break;
                    }
                }

                if (!row.IsNull())
                {
                    ibutt = (ImageButton)row.FindControl("imbSeal");
                    ibutt.AlternateText = "Comprobante sellado.";
                    ibutt.CommandArgument = "0";
                    ibutt.ImageUrl = "~/Includes/Images/png/apply.png";
                }
            };

            using (PACFD.DataAccess.BillingsDataSet dataset = bill.GetFullBilling(billingid))
            {
                if (dataset.Billings.Count > 0 && dataset.Billings[0].IsFolioIDNull())
                {
                    using (PACFD.DataAccess.SerialDataSet.Serial_GetUnusedFolioByBillerIDDataTable unusedserial =
                        (new PACFD.Rules.Series()).GetUnusedSerialByBillerID(this.CurrentBillerID, this.CurrentBranchID))
                    {
                        dataset.Billings[0].BeginEdit();
                        dataset.Billings[0].Serial = unusedserial[0].Serial;
                        dataset.Billings[0].Folio = unusedserial[0].Folio;
                        dataset.Billings[0].FolioID = unusedserial[0].FolioID;
                        dataset.Billings[0].SerialID = unusedserial[0].SerialID;
                        dataset.Billings[0].ApprovalNumber = unusedserial[0].AprovationNumber;
                        dataset.Billings[0].ApprovalYear = unusedserial[0].AprovationYear;

                        if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                        {
                            if (unusedserial[0].QRImage.IsNull())
                            {
                                this.wmbCancelBilling.CommandArguments = string.Empty;
                                this.wmbCancelBilling.ShowMessage("Error al sellar precomprobante no foliado, no se pudo obtener el Qr.",
                                    System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                                return;
                            }

                            dataset.Billings[0].QrImage = unusedserial[0].QRImage;
                        }

                        dataset.Billings[0].EndEdit();
                        dataset.Billings[0].AcceptChanges();
                        dataset.Billings[0].SetModified();
                    }

                    if (!bill.Update(dataset) || !bill.SealCFD(dataset, ref error))
                    {
                        bill = null;
                        string m = "Error al intentar sellar el comprobante.";

                        if (error != null)
                            m = string.Format("Error: {0}", error.Error.Message);

                        this.wmbCancelBilling.CommandArguments = string.Empty;
                        this.wmbCancelBilling.ShowMessage(m, System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                        return;
                    }
                    else
                    {
                        bill = null;
                        changesealimage();
                        this.wmbCancelBilling.CommandArguments = MESSAGE_SEAL_SUCCESS;
                        this.wmbCancelBilling.ShowMessage("Comprobante sellado con éxito.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                        return;
                    }
                }
            }

            if (!bill.SealCFD(billingid, ref error))
            {
                bill = null;
                string m = "Error al intentar sellar el comprobante.";

                if (error != null)
                    m = string.Format("Error: {0}", error.Error.Message);

                this.wmbCancelBilling.CommandArguments = string.Empty;
                this.wmbCancelBilling.ShowMessage(m, System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            bill = null;
            changesealimage();
            this.wmbCancelBilling.CommandArguments = MESSAGE_SEAL_SUCCESS;
            this.wmbCancelBilling.ShowMessage("Comprobante sellado con éxito.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }

        protected void wmbCancelBilling_Click(object sender, WebMessageBoxEventArgs e)
        {
            bool canceled = false;
            //System.Data.DataRow[] rows;
            PACFD.DataAccess.BillingsDataSet.BillingsSearchRow[] srows;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable;

            switch (e.CommandArguments)
            {

                case MESSAGE_NOAVAILABLE:
                    this.TemporalBillingID = 0;
                    break;
                case MESSAGE_CANCEL:
                    if (this.TemporalBillingID < 1 || e.DialogResult == WebMessageBoxDialogResultType.No)
                    {
                        this.TemporalBillingID = 0;
                        return;
                    }

                    try
                    {
                        canceled = (new PACFD.Rules.Billings()).SetActive(this.TemporalBillingID, false);
                    }
                    catch (PACFD.Rules.BillingsExceptions.BillingCancelError ex1)
                    {
                        this.TemporalBillingID = 0;
                        this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL_ERROR;
                        this.wmbCancelBilling.ShowMessage(
                            string.Format("Error cancelando CFDI: {0}, Codigo: {1}", ex1.Message, ex1.PacError.Error.Code)
                            , true
                            , WebMessageBoxButtonType.Accept);
                        break;
                    }

                    if (canceled)
                    {
                        srows = this.SearchDataTable.Select(string.Format("BillingID={0}", this.TemporalBillingID)) as PACFD.DataAccess.BillingsDataSet.BillingsSearchRow[];

                        if (srows != null && srows.Length > 0)
                        {
                            srows[0].BeginEdit();
                            srows[0].Active = false;
                            srows[0].EndEdit();
                            srows[0].AcceptChanges();
                            this.gdvBillings.DataSource = this.SearchDataTable;
                            this.gdvBillings.DataBind();
                            this.GridViewBillingsValidateHeaders();
                        }

                        this.wmbCancelBilling.ShowMessage("Cancelación existosa", WebMessageBoxButtonType.Accept);
                        this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL_SUCCESS;
                    }
                    else
                    {
                        this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL_ERROR;
                        this.wmbCancelBilling.ShowMessage("Error en la cancelación", WebMessageBoxButtonType.Accept);
                    }

                    this.TemporalBillingID = 0;
                    //this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
                    break;
                case MESSAGE_CANCEL_SUCCESS:
                case MESSAGE_CANCEL_ERROR:
                    break;
                case MESSAGE_SEAL:
                    this.SealBilling(this.TemporalBillingID);
                    this.TemporalBillingID = 0;
                    this.wmbCancelBilling.CommandArguments = "";
                    break;
                case MESSAGE_SEAL_SUCCESS:
                    this.Response.Redirect(typeof(BillingsList).Name + ".aspx");
                    break;
                case MESSAGE_SEND_EMAIL:

                    break;
                case MESSAGE_CBB_TO_BILLING:
                    if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
                    {
                        using (billtable = (new PACFD.Rules.Billings()).SelectByID(this.TemporalBillingID))
                        {
                            if (billtable.Count < 1)
                            { return; }

                            billtable[0].BeginEdit();
                            billtable[0].PreBilling = false;
                            billtable[0].AcceptChanges();
                            billtable[0].SetModified();
                            billtable[0].EndEdit();
                            canceled = (new PACFD.Rules.Billings()).Update(billtable);
                        }

                        if (canceled)
                        {
                            this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL_SUCCESS;
                            this.wmbCancelBilling.ShowMessage("Precomprobante registrado como comprobante.", WebMessageBoxButtonType.Accept);
                            srows = this.SearchDataTable.Select(string.Format("BillingID={0}", this.TemporalBillingID)) as PACFD.DataAccess.BillingsDataSet.BillingsSearchRow[];

                            if (srows != null && srows.Length > 0)
                            {
                                srows[0].BeginEdit();
                                srows[0].PreBilling = false;
                                srows[0].EndEdit();
                                srows[0].AcceptChanges();
                                this.gdvBillings.DataSource = this.SearchDataTable;
                                this.gdvBillings.DataBind();
                                this.GridViewBillingsValidateHeaders();
                            }
                        }
                        else
                        {
                            this.wmbCancelBilling.CommandArguments = MESSAGE_CANCEL_ERROR;
                            this.wmbCancelBilling.ShowMessage("No se pudo registrar Precomprobante como comprobante.", WebMessageBoxButtonType.Accept);
                        }
                    }

                    this.TemporalBillingID = 0;
                    break;
            }
        }
        /// <summary>
        /// Get a string with the encrypted BillerID.
        /// </summary>
        /// <param name="id">ID to encrypt.</param>
        /// <returns>string value.</returns>
        protected string DataBindEncryptBillingAndBillerID(string id)
        {
            return PACFD.Common.Cryptography.EncryptUnivisitString(id);
        }
        /// <summary>
        /// Get a string value with the available state for paid bills.
        /// </summary>
        /// <param name="datbinder">Data binder of the gridview.</param>
        /// <returns>"No aplica." or "Pagado" / "No pagado" link.</returns>
        protected string DataBind_IsPaid(object datbinder)
        {
            System.Data.DataRowView b = datbinder as System.Data.DataRowView;
            string
                billtype = b.Row[8].ToString()
                , isactive = b.Row[1].ToString()
                , sispaid = b.Row[15].ToString()
                , billingid = b.Row[0].ToString()
                , prebill = b.Row[7].ToString()
                , iscredit = b.Row[17].ToString()
                , electronicbillyngtype = b.Row[18].ToString()
                , paymentmethod = b["PaymentMethod"].ToString()
                ;
            StringBuilder sb = new StringBuilder();
            bool ispaid = false;

            if (string.IsNullOrEmpty(billtype) || 
                billtype.Trim().ToLower() == "e" || 
                isactive.Trim().ToLower() == "false" || 
                prebill.Trim().ToLower() == "true"
                //|| iscredit.Trim().ToLower() == "false"
                /*|| paymentmethod.Trim().ToLower() == "pue"*/)
                return "<span style=\"color:Red;\">No aplica.</span>";

            if (string.IsNullOrEmpty(sispaid))
                ispaid = false;

            bool.TryParse(sispaid, out ispaid);
            string redirectTo = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["ShowNewPayment"]) ? "../Billings/BillingsPaymentList.aspx" :
                "../BillingStatement/BillingStatementList.aspx";
            sb.AppendLine(string.Format(
                "<span class=\"vtip\" title=\"Estado del <b>(pre)comprobante</b>: {0}.\">",
                ispaid == true ? "<b>Pago</b> en su totalidad" : "<b>No pagado</b>"));
            sb.AppendLine(string.Format("<a href='{0}?pid={1}'>",
                redirectTo,
                PACFD.Common.Cryptography.EncryptUnivisitString(billingid)));
            sb.AppendLine(ispaid == true ? "Pagado" : "No pagado");
            sb.AppendLine("</a>");
            sb.AppendLine("</span>");

            return sb.ToString();
        }
        /// <summary>
        /// Grid page index changer method.
        /// </summary>
        /// <param name="e">GridViewPageEventArgs send by the grid.</param>
        protected void gdvBillings_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvBillings.PageIndex = e.NewPageIndex;
            this.lblNumberBillings.Text = this.SearchDataTable.Rows.Count.ToString();
            this.gdvBillings.DataSource = this.SearchDataTable;
            this.gdvBillings.DataBind();
        }
        /// <summary>
        /// Grid sort method.
        /// </summary>
        /// <param name="sender">GridViewSortEventArgs send by the grid.</param>
        protected void gdvBillings_Sorting(object sender, GridViewSortEventArgs e)
        {
            this.SortColumn = e.SortExpression;
            this.SearchDataTable.DefaultView.Sort = string.Format("{0} {1}", e.SortExpression, this.SortDirectionUsed == SortDirection.Ascending ? "DESC" : "ASC");
            this.SortDirectionUsed = this.SortDirectionUsed == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
            this.gdvBillings.DataSource = this.SearchDataTable;
            this.gdvBillings.DataBind();
        }
        /// <summary>
        /// Get a string with the javascript client to do a postback to sort.
        /// </summary>
        /// <param name="sort">Column to be sort.</param>
        /// <returns>string value. javascript:__doPostBack("clientID","Sort$columnID")</returns>
        protected string GetSortJavaScript(string sort)
        {
            return string.Format("javascript:__doPostBack(\"{0}\",\"Sort${1}\")",
                this.gdvBillings.ClientID.Replace("_", "$"), sort);
        }
        /// <summary>
        /// Get a boolean value indicating if the colum sort order is up or down.
        /// </summary>
        /// <returns>boolean value.</returns>
        protected bool GetSortedIsUp()
        {
            if (this.SortDirectionUsed == SortDirection.Descending)
                return true;

            return false;
        }
        /// <summary>
        /// Get a boolean value with the visible state of the column.
        /// </summary>
        /// <param name="column">Column to evalueate.</param>
        /// <returns>boolean value.</returns>
        protected bool GetColumnSortedVisible(string column)
        {
            if (this.SortColumn == column)
                return true;

            return false;
        }

        /// <summary>
        /// Seal header tip title
        /// </summary>
        protected string GetSealHeadTitleTip()
        {
            return string.Format(
                "Haga click en la imagen <img src=\"../Includes/Images/png/SealPreBilling.png\" /> para pasar de <b>precomprobante</b> a <b>comprobante</b>.<br />Si el <b>comprobante</b> esta <b>{0}</b> la imagen sera <img src=\"../Includes/Images/png/apply.png\" />."
                , this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? "finalizado" : "sellado");
        }
        /// <summary>
        /// Seal header main title
        /// </summary>
        /// <returns></returns>
        protected string GetSealHeadTitle()
        {
            return this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ? "Acompletar" : "Sellado";
        }

        protected bool GetAddendumApplied(object billingid)
        {
            return (new PACFD.Rules.BillingsAddendum()).Exist(Convert.ToInt32(billingid));
        }

        protected void gdvBillings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            (e.Row.FindControl("imbSeal") as ImageButton).ImageUrl =
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB ?
                DataBinder.Eval(e.Row.DataItem, "PreBilling").ToString() == "False" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/preInvoice.png"
                : DataBinder.Eval(e.Row.DataItem, "PreBilling").ToString() == "False" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/SealPreBilling.png";

            switch (this.CurrentElectronicBillingType)
            {
                case PACFD.Rules.ElectronicBillingType.CFD:
                case PACFD.Rules.ElectronicBillingType.CFD2_2:

                    if (string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "Seal").ToString().Trim()))
                    {
                        (e.Row.FindControl("imbSeal") as ImageButton).ImageUrl = "~/Includes/Images/png/SealPreBilling.png";
                        (e.Row.FindControl("imbSeal") as ImageButton).CommandArgument = DataBinder.Eval(e.Row.DataItem, "BillingID").ToString();
                    }
                    break;
                case PACFD.Rules.ElectronicBillingType.CFDI:
                case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_3:

                    if (string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "UUID").ToString().Trim()))
                    {
                        (e.Row.FindControl("imbSeal") as ImageButton).ImageUrl = "~/Includes/Images/png/SealPreBilling.png";
                        (e.Row.FindControl("imbSeal") as ImageButton).CommandArgument = DataBinder.Eval(e.Row.DataItem, "BillingID").ToString();
                    }

                    break;
            }


            (e.Row.FindControl("imbAddendum") as ImageButton).ImageUrl = "~/Includes/Images/png/addenda.png";
        }

        //protected void gdvBillings_DataBound(object sender, EventArgs e)
        //{
        //    GridViewRow pager = this.gdvBillings.TopPagerRow;
        //    PlaceHolder panel;

        //    if (pager.IsNull() || !(pager.Visible = !(this.gdvBillings.PageCount < 2)))
        //        return;

        //    panel = (PlaceHolder)pager.FindControl("plhPanel");

        //    if (panel.IsNull())
        //        return;

        //    for (int i = 0; i < this.gdvBillings.PageCount; ++i)
        //    {
        //        LinkButton l = new LinkButton()
        //        {
        //            Text = i.ToString(),
        //            CommandName = "Pager",
        //            CommandArgument = i.ToString(),
        //            ID = string.Format("{0}_Pager_{1}", this.gdvBillings.ClientID, i)
        //        };
        //        panel.Controls.Add(l);
        //    }
        //}
    }
}