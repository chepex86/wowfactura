﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billings
{
    public delegate void DataBindFromBillingIDEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// Event fired when a client request to be modify.
    /// </summary>
    /// <param name="sender">Object sender of the event.</param>
    /// <param name="e">event arguments send by the object.</param>
    public delegate void ClientModifyEventHandler(object sender, Billings.BillingsClientEditEventArgs e);
    /// <summary>
    /// Event fired when a concept is added to the list of the bill.
    /// </summary>
    /// <param name="sender">Object sender of the event.</param>
    /// <param name="e">event arguments send by the object.</param>
    public delegate void ConceptAfterAddToBillEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// Event fired when a concept is added to the list of the bill.
    /// </summary>
    /// <param name="sender">Object sender of the event.</param>
    /// <param name="e">event arguments send by the object.</param>
    public delegate void BillingfterAddToBillEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// Event handler of the ApplyChanges event.
    /// </summary>
    /// <param name="sender">Object sender of the event.</param>
    /// <param name="e">event arguments send by the object.</param>
    public delegate void GeneratedBillingEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ArticleBeforeAddEventHandler(object sender, BillingsEditorArticleAddEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ArticleAfterAddEventHandler(object sender, BillingsEditorArticleAddEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void BillingBeforeAddEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void BillingAfterAddEventHandler(object sender, BillingsEditorEventArgs e);
    /// <summary>
    /// Event handler of Billing search event method.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the sender.</param>
    public delegate void SearchEventHandler(object sender, BillingsSearchEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ArticlesSelectedEventHandler(object sender, BillingsArticleSearchSelectedEventArgs e);
    /// <summary>
    /// Event handler of BillingArticleSearch event method.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SearchBillingArticleEventHandler(object sender, BillingsArticleSearchEventArgs e);

    public delegate void BillingTaxTypeSelectorSelectedEventHandler(object sender, BillingTaxTypeSelectorEventArgs e);

    public delegate void BillingsPaymentReceptorSelectedEventHandler(object sender, BillingsPaymentReceptorSelectorEventArgs e);

    public delegate void BillingsPaymentBillingSelectedEventHandler(object sender, BillingsPaymentBillingSelectorEventArgs e);
}
