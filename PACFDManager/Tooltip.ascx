﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tooltip.ascx.cs" Inherits="PACFDManager.Tooltip" %>
<span style="cursor: help">
    <asp:Image ID="imgToolTip" runat="server" class="vtip" ImageUrl="~/Includes/Images/png/questionIcon.png" />
</span>