﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="ConceptsDetails.aspx.cs" Inherits="PACFDManager.Concepts.ConceptsDelete"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="ConceptsEditor.ascx" TagName="ConceptsEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:ConceptsEditor ID="ucConceptsEditor" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
