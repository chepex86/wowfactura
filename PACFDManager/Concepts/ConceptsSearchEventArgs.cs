﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace PACFDManager.Concepts
{
    public class ConceptsSearchEventArgs : EventArgs 
    {
        public string Value { get; private set; }
        public ConceptsSearchMode SearchMode { get; private set; }
        public string UnitType { get; private set; }
        public string Description { get; private set; }
        public string Code { get; private set; }

        public ConceptsSearchEventArgs(string value, ConceptsSearchMode editmode)
        {
            this.SearchMode = editmode;
            this.Value = value;
        }

        public ConceptsSearchEventArgs(string utype, string descript, string code)
        {
            this.UnitType = utype;
            this.Description = descript;
            this.Code = code;
        }
    }
}