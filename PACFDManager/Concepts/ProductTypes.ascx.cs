﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ProductTypes : System.Web.UI.UserControl
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Set or Get UMC value.
        /// </summary>
        public string UMC
        {
            get { return this.ddlProductTypes.SelectedValue; }
            set 
            {
                this.ddlProductTypes.SelectedIndex = this.ddlProductTypes.Items.IndexOf(this.ddlProductTypes.Items.FindByText(value));
            }
        }
        /// <summary>
        /// Set or Get selected index value.
        /// </summary>
        public int SelectedIndex
        {
            get { return this.ddlProductTypes.SelectedIndex; }
            set { this.ddlProductTypes.SelectedIndex = value; }
        }
        /// <summary>
        /// Gets or sets the enabled property value.
        /// </summary>
        public bool Enabled
        {
            get { return this.ddlProductTypes.Enabled; }
            set { this.ddlProductTypes.Enabled = value; }
        }
        /// <summary>
        /// Sets the behavior of control.
        /// </summary>
        public bool AddMode
        {
            set 
            {
                if (value)
                    this.ddlProductTypes.Items.Remove(this.ddlProductTypes.Items.FindByText("[Todos]"));
            }
        }
        /// <summary>
        /// Gets the unique identifier of the control
        /// </summary>
        public String ProductClientID
        {
            get { return this.ddlProductTypes.ClientID; }
        }
    }
}