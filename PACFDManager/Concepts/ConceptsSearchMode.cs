﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Concepts
{
    public enum ConceptsSearchMode
    {
        Code = 0,
        Type = 1,
        Description = 2,
        All = 3,
    }
}
