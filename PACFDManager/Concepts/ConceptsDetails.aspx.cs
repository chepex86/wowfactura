﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsDelete : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");

            this.Title = "Detalle del Producto";
            this.CustomizeEditor();
        }
        /// <summary>
        /// Load concept information by ConceptID.
        /// </summary>
        /// <param name="ConceptID">int ConceptID</param>
        private void LoadData(int ConceptID)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.ConceptsDataTable t;
            t = concept.SelectByID(ConceptID);
            this.ucConceptsEditor.ModifyLoadData(t[0].Code, 
                (t[0].UnitType.Trim() == String.Empty ? "[Sin Asignar]" : t[0].UnitType), t[0].Description, 
                t[0].UnitPrice.ToString("0,0.00"), t[0].TaxRatePercentage.ToString("0,0.00"), t[0].AppliesTax, t[0].Account, t[0].IsClavProdServNull() ? "" : t[0].ClavProdServ);
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            if (Request.QueryString["ConceptID"].IsNull())
                this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");
            this.LoadData(Convert.ToInt32(Request.QueryString["ConceptID"]));
            this.ucConceptsEditor.EditorMode = ConceptsEditorMode.Details;
            this.ucConceptsEditor.DetailMode = true;
        }
    }
}