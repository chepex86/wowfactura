﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="ConceptsImport.aspx.cs"
    Inherits="PACFDManager.Concepts.ConceptsImport" %>

<%@ Register Src="ProductTypes.ascx" TagName="ProductTypes" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Importar Productos" />
                            </h2>
                        </div>
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblImportTitle" runat="server" Text="Seleccionar la ruta del archivo:"></asp:Label>
                                </label>
                                <span class="vtip" title="Selecciona la ruta del archivo [<b>*.txt</b>]<br />que se va a importar al sistema.">
                                    <asp:FileUpload ID="fuConceptsImport" runat="server" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvFUImport" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="fuConceptsImport" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li class="buttons clear">
                                <div>
                                    <asp:Button ID="btnLoad" runat="server" CssClass="Button" OnClick="btnLoad_Click"
                                        Text="Cargar Archivo" ValidationGroup="Validators" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="txt_left" id="dvList" runat="server" visible="false">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleList" runat="server" Text="Listado de Productos a Importar" />
                    </h2>
                </div>
                <span>
                    <asp:GridView ID="gvConceptsTemplate" runat="server" AutoGenerateColumns="False"
                        OnRowDataBound="gvConceptsTemplate_RowDataBound" Visible="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Clave">
                                <ItemTemplate>
                                    <span id="spnCode1" runat="server" visible="false" style="cursor: help;" class="vtip"
                                        title='<%#"El producto <b>" + Eval("Code") + "</b> tiene conflictos<br />con otro producto que ya existe en el sistema,<br />selecciona una de las 3 opciones para resolver<br />el conflicto."%>'>
                                        <asp:Label ID="lblCode" runat="server" Text='<%#Eval("Code")%>'></asp:Label>
                                    </span><span id="spnCode2" visible="true" runat="server">
                                        <asp:Label ID="lblCode2" runat="server" Text='<%#Eval("Code")%>'></asp:Label>
                                    </span>
                                    <asp:HiddenField ID="hdState" runat="server" Value='<%#Eval("CodeExists")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo">
                                <ItemTemplate>
                                    <span class="vtip" title="No se encontro el tipo de producto asignado.<br />Selecciona un tipo de producto de la lista.">
                                        <uc1:ProductTypes ID="ProductTypes1" runat="server" Visible="false" />
                                    </span>
                                    <asp:Label ID="lblType" runat="server" Text='<%#Eval("Type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Descripción" DataField="Description" />
                            <asp:BoundField HeaderText="Precio Unitario" DataField="UnitPrice" DataFormatString="{0:#,#.00 '%'}"
                                ItemStyle-HorizontalAlign="Right" HtmlEncode="False" />
                            <asp:BoundField HeaderText="Aplica Impuestos" DataField="ApplyTax" />
                            <asp:BoundField HeaderText="Cuenta Contable" DataField="Account" />
                            <asp:TemplateField HeaderText="Estado">
                                <HeaderTemplate>
                                    <span id="spnState1" runat="server" visible="true" class="vtip" title="Selecciona una de las 3 acciones a<br />realizar con un producto en conflicto:<br/><br /><b>- Ignorar.</b> El producto sera descartado<br />y no se agregara al sistema.<br /><b>- Editar.</b> El producto sera agregado al<br />sistema con una clave nueva generada<br />automaticamente, la cual posteriormente<br />podra cambiarse.<br /><b>- Sobreescribir.</b> El producto sera agregado<br />al sistema, reemplazando al producto<br />ya existente.">
                                        <uc2:Tooltip ID="ttState1" runat="server" />
                                    </span><span id="spnState2" runat="server" visible="false" class="vtip" title="La imagen <img src='../Includes/Images/png/apply.png' alt='Agregado' /> significa que el producto<br />fue importado correctamente al sistema.<br /><br />La imagen <img src='../Includes/Images/png/deleteIcon.png' alt='No agregado' /> significa que el producto<br />no pudo ser importado al sistema.">
                                        <uc2:Tooltip ID="ttState2" runat="server" />
                                    </span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:DropDownList ID="ddlState" runat="server">
                                            <asp:ListItem>Ignorar</asp:ListItem>
                                            <asp:ListItem>Editar</asp:ListItem>
                                            <asp:ListItem>Sobreescribir</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdCode" runat="server" Value='<%#Eval("Code")%>' />
                                        <asp:Image ID="imgState" Visible="false" runat="server" ImageUrl="~/Includes/Images/png/apply.png" />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div>
                        <asp:Label ID="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                </span>
                <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="Button" OnClick="btnSave_Click"
                    Visible="False" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button" Visible="false"
                    OnClick="btnCancel_Click" />
                <asp:Button ID="btnAccept" runat="server" CausesValidation="False" CssClass="Button"
                    PostBackUrl="~/Concepts/ConceptsList.aspx" Text="Aceptar" Visible="False" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoad" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
