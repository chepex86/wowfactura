﻿<%@ Page Title="Agregar Producto" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ConceptsAdd.aspx.cs" Inherits="PACFDManager.Concepts.ConceptsAdd" %>

<%@ Register Src="ConceptsEditor.ascx" TagName="ConceptsEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">

    <script type="text/javascript">

        Number.prototype.decimal = function(n) {
            pot = Math.pow(10, parseInt(n));
            return parseInt(this * pot) / pot;
        }

        function CalculateTax(total, unitPrice, tax, chkBoxTax, RegisterScript) {

            total = document.getElementById(total);
            unitPrice = document.getElementById(unitPrice);
            tax = document.getElementById(tax);
            chkBoxTax = document.getElementById(chkBoxTax);


            var sum;
            var result = 0;
            var aux;

            if (total == null || unitPrice == null || tax == null || chkBoxTax == null) {
                // alert("Uno o más valores son nulos.");
                return;
            }

            result = chkBoxTax.checked ? unitPrice.value * tax.value / 100 : unitPrice.value;

            if (chkBoxTax.checked == false) {
                total.value = result;
            }
            else {
                sum = parseFloat(unitPrice.value) + parseFloat(result);
                total.value = sum.decimal(2);
            }
            if (total.value == 'NaN') {
                total.value = '';
            }

        }

        function OnlyNumbers() {
            if (event != null) {
                if (event.keyCode < 45 || event.keyCode > 57) {
                    event.returnValue = false;
                }
                else {
                    return true;
                }
            }
            else {
                alert("null");
            }
        }

        function Count(description, label) {
            cant = document.getElementById(description).value.length;

            if ((1000 - cant) > 0) {
                document.getElementById(label).innerHTML = 1000 - cant;

            } else {
                document.getElementById(label).innerHTML = 1000 - cant;
                var key = event.keyCode;

                if (key == 8 || key == 46 || key == 37 || key == 39) {
                } else event.returnValue = false;
            }
        }

        function Load(description, label) {
            var cant = document.getElementById(description).value.length;
            document.getElementById(label).innerHTML = 1000 - cant;
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:ConceptsEditor ID="ucConceptsEditor" runat="server" OnCancelClick="ucConceptsEditor_CancelClick"
                OnApplyChanges="ucConceptsEditor_ApplyChanges" />
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick"
                OnCancelClick="WebMessageBox1_OnCancelClick" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
