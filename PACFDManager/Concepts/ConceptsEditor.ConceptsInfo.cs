﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Concepts
{
    partial class ConceptsEditor
    {
        /// <summary>
        /// Class used to group all main information about ConceptsEditor class.
        /// </summary>
        public class ConceptsEditorInfo
        {
            /// <summary>
            /// Get the ConceptsEditor parent class of the object.
            /// </summary>
            public ConceptsEditor Parent { get; private set; }
            ///<summary>
            ///
            /// </summary>
            public string Title
            {
                get { return this.Parent.lblTitleOptions.Text; }
                set { this.Parent.lblTitleOptions.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Code
            {
                get { return this.Parent.txtCode.Text; }
                set { this.Parent.txtCode.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Description
            {
                get { return this.Parent.txtDescription.Text; }
                set { this.Parent.txtDescription.Text = value; }
            }
            ///<summary>
            ///
            /// </summary>
            public string UnitPrice
            {
                get { return this.Parent.txtUnitPrice.Text; }
                set { this.Parent.txtUnitPrice.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Tax
            {
                get { return this.Parent.txtTax.Text; }
                set { this.Parent.txtTax.Text = value; }
            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">ConceptsEditor parent of the class.</param>
            public ConceptsEditorInfo(ConceptsEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
