﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Concepts
{
    partial class ConceptsSearch
    {
        /// <summary>
        /// Class used to group all main information about ConceptsSearch class.
        /// </summary>
        public class ConceptsSearchInfo
        {
            /// <summary>
            /// Get the ConceptsSearch parent class of the object.
            /// </summary>
            public ConceptsSearch Parent { get; private set; }
            ///<summary>
            ///
            /// </summary>
            public string Code
            {
                get { return this.Parent.txtSearchByCode.Text; }
                set { this.Parent.txtSearchByCode.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public bool Visible
            {
                set
                {
                    this.Parent.lblTitle.Visible = 
                    //this.Parent.lblSearchByType.Visible = 
                    //this.Parent.lblSearchByCode.Visible = 
                    this.Parent.ddlSearchByType.Visible = 
                    this.Parent.txtSearchByCode.Visible = value;
                }

            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">ConceptsSearch parent of the class.</param>
            public ConceptsSearchInfo(ConceptsSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
