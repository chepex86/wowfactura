﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsAdd : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = "Agregar Producto";
            this.CustomizeEditor();
            this.LoadTaxRatePercentage();
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucConceptsEditor_ApplyChanges(object sender, ConceptsEventArgs e)
        {
            if (e.EditorMode != ConceptsEditorMode.Add) return;
            if (this.Add(e))
            {
                this.WebMessageBox1.Title = "Agregar Producto";
                this.WebMessageBox1.ShowMessage("El Producto ha sido agregado exitosamente,<br />¿Deseas agregar otro?",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsAdd.WebMessageBox.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if(e.DialogResult == WebMessageBoxDialogResultType.Yes)
                this.Response.Redirect(typeof(ConceptsAdd).Name + ".aspx");
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsAdd.WebMessageBox.OnCancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnCancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucConceptsEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");
        }
        /// <summary>
        /// Checks if there is a tax already imposed by default.
        /// </summary>
        [Obsolete("This function is obsolete")]
        private void LoadTaxRatePercentage()
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable ta;
            ta = concept.GetTaxRatePercentageByBillerID(this.CurrentBillerID);
            //if (ta.Count < 1)
            //    this.ucConceptsEditor.LoadTaxRatePercentage = String.Empty;
            //else
            //{
            //    this.ucConceptsEditor.LoadTaxRatePercentage = (!ta[0].TaxRatePercentage.IsNull() ?
            //        ta[0].TaxRatePercentage.ToString() : String.Empty);
            //}
        }
        /// <summary>
        /// Inserts a new row with the concept information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Add(ConceptsEventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.ConceptsRow drConcept = ds.Concepts.NewConceptsRow();

            drConcept.Code = e.Code;
            drConcept.BillerID = e.BillerID;
            drConcept.UnitType = e.Type;
            drConcept.Description = e.Description;
            drConcept.UnitPrice = Convert.ToDecimal(e.UnitPrice);
            drConcept.TaxRatePercentage = Convert.ToDecimal(e.Tax);
            drConcept.Active = true;
            drConcept.TaxType = 0;
            drConcept.AppliesTax = e.AppliesTax;
            drConcept.Account = e.Account;
            drConcept.ClavProdServ = e.Prod;
            ds.Concepts.AddConceptsRow(drConcept);

            if (!concept.Update(ds.Concepts))
                return false;

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo concepto al sistema.",
                this.UserName), this.CurrentBillerID, null, ds.GetXml());
            #endregion
            
            this.ucConceptsEditor.CleaningForm = true;

            LogManager.WriteStackTrace(new Exception("Éxito al Agregar Nuevo Producto."));
            return true;
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucConceptsEditor.EditorMode = ConceptsEditorMode.Add;
            this.ucConceptsEditor.AddMode = true;
            this.ucConceptsEditor.RegisterScripts(true);
        }
    }
} 