﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the Search Event Handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void SearchEventHandler(object sender, ConceptsSearchEventArgs e);
        /// <summary>
        /// Event handler for the concepts Search.
        /// </summary>
        public event SearchEventHandler Search;
        /// <summary>
        /// Get or Set a ConceptsSearchMode value with the control mode.
        /// </summary>
        public ConceptsSearchMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return ConceptsSearchMode.Type;

                return o.GetType() == typeof(ConceptsSearchMode) ? (ConceptsSearchMode)o : ConceptsSearchMode.Type;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.ShowProgressBar))
                this.RegisterScriptsForProgressBar();
        }
        public String ShowProgressBar
        {
            get
            {
                if (!this.ViewState["Show_Progress"].IsNull())
                    return this.ViewState["Show_Progress"].ToString();
                else
                    return null;
            }
            set { this.ViewState["Show_Progress"] = value; }
        }
        private void RegisterScriptsForProgressBar()
        {
            if (this.Page.ClientScript.IsStartupScriptRegistered(String.Format("LoadBindControls_{0}", this.Page.ClientID)))
                return;

            String scriptToRegister = String.Empty;
            String uProgressByCode = String.Empty;
            String uProgressByDesc = String.Empty;

            uProgressByCode = String.Format("jsConcept.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtSearchByCode_AutoCompleteExtender.BehaviorID, this.spnByCode.ClientID, true);

            uProgressByDesc = String.Format("jsConcept.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtSearchByDescription_AutoCompleteExtender.BehaviorID, this.spnByDesc.ClientID, true);

            scriptToRegister = "Sys.Application.add_load(function() {";
            scriptToRegister += String.Format("{2}{0}{2}{1}{2}", uProgressByCode, uProgressByDesc, System.Environment.NewLine);
            scriptToRegister += "});";

            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), String.Format("LoadBindControls_{0}", this.Page.ClientID), scriptToRegister, true);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsSearch.OnSearch event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(ConceptsSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsSearch.Search_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ConceptsSearchEventArgs a;
            String c = String.Empty;

            a = new ConceptsSearchEventArgs((this.ddlSearchByType.UMC != "-1" ? this.ddlSearchByType.UMC : c),
                                            (this.txtSearchByDescription.Text.Trim() != c ? this.txtSearchByDescription.Text.Trim() : c),
                                            (this.txtSearchByCode.Text.Trim() != c ? this.txtSearchByCode.Text.Trim() : c));
            this.OnSearch(a);
        }
    }
}