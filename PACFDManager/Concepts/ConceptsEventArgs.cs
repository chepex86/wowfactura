﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Concepts
{
    public class ConceptsEventArgs : EventArgs
    {
        public ConceptsEditorMode EditorMode { get; private set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string UnitPrice { get; set; }
        public string Tax { get; set; }
        public int BillerID { get; set; }
        public bool AppliesTax { get; set; }
        public string Account { get; set; }
        public string Prod { get; set; }
        public ConceptsEventArgs(int billerid, string code, string type, string description, string uprice, string tax, bool appliestax, string account, string prod, ConceptsEditorMode editorMode)
        {
            this.BillerID = billerid;
            this.Code = code;
            this.Type = type;
            this.Description = description;
            this.UnitPrice = uprice;
            this.Tax = tax;
            this.AppliesTax = appliestax;
            this.Account = account;
            this.EditorMode = editorMode;
            this.Prod = prod;
        }
    }
}
