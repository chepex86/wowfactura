﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Text;
using System.Data;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsImport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblError.Visible = false;
        }

        private bool LoadData()
        {
            Byte[] stream;
            String file = String.Empty;
            
            try
            {
                if (!this.fuConceptsImport.HasFile)
                    return false;

                stream = this.fuConceptsImport.FileBytes;

                file = Encoding.UTF8.GetString(stream);

                this.ReadFile(file);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        private void ReadFile(String file)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            DataTable table = new DataTable();

            if (file == String.Empty)
            {
                this.lblError.Text = "El archivo [" + this.fuConceptsImport.FileName + "] esta vacio.";
                this.dvList.Visible =
                this.lblError.Visible = true;
                return;
            }

            String err = concept.ReadFile(ref table, this.CurrentBillerID, file);
            if ((TypesErrorsImport)Convert.ToInt32(err.Split('_')[0]) != TypesErrorsImport.Success)
            {
                this.ErrorMessage(err);
                return;
            }

            if (table.Rows.Count < 1)
            {
                this.dvList.Visible =
                this.lblError.Visible = true;
                this.lblError.Text = "No se encontro ningun producto para importar.";
                return;
            }

            this.dvList.Visible = 
            this.btnSave.Visible =
            this.btnCancel.Visible =
            this.gvConceptsTemplate.Visible = true;
            this.btnAccept.Visible = false;
            this.lblTitleList.Text = "Listado de Productos a Importar";

            this.ViewState["ResultMode"] = null;

            String UMC = "[Sin Asignar]_Barril_Botella_Cabeza_Caja_Cientos_Decenas_Docenas_Gramo_" +
                         "Gramo Neto_Juego_Kilo_Kilowatt_Kilowatt/Hora_Litro_Metro Cuadrado_Metro Cubico_" + 
                         "Metro Lineal_Millar_Paquete_Par_Pieza_Servicio_Tonelada";
            bool flag = false;
            int cont = 0;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < 23; j += 1)
                {
                    if (table.Rows[i]["Type"].ToString().ToLower().Trim() == UMC.Split('_')[j].ToLower().Trim())
                    {
                        flag = true;
                        break;
                    }
                    cont += 1;
                }

                if (flag)
                    table.Rows[i]["Type"] = UMC.Split('_')[cont];
                else
                    table.Rows[i]["Type"] = UMC.Split('_')[0];

                flag = false;
                cont = 0;
            }

            this.gvConceptsTemplate.DataSource = table;
            this.gvConceptsTemplate.DataBind();     
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (!this.LoadData())
                return;
        }

        protected void gvConceptsTemplate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowIndex > -1)
            {
                DropDownList ddl = row.FindControl("ddlState") as DropDownList;
                HiddenField hd = row.FindControl("hdState") as HiddenField;
                Image img = row.FindControl("imgState") as Image;

                row.Cells[4].Text = row.Cells[4].Text == "1" ? "Si" : "No";

                if (!this.ViewState["ResultMode"].IsNull())
                {
                    img.ImageUrl = Convert.ToBoolean(hd.Value) ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/deleteIcon.png";
                    img.Visible = true;
                    ddl.Visible = false;
                }
                else
                {                   
                    Label lblType = row.FindControl("lblType") as Label;

                    if (lblType.Text == "[Sin Asignar]")
                    {
                        ProductTypes type = row.FindControl("ProductTypes1") as ProductTypes;
                        type.AddMode = true;
                        type.Visible = true;
                        lblType.Visible = false;
                    }

                    if (hd.Value == String.Empty)
                        return;

                    if (Convert.ToBoolean(hd.Value))
                    {
                        row.FindControl("spnCode1").Visible = true;
                        row.FindControl("spnCode2").Visible = false;

                        row.Cells[0].ForeColor = System.Drawing.Color.Crimson;
                    }
                    else
                    {
                        ddl.Visible = false;
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            DataTable table = new DataTable();
            DataRow row;

            Label lbl;
            HiddenField hdf;
            DropDownList ddl;
            ProductTypes pType;
            Label lblType;
           
            table.Columns.Add("Code");
            table.Columns.Add("Type");
            table.Columns.Add("Description");
            table.Columns.Add("UnitPrice");
            table.Columns.Add("ApplyTax");
            table.Columns.Add("Account");
            table.Columns.Add("CodeExists");
            table.Columns.Add("State");

            for (int i = 0; i < this.gvConceptsTemplate.Rows.Count; i += 1)
            {
                ddl = this.gvConceptsTemplate.Rows[i].FindControl("ddlState") as DropDownList;
                lbl = this.gvConceptsTemplate.Rows[i].FindControl("lblCode") as Label;
                hdf = this.gvConceptsTemplate.Rows[i].FindControl("hdState") as HiddenField;
                pType = this.gvConceptsTemplate.Rows[i].FindControl("ProductTypes1") as ProductTypes;
                lblType = this.gvConceptsTemplate.Rows[i].FindControl("lblType") as Label;

                row = table.NewRow();

                row["State"] = Convert.ToBoolean(hdf.Value) ? ddl.SelectedIndex + 1 : 0;
                row["Code"] = lbl.Text;
                row["Type"] = pType.Visible ? pType.UMC : lblType.Text;           
                row["Description"] = this.gvConceptsTemplate.Rows[i].Cells[2].Text.Trim().Replace("&nbsp;", String.Empty);
                row["UnitPrice"] = this.gvConceptsTemplate.Rows[i].Cells[3].Text.Trim().Replace("&nbsp;", String.Empty);
                row["ApplyTax"] = this.gvConceptsTemplate.Rows[i].Cells[4].Text.Trim().Replace("&nbsp;", String.Empty) == "Si" ? 1 : 0;
                row["Account"] = this.gvConceptsTemplate.Rows[i].Cells[5].Text.Trim().Replace("&nbsp;", String.Empty);

                table.Rows.Add(row);              
            }

            table = concept.Import(this.CurrentBillerID, table);

            if (!table.IsNull())
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(table);

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} importo uno o más conceptos al sistema.",
                    this.UserName), this.CurrentBillerID, null, ds.GetXml());
                #endregion

                this.ViewState["ResultMode"] = true;
                this.gvConceptsTemplate.DataSource = table;
                this.gvConceptsTemplate.DataBind();
                this.lblTitleList.Text = "Reporte de Resultados";
            }
            else
            {

            }

            this.gvConceptsTemplate.HeaderRow.Cells[6].FindControl("spnState1").Visible = false;
            this.gvConceptsTemplate.HeaderRow.Cells[6].FindControl("spnState2").Visible = true;
            this.btnCancel.Visible =
            this.btnSave.Visible = false;
            this.btnAccept.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.dvList.Visible =
            this.gvConceptsTemplate.Visible =
            this.btnSave.Visible =
            this.btnCancel.Visible = false;
        }

        public bool ToolTipMode()
        {
            return this.ViewState["ResultMode"].IsNull() ? true : false;
        }

        #region error message
        private void ErrorMessage(String error)
        {
            String Message = String.Empty;
            String error1, error2, x, y, c;
            int pos = 0;

            switch ((TypesErrorsImport)Convert.ToInt32(error.Split('_')[0]))
            {
                case TypesErrorsImport.FatalError:
                    Message = "Ocurrio un problema mientras se leia el archivo.<br />Revise si el archivo tiene el formato correcto o si esta dañado.";
                    break;
                case TypesErrorsImport.Twice:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    c = error.Split('_')[3].Split('|')[1];

                    error1 = error.Split('_')[3];
                    error1 = error1.Replace(c, "<b style='color:red'>" + c + "</b>");
                    error2 = error.Split('_')[4];
                    error2 = error2.Replace(c, "<b style='color:red'>" + c + "</b>");

                    Message = "Se detecto duplicado de registros en la fila [" + x + "] y en la fila [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span><br />" + "[" + y + "]---> <span style='color:#305389'>" + error2 + "</span>";
                    break;
                case TypesErrorsImport.MissingHeader:
                    Message = "Ausencia de la cabecera [P] en la fila [" + error.Split('_')[1] + "].<br /><br />" +
                              "[" + error.Split('_')[1] + "]---><span style='color:#305389'>" + error.Split('_')[3] + "</span>";
                    break;
                case TypesErrorsImport.MaxLength:
                    Message = "El valor supero la longitud maxima permitida: fila [" + error.Split('_')[1] +
                              "], columna [" + error.Split('_')[2] + "].";
                    break;
                case TypesErrorsImport.StringEmpty:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    pos = Convert.ToInt32(error.Split('_')[4]);
                    c = error.Split('_')[3].Split('|')[pos];

                    error1 = error.Split('_')[3];

                    Message = "El valor no puede estar vacio: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span>";
                    break;
                case TypesErrorsImport.ErrorConversion:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    c = error.Split('_')[3].Split('|')[4];

                    error1 = error.Split('_')[3];
                    error1 = error1.Replace(c, "<b style='color:red'>" + c + "</b>");

                    Message = "Error de conversion a tipo de dato moneda, el valor contiene caracteres no validos: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span>";
                    break;
                case TypesErrorsImport.ReadingError:
                    x = error.Split('_')[1];
                    y = error.Split('_')[2];
                    c = error.Split('_')[3].Split('|')[5];

                    error1 = error.Split('_')[3];
                    error1 = error1.Replace(c, "<b style='color:red'>" + c + "</b>");

                    Message = "Error de lectura, el valor contiene caracteres no validos: fila [" + x + "], columna [" + y + "].<br /><br />" +
                              "[" + x + "]---> <span style='color:#305389'>" + error1 + "</span>";
                    break;
                default:
                    break;
            }

            this.dvList.Visible =
            this.lblError.Visible = true;
            this.gvConceptsTemplate.Visible = false;
            this.lblError.Text = Message;
        }
        #endregion

        #region ....
        /*
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            GridViewRow row = ddl.NamingContainer as GridViewRow;

            Label lbl = row.FindControl("lblCode") as Label;

            if (ddl.SelectedValue == "Editar")
            {
                Panel pnl = row.FindControl("pnlCode") as Panel;
                TextBox txt = row.FindControl("txtCode") as TextBox;

                if (!pnl.IsNull() && !lbl.IsNull() && !txt.IsNull())
                {
                    txt.Text = lbl.Text;
                    pnl.Visible = true;
                    lbl.Visible = false;
                }
            }
            else if (ddl.SelectedValue == "Sobreescribir")
            {
                DataTable table = (DataTable)HttpContext.Current.Session["ConceptsImport"];
                if (table.IsNull() || table.Rows.Count < 1)
                    return; //Desplegar mensaje de error o alerta

                DataRow[] roew;
                roew = table.Select(String.Format("{0} Like '%{1}%'", "Code", lbl.Text));

                String c = roew[0].ItemArray[7].ToString();

                int cont = 0;

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (table.Rows[i] == roew[0])
                        break;

                    cont += 1;
                }

                table.Rows[cont]["State"] = "3"; //Sobreescribir registro actual
                table.AcceptChanges();

                this.gvConceptsTemplate.DataSource = table;
                this.gvConceptsTemplate.DataBind();
            }
        }

        protected void btnCancelCode_OnClick(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = btn.NamingContainer as GridViewRow;

            Panel pnl = row.FindControl("pnlCode") as Panel;
            Label lbl = row.FindControl("lblCode") as Label;
            DropDownList ddl = row.FindControl("ddlState") as DropDownList;

            if (!pnl.IsNull() && !lbl.IsNull() && !ddl.IsNull())
            {
                ddl.SelectedIndex = 0;
                pnl.Visible = false;
                lbl.Visible = true;
            }
        }

        protected void btnModifyCode_OnClick(object sender, EventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            Button b = sender as Button;
            GridViewRow row = b.NamingContainer as GridViewRow;
            Label lbl = row.FindControl("lblCode") as Label;
            Panel pnl = row.FindControl("pnlCode") as Panel;
            TextBox txt = row.FindControl("txtCode") as TextBox;

            DataTable table = (DataTable)HttpContext.Current.Session["ConceptsImport"];
            if (table.IsNull() || table.Rows.Count < 1)
                return; //Desplegar mensaje de error o alerta

            DataRow[] roew;
            roew = table.Select(String.Format("{0} Like '%{1}%'", "Code", lbl.Text));

            String c = roew[0].ItemArray[7].ToString();

            int cont = 0;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i] == roew[0])
                    break;

                cont += 1;
            }

            pnl.Visible = false;
            lbl.Visible = true;

            if (concept.SearchByCode(this.CurrentBillerID, (txt.Text.Trim() != String.Empty ? txt.Text.Trim() : lbl.Text)))
                return;

            table.Rows[cont]["Code"] = txt.Text.Trim();
            table.Rows[cont]["State"] = "2"; //Modificado
            table.Rows[cont]["CodeExists"] = false;
            table.AcceptChanges();

            this.gvConceptsTemplate.DataSource = table;
            this.gvConceptsTemplate.DataBind();
        }*/
        #endregion
    }
}