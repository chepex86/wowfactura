﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Concepts
{
    public enum ConceptsEditorMode
    {
        Add = 0,
        Details = 1,
        Modify = 2,
    }
}
