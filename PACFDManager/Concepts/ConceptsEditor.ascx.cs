﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using PACFD.Rules;
using System.Data;
using PACFDManager.Billings;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the control Apply Changes.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void ApplyChangesEventHandler(object sender, ConceptsEventArgs e);
        /// <summary>
        /// Event handler for the control Apply Changes.
        /// </summary>
        public event ApplyChangesEventHandler ApplyChanges;
        /// <summary>
        /// Event handler for the control Cancel Changes.
        /// </summary>
        public event EventHandler CancelClick;

        /// <summary>
        /// Set or Get UMC value.
        /// </summary>
        public string UMC
        {
            get { return this.txtProducTypesSearch.Text; }
            set
            {
                this.txtProducTypesSearch.Text = Helpers.GetClaveUnidadName(value);
            }
        }

        public string PROD
        {
            get { return this.txtClaveSearch.Text; }
            set
            {
                this.txtClaveSearch.Text = Helpers.GetClaveProdServName(value);
            }
        }

        /// <summary>
        /// Get a ConceptsEditorInfo with all main information.
        /// </summary>
        public ConceptsEditorInfo ConceptsInformation { get; private set; }
        /// <summary>
        /// Get or Set a ConceptsEditorMode value with the control mode.
        /// </summary>
        public ConceptsEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return ConceptsEditorMode.Add;

                return o.GetType() == typeof(ConceptsEditorMode) ? (ConceptsEditorMode)o : ConceptsEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Sets value the enabled property of all validators of the Editor.
        /// </summary>
        protected bool EnableAllValidators
        {
            set
            {
                this.revTax.Enabled =
                this.revUnitPrice.Enabled =
                this.rfvCode.Enabled =
                this.rfvDescription.Enabled =
                this.rfvTax.Enabled =
                this.rfvUnitPrice.Enabled = value;
            }
        }
        /// <summary>
        /// Sets value the visible property of the options: Add, Details, Modify or Edit.
        /// </summary>
        protected bool VisibleOptions { set { this.pnlConceptsOptions.Visible = value; } }
        /// <summary>
        /// Sets the mode of how to behave the editor: Add.
        /// </summary>
        public bool AddMode
        {
            set
            {
                this.lblTitleOptions.Text = "Agregar Producto";
                this.btnAccept.Text = "Aceptar";
                this.VisibleOptions = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Edit.
        /// </summary>
        public bool ModifyMode
        {
            set
            {
                this.lblTitleOptions.Text = "Editar Producto";
                this.btnAccept.Text = "Aceptar";
                this.VisibleOptions = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Detail.
        /// </summary>
        public bool DetailMode
        {
            set
            {
                this.lblTitleOptions.Text = "Detalle del Producto";
                this.btnCancel.Text = "Aceptar";
                this.txtCode.ReadOnly =
                this.txtDescription.ReadOnly =
                this.txtUnitPrice.ReadOnly =
                this.txtTax.ReadOnly =
                this.txtTotal.ReadOnly =
                this.txtAccount.ReadOnly = true;
                this.tipTotal.Visible =
                this.tipTaxTemplates.Visible =
                    //this.tipTax.Visible =
                //this.ddlProductTypes.Enabled =
                this.btnAccept.Visible = false;

                this.VisibleOptions = value;
                //this.ddlProductTypes.Items.Remove(this.ddlProductTypes.Items.FindByText("[Todos]"));
                //this.chkBoxTax.Enabled =

                this.imgBox.Src = this.chkBoxTax.Checked ? "~/Includes/Images/png/tax_apply.png" : "~/Includes/Images/png/tax_no_apply.png";
                this.imgBox.Visible = true;
                this.chkBoxTax.Visible = false;
                this.hfReadOnly.Value = "1";
            }
        }
        /// <summary>
        /// Hide or show template tax controls. 
        /// </summary>
        private bool TaxApplies
        {
            set
            {
                this.chkBoxTax.Visible =
                this.ddlTaxTemplates.Visible =
                    //this.tipTax.Visible =
                this.txtTotal.Visible =
                this.tipTaxTemplates.Visible =
                this.lblTotal.Visible =
                this.tipTotal.Visible = value;
            }
        }
        /// <summary>
        /// Clears all text boxes and reset the dropdownlist
        /// </summary>
        public bool CleaningForm
        {
            set
            {
                if (value)
                {
                    this.txtCode.Text =
                    this.txtDescription.Text =
                    this.txtTotal.Text =
                    this.txtUnitPrice.Text =
                    this.txtTax.Text =
                    this.txtAccount.Text = String.Empty;
                    if (this.ddlTaxTemplates.SelectedIndex != -1)
                        this.ddlTaxTemplates.SelectedIndex = 0;
                    //this.ddlProductTypes.SelectedIndex = 0;
                }
            }
        }
        /// <summary>
        /// Set and Get the RegisterCount property
        /// </summary>
        private bool RegisterCount { get; set; }

        /// <summary>
        /// Load data for editor in edit mode.
        /// </summary>
        /// <param name="code">string code</param>
        /// <param name="type">string type</param>
        /// <param name="description">string description</param>
        /// <param name="uprice">string uprice</param>
        /// <param name="tax">string tax</param>
        public void ModifyLoadData(String code, String type, String description, String uprice, String tax, bool applietax, String account, string prod)
        {
            this.txtCode.Text = code;
            this.UMC = type;
            this.PROD = prod;
            this.txtDescription.Text = description;
            this.txtUnitPrice.Text = String.Format("{0:#,#.00}", Convert.ToDecimal(uprice));
            this.txtTax.Text = tax;
            this.hfTax.Value = tax;
            this.chkBoxTax.Checked = applietax;
            this.txtTotal.Text = (Convert.ToDecimal(uprice) + ((Convert.ToDecimal(uprice) * Convert.ToDecimal(tax)) / 100)).ToString("#,#.00");
            this.lblTotal.Text = "Precio con Impuesto (" + String.Format("{0:#,#.00 '%'}", Convert.ToDecimal(this.hfTax.Value)) + "):";
            this.txtAccount.Text = account;
        }
        /// <summary>
        /// Sets the registerscripts value
        /// </summary>
        /// <param name="value">bool value</param>
        public void RegisterScripts(bool value)
        {
            this.RegisterCount = value;
        }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public ConceptsEditor()
        {
            this.ConceptsInformation = new ConceptsEditorInfo(this);
        }
        /// <summary>
        /// Generate a new instance of the class of type ConceptsInfo to control the fields of the control.
        /// </summary>
        /// <returns>Returns a ConceptsInfo class.</returns>
        protected virtual ConceptsEditorInfo OngenerateConceptsInformation()
        {
            return new ConceptsEditorInfo(this);
        }

        public bool selected { get; set; }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.tipTax.Visible =
            this.ddlTaxTemplates.Visible =
            this.tipTaxTemplates.Visible =
            this.lblTotal.Visible =
            this.txtTotal.Visible =
            this.tipTotal.Visible = true;
            //this.chkBoxTax.Visible = false;

            if (this.EditorMode == ConceptsEditorMode.Details)
                return;

            if (!IsPostBack)
            {
                if (this.RegisterCount)
                {
                    this.RegisterScriptCount();
                    this.lblCount.Visible = true;
                    this.lblCountTitle.Visible = true;
                }
                if (!this.FillDropDownListTaxTemplates())
                {
                    this.TaxApplies = false;
                    this.RegisterScript(false);
                }
                else this.RegisterScript(true);

                if (this.CurrentElectronicBillingType == ElectronicBillingType.CFD2_2
                    || this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_2
                    || this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_3)
                {
                    //this.ddlProductTypes_RequiredFieldValidator.Visible = true;
                }
                else
                {
                    //this.ddlProductTypes_RequiredFieldValidator.Visible = false;
                }

                //this.ddlProductTypes.Items.Clear();
                //this.ddlProductTypes.DataTextField = "Nombre";
                //this.ddlProductTypes.DataValueField = "c_ClaveUnidad";
                //this.ddlProductTypes.Items.Insert(0,new ListItem("Dia", "DAY"));
                ////this.ddlProductTypes.DataSource = Helpers.GetClaveUnidad();
                //this.ddlProductTypes.DataBind();


                //this.ddlClavProdSer.Items.Clear();
                //this.ddlClavProdSer.DataTextField = "Descripcion";
                //this.ddlClavProdSer.DataValueField = "c_ClaveProdServ";
                ////this.ddlClavProdSer.DataSource = Helpers.GetClaveProdServ();
                //this.ddlClavProdSer.Items.Insert(0, new ListItem("[No existe en catalogo]", "01010101"));

            }
        }
        /// <summary>
        /// Add the script CalculateTax
        /// </summary>
        /// <param name="value">bool value</param>
        private void RegisterScript(bool value)
        {
            String javafun = String.Format("javascript:CalculateTax('{0}', '{1}', '{2}', '{3}', '{4}');",
                this.txtTotal.ClientID, this.txtUnitPrice.ClientID, this.hfTax.ClientID, this.chkBoxTax.ClientID, this.hfRegScript.ClientID);

            this.chkBoxTax.Attributes["onclick"] = javafun;
            this.txtUnitPrice.Attributes["onkeyup"] = javafun;
            this.txtTax.Attributes["onkeyup"] = javafun;
            this.hfRegScript.Value = value.ToString();
            this.txtAccount.Attributes["onkeypress"] = "javascript:OnlyNumbers();";
            this.txtUnitPrice.Attributes["onkeypress"] = "javascript:OnlyNumbers();";
        }
        /// <summary>
        /// Add the script Count
        /// </summary>
        private void RegisterScriptCount()
        {
            String javafun = String.Format("javascript:Count('{0}', '{1}');", this.txtDescription.ClientID, this.lblCount.ClientID);
            String javafunLoad = String.Format("Load('{0}', '{1}');", this.txtDescription.ClientID, this.lblCount.ClientID);

            this.txtDescription.Attributes["onkeypress"] = javafun;
            this.txtDescription.Attributes["onkeyup"] = javafun;
            this.txtDescription.Attributes["onkeydown"] = javafun;

            if (!Page.ClientScript.IsStartupScriptRegistered("load"))
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "load", javafunLoad, true);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.ApplyChanges event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnApplyChanges(ConceptsEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.CancelClik event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.btnAccept_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.OnApplyChanges(this.OnGenerateAcceptArguments());
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.btnCancel_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
        /// <summary>
        /// Generate a new instance of the class ConceptsEditorEventArgs.
        /// </summary>
        /// <returns>
        /// Returns a new instance of ConceptsEditorEventArgs, 
        /// the parameters entered into the editor of concepts.
        /// </returns>
        protected ConceptsEventArgs OnGenerateAcceptArguments()
        {
            return new ConceptsEventArgs(this.CurrentBillerID, this.txtCode.Text, Helpers.GetClaveUnidadCode(this.UMC), this.txtDescription.Text,
                      this.txtUnitPrice.Text, this.LoadTaxRate(), this.chkBoxTax.Checked, this.txtAccount.Text, Helpers.GetClaveProdServCode(this.PROD), this.EditorMode);
        }
        /// <summary>
        /// Checks if there is a tax already imposed by default.      
        /// </summary>
        /// <returns>Returns the value of tax.</returns>
        private string LoadTaxRate()
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.Concepts_GetTaxRatePercentageDataTable ta;
            ta = concept.GetTaxRatePercentageByBillerID(this.CurrentBillerID);
            if (ta.Count > 0)
                return ta[0].TaxRatePercentage.ToString();
            else
                return "0";
        }
        /// <summary>
        /// Fills the DropDownList with the related templates current biller.
        /// </summary>
        /// <returns>Returns true if at least one template there exists, otherwise returns false.</returns>
        protected bool FillDropDownListTaxTemplates()
        {
            PACFD.Rules.TaxTemplate template = new PACFD.Rules.TaxTemplate();
            TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable ta;

            ta = template.TaxTemplatesGetByBillerID(this.CurrentBillerID);

            if (ta.Count < 1)
                return false;

            ViewState["TaxTemplatesTable"] = ta;

            ListItem l;

            this.ddlTaxTemplates.Items.Clear();

            for (int i = 0; i < ta.Count; i += 1)
            {
                l = new ListItem();
                l.Text = ta[i].TaxTemplatesName;
                l.Value = ta[i].TaxTemplateTaxTypeID.ToString();
                this.ddlTaxTemplates.Items.Add(l);
            }

            ta.Dispose();

            return true;
        }
        /// <summary>
        /// Fires the event SelectedIndexChanged of dropdownlis control.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ddlTaxTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            //(TaxTemplatesDataSet.TaxTemplates_GetByBillerIDDataTable)ViewState["TaxTemplatesTable"];
            //LoadTaxRatePercentage =
        }
        /// <summary>
        /// Check the current status of editor.
        /// </summary>
        /// <returns>Returns a string true or false.</returns>
        public String ToolTipMode()
        {
            return this.EditorMode == ConceptsEditorMode.Details ? "True" : "False";
        }
    }
}