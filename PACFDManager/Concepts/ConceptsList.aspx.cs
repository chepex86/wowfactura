﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Data;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsList : PACFDManager.BasePage
    {
        public const string MESSAGE_DELETE = "delete";

        private int ConcepTempID
        {
            get
            {
                object o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull() || o.GetType() != typeof(int))
                    return 0;

                return (int)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ConceptsSearch1.ShowProgressBar = "#///#";

            if (IsPostBack)
                return;

            this.DataBind();
        }

        protected void ConceptsSearch1_Search(object sender, ConceptsSearchEventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();

            this.DataBind(concept.SelectConceptBySearching(this.CurrentBillerID, e.UnitType, e.Description, e.Code));
            //this.lblNumberProducts.Text = this.gdvConcepts.Rows.Count.ToString();

            if (this.gdvConcepts.Rows.Count < 1)
            {
                DataTable dt = new ConceptsDataSet.ConceptsDataTable();
                DataRow dr = dt.NewRow();
                dr["BillerID"] = -1;
                dr["UnitPrice"] = -1;
                dr["Description"] = String.Empty;
                dr["UnitType"] = -1;
                dr["Active"] = false;
                dr["Code"] = String.Empty;
                dr["TaxRatePercentage"] = -1;
                dr["TaxType"] = -1;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gdvConcepts.DataSource = dt;
                this.gdvConcepts.DataBind();
            }
        }

        public override void DataBind()
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            String c = String.Empty;
            this.DataBind(concept.SelectConceptBySearching(this.CurrentBillerID, c, c, c));
        }

        public void DataBind(ConceptsDataSet.Concepts_GetBySearchingDataTable data)
        {
            base.DataBind();
            this.gdvConcepts.DataSource = data;
            this.gdvConcepts.DataBind();
        }

        protected void gdvConcepts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvConcepts.PageIndex = e.NewPageIndex;
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            String c = String.Empty;

            this.DataBind();
        }

        protected void imgbtnConcepts_Click(object sender, EventArgs e)
        {
            int i;
            ImageButton s = sender as ImageButton;
            this.WebMessageBox1.CommandArguments = string.Empty;

            switch (s.ID)
            {
                case "imgbtnDelete":

                    if (!int.TryParse(s.CommandArgument, out i))
                        return;

                    this.WebMessageBox1.Title = "Eliminar Producto";
                    this.WebMessageBox1.ShowMessage("¿Seguro que desea eliminar este producto?", System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
                    this.WebMessageBox1.CommandArguments = MESSAGE_DELETE;
                    this.ConcepTempID = i;
                    break;
                case "imgbtnDetail":
                    Response.Redirect("~/Concepts/ConceptsDetails.aspx?ConceptID=" + s.CommandArgument);
                    break;
                case "imgbtnEdit":
                    Response.Redirect("~/Concepts/ConceptsModify.aspx?ConceptID=" + s.CommandArgument);
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_DELETE:

                    if (e.DialogResult == WebMessageBoxDialogResultType.No)
                        return;

                    this.DeleteConcept(e);
                    break;
            }
        }

        private void DeleteConcept(WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Concepts concept;
            ConceptsDataSet ds;
            int x;

            ds = new ConceptsDataSet();
            concept = new PACFD.Rules.Concepts();
            concept.SelectByID(ds.Concepts, this.ConcepTempID);
            String BeforeDataSet = ds.GetXml();

            if (ds.Concepts.Count > 0)
            {
                ConceptsDataSet.ConceptsRow drConcepts = ds.Concepts[0];
                drConcepts.Delete();
            }

            x = concept.Update_Delete(ds.Concepts);
            this.ConcepTempID = 0;
            this.DataBind();

            if (x == 0)
            {
                LogManager.WriteStackTrace(new Exception("Error al tratar de eliminar el Producto"));
                this.WebMessageBox1.Title = "Eliminar Producto";
                this.WebMessageBox1.ShowMessage("Error al tratar de eliminar el Producto", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                this.WebMessageBox1.CommandArguments = string.Empty;
                return;
            }
            else if (x == 2)
            {
                this.WebMessageBox1.Title = "Eliminar Producto";
                WebMessageBox1.ShowMessage("No es posible eliminar este producto, ya que ha sido incluido en un comprobante.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                this.WebMessageBox1.CommandArguments = string.Empty;
                return;
            }

            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino un concepto del sistema.", this.UserName), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
            LogManager.WriteStackTrace(new Exception("Éxito al Eliminar el Producto."));
        }
    }
}