﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Concepts
{
    public partial class ConceptsModify : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ucConceptsEditor.EditorMode = ConceptsEditorMode.Modify;

            if (IsPostBack)
                return;
            if (Request.QueryString["ConceptID"].IsNull())
                return;
            ViewState["ConceptID"] = Request.QueryString["ConceptID"];

            this.Title = "Modificar Producto";
            this.CustomizeEditor();
            this.LoadData(Convert.ToInt32(ViewState["ConceptID"]));
        }
        /// <summary>
        /// Load concept information.
        /// </summary>
        /// <param name="ConceptID">int ConceptID</param>
        private void LoadData(int ConceptID)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            ConceptsDataSet.ConceptsDataTable t;
            t = concept.SelectByID(ConceptID);
            this.ucConceptsEditor.ModifyLoadData(t[0].Code, 
                (t[0].UnitType.Trim() == String.Empty ? "[Sin Asignar]" : t[0].UnitType), t[0].Description, 
                t[0].UnitPrice.ToString("0,0.00"), t[0].TaxRatePercentage.ToString("0,0.00"), t[0].AppliesTax, t[0].Account, t[0].IsClavProdServNull() ? "" : t[0].ClavProdServ);
        }
        /// <summary>
        /// Modify the concept information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Modify(ConceptsEventArgs e)
        {
            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet ds = new ConceptsDataSet();
            concept.SelectByID(ds.Concepts, Convert.ToInt32(ViewState["ConceptID"]));

            String BeforeDataSet = ds.GetXml();

            if (ds.Concepts.Count > 0)
            {
                ConceptsDataSet.ConceptsRow drConcepts = ds.Concepts[0];
                drConcepts.Code = e.Code;
                drConcepts.UnitType = e.Type;
                drConcepts.Description = e.Description;
                drConcepts.UnitPrice = Convert.ToDecimal(e.UnitPrice);
                drConcepts.TaxRatePercentage = Convert.ToDecimal(e.Tax);
                drConcepts.AppliesTax = e.AppliesTax;
                drConcepts.Account = e.Account;
                drConcepts.ClavProdServ = e.Prod;
            }

            if (!concept.Update(ds.Concepts))
                return false;

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico la información de un concepto.",
                this.UserName), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
            #endregion

            LogManager.WriteStackTrace(new Exception("Éxito al Modificar el Producto."));
            this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");
            return true;
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucConceptsEditor_ApplyChanges(object sender, ConceptsEventArgs e)
        {
            if (e.EditorMode != ConceptsEditorMode.Modify) return;
            else this.Modify(e);
        }
        /// <summary>
        /// Fires the PACFDManager.Concepts.ConceptsEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucConceptsEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(ConceptsList).Name + ".aspx");
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucConceptsEditor.ModifyMode = true;
            this.ucConceptsEditor.RegisterScripts(true);
        }
    }
}