﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Common;
using CrystalDecisions.Shared;
using System.IO;
#endregion

namespace PACFDManager.Exportation
{
    public partial class ReportViewer : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Sets the visible property of controls that contains the UserControl ReportViewer.
        /// </summary>
        public override bool Visible
        {
            set
            {
                this.CrystalReportViewer1.Visible =
                this.CRS1.Visible =
                this.ddlTypes.Visible =
                //this.tipTypes.Visible =
                this.btnExport.Visible = value;
            }
        }
        /// <summary>
        /// Sets the filename property of the report.
        /// </summary>
        public string FileName
        {
            set { this.CRS1.Report.FileName = value; }
        }
        /// <summary>
        /// Sets the data source are that displayed in the report.
        /// </summary>
        public DataSet SetDataSource
        {
            set
            {
                if (!value.IsNull())
                {
                    this.CRS1.ReportDocument.SetDataSource(value);
                    this.CrystalReportViewer1.ReportSource = CRS1;
                }
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Exportation.ReportViewer.btnExport_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            var rptStream = CRS1.ReportDocument.ExportToStream((ExportFormatType)Convert.ToInt32(this.ddlTypes.SelectedValue));
            var b = new byte[rptStream.Length];
            rptStream.Read(b, 0, Convert.ToInt32(rptStream.Length));
            Response.Clear();
            Response.Buffer = true;

            String ContentType = DocumentFormat().Split('_')[0];
            String ReportExtention = DocumentFormat().Split('_')[1];

            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report" + ReportExtention);
            Response.ContentType = ContentType;
            Response.BinaryWrite(b);
            Response.End();
        }
        /// <summary>
        /// Select the type and extent of the file to be downloaded.
        /// </summary>
        /// <returns>Returns a string containing the extent and type of file that will be downloaded</returns>
        private string DocumentFormat()
        {
            string type = String.Empty;
            string ReportName = String.Empty;

            switch ((ExportFormatType)int.Parse(this.ddlTypes.SelectedValue))
            {
                case ExportFormatType.Excel:
                    type = "application/vnd.ms-excel";
                    ReportName = ".xls";
                    break;
                case ExportFormatType.RichText:
                    type = "application/rtf";
                    ReportName = ".rtf";
                    break;
                case ExportFormatType.WordForWindows:
                    type = "application/msword";
                    ReportName = ".doc";
                    break;
                default:
                    type = "application/pdf";
                    ReportName = ".pdf";
                    break;
            }
            return type + "_" + ReportName;
        }
    }
}