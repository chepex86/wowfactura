﻿<%@ Page Title="Exportación" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Exportation.aspx.cs" Inherits="PACFDManager.Exportation.Exportation" %>

<%@ Register Src="ReportViewer.ascx" TagName="ReportViewer" TagPrefix="uc1" %>
<%@ Register Src="SearchFilter.ascx" TagName="SearchFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <div id="container" class="wframe60">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Exportación"></asp:Label>
                    </h2>
                </div>
                <div>
                    <uc2:SearchFilter ID="SearchFilter1" runat="server" />
                </div>
                <asp:Panel ID="pnlDownload" runat="server" Visible="False">
                    <div>
                        <label class="desc">
                            <asp:Label ID="lblDownload" runat="server" Text="Descargar reporte de diario"></asp:Label></label>
                        <span>
                            <asp:ImageButton ID="imgBtnDownload" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                OnClick="imgBtnDownload_Click" /></span>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <br />
    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
    <br />
    <div>
        <uc1:ReportViewer ID="ReportViewer1" runat="server" />
    </div>
</asp:Content>
