﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.Exportation
{
    public partial class DownloadFiles : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (this.LoadFile())
                    Response.Redirect(Request.UrlReferrer.ToString());
        }
        /// <summary>
        ///Generates the file to download, based on the argument hosted at the session.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        protected bool LoadFile()
        {
            if (Session["File"].IsNull())
                return false;

            String c = Session["File"].ToString();
            String NameFile = String.Empty;

            if (c.Split('_')[0] == "1" || c.Split('_')[0] == "2") //ContPaq - ContPaqI
            {
                PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
                BillersDataSet.BillersDataTable ta;
                ta = biller.SelectByID(this.CurrentBillerID);

                NameFile = (c.Split('_')[0] == "2" ? "Exportacion_a_ContPaq_I_" : "Exportacion_a_ContPaq_") + ta[0].RFC + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            }
            else if (c.Split('_')[0] == "3") //Aspel Coi
            {
                NameFile = "Poliza_Modelo_Aspel_Coi_" + DateTime.Now.ToString("yyyyMMdd") + ".pol";
            }

            Response.AddHeader("Content-disposition", "attachment; filename=" + NameFile);
            Response.ContentType = "application/octet-stream";
            Response.Write(c.Split('_')[1]);
            Response.End();
            ViewState["Name"] = null;

            return true;
        }
    }
}