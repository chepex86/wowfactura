﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace PACFDManager.Exportation
{
    /// <summary>
    /// Class used as arguments send by the SearchFilter control in a search.
    /// </summary>
    public class ExportSearchEventArgs
    {
        /// <summary>
        /// Gets the date you want to find.
        /// </summary>
        public DateTime StartDate { get; private set; }
        /// <summary>
        /// Gets the Application name that will export the information.
        /// </summary>
        public String ExportTo { get; private set; }
        /// <summary>
        /// Gets the Rate of exchange.
        /// </summary>
        public Decimal Currency { get; private set; }
        /// <summary>
        /// Gets the text of the filter search.
        /// </summary>
        public String TextFilter { get; private set; }
        /// <summary>
        /// Creates a new instance of the class ExportSearchEventArgs.
        /// </summary>
        /// <param name="startDate">The date you want to find</param>
        /// <param name="exportTo">Application name that will export the information</param>
        /// <param name="currency">Rate of exchange</param>
        public ExportSearchEventArgs(DateTime startDate, String exportTo, Decimal currency)
        {
            this.StartDate = startDate;
            this.ExportTo = exportTo;
            this.Currency = currency;
        }
        /// <summary>
        /// Creates a new instance of the class ExportSearchEventArgs.
        /// </summary>
        /// <param name="startDate">The date you want to find</param>
        /// <param name="currency">Rate of exchange</param>
        public ExportSearchEventArgs(DateTime startDate, Decimal currency)
        {
            this.StartDate = startDate;
            this.Currency = currency;
        }
    }
}