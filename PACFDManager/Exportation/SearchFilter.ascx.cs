﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Text;
#endregion

namespace PACFDManager.Exportation
{
    public partial class SearchFilter : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the Search Event Handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller</param>
        public delegate void SearchEventHandler(object sender, ExportSearchEventArgs e);
        /// <summary>
        /// Event handler for the concepts Search.
        /// </summary>
        public event SearchEventHandler Search;
        /// <summary>
        /// Fires the PACFDManager.Exportation.SearchFilter.OnSearch event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(ExportSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.txtStartDate_CalendarExtender.Format = "dd/MM/yyyy";
                this.txtStartDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                this.txtCurrency.Text = "1";
            }

            this.lblError.Visible = false;
            this.RegisterScript();
        }
        /// <summary>
        /// Fires the PACFDManager.Exportation.SearchFilter.Search_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            ExportSearchEventArgs a;
            try
            {
                a = new ExportSearchEventArgs(DateTime.Parse(this.txtStartDate.Text),
                                              this.ddlExport.SelectedValue,
                                              Decimal.Parse(this.txtCurrency.Text));
                this.OnSearch(a);
            }
            catch (Exception ex)
            {
                this.lblError.Text = "El formato del tipo de cambio no es correcto.";
                this.lblError.Visible = true;

                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Register a script in the page.
        /// </summary>
        /// <returns>Returns true if the scripts are registered properly, otherwise returns false.</returns>
        protected bool RegisterScript()
        {
            try
            {
                this.txtCurrency.Attributes["onkeypress"] = "javascript:sFilter.OnlyNumbers()";
                this.txtStartDate.Attributes["onKeyDown"] = "javascript:sFilter.ReadOnly()";
                this.txtStartDate.Attributes["onKeyPress"] = "javascript: return false;";
                this.txtStartDate.Attributes["onPaste"] = "javascript: return false;";
    
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}