﻿var sFilter = new jclassSearchFilter();

function jclassSearchFilter() {
    this.ReadOnly = function(e) {
        var evt = e || window.event;
        if (evt) {
            var keyCode = evt.charCode || evt.keyCode;
            if (keyCode === 8 || keyCode === 46) {
                if (evt.preventDefault) {
                    evt.preventDefault();
                } else {
                    evt.returnValue = false;
                }
            }
        }
    }

    this.OnlyNumbers = function() {
        if (event.keyCode < 45 || event.keyCode > 57) {
            event.returnValue = false;
        } else { return true; }
    }
}