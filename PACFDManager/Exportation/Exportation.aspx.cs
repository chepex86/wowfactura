﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using PACFD.Rules;
using System.Data;
#endregion

namespace PACFDManager.Exportation
{
    public partial class Exportation : PACFDManager.BasePage
    {
        /// <summary>
        ///  Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReportViewer1.Visible = false;
            this.SearchFilter1.Search += new SearchFilter.SearchEventHandler(SearchFilter1_Search);
            this.lblMessage.Text = String.Empty;
            this.lblMessage.Visible = false;
        }
        /// <summary>
        /// Event fired by the delegate of the user control SearchFilter.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        void SearchFilter1_Search(object sender, ExportSearchEventArgs e)
        {
            PACFD.DataAccess.ExportContpaq_DA export = new PACFD.DataAccess.ExportContpaq_DA();
            ExportContpaqDataSet ds = new ExportContpaqDataSet();
            ds = export.FillTables(this.CurrentBillerID, e.StartDate, e.Currency);

            if (ds.IsNull())
            {
                this.lblMessage.Text = "No hay registros disponibles para la fecha seleccionda.";
                this.lblMessage.Visible = true;
                return;
            }
            else
            {
                bool flag = true;
                for (int i = 0; i < ds.ExportInfoDetails.Count; i += 1)
                {
                    if (ds.ExportInfoDetails[i].Account == String.Empty)
                        flag = false;
                }
                if (!flag)
                {
                    this.lblMessage.Text = "Uno o varios registros no tienen un número de cuenta, <br /> dirigirse al Listado de Productos para asignarles una.";
                    this.lblMessage.Visible = true;
                }
            }

            this.pnlDownload.Visible =
            this.ReportViewer1.Visible = true;
            this.ReportViewer1.FileName = "~/Exportation/CrystalReports/crExportation.rpt";
            this.ReportViewer1.SetDataSource = ds;
            this.ExportInfo = ds;
            ds.Dispose();

            switch (e.ExportTo)
            {
                case "ContPaq":
                    this.ExportContPaq(30);
                    break;
                case "ContPaq I":
                    this.ExportContPaq(100);
                    break;
                case "Aspel Coi":
                    this.ExportAspelCoi(e);
                    break;
            }
        }
        /// <summary>
        /// Redirected to the page where the file is downloaded.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void imgBtnDownload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Exportation/DownloadFiles.aspx");
        }
        /// <summary>
        /// Generates the file will be exported to Contpaq / Contpaq I.
        /// </summary>
        /// <param name="Long">Variable that will decide the length of description field.</param>
        private void ExportContPaq(int Long)
        {
            ExportContpaqDataSet ds = (ExportContpaqDataSet)ExportInfo;
            ExportContpaqDataSet.ExportInfoDetailsDataTable d = ds.ExportInfoDetails;

            ViewState["flag"] = true;
            String c = String.Empty;
            String reference = "Ing de: " + ds.ExportInfo[0].Reference;
            String e = "#,0.00", b = " ";
            int t;

            c = "P " + ds.ExportInfo[0].Date.ToString("yyyyMMdd") + " 1 00000001 1 000 " +
                (reference.Length < 100 ? (String.Format("{0, -100}", reference)) : reference.Substring(0, 100)) +
                " 01 2 " + System.Environment.NewLine;

            for (int i = 0; i < d.Count; i += 1)
            {
                t = d[i].Type;
                c += "M" +
                     b + (d[i].Account.Length < 20 ? String.Format("{0, -20}", d[i].Account) : d[i].Account.Substring(0, 20)) +
                     b + (d[i].Reference.Length < 10 ? String.Format("{0, -10}", d[i].Reference) : d[i].Reference.Substring(0, 10)) + b + t +
                     b + ((t == 1 ? d[i].ChargeMXN.ToString(e).Length : d[i].DepositMXN.ToString(e).Length) < 20 ?
                     String.Format("{0, 16}", (t == 1 ? d[i].ChargeMXN.ToString(e) : d[i].DepositMXN.ToString(e))) :
                     (t == 1 ? d[i].ChargeMXN : d[i].DepositMXN).ToString(e).Substring(0, 16)) +
                     ((t == 1 ? d[i].ChargeUSD.ToString(e).Length : d[i].DepositUSD.ToString(e).Length) < 20 ?
                     String.Format("{0, 21}", (t == 1 ? d[i].ChargeUSD.ToString(e) : d[i].DepositUSD.ToString(e))) :
                     (t == 1 ? d[i].ChargeUSD : d[i].DepositUSD).ToString(e).Substring(0, 21)) +
                     b + (d[i].Description.Length < Long ? d[i].Description : d[i].Description.Substring(0, Long)) + System.Environment.NewLine;
            }

            this.Session["File"] = (Long == 30 ? "1_" : "2_") + c;
        }
        /// <summary>
        /// Generates the file will be exported to Aspel Coi.
        /// </summary>
        /// <param name="a">ExportSearchEventArgs arguments</param>
        private void ExportAspelCoi(ExportSearchEventArgs a)
        {
            ExportContpaqDataSet ds = (ExportContpaqDataSet)ExportInfo;
            ExportContpaqDataSet.ExportInfoDetailsDataTable d = ds.ExportInfoDetails;
            String c = String.Empty;
            String f = "dd/MM/yy";
            Int32 t = 0;

            c += "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>";
            c += "<DATAPACKET Version=\"2.0\">";
            c += "<METADATA>";
            c += "<FIELDS>";
            c += "<FIELD attrname=\"VersionCOI\" fieldtype=\"i2\" />";
            c += "<FIELD attrname=\"TipoPoliz\" fieldtype=\"string\" WIDTH=\"2\" />";
            c += "<FIELD attrname=\"DiaPoliz\" fieldtype=\"string\" WIDTH=\"8\" />";
            c += "<FIELD attrname=\"ConcepPoliz\" fieldtype=\"string\" WIDTH=\"120\" />";
            c += "<FIELD attrname=\"Partidas\" fieldtype=\"nested\">";
            c += "<FIELDS>";
            c += "<FIELD attrname=\"Cuenta\" fieldtype=\"string\" WIDTH=\"21\" />";
            c += "<FIELD attrname=\"Depto\" fieldtype=\"i4\" />";
            c += "<FIELD attrname=\"ConceptoPol\" fieldtype=\"string\" WIDTH=\"120\" />";
            c += "<FIELD attrname=\"Monto\" fieldtype=\"r8\" />";
            c += "<FIELD attrname=\"TipoCambio\" fieldtype=\"r8\" />";
            c += "<FIELD attrname=\"DebeHaber\" fieldtype=\"string\" WIDTH=\"1\" />";
            c += "</FIELDS>";
            c += "<PARAMS />";
            c += "</FIELD>";
            c += "</FIELDS>";
            c += "<PARAMS />";
            c += "</METADATA>";
            c += "<ROWDATA>";
            c += "<ROW VersionCOI=\"50\" TipoPoliz=\"Ig\" DiaPoliz=\"" + a.StartDate.ToString(f) + "\" ConcepPoliz=\"CAPTURA RAPIDA SIN PROBLEMAS\">";
            c += "<Partidas>";

            for (int i = 0; i < d.Count; i++)
            {
                t = d[i].Type;
                c += "<ROWPartidas Cuenta=\"" + d[i].Account + "\" Depto=\"0\" ConceptoPol=\"" + d[i].Reference + " - " + d[i].Description + "\" ";
                c += "Monto=\"" + (t == 1 ? d[i].ChargeMXN : d[i].DepositMXN) + "\" TipoCambio=\"" + a.Currency + "\" ";
                c += "DebeHaber=\"" + (t == 1 ? "D" : "H") + "\" />";
            }

            c += "</Partidas>";
            c += "</ROW>";
            c += "</ROWDATA>";
            c += "</DATAPACKET>";

            Session["File"] = "3_" + c;
        }
        /// <summary>
        /// Holds the state on the viewstate of the dataset.
        /// </summary>
        private object ExportInfo
        {
            get { return this.ViewState["ds"]; }
            set { this.ViewState["ds"] = value; }
        }
    }
}