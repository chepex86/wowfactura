﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Data;
using System.Collections;
#endregion

namespace PACFDManager
{
    public partial class Select : BasePage
    {
        const string CFD = "CFD 2.0";
        const string CFDI = "CFDI 3.0";
        const string CBB = "CBB 1.0";
        const string CFD2_2 = "CFD 2.2";
        const string CFDI3_2 = "CFDI 3.2";
        const string CFDI3_3 = "CFDI 3.3";
        const string CFDI4_0 = "CFDI 4.0";

        protected void Page_Load(Object sender, EventArgs e)
        {
            this.lblQueryString.Text = String.Empty;
            this.lblQueryString.Visible = false;
            this.lblMessage.Text = String.Empty;

            if (IsPostBack) //return if is post back
                return;

            String op = this.GetValueOfQueryString("op", true);

            if (String.IsNullOrEmpty(op))
            {
                this.LoadFirtsData();
                return;
            }

            String backPage = this.GetValueOfQueryString("page", false);

            if (backPage == String.Empty)
                backPage = "esta opción";

            switch (op)
            {
                case "needCli":
                    this.lblQueryString.Text = String.Format("Necesita seleccionar un grupo para entrar a {0}", backPage);
                    this.lblQueryString.Visible = true;
                    this.LoadFirtsData();
                    break;
                case "needBill":
                    this.lblQueryString.Text = String.Format("Necesita seleccionar una empresa para entrar a {0}", backPage);
                    this.lblQueryString.Visible = true;

                    if (this.IsAdvancedClient || this.IsBasicClient)
                        this.LoadFirtsData();
                    else
                        this.LoadLastData(this.CurrentGroupID);
                    break;
                case "needCliBill":
                    this.lblQueryString.Text = String.Format("Necesita seleccionar un grupo y luego una empresa para entrar a {0}", backPage);
                    this.lblQueryString.Visible = true;
                    this.LoadFirtsData();
                    break;
                case "chgCli":
                    this.LoadFirtsData();
                    break;
                case "chgBill":
                    this.LoadLastData(this.CurrentGroupID);
                    break;
                default:
                    this.LoadFirtsData();
                    break;
            }
        }

        private String GetValueOfQueryString(String parameterName, bool descrypt)
        {
            if (String.IsNullOrEmpty(this.Request.QueryString[parameterName]))
                return String.Empty;

            try
            {
                if (descrypt)
                    return Cryptography.DecryptUnivisitString(this.Request.QueryString[parameterName].Replace(' ', '+'));
                else
                    return this.Request.QueryString[parameterName];
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                LogManager.WriteError(ex);
            }
            return String.Empty;
        }

        protected void dgvBillers_SelectedIndexChanged(Object sender, EventArgs e)
        {
            GridViewRow row = this.dgvBillers.SelectedRow;
            ElectronicBillingType type = ElectronicBillingType.Indeterminate;
            bool flag = true;

            switch (((Label)row.FindControl("lblBillingType")).Text.Trim().ToUpper())
            {
                case CFD:
                    if (CFDEnable)
                        type = ElectronicBillingType.CFD;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFD.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CFDI:
                    if (CFDIEnable)
                        type = ElectronicBillingType.CFDI;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFDI.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CBB:
                    if (CBBEnable)
                        type = ElectronicBillingType.CBB;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CBB.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CFD2_2:
                    if (CFD2_2Enable)
                        type = ElectronicBillingType.CFD2_2;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFD 2.2.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CFDI3_2:
                    if (CFDI3_2Enable)
                        type = ElectronicBillingType.CFDI3_2;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFDI 3.2.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CFDI3_3:
                    if (CFDI3_3Enable)
                        type = ElectronicBillingType.CFDI3_3;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFDI 3.3.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                case CFDI4_0:
                    if (CFDI4_0Enable)
                        type = ElectronicBillingType.CFDI4_0;
                    else
                    {
                        flag = false;
                        this.WebMessageBox1.Title = "Alerta";
                        this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación CFDI 4.0.",
                            System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                    }
                    break;
                default: type = ElectronicBillingType.CFDI4_0; break;
            }

            if (flag)
            {
                SetCurrentBillerID(int.Parse(/*row.Cells[0].Text*/(row.FindControl("hfBillerID") as HiddenField).Value), row.Cells[2].Text, type);

                if (!String.IsNullOrEmpty((row.FindControl("hfBranchID") as HiddenField).Value) &&
                    !String.IsNullOrEmpty((row.FindControl("lblBranchName") as Label).Text))
                {
                    SetCurrentBranchID(int.Parse(
                        (row.FindControl("hfBranchID") as HiddenField).Value),
                        (row.FindControl("lblBranchName") as Label).Text);
                }

                this.Response.Redirect(@"~/Default.aspx");
            }
        }

        protected void dgvGroup_SelectedIndexChanged(Object sender, EventArgs e)
        {
            GridViewRow row = this.dgvGroup.SelectedRow;
            int groupID = int.Parse(row.Cells[1].Text);
            SetCurrentGroupID(groupID, row.Cells[2].Text);
            this.LoadLastData(groupID);
        }

        protected void dgvBillers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Label l;

            if (e.Row.RowIndex < 0)
                return;

            l = (Label)e.Row.FindControl("lblBillingType");

            switch (l.Text)
            {
                case "CFD": l.Text = CFD; break;
                case "CFDI": l.Text = CFDI; break;
                case "CBB": l.Text = CBB; break;
                case "CFD2_2": l.Text = CFD2_2; break;
                case "CFDI3_2": l.Text = CFDI3_2; break;
                case "CFDI3_3": l.Text = CFDI3_3; break;
                case "CFDI4_0": l.Text = CFDI4_0; break;

                default: l.Text = "????"; break;
            }
        }

        private void LoadFirtsData()
        {
            this.lblGroupTitle.Visible = false;
            this.lkbAddNewClient.Visible = false;
            this.lkbSelectAnotherClient.Visible = false;
            this.lkbAddNewBillers.Visible = false;
            this.lblBillerTitle.Visible = false;

            #region Users
            PACFD.Rules.Groups ruGroup = new PACFD.Rules.Groups();
            GroupsDataSet dsGroups = new GroupsDataSet();

            if (this.IsAdministrator)
            {
                ruGroup.SelectBySearching(dsGroups.Groups, null, true);

                if (dsGroups.Groups.Count > 0)
                {
                    if (dsGroups.Groups.Count == 1)
                    {
                        GroupsDataSet.GroupsRow drGroup = dsGroups.Groups[0];
                        SetCurrentGroupID(drGroup.GroupID, drGroup.Name);
                        this.LoadLastData(this.CurrentGroupID);
                    }
                    else
                    {
                        this.lblGroupTitle.Visible = true;
                        this.divUserList.Visible = true;
                        this.divBillers.Visible = false;
                        this.dgvGroup.DataSource = dsGroups.Groups;
                        this.dgvGroup.DataBind();
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbAddNewClient.Visible = true;
                    this.lblMessage.Text = "No existe ningun grupo dado de alta.<br /> Opciones Disponibles";
                }
            }
            else if (this.IsSAT)
            {
                ruGroup.SelectBySearching(dsGroups.Groups, null, true);

                if (dsGroups.Groups.Count > 0)
                {
                    if (dsGroups.Groups.Count == 1)
                    {
                        GroupsDataSet.GroupsRow drGroup = dsGroups.Groups[0];
                        SetCurrentGroupID(drGroup.GroupID, drGroup.Name);
                        this.LoadLastData(this.CurrentGroupID);
                    }
                    else
                    {
                        this.lblGroupTitle.Visible = true;
                        this.divUserList.Visible = true;
                        this.divBillers.Visible = false;
                        this.dgvGroup.DataSource = dsGroups.Groups;
                        this.dgvGroup.DataBind();
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbAddNewClient.Visible = true;
                    this.lblMessage.Text = "No existe ningun grupo dado de alta.<br /> Opciones Disponibles";
                }
            }
            else if (this.IsAdvancedClient)
            {
                this.divUserList.Visible = false;
                PACFD.Rules.Billers ruBillers = new PACFD.Rules.Billers();
                BillersDataSet dsBiller = new BillersDataSet();
                ruBillers.SelectBillersForList(dsBiller.Billers_GetForList, this.CurrentGroupID, true);

                if (dsBiller.Billers_GetForList.Count > 0)
                {
                    if (dsBiller.Billers_GetForList.Count == 1)
                    {
                        var drBiller = dsBiller.Billers_GetForList[0];
                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;
                        if (!drBiller.IsElectronicBillingTypeNull())
                            type = (ElectronicBillingType)drBiller.ElectronicBillingType;

                        SetCurrentBillerID(drBiller.BillerID, drBiller.Name, type);
                        SetCurrentBranchID(drBiller.BranchID, drBiller.BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        this.divBillers.Visible = true;
                        this.dgvBillers.DataSource = dsBiller.Billers_GetForList;
                        this.dgvBillers.DataBind();
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = false;
                    this.lkbAddNewBillers.Visible = true;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
            else if (this.IsBasicClient)
            {
                this.divUserList.Visible = false;
                PACFD.Rules.Users ruBillers = new PACFD.Rules.Users();
                UsersDataSet.GetUserBillerSelectedDataTable dtBiller;
                ArrayList array = new ArrayList();
                dtBiller = ruBillers.GetUserBillerSelected(this.UserID);

                if (dtBiller.Count > 0)
                {
                    for (int i = dtBiller.Count; i >= 1; i--)
                    {
                        if (dtBiller[i - 1].BillerSelected == 0)
                            dtBiller[i - 1].Delete();
                    }

                    // this.DeleteRowsInTable(ref dt, array);
                    dtBiller.AcceptChanges();

                    if (dtBiller.Count == 1)
                    {
                        var drBiller = dtBiller[0];
                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;

                        if (!drBiller.IsElectronicBillingTypeNull())
                            type = (ElectronicBillingType)drBiller.ElectronicBillingType;

                        SetCurrentBillerID(int.Parse(dtBiller[0].BillerID.ToString()), dtBiller[0].Name.ToString(), type);
                        SetCurrentBranchID(dtBiller[0].BranchID, dtBiller[0].BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        if (dtBiller.Count < 1)
                        {
                            this.lblGroupTitle.Text = "En este momento no tienes ninguna empresa asignada, ponte en contacto con el Administrador del Sistema.";
                            this.lblGroupTitle.Visible = true;
                            this.dgvBillers.Visible = false;
                        }
                        else
                        {
                            this.divBillers.Visible = true;
                            this.dgvBillers.DataSource = dtBiller;
                            this.dgvBillers.DataBind();
                        }
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = false;
                    this.lkbAddNewBillers.Visible = true;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
            #endregion
        }

        private void DeleteRowsInTable(ref DataTable dt, ArrayList array)
        {
            if (array == null || array.Count < 1)
            { return; }

            foreach (DataRow dr in array)
            { dt.Rows.Remove(dr); }
        }

        private void LoadLastData(int groupID)
        {
            BillersDataSet dsBiller = new BillersDataSet();
            PACFD.Rules.Billers ruBillers = new PACFD.Rules.Billers();

            this.lblGroupTitle.Visible = false;
            this.lkbAddNewClient.Visible = false;
            this.lkbSelectAnotherClient.Visible = false;
            this.lkbAddNewBillers.Visible = false;
            this.divUserList.Visible = false;
            this.lblBillerTitle.Visible = true;

            if (this.IsAdministrator)
            {
                ruBillers.SelectBillersForList(dsBiller.Billers_GetForList, groupID, true);
                if (dsBiller.Billers_GetForList.Count > 0)
                {
                    if (dsBiller.Billers_GetForList.Count == 1)
                    {
                        var drBiller = dsBiller.Billers_GetForList[0];
                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;

                        if (!drBiller.IsElectronicBillingTypeNull())
                        { type = (ElectronicBillingType)drBiller.ElectronicBillingType; }

                        SetCurrentBillerID(drBiller.BillerID, drBiller.Name, type);
                        SetCurrentBranchID(drBiller.BranchID, drBiller.BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        this.divBillers.Visible = true;
                        this.dgvBillers.DataSource = dsBiller.Billers_GetForList;
                        this.dgvBillers.DataBind();
                    }
                }
                else
                {
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = true;
                    this.lkbAddNewBillers.Visible = true;
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
            else if (this.IsSAT)
            {
                ruBillers.SelectBillersForList(dsBiller.Billers_GetForList, this.CurrentGroupID, true);
                if (dsBiller.Billers_GetForList.Count > 0)
                {
                    if (dsBiller.Billers_GetForList.Count == 1)
                    {
                        var drBiller = dsBiller.Billers_GetForList[0];
                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;

                        if (!drBiller.IsElectronicBillingTypeNull())
                        { type = (ElectronicBillingType)drBiller.ElectronicBillingType; }

                        SetCurrentBillerID(drBiller.BillerID, drBiller.Name, type);
                        SetCurrentBranchID(drBiller.BranchID, drBiller.BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        this.divBillers.Visible = true;
                        this.dgvBillers.DataSource = dsBiller.Billers_GetForList;
                        this.dgvBillers.DataBind();
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lkbAddNewBillers.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = true;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
            else if (this.IsAdvancedClient)
            {
                this.divUserList.Visible = false;

                ruBillers.SelectBillersForList(dsBiller.Billers_GetForList, this.CurrentGroupID, true);
                if (dsBiller.Billers_GetForList.Count > 0)
                {
                    if (dsBiller.Billers_GetForList.Count == 1)
                    {
                        var drBiller = dsBiller.Billers_GetForList[0];

                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;

                        if (!drBiller.IsElectronicBillingTypeNull())
                        { type = (ElectronicBillingType)drBiller.ElectronicBillingType; }

                        SetCurrentBillerID(drBiller.BillerID, drBiller.Name, type);
                        SetCurrentBranchID(drBiller.BranchID, drBiller.BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        this.divBillers.Visible = true;
                        this.dgvBillers.DataSource = dsBiller.Billers_GetForList;
                        this.dgvBillers.DataBind();
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = false;
                    this.lkbAddNewBillers.Visible = true;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
            else if (this.IsBasicClient)
            {
                this.divUserList.Visible = false;
                PACFD.Rules.Users ruBiller = new PACFD.Rules.Users();
                UsersDataSet.GetUserBillerSelectedDataTable dtBiller;
                ArrayList array = new ArrayList();
                dtBiller = ruBiller.GetUserBillerSelected(this.UserID);

                if (dtBiller.Count > 0)
                {
                    for (int i = dtBiller.Count; i >= 1; i--)
                    {
                        if (dtBiller[i - 1].BillerSelected == 0)
                            dtBiller[i - 1].Delete();
                    }

                    dtBiller.AcceptChanges();

                    if (dtBiller.Count == 1)
                    {
                        var drBiller = dtBiller[0];
                        ElectronicBillingType type = ElectronicBillingType.Indeterminate;

                        if (!drBiller.IsElectronicBillingTypeNull())
                        { type = (ElectronicBillingType)drBiller.ElectronicBillingType; }

                        SetCurrentBillerID(int.Parse(dtBiller[0].BillerID.ToString()), dtBiller[0].Name, type);
                        SetCurrentBranchID(drBiller.BranchID, drBiller.BranchName);
                        this.Response.Redirect(@"~/Default.aspx");
                        return;
                    }
                    else
                    {
                        if (dtBiller.Count < 1)
                        {
                            this.lblGroupTitle.Text = "En este momento no tienes ninguna empresa asignada, ponte en contacto con el Administrador del Sistema.";
                            this.lblGroupTitle.Visible = true;
                            this.dgvBillers.Visible = false;
                        }
                        else
                        {
                            this.divBillers.Visible = true;
                            this.dgvBillers.DataSource = dtBiller;
                            this.dgvBillers.DataBind();
                        }
                    }
                }
                else
                {
                    this.lblBillerTitle.Visible = false;
                    this.lblMessage.Visible = true;
                    this.lkbSelectAnotherClient.Visible = false;
                    this.lkbAddNewBillers.Visible = true;
                    this.lblMessage.Text = String.Format("El grupo \"{0}\" no tiene configurado ninguna empresa.<br /> Opciones Disponibles", this.CurrentGroupName);
                }
            }
        }

        protected void lkbAddNewClient_Click(Object sender, EventArgs e) { this.Response.Redirect(@"~/groups/groupAdd.aspx"); }
        protected void lkbSelectAnotherClient_Click(Object sender, EventArgs e) { this.LoadFirtsData(); }
        protected void lkbAddNewBillers_Click(Object sender, EventArgs e) { this.Response.Redirect(@"~/Billers/BillersAdd.aspx"); }
    }
}