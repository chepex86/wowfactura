﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerSearch.ascx.cs"
    Inherits="PACFDManager.Clients.CustomerSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<table>
    <tr>
        <td align="center" colspan="2" class="Titulo">
            <asp:Label ID="lblTitle" runat="server" Text="Busqueda de Clientes" meta:resourcekey="lblTitleResource1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:CheckBox ID="chkboxByEmail" runat="server" Text="Por Correo:" />
        </td>
        <td>
            <asp:TextBox ID="txtSearchByEmail" runat="server" MaxLength="50" Width="200px" meta:resourcekey="txtSearchByEmailResource1"></asp:TextBox>
            <asp:AutoCompleteExtender ID="txtSearchByEmail_AutoCompleteExtender" runat="server"
                EnableCaching="true" UseContextKey="True" ServicePath="~/AutoComplete.asmx" MinimumPrefixLength="1"
                ServiceMethod="GetClientByEmail" TargetControlID="txtSearchByEmail">
            </asp:AutoCompleteExtender>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:CheckBox ID="chkboxByName" runat="server" Text="Por Nombre:" />
        </td>
        <td>
            <asp:TextBox ID="txtSearchByName" runat="server" MaxLength="255" Width="200px" meta:resourcekey="txtSearchByNameResource1"></asp:TextBox>
            <asp:AutoCompleteExtender ID="txtSearchByName_AutoCompleteExtender" runat="server"
                EnableCaching="true" UseContextKey="True" ServicePath="~/AutoComplete.asmx" MinimumPrefixLength="1"
                ServiceMethod="GetClientByName" TargetControlID="txtSearchByName">
            </asp:AutoCompleteExtender>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnSearch" runat="server" Text="Buscar" CssClass="Button" OnClick="btnSearch_Click"
                meta:resourcekey="btnSearchByNameResource1" />
        </td>
    </tr>
</table>
