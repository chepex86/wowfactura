﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="CustomerDetails.aspx.cs"
    Inherits="PACFDManager.Clients.CustomerDetails" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Src="CustomerEditor.ascx" TagName="CustomerEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" id="BookingContainer">
                <tr>
                    <td>
                        <uc1:CustomerEditor ID="ucCustomerEditor" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
