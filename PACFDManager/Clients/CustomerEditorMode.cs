﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Clients
{
    public enum CustomerEditorMode
    {
        Add = 0,
        Details = 1,
        List = 2,
        Modify = 3,
    }
}
