﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Clients
{
    partial class CustomerSearch
    {
        /// <summary>
        /// Class used to group all main information about CustomerSearch class.
        /// </summary>
        public class CustomerSearchInfo
        {
            /// <summary>
            /// Get the CustomerSearch parent class of the object.
            /// </summary>
            public CustomerSearch Parent { get; private set; }
            /// <summary>
            /// 
            /// </summary>
            public string Email
            {
                get { return this.Parent.txtSearchByEmail.Text; }
                set { this.Parent.txtSearchByEmail.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this.Parent.txtSearchByName.Text; }
                set { this.Parent.txtSearchByName.Text = value; }
            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">CustomerSearch parent of the class.</param>
            public CustomerSearchInfo(CustomerSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
