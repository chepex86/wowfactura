﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Clients
{
    public class CustomerSearchEventArgs : EventArgs 
    {
        public string Value { get; private set; }
        public CustomerSearchMode SearchMode { get; private set; }
        public string Name { get; private set; }
        public string UserName { get; private set; }

        public CustomerSearchEventArgs(string value, CustomerSearchMode editmode)
        {
            this.SearchMode = editmode;
            this.Value = value;
        }

        public CustomerSearchEventArgs(string username, string name)
        {
            this.UserName = username;
            this.Name = name;
        }
    }
}
