﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="CustomerModify.aspx.cs"
    Inherits="PACFDManager.Clients.CustomerModify" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Src="CustomerEditor.ascx" TagName="CustomerEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" id="BookingContainer">
                <tr>
                    <td>
                        <uc1:CustomerEditor ID="ucCustomerEditor" runat="server" OnCancelClick="ucCustomerEditor_CancelClick"
                            OnApplyChanges="ucCustomerEditor_ApplyChanges" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
