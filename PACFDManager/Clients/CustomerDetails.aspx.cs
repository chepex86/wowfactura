﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Clients
{
    public partial class CustomerDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
            this.Title = this.GetLocalResourceObject("ClientDetails").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            if (Request.QueryString["ClientID"].IsNull())
                this.Response.Redirect(typeof(CustomerList).Name + ".aspx"); 
            this.LoadData(Convert.ToInt32(Request.QueryString["ClientID"]));
            this.ucCustomerEditor.EditorMode = CustomerEditorMode.Details;
            this.ucCustomerEditor.DetailMode = true;
        }
        /// <summary>
        /// Load customer information.
        /// </summary>
        /// <param name="ClientID">int ClientID</param>
        private void LoadData(int ClientID)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.Clients_GetCustomDataTable ta;
            ta = user.SelectByIDClientsCustom(ClientID);
            ucCustomerEditor.DetailsLoadData(ta[0].ClientID.ToString(), ta[0].UserName, ta[0].Name,
                           (ta[0].Active == true ? this.GetLocalResourceObject("Active").ToString() : 
                            this.GetLocalResourceObject("NotActive").ToString()), ta[0].Role);
        }
    }
}
