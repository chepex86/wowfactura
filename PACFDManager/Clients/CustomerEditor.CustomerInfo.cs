﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Clients
{
    partial class CustomerEditor
    {
        /// <summary>
        /// Class used to group all main information about CustomerEditor class.
        /// </summary>
        public class CustomerEditorInfo
        {
            /// <summary>
            /// Get the CustomerEditor parent class of the object.
            /// </summary>
            public CustomerEditor Parent { get; private set; }
            ///<summary>
            ///
            /// </summary>
            public string Title
            {
                get { return this.Parent.lblTitleOptions.Text; }
                set { this.Parent.lblTitleOptions.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Email
            {
                get { return this.Parent.txtEmail.Text; }
                set { this.Parent.txtEmail.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Password
            {
                get { return this.Parent.txtPassword.Text; }
                set { this.Parent.txtPassword.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this.Parent.txtName.Text; }
                set { this.Parent.txtName.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public bool OptionsVisible
            {
                get { return this.Parent.pnlCustomerOptions.Visible; }
                set { this.Parent.pnlCustomerOptions.Visible = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public bool ListVisible
            {
                get { return this.Parent.pnlCustomerList.Visible; }
                set { this.Parent.pnlCustomerList.Visible = value; }
            }
           
            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">CustomerEditor parent of the class.</param>
            public CustomerEditorInfo(CustomerEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
