﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Clients
{
    public partial class CustomerSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the Search Event Handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void SearchEventHandler(object sender, CustomerSearchEventArgs e);
        /// <summary>
        /// Event handler for the customers Search.
        /// </summary>
        public event SearchEventHandler Search;
        /// <summary>
        /// Get or Set a CustomerSearchMode value with the control mode.
        /// </summary>
        public CustomerSearchMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + (new System.Diagnostics.StackFrame()).GetMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return CustomerSearchMode.Email;

                return o.GetType() == typeof(CustomerSearchMode) ? (CustomerSearchMode)o : CustomerSearchMode.Email;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + (new System.Diagnostics.StackFrame()).GetMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        ///  Fires the PACFDManager.Clients.CustomerSearch.OnSearch event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(CustomerSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerSearch.Search_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            CustomerSearchEventArgs a;
            a = new CustomerSearchEventArgs((this.chkboxByEmail.Checked ? this.txtSearchByEmail.Text.Trim() : String.Empty),
                                            (this.chkboxByName.Checked ? this.txtSearchByName.Text.Trim() : String.Empty));
            this.OnSearch(a);
        }
    }
}