﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Clients
{
    public partial class CustomerList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            this.Title = this.GetLocalResourceObject("ClientList").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            ucCustomerEditor.EditorMode = CustomerEditorMode.List;
            this.ucCustomerEditor.ListMode = true;
            this.LoadList();
        }
        /// <summary>
        /// Load the customer list.
        /// </summary>
        private void LoadList()
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            this.ucCustomerEditor.FillGrid(user.SelectClientBySearching(String.Empty, String.Empty));
        }
    }
}
