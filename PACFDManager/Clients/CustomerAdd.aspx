﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="CustomerAdd.aspx.cs"
    Inherits="PACFDManager.Clients.NewCustomer" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Src="CustomerEditor.ascx" TagName="CustomerEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" id="BookingContainer">
                <tr>
                    <td>
                        <uc1:CustomerEditor ID="CustomerEditor1" runat="server" OnCancelClick="CustomerEditor1_CancelClick"
                            OnApplyChanges="CustomerEditor1_ApplyChanges" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
