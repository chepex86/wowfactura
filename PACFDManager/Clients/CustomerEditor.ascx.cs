﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.Data;
#endregion

namespace PACFDManager.Clients
{
    public partial class CustomerEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the control Apply Changes.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void ApplychangesEventHandler(object sender, CustomerEditorEventArgs e);
        /// <summary>
        /// Event handler for the control Apply Changes.
        /// </summary>
        public event ApplychangesEventHandler ApplyChanges;
        /// <summary>
        /// Event handler for the control Cancel Changes.
        /// </summary>
        public event EventHandler CancelClick;
        /// <summary>
        /// Get a CustomerEditorInfo with all main information.
        /// </summary>
        public CustomerEditorInfo CustomerInformation { get; private set; }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public CustomerEditor()
        {
            this.CustomerInformation = this.OngenerateCustomerInformation();
        }
        /// <summary>
        /// Get or Set a CustomerEditorMode value with the control mode.
        /// </summary>
        public CustomerEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + (new System.Diagnostics.StackFrame()).GetMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return CustomerEditorMode.Add;

                return o.GetType() == typeof(CustomerEditorMode) ? (CustomerEditorMode)o : CustomerEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + (new System.Diagnostics.StackFrame()).GetMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ucCustomerSearch.Search += new CustomerSearch.SearchEventHandler(ucCustomerSearch_Search);
        }
        /// <summary>
        /// Event fired by the delegate of the user control ucCustomerSearch.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucCustomerSearch_Search(object sender, CustomerSearchEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            this.gvCustomerList.DataSource = user.SelectClientBySearching(e.UserName, e.Name);
            this.gvCustomerList.DataBind();

            if (this.gvCustomerList.Rows.Count < 1)
            {
                DataTable dt = new UsersDataSet.Clients_GetBySearchingDataTable();
                DataRow dr = dt.NewRow();
                dr["Active"] = false;
                dr["Name"] = String.Empty;
                dr["UserName"] = String.Empty;
                dr["Password"] = String.Empty;
                dr["UserID"] = -1;
                dr["Role"] = -1;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvCustomerList.DataSource = dt;
                this.gvCustomerList.DataBind();
            }
        }
        /// <summary>
        /// Brings all the corresponding rows of the database to fill the list of customers.
        /// </summary>
        /// <param name="table">UsersDataSet.Clients_GetBySearchingDataTable table</param>
        public void FillGrid(UsersDataSet.Clients_GetBySearchingDataTable table)
        {
            this.gvCustomerList.DataSource = table;
            this.gvCustomerList.DataMember = table.TableName;
            this.gvCustomerList.DataBind();
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ApplyChanges event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnApplyChanges(CustomerEditorEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.CancelClik event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.btnAccept_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.OnApplyChanges(this.OnGenerateAcceptArguments());
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.btnCancel_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
        /// <summary>
        /// Generate a new instance of the class of type CustomerInfo to control the fields of the control.
        /// </summary>
        /// <returns>Returns a CustomerInfo class.</returns>
        protected virtual CustomerEditorInfo OngenerateCustomerInformation()
        {
            return new CustomerEditorInfo(this);
        }
        /// <summary>
        /// Generate a new instance of the class CustomerEditorEventArgs.
        /// </summary>
        /// <returns>
        /// Returns a new instance of CustomerEditorEventArgs, 
        /// the parameters entered into the editor of customers.
        /// </returns>
        protected CustomerEditorEventArgs OnGenerateAcceptArguments()
        {
            return new CustomerEditorEventArgs(txtEmail.Text, txtPassword.Text, txtName.Text,
                                               Convert.ToInt32(this.ddlUserLevel.SelectedValue), this.EditorMode);
        }
        /// <summary>
        /// Load data for editor in edit mode.
        /// </summary>
        /// <param name="email">string email</param>
        /// <param name="password">string password</param>
        /// <param name="name">string name</param>
        public void ModifyLoadData(string email, string password, string name, int userlevel)
        {
            this.txtEmail.Text = email;
            this.txtName.Text = name;
            this.txtPassword.Text = password;
            this.txtPasswordConfirm.Text = password;
            this.ddlUserLevel.SelectedIndex = (userlevel - 2);
        }
        /// <summary>
        /// Load data for editor in detail mode.
        /// </summary>
        /// <param name="ClientID">string ClientID</param>
        /// <param name="email">string email</param>
        /// <param name="name">string name</param>
        /// <param name="state">string state</param>
        public void DetailsLoadData(string ClientID, string email, string name, string state, int userlevel)
        {
            this.txtEmail.Text = ClientID;
            this.txtPassword.Text = email;
            this.txtPasswordConfirm.Text = name;
            this.txtName.Text = state;
            this.ddlUserLevel.SelectedIndex = (userlevel - 2);
        }
        /// <summary>
        /// Change the default value depending on the cell that contains for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvCustomerList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowIndex > -1)
            {
                row.Cells[3].Text = (row.Cells[3].Text == "True" ? this.GetLocalResourceObject("Active").ToString() :
                                                                   this.GetLocalResourceObject("NotActive").ToString());

                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int i = 0; i < row.Cells.Count; i += 1)
                        row.Cells[i].Visible = false;
                    ViewState["Empty"] = null;
                }
            }
        }
        /// <summary>
        /// Redirects to the customer detail page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lbtnDetail_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            Response.Redirect("~/Clients/CustomerDetails.aspx?ClientID=" + s.CommandArgument);
        }
        /// <summary>
        /// Redirects to the customer editing page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lbtnEdit_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            Response.Redirect("~/Clients/CustomerModify.aspx?ClientID=" + s.CommandArgument);
        }
        /// <summary>
        /// Send a confirmation message.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;

            this.WebMessageBox1.Title = this.GetLocalResourceObject("DeleteClient").ToString();
            this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Question").ToString(),
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["UserID"] = s.CommandArgument.Split('_')[1];
        }
        /// <summary>
        /// If you accept the delete confirmation to the selected customer, otherwise returns to the list of customers.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            user.SelectByIDUsers(ds.Users, Convert.ToInt32(ViewState["UserID"]));
            if (ds.Users.Count > 0)
            {
                UsersDataSet.UsersRow drUsers = ds.Users[0];
                drUsers.Active = false;
            }
            if (!user.UpdateUsers(ds.Users))
            {
                LogManager.WriteStackTrace(new Exception("Error al tratar de eliminar Cliente"));
                this.WebMessageBox1.Title = this.GetLocalResourceObject("DeleteClient").ToString();
                WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Error").ToString(), System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }
            this.ViewState["UserID"] = null;
            LogManager.WriteStackTrace(new Exception("Éxito al Eliminar Cliente"));
            this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
        }
        /// <summary>
        /// Configurations for the editor.
        /// </summary>
        #region Properties
        /// <summary>
        /// Sets value the visible property of the options: Add, Details, Modify or Edit.
        /// </summary>
        public bool VisibleOptions
        {
            set
            {
                this.pnlCustomerOptions.Visible = value;
            }
        }
        /// <summary>
        /// Sets value the visible property of the listing.
        /// </summary>
        public bool VisibleList
        {
            set
            {
                this.pnlCustomerList.Visible = value;
            }
        }
        /// <summary>
        /// Disable the validators of the Password field.
        /// </summary>
        public bool DisableValidators
        {
            set
            {
                this.rfvPassword.Enabled =
                this.rfvPasswordConfirm.Enabled = value;
            }
        }
        /// <summary>
        /// Disable all validators of the editor.
        /// </summary>
        private bool DisableAllValidators
        {
            set
            {
                this.cvPasswordConfirm.Enabled =
                this.revEmail.Enabled =
                this.rfvEmail.Enabled =
                this.rfvName.Enabled =
                this.rfvPassword.Enabled =
                this.rfvPasswordConfirm.Enabled = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Detail.
        /// </summary>
        public bool DetailMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("ClientDetail").ToString();
                this.btnAccept.Text = this.GetLocalResourceObject("Return").ToString();
                this.lblEmail.Text = this.GetLocalResourceObject("ClientID").ToString();
                this.lblPassword.Text = this.GetLocalResourceObject("Email").ToString();
                this.lblPasswordConfirm.Text = this.GetLocalResourceObject("Name").ToString();
                this.lblName.Text = this.GetLocalResourceObject("State").ToString();
                this.txtPassword.TextMode = TextBoxMode.SingleLine;
                this.txtPasswordConfirm.TextMode = TextBoxMode.SingleLine;
                this.txtEmail.Enabled =
                this.txtPassword.Enabled =
                this.txtPasswordConfirm.Enabled =
                this.txtName.Enabled =
                this.ddlUserLevel.Enabled =
                this.DisableAllValidators =
                this.btnCancel.Visible = false;
                this.VisibleOptions = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Add.
        /// </summary>
        public bool AddMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("AddNewClient").ToString();
                this.btnAccept.Text = this.GetLocalResourceObject("Add").ToString();
                this.VisibleOptions = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Edit.
        /// </summary>
        public bool ModifyMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("ModifyClient").ToString();
                this.btnAccept.Text = this.GetLocalResourceObject("Modify").ToString();
                this.VisibleOptions = value;
                this.DisableValidators = false;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: List.
        /// </summary>
        public bool ListMode
        {
            set
            {
                this.VisibleList = value;
            }
        }
        #endregion
    }
}