﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Clients
{
    public partial class CustomerModify : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            if (Request.QueryString["ClientID"].IsNull())
                return;
            ViewState["ClientID"] = Request.QueryString["ClientID"];

            this.Title = this.GetLocalResourceObject("ModifyClient").ToString();
            this.CustomizeEditor();
            this.LoadData(Convert.ToInt32(ViewState["ClientID"]));
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucCustomerEditor_ApplyChanges(object sender, CustomerEditorEventArgs e)
        {
            if (e.EditorMode != CustomerEditorMode.Modify) return;
            else this.Modify(e);
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucCustomerEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
        }
        /// <summary>
        /// Load customer information.
        /// </summary>
        /// <param name="ClientID">Client ID</param>
        private void LoadData(int ClientID)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.Clients_GetCustomDataTable ta;
            ta = user.SelectByIDClientsCustom(ClientID);
            ucCustomerEditor.ModifyLoadData(ta[0].UserName, ta[0].Password, ta[0].Name, ta[0].Role);
        }
        /// <summary>
        /// Modify the customer information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Modify(CustomerEditorEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();

            user.SelectByIDUsers(ds.Users, Convert.ToInt32(ViewState["ClientID"]));
            if (ds.Users.Count > 0)
            {
                UsersDataSet.UsersRow drUsers = ds.Users[0];
                drUsers.UserName = e.Email;
                if(e.Password != string.Empty)
                    drUsers.Password = Cryptography.EncryptUnivisitString(e.Password);
                drUsers.Role = e.UserLevel;
            }

            user.SelectByIDClients(ds.Clients, Convert.ToInt32(ViewState["ClientID"]));
            if (ds.Clients.Count > 0)
            {
                UsersDataSet.ClientsRow drClient = ds.Clients[0];
                drClient.Name = e.Name;
            }

            if (!user.UpdateClients(ds))
                return false;

            LogManager.WriteStackTrace(new Exception("Éxito al Modificar Cliente."));
            this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
            return true;
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucCustomerEditor.EditorMode = CustomerEditorMode.Modify;
            this.ucCustomerEditor.ModifyMode = true;
        }
    }
}