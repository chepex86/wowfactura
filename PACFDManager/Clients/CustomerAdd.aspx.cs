﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Clients
{
    public partial class NewCustomer : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            this.Title = this.GetLocalResourceObject("AddClient").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void CustomerEditor1_ApplyChanges(object sender, CustomerEditorEventArgs e)
        {
            if (e.EditorMode != CustomerEditorMode.Add) return;
            else Add(e);
        }
        /// <summary>
        /// Fires the PACFDManager.Clients.CustomerEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void CustomerEditor1_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
        }
        /// <summary>
        /// Inserts a new row with the customer information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Add(CustomerEditorEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.UsersRow drUser = ds.Users.NewUsersRow();

            drUser.UserName = e.Email;
            drUser.Password = Cryptography.EncryptUnivisitString(e.Password);
            drUser.Active = true;
            drUser.Role = e.UserLevel;

            ds.Users.AddUsersRow(drUser);

            UsersDataSet.ClientsRow drClient = ds.Clients.NewClientsRow();
            drClient.Name = e.Name;
            drClient.SetParentRow(drUser);
            ds.Clients.AddClientsRow(drClient);

            if (!user.UpdateClients(ds))
                return false;

            LogManager.WriteStackTrace(new Exception("Éxito al Agregar Nuevo Cliente."));
            this.Response.Redirect(typeof(CustomerList).Name + ".aspx");
            return true;
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.CustomerEditor1.EditorMode = CustomerEditorMode.Add;
            this.CustomerEditor1.AddMode = true;
        }
    }
}
