﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerEditor.ascx.cs"
    Inherits="PACFDManager.Clients.CustomerEditor" %>
<%@ Register Src="CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<style type="text/css">
    .style1
    {
        height: 23px;
    }
</style>
<table align="center">
    <tr>
        <td class="style1" align="center">
            <asp:Panel ID="pnlCustomerOptions" runat="server" Visible="False">
                <table>
                    <tr>
                        <td align="center" colspan="2" class="Titulo">
                            <asp:Label ID="lblTitleOptions" runat="server" Text="Titulo"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblEmail" runat="server" Text="Correo:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                ErrorMessage="Correo Incorrecto" ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                                ValidationGroup="Validators" Display="Dynamic"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblPassword" runat="server" Text="Contraseña:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblPasswordConfirm" runat="server" Text="Confirmar Contraseña:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="50" Width="200px"
                                TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" ControlToValidate="txtPasswordConfirm"
                                ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvPasswordConfirm" runat="server" ControlToCompare="txtPassword"
                                ControlToValidate="txtPasswordConfirm" ErrorMessage="Contraseña diferente" ValidationGroup="Validators"
                                Display="Dynamic"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblName" runat="server" Text="Nombre:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server" MaxLength="255" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblUserLevel" runat="server" Text="Nivel de Usuario:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlUserLevel" runat="server">
                                <asp:ListItem Value="2">Avanzado</asp:ListItem>
                                <asp:ListItem Value="3">Medio</asp:ListItem>
                                <asp:ListItem Value="4">Principiante</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click"
                                            ValidationGroup="Validators" />
                                        &nbsp;
                                        <asp:Button ID="btnCancel" runat="server" CssClass="Button" OnClick="btnCancel_Click"
                                            Text="Cancelar" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Panel ID="pnlCustomerList" runat="server" Visible="False">
                <table>
                    <tr>
                        <td align="center" class="Titulo">
                            <asp:Label ID="lblTitleManager" runat="server" Text="Manejador de Clientes"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:CustomerSearch ID="ucCustomerSearch" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Titulo">
                            <asp:Label ID="lblTitleList" runat="server" Text="Listado de Clientes"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnAddCustomerTop" runat="server" CssClass="Button" PostBackUrl="~/Clients/CustomerAdd.aspx"
                                Text="Agregar Cliente" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvCustomerList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" CssClass="DataGrid" OnRowDataBound="gvCustomerList_RowDataBound"
                                Font-Size="9pt" HorizontalAlign="Center">
                                <RowStyle BackColor="#EFF3FB" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="ClientID" HeaderText="ID" />
                                    <asp:BoundField DataField="UserName" HeaderText="Correo" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left"  />
                                    <asp:BoundField DataField="Active" HeaderText="Estado" />
                                    <asp:TemplateField HeaderText="Detalles">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lbtnDetail" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ClientID") %>'
                                                ImageUrl="~/Includes/Images/png/kfind.png" OnClick="lbtnDetail_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Editar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ClientID") %>'
                                                ImageUrl="~/Includes/Images/png/editIcon.png" OnClick="lbtnEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Eliminar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ClientID") + "_" + Eval("UserID") %>'
                                                ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="lbtnDelete_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnAddCustomerBottom" runat="server" Text="Agregar Cliente" PostBackUrl="~/Clients/CustomerAdd.aspx"
                                CssClass="Button" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td align="center">
            <uc2:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
        </td>
    </tr>
</table>
