﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Clients
{
    public class CustomerEditorEventArgs
    {
        public CustomerEditorMode EditorMode { get; private set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int UserLevel { get; set; }

        public CustomerEditorEventArgs(string email, string password, string name, int userlevel, CustomerEditorMode editorMode)
        {
            this.Email = email;
            this.Password = password;
            this.Name = name;
            this.EditorMode = editorMode;
            this.UserLevel = userlevel;
        }
    }
}
