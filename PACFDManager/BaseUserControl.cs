﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager
{
    public class BaseUserControl : System.Web.UI.UserControl
    {
        public PACFD.Rules.ElectronicBillingType CurrentElectronicBillingType { get { return Security.Security.CurrentElectronicBillingType; } }
        public string UserName { get { return Security.Security.UserName; } }
        public int UserID { get { return Security.Security.UserID; } }
        public bool IsAdministrator { get { return Security.Security.IsAdministrator; } }
        public bool IsSAT { get { return Security.Security.IsSAT; } }
        public bool IsAdvancedClient { get { return Security.Security.IsAdvancedClient; } }
        public bool IsBasicClient { get { return Security.Security.IsBasicClient; } }
        public int CurrentGroupID { get { return Security.Security.CurrentGroupID; } }
        public string CurrentGroupName { get { return Security.Security.CurrentGroupName; } }
        public int CurrentBillerID
        {
            get
            {
                object o = HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")];
                return o.IsNull() || o.GetType() != typeof(int) ? -1 : (int)o;
            }
        }
        public string CurrentBillerName { get { return Security.Security.CurrentBillerName; } }
        public int CurrentBranchID { get { return Security.Security.CurrentBranchID; } }
        public String CurrentBranchName { get { return Security.Security.CurrentBranchName; } }

        public bool HaveBranch { get { return Security.Security.HaveBranch; } }

        public static bool CFDEnable { get { return Security.Security.CFDEnable; } }
        public static bool CBBEnable { get { return Security.Security.CBBEnable; } }
        public static bool CFDIEnable { get { return Security.Security.CFDIEnable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFD 2.2 is enabled.
        /// </summary>
        public static bool CFD2_2Enable { get { return Security.Security.CFD2_2Enable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.2 is enabled.
        /// </summary>
        public static bool CFDI3_2Enable { get { return Security.Security.CFDI3_2Enable; } }

        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.3 is enabled.
        /// </summary>
        public static bool CFDI3_3Enable { get { return Security.Security.CFDI3_3Enable; } }

        /// <summary>
        /// Get a boolean value indicating if the CFDI 4.0 is enabled.
        /// </summary>
        public static bool CFDI4_0Enable { get { return Security.Security.CFDI4_0Enable; } }
        public Decimal CurrentExchangeRate { get { return Security.Security.CurrentExchangeRate; } }
    }
}