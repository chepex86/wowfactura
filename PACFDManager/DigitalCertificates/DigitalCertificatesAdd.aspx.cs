﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Security.Cryptography.X509Certificates;
#endregion

namespace PACFDManager.DigitalCertificates
{
    public partial class DigitalCertificatesAdd : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = this.GetLocalResourceObject("AddDigitalCertificate").ToString();
            this.lblError.Visible = false;
        }
        /// <summary>
        /// Fires the PACFDManager.DigitalCertificates.DigitalCertificatesAdd.ApplyChanges.WebMessageBox.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Accept)
                this.Response.Redirect(typeof(DigitalCertificatesList).Name + ".aspx");
        }
        /// <summary>
        /// Stored in the database of digital certificates.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        private bool LoadData()
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet ds = new DigitalCertificatesDataSet();
            DigitalCertificatesDataSet.DigitalCertificatesRow drCert = ds.DigitalCertificates.NewDigitalCertificatesRow();

            String e = String.Empty;
            String k = e, c = e, n = e, Pem = e, BillerRFC = e;
            String FileNameCer = e, FileNameKey = e;
            DateTime[] dt = new DateTime[2];
            ErrorsArgs er;
            int error = -1;

            try
            {
                BillerRFC = this.GetBillerRFC();

                c = this.FileUpload(this.fuCertificate);
                k = this.FileUpload(this.fuPrivateKey);

                FileNameCer = this.fuCertificate.FileName;
                FileNameKey = this.fuPrivateKey.FileName;

                String pass = this.txtPublicKey.Text.Trim();

                if (!FileNameCer.Trim().ToLower().Contains(".cer")) 
                    return this.ErrorManager(er = new ErrorsArgs(Errors.ExtCer, FileNameCer));

                if (!FileNameKey.Trim().ToLower().Contains(".key"))
                    return this.ErrorManager(er = new ErrorsArgs(Errors.ExtKey, FileNameKey));

                if (!certificate.ValidateRFC(c))
                    return this.ErrorManager(er = new ErrorsArgs(Errors.RFC));
                try
                {
                    error = PACFD.Rules.DigitalCertificates.ValidatePrivateKeyAndIsCouple(c, k, pass);
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                    LogManager.WriteError(ex);

                    return this.ErrorManager(er = new ErrorsArgs(Errors.CCP));
                }

                switch (error)
                {
                    case 1:                      
                        return this.ErrorManager(er = new ErrorsArgs(Errors.PrivateKey, FileNameKey));
                    case 2:
                        return this.ErrorManager(er = new ErrorsArgs(Errors.CerKey, FileNameKey, FileNameCer));
                    case 99:
                        return this.ErrorManager(er = new ErrorsArgs(Errors.NPI));
                }

                drCert.BillerID = this.CurrentBillerID;
                drCert.Certificate = c;
                drCert.CertificateName = FileNameCer.Trim();

                drCert.PrivtaeKey = k;
                drCert.PrivateKeyName = FileNameKey.Trim();
                drCert.Ccp = Cryptography.EncryptUnivisitString(this.txtPublicKey.Text.Trim());
                drCert.Seal = String.Empty;
                drCert.Status = 0;

                try
                {
                    dt = this.GetDate(Convert.FromBase64String(c));
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                    LogManager.WriteError(ex);

                    return this.ErrorManager(er = new ErrorsArgs(Errors.Certificate, FileNameCer));
                }

                n = GetCertificateNumber(Convert.FromBase64String(c));

                if (n.Length != 20)
                    return this.ErrorManager(er = new ErrorsArgs(Errors.CerNumber, FileNameCer));

                drCert.CertificateNumber = n;
                drCert.CertificateValidityBegin = dt[0];
                drCert.CertificateValidityEnd = dt[1];

                ds.DigitalCertificates.AddDigitalCertificatesRow(drCert);

                certificate.Update(ds.DigitalCertificates);

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo certificado digital.",
                    this.UserName), this.CurrentBillerID, null, ds.GetXml());
                #endregion

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
        /// <summary>
        /// Finds the RFC the current biller. 
        /// </summary>
        /// <returns>Returns RFC of the current biller.</returns>
        private string GetBillerRFC()
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.BillersDataTable dt = new BillersDataSet.BillersDataTable();

            dt = biller.SelectByID(this.CurrentBillerID);

            return dt.Count < 1 ? String.Empty : dt[0].RFC;
        }
        /// <summary>
        /// Converts a file to an array of bytes and then converted to string.
        /// </summary>
        /// <param name="fu">FileUpload file</param>
        /// <returns>Returns the file submitted become character string or null depending on the outcome of the operation.</returns>
        private String FileUpload(FileUpload fu)
        {
            int size;
            byte[] buffer;

            if (fu.PostedFile.IsNull() && fu.FileName == String.Empty)
                return null;

            if (fu.PostedFile.ContentLength < 1)
                return null;

            if (fu.PostedFile.InputStream.Length > int.MaxValue)
                return null;

            int.TryParse(fu.PostedFile.InputStream.Length.ToString(), out size);

            if (size == 0)
                return null;

            buffer = new byte[size];

            fu.PostedFile.InputStream.Read(buffer, 0, size);

            return Convert.ToBase64String(buffer);
        }
        /// <summary>
        /// Gets the valid date range of a digital certificate.
        /// </summary>
        /// <param name="buffer">byte[] buffer</param>
        /// <returns>DateTime[]</returns>
        private DateTime[] GetDate(byte[] buffer)
        {
            DateTime[] dt = new DateTime[2];
            X509Certificate2 m_cer = new X509Certificate2(buffer);

            if (!m_cer.IsNull())
            {
                try
                {
                    dt[0] = m_cer.NotBefore;
                    dt[1] = m_cer.NotAfter;

                    return dt;
                }
                catch (Exception ex)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                    LogManager.WriteError(ex);
                    return null;
                }
            }
            return null;
        }
        /// <summary>
        /// Get the number of the certificate in a file [*.cer] valid.
        /// </summary>
        /// <param name="buffer">byte[] buffer</param>
        /// <returns>Returns the number of the certificate as a string of length 20.</returns>
        private string GetCertificateNumber(byte[] buffer)
        {
            try
            {
                X509Certificate2 m_cer = new X509Certificate2(buffer);

                if (!m_cer.IsNull())
                {
                    String dHex = m_cer.SerialNumber;
                    String aux = String.Empty;
                    String result = String.Empty;

                    while (dHex.Length > 0)
                    {
                        aux = System.Convert.ToChar(System.Convert.ToUInt32(dHex.Substring(0, 2), 16)).ToString();
                        result += aux;
                        dHex = dHex.Substring(2, dHex.Length - 2);
                    }
                    return result;
                }
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return String.Empty;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.DigitalCertificates.DigitalCertificatesAdd.btnLoad_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (!this.LoadData())
                return;

            this.WebMessageBox1.Title = "Agregar Certificado Digital";
            this.WebMessageBox1.ShowMessage("El Certificado ha sido agregado exitosamente.<br /><br />Para poder expedir comprobantes debe activar un certificado del Listado de Certificados Digitales.",
                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }
        /// <summary>
        /// Displays an error message on screen.
        /// </summary>
        /// <param name="message">String Error Message.</param>
        /// <returns>Always returns false</returns>
        private bool ErrorMessage(String message)
        {
            this.lblError.Text = String.Empty;
            this.lblError.Text = message;
            this.lblError.Visible = true;
            return false;
        }
        /// <summary>
        /// Assigns the corresponding error message
        /// </summary>
        /// <param name="e">ErrorsArgs args</param>
        /// <returns>Always returns false</returns>
        private bool ErrorManager(ErrorsArgs args)
        {
            String e = String.Empty;

            switch (args.Error)
            {
                case Errors.CerNumber:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El archivo [" + args.Certificate + "] no es válido ó está dañado ya que.<br/>" +
                        "no ha sido posible leer el numero de certificado ó no tiene una longitud valida.";
                    break;
                case Errors.PrivateKey:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El archivo [" + args.PrivateKey + "] no es válido ó está dañado.<br/>" +
                        "- Verifique también que la llave pública sea correcta.";
                    break;
                case Errors.Certificate:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El archivo [" + args.Certificate + "] no es válido ó está dañado.<br/>";
                    break;
                case Errors.CerKey:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El certificado [" + args.Certificate + "] no tiene relación con<br/>" +
                        "la llave privada [" + args.PrivateKey + "].";
                    break;
                case Errors.ExtCer:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El archivo [" + args.Certificate + "] no es de tipo [*.cer] ó está dañado.<br/>";
                    break;
                case Errors.ExtKey:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                        "- El archivo [" + args.PrivateKey + "] no es de tipo [*.key] ó está dañado.<br/>";
                    break;
                case Errors.NPI:
                    e = "Error: NPI-99";
                    break;
                case Errors.Path:
                    e = "Ocurrio un problema en el proceso de cargar los archivos.";
                    break;
                case Errors.RFC:
                    e = "El RFC del certificado digital no contiene un formato valido.";
                    break;
                case Errors.CCP:
                    e = "No ha sido posible cargar los archivos debido a que:<br/>" + 
                        "- El cpp no es valido.";
                    break;
                default:
                    break;
            }

            return this.ErrorMessage(e);
        }
    }
    
    public enum Errors
    {
        CerNumber = 0,
        PrivateKey = 1,
        Certificate = 2,
        CerKey = 3,
        ExtCer = 4,
        ExtKey = 5,
        Path = 6,
        RFC = 7,
        CCP = 8,
        NPI = 99,
    }
  
    public class ErrorsArgs
    {
        public Errors Error { get; private set; }
        public String PrivateKey { get; private set; }
        public String Certificate { get; private set; }

        public ErrorsArgs(Errors error, String privateKey, String certificate)
        {
            this.Error = error;
            this.PrivateKey = privateKey;
            this.Certificate = certificate;
        }
        public ErrorsArgs(Errors error, String value)
        {
            this.Error = error;
            this.PrivateKey = value;
            this.Certificate = value;
        }
        public ErrorsArgs(Errors error)
        {
            this.Error = error;
        }
    }
}