﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DigitalCertificatesEditor.ascx.cs"
    Inherits="PACFDManager.DigitalCertificates.DigitalCertificatesEditor" %>
<div class="info">
    <h2>
        <asp:Label ID="lblTitleDetail" runat="server" Text="Datos de la Empresa" />
    </h2>


<asp:Label ID="lblTitleRFC" runat="server" Text="R.F.C.:" Font-Bold="True" meta:resourcekey="lblTitleRFCResource1" />
<asp:Label ID="lblRFC" runat="server" meta:resourcekey="lblRFCResource1" /><br/>
<asp:Label ID="lblTitleSocialReason" runat="server" Text="Razón Social:" Font-Bold="True" meta:resourcekey="lblTitleSocialReasonResource1" />
<asp:Label ID="lblSocialReason" runat="server" meta:resourcekey="lblSocialReasonResource1" /><br/>
<asp:Label ID="lblTitleAddress" runat="server" Text="Dirección:" Font-Bold="True" meta:resourcekey="lblTitleAddressResource1" />
<asp:Label ID="lblAddress" runat="server" meta:resourcekey="lblAddressResource1" />

</div>
