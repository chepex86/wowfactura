﻿<%@ Page Title="Agregar Certificados Digitales" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="DigitalCertificatesAdd.aspx.cs" Inherits="PACFDManager.DigitalCertificates.DigitalCertificatesAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="DigitalCertificatesEditor.ascx" TagName="DigitalCertificatesEditor"
    TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:DigitalCertificatesEditor ID="DigitalCertificatesEditor1" runat="server" />
            <br />
            <div id="container">
                <div class="form">
                    <div class="formStyles">
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblUploadCertificate" runat="server" Text="Certificado:" /></label>
                                <span class="vtip" title="Seleccionar la ruta donde se<br />encuentra el certificado digital <img src='../Includes/Images/png/certificate.png' alt='certificado'/><br />(el archivo con extensión <b>*.cer</b>)">
                                    <asp:FileUpload ID="fuCertificate" runat="server" />
                                </span>
                                <div class="validator-msg" style="width: 250px">
                                    <asp:RequiredFieldValidator ID="rfvCertificate" runat="server" ControlToValidate="fuCertificate"
                                        Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblPrivateKey" runat="server" Text="Llave Pública:" />
                                </label>
                                <span class="vtip" title="Seleccionar la ruta donde se<br />encuentra la llave pública <img src='../Includes/Images/png/certificate-key.png' alt='llave privada'/><br />(el archivo con extensión <b>*.key</b>)">
                                    <asp:FileUpload ID="fuPrivateKey" runat="server" />
                                </span>
                                <div class="validator-msg" style="width: 250px">
                                    <asp:RequiredFieldValidator ID="rfvPrivateKey" runat="server" ControlToValidate="fuPrivateKey"
                                        Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblPublicKey" runat="server" Text="CCP:" /></label>
                                <span class="vtip" title="Introducir el CCP.<br />ej: <b>a0123456789</b>">
                                    <asp:TextBox ID="txtPublicKey" runat="server" MaxLength="20" /></span>
                                <div class="validator-msg" style="width: 250px">
                                    <asp:RequiredFieldValidator ID="rfvPublicKey" runat="server" ControlToValidate="txtPublicKey"
                                        Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                            </li>
                            <li class="buttons clear">
                                <div>
                                    <asp:Button ID="btnLoad" runat="server" CssClass="Button" OnClick="btnLoad_Click"
                                        Text="Aceptar" ValidationGroup="Validators" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="Button" meta:resourcekey="btnCancelResource1"
                                        PostBackUrl="~/DigitalCertificates/DigitalCertificatesList.aspx" Text="Cancelar" /></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <uc3:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoad" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
