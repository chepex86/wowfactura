﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.DigitalCertificates
{
    public partial class DigitalCertificatesEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadData();
        }
        /// <summary>
        /// Makes a search of the current biller information.
        /// </summary>
        private void LoadData()
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet ds = new BillersDataSet();
            BillersDataSet.BillersDataTable ta;
            ta = biller.SelectByID(this.CurrentBillerID);
            this.FillInfo(ta);
        }
        /// <summary>
        /// Displays biller information received in a datatable.
        /// </summary>
        /// <param name="ta">BillersDataSet.BillersDataTable table</param>
        private void FillInfo(BillersDataSet.BillersDataTable ta)
        {
            this.lblRFC.Text = ta[0].RFC;
            this.lblSocialReason.Text = ta[0].Name;
            this.lblAddress.Text = ta[0].Address + ", " + ta[0].Colony;
        }
    }
}