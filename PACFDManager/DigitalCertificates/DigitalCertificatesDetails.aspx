﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DigitalCertificatesDetails.aspx.cs"
    Inherits="PACFDManager.DigitalCertificates.DigitalCertificatesDetails" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer">
                <tr>
                    <td align="center">
                        <div id="container">
                            <div class="form">
                                <div class="formStyles">
                                    <div class="info">
                                        <h2>
                                            <asp:Label ID="lblTitleDetails" runat="server" Text="Detalle del Certificado Digital"></asp:Label>
                                        </h2>
                                    </div>
                                    <div>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleCertificateName" runat="server" Font-Bold="True" Font-Italic="False"
                                                        Text="Certificado:" meta:resourcekey="lblTitleCertificateNameResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCertificateName" runat="server" meta:resourcekey="lblCertificateNameResource1"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:Label ID="lblTitlePrivateKeyName" runat="server" Font-Bold="True" Text="Llave Privada:"
                                                        meta:resourcekey="lblTitlePrivateKeyNameResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblPrivateKeyName" runat="server" meta:resourcekey="lblPrivateKeyNameResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleCertificateValidityBegin" runat="server" Font-Bold="True"
                                                        Text="Valido desde:" meta:resourcekey="lblTitleCertificateValidityBeginResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCertificateValidityBegin" runat="server" meta:resourcekey="lblCertificateValidityBeginResource1"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleCertificateValidityEnd" runat="server" Font-Bold="True" Text="Hasta:"
                                                        meta:resourcekey="lblTitleCertificateValidityEndResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCertificateValidityEnd" runat="server" meta:resourcekey="lblCertificateValidityEndResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleStatus" runat="server" Font-Bold="True" Text="Estado:" meta:resourcekey="lblTitleStatusResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblStatus" runat="server" meta:resourcekey="lblStatusResource1"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleCcp" runat="server" Font-Bold="True" Text="Contraseña Llave Privada:"
                                                        meta:resourcekey="lblTitleCcpResource1"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCcp" runat="server" meta:resourcekey="lblCcpResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="lblTitleCertificateNumber" runat="server" Font-Bold="True" Text="Número de Certificado:"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCertificateNumber" runat="server"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    &nbsp;
                                                </td>
                                                <td align="left">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="Titulo" colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblDownloadCertificate" runat="server" Text="Descargar Certificado"
                                                        meta:resourcekey="lblDownloadCertificateResource1"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblDownloadKey" runat="server" Text="Descargar Llave Privada" meta:resourcekey="lblDownloadKeyResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <span class="vtip" title="Descargar el certificado<br /><img src='../Includes/Images/png/certificate.png' alt='Certificado' />">
                                                        <asp:ImageButton ID="ImgBtnCer" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                                            OnClick="ImgBtnCer_Click" meta:resourcekey="ImgBtnCerResource1" /></span>
                                                </td>
                                                <td align="center"><span class="vtip" title="Descargar la llave privada<br /><img src='../Includes/Images/png/certificate-key.png' alt='Llave Privada' />">
                                                    <asp:ImageButton ID="ImgBtnKey" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                                        OnClick="ImgBtnKey_Click" meta:resourcekey="ImgBtnKeyResource1" /></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <br />
                                                    <asp:Button ID="btnReturn" runat="server" CssClass="Button" PostBackUrl="~/DigitalCertificates/DigitalCertificatesList.aspx"
                                                        Text="Aceptar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
