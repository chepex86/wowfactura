﻿#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.DigitalCertificates
{
    public partial class DownloadFiles : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (this.LoadFile())
                    Response.Redirect(Request.UrlReferrer.ToString());
        }
        /// <summary>
        /// Generates the file to download, based on the arguments received by QueryString:
        /// DigitalCertificateID & FileType.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        protected bool LoadFile()
        {
            if (Request.QueryString["DigitalCertificateID"].IsNull() || Request.QueryString["FileType"].IsNull())
                return false;

            Byte[] file = SeekFile(this.CurrentBillerID, 
                                   Convert.ToInt32(Request.QueryString["DigitalCertificateID"]), 
                                   Convert.ToInt32(Request.QueryString["FileType"]));
            if (file == null)
                return false;

            Response.AddHeader("Content-disposition", "attachment; filename=" + ViewState["Name"].ToString());
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(file);
            Response.End();
            ViewState["Name"] = null;

            return true;
        }
        /// <summary>
        /// Finds the string that is need to generate the file and converts it into an array of bytes.
        /// </summary>
        /// <param name="BillerID">ID Biller</param>
        /// <param name="DigitalCertificateID">ID Digital Certificate</param>
        /// <param name="FileType">Specifies whether the file to be look for is a *.cer or *.key</param>
        /// <returns>Return the array of bytes needed to build the requested file.</returns>
        protected Byte[] SeekFile(Int32 BillerID, Int32 DigitalCertificateID, Int32 FileType)
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable ta;
            ta = certificate.SeekFile(this.CurrentBillerID, DigitalCertificateID);
            Byte[] file;

            if (ta.Count < 1)
                return null;

            file = new Byte[ta[0].Certificate.Length];
            file = Convert.FromBase64String(FileType == 0 ? ta[0].Certificate : ta[0].PrivtaeKey);
            ViewState["Name"] = (FileType == 0 ? ta[0].CertificateName : ta[0].PrivateKeyName);
            ta.Dispose();
            
            return file;
        }
    }
}