﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.DigitalCertificates
{
    public partial class DigitalCertificatesDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.ProgressBar.Visible = false;

            if (IsPostBack)
                return;

            if(Request.QueryString["DigitalCertificateID"].IsNull())
                Response.Redirect(Request.UrlReferrer.ToString());

            this.Title = this.GetLocalResourceObject("DigitalCertificateDetails").ToString();
            ViewState["DigitalCertificateID"] = Request.QueryString["DigitalCertificateID"];
            this.LoadData(Convert.ToInt32(Request.QueryString["DigitalCertificateID"]));
        }
        /// <summary>
        /// Load information for Digital Certificate detail.
        /// </summary>
        /// <param name="DigitalCertificateID">ID Digital Certificate</param>
        protected void LoadData(int DigitalCertificateID)
        {
            String c = this.GetLocalResourceObject("FormatDateDisplay").ToString();
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_SeekFileDataTable ta;
            ta = certificate.SeekFile(this.CurrentBillerID, DigitalCertificateID);

            if (ta.Count < 1)
                return;

            this.lblCertificateName.Text = ta[0].CertificateName;
            this.lblPrivateKeyName.Text = ta[0].PrivateKeyName;
            this.lblCertificateValidityBegin.Text = ta[0].CertificateValidityBegin.ToString(c);
            this.lblCertificateValidityEnd.Text = ta[0].CertificateValidityEnd.ToString(c);
            this.lblStatus.Text = (ta[0].Status == 1 ? this.GetLocalResourceObject("Active").ToString() : 
                                                  this.GetLocalResourceObject("NotActive").ToString());
            this.lblCcp.Text = Cryptography.DecryptUnivisitString(ta[0].Ccp);
            this.lblCertificateNumber.Text = ta[0].CertificateNumber;

            ta.Dispose();
        }
        /// <summary>
        /// Redirects and specifies that the file to download is the Certificate, which corresponds to DigitalCertificateID sent.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnCer_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/DigitalCertificates/DownloadFiles.aspx?DigitalCertificateID=" +
                               ViewState["DigitalCertificateID"].ToString() + "&FileType=0");
        }
        /// <summary>
        /// Redirects and specifies that the file to download is the Key, which corresponds to DigitalCertificateID sent.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnKey_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/DigitalCertificates/DownloadFiles.aspx?DigitalCertificateID=" +
                               ViewState["DigitalCertificateID"].ToString() + "&FileType=1");
        }
    }
}