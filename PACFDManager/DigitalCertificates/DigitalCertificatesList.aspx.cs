﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.Web.UI.HtmlControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.DigitalCertificates
{
    public partial class DigitalCertificatesList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.GetLocalResourceObject("DigitalCertificateList").ToString();
            this.LoadData();
        }
        /// <summary>
        /// Brings all the corresponding rows of the database to fill the list of Digital Certificates.
        /// </summary>
        protected void LoadData()
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable ta;
            ta = certificate.SelectByBillerID(this.CurrentBillerID);

            this.gvDigitalCertificates.DataMember = ta.TableName;
            this.gvDigitalCertificates.DataSource = ta;
            this.gvDigitalCertificates.DataBind();

            if (this.gvDigitalCertificates.Rows.Count < 1)
            {
                DataTable dt = new DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDDataTable();
                DataRow dr = dt.NewRow();
                dr["DigitalCertificateID"] = -1;
                dr["BillerID"] = -1;
                dr["Certificate"] = String.Empty;
                dr["PrivateKeyName"] = String.Empty;
                dr["PrivtaeKey"] = String.Empty;
                dr["Ccp"] = String.Empty;
                dr["Seal"] = String.Empty;
                dr["Status"] = 1;
                dr["CertificateValidityBegin"] = DateTime.Now;
                dr["CertificateValidityEnd"] = DateTime.Now;
                dr["CertificateNumber"] = String.Empty;
                dr["CertificateName"] = String.Empty;
                dt.Rows.Add(dr);

                this.ViewState["Empty"] = true;

                this.gvDigitalCertificates.DataSource = dt;
                this.gvDigitalCertificates.DataBind();

                dt.Dispose();
            }

            ta.Dispose();
        }
        /// <summary>
        /// Calls message control to confirm that it will de/activate the digital certificate.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnActive_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            String c = (s.CommandName == "1" ? this.GetLocalResourceObject("Activate").ToString() :
                                               this.GetLocalResourceObject("Deactivate").ToString());

            this.WebMessageBox1.Title = c + " " + this.GetLocalResourceObject("Certificate").ToString();
            this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("QuestionPart1").ToString() + " " + c
                                    + " " + this.GetLocalResourceObject("QuestionPart2").ToString(),
                                    System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["DigitalCertificateID"] = s.CommandArgument;
        }
        /// <summary>
        /// Changes the status of Digital Certificate to Active / Not Active
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet ds = new DigitalCertificatesDataSet();
            String c = String.Empty;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            certificate.SelectByDigitalCertificateID(ds.DigitalCertificates, Convert.ToInt32(ViewState["DigitalCertificateID"]));

            String BeforeDataSet = ds.GetXml();

            if (ds.DigitalCertificates.Count > 0)
            {
                DigitalCertificatesDataSet.DigitalCertificatesRow drCert = ds.DigitalCertificates[0];
                drCert.Status = (drCert.Status == 1 ? 0 : 1);
                c = (drCert.Status == 1 ? this.GetLocalResourceObject("Deactivate").ToString() :
                                          this.GetLocalResourceObject("Activate").ToString());
            }

            if (!certificate.Update(ds.DigitalCertificates))
            {
                LogManager.WriteStackTrace(new Exception("Error al tratar de " + c + " Certificado"));
                this.WebMessageBox1.Title = c + " " + this.GetLocalResourceObject("Certificate").ToString();
                WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Error").ToString() + " " + c
                                   + " " + this.GetLocalResourceObject("Certificate").ToString(),
                                   System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} ha {1} un certificado digital.",
                this.UserName, c), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
            #endregion

            this.ViewState["ClientID"] = null;
            LogManager.WriteStackTrace(new Exception("Éxito al " + c + " Certificado"));
            this.Response.Redirect(typeof(DigitalCertificatesList).Name + ".aspx");
        }
        /// <summary>
        /// Changes the image depending of value on the cell that contains and 
        /// the value of the property enable, for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvDigitalCertificates_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            ImageButton i = e.Row.FindControl("ImgBtnActive") as ImageButton;
            if (row.RowIndex > -1)
            {
                if (!ViewState["State"].IsNull())
                {
                    if (Convert.ToInt32(ViewState["State"].ToString()) > 0)
                    {
                        if (row.Cells[5].Text == "1")
                        {
                            row.Cells[7].Enabled = 
                            i.Visible = true;
                            row.Cells[5].BackColor = System.Drawing.Color.FromArgb(198, 239, 206);//Green
                            row.Cells[5].ForeColor = System.Drawing.Color.FromArgb(43, 97, 45);
                        }
                        else
                        {
                            row.Cells[7].Enabled = 
                            i.Visible = false;
                            row.Cells[5].BackColor = System.Drawing.Color.FromArgb(255, 199, 206);//Red
                            row.Cells[5].ForeColor = System.Drawing.Color.FromArgb(172, 0, 48);
                        }
                    }
                    else
                    {
                        row.Cells[7].Enabled = true;
                        row.Cells[5].BackColor = System.Drawing.Color.FromArgb(255, 199, 206);//Red
                        row.Cells[5].ForeColor = System.Drawing.Color.FromArgb(172, 0, 48);
                    }

                    i.CommandName = (row.Cells[5].Text == "1" ? "0" : "1");
                    i.ImageUrl = (row.Cells[5].Text == "1" ? "~/Includes/Images/png/editdelete.png" : "~/Includes/Images/png/apply.png");
                    row.Cells[5].Text = (row.Cells[5].Text == "1" ? this.GetLocalResourceObject("Active").ToString() :
                                                                    this.GetLocalResourceObject("NotActive").ToString());
                }

                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int j = 0; j < row.Cells.Count; j += 1)
                        row.Cells[j].Visible = false;
                    ViewState["Empty"] = null;
                }
            }
            else
            {
                PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
                DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable ta;
                ta = certificate.GetActiveByBillerID(this.CurrentBillerID);
                this.ViewState["State"] = ta.Count;
            }

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            HtmlAnchor key = e.Row.FindControl("aDownLoadKey") as HtmlAnchor;
            HtmlAnchor cer = e.Row.FindControl("aDownLoadCer") as HtmlAnchor;
            HtmlAnchor det = e.Row.FindControl("aDetails") as HtmlAnchor;

            DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDRow dr = ((System.Data.DataRowView)e.Row.DataItem).Row as DigitalCertificatesDataSet.DigitalCertificates_GetByFkBillerIDRow;

            if (!key.IsNull()) 
                key.HRef = String.Format("~/DigitalCertificates/DownloadFiles.aspx?DigitalCertificateID={0}&FileType=1", dr.DigitalCertificateID);
            if (!cer.IsNull()) 
                cer.HRef = String.Format("~/DigitalCertificates/DownloadFiles.aspx?DigitalCertificateID={0}&FileType=0", dr.DigitalCertificateID);
            if (!det.IsNull()) 
                det.HRef = String.Format("~/DigitalCertificates/DigitalCertificatesDetails.aspx?DigitalCertificateID={0}", dr.DigitalCertificateID);
        }
    }
}