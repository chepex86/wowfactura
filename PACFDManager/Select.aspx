<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Select.aspx.cs" Inherits="PACFDManager.Select" %>

<%@ Register Src="WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="select_data">
                <asp:Label runat="server" ID="lblQueryString" class="alertBox" Style="display: none" />
                <asp:Label runat="server" ID="lblGroupTitle" Text="Seleccione con que grupo desea trabajar"
                    class="alertBox" />
                <asp:Label ID="lblBillerTitle" runat="server" Text="Seleccione con que empresa desea trabajar"
                    class="alertBox" />
                <br />
                <div runat="server" id="divUserList" class="divUserList">
                    <asp:GridView ID="dgvGroup" runat="server" AutoGenerateColumns="False" CssClass="DataGrid"
                        AutoGenerateSelectButton="True" OnSelectedIndexChanged="dgvGroup_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="GroupID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Grupo" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="dgHeader" />
                        <RowStyle CssClass="dgItem" />
                        <AlternatingRowStyle CssClass="dgAlternate" />
                    </asp:GridView>
                </div>
                <br />
                <div runat="server" id="divBillers">
                    <div class="wframe60">
                        <asp:GridView ID="dgvBillers" runat="server" AutoGenerateColumns="False" CssClass="DataGrid"
                            OnRowDataBound="dgvBillers_RowDataBound" AutoGenerateSelectButton="True" OnSelectedIndexChanged="dgvBillers_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%#string.Format("{0}{1}", Eval("BillerID"), !String.IsNullOrEmpty(Eval("BranchCode").ToString()) ? " - " + Eval("BranchCode").ToString() : "")%>
                                        <asp:HiddenField ID="hfBillerID" runat="server" Value='<%#Eval("BillerID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Empresa" />
                                <asp:TemplateField HeaderText="Sucursal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranchName" runat="server" Text='<%#Eval("BranchName")%>' />
                                        <asp:HiddenField ID="hfBranchID" runat="server" Value='<%#Eval("BranchID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RFC" HeaderText="RFC" />
                                <asp:BoundField DataField="CurrencyCode" HeaderText="Codigo de moneda" />
                                <asp:TemplateField HeaderText="Tipo de Factura">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBillingType" runat="server" Text='<%#Eval("ElectronicBillingTypeText")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                            <HeaderStyle CssClass="dgHeader" />
                            <RowStyle CssClass="dgItem" />
                            <AlternatingRowStyle CssClass="dgAlternate" />
                        </asp:GridView>
                    </div>
                </div>
                <asp:Label ID="lblMessage" runat="server" Font-Bold="true" Text="" />
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" />
                <div runat="server" id="divLkb" style="margin-top: 10px;">
                    <asp:LinkButton ID="lkbAddNewClient" runat="server" CausesValidation="False" Font-Bold="True"
                        ForeColor="#FF8000" OnClick="lkbAddNewClient_Click"> [Agregar un grupo] </asp:LinkButton>
                    <asp:LinkButton ID="lkbSelectAnotherClient" runat="server" CausesValidation="False"
                        Font-Bold="True" ForeColor="#FF8000" OnClick="lkbSelectAnotherClient_Click"> [Seleccionar otro grupo] </asp:LinkButton>
                    <asp:LinkButton ID="lkbAddNewBillers" runat="server" CausesValidation="False" Font-Bold="True"
                        ForeColor="#FF8000" OnClick="lkbAddNewBillers_Click"> [Agregar una  empresa] </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
