﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Security
{
    public partial class AccessDenied : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string backPage = GetValueOfQueryString("page", false);
            if (backPage == string.Empty)
                backPage = "esta opción";
            lblQueryString.Text = string.Format("No tienes acceso a {0}",backPage);

        }


        private string GetValueOfQueryString(string parameterName, bool descrypt)
        {

            try
            {
                if (Request.QueryString[parameterName] != null && Request.QueryString[parameterName].ToString() != string.Empty)
                {
                    if (descrypt)
                        return Cryptography.EncryptUnivisitString(Request.QueryString[parameterName].ToString().Replace(' ', '+'));
                    else
                        return Request.QueryString[parameterName].ToString();
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + "Security"
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
            return string.Empty;


        }
    }
}
