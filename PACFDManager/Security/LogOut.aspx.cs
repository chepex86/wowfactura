﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;
#endregion

namespace PACFDManager.Security
{
    public partial class LogOut : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String add = String.Empty;

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} ha finalizado sesión en el sistema.",
                this.UserName), this.CurrentBillerID, null, null);
            #endregion

            Security.SignOut();
            
            if (HaveCurrentBrand)
            {
                if (CurrentBrand["Directory"] != null)
                {
                    add = String.Format("?di={0}", CurrentBrand["Directory"].ToString());
                }
            }

            HttpContext.Current.Session.Abandon();
            this.Response.Redirect(String.Format("~/Default.aspx{0}", add));
        }
    }
}