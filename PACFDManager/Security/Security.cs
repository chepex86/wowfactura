﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
//using Tool = PACFD.Common.Utilities;
#endregion

namespace PACFDManager.Security
{
    /// <summary>
    /// Class used to log in, out, unlok and get global information about the current user.
    /// </summary>
    public static class Security
    {
        #region Methods
        /// <summary>
        /// Log in an user in to the system. Set in the session the roles, name, ID and group.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">User password.</param>
        /// <returns>If the log in is success return true else false.</returns>
        public static bool Login(String userName, String password)
        {
            try
            {
                SignOut();
                FormsAuthentication.Initialize();

                string passwordEncrypt = Cryptography.EncryptUnivisitString(password);
                //testCFDI();
                PACFD.Rules.Users user = new PACFD.Rules.Users();
                UsersDataSet dsUser = new UsersDataSet();
                user.SelectUserforLogin(dsUser.Login, userName, passwordEncrypt);

                if (dsUser.Login.Count > 0)
                {
                    HttpContext.Current.Session[string.Format("PACFDManager-RolesLevel")] = dsUser.Login[0].Role;

                    switch ((eRoles)dsUser.Login[0].Role)
                    {
                        case eRoles.Administrator:
                            HttpContext.Current.Session[string.Format("PACFDManager-Roles")] = eRoles.Administrator.ToString();
                            break;
                        case eRoles.SAT:
                            HttpContext.Current.Session[string.Format("PACFDManager-Roles")] = eRoles.SAT.ToString();
                            break;
                        case eRoles.AdvancedClient:
                            HttpContext.Current.Session[string.Format("PACFDManager-Roles")] = eRoles.AdvancedClient.ToString();
                            break;
                        case eRoles.BasicClient:
                            HttpContext.Current.Session[string.Format("PACFDManager-Roles")] = eRoles.BasicClient.ToString();
                            HttpContext.Current.Session[string.Format("PACFDManager-ElectronicBillingType")] = GetElectronicBillingType(dsUser.Login[0].BillerID);
                            break;
                    }

                    HttpContext.Current.Session[string.Format("PACFDManager-UserName")] = dsUser.Login[0].UserName;
                    HttpContext.Current.Session[String.Format("PACFDManager-UserID")] = dsUser.Login[0].UserID;
                    HttpContext.Current.Session[string.Format("PACFDManager-IsLogged")] = true;

                    HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupID")] = dsUser.Login[0].GroupID;
                    HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupName")] = dsUser.Login[0].GroupName;

                    HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")] = dsUser.Login[0].BillerID;
                    HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerName")] = dsUser.Login[0].BillerName;

                    //The AddMinutes determines how long the user will be logged in after leaving
                    //the site if he doesn't log off.
                    String UserName = dsUser.Login[0].UserName;
                    String Role = ((eRoles)dsUser.Login[0].Role).ToString();

                    FormsAuthenticationTicket fat;
                    fat = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddMinutes(30), false, Role, FormsAuthentication.FormsCookiePath);

                    HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(fat)));

                    //HttpContext.Current.Response.Redirect(FormsAuthentication.GetRedirectUrl(dsUser.Login[0].UserName, false));
                    //string url = FormsAuthentication.GetRedirectUrl(UserName, false);                        
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + "Security " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }

        private static int GetElectronicBillingType(int BillerID)
        {
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();

            table = biller.SelectByID(BillerID);

            return table.Count < 1 ? -1 : table[0].ElectronicBillingType;
        }

        public static bool Unlock(String userName, ref String password)
        {
            try
            {
                //string passwordEncrypt = crypto.EncryptString128Bit(password, crypto.PublicKey);
                PACFD.Rules.Users user = new PACFD.Rules.Users();
                UsersDataSet dsUser = new UsersDataSet();
                user.SelectUserforUnlock(dsUser.Login, userName);

                if (dsUser.Login.Count > 0)
                {
                    password = Cryptography.DecryptUnivisitString(dsUser.Login[0].Password);
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + "Security" + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }
        /// <summary>
        /// Chage the password of an user.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="oldPassword">Old password to be changed.</param>
        /// <param name="newPassword">New password to be set.</param>
        /// <returns>If success return true else false.</returns>
        public static bool ChangePassword(String userName, String oldPassword, String newPassword)
        {
            try
            {

                string oldPasswordEncrypt = Cryptography.EncryptUnivisitString(oldPassword);

                PACFD.Rules.Users user = new PACFD.Rules.Users();
                UsersDataSet dsUser = new UsersDataSet();
                user.SelectUserforLogin(dsUser.Login, userName, oldPasswordEncrypt);
                if (dsUser.Login.Count > 0)
                {
                    newPassword = Cryptography.EncryptUnivisitString(newPassword);
                    user.SelectByIDUsers(dsUser.Users, dsUser.Login[0].UserID);
                    if (dsUser.Users.Count > 0)
                    {
                        dsUser.Users[0].Password = newPassword;
                        user.UpdateUsers(dsUser.Users);
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + "Security" + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + " " + ex.Message);
                LogManager.WriteError(ex);
            }
            return false;
        }
        /// <summary>
        /// Log out an actual log in user from the system. Set all session to null.
        /// </summary>
        public static void SignOut()
        {
            //HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
            HttpContext.Current.Session[String.Format("PACFDManager-Roles")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-UserName")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-IsLogged")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentGroupID")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentGroupName")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBillerID")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBillerName")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchID")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchName")] = null;
            HttpContext.Current.Session[String.Format("PACFDManager-RolesLevel")] = null;
        }

        public static void SetBillType()
        {
            SystemConfiguration config = new SystemConfiguration();
            PACFD.DataAccess.SystemConfigurationDataSet.SystemConfigurationDataTable dtConfig;

            dtConfig = config.GetAllSystemConfiguration();

            HttpContext.Current.Session[String.Format("PACFDManager-CFDEnable")] = dtConfig.Count < 1 ? true : dtConfig[0].CFDEnable;
            HttpContext.Current.Session[String.Format("PACFDManager-CBBEnable")] = dtConfig.Count < 1 ? false : dtConfig[0].CBBEnable;
            HttpContext.Current.Session[String.Format("PACFDManager-CFDIEnable")] = dtConfig.Count < 1 ? false : dtConfig[0].CFDIEnable;

            HttpContext.Current.Session[String.Format("PACFDManager-CFD2_2Enable")] = dtConfig.Count < 1 ? false : dtConfig[0].IsCFD2_2EnableNull() ?false : dtConfig[0].CFD2_2Enable;
            HttpContext.Current.Session[String.Format("PACFDManager-CFDI3_2Enable")] = dtConfig.Count < 1 ? false : dtConfig[0].IsCFDI3_2EnableNull() ? false : dtConfig[0].CFDI3_2Enable;
            HttpContext.Current.Session[String.Format("PACFDManager-CFDI3_3Enable")] = dtConfig.Count < 1 ? false : dtConfig[0].IsCFDI3_3EnableNull() ? false : dtConfig[0].CFDI3_3Enable;
            HttpContext.Current.Session[String.Format("PACFDManager-CFDI4_0Enable")] = dtConfig.Count < 1 ? false : dtConfig[0].IsCFDI4_0EnableNull() ? false : dtConfig[0].CFDI4_0Enable;

        }
        public static void SetExchangeRate()
        {
            SystemConfiguration config = new SystemConfiguration();
            PACFD.DataAccess.SystemConfigurationDataSet.SystemConfigurationDataTable dtConfig;

            dtConfig = config.GetAllSystemConfiguration();

            HttpContext.Current.Session[String.Format("PACFDManager-CurrentExchangeRate")] = dtConfig.Count < 1 ? 0 : dtConfig[0].ExchangeRate;
        }

        /*PARA QR DE CFDIs
         * 
         * public static void testCFDI()
        {
            int b = 214460;
            PACFD.DataAccess.BillingsDataSet ds = (new PACFD.Rules.Billings()).GetFullBilling(b);
            var array = (new PACFD.Rules.Billings()).GetCFDIArray(ds);


            //update test
            PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter q = new PACFD.DataAccess.BillingsDataSetTableAdapters.QueriesTableAdapter();
            Tool.RemoveOwnerSqlCommand(q);
            q.spSealCFDI(ds.Billings[0].BillingID
                                     , ds.Billings[0].OriginalString
                                     , ds.Billings[0].Seal
                                     , ds.Billings[0].UUID
                                     , ds.Billings[0].DateStamped
                                     , ds.Billings[0].SealSat
                                     , ds.Billings[0].CertificateNumberSat
                                     , ds.Billings[0].OrignalStringSat
                                     , ds.Billings[0].BatchIdSat
                                     , ds.Billings[0].StatusSat
                                     , ds.Billings[0].VersionSat
                                     , array
                                     , ds.Billings[0].RfcProvCertic
                                     , ds.Billings[0].SelloCFD);
        }
        */
        #endregion

        #region Properties
        public static ElectronicBillingType CurrentElectronicBillingType
        {
            get
            {
                if (HttpContext.Current.Session[String.Format("PACFDManager-ElectronicBillingType")] != null)
                {
                    return (ElectronicBillingType)HttpContext.Current.Session[String.Format("PACFDManager-ElectronicBillingType")];
                }
                return ElectronicBillingType.Indeterminate;
            }
            set
            {
            }
        }

        public static bool IsLogged
        {
            get
            {
                object o = HttpContext.Current.Session[String.Format("PACFDManager-IsLogged")];
                return o.IsNull() || o.GetType() != typeof(bool) ? false : true;
            }
        }
        public static String Roles
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session[String.Format("PACFDManager-Roles")].ToString();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return String.Empty;
            }
        }
        /// <summary>
        /// Get a boolean value, true if the user is an administrator user else false.
        /// </summary>
        public static bool IsAdministrator
        {
            get
            {
                try
                {
                    return Roles.Contains(eRoles.Administrator.ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return false;
            }
        }
        /// <summary>
        /// Get a boolean value, true if the user is a "SAT" user else false.
        /// </summary>
        public static bool IsSAT
        {
            get
            {
                try
                {
                    return Roles.Contains(eRoles.SAT.ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return false;
            }
        }
        /// <summary>
        /// Get a boolean value, true if is an advanced client else false.
        /// </summary>
        public static bool IsAdvancedClient
        {
            get
            {
                try
                {
                    return Roles.Contains(eRoles.AdvancedClient.ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return false;
            }
        }
        /// <summary>
        /// Get a boolean value, true if is a basic user else false.
        /// </summary>
        public static bool IsBasicClient
        {
            get
            {
                try
                {
                    return Roles.Contains(eRoles.BasicClient.ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return false;
            }
        }
        /// <summary>
        /// Get the usen name.
        /// </summary>
        public static string UserName
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session[string.Format("PACFDManager-UserName")].ToString();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return "";
            }
        }
        public static int UserID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(HttpContext.Current.Session[String.Format("PACFDManager-UserID")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return -1;
            }
        }
        /// <summary>
        /// Get an integer with the group ID of one user.
        /// </summary>
        public static int CurrentGroupID
        {
            get
            {
                try
                {

                    return int.Parse(HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupID")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return -1;
            }
        }
        /// <summary>
        /// Get a integer value with the role level of the user.
        /// </summary>
        public static int RolLevel
        {
            get
            {
                try
                {

                    return int.Parse(HttpContext.Current.Session[string.Format("PACFDManager-RolesLevel")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return -1;
            }
        }
        /// <summary>
        /// Get an integer value with the current biller ID selected. Default -1.
        /// </summary>
        public static int CurrentBillerID
        {
            get
            {
                try
                {
                    return int.Parse(HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return -1;
            }
        }
        public static int CurrentBranchID
        {
            get
            {
                int i = -1;
                object o = HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchID")];
                return o.IsNull() || int.TryParse(o.ToString(), out i) ? i: -1;
            }
        }
        public static String CurrentBranchName
        {
            get
            {
                object o = HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchName")];

                try
                {
                    if(o.IsNull())
                    {
                        System.Diagnostics.Debug.WriteLine("PACFDManager-CurrentBranchNam is null");
                        return string.Empty;
                    }

                    return o.ToString();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return String.Empty;
            }
        }

        public static bool HaveBranch { get { return (CurrentBranchID != -1); } }

        /// <summary>
        /// Get a boolean value. If a valid biller is selected return true else false.
        /// </summary>
        public static Boolean IsBillerSelected
        {
            get
            {
                int i;
                object o = HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")];

                if (o.IsNull())
                    return false;

                if (!int.TryParse(o.ToString(), out i))
                    return false;

                if (i < 1)
                    return false;

                return true;
            }
        }
        public static string CurrentGroupName
        {
            get
            {
                try
                {

                    return HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupName")].ToString();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return string.Empty;
            }
        }
        public static string CurrentBillerName
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerName")].ToString();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return string.Empty;
            }

        }
        public static bool CFDEnable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFDEnable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }
        public static bool CFDIEnable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFDIEnable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }

        /// <summary>
        /// Get a boolean value indicating if the CFD 2.2 is enabled.
        /// </summary>
        public static bool CFD2_2Enable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFD2_2Enable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.2 is enabled.
        /// </summary>
        public static bool CFDI3_2Enable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFDI3_2Enable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.3 is enabled.
        /// </summary>
        public static bool CFDI3_3Enable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFDI3_3Enable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }

        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.3 is enabled.
        /// </summary>
        public static bool CFDI4_0Enable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CFDI4_0Enable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }

        public static bool CBBEnable
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(HttpContext.Current.Session[String.Format("PACFDManager-CBBEnable")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return false;
                }
            }
        }
        public static Decimal CurrentExchangeRate
        {
            get
            {
                try
                {
                    return Convert.ToDecimal(HttpContext.Current.Session[String.Format("PACFDManager-CurrentExchangeRate")].ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    return 0;
                }
            }
        }

        #endregion
    }
}