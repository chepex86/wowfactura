﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Rules.Mail;
#endregion

namespace PACFDManager.Security
{
    public partial class PasswordUnlock : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPassword.Visible = false;
            this.lblMessage.Visible = false;
        }

        protected void bntOther_Click(object sender, EventArgs e)
        {
            this.lblSendEmailError.Visible =
            this.lblSendEmail.Visible =
            this.btnOther.Visible = false;
            this.btnUnlock.Visible = 
            this.cbSendEmail.Checked = true;
            this.txtEmail.Text = String.Empty;
            this.lblPassword.Text = String.Empty;
        }

        protected void btnUnlock_Click(object sender, EventArgs e)
        {
            String password = String.Empty;

            if (Security.Unlock(this.txtEmail.Text.Trim(), ref password))
            {
                this.lblPassword.Visible = true;
                this.lblPassword.Text = password;

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} ha desbloqueado la contraseña de el usuario {1}",
                    this.UserName, this.txtEmail.Text.Trim()), this.CurrentBillerID, null, null);
                #endregion

                if (this.cbSendEmail.Checked)
                {
                    if (this.SendEmail(password))
                        this.lblSendEmail.Visible = true;
                    else
                        this.lblSendEmailError.Visible = true;
                }

                this.btnOther.Visible = true;
                this.btnUnlock.Visible = false;
            }
            else
            {
                this.lblMessage.Visible = true;
                this.lblMessage.Text = Resources.SystemStrings.M0002;
            }
        }

        private bool SendEmail(String password)
        {
            MailSender mail = new MailSender();
            String path = this.Context.Request.MapPath("~/Includes/Mail/EmisorAdded.es.xml");
            mail.Message = MailSenderHelper.GetMessageFromXML(path, "003");

            mail.Parameters.Add("lblEmail", this.txtEmail.Text.Trim());
            mail.Parameters.Add("lblPassword", password);

            try
            {
                mail.Send();
                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }
    }
}