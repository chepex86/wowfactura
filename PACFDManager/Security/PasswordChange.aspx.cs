﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Security
{
    public partial class ChangePassword : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {

            if (Security.ChangePassword(this.UserName, txtCurrentPassword.Text, txtNewPassword.Text))
            {

            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = Resources.SystemStrings.M0003;
            }
        }
    }
}
