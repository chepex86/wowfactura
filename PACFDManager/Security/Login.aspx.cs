﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using PACFD.Rules.Mail;
using System.Text;
#endregion

namespace PACFDManager.Security
{
    public partial class Login : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                this.RegisterScript();
                Security.SignOut();
                Page.Form.DefaultButton = btnIn.UniqueID;
            }
        }

        protected void lnkBtnRemember_Click(object sender, EventArgs e)
        {
            this.PasswordRecovery();
        }

        private bool PasswordRecovery()
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.Users_GetBySearchingDataTable dt;

            dt = user.SelectUserBySearching(null, this.txtEmail.Text.Trim());

            if (dt.Count < 1)
                return false;

            if (!this.SendEmail(dt[0].UserID, dt[0].UserName, Cryptography.DecryptUnivisitString(dt[0].Password)))
                return false;

            return true;
        }

        private bool SendEmail(int userid, String username, String password)
        {
            try
            {
                PACFD.Rules.Mail.MailSender sender = new PACFD.Rules.Mail.MailSender();
                String path = this.Context.Request.MapPath("~/Includes/Mail/EmisorAdded.es.xml");
                sender.Message = MailSenderHelper.GetMessageFromXML(path, "003");

                sender.Parameters.Add("lblEmail", this.txtEmail.Text.Trim());
                sender.Parameters.Add("lblPassword", password);
                sender.Send();

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(userid, String.Format("Se le envió al usuario {0} un email para recordar su contraseña",
                    username), this.CurrentBillerID, null, null);
                #endregion

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        protected void btnIn_Click(object sender, ImageClickEventArgs e)
        {
            System.Data.DataSet newsdataset;

            if (!Security.Login(txtUserName.Text, txtPassword.Text))
            {
                this.lblError.Text = Resources.SystemStrings.M0001;
                lblError.Visible = true;
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} ha iniciado sesión en el sistema.", 
                this.UserName), this.CurrentBillerID, null, null);
            #endregion

            #region Load News
            SystemConfigurationDataSet.SystemConfigurationDataTable table;
            SystemConfiguration config = new SystemConfiguration();
            PACFD.Rules.News news = new PACFD.Rules.News();

            table = config.GetAllSystemConfiguration();

            if (table.Count < 1)
            {
                config.AddSystemConfiguration();
                news.AddNews(news.GetXMLDataSet().Tables["item"]);
            }
            else
            {
                if (news.CompareDates(table[0].LatestNewsReview) >= 24)
                {
                    config.OverwriteConfiguration();
                    news.DeleteAllNews();

                    newsdataset = news.GetXMLDataSet();

                    if (newsdataset != null && newsdataset.Tables.Count > 0)
                    { news.AddNews(newsdataset.Tables["item"]); }
                }
            }
            #endregion

            Security.SetBillType();
             
            HttpContext.Current.Response.Redirect("~/Default.aspx");
        }

        private bool RegisterScript()
        {
            try
            {
                this.aRecoveryPassword.Attributes["onclick"] = "javascript:jcSecurity.ShowHiddeRecoverPassword()";
                this.aReturn.Attributes["onclick"] = "javascript:jcSecurity.ShowHiddeRecoverPassword()";

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}