﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
#endregion

namespace PACFDManager
{
    public partial class Tooltip : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Sets the text will be displayed in the tooltip.
        /// </summary>
        public String ToolTip
        {
            get { return this.imgToolTip.ToolTip; }
            set
            {
                this.imgToolTip.ToolTip = value;
            }
        }
        /// <summary>
        /// Sets the visible property value control.
        /// </summary>
        public override bool Visible
        {
            get { return base.Visible; }
            set { base.Visible = this.imgToolTip.Visible = value; }
        }
    }
}