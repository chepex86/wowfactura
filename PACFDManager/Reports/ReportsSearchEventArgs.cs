﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace PACFDManager.Reports
{
    public class ReportsSearchEventArgs : EventArgs
    {
        public int? BillingID { get; private set; }
        public DateTime? Start { get; private set; }
        public DateTime? End { get; private set; }
        public int? ReceptorID { get; private set; }
        public String UnitType { get; private set; }
        public bool? Status { get; private set; }
        public int? ConceptID { get; private set; }
        public String TextFilter { get; private set; }
        public int? AppliesTax { get; private set; }
        public String ReceptorName { get; private set; }
        public bool? PreBilling { get; private set; }
        public bool? IsCredit { get; private set; }
        public int? GroupID { get; private set; }
        public int? BillerID { get; private set; }
        public Byte? ElectronicBillingType { get; private set; }

        public ReportsSearchEventArgs(int? billingId, DateTime? start, DateTime? end, int? receptorId, String unittype, bool? status, 
                                      int? conceptId, bool? preBilling, bool? isCredit, String textfilter)
        {
            this.BillingID = billingId;
            this.Start = start;
            this.End = end;
            this.ReceptorID = receptorId;
            this.UnitType = unittype;
            this.Status = status;
            this.ConceptID = conceptId;
            this.TextFilter = textfilter;
            this.PreBilling = preBilling;
            this.IsCredit = isCredit;
        }

        public ReportsSearchEventArgs(int? appliesTax, String uniteType, String textfilter)
        {
            this.AppliesTax = appliesTax;
            this.UnitType = uniteType;
            this.TextFilter = textfilter;
        }

        public ReportsSearchEventArgs(int? groupid, int? billerid, Byte? electronicbillingtype, DateTime? start, DateTime? end, String textfilter)
        {
            this.GroupID = groupid;
            this.BillerID = billerid;
            this.ElectronicBillingType = electronicbillingtype;
            this.Start = start;
            this.End = end;
            this.TextFilter = textfilter;
        }
    }
}