﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Reports
{
    /// <summary>
    /// Search user control for excel reports.
    /// </summary>
    public partial class ExcelReportSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Search event.
        /// </summary>
        public event ExcelReportSearchEventHandler Search;

        /// <summary>
        /// Get or set information of the control.
        /// </summary>
        public ExcelReportSearchInformation ExcelInformation { get { return new ExcelReportSearchInformation(this); } }

        /// <summary>
        /// Page lod method.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.SetJavaScript();

            this.DataBind();

            if (this.IsPostBack)
            { return; }

            this.txtInitialDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            this.txtEndDate.Text = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).Subtract(new TimeSpan(1, 0, 0, 0)).ToString("dd/MM/yyyy");
        }
        /// <summary>
        /// Data bind the needed data to the control.
        /// </summary>
        public override void DataBind()
        {
            base.DataBind();

            if (this.IsPostBack)
            { return; }

            var t = from x in (new PACFD.Rules.Receptors()).SelectByBillersID(this.CurrentBillerID)
                    select new { Name = string.Format("{0} - {1}", x.RFC, x.Name), ReceptorID = x.ReceptorID };
            this.ddlReceptor.DataSource = t;
            this.ddlReceptor.DataBind();
            this.ddlReceptor.Items.Insert(0, new ListItem("[Seleccionar]", "-1"));
            this.ddlReceptor.SelectedIndex = 0;
        }

        //void SetJavaScript()
        //{
        //    string s;
        //    string javascriptinstance = string.Format("{0}_ExcelReportSearch", this.ClientID);
        //    ScriptManager sm;
        //    ScriptReference sr;

        //    s = string.Format("\nvar {0} = new jclassBillingsEditor({1});\n", javascriptinstance
        //            , string.Format("{{ txtConcept_AutoCompleteExtender:'{0}', hflArticleSearch:'{1}' }}"
        //                , this.txtConcept_AutoCompleteExtender.ClientID
        //                , this.hflArticleSearch.ClientID
        //            ));
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), javascriptinstance, s, true);

        //    if (this.IsPostBack || this.Page.IsNull())
        //    { return; }

        //    sr = new ScriptReference("~/Reports/ExcelReportSearch.js");
        //    sm = ScriptManager.GetCurrent(this.Page);

        //    if (!sm.Scripts.Contains(sr))
        //    { sm.Scripts.Add(sr); }

        //    this.txtConcept_AutoCompleteExtender.OnClientItemSelected = string.Format("function() {{ {0}.autocompleteOnSelectItem(this); }}", javascriptinstance);
        //}

        /// <summary>
        /// Invoke the search event.
        /// </summary>
        public void InvokeSearch()
        {
            ExcelReportSearchEventArgs e = new ExcelReportSearchEventArgs(
                this.ckbIncludeDates.Checked ? this.ExcelInformation.InitialDate : (DateTime?)null
                , this.ckbIncludeDates.Checked ? this.ExcelInformation.EndDate : (DateTime?)null
                , this.ExcelInformation.IsPaid
                , this.ExcelInformation.IsCredit
                , this.ExcelInformation.ReceptorID
                , this.ExcelInformation.Active);

            this.OnSearch(e);
        }

        protected virtual void OnSearch(ExcelReportSearchEventArgs e)
        {
            if (!this.Search.IsNull())
            { this.Search(this, e); }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.InvokeSearch();
        }

        /// <summary>
        /// Nested class used to encapsulating information.
        /// </summary>
        public class ExcelReportSearchInformation
        {
            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Onwer of the class.</param>
            public ExcelReportSearchInformation(ExcelReportSearch owner) { this.Parent = owner; }

            /// <summary>
            /// Owner of the class.
            /// </summary>
            public ExcelReportSearch Parent { get; private set; }
            /// <summary>
            /// Get or set the nullable initial date.
            /// </summary>
            public DateTime? InitialDate
            {
                get
                {
                    DateTime d = DateTime.Now;

                    if (string.IsNullOrEmpty(this.Parent.txtInitialDate.Text))
                    { return null; }

                    return DateTime.TryParse(this.Parent.txtInitialDate.Text, out d) ? d : DateTime.Now;
                }
                set
                {
                    if (value.IsNull())
                    { this.Parent.txtInitialDate.Text = ""; }
                    else
                    { this.Parent.txtInitialDate.Text = ((DateTime)value).ToString("dd/MM/yyyy"); }
                }
            }
            /// <summary>
            /// Get or set the nullable end date.
            /// </summary>
            public DateTime? EndDate
            {
                get
                {
                    DateTime d = DateTime.Now;

                    if (string.IsNullOrEmpty(this.Parent.txtEndDate.Text))
                    { return null; }

                    return DateTime.TryParse(this.Parent.txtEndDate.Text, out d) ? d : DateTime.Now;
                }
                set
                {
                    if (value.IsNull())
                    { this.Parent.txtEndDate.Text = ""; }
                    else
                    { this.Parent.txtEndDate.Text = ((DateTime)value).ToString("dd/MM/yyyy"); }
                }
            }
            /// <summary>
            /// Get or set the nullable is paid.
            /// </summary>
            public bool? IsPaid
            {
                get
                {
                    if (this.Parent.ddlPaid.SelectedIndex < 1)
                    { return null; }

                    if (this.Parent.ddlPaid.SelectedIndex == 1)
                    { return false; }

                    return true;
                }
                private set
                {
                    if (value == null) { this.Parent.ddlPaid.SelectedIndex = 0; }
                    else if ((Boolean)value) { this.Parent.ddlPaid.SelectedIndex = 2; }
                    else { this.Parent.ddlPaid.SelectedIndex = 1; }
                }
            }
            /// <summary>
            /// Get or set the nullable is credit.
            /// </summary>
            public bool? IsCredit
            {
                get
                {
                    if (this.Parent.ddlIsCredit.SelectedIndex < 1)
                    { return null; }

                    if (this.Parent.ddlIsCredit.SelectedIndex == 1)
                    { return false; }

                    return true;
                }
                private set
                {
                    if (value == null) { this.Parent.ddlIsCredit.SelectedIndex = 0; }
                    else if ((Boolean)value) { this.Parent.ddlIsCredit.SelectedIndex = 2; }
                    else { this.Parent.ddlIsCredit.SelectedIndex = 1; }
                }
            }
            /// <summary>
            /// Get or set the receptor ID.
            /// </summary>
            public int ReceptorID
            {
                get { return this.Parent.ddlReceptor.SelectedIndex < 1 ? 0 : int.Parse(this.Parent.ddlReceptor.SelectedValue); }
                set
                {
                    ListItem l;

                    if ((l = this.Parent.ddlReceptor.Items.FindByValue(value.ToString())) != null)
                    {
                        this.Parent.ddlReceptor.ClearSelection();
                        l.Selected = true;
                    }
                    else { this.Parent.ddlReceptor.SelectedIndex = 0; }
                }
            }
            /// <summary>
            /// Get or set the nullable active.
            /// </summary>
            public bool? Active
            {
                get
                {
                    if (this.Parent.ddlActive.SelectedIndex < 1)
                    { return null; }

                    if (this.Parent.ddlActive.SelectedIndex == 1)
                    { return true; }

                    return false;
                }
                set
                {
                    if (value == null)
                    {
                        this.Parent.ddlActive.SelectedIndex = 0;
                    }
                    else if ((bool)value)
                    {
                        this.Parent.ddlActive.SelectedIndex = 1;
                    }
                    else
                    {
                        this.Parent.ddlActive.SelectedIndex = 2;
                    }
                }
            }
        }
    }
    /// <summary>
    /// Event handler of the search.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the object.</param>
    public delegate void ExcelReportSearchEventHandler(object sender, ExcelReportSearchEventArgs e);
    /// <summary>
    /// Event arguments send by the search control.
    /// </summary>
    public class ExcelReportSearchEventArgs : EventArgs
    {
        /// <summary>
        /// Nullable initial date.
        /// </summary>
        public DateTime? InitialDate { get; private set; }
        /// <summary>
        /// Nullable end date.
        /// </summary>
        public DateTime? EndDate { get; private set; }
        /// <summary>
        /// Nullable is paid.
        /// </summary>
        public bool? IsPaid { get; private set; }
        /// <summary>
        /// Nullable is credit.
        /// </summary>
        public bool? IsCredit { get; private set; }
        /// <summary>
        /// Receptor ID.
        /// </summary>
        public int ReceptorID { get; private set; }
        /// <summary>
        /// Nullable is active.
        /// </summary>
        public bool? IsActive { get; private set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="initialdate">Nullable initial date.</param>
        /// <param name="enddate">Nullable end date.</param>
        /// <param name="ispaid">Nullable is paid.</param>
        /// <param name="iscredit">Nullable is credit.</param>
        /// <param name="receptorid">Receptor ID.</param>
        /// <param name="isactive">Nullable is active.</param>
        public ExcelReportSearchEventArgs(DateTime? initialdate, DateTime? enddate, bool? ispaid, bool? iscredit, int receptorid, bool? isactive)
        {
            this.EndDate = enddate;
            this.InitialDate = initialdate;
            this.IsPaid = ispaid;
            this.IsCredit = iscredit;
            this.ReceptorID = receptorid;
            this.IsActive = isactive;
        }
    }
}