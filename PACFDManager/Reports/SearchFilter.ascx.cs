﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Web.UI.HtmlControls;
using System.Text;
#endregion

namespace PACFDManager.Reports
{
    public partial class SearchFilter : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the control Apply Changes.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void SearchEventHandler(Object sender, ReportsSearchEventArgs e);
        /// <summary>
        /// Event handler for the control Search.
        /// </summary>
        public event SearchEventHandler Search;
        /// <summary>
        /// Get TypeFilter property.
        /// </summary>
        public TypeFilter TypeFilter { get; private set; }
        /// <summary>
        /// Set TypeFilter property
        /// </summary>
        /// <param name="type">TypeFilter type</param>
        public void SetTypeFilter(TypeFilter type)
        {
            this.TypeFilter = type;
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.SearchFilter.Search event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(ReportsSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(Object sender, EventArgs e)
        {
            if (this.CurrentElectronicBillingType == ElectronicBillingType.CBB)
                this.liBillType.Visible = false;

            this.ConfigureSearchFilter();

            if (!IsPostBack)
            {
                this.txtDateStart_CalendarExtender.Format = "dd/MM/yyyy";
                this.txtDateEnd_CalendarExtender.Format = "dd/MM/yyyy";
                this.txtDateStart.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
                this.txtDateEnd.Text = DateTime.Now.ToString("dd/MM/yyyy");

                this.txtStart_CalendarExtender.Format = "dd/MM/yyyy";
                this.txtEnd_CalendarExtender.Format = "dd/MM/yyyy";
                this.txtStart.Text = (new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 1)).ToString("dd/MM/yyyy");
                this.txtEnd.Text = (new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month,
                    (new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 1).AddMonths(1).AddDays(-1).Day))).ToString("dd/MM/yyyy");
            }
            this.RegisterScript();

            if (!IsPostBack)
            {
                this.FillDropDownListConsumer();
                this.FillDropDownListProductDescription();
            }
        }
        /// <summary>
        /// Fills the customer dropdownlist with customer information.
        /// </summary>
        /// <returns>Returns true if the dropdownlist was filled, otherwise it returns false.</returns>
        private bool FillDropDownListConsumer()
        {
            if (IsPostBack)
                return false;

            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            ReceptorsDataSet.ReceptorsGetByBillersIDDataTable ta;
            ta = receptor.SelectByBillersID(this.CurrentBillerID);
            ListItem l = new ListItem();

            this.ddlConsumer.Items.Clear();

            l.Text = "[Todos]";
            l.Value = "-1";
            this.ddlConsumer.Items.Add(l);

            if (ta.Count < 1)
                return false;

            for (int i = 0; i < ta.Count; i += 1)
            {
                l = new ListItem();
                l.Text = ta[i].Name;
                l.Value = ta[i].ReceptorID.ToString();
                this.ddlConsumer.Items.Add(l);
            }

            return true;
        }
        /// <summary>
        /// Fills the ProductDescription dropdownlist with product description.
        /// </summary>
        /// <returns>Returns true if the dropdownlist was filled, otherwise it returns false.</returns>
        private bool FillDropDownListProductDescription()
        {
            if (IsPostBack)
                return false;

            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            ConceptsDataSet.Concepts_GetByFkBillerIDDataTable ta;
            ta = concept.GetConceptsByBillerID(this.CurrentBillerID);
            ListItem l = new ListItem();

            this.ddlProductDescription.Items.Clear();

            l.Text = "[Todos]";
            l.Value = "-1";

            this.ddlProductDescription.Items.Add(l);

            if (ta.Count < 1)
                return false;

            for (int i = 0; i < ta.Count; i += 1)
            {
                l = new ListItem();
                if(ta[i].Description.Length >= 80)
                    l.Text = string.Format("{0}...",ta[i].Description.Substring(0,80));    
                else
                    l.Text = ta[i].Description;
                l.Value = ta[i].ConceptID.ToString();
                this.ddlProductDescription.Items.Add(l);
            }

            return true;
        }
        private bool FillDropDownListGroup()
        {
            if (IsPostBack)
                return false;

            PACFD.Rules.Groups group = new PACFD.Rules.Groups();
            GroupsDataSet.GroupsDataTable ta;
            ta = group.SelectBySearching(null, null);
            ListItem l = new ListItem();

            this.ddlGroup.Items.Clear();

            l.Text = "[Todos]";
            l.Value = "-1";

            this.ddlGroup.Items.Add(l);

            if (ta.Count < 1)
                return false;

            for (int i = 0; i < ta.Count; i++)
            {
                l = new ListItem();
                l.Text = ta[i].Name;
                l.Value = ta[i].GroupID.ToString();
                this.ddlGroup.Items.Add(l);
            }

            return true;
        }
        private bool FillDropDownListBiller()
        {
            if (IsPostBack)
                return false;

            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.SelectBillersAllDataTable ta;
            ta = biller.SelectAll();
            ListItem l = new ListItem();

            this.ddlBiller.Items.Clear();

            l.Text = "[Todos]";
            l.Value = "-1";

            this.ddlBiller.Items.Add(l);

            if (ta.Count < 1)
                return false;

            for (int i = 0; i < ta.Count; i++)
            {
                l = new ListItem();
                l.Text = ta[i].Name;
                l.Value = ta[i].BillerID.ToString();
                this.ddlBiller.Items.Add(l);
            }

            return true;
        }
        private bool FillDropDownListBillerByGroupID(int GroupID)
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.GetByGroupIDDataTable ta;
            ta = biller.SelectByGroupID(GroupID);
            ListItem l = new ListItem();

            this.ddlBiller.Items.Clear();

            l.Text = "[Todos]";
            l.Value = "-1";

            this.ddlBiller.Items.Add(l);

            if (ta.Count < 1)
                return false;

            for (int i = 0; i < ta.Count; i++)
            {
                l = new ListItem();
                l.Text = ta[i].Name;
                l.Value = ta[i].BillerID.ToString();
                this.ddlBiller.Items.Add(l);
            }

            return true;
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.SearchFilter.btnGenerate_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnGenerate_Click(Object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b.IsNull())
                return;

            if (b == this.btnReport_001)
                this.OnSearch(this.Report_001());
            else if (b == this.btnReport_002)
                this.OnSearch(this.Report_002());
            else if (b == this.btnReport_003)
                this.OnSearch(this.Report_003());
            else if (b == this.btnInvoiceReport)
                this.OnSearch(this.InvoiceReport());
        }
        /// <summary>
        /// Generates the arguments that will be sent to the 001 report.
        /// </summary>
        /// <returns>Returns an object of type RerportsSearchEventArgs wiht 
        /// the arguments that will be sent to the 001 report</returns>
        private ReportsSearchEventArgs Report_001()
        {
            ReportsSearchEventArgs a;
            string u = null;
            bool? b = null;
            DateTime? d = null;
            int? ReceptorID = Convert.ToInt32(this.ddlConsumer.Items[this.ddlConsumer.SelectedIndex].Value);
            int? ConceptID = Convert.ToInt32(this.ddlProductDescription.Items[this.ddlProductDescription.SelectedIndex].Value);
            ReceptorID = ReceptorID > 0 ? ReceptorID : null;
            ConceptID = ConceptID > 0 ? ConceptID : null;

            a = new ReportsSearchEventArgs(
                this.CurrentBillerID, //BillerID
                (this.chkBoxIncludeDates.Checked ? Convert.ToDateTime(this.txtDateStart.Text) : d), //Date Start
                (this.chkBoxIncludeDates.Checked ? Convert.ToDateTime(this.txtDateEnd.Text) : d), //Date End
                ReceptorID, //ReceptorID
                this.ddlProductTypes.SelectedValue == "Todos" ? u : this.ddlProductTypes.SelectedValue, //UnitType
                (this.ddlStatus.SelectedIndex == 0 ? b : (this.ddlStatus.SelectedIndex == 1 ? true : false)), //Status
                ConceptID, //ConceptID
                (this.ddlCFDState.SelectedIndex == 0 ? b : (this.ddlCFDState.SelectedIndex == 1 ? true : false)), //PreBilling
                (this.ddlIsCredit.SelectedIndex == 0 ? b : (this.ddlIsCredit.SelectedIndex == 1 ? false : true)), //IsCredit
                this.Report_001TextFilter());

            return a;
        }
        /// <summary>
        /// Generates the arguments that will be sent to the 002 report.
        /// </summary>
        /// <returns>Returns an object of type RerportsSearchEventArgs wiht 
        /// the arguments that will be sent to the 002 report</returns>
        private ReportsSearchEventArgs Report_002()
        {
            ReportsSearchEventArgs a;
            String c = null;
            int? i = null;

            a = new ReportsSearchEventArgs(int.Parse(this.ddlAppliesTax.SelectedValue) == -1 ? i :
                                           int.Parse(this.ddlAppliesTax.SelectedValue),
                                           this.ddlProductTypes0.SelectedValue == "Todos" ? c :
                                           this.ddlProductTypes0.SelectedValue,
                                           this.Report_002TextFilter());
            return a;
        }
        /// <summary>
        /// Generates the arguments that will be sent to the 002 report.
        /// </summary>
        /// <returns>Returns an object of type RerportsSearchEventArgs wiht 
        /// the arguments that will be sent to the 003 report</returns>
        private ReportsSearchEventArgs Report_003()
        {
            ReportsSearchEventArgs a = null;
            return a;
        }
        private ReportsSearchEventArgs InvoiceReport()
        {
            ReportsSearchEventArgs a;

            int? GroupID;
            int? BillerID;
            Byte? TypeOfBill;
            DateTime? Start = null;
            DateTime? End = null;

            GroupID = this.ddlGroup.SelectedValue == "-1" ? null : ((int?)Convert.ToInt32(this.ddlGroup.SelectedValue));

            if (this.ddlBiller.Visible)
                BillerID = this.ddlBiller.SelectedValue == "-1" ? null : ((int?)Convert.ToInt32(this.ddlBiller.SelectedValue));
            else
                BillerID = null;

            TypeOfBill = this.ddlElectronicBillingType.SelectedValue == "-1" ? null : ((Byte?)Convert.ToByte(this.ddlElectronicBillingType.SelectedValue));

            if (this.cbxEnableDates.Checked)
            {
                Start = Convert.ToDateTime(this.txtStart.Text);
                End = Convert.ToDateTime(this.txtEnd.Text);
            }

            a = new ReportsSearchEventArgs(GroupID, BillerID, TypeOfBill, Start, End, this.InvoiceReportTextFilter());

            return a;
        }
        /// <summary>
        /// Generates a string with the selected filters.
        /// </summary>
        /// <returns>Returns the string generated by the selected filters.</returns>
        public String Report_001TextFilter()
        {
            String c = String.Empty;

            c += this.ddlConsumer.SelectedIndex == 0 ? "Todos los consumidores," : "El cliente " + this.ddlConsumer.SelectedItem.Text + ",";
            c += this.chkBoxIncludeDates.Checked ? " entre el rango de fechas " + this.txtDateStart.Text + " al " + this.txtDateEnd.Text + "," : String.Empty;
            c += this.ddlStatus.SelectedIndex == 0 ? " todos los estatus del comprobante," : " con el estatus del comprobante en " + this.ddlStatus.SelectedValue + ",";
            c += this.ddlProductTypes.SelectedIndex == 0 ? " todos los tipos de productos," : " el tipo de producto " + this.ddlProductTypes.SelectedValue + ",";
            c += this.ddlProductDescription.SelectedIndex == 0 ? " todas las descripciones." : " la descripción de producto " + this.ddlProductDescription.SelectedValue + ".";

            return c;
        }
        public String InvoiceReportTextFilter()
        {
            String c = String.Empty;

            c += this.ddlGroup.SelectedIndex == 0 ? "Todos los grupos, todas las empresas," : "El grupo " + this.ddlGroup.SelectedItem.Text + ",";
            c += this.ddlGroup.SelectedIndex == 0 ? String.Empty : (this.ddlBiller.SelectedIndex == 0 ? " todas las empresas," : "la empresa " + this.ddlBiller.SelectedItem.Text + ",");
            c += this.ddlElectronicBillingType.SelectedIndex == 0 ? " todos los tipos de facturación," : " con el tipo de facturación " + this.ddlElectronicBillingType.SelectedItem.Text + ",";
            c += this.cbxEnableDates.Checked ? " de la fecha " + this.txtStart.Text + " a " + this.txtEnd.Text + "." : " todas las fechas.";

            return c;
        }
        /// <summary>
        /// Generates a string with the selected filters.
        /// </summary>
        /// <returns>Returns the string generated by the selected filters.</returns>
        public String Report_002TextFilter()
        {
            String c = String.Empty;

            c += this.ddlProductTypes0.SelectedIndex == 0 ? "Todos los tipos de productos," : "El tipo de producto " + this.ddlProductTypes0.SelectedValue + ", ";
            c += this.ddlAppliesTax.SelectedIndex == -1 ? " y aplica/no aplica impuesto." : (this.ddlAppliesTax.SelectedIndex == 0 ? " y no aplica impuesto." : " y aplica impuesto.");

            return c;
        }
        /// <summary>
        /// Configurates searchfilter control, in based on control typeFilter property.
        /// </summary>
        private void ConfigureSearchFilter()
        {
            switch (this.TypeFilter)
            {
                case TypeFilter.Report_001:
                    this.dvInvoiceReport.Visible =
                    this.dvReport_002.Visible =
                    this.dvReport_003.Visible = false;
                    this.FillDropDownListConsumer();
                    this.FillDropDownListProductDescription();
                    break;
                case TypeFilter.Report_002:
                    this.dvReport_001.Visible =
                    this.dvReport_003.Visible =
                    this.dvInvoiceReport.Visible = false;
                    break;
                case TypeFilter.Report_003:
                    this.dvReport_001.Visible =
                    this.dvReport_002.Visible =
                    this.dvInvoiceReport.Visible = false;
                    break;
                case TypeFilter.Report_004:
                    this.dvReport_002.Visible =
                    this.dvReport_003.Visible =
                    this.dvInvoiceReport.Visible = false;
                    this.FillDropDownListConsumer();
                    this.FillDropDownListProductDescription();
                    break;
                case TypeFilter.InvoiceReport:
                    this.dvReport_001.Visible =
                    this.dvReport_002.Visible =
                    this.dvReport_003.Visible = false;
                    this.FillDropDownListGroup();
                    break;
            }
        }
        /// <summary>
        /// Invokes the functions of the class jclassSearchFilter and the associate with the controls.
        /// </summary>
        /// <returns>Returns true if it was possible to make the process of association, otherwise it returns false.</returns>
        private bool RegisterScript()
        {
            HtmlGenericControl body = (HtmlGenericControl)Page.Master.FindControl("MasterBody");

            try
            {
                body.Attributes["onLoad"] = String.Format("javascript:sFilter.onload('{0}')",
                     this.chkBoxIncludeDates.ClientID);

                this.chkBoxIncludeDates.Attributes["onclick"] = String.Format(
                    "javascript:sFilter.DisableValidator('{0}','{1}','{2}')",
                    this.chkBoxIncludeDates.ClientID, this.rfvStartDate.ClientID, this.rfvEndDate.ClientID);

                this.btnReset.Attributes["onclick"] = String.Format(
                    "javascript:sFilter.resetControls('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                    this.ddlConsumer.ClientID, this.ddlStatus.ClientID, this.ddlCFDState.ClientID,
                    this.ddlIsCredit.ClientID, this.ddlProductTypes.ClientID, this.ddlProductDescription.ClientID,
                    this.txtDateStart.ClientID, this.txtDateEnd.ClientID, this.chkBoxIncludeDates.ClientID);

                this.txtDateStart.Attributes["onKeyDown"] = "javascript:sFilter.preventBackspace()";
                this.txtDateEnd.Attributes["onKeyDown"] = "javascript:sFilter.preventBackspace()";

                this.txtDateStart.Attributes["onKeyPress"] = "javascript: return false;";
                this.txtDateStart.Attributes["onPaste"] = "javascript: return false;";
                this.txtDateEnd.Attributes["onKeyPress"] = "javascript: return false;";
                this.txtDateEnd.Attributes["onPaste"] = "javascript: return false;";

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }
        }
        protected void ddlGroup_SelectedIndexChanged(Object sender, EventArgs e)
        {
            int GroupID = -1;
            int.TryParse(this.ddlGroup.SelectedValue, out GroupID);

            if (GroupID == -1)
            {
                this.lblBillerTitle.Visible =
                this.ddlBiller.Visible = false;
            }
            else
            {
                this.lblBillerTitle.Visible =
                this.ddlBiller.Visible = true;
                this.FillDropDownListBillerByGroupID(GroupID);
            }
        }

        protected void cbxEnableDates_CheckedChanged(object sender, EventArgs e)
        {
            this.liDatesIR.Visible = this.cbxEnableDates.Checked;
        }
    }
}