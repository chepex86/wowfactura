﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExcelReportSearch.ascx.cs"
    Inherits="PACFDManager.Reports.ExcelReportSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div id="container" class="wframe80">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblSearchTitle" runat="server" Text="Búsqueda por cliente" />
                </h2>
            </div>
            <ul>
                <li>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="ckbIncludeDates" runat="server" Checked="true" Text="Incluir fechas" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Fecha inicial de busqueda.">
                                    <asp:Label ID="lblInitialDate" runat="server" Text="Fecha inicial." />
                                </span>
                                <br />
                                <span>
                                    <asp:TextBox ID="txtInitialDate" runat="server" />
                                    <asp:CalendarExtender ID="txtInitialDate_CalendarExtender" runat="server" TargetControlID="txtInitialDate"
                                        TodaysDateFormat="dd MM yyyy" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </span>
                                <%--<div class="validator-msg">
                        <asp:RequiredFieldValidator ID="txtInitialDate_RequiredFieldValidator" runat="server"
                            ControlToValidate="txtInitialDate" Display="Dynamic" ErrorMessage="Este campo es requerido" />
                    </div>--%>
                            </td>
                            <td>
                                <span class="vtip" title="Fecha inicial de busqueda."></span>
                                <asp:Label ID="lblEnddate" runat="server" Text="Fecha final." />
                                <br />
                                <asp:TextBox ID="txtEndDate" runat="server" />
                                <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" TargetControlID="txtEndDate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td>
                                <span class="vtip" title="Buscar in.">Pagado:</span>
                                <br />
                                <asp:DropDownList ID="ddlPaid" runat="server">
                                    <asp:ListItem Selected="true" Text="[Seleccionar]" Value="0" />
                                    <asp:ListItem Text="No pagado" Value="1" />
                                    <asp:ListItem Text="Pagado" Value="2" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <span class="vtip" title="Buscar in.">A credito:</span>
                                <br />
                                <asp:DropDownList ID="ddlIsCredit" runat="server">
                                    <asp:ListItem Selected="true" Text="[Seleccionar]" Value="0" />
                                    <asp:ListItem Text="A credito" Value="1" />
                                    <asp:ListItem Text="Contado" Value="2" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <span class="vtip" title="Buscar in.">Activo:</span>
                                <br />
                                <asp:DropDownList ID="ddlActive" runat="server">
                                    <asp:ListItem Selected="true" Text="[Seleccionar]" Value="0" />
                                    <asp:ListItem Text="Si" Value="1" />
                                    <asp:ListItem Text="No" Value="2" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </li>
                <li><span class="vtip" title="Buscar in.">
                    <asp:Label ID="lblReceptor" runat="server" Text="Cliente" /></span>
                    <br />
                    <asp:DropDownList ID="ddlReceptor" runat="server" DataTextField="Name" DataValueField="ReceptorID">
                    </asp:DropDownList>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="ddlReceptor_RequiredFieldValidator" runat="server"
                            ErrorMessage="Seleccione un cliente" InitialValue="-1" ControlToValidate="ddlReceptor"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </li>
                <%-- <li><span class="vtip" title="Buscar in.">
                    <asp:Label ID="lblConcept" runat="server" Text="Concepto" /></span>
                    <br />
                    <asp:TextBox ID="txtConcept" runat="server" />
                    <asp:AutoCompleteExtender ID="txtConcept_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetConceptsByBillersIDAndDescription"
                        FirstRowSelected="true" MinimumPrefixLength="1" EnableCaching="true" TargetControlID="txtConcept"
                        CompletionInterval="1" UseContextKey="True" />
                    <asp:HiddenField ID="hflArticleSearch" runat="server" OnValueChanged="hflArticleSearch_ValueChanged" />
                </li>--%>
                <li style="text-align: right;">
                    <asp:Button CssClass="Button_Search" ID="btnSearch" runat="server" Text="Buscar"
                        OnClick="btnSearch_Click" /></li>
            </ul>
        </div>
    </div>
</div>
