﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Reports
{
    public partial class InvoiceCut : BasePage
    {
        private PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable SearchTemporalTable
        {
            get
            {
                object o = this.Session["excel"];
                return o.IsNull() || o.GetType() != typeof(PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable) ?
                    new PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable()
                    : (PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable)o;
            }
            set { this.Session["excel"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.aDownloadXls.Visible = (this.gdvInvoiceCut.Rows.Count > 0);
        }

        protected void InvoiceCutSearch1_Search(object sender, InvoiceCutSearchEventArgs e)
        {
            GC.Collect();
            this.SearchTemporalTable = (new PACFD.Rules.Reports()).SelectBillingByGroupAndDate(e.GroupID, e.InitialDate, e.EndDate);
            this.DataBind();
        }

        public override void DataBind()
        {
            base.DataBind();
            this.gdvInvoiceCut.DataSource = this.SearchTemporalTable;
            this.gdvInvoiceCut.DataBind();
            this.aDownloadXls.Visible = (this.gdvInvoiceCut.Rows.Count > 0);
        }

        protected void gdvInvoiceCut_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvInvoiceCut.PageIndex = e.NewPageIndex;
            this.DataBind();
        }

        public string GetSelectedGroup()
        {
            string s;

            s = string.Format("&name={0}&inidate={1}&enddate={2}"
                , this.InvoiceCutSearch1.InvoiceInformation.SelectedGroup == null ? "" : this.InvoiceCutSearch1.InvoiceInformation.SelectedGroup.Text
                , this.InvoiceCutSearch1.InvoiceInformation.InitialDate == null ? "" : ((DateTime)this.InvoiceCutSearch1.InvoiceInformation.InitialDate).ToString("dd-MMMM-yyyy")
                , this.InvoiceCutSearch1.InvoiceInformation.EndDate == null ? "" : ((DateTime)this.InvoiceCutSearch1.InvoiceInformation.EndDate).ToString("dd-MMMM-yyyy")
                );

            return s;
        }
    }
}
