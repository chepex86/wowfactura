﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.Reports.BillerMonthlyReport
{
    public partial class DownloadReport : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (this.LoadFile())
                    Response.Redirect(Request.UrlReferrer.ToString());
        }
        /// <summary>
        /// Generates the file to download, based on the arguments received by QueryString:
        /// Year & Month.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        protected bool LoadFile()
        {
            if (!this.ValidateQueryString())
                return false;

            int year = Convert.ToInt32(Request.QueryString["Year"]);
            byte month = Convert.ToByte(Request.QueryString["Month"]);

            String file = SeekFile(year, month);

            if (file == null)
                return false;

            String FileName = 2 + ViewState["RFC"].ToString() +
                   (month < 10 ? "0" + month.ToString() : month.ToString()) + year + ".txt";

            Response.AddHeader("Content-disposition", "attachment; filename=" + FileName);
            Response.ContentType = "text/plain";
            Response.Write(file);
            Response.End();
            ViewState["RFC"] = null;

            return true;
        }
        /// <summary>
        /// Looking for a report.
        /// </summary>
        /// <param name="year">Report month.</param>
        /// <param name="month">Report year.</param>
        /// <returns>Returns the report's contents as a string.</returns>
        protected string SeekFile(Int32 year, byte month)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.BillerMonthlyReports_GetByDateDataTable ta;
            ta = report.GetReportByDate(this.CurrentBillerID, year, month);
            String file = "";

            if (ta.Count < 1)
                return null;

            file = ta[0].Report;
            ViewState["RFC"] = ta[0].RFC;
            ta.Dispose();

            return file;
        }
        /// <summary>
        /// Validates the contents of the variable received by querystring.
        /// </summary>
        /// <returns>Returns true if the value is different from null, not empty and is a number, otherwise returns false.</returns>
        private bool ValidateQueryString()
        {
            if (Request.QueryString["Year"].IsNull() || Request.QueryString["Month"].IsNull())
                return false;

            if (Request.QueryString["Year"].ToString() == String.Empty || Request.QueryString["Month"].ToString() == String.Empty)
                return false;

            if (Convert.ToInt32(Request.QueryString["Year"]) < 1900 && Convert.ToInt32(Request.QueryString["Year"]) > 2100 ||
                Convert.ToByte(Request.QueryString["Month"]) > 1 && Convert.ToByte(Request.QueryString["Month"]) > 12)
                return false;

            return true;
        }
    }
}