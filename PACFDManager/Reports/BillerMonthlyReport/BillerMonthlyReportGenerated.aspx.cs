﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.IO;
#endregion

namespace PACFDManager.Reports.BillerMonthlyReport
{
    public partial class BillerMonthlyReportGenerated : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.CurrentBillerID < 1)
                Response.Redirect("~/Default.aspx");

            this.Page.Title = "Reporte Mensual";
            this.Master.ProgressBar.Visible =
            this.pnlDownload.Visible =
            this.lblErrorMonth.Visible =
            this.lblError.Visible = false;
        }
        /// <summary>
        /// Fired when a fully loaded page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!this.Request.QueryString["month"].IsNull() && !this.Request.QueryString["year"].IsNull())
            {
                this.PeriodChooser1.Month = this.Request.QueryString["month"].ToInt32();
                this.PeriodChooser1.Year = this.Request.QueryString["year"].ToInt32();
            }
        }
        /// <summary>
        /// Generates the report with the specified date.
        /// </summary>
        private void LoadData()
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();

            this.txtContent.Wrap = true;

            String r = report.GenerateReport(this.CurrentBillerID, this.PeriodChooser1.Year, this.PeriodChooser1.Month, this.UserID, this.UserName);

            if (r.IsNull())
                return;

            this.txtContent.Width = 750;
            this.txtContent.Height = Convert.ToInt32(r.Split('_')[1]) * 20;
            this.txtContent.Text = r.Split('_')[0];
            this.imgBtnDownload.CommandArgument = this.PeriodChooser1.Year + "_" + this.PeriodChooser1.Month;
            this.txtContent.Visible = 
            this.pnlDownload.Visible = true;

        }
        /// <summary>
        /// Fires the PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportGenerated.btnGenerate_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable ta;
            ta = report.SelectFirstBillingByBillerID(this.CurrentBillerID);

            if (ta.Count < 1)
            {
                this.lblError.Text = "No se encontro ningun comprobante expedido para raportar al SAT.";
                this.lblError.Visible = true;
                return;
            }
            int month = this.PeriodChooser1.Month;

            DateTime dt = ta[0].BillingDate;

            String date1 = this.PeriodChooser1.Year.ToString() + (month < 10 ? "0" + month.ToString() : month.ToString());
            String date2 = dt.Year.ToString() + dt.Month.ToString();

            if (Convert.ToInt32(date1) >= Convert.ToInt32(date2))
            {
                if (SearchReports(this.PeriodChooser1.Year, this.PeriodChooser1.Month))
                {
                    if (this.PeriodChooser1.Year < DateTime.Now.Year || this.PeriodChooser1.Month <= (DateTime.Now.Month))
                        this.LoadData();
                    else
                        this.lblErrorMonth.Visible = true;
                }
                else
                {
                    this.lblError.Text = "Ya se ha generado un reporte con las fechas seleccionadas.";
                    this.lblError.Visible = true;
                    return;
                }
            }
            else
            {
                this.lblError.Text = "No es posible generar reportes con fechas anteriores al registro de la empresa en el sistema.<br />" +
                                     "Generar reportes a partir de la fecha: " + dt.Month + "/" + dt.Year + ".";
                this.lblError.Visible = true;
                return;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportGenerated.imgBtnDownloadReport_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void imgBtnDownloadReport_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;
            String y = s.CommandArgument.Split('_')[0];
            String m = s.CommandArgument.Split('_')[1];

            this.Response.Redirect(typeof(DownloadReport).Name + ".aspx?Year=" + y + "&Month=" + m);
        }
        /// <summary>
        /// Checks if a report has been generated previously.
        /// </summary>
        /// <param name="year">Report year.</param>
        /// <param name="month">Report month.</param>
        /// <returns>Returns true if the report does not exist, returns false otherwise.</returns>
        private bool SearchReports(int year, int month)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.BillerMonthlyReports_GetByDateDataTable ta;
            ta = report.GetReportByDate(this.CurrentBillerID, year, Convert.ToByte(month));

            return ta.Count < 1 ? true : false;
        }
    }
}