﻿<%@ Page Title="Generar Reporte Mensual para el SAT" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillerMonthlyReportGenerated.aspx.cs" Inherits="PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportGenerated" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="../../PeriodChooser.ascx" TagName="PeriodChooser" TagPrefix="uc1" %>
<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblMonthlyReport" runat="server" Text="Reporte Mensual para el SAT"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li><span>
                                <uc1:PeriodChooser ID="PeriodChooser1" runat="server" />
                            </span><span>
                                <uc2:Tooltip ID="tipPeriodChooser1" runat="server" ToolTip="Selecciona el mes y el año del reporte que quieras generar.<br /><b>Nota:</b> El mes debe ser menor al actual, solo se puede generar un reporte por mes al año." />
                            </span></li>
                            <li class="buttons">
                                <div>
                                    <asp:Button ID="btnGenerate" runat="server" CssClass="Button" OnClick="btnGenerate_Click"
                                        Text="Aceptar" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="Button" PostBackUrl="~/Reports/BillerMonthlyReport/BillerMonthlyReportList.aspx"
                                        Text="Cancelar" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblErrorResource1"
                Text="Ya se ha generado un reporte con las fechas seleccionadas." Visible="False"></asp:Label>
            <asp:Label ID="lblErrorMonth" runat="server" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblErrorMonthResource1"
                Text="Solo se pueden generar reportes de meses anteriores al actual." Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="txtContent" runat="server" Height="124px" meta:resourcekey="txtContentResource1"
                TextMode="MultiLine" Visible="False" Width="525px" ReadOnly="True"></asp:TextBox>
            <br />
            <asp:Panel ID="pnlDownload" runat="server">
                <table>
                    <tr>
                        <td align="center" class="Titulo">
                            <asp:Label ID="lblDownloadReport" runat="server" Text="Descargar Reporte"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgBtnDownload" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                OnClick="imgBtnDownloadReport_Click" ToolTip="Descargar Reporte" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            </td> </tr> </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
