﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Web.UI.HtmlControls;
using System.Data;
#endregion

namespace PACFDManager.Reports.BillerMonthlyReport
{
    public partial class BillerMonthlyReportList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                this.LoadList();
        }
        /// <summary>
        /// Load information for the list.
        /// </summary>
        private void LoadList()
        {
            this.LoadBillerInfo();

            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable table;
            table = report.GetReportsByBillerID(this.CurrentBillerID);
            
            this.gvReportList.DataSource = table;
            this.gvReportList.DataBind();

            if (this.gvReportList.Rows.Count < 1)
            {
                DataTable dt = new ReportsDataSet.MonthlyReports_GetByBillerIDForListDataTable();
                DataRow dr = dt.NewRow();
                dr["MonthlyReportID"] = -1;
                dr["CreationDate"] = DateTime.Now.ToString();
                dr["Month"] = 1;
                dr["Year"] = -1;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvReportList.DataSource = dt;
                this.gvReportList.DataBind();
            }
        }
        /// <summary>
        /// Generates the name file.
        /// </summary>
        /// <param name="month">byte Month</param>
        /// <param name="year">int Year</param>
        /// <returns>Returns the name file.</returns>
        protected string GenerateReportName(byte month, int year)
        {
            return (month < 10 ? "0" + month.ToString() : month.ToString()) + "/" + year;
        }
        /// <summary>
        /// Change the default value depending on the cell that contains for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvReportList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowIndex > -1)
            {
                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int i = 0; i < row.Cells.Count; i += 1)
                        row.Cells[i].Visible = false;
                    ViewState["Empty"] = null;
                }
            }

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            ReportsDataSet.MonthlyReports_GetByBillerIDForListRow dr = ((DataRowView)e.Row.DataItem).Row as ReportsDataSet.MonthlyReports_GetByBillerIDForListRow;

            HtmlAnchor download = e.Row.FindControl("aDownload") as HtmlAnchor;
            HtmlAnchor details = e.Row.FindControl("aDetails") as HtmlAnchor;

            download.HRef = typeof(DownloadReport).Name + ".aspx?Year=" + dr.Year + "&Month=" + dr.Month;
            details.HRef = typeof(BillerMonthlyReportDetails).Name + ".aspx?ReportID=" + dr.MonthlyReportID;

            LinkButton lb = e.Row.FindControl("lnkBtnSent") as LinkButton;

            if (dr.IsSentNull() || !dr.Sent)
            {
                lb.Text = "Marcar como enviado";
                lb.CommandArgument += "_1";
                row.Cells[2].BackColor = System.Drawing.Color.FromArgb(255, 199, 206);
                lb.ForeColor = System.Drawing.Color.FromArgb(172, 0, 48);
            }
            else
            {
                lb.Text = "Marcar como no enviado";
                lb.CommandArgument += "_0";
                row.Cells[2].BackColor = System.Drawing.Color.FromArgb(198, 239, 206);
                lb.ForeColor = System.Drawing.Color.FromArgb(43, 97, 45);
            }
        }
        /// <summary>
        /// Makes a search of the current biller information.
        /// </summary>
        private void LoadBillerInfo()
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.BillersDataTable ta;
            ta = biller.SelectByID(this.CurrentBillerID);
            this.FillInfo(ta);
        }
        /// <summary>
        /// Displays biller information received in a datatable.
        /// </summary>
        /// <param name="ta">BillersDataSet.BillersDataTable table</param>
        private void FillInfo(BillersDataSet.BillersDataTable ta)
        {
            lblRFC.Text = ta[0].RFC;
            lblName.Text = ta[0].Name;
            lblAddress.Text = ta[0].Address + ", " + ta[0].Colony;
        }
        /// <summary>
        /// Send a confirmation message.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void imgBtnDelete_OnClick(Object sender, EventArgs e)
        {
            ImageButton b = sender as ImageButton;

            this.WebMessageBox1.Title = "Eliminar Reporte Mensual";
            this.WebMessageBox1.ShowMessage("¿Deseas eliminar el Reporte Mensual seleccionado?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["date"] = b.CommandArgument;
        }
        /// <summary>
        /// If you accept the delete confirmation to the selected report, otherwise returns to the list of users.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet ds = new ReportsDataSet();
            ReportsDataSet.BillerMonthlyReports_GetByDateDataTable t;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            int year = Convert.ToInt32(this.ViewState["date"].ToString().Split('_')[0]);
            byte month = Convert.ToByte(this.ViewState["date"].ToString().Split('_')[1]);
            t = report.GetReportByDate(this.CurrentBillerID, year, month);

            ds.MonthlyReports.Merge(t);

            String BeforeDataSet = ds.GetXml();

            if (ds.MonthlyReports.Count > 0)
            {
                ReportsDataSet.MonthlyReportsRow row = ds.MonthlyReports[0];
                row.Delete();
            }
            if (!report.BillerMonthlyReportsUpdate(ds.MonthlyReports))
            {
                LogManager.WriteStackTrace(new Exception("Error al tratar de eliminar el Reporte Mensual"));
                this.WebMessageBox1.Title = "Eliminar Reporte Mensual";
                WebMessageBox1.ShowMessage("Error al tratar de eliminar el Reporte Mensual.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                return;
            }
            else
            {
                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino el reporte mensual para el SAT {1}/{2}",
                    this.UserName, month, year), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
                #endregion

                this.ViewState["date"] = null;
                Response.Redirect("~/Reports/BillerMonthlyReport/BillerMonthlyReportList.aspx");
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportList.lnkBtnSent.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lnkBtnSent_OnClick(Object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            int ReportID = Convert.ToInt32(lb.CommandArgument.Split('_')[0]);

            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet ds = new ReportsDataSet();

            report.SelectByReportID(ds.MonthlyReports, ReportID);

            if (ds.MonthlyReports.Count > 0)
            {
                ReportsDataSet.MonthlyReportsRow row = ds.MonthlyReports[0];
                row.Sent = lb.CommandArgument.Split('_')[1] == "1" ? true : false;

                if (lb.CommandArgument.Split('_')[1] == "1") row.SentDate = DateTime.Now;
                else row.SetSentDateNull();

                if (!report.BillerMonthlyReportsUpdate(ds.MonthlyReports))
                    return;

                this.Response.Redirect(typeof(BillerMonthlyReportList).Name + ".aspx");
            }
        }
        /// <summary>
        /// Receives the number of months and returns the name associated with that month.
        /// </summary>
        /// <param name="month">int month</param>
        /// <returns>Returns a string with the name of the month.</returns>
        protected String FormatDate(int month)
        {
            String Date = String.Empty;

            switch (month)
            {
                case 1: Date = "Enero"; break;
                case 2: Date = "Febrero"; break;
                case 3: Date = "Marzo"; break;
                case 4: Date = "Abril"; break;
                case 5: Date = "Mayo"; break;
                case 6: Date = "Junio"; break;
                case 7: Date = "Julio"; break;
                case 8: Date = "Agosto"; break;
                case 9: Date = "Septiembre"; break;
                case 10: Date = "Octubre"; break;
                case 11: Date = "Noviembre"; break;
                case 12: Date = "Diciembre"; break;                   
            }

            return Date;
        }
    }
}