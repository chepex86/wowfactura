﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Reports
{
    public partial class ExcelReportList : PACFDManager.BasePage
    {
        const string MESSAGE_BILLINGS_EMPTY = "MESSAGE_BILLINGS_EMPTY";

        /// <summary>
        /// Temporal data set used to save the search.
        /// </summary>
        private PACFD.DataAccess.BillingExcelDataSet TemporalDataSet
        {
            get
            {
                object o = this.Session["excel"];
                return o.IsNull() || o.GetType() != typeof(PACFD.DataAccess.BillingExcelDataSet) ? null : (PACFD.DataAccess.BillingExcelDataSet)o;
            }
            set { this.Session["excel"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.divSearchData.Visible = (this.gdvExcelReport.Rows.Count > 0);
        }

        protected void ExcelReportSearch1_Search(object sender, ExcelReportSearchEventArgs e)
        {
            if (e.ReceptorID < 1)
            { return; }

            GC.Collect();
            this.TemporalDataSet = (new PACFD.Rules.BillingExcel()).GetDataSetForExcel(e.ReceptorID, e.InitialDate, e.EndDate, e.IsPaid, e.IsCredit, e.IsActive);
            this.DataBind();

            using (PACFD.DataAccess.ReceptorsDataSet.ReceptorsDataTable table = (new PACFD.Rules.Receptors()).SelectByID(this.ExcelReportSearch1.ExcelInformation.ReceptorID))
            { this.lblReceptorName.Text = table.Count < 1 ? "" : table[0].Name; }

            this.lblActualDate.Text = DateTime.Now.ToString("dd/MMMM/yyyy");

            if (this.gdvExcelReport.Rows.Count < 1)
            { this.WebMessageBox1.ShowMessage("No se encontraron facturas", WebMessageBoxButtonType.Accept); }
        }

        protected void gdvExcelReport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Label l;
            int billingid = 0;
            decimal total = 0;

            if (e.Row.RowType != DataControlRowType.DataRow)
            { return; }

            billingid = this.TemporalDataSet.Billings[e.Row.DataItemIndex].BillingID;
            l = e.Row.FindControl("lblPaidTotal") as Label;
            var sum = from x in ((PACFD.DataAccess.BillingExcelDataSet.BillingPaymentsDataTable)this.TemporalDataSet.Tables["BillingPayments"])
                      where x.BillingID == billingid
                      select x.Amount;
            l.Text = sum.Sum().ToString("C");
            l = e.Row.FindControl("lblToPaid") as Label;
            total = this.TemporalDataSet.Billings[e.Row.DataItemIndex].Total;
            l.Text = (total - sum.Sum()).ToString("C");
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {

        }

        protected void gdvExcelReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gdvExcelReport.PageIndex = e.NewPageIndex;
            this.DataBind();
        }

        public override void DataBind()
        {
            base.DataBind();
            this.gdvExcelReport.DataSource = this.TemporalDataSet == null ? null : this.TemporalDataSet.Billings;
            this.gdvExcelReport.DataBind();
            this.divSearchData.Visible = (this.gdvExcelReport.Rows.Count > 0);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            System.IO.MemoryStream bookstream, pdfstream;

            using (DownloadExcel d = new DownloadExcel())
            {
                d.StartSearchDate = this.ExcelReportSearch1.ExcelInformation.InitialDate.IsNull() ?
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : (DateTime)this.ExcelReportSearch1.ExcelInformation.InitialDate;
                d.EndSearchDate = this.ExcelReportSearch1.ExcelInformation.EndDate.IsNull() ?
                    DateTime.Now : (DateTime)this.ExcelReportSearch1.ExcelInformation.EndDate;

                pdfstream = d.SendPdfExcelReport();
                bookstream = d.SendXlsExcelReport();
            }
        }
    }
}
