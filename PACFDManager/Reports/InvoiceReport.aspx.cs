﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
#endregion

namespace PACFDManager.Reports
{
    public partial class InvoiceReport : BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReportViewer1.Visible = false;
            this.SearchFilter1.SetTypeFilter(TypeFilter.InvoiceReport);
            this.SearchFilter1.Search += new SearchFilter.SearchEventHandler(SearchFilter1_Search);
        }

        void SearchFilter1_Search(object sender, ReportsSearchEventArgs e)
        {
            PACFD.Rules.CustomReports report = new PACFD.Rules.CustomReports();
            DataTable dt;

            dt = report.InvoiceReport(e.GroupID, e.BillerID, e.ElectronicBillingType, e.Start, e.End);

            this.ReportViewer1.Visible = true;

            this.ReportViewer1.FileName = "~/Reports/CrystalReports/crInvoiceReport.rpt";
            this.ReportViewer1.TextFilter = e.TextFilter;
            this.ReportViewer1.SetDataSource = dt;
        }
    }
}