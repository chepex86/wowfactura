﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Reports
{
    public partial class InvoiceCutSearch : System.Web.UI.UserControl
    {
        /// <summary>
        /// Event declaration. 
        /// </summary>
        public event InvoiceCutSearchEventHandler Search;

        public InvoiceCutSearchInformation InvoiceInformation { get { return new InvoiceCutSearchInformation(this); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            { return; }

            this.txtInitialDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            this.txtEndDate.Text = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).Subtract(new TimeSpan(1, 0, 0, 0)).ToString("dd/MM/yyyy");
            this.ddlGroup.DataSource = (new PACFD.Rules.Groups()).SelectAll();
            this.ddlGroup.DataBind();
            this.ddlGroup.ClearSelection();
            this.ddlGroup.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
        }
        /// <summary>
        /// Invoke the search method.
        /// </summary>
        public void InvokeSearch()
        {
            InvoiceCutSearchEventArgs e = new InvoiceCutSearchEventArgs(
                this.InvoiceInformation.SelectedGroup == null ? null : (int?)int.Parse(this.InvoiceInformation.SelectedGroup.Value)
                , this.ckbIncludeDates.Checked ? this.InvoiceInformation.InitialDate : (DateTime?)null
                , this.ckbIncludeDates.Checked ? this.InvoiceInformation.EndDate : (DateTime?)null);

            this.OnSearch(e);
        }

        protected virtual void OnSearch(InvoiceCutSearchEventArgs e)
        {
            if (!this.Search.IsNull())
            { this.Search(this, e); }
        }

        /// <summary>
        /// Nested class used to reatrive information.
        /// </summary>
        public class InvoiceCutSearchInformation
        {
            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Owner of the class.</param>
            public InvoiceCutSearchInformation(InvoiceCutSearch owner) { this.Parent = owner; }

            /// <summary>
            /// Owner of the class.
            /// </summary>
            private InvoiceCutSearch Parent { get; set; }
            /// <summary>
            /// Initial date of the search
            /// </summary>
            public DateTime? InitialDate
            {
                get
                {
                    DateTime d;
                    return string.IsNullOrEmpty(this.Parent.txtInitialDate.Text) || !DateTime.TryParse(this.Parent.txtInitialDate.Text, out d) ? null : (DateTime?)d;
                }
                set
                {
                    if (value.IsNull())
                    { this.Parent.txtInitialDate.Text = ""; }
                    else
                    { this.Parent.txtInitialDate.Text = ((DateTime)value).ToString("dd/MM/yyyy"); }
                }
            }
            /// <summary>
            /// End date of the search.
            /// </summary>
            public DateTime? EndDate
            {
                get
                {
                    DateTime d;
                    return string.IsNullOrEmpty(this.Parent.txtEndDate.Text) || !DateTime.TryParse(this.Parent.txtEndDate.Text, out d) ? null : (DateTime?)d;
                }
                set
                {
                    if (value.IsNull())
                    { this.Parent.txtEndDate.Text = ""; }
                    else
                    { this.Parent.txtEndDate.Text = ((DateTime)value).ToString("dd/MM/yyyy"); }
                }
            }

            public ListItem SelectedGroup
            {
                get
                {
                    if (this.Parent.ddlGroup.SelectedIndex < 1)
                    { return null; }

                    return this.Parent.ddlGroup.SelectedItem;
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.InvokeSearch();
        }
    }
    /// <summary>
    /// Delegate event handler.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the onwer.</param>
    public delegate void InvoiceCutSearchEventHandler(object sender, InvoiceCutSearchEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    public class InvoiceCutSearchEventArgs : EventArgs
    {
        /// <summary>
        /// Initial date of the search
        /// </summary>
        public DateTime? InitialDate { get; private set; }
        /// <summary>
        /// End date of the search.
        /// </summary>
        public DateTime? EndDate { get; private set; }
        public int? GroupID { get; private set; }

        /// <summary>
        /// Create a enw instance of the class.
        /// </summary>
        /// <param name="initialdate"></param>
        /// <param name="enddate"></param>
        public InvoiceCutSearchEventArgs(int? groupid, DateTime? initialdate, DateTime? enddate)
        {
            this.InitialDate = initialdate;
            this.EndDate = enddate;
            this.GroupID = groupid;
        }
    }
}