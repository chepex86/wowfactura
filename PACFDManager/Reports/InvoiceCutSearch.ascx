﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceCutSearch.ascx.cs"
    Inherits="PACFDManager.Reports.InvoiceCutSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div id="container" class="wframe80">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblSearchTitle" runat="server" Text="Búsqueda de empresa" />
                </h2>
            </div>
            <ul>
                <li>
                    <asp:CheckBox ID="ckbIncludeDates" runat="server" Text="Incluir fechas" Checked="true" />
                </li>
                <li>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <span class="vtip" title="Nombre del grupo a buscar.">
                                    <asp:Label ID="Label1" runat="server" Text="Grupo a buscar." />
                                </span>
                                <br />
                                <asp:DropDownList ID="ddlGroup" runat="server" DataTextField="Name" DataValueField="GroupID">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <span class="vtip" title="Fecha inicial de busqueda.">
                                    <asp:Label ID="lblInitialDate" runat="server" Text="Fecha inicial." />
                                </span>
                                <br />
                                <span>
                                    <asp:TextBox ID="txtInitialDate" runat="server" />
                                    <asp:CalendarExtender ID="txtInitialDate_CalendarExtender" runat="server" TargetControlID="txtInitialDate"
                                        TodaysDateFormat="dd MM yyyy" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </span>
                            </td>
                            <td>
                                <span class="vtip" title="Fecha inicial de busqueda."></span>
                                <asp:Label ID="lblEnddate" runat="server" Text="Fecha final." />
                                <br />
                                <asp:TextBox ID="txtEndDate" runat="server" />
                                <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" TargetControlID="txtEndDate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </li>
                <li style="text-align: right;">
                    <asp:Button CssClass="Button_Search" ID="btnSearch" runat="server" Text="Buscar"
                        OnClick="btnSearch_Click" />
                </li>
            </ul>
        </div>
    </div>
</div>
