﻿<%@ Page Title="Reporte de Productos" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="Report_002.aspx.cs" Inherits="PACFDManager.Reports.Report_002" %>

<%@ Register Src="ReportViewer.ascx" TagName="ReportViewer" TagPrefix="uc1" %>
<%@ Register Src="../Concepts/ProductTypes.ascx" TagName="ProductTypes" TagPrefix="uc2" %>
<%@ Register Src="SearchFilter.ascx" TagName="SearchFilter" TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <div id="container" class="wframe50">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Reporte de Productos "></asp:Label>
                    </h2>
                </div>
                <div>
                    <uc3:SearchFilter ID="SearchFilter1" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div>
        <uc1:ReportViewer ID="ReportViewer1" runat="server" FileName="~/Reports/CrystalReports/crReport_002.rpt" />
    </div>
</asp:Content>
