﻿<%@ Page Title="Reporte de Productos en Comprobante" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="Report_004.aspx.cs" Inherits="PACFDManager.Reports.Report_004" %>

<%@ Register Src="SearchFilter.ascx" TagName="SearchFilter" TagPrefix="uc1" %>
<%@ Register Src="ReportViewer.ascx" TagName="ReportViewer" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Reporte de Productos en Comprobante"></asp:Label>
                    </h2>
                </div>
                <div>
                    <uc1:SearchFilter ID="SearchFilter1" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div>
        <uc2:ReportViewer ID="ReportViewer1" runat="server" FileName="~/Reports/CrystalReports/crReport_004.rpt" />
    </div>
</asp:Content>
