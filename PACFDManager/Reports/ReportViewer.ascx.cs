﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Common;
using CrystalDecisions.Shared;
using System.IO;
#endregion

namespace PACFDManager.Reports
{
    public partial class ReportViewer : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e) { }
        /// <summary>
        /// Set visible property.
        /// </summary>
        public override bool Visible
        {
            set
            {
                this.CRS1.Visible =
                this.ddlTypes.Visible =
                this.btnExport.Visible =
                this.CrystalReportViewer1.Visible = value;
            }
        }
        /// <summary>
        /// Set file name property.
        /// </summary>
        public String FileName
        {
            set { this.CRS1.Report.FileName = value; }
        }
        /// <summary>
        /// Set text filter property.
        /// </summary>
        public String TextFilter
        {
            set
            {
                if (this.CRS1.Report.Parameters.Count > 0)
                    this.CRS1.Report.Parameters[0].DefaultValue = value;
            }
        }
        /// <summary>
        /// Set data source property.
        /// </summary>
        public DataTable SetDataSource
        {
            set
            {
                if (!value.IsNull())
                {
                    this.CRS1.ReportDocument.SetDataSource(value);
                    this.CrystalReportViewer1.ReportSource = CRS1;
                }
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.ReportViewer.btnExport_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            var rptStream = CRS1.ReportDocument.ExportToStream((ExportFormatType)Convert.ToInt32(this.ddlTypes.SelectedValue));
            var b = new byte[rptStream.Length];
            rptStream.Read(b, 0, Convert.ToInt32(rptStream.Length));
            Response.Clear();
            Response.Buffer = true;

            String ContentType = DocumentFormat().Split('_')[0];
            String ReportExtention = DocumentFormat().Split('_')[1];

            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Report" + ReportExtention);
            Response.ContentType = ContentType;
            Response.BinaryWrite(b);
            Response.End();
        }
        /// <summary>
        /// Sets the format you will export the report.
        /// </summary>
        /// <returns>Returns a string in the format that will be exported to the report.</returns>
        private String DocumentFormat()
        {
            String type = String.Empty;
            String ReportName = String.Empty;

            switch ((ExportFormatType)int.Parse(this.ddlTypes.SelectedValue))
            {
                case ExportFormatType.Excel:
                    type = "application/vnd.ms-excel";
                    ReportName = ".xls";
                    break;
                case ExportFormatType.RichText:
                    type = "application/rtf";
                    ReportName = ".rtf";
                    break;
                case ExportFormatType.WordForWindows:
                    type = "application/msword";
                    ReportName = ".doc";
                    break;
                default:
                    type = "application/pdf";
                    ReportName = ".pdf";
                    break;
            }
            return type + "_" + ReportName;
        }
    }
}