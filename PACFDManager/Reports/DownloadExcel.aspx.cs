﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;

using PdfSharp.Pdf;
using PdfSharp.Drawing;
#endregion

namespace PACFDManager.Reports
{
    public partial class DownloadExcel : System.Web.UI.Page
    {
        /// <summary>
        /// Download report type file
        /// </summary>
        private enum DownloadFileType
        {
            None = 0,
            ExcelReportXls = 1,
            InvoiceCutXls = 2,
            ExcelReportPdf = 3,
        }

        private DateTime? startSearchDate;
        private DateTime? endSearchTime;

        /// <summary>
        /// Download file type.
        /// </summary>
        private DownloadFileType FileType
        {
            get
            {
                string s;

                if (string.IsNullOrEmpty(s = this.Request.QueryString["downloadtype"]))
                { return DownloadFileType.None; }

                switch (s.ToLower())
                {
                    case "excelreport": return DownloadFileType.ExcelReportXls;
                    case "invoicecut": return DownloadFileType.InvoiceCutXls;
                    case "pdfreport": return DownloadFileType.ExcelReportPdf;
                }

                return DownloadFileType.None;
            }
        }
        /// <summary>
        /// Get the start date of the date.
        /// </summary>
        public DateTime StartSearchDate
        {
            get
            {
                if (this.startSearchDate != null)
                    return (DateTime)this.startSearchDate;

                DateTime d = new DateTime();
                string s = this.Request.QueryString["idate"];
                return !DateTime.TryParse(s, out d) ? DateTime.Now : d;
            }
            set { this.startSearchDate = (DateTime?)value; }
        }
        /// <summary>
        /// Get the end date of the search.
        /// </summary>
        public DateTime EndSearchDate
        {
            get
            {
                if (this.endSearchTime != null)
                    return (DateTime)this.endSearchTime;

                DateTime d = new DateTime();
                string s = this.Request.QueryString["edate"];
                return !DateTime.TryParse(s, out d) ? DateTime.Now : d;
            }
            set { this.endSearchTime = (DateTime?)value; }
        }
        /// <summary>
        /// Temporal data set.
        /// </summary>
        private System.Data.DataSet DataSet
        {
            get
            {
                object o = this.Session["excel"];
                return o.IsNull() || (o as System.Data.DataSet).IsNull() ? null : (System.Data.DataSet)o;
            }
            set { this.Session["excel"] = value; }
        }
        /// <summary>
        /// Temporal data table.
        /// </summary>
        private System.Data.DataTable DataTable
        {
            get
            {
                object o = this.Session["excel"];
                return o.IsNull() || (o as System.Data.DataTable).IsNull() ? null : (System.Data.DataTable)o;
            }
            set { this.Session["excel"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.FileType != DownloadFileType.None && (this.DataSet != null || this.DataTable != null))
            {
                switch (this.FileType)
                {
                    case DownloadFileType.ExcelReportXls: this.SendWoorkBook(this.SendXlsExcelReport()); break;
                    case DownloadFileType.InvoiceCutXls: this.SendWoorkBook(this.SendXlsInvoiceCut()); break;
                    case DownloadFileType.ExcelReportPdf: this.SendWoorkBook(this.SendPdfExcelReport()); break;
                }

                this.Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        void SendWoorkBook(System.IO.MemoryStream book)
        {
            byte[] b;

            if (book.IsNull())
                return;

            book.Seek(0, System.IO.SeekOrigin.Begin);
            b = new byte[book.Length];
            book.Read(b, 0, b.Length);

            this.Context.Response.ClearContent();
            this.Context.Response.ClearHeaders();
            this.Context.Response.ContentType = "application/octet-stream";
            this.Context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={1} {2} {3}.{0}"
                , this.FileType == DownloadFileType.ExcelReportXls | this.FileType == DownloadFileType.InvoiceCutXls ? "xls" :
                    this.FileType == DownloadFileType.ExcelReportPdf ? "pdf" : "unknown"
                , this.FileType.ToString()
                , DateTime.Now.ToString("ddMMMMyyyy")
                , System.Environment.TickCount
                ));
            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.End();
        }

        /// <summary>
        /// Get the invoice cut report.
        /// </summary>
        /// <returns>If success return true else false.</returns>
        System.IO.MemoryStream SendXlsInvoiceCut()
        {
            const int xini = 1, yini = 3;
            int x = 1, y = 1;
            HSSFWorkbook book;
            HSSFSheet sheet;
            HSSFCell cell;
            HSSFCellStyle style;
            HSSFDataFormat format;
            HSSFFont font;
            PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable data;

            data = this.DataTable as PACFD.DataAccess.ReportsDataSet.Reports_InvoiceCutDataTable;

            if (data.IsNull())
                return null;

            book = new HSSFWorkbook();
            sheet = book.CreateSheet("Corte de facturas") as HSSFSheet;

            font = book.CreateFont() as HSSFFont;
            style = book.CreateCellStyle() as HSSFCellStyle;
            font.Boldweight = short.MaxValue;
            style.SetFont(font);
            cell = sheet.CreateRow(yini - 2).CreateCell(xini) as HSSFCell;
            cell.SetCellValue("Fecha de creación:");
            cell.CellStyle = style;

            cell = sheet.GetRow(yini - 2).CreateCell(xini + 1) as HSSFCell;
            cell.SetCellValue(DateTime.Now.ToString("dd/MMMM/yyyy"));

            sheet.CreateRow(yini - 1).CreateCell(xini).SetCellValue(string.Format("Del {0} a {1}"
                , this.Request.QueryString["inidate"], this.Request.QueryString["enddate"]));
            sheet.GetRow(yini - 1).CreateCell(xini + 8).SetCellValue(string.Format("Nombre:{0}", this.Request.QueryString["name"]));

            x = xini;
            y = yini;

            font = book.CreateFont() as HSSFFont;
            style = book.CreateCellStyle() as HSSFCellStyle;
            font.Color = HSSFColor.White.Index;
            font.Boldweight = short.MaxValue;
            style.SetFont(font);
            style.FillPattern = FillPattern.SolidForeground;
            style.FillForegroundColor = HSSFColor.Blue.Index;
            style.Alignment = HorizontalAlignment.Center;

            sheet.SetColumnWidth(x, 2000);
            cell = sheet.CreateRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("ID");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Emisor");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Tipo");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("RFC Ems.");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Cliente");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("RFC Cln.");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Folio");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("UUID");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Folio Extr.");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Total");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Moneda");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Fecha");
            cell.CellStyle = style;

            sheet.SetColumnWidth(x, 4000);
            cell = sheet.GetRow(yini).CreateCell(x++) as HSSFCell;
            cell.SetCellValue("Estado");
            cell.CellStyle = style;

            x = xini;

            for (y = 0; y < data.Count; y++)
            {
                font = book.CreateFont() as HSSFFont;
                style = book.CreateCellStyle() as HSSFCellStyle;
                style.SetFont(font);
                style.Alignment = HorizontalAlignment.Center;
                cell = sheet.CreateRow(y + yini + 1).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data[y].ID);
                cell.CellStyle = style;

                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].Emisor);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].Type);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].RfcEmisor);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].Receptor);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].RfcReceptor);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].Folio);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].UUID);
                sheet.GetRow(y + yini + 1).CreateCell(x++).SetCellValue(data[y].ExternalFolio);

                style = book.CreateCellStyle() as HSSFCellStyle;
                style.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
                cell = sheet.GetRow(y + yini + 1).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(Convert.ToDouble(data[y].Total));
                cell.CellStyle = style;

                cell = sheet.GetRow(y + yini + 1).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data[y].Money);
                cell.CellStyle = style;

                style = book.CreateCellStyle() as HSSFCellStyle;
                format = book.CreateDataFormat() as HSSFDataFormat;
                style.DataFormat = format.GetFormat("dd/MMMM/yyyy");
                cell = sheet.GetRow(y + yini + 1).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data[y].Date.ToString("dd/MMMM/yyyy"));
                cell.CellStyle = style;

                font = book.CreateFont() as HSSFFont;
                style = book.CreateCellStyle() as HSSFCellStyle;
                style.SetFont(font);
                style.Alignment = HorizontalAlignment.Center;
                cell = sheet.GetRow(y + yini + 1).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data[y].Status);
                cell.CellStyle = style;

                x = xini;
            }

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            book.Write(stream);

            return stream;
        }

        /// <summary>
        /// Get the billing state report.
        /// </summary>
        /// <returns>If success return true else false.</returns>
        public System.IO.MemoryStream SendXlsExcelReport()
        {
            int x = 0, y = 0;
            string sfile = null;
            decimal saldo = 0;
            HSSFWorkbook book;
            HSSFSheet sheet;
            HSSFCell cell;
            HSSFCellStyle style;
            HSSFDataFormat format;
            HSSFFont font;
            PACFD.DataAccess.BillingExcelDataSet data;

            data = this.DataSet as PACFD.DataAccess.BillingExcelDataSet;

            if (data.IsNull())
                return null;

            book = new NPOI.HSSF.UserModel.HSSFWorkbook();
            sheet = book.CreateSheet("Estado de cuenta") as HSSFSheet;

            y = 4;
            x = 3;

            font = book.CreateFont() as HSSFFont;
            style = book.CreateCellStyle() as HSSFCellStyle;
            font.Boldweight = short.MaxValue;
            style.SetFont(font);
            cell = sheet.CreateRow(y).CreateCell(3) as HSSFCell;
            cell.SetCellValue("Cliente:");
            cell.CellStyle = style;

            sheet.GetRow(y).CreateCell(4).SetCellValue(data.Billings[0].ReceptorName);

            font = book.CreateFont() as HSSFFont;
            style = book.CreateCellStyle() as HSSFCellStyle;
            font.Boldweight = short.MaxValue;
            style.SetFont(font);
            cell = sheet.GetRow(4).CreateCell(10) as HSSFCell;
            cell.SetCellValue("Fecha:");
            cell.CellStyle = style;

            sheet.GetRow(y).CreateCell(11).SetCellValue(DateTime.Now.ToString("dd/MM/yyyy"));

            ++y;

            cell = sheet.CreateRow(y).CreateCell(8) as HSSFCell;
            cell.SetCellValue("Desde");
            cell.CellStyle = style;
            sheet.GetRow(y).CreateCell(9).SetCellValue(this.StartSearchDate.ToString("dd/MM/yyyy"));

            cell = sheet.GetRow(y).CreateCell(10) as HSSFCell;
            cell.SetCellValue("A");
            cell.CellStyle = style;
            sheet.GetRow(y).CreateCell(11).SetCellValue(this.EndSearchDate.ToString("dd/MM/yyyy"));

            ++y;

            font = book.CreateFont() as HSSFFont;
            style = book.CreateCellStyle() as HSSFCellStyle;
            font.Color = HSSFColor.White.Index;
            font.Boldweight = short.MaxValue;
            style.SetFont(font);
            style.FillPattern = FillPattern.SolidForeground;
            style.FillForegroundColor = HSSFColor.Blue.Index;
            style.Alignment = HorizontalAlignment.Center;
            cell = sheet.CreateRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Fecha");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Comprobante No.");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Concepto");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Importe");
            cell.CellStyle = style;
            ++x;

            //-----------------------------------------------------
            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 1500);
            cell.SetCellValue("Moneda");
            cell.CellStyle = style;
            ++x;
            //-----------------------------------------------------

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Pagos");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Saldo");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 1500);
            cell.SetCellValue("Credito");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 1500);
            cell.SetCellValue("Activa");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 4000);
            cell.SetCellValue("Referencia");
            cell.CellStyle = style;
            ++x;

            cell = sheet.GetRow(y).CreateCell(x) as HSSFCell;
            sheet.SetColumnWidth(x, 8000);
            cell.SetCellValue("Tipo Comprobante");
            cell.CellStyle = style;
            ++x;

            y = 7;

            for (int i = 0; i < data.Billings.Count; i++)
            {
                style = book.CreateCellStyle() as HSSFCellStyle;
                format = book.CreateDataFormat() as HSSFDataFormat;
                style.DataFormat = format.GetFormat("dd/MMMM/yyyy");

                x = 3;
                cell = sheet.CreateRow(y).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data.Billings[i].BillingDate.ToString("dd/MMMM/yyyy"));
                cell.CellStyle = style;

                cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(string.Format("{0}-{1}", data.Billings[i].Serial, data.Billings[i].Folio));

                {
                    var dr = from r in data.BillingsDetails
                             where r.BillingID == data.Billings[i].BillingID
                             select r.Description;

                    sheet.GetRow(y).CreateCell(x++).SetCellValue(sfile = string.Join("\n", dr.ToArray()));
                }

                style = book.CreateCellStyle() as HSSFCellStyle;
                style.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");

                cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(Convert.ToDouble(data.Billings[i].Total));
                cell.CellStyle = style;

                cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                cell.SetCellValue(data.Billings[i].CurrencyCode);

                if (!data.Billings[i].IsIsCreditNull() && !data.Billings[i].IsCredit)
                {
                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(Convert.ToDouble(data.Billings[i].Total));
                    cell.CellStyle = style;

                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;

                    if (saldo > 0)
                    {
                        style = book.CreateCellStyle() as HSSFCellStyle;
                        style.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
                        cell.SetCellValue(Convert.ToDouble(saldo));
                        cell.CellStyle = style;
                    }
                    else
                    {
                        font = book.CreateFont() as HSSFFont;
                        style = book.CreateCellStyle() as HSSFCellStyle;
                        font.Boldweight = short.MaxValue;
                        style.SetFont(font);
                        style.Alignment = HorizontalAlignment.Center;
                        cell.SetCellValue("-");
                        cell.CellStyle = style;
                    }

                    font = book.CreateFont() as HSSFFont;
                    style = book.CreateCellStyle() as HSSFCellStyle;
                    style.SetFont(font);
                    style.Alignment = HorizontalAlignment.Center;
                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(data.Billings[i].IsCredit ? "Si" : "No");
                    cell.CellStyle = style;

                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(data.Billings[i].Active ? "Si" : "No");
                    cell.CellStyle = style;

                    sheet.GetRow(y).CreateCell(13).SetCellValue(
                    data.Billings[i].PreBilling && data.Billings[i].IsFolioIDNull() ? "Precomprobante no foliado no sellado"
                    : data.Billings[i].PreBilling ? "Precomprobante no sellado" : "Comprobante");
                }
                else
                {
                    var dr = from r in data.BillingPayments
                             where r.BillingID == data.Billings[i].BillingID
                             select r;

                    ++x;

                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(Convert.ToDouble(saldo = data.Billings[i].Total + saldo));
                    cell.CellStyle = style;

                    font = book.CreateFont() as HSSFFont;
                    style = book.CreateCellStyle() as HSSFCellStyle;
                    style.SetFont(font);
                    style.Alignment = HorizontalAlignment.Center;
                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(data.Billings[i].IsCredit ? "Si" : "No");
                    cell.CellStyle = style;

                    cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                    cell.SetCellValue(data.Billings[i].Active ? "Si" : "No");
                    cell.CellStyle = style;

                    x = 3;

                    sheet.GetRow(y).CreateCell(13).SetCellValue(
                    data.Billings[i].PreBilling && data.Billings[i].IsFolioIDNull() ? "Precomprobante no foliado no sellado"
                    : data.Billings[i].PreBilling ? "Precomprobante no sellado" : "Comprobante");

                    if (dr.Any())
                    {
                        foreach (PACFD.DataAccess.BillingExcelDataSet.BillingPaymentsRow rw in dr)
                        {
                            y++;
                            sheet.CreateRow(y).CreateCell(x++).SetCellValue(rw.PaidDate.ToString("dd/MMMM/yyyy"));
                            sheet.GetRow(y).CreateCell(x++).SetCellValue(string.Format("{0}-{1}", data.Billings[i].Serial, data.Billings[i].Folio));
                            sheet.GetRow(y).CreateCell(x++).SetCellValue(sfile);

                            //-----------------------
                            //second column
                            //-----------------------
                            ++x;
                            ++x;

                            style = book.CreateCellStyle() as HSSFCellStyle;
                            style.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
                            cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                            cell.SetCellValue(Convert.ToDouble(rw.Amount));
                            cell.CellStyle = style;

                            cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;

                            if ((saldo = saldo - rw.Amount) > 0)
                            {
                                cell.SetCellValue(Convert.ToDouble(saldo));
                                cell.CellStyle = style;
                            }
                            else
                            {
                                font = book.CreateFont() as HSSFFont;
                                style = book.CreateCellStyle() as HSSFCellStyle;
                                font.Boldweight = short.MaxValue;
                                style.SetFont(font);
                                style.Alignment = HorizontalAlignment.Center;
                                cell.SetCellValue("-");
                                cell.CellStyle = style;
                            }

                            font = book.CreateFont() as HSSFFont;
                            style = book.CreateCellStyle() as HSSFCellStyle;
                            style.SetFont(font);
                            style.Alignment = HorizontalAlignment.Center;
                            cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                            cell.SetCellValue(data.Billings[i].IsCredit ? "Si" : "No");
                            cell.CellStyle = style;

                            cell = sheet.GetRow(y).CreateCell(x++) as HSSFCell;
                            cell.SetCellValue(data.Billings[i].Active ? "Si" : "No");
                            cell.CellStyle = style;

                            sheet.GetRow(y).CreateCell(x++).SetCellValue(rw.Reference);
                            //13
                            sheet.GetRow(y).CreateCell(x).SetCellValue(
                                data.Billings[i].PreBilling && data.Billings[i].IsFolioIDNull() ? "Precomprobante no foliado no sellado"
                                : data.Billings[i].PreBilling ? "Precomprobante no sellado" : "Comprobante");

                            x = 3;
                        }
                    }
                }

                ++y;
            }

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            book.Write(stream);
            book.Dispose();

            return stream;
        }
        public System.IO.MemoryStream SendPdfExcelReport()
        {
            int i = 0
                , y = 20
                , ilinesused = 0
                , icurrentline = 0
                , imaxline = 0
                ;
            const int token_date_x = 10;
            const int token_folio_x = 55;
            const int token_concept_x = 90;
            const int token_import_x = 300;

            const int token_ammount_x = 350;
            const int token_saldo_x = 400;
            const int token_credit_x = 428;
            const int token_active_x = 458;
            const int token_reference_x = 490;
            const int token_type_x = 538;

            const int token_new_page = 740;

            PdfDocument doc = new PdfDocument();
            PdfPage page;
            XGraphics g;
            XFont font = new XFont("Arial", 8);
            XSolidBrush brush = new XSolidBrush(System.Drawing.Color.Black);
            PACFD.DataAccess.BillingExcelDataSet data = this.DataSet as PACFD.DataAccess.BillingExcelDataSet;
            PACFD.DataAccess.BillingExcelDataSet.BillingsRow row;
            string s = null
                , sdes;
            decimal saldo = 0;

            while (i < data.Billings.Count)
            {
                page = new PdfPage(doc);

                using (g = XGraphics.FromPdfPage(page))
                {
                    y = 20;
                    this.SendPdfExcelReport_DrawHead(g, font, brush, y);
                    y = 40;
                    //84  es el maximo con la fuente

                    for (icurrentline = y, imaxline = 0; i < data.Billings.Count; ++i)
                    {
                        icurrentline = icurrentline + (imaxline);

                        if (icurrentline >= token_new_page)
                        {
                            --i;
                            break;
                        }

                        ilinesused = 0;
                        imaxline = 0;
                        row = data.Billings[i];

                        s = row.BillingDate.ToString("dd/MM/yyyy");
                        ilinesused = (this.DrawInLinesChar(s, g, font, 50, brush, token_date_x, icurrentline) * font.Height);
                        imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                        s = string.Format("{0}-{1}", row.Serial, row.Folio);
                        ilinesused = (this.DrawInLinesChar(s, g, font, 30, brush, token_folio_x, icurrentline) * font.Height);
                        imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                        {
                            var dr = from r in data.BillingsDetails
                                     where r.BillingID == data.BillingsDetails[i].BillingID
                                     select r.Description;
                            s = string.Join("\n", dr.ToArray());
                            sdes = s;
                            dr = null;
                        }
                        ilinesused = (this.DrawInLinesChar(s, g, font, 200, brush, token_concept_x, icurrentline) * font.Height);
                        imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                        s = string.Format("{0}", row.Total.ToString("C"));
                        ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_import_x, icurrentline) * font.Height);
                        imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                        if (!row.IsIsCreditNull() && !row.IsCredit)
                        {
                            //x = 150;
                            //s = string.Format("{0}", row.Total.ToString("C"));
                            //ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, x, icurrentline) * font.Height);
                            //imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            if (saldo > 0)
                            {
                                s = string.Format("{0}", saldo.ToString("C"));
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_saldo_x, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                            }
                            else
                            {
                                s = string.Format("-");
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_saldo_x + 10, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                            }

                            s = string.Format("{0}", row.IsIsCreditNull() || !row.IsCredit ? "No" : "Si");
                            ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_credit_x + 10, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            s = string.Format("{0}", row.Active ? "Si" : "No");
                            ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_active_x + 10, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            s = row.PreBilling && row.IsFolioIDNull() ? "Precomprobante no foliado no sellado" : row.PreBilling ? "Precomprobante no sellado" : "Comprobante";
                            ilinesused = (this.DrawInLinesChar(s, g, font, 50, brush, token_type_x, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                        }
                        else
                        {
                            var dr = from r in data.BillingPayments
                                     where r.BillingID == data.Billings[i].BillingID
                                     select r;

                            s = string.Format("{0:c}", saldo = row.Total + saldo);
                            ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_saldo_x, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            s = string.Format("{0}", row.IsIsCreditNull() || !row.IsCredit ? "No" : "Si");
                            ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_credit_x + 10, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            s = string.Format("{0}", row.Active ? "Si" : "No");
                            ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_active_x + 10, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            s = row.PreBilling && row.IsFolioIDNull() ? "Precomprobante no foliado no sellado" : row.PreBilling ? "Precomprobante no sellado" : "Comprobante";
                            ilinesused = (this.DrawInLinesChar(s, g, font, 50, brush, token_type_x, icurrentline) * font.Height);
                            imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                            foreach (PACFD.DataAccess.BillingExcelDataSet.BillingPaymentsRow rw in dr)
                            {
                                icurrentline = icurrentline + (imaxline);

                                if (icurrentline >= token_new_page)
                                {
                                    g.Dispose();
                                    doc.AddPage(page);
                                    page = new PdfPage(doc);
                                    g = XGraphics.FromPdfPage(page);
                                    y = 20;
                                    this.SendPdfExcelReport_DrawHead(g, font, brush, y);
                                    y = 40;
                                    icurrentline = y;
                                }

                                ilinesused = 0;
                                imaxline = 0;

                                s = string.Format("{0}", rw.PaidDate.ToString("dd/MM/yyyy"));
                                ilinesused = (this.DrawInLinesChar(s, g, font, 50, brush, token_date_x, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                //    x = 30;
                                //    s = string.Format("{0}-{1}", data.Billings[i].Serial, data.Billings[i].Folio);
                                //    ilinesused = (this.DrawInLinesChar(s, g, font, 20, brush, x, icurrentline) * font.Height);
                                //    imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                //    x = 50;
                                //    s = string.Format("{0}", sdes);
                                //    ilinesused = (this.DrawInLinesChar(s, g, font, 30, brush, x, icurrentline) * font.Height);
                                //    imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                s = string.Format("{0}", rw.Amount.ToString("C"));
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_ammount_x, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                if ((saldo = saldo - rw.Amount) > 0)
                                {
                                    s = string.Format("{0}", saldo.ToString("C"));
                                    ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_saldo_x, icurrentline) * font.Height);
                                    imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                                }
                                else
                                {
                                    s = string.Format("-");
                                    ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_saldo_x + 10, icurrentline) * font.Height);
                                    imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                                }

                                s = string.Format("{0}", rw.Reference);
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_reference_x, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                s = string.Format("{0}", row.IsIsCreditNull() || !row.IsCredit ? "No" : "Si");
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_credit_x + 10, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                s = string.Format("{0}", row.Active ? "Si" : "No");
                                ilinesused = (this.DrawInLinesChar(s, g, font, 40, brush, token_active_x + 10, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;

                                s = row.PreBilling && row.IsFolioIDNull() ? "Precomprobante no foliado no sellado" : row.PreBilling ? "Precomprobante no sellado" : "Comprobante";
                                ilinesused = (this.DrawInLinesChar(s, g, font, 50, brush, token_type_x, icurrentline) * font.Height);
                                imaxline = ilinesused > imaxline ? ilinesused : imaxline;
                            }
                        }
                    }
                }

                doc.AddPage(page);
            }

            //doc.AddPage(page);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            doc.Save(stream, false);
            doc.Dispose();

            return stream;
        }
        void SendPdfExcelReport_DrawHead(XGraphics g, XFont font, XSolidBrush brush, int y)
        {
            const int token_date_x = 10;
            const int token_folio_x = 55;
            const int token_concept_x = 90;
            const int token_import_x = 300;

            const int token_ammount_x = 350;
            const int token_saldo_x = 400;
            const int token_credit_x = 428;
            const int token_active_x = 458;
            const int token_reference_x = 490;
            const int token_type_x = 538;

            g.DrawString("Fecha", font, brush, token_date_x, y);
            g.DrawString("Folio", font, brush, token_folio_x, y);
            g.DrawString("Concepto", font, brush, token_concept_x, y);
            g.DrawString("Importe", font, brush, token_import_x, y);
            g.DrawString("Pagos", font, brush, token_ammount_x, y);
            g.DrawString("Saldo", font, brush, token_saldo_x, y);
            g.DrawString("Credito", font, brush, token_credit_x, y);
            g.DrawString("Activa", font, brush, token_active_x, y);
            g.DrawString("Referencia", font, brush, token_reference_x, y);
            g.DrawString("Tipo", font, brush, token_type_x, y);
        }

        int DrawInLinesChar(string text, XGraphics g, XFont font, int width, XSolidBrush brush, int x, int y)
        {
            char[] c;
            string[] sp;
            string aux = string.Empty;

            text = text.Replace("\n", "").Replace("\r", "");
            c = text.ToCharArray();
            text = string.Empty;

            for (int i = 0; i < c.Length; i++)
            {
                if (g.MeasureString(string.Format("{0}{1}", aux, c[i]), font).Width < width)
                {
                    aux += c[i];
                    continue;
                }

                text += string.Format("{0}\n", aux);
                aux = string.Empty;
                i--;
            }

            sp = (text += aux).Split('\n');
            text = null;

            foreach (string s in sp)
            {
                if (string.IsNullOrEmpty(s.Trim()))
                { continue; }

                g.DrawString(s, font, brush, x, y);
                y += font.Height;
            }

            return sp.Length;
        }

        ///// <summary>
        ///// Draw a string using multiline format.
        ///// </summary>
        ///// <param name="text">Text to be draw.</param>
        ///// <param name="g">Graphic class used to draw over the Pdf.</param>
        ///// <param name="font">Font used to draw the text.</param>
        ///// <param name="width">Max characters per line.</param>
        ///// <param name="brush">Brush used to draw the text.</param>
        ///// <param name="x">Start X coordenated, where began to draw.</param>
        ///// <param name="y">Start Y coordenated, where began to draw.</param>
        ///// <returns>Return an integer value, indicating the numbers of lines used.</returns>
        //int DrawInLines(string text, XGraphics g, XFont font, int width, XSolidBrush brush, int x, int y)
        //{
        //    string[] sp;
        //    string stext = ""
        //        , sline = "";

        //    sline = text.Replace("\n", "").Replace("\r", "");
        //    sp = text.Split(' ');
        //    sline = "";

        //    for (int i = 0; i < sp.Length; i++)
        //    {
        //        if (string.IsNullOrEmpty(sp[i]))
        //            continue;

        //        if ((int)g.MeasureString(sline.Trim().Length > 0 ? string.Format("{0} {1}", sline, sp[i]) : sp[i], font).Width <= width)
        //        {
        //            sline += string.Format("{0}{1}", (sline.Trim().Length > 0 ? " " : string.Empty), sp[i]);
        //        }
        //        else
        //        {
        //            stext += string.Format("{0}{1}", stext.Trim().Length > 0 ? "\n" : string.Empty, sline);
        //            sline = sp[i];
        //        }
        //    }

        //    if (sline != "")
        //        stext += string.Format("{0}{1}", stext.Trim().Length > 0 ? " " : string.Empty, sline);

        //    //text = string.Empty;

        //    //for (i = 0; i < sp.Length; i++)
        //    //{
        //    //    if (string.IsNullOrEmpty(sp[i]))
        //    //        continue;

        //    //    if ((int)g.MeasureString(string.Format("{0} {1}", aux, sp[i]), font).Width < width)
        //    //    {
        //    //        aux += (aux.Trim().Length > 0 ? " " : string.Empty) + sp[i];
        //    //        continue;
        //    //    }
        //    //    else if ((int)g.MeasureString(sp[i], font).Width >= width)
        //    //    {
        //    //        aux = string.Empty;

        //    //        foreach (char c in sp[i])
        //    //        {
        //    //            if ((int)g.MeasureString(aux + c, font).Width < width || width < 1)
        //    //            {
        //    //                aux += c;
        //    //                continue;
        //    //            }

        //    //            text += string.Format("{0}\n", aux);
        //    //            aux = c.ToString();
        //    //        }

        //    //        text += string.Format("{0}", aux);
        //    //        aux = string.Empty;
        //    //        continue;
        //    //    }
        //    //    else if ((int)g.MeasureString(string.Format("{0} {1}", aux, sp[i]), font).Width > width && (int)g.MeasureString(sp[i], font).Width < width)
        //    //    {
        //    //        text += string.Format("{0}\n", aux);
        //    //        aux = string.Empty;
        //    //        i--;
        //    //        continue;
        //    //    }

        //    //    if (enter)
        //    //    {
        //    //        text += string.Format("{0}\n", sp[i]);
        //    //        enter = false;
        //    //        aux = string.Empty;
        //    //        continue;
        //    //    }

        //    //    enter = true;
        //    //    text += string.Format("{0}\n", aux);
        //    //    aux = string.Empty;
        //    //    i--;
        //    //}

        //    //text += aux;
        //    sp = stext.Split('\n');
        //    sline = null;

        //    foreach (string s in sp)
        //    {
        //        if (string.IsNullOrEmpty(s.Trim()))
        //            continue;

        //        g.DrawString(s, font, brush, x, y);
        //        y += font.Height;
        //    }

        //    return sp.Length;
        //}

        //int DrawInLines(string printtext, XGraphics g, XFont font, int width, XSolidBrush brush, int x, int y)
        //{
        //    string[] sp;
        //    string aux = string.Empty;
        //    int i;
        //    bool enter = false;
        //    string text = printtext;

        //    text = text.Replace("\n", "").Replace("\r", "");
        //    sp = text.Split(' ');
        //    text = string.Empty;

        //    for (i = 0; i < sp.Length; i++)
        //    {
        //        if (string.IsNullOrEmpty(sp[i]))
        //            continue;

        //        if ((int)g.MeasureString(string.Format("{0} {1}", aux, sp[i]), font).Width < width)
        //        {
        //            aux += (aux.Trim().Length > 0 ? " " : string.Empty) + sp[i];
        //            continue;
        //        }
        //        else if ((int)g.MeasureString(sp[i], font).Width >= width)
        //        {
        //            aux = string.Empty;

        //            foreach (char c in sp[i])
        //            {
        //                if ((int)g.MeasureString(aux + c, font).Width < width || width < 1)
        //                {
        //                    aux += c;
        //                    continue;
        //                }

        //                text += string.Format("{0}\n", aux);
        //                aux = c.ToString();
        //            }

        //            text += string.Format("{0}", aux);
        //            aux = string.Empty;
        //            continue;
        //        }
        //        else if ((int)g.MeasureString(string.Format("{0} {1}", aux, sp[i]), font).Width > width && (int)g.MeasureString(sp[i], font).Width < width)
        //        {
        //            text += string.Format("{0}\n", aux);
        //            aux = string.Empty;
        //            i--;
        //            continue;
        //        }

        //        if (enter)
        //        {
        //            text += string.Format("{0}\n", sp[i]);
        //            enter = false;
        //            aux = string.Empty;
        //            continue;
        //        }

        //        enter = true;
        //        text += string.Format("{0}\n", aux);
        //        aux = string.Empty;
        //        i--;
        //    }

        //    text += aux;
        //    sp = text.Split('\n');

        //    text = null;

        //    foreach (string s in sp)
        //    {
        //        if (string.IsNullOrEmpty(s.Trim()))
        //            continue;

        //        g.DrawString(s, font, brush, x, y);
        //        y += font.Height;
        //    }

        //    return sp.Length;
        //}
    }
}