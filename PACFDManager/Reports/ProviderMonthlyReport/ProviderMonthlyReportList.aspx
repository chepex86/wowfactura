﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="ProviderMonthlyReportList.aspx.cs" Inherits="PACFDManager.Reports.ProviderMonthlyReport.ProviderMonthlyReportList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer">
                <tr>
                    <td align="center">
                        <table style="width: 319px">
                            <tr>
                                <td align="center" class="Titulo" colspan="2">
                                    <asp:Label ID="lblTitleData" runat="server" Text="Datos del Proveedor"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblTitleSocialReason" runat="server" Font-Bold="True" 
                                        Text="Razón Social:"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblSocialReason" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblTitleRFC" runat="server" Font-Bold="True" Text="R.F.C.:"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblRFC" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="Titulo">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" class="Titulo">
                        &nbsp;
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de Reportes Mensuales"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnGeneratesReportTop" runat="server" CssClass="Button" PostBackUrl="~/Reports/ProviderMonthlyReport/ProviderMonthlyReportGenerated.aspx"
                            Text="Generar Reporte" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvProviderMonthlyReportsList" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="#333333" HorizontalAlign="Center" OnRowDataBound="gvProviderMonthlyReportsList_RowDataBound">
                            <RowStyle BackColor="#EFF3FB" />
                            <Columns>
                                <asp:BoundField DataField="CreationDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha de Creación"
                                    HtmlEncode="false" />
                                <asp:TemplateField HeaderText="Reporte">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%#GenerateReportName((Byte)Eval("Month"), (int)Eval("Year"))%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detalles">
                                    <ItemTemplate>
                                        <a id="aDetails" runat="server" href="ProviderMonthlyReportDetails.aspx" target="_self">
                                            <img id="imgDetails" runat="server" alt="Details" border="0" src="~/Includes/Images/png/kfind.png" /></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Descargar">
                                    <ItemTemplate>
                                        <a id="aDownload" runat="server" href="DownloadReport.aspx" target="_self">
                                            <img id="imgDownload" runat="server" alt="Download" border="0" src="~/Includes/Images/png/filesave-48.png" /></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnGeneratesReportBottom" runat="server" CssClass="Button" PostBackUrl="~/Reports/ProviderMonthlyReport/ProviderMonthlyReportGenerated.aspx"
                            Text="Generar Reporte" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
