﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.Web.UI.HtmlControls;
using System.Data;
#endregion

namespace PACFDManager.Reports.ProviderMonthlyReport
{
    public partial class ProviderMonthlyReportList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                this.LoadList();
        }
        /// <summary>
        /// Load information for the list.
        /// </summary>
        private void LoadList()
        {
            this.LoadProviderInfo();
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            this.gvProviderMonthlyReportsList.DataSource = report.GetReporstForList();
            this.gvProviderMonthlyReportsList.DataBind();

            if (this.gvProviderMonthlyReportsList.Rows.Count < 1)
            {
                DataTable dt = new ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListDataTable();
                DataRow dr = dt.NewRow();
                dr["CFDVersion"] = String.Empty;
                dr["Name"] = String.Empty;
                dr["AuthorizationDate"] = DateTime.Now.ToString();
                dr["AuthorizationNumber"] = String.Empty;
                dr["RFC"] = String.Empty;
                dr["ProviderMonthlyReportID"] = -1;
                dr["ProviderID"] = -1;
                dr["Report"] = String.Empty;
                dr["CreationDate"] = DateTime.Now.ToString();
                dr["Month"] = 1;
                dr["Year"] = -1;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvProviderMonthlyReportsList.DataSource = dt;
                this.gvProviderMonthlyReportsList.DataBind();
            }
        }
        /// <summary>
        /// Generates the name file.
        /// </summary>
        /// <param name="month">byte Month</param>
        /// <param name="year">int Year</param>
        /// <returns>Returns the name file.</returns>
        protected String GenerateReportName(byte month, int year)
        {
            return (month < 10 ? "0" + month.ToString() : month.ToString()) + "/" + year;
        }
        /// <summary>
        /// Change the default value depending on the cell that contains for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvProviderMonthlyReportsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowIndex > -1)
            {
                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int i = 0; i < row.Cells.Count; i += 1)
                        row.Cells[i].Visible = false;
                    ViewState["Empty"] = null;
                }
            }

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
          
            ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListRow dr = ((DataRowView)e.Row.DataItem).Row as ReportsDataSet.ProviderMonthlyReports_GetByProviderIDForListRow;

            HtmlAnchor download = e.Row.FindControl("aDownload") as HtmlAnchor;
            HtmlAnchor details = e.Row.FindControl("aDetails") as HtmlAnchor;

            download.HRef = typeof(DownloadReport).Name + ".aspx?Year=" + dr.Year + "&Month=" + dr.Month;
            details.HRef = typeof(ProviderMonthlyReportDetails).Name + ".aspx?ReportID=" + dr.ProviderMonthlyReportID;
        }
        /// <summary>
        /// Makes a search of the current biller information.
        /// </summary>
        private void LoadProviderInfo()
        {
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet ds = new ProviderDataSet();
            ProviderDataSet.ProviderDataTable ta;
            ta = provider.GetProvider();
            this.FillInfo(ta);
        }
        /// <summary>
        /// Displays biller information received in a datatable.
        /// </summary>
        /// <param name="ta">BillersDataSet.BillersDataTable table</param>
        private void FillInfo(ProviderDataSet.ProviderDataTable ta)
        {
            this.lblRFC.Text = ta[0].RFC;
            this.lblSocialReason.Text = ta[0].Name;
        }
    }
}