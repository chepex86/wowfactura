﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.Reports.ProviderMonthlyReport
{
    public partial class ProviderMonthlyReportDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.ProgressBar.Visible = false;
            if (!IsPostBack)
                this.LoadData();
        }
        /// <summary>
        /// Redirects and specifies that the file to download is the Report, which corresponds to ReportID sent.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void imgBtnDownload_Click(object sender, ImageClickEventArgs e)
        {
            String m = ViewState["MonthYear"].ToString().Split('_')[0];
            String y = ViewState["MonthYear"].ToString().Split('_')[1];

            Response.Redirect(typeof(DownloadReport).Name + ".aspx?Year=" + y + "&Month=" + m);
        }
        /// <summary>
        /// Load information for Report detail.
        /// </summary>
        /// <returns>Returns true if the information is loaded correctly, otherwise returns false.</returns>
        private bool LoadData()
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.ProviderMonthlyReports_GetByReportIDDataTable ta;
            if (Request.QueryString["ReportID"].IsNull())
                return false;

            ta = report.GetReporstByReportID(Convert.ToInt32(Request.QueryString["ReportID"]));

            if (ta.Count < 1)
                return false;

            this.lblCreationDate.Text = ta[0].CreationDate.ToString("dd/MM/yyyy");
            this.lblName.Text = this.GenerateReportName(ta[0].AuthorizationNumber, ta[0].Month, ta[0].Year);
            this.lblMonth.Text = ta[0].Month < 10 ? "0" + ta[0].Month.ToString() : ta[0].Month.ToString();
            this.lblYear.Text = ta[0].Year.ToString();
            this.txtContent.Text = ta[0].Report;

            ViewState["MonthYear"] = ta[0].Month.ToString() + "_" + ta[0].Year.ToString();

            return true;
        }
        /// <summary>
        /// Generates the name file.
        /// </summary>
        /// <param name="AuthorizationNumber">string Authorization Number</param>
        /// <param name="month">byte Month</param>
        /// <param name="year">int Year</param>
        /// <returns>Returns the name file.</returns>
        private string GenerateReportName(String AuthorizationNumber, byte month, int year)
        {
            return AuthorizationNumber + (month < 10 ? "0" + month.ToString() : month.ToString()) + year + ".txt";
        }
    }
}