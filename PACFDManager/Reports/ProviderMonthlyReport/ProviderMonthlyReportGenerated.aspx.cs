﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.IO;
#endregion

namespace PACFDManager.Reports.ProviderMonthlyReport
{
    public partial class ProviderMonthlyReportGenerated : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = this.GetLocalResourceObject("MonthlyReport").ToString();
            this.Master.ProgressBar.Visible =
            this.pnlDownloadReport.Visible =
            this.lblErrorMonth.Visible = 
            this.lblError.Visible = false;
        }
        /// <summary>
        /// Generates the report with the specified date.
        /// </summary>
        private void LoadData()
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.ProviderMonthlyReportDataTable t;

            t = report.ProviderMonthlyReport(new DateTime(this.PeriodChooser1.Year, this.PeriodChooser1.Month, 1));
            if (t.Count < 0)
                return;

            this.txtContent.Wrap = true;
            
            String c = String.Empty, p = "|";

            for (int i = 0; i < t.Count; i += 1)
                c += p + t[i].RFC + p + t[i].Serial + p + t[i].Issued + p + t[i].Canceled + p + System.Environment.NewLine;

            this.txtContent.Width = 700;
            this.txtContent.Height = t.Count * 25;
            this.txtContent.Text = c;
            this.imgBtnDownloadReport.CommandArgument = this.PeriodChooser1.Year + "_" + this.PeriodChooser1.Month;
            this.txtContent.Visible = true;

            ReportsDataSet ds = new ReportsDataSet();
            ReportsDataSet.ProviderMonthlyReportsRow drCert = ds.ProviderMonthlyReports.NewProviderMonthlyReportsRow();

            try
            {
                drCert.ProviderID = 1;
                drCert.Report = c;  
                drCert.CreationDate = DateTime.Now;
                drCert.Month = Convert.ToByte(this.PeriodChooser1.Month);
                drCert.Year = this.PeriodChooser1.Year;

                ds.ProviderMonthlyReports.AddProviderMonthlyReportsRow(drCert);
                report.ProviderMonthlyReportsUpdate(ds.ProviderMonthlyReports);

                this.pnlDownloadReport.Visible = true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.ProviderMonthlyReport.ProviderMonthlyReportGenerated.btnGenerate_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            if (SearchReports(this.PeriodChooser1.Year, this.PeriodChooser1.Month))
            {
                if (this.PeriodChooser1.Month <= (DateTime.Now.Month - 1))
                    this.LoadData();
                else
                    this.lblErrorMonth.Visible = true;
            }
            else
            {
                this.lblError.Visible = true;
                return;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Reports.ProviderMonthlyReport.ProviderMonthlyReportGenerated.imgBtnDownloadReport_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void imgBtnDownloadReport_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;
            String y = s.CommandArgument.Split('_')[0];
            String m = s.CommandArgument.Split('_')[1];

            Response.Redirect(typeof(DownloadReport).Name + ".aspx?Year=" + y + "&Month=" + m);
        }
        /// <summary>
        /// Checks if a report has been generated previously.
        /// </summary>
        /// <param name="year">Report year.</param>
        /// <param name="month">Report month.</param>
        /// <returns>Returns true if the report does not exist, returns false otherwise.</returns>
        private bool SearchReports(int year, int month)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.ProviderMonthlyReports_GetByDateDataTable ta;
            ta = report.GetReportByDate(year, Convert.ToByte(month));

            return ta.Count < 1 ? true : false;
        }
    }
}