<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="ProviderMonthlyReportGenerated.aspx.cs" Inherits="PACFDManager.Reports.ProviderMonthlyReport.ProviderMonthlyReportGenerated"
    Culture="auto" UICulture="auto" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="../../PeriodChooser.ascx" TagName="PeriodChooser" TagPrefix="uc1" %>
<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer">
                <tr>
                    <td align="center" class="Titulo">
                        <asp:Label ID="lblTitleReports" runat="server" Text="Reporte Mensual"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td align="right">
                                    <uc1:PeriodChooser ID="PeriodChooser1" runat="server" />
                                </td>
                                <td align="left">
                                    <uc2:Tooltip ID="tipPeriodChooser1" runat="server" ToolTip="Selecciona el mes y el a�o del reporte que quieras generar.<br /><b>Nota:</b> El mes debe ser menor al actual." />
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnGenerate" runat="server" CssClass="Button" OnClick="btnGenerate_Click"
                                        Text="Generar" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red" Text="Ya se ha generado un reporte con las fechas seleccionadas."
                            Visible="False"></asp:Label>
                        <asp:Label ID="lblErrorMonth" runat="server" Font-Bold="True" ForeColor="Red" Text="Solo se pueden generar reportes de meses anteriores al actual."
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;<asp:TextBox ID="txtContent" runat="server" Height="124px" TextMode="MultiLine"
                            Visible="False" Width="282px" ReadOnly="True"></asp:TextBox>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlDownloadReport" runat="server" Visible="False">
                            <table>
                                <tr>
                                    <td align="center" class="Titulo">
                                        <asp:Label ID="lblDownloadReport" runat="server" Text="Descargar Reporte"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:ImageButton ID="imgBtnDownloadReport" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                            OnClick="imgBtnDownloadReport_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
