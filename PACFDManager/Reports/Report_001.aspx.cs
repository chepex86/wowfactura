﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using PACFD.Rules;
using System.Data;
#endregion

namespace PACFDManager.Reports
{
    public partial class Report_001 : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReportViewer1.Visible = false;
            this.SearchFilter1.SetTypeFilter(TypeFilter.Report_001);
            this.SearchFilter1.Search += new SearchFilter.SearchEventHandler(SearchFilter1_Search);
        }
        /// <summary>
        /// Event fired by the delegate of the user control SearchFilter.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        void SearchFilter1_Search(object sender, ReportsSearchEventArgs e)
        {
            PACFD.Rules.CustomReports report = new PACFD.Rules.CustomReports();
            CustomReportsDataSet ds = new CustomReportsDataSet();
            DataTable dt;

            dt = report.Report_001(e.BillingID, e.Start, e.End, e.ReceptorID, e.UnitType, e.Status, e.ConceptID, e.PreBilling, e.IsCredit);

            this.ReportViewer1.Visible = true;

            this.ReportViewer1.FileName = "~/Reports/CrystalReports/crReport_001.rpt";
            this.ReportViewer1.TextFilter = e.TextFilter;
            this.ReportViewer1.SetDataSource = dt;
            //this.ReportViewer1.DataBind();
        }
    }
}