﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.PlaceDispatch
{
    public partial class PlaceDispatchList : BasePage
    {
        private PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable TableTemp
        {
            get { return this.Session["tmp"] as PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable; }
            set { this.Session["tmp"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.gdvPlaceDispatch.DataSource = this.TableTemp = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
            this.gdvPlaceDispatch.DataBind();
        }

        protected void ImgbtnActive_Click(object sender, ImageClickEventArgs e)
        {
            int i = 0;
            ImageButton b = sender as ImageButton;
            PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDDataTable table = this.TableTemp;
            System.Data.DataRow[] row;
            PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDRow brow;

            if (b.IsNull() || table.IsNull() || !int.TryParse(b.CommandArgument, out i) || i < 1)
                return;

            row = table.Select("PlaceDispatchID=" + i.ToString());

            if (row.Length < 1)
                return;

            brow = row[0] as PACFD.DataAccess.PlaceDispatchDataSet.GetByBillerIDRow;
            (new global::PACFD.Rules.PlaceDispatch()).SetActive(this.CurrentBillerID, i, !brow.Active);

            this.gdvPlaceDispatch.DataSource = this.TableTemp = (new global::PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.CurrentBillerID);
            this.gdvPlaceDispatch.DataBind();
        }

        protected string GetActiveImageState(object o)
        {
            int i = Convert.ToInt32(o);
            return i == 1 ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(PlaceDispatch.PlaceDispatchAdd).Name + ".aspx");
        }
    }
}
