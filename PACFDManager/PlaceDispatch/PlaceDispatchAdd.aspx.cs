﻿using PACFDManager.Billings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.PlaceDispatch
{
    public partial class PlaceDispatchAdd : BasePage
    {
        const string MESSAGE_ERROR = "MESSAGE_ERROR";
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            global::PACFD.DataAccess.PlaceDispatchDataSet.PlaceDispatchDataTable table = new PACFD.DataAccess.PlaceDispatchDataSet.PlaceDispatchDataTable();

            if (string.IsNullOrEmpty(this.txtPlaceDispatch.Text))
            {
                this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.Accept;
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.DialogType = WebMessageBoxDialogType.Question;
                this.WebMessageBox1.ShowMessage("El Lugar de Expedición es necesario.", false);
                return;
            }

            if (string.IsNullOrEmpty(this.txtZipCode.Text))
            {
                this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.Accept;
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.DialogType = WebMessageBoxDialogType.Question;
                this.WebMessageBox1.ShowMessage("El Código postal es necesario.", false);
                return;
            }

            if (!this.txtZipCode.Text.IsValidZipCode())
            {
                this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.Accept;
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.DialogType = WebMessageBoxDialogType.Question;
                this.WebMessageBox1.ShowMessage("El Código postal no nse encuentra en nuestros catalogos.", false);
                return;
            }
            table.AddPlaceDispatchRow(this.CurrentBillerID, this.txtPlaceDispatch.Text, true, this.txtZipCode.Text);
            this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.Accept;

            if ((new global::PACFD.Rules.PlaceDispatch()).Update(table))
            {
                this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.AcceptCancel;
                this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
                this.WebMessageBox1.ShowMessage("Lugar de expedición agregado correctamente.<br/>¿Desea agregar otro lugar de expedición?", false);
            }
            else
            {
                this.WebMessageBox1.ButtonTypes = WebMessageBoxButtonType.Accept;
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("No se pudo agregar el lugar de expedición", false);
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (WebMessageBox1.CommandArguments)
            {
                case MESSAGE_SUCCESS:
                    if (e.DialogResult == WebMessageBoxDialogResultType.Cancel)
                        btnCancel_Click(sender, EventArgs.Empty);

                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(PlaceDispatch.PlaceDispatchList).Name + ".aspx");
        }
    }
}
