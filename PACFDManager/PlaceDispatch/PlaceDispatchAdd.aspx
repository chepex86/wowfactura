﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="PlaceDispatchAdd.aspx.cs" Inherits="PACFDManager.PlaceDispatch.PlaceDispatchAdd" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server" h>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="divContent" runat="server">
                <div id="container" class="wframe60" style="height:300px;">
                    <div class="form">
                        <div class="formStyles">
                            <div class="info">
                                <label class="desc">
                                    <h2>
                                        Lugar de Expedición
                                    </h2>
                                </label>
                            </div>
                            <ul>
                                <li class="left">
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="desc">
                                                    <asp:Label ID="lblName" runat="server" Text="Nombre:" Width="400px" />
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="vtip" title='<%="Escriba el nombre del lugar de expedición."%>'>
                                                    <asp:TextBox ID="txtPlaceDispatch" runat="server" Width="400px" 
                                                    MaxLength="250" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="validator-msg">
                                                    <asp:RequiredFieldValidator ID="txtPlaceDispatch_RequiredFieldValidator" runat="server"
                                                        ControlToValidate="txtPlaceDispatch" Display="Dynamic" ErrorMessage="Este campo es requerido" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                             <ul>
                                <li class="left">
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="desc">
                                                    <asp:Label ID="LabelZipCode" runat="server" Text="Código postal:" Width="400px" />
                                                </label>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <span class="vtip" title='<%="Escriba el código postal del lugar de expedición."%>'>
                                                    <asp:TextBox ID="txtZipCode" runat="server" Width="400px" 
                                                    MaxLength="50" />
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                            <ul>
                                <li class="buttons">
                                    <div>
                                        <asp:Button ID="btnAcept" CssClass="Button" runat="server" Text="Aceptar" OnClick="btnAcept_Click" />
                                        <asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                            Text="Cancelar" OnClick="btnCancel_Click" />
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
