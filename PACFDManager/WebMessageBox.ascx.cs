﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace PACFDManager
{
    public partial class WebMessageBox : System.Web.UI.UserControl
    {
        public delegate void ClickEventHandler(object sender, WebMessageBoxEventArgs e);
        public delegate void EventHandler(object sender, EventArgs e);
        public event ClickEventHandler Click;
        public event EventHandler CancelClick;
        
        /// <summary>
        /// Get or Set the type of dialog, is used to set the icon image.
        /// </summary>
        [Browsable(false)]
        public WebMessageBoxDialogType DialogType
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return WebMessageBoxDialogType.Normal;


                return o.GetType() == typeof(WebMessageBoxDialogType) ? (WebMessageBoxDialogType)o : WebMessageBoxDialogType.Normal;
            }
            set
            {
                switch (value)
                {
                    case WebMessageBoxDialogType.Normal:
                        this.MessageForeColor = System.Drawing.Color.DarkBlue;
                        this.ShowIcon = false;
                        break;
                    case WebMessageBoxDialogType.Warning:
                        this.MessageForeColor = System.Drawing.Color.DarkViolet;
                        this.ShowIcon = true;
                        break;
                    case WebMessageBoxDialogType.Stop:
                    case WebMessageBoxDialogType.Error:
                        this.MessageForeColor = System.Drawing.Color.Red;
                        this.ShowIcon = true;
                        break;
                }

                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get or Set a boolean value indicating if the dialog must show an icon.
        /// </summary>
        [Description("Get or Set a boolean value indicating if the dialog must show an icon."),
        DefaultValue(true), Category("Appearance")]
        public Boolean ShowIcon
        {
            get
            {
                bool r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return true;

                if (!Boolean.TryParse(o.ToString(), out r))
                    return true;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// The color of the text used to display.
        /// </summary>
        [Category("Appearance")]
        public System.Drawing.Color MessageForeColor
        {
            get
            {
                System.Drawing.Color r;
                object o;
                int icolor;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return System.Drawing.Color.Black;

                if (!int.TryParse(o.ToString(), out icolor))
                    return System.Drawing.Color.Black;

                if (icolor < 0)
                    icolor = 0;

                r = System.Drawing.Color.FromArgb(icolor);

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get or Set the title of the dialog box.
        /// </summary>
        [Description("Get or Set the title of the dialog box."), Category("Appearance")]
        public string Title
        {
            get { return this.lblTitle.Text; }
            set { this.lblTitle.Text = value; }
        }
        /// <summary>
        /// The type of buttons to display in the dialog box.
        /// </summary>
        [Browsable(false)]
        public WebMessageBoxButtonType ButtonTypes
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return WebMessageBoxButtonType.AcceptCancel;

                return o.GetType() == typeof(WebMessageBoxButtonType) ? (WebMessageBoxButtonType)o : WebMessageBoxButtonType.AcceptCancel;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
                this.SetChangedButtons();
            }
        }
        /// <summary>
        /// Get or set additional argumets to be send in the click event.
        /// </summary>
        [Description("Get or set additional argumets to be send in the click event."), Category("Behavior")]
        public string CommandArguments
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            btnAceptar.OnClientClick = String.Format("fnClickUpdate('{0}','{1}')", btnAceptar.UniqueID, "");
            btnCancelar.OnClientClick = String.Format("fnClickUpdate('{0}','{1}')", btnCancelar.UniqueID, "");

            this.ShowIcon = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            WebMessageBoxDialogResultType m;
            WebMessageBoxEventArgs arg;
            this.mpeMessages.Hide();

            m = WebMessageBoxDialogResultType.Accept;

            switch (this.ButtonTypes)
            {
                case WebMessageBoxButtonType.Accept:
                case WebMessageBoxButtonType.AcceptCancel: m = WebMessageBoxDialogResultType.Accept; break;
                case WebMessageBoxButtonType.ContinueIgnore: m = WebMessageBoxDialogResultType.Continue; break;
                case WebMessageBoxButtonType.YesNo: m = WebMessageBoxDialogResultType.Yes; break;
            }

            arg = new WebMessageBoxEventArgs(m, this.ButtonTypes, this.CommandArguments);
            this.OnClick(arg);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.OnCancel(e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnClick(WebMessageBoxEventArgs e)
        {
            if (!this.Click.IsNull())
                this.Click(this, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void OnCancel(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Refresh the buttons and text to display.
        /// </summary>
        private void SetChangedButtons()
        {
            this.btnAceptar.Visible = true;
            this.btnCancelar.Visible = true;

            switch (this.ButtonTypes)
            {
                default:
                case WebMessageBoxButtonType.AcceptCancel:
                    this.btnAceptar.Text = "Aceptar";
                    this.btnCancelar.Text = "Cancelar";
                    break;
                case WebMessageBoxButtonType.YesNo:
                    this.btnAceptar.Text = "Sí";
                    this.btnCancelar.Text = "No";
                    break;
                case WebMessageBoxButtonType.ContinueIgnore:
                    this.btnAceptar.Text = "Continuar";
                    this.btnCancelar.Text = "Ignorar";
                    break;
                case WebMessageBoxButtonType.Accept:
                    this.btnCancelar.Visible = false;
                    this.btnAceptar.Text = "Aceptar";
                    break;
            }
        }
        /// <summary>
        /// Initialize the icon og the dialog box.
        /// </summary>
        private void SetIconUsed()
        {
            this.imgIcon.Visible = true;

            switch (this.DialogType)
            {
                default:
                case WebMessageBoxDialogType.Normal:
                    this.imgIcon.Visible = false;
                    this.imgIcon.ImageUrl = string.Empty;
                    break;
                case WebMessageBoxDialogType.Question:
                    this.imgIcon.ImageUrl = "~/Includes/Images/questionIcon.jpg";
                    break;
                case WebMessageBoxDialogType.Warning:
                    this.imgIcon.ImageUrl = "~/Includes/Images/warningIcon.jpg";
                    break;
                case WebMessageBoxDialogType.Error:
                case WebMessageBoxDialogType.Stop:
                    this.imgIcon.ImageUrl = "~/Includes/Images/deleteIcon.jpg";
                    break;
            }
        }
        /// <summary>
        /// Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box.</param>
        public void ShowMessage(string message)
        {
            message = message.Length > 500 ? message.Substring(0, 500) : message;
            this.udpMessages.Visible = true;
            this.lblMessages.Text = message;
            this.lblMessages.ForeColor = this.MessageForeColor;
            this.SetIconUsed();
            this.mpeMessages.Show();
        }
        /// <summary>
        /// Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box.</param>
        /// <param name="iserror">
        /// If true show the text in red else show the text in green.
        /// If  have to use another color use MessageForeColor to change the color 
        /// used to display and the ShowMessage(msg)/ShowMessage(msg, color)
        /// method to display.
        /// </param>        
        public void ShowMessage(string message, bool iserror)
        {
            this.ShowMessage(message, iserror ? System.Drawing.Color.Red : System.Drawing.Color.Green);
        }
        /// <summary>
        ///  Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box.</param>
        /// <param name="txtcolor">Color of the text.</param>
        public void ShowMessage(string message, System.Drawing.Color txtcolor)
        {
            message = message.Length > 500 ? message.Substring(0, 500) : message;
            this.udpMessages.Visible = true;
            this.lblMessages.Text = message;
            this.lblMessages.ForeColor = txtcolor;
            this.SetIconUsed();
            this.mpeMessages.Show();
        }
        /// <summary>
        ///  Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box.</param>
        /// <param name="txtcolor">Color of the text.</param>
        /// <param name="buttons">The type of buttons to display in the dialog box.</param>
        public void ShowMessage(string message, System.Drawing.Color txtcolor, WebMessageBoxButtonType buttons)
        {
            message = message.Length > 500 ? message.Substring(0, 500) : message;
            this.ButtonTypes = buttons;
            this.udpMessages.Visible = true;
            this.lblMessages.Text = message;
            this.lblMessages.ForeColor = txtcolor;
            this.SetIconUsed();
            this.mpeMessages.Show();
        }
        /// <summary>
        /// Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box.</param>
        /// <param name="buttons">The type of buttons to display in the dialog box.</param>
        public void ShowMessage(string message, WebMessageBoxButtonType buttons)
        {
            message = message.Length > 500 ? message.Substring(0, 500) : message;
            this.ButtonTypes = buttons;
            this.udpMessages.Visible = true;
            this.lblMessages.Text = message;
            this.lblMessages.ForeColor = this.MessageForeColor;
            this.SetIconUsed();
            this.mpeMessages.Show();
        }
        /// <summary>
        /// Show the message box in the web browser.
        /// </summary>
        /// <param name="message">Message to display in the dialog box</param>
        /// <param name="iserror">
        /// If true show the text in red else show the text in green.
        /// If  have to use another color use MessageForeColor to change the color 
        /// used to display and the ShowMessage(msg)/ShowMessage(msg, color)
        /// method to display.
        /// </param> 
        /// <param name="buttons">The type of buttons to display in the dialog box.</param>
        public void ShowMessage(string message, bool iserror, WebMessageBoxButtonType buttons)
        {
            this.ButtonTypes = buttons;
            this.ShowMessage(message, iserror);
        }
    }
}