﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
namespace PACFDManager
{
    public partial class ReSeal : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnReSeal_Click(object sender, EventArgs e)
        {
            int id = -1;
            if (int.TryParse(txtIdFactura.Text, out id))
            {
                PACFD.Rules.Billings b = new PACFD.Rules.Billings();
                bool result = b.ReSealCFD(id);
                lblMsg.Text = result ? "Comprobante resellado" : "Error al re sellar";
            }
            else
            {
                lblMsg.Text = "ID incorrecto";
            }
        }

        protected void btnResellarTodo_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Billings b = new PACFD.Rules.Billings();
            bool result = b.ReSealCFD();
            lblMsg2.Text = result ? "Comprobante(s) re-sellado" : "Error al re-sellar";
        }
    }
}
