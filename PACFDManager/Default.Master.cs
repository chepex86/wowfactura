﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PACFD.Common;
using System.Xml;
using System.Data;
using System.Text;

namespace PACFDManager
{
    public partial class Default : System.Web.UI.MasterPage
    {
        public string CurrentUrlAplication
        {
            get
            {
                string url = string.Empty;

                if (Session[string.Format("CurrentUrlAplication")] == null)
                { Session[string.Format("CurrentUrlAplication")] = string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath); }

                url = Session[string.Format("CurrentUrlAplication")].ToString();
                return url;
            }
        }
        public global::System.Web.UI.UpdateProgress ProgressBar { get { return this.UpdateProgress1; } }
        public string Roles { get { return HttpContext.Current.Session[string.Format("PACFDManager-Roles")].IsNull() ? string.Empty : HttpContext.Current.Session[string.Format("PACFDManager-Roles")].ToString(); } }
        public AjaxControlToolkit.ToolkitScriptManager ToolScript { get { return this.ToolkitScriptManager1; } }

        /// <summary>
        /// Registry a button to do click when a item has been selected from an autocompletextender control.
        /// </summary>
        /// <param name="button">Button to do click event.</param>
        public void RegistryAutoCompletExtenderOnSelectedItem(Button button, AjaxControlToolkit.AutoCompleteExtender autocompletex)
        {
            string javafun = this.ClientID + "_AutoCompletExtenderOnSelectedItem";
            autocompletex.OnClientItemSelected = javafun;

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), javafun,
                string.Format("function {0}(){1} PACFDManager_fbtnClickForAutoCompletExtender_SelectedItem('{2}'); {3}",
                javafun,
                "{",
                button.ClientID,
                "}"),
                true);
        }
        /// <summary>
        /// Registry a hiddenfiled to do ValueChanged when a item has been selected from an autocompletextender control.
        /// </summary>
        /// <param name="button">HiddenField to do change event.</param>
        public void RegistryAutoCompletExtenderOnSelectedItem(HiddenField hidden, AjaxControlToolkit.AutoCompleteExtender autocompletex)
        {
            string javafun = this.ClientID + "_" + hidden.ClientID + "_AutoCompletExtenderOnSelectedItem";
            autocompletex.OnClientItemSelected = javafun;

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), javafun,
                string.Format("function {0}(){1} PACFDManager_fhflChangeForAutoCompletExtender_SelectItem('{2}'); {3}",
                javafun,
                "{",
                hidden.ClientID,
                "}"),
                true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            liScript.Text = string.Format("<script language='javascript' type='text/javascript' >var CurrentUrlAplication='{0}';</script>", CurrentUrlAplication);
        }

        public void RegistryToolTip(UpdatePanel panel)
        {
            String jquery = "<script src=\"" + ResolveClientUrl("~/Includes/jQuery/jquery-1.6.4.min.js")
            + "\" originalAttribute=\"src\" originalPath=\"" + ResolveClientUrl("~/Includes/jQuery/jquery-1.6.4.min.js") +
            "\" type=\"text/javascript\" ></script>";

            String vtip = "<script src=\"" + ResolveClientUrl("~/Includes/Tooltip/vtip.js")
            + "\" originalAttribute=\"src\" originalPath=\"" + ResolveClientUrl("~/Includes/Tooltip/vtip.js") +
            "\" type=\"text/javascript\" ></script>";

            ScriptManager.RegisterClientScriptBlock(panel, panel.GetType(), "ScriptJqueryTooltip", jquery + vtip, false);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Security.Security.IsLogged)
            {
                tbSelect.Visible = true;
                this.userMenu.Visible = true;
                SiteMapDataSource1.SiteMapProvider = "Web";

                if (((BasePage)Page).CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
                    SiteMapDataSource1.SiteMapProvider = "MapCBB";
                else if (((BasePage)Page).CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ||
                    ((BasePage)Page).CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                    ((BasePage)Page).CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3)
                    SiteMapDataSource1.SiteMapProvider = "MapCFDI";

                #region Select
                string rol = string.Empty;
                switch (Security.Security.Roles)
                {
                    case "Administrator": rol = "Administrator"; break;
                    case "SAT": rol = "SAT"; break;
                    case "AdvancedClient": rol = "Avanzado"; break;
                    case "BasicClient": rol = "Basico"; break;
                    default: rol = "Desconocido"; break;
                }

                lblUser.Text = string.Format("{0} ({1})", Security.Security.UserName, rol);
                if (Page is Select)
                {
                    if (!IsPostBack)
                    {
                        lblGroupCaption.Visible = false;
                        lblGroup.Visible = false;
                        lkbChangeClient.Visible = false;
                        lblBillersCaption.Visible = false;
                        lblBillers.Visible = false;
                        lkbChangeBiller.Visible = false;
                        lblBillTypeCaption.Visible = false;
                        trBranches.Visible = false;
                    }
                }
                else
                {
                    if ((BasePage)this.Page != null)
                    {

                        if (((BasePage)this.Page).CurrentGroupID == -1)
                            lkbChangeClient.Text = "[Seleccionar]";
                        else
                            lkbChangeClient.Text = "[Cambiar]";

                        if (((BasePage)this.Page).CurrentBillerID == -1)
                            lkbChangeBiller.Text = "[Seleccionar]";
                        else
                            lkbChangeBiller.Text = "[Cambiar]";

                        trBranches.Visible = false;

                        if (((BasePage)this.Page).HaveBranch)
                        {
                            trBranches.Visible = true;
                            lblBranches.Text = ((BasePage)this.Page).CurrentBranchName;
                        }

                        if (((BasePage)this.Page).IsAdministrator)
                        {
                            lblGroup.Visible = true;
                            lkbChangeClient.Visible = true;
                            lblGroup.Text = ((BasePage)this.Page).CurrentGroupName;
                            lblBillers.Text = ((BasePage)this.Page).CurrentBillerName;
                        }
                        else if (((BasePage)this.Page).IsSAT)
                        {
                            lblGroup.Visible = true;
                            lkbChangeClient.Visible = true;
                            lblGroup.Text = ((BasePage)this.Page).CurrentGroupName;
                            lblBillers.Text = ((BasePage)this.Page).CurrentBillerName;
                        }
                        else if (((BasePage)this.Page).IsAdvancedClient)
                        {
                            lblGroupCaption.Visible = false;
                            lblGroup.Visible = false;
                            lkbChangeClient.Visible = false;
                            lblBillers.Text = ((BasePage)this.Page).CurrentBillerName;
                        }
                        else if (((BasePage)this.Page).IsBasicClient)
                        {
                            lblGroupCaption.Visible = false;
                            lblGroup.Visible = false;
                            lkbChangeClient.Visible = false;
                            //lkbChangeBiller.Visible = false;
                            lblBillers.Text = ((BasePage)this.Page).CurrentBillerName;
                        }

                        lblBillers.Text = LimitCad(lblBillers.Text, 30);
                        lblBranches.Text = LimitCad(lblBranches.Text, 30);
                        lkbChangeBiller.Visible = (true && !(((BasePage)this.Page).HaveBranch));

                        this.lblBillTypeCaption.Visible = true;
                        this.lblBillType.Text = (((BasePage)this.Page).CurrentElectronicBillingType).GetTitle();
                    }
                }
                #endregion
            }
            else
            { tbSelect.Visible = false; }
        }
        protected string LimitCad(string cad, int len)
        {
            string result = string.Empty;

            if (cad.Length > len)
            { result = string.Format("{0}..", cad.Substring(0, len)); }
            else
            { result = cad; }

            return result;
        }
        protected void btnES_Click(object sender, ImageClickEventArgs e)
        {
            Session["SystemCulture"] = "es-MX";
            Session["IdIdioma"] = 1;
            Response.Redirect(Request.Path);
        }

        protected void btnEN_Click(object sender, ImageClickEventArgs e)
        {
            Session["SystemCulture"] = "en-US";
            Session["IdIdioma"] = 2;
            Response.Redirect(Request.Path);
        }

        protected void lkbChangeClient_Click(object sender, EventArgs e)
        {
            BasePage.SetCurrentGroupID(-1, "");
            BasePage.SetCurrentBillerID(-1, "", PACFD.Rules.ElectronicBillingType.Indeterminate);
            BasePage.SetCurrentBranchID(-1, "");
            Response.Redirect(string.Format("~/Select.aspx?op={0}", Cryptography.EncryptUnivisitString("chgCli")));
        }

        protected void lkbChangeBiller_Click(object sender, EventArgs e)
        {
            BasePage.SetCurrentBillerID(-1, "", PACFD.Rules.ElectronicBillingType.Indeterminate);
            BasePage.SetCurrentBranchID(-1, "");
            Response.Redirect(string.Format("~/Select.aspx?op={0}", Cryptography.EncryptUnivisitString("chgBill")));
        }
        /// <summary>
        /// Menu system data bind.
        /// </summary>
        /// <param name="sender">Menu sender object.</param>
        /// <param name="e">Events arguments sended by the object.</param>
        protected void userMenu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            string s;

            if (e.Item.NavigateUrl == this.ResolveUrl("~") + "PACFD_OpenPDF_XML_List")
            {
                s = this.ResolveUrl("~") + "Billings/BillingPDFXMLList.aspx";
                e.Item.NavigateUrl = string.Format("javascript:jcPACFD.OpenPDFXMLList('{0}');", s);
            }
        }

        protected void userMenu_DataBound(object sender, EventArgs e)
        {
            //MenuItem menu;
            //MenuItem submenu;

            //foreach (MenuItem mainmenu in userMenu.Items)
            //{
            //    for (int i = mainmenu.ChildItems.Count - 1; i > -1; i--)
            //    {
            //        menu = mainmenu.ChildItems[i];

            //        if (PACFDManager.Security.Security.IsAdvancedClient)
            //        {
            //            switch (menu.ValuePath.Trim())
            //            {
            //                case "Administración/Grupos":
            //                case "Administración/Respaldar BDD":
            //                case "Administración/Catálogo de Impuestos":
            //                case "Administración/Configuración de Comprobantes Bases":
            //                case "Administración/Configuración de Acceso a WebService":
            //                case "Administración/Acceso a Pac":
            //                case "Administración/Configuración de Envió de Correo por Empresa":
            //                case "Administración/Configuración de Envió de Correo":
            //                case "Administración/Visor de Logs":
            //                case "Administración/Configuración de PDF":
            //                    mainmenu.ChildItems.RemoveAt(i);
            //                    break;
            //                case "Administración/Usuarios":
            //                    for (int isubmenus = menu.ChildItems.Count - 1; isubmenus > -1; isubmenus--)
            //                    {
            //                        submenu = menu.ChildItems[isubmenus];

            //                        switch (submenu.ValuePath.Trim())
            //                        {
            //                            case "Administración/Usuarios/Desbloquear Usuario":
            //                                menu.ChildItems.RemoveAt(isubmenus);
            //                                break;
            //                        }
            //                    }
            //                    break;
            //            }

            //            //System.Diagnostics.Debug.WriteLine(menu.Value);
            //            System.Diagnostics.Debug.WriteLine(menu.ValuePath);
            //        }
            //    }
            //}
        }
    }
}
