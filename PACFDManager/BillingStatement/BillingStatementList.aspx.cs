﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.BillingStatement
{
    public partial class BillingStatementList : PACFDManager.BasePage
    {
        const string MESSAGE_BILLING_MISSID = "MESSAGE_BILLING_MISSID";


        /// <summary>
        /// 
        /// </summary>
        private int BillingID
        {
            get
            {
                int id;
                string s;

                s = this.Request.QueryString["pid"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                if (!int.TryParse(s, out id))
                    return 0;

                return id;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.BillingID < 1)
            {
                this.WebMessageBox1.ShowMessage("No se encuentra el (pre)comprobante.", true);
                return;
            }

            if (this.IsPostBack)
                return;

            this.DataBind((DateTime?)null, null, (bool?)null);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_BILLING_MISSID:
                    this.Response.Redirect("~/Billings/BillingsList.aspx");
                    break;
            }
        }

        protected void BillingStatementSearch1_Search(object sender, BillingStatementSearchEventArgs e)
        {
            this.DataBind(e.Date, e.Reference, e.Active);
        }

        public void DataBind(DateTime? date, string reference, bool? ispaid)
        {
            decimal d = 0;
            PACFD.Rules.BillingPayments bill = new PACFD.Rules.BillingPayments();

            base.DataBind();

            using (PACFD.DataAccess.BillingPaymentsDataSet.SearchDataTable table =
                bill.GetByBillingID(this.BillingID, date, reference, ispaid))
            {
                this.gdvBillingStatement.DataSource = table;
                this.gdvBillingStatement.DataBind();

                foreach (PACFD.DataAccess.BillingPaymentsDataSet.SearchRow srow in table)
                {
                    if (!srow.IsCurrencyCodeNull() && srow.CurrencyCode != "MXN")
                        d += srow.ExchangeRateMXN * srow.Amount;
                    else
                        d += srow.Amount;
                }

                this.lblPaidTotal.Text = string.Format("{0:0.00}", d);
            }

            this.LoadBilling();
        }

        private void LoadBilling()
        {
            decimal d;
            PACFD.Rules.Billings bil = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.BillingsDataTable table = bil.SelectByID(this.BillingID))
            {
                if (table.Count < 1)
                    return;

                this.lblBillingTotal.Text = string.Format("{0:0.00}", table[0].Total);
                d = decimal.Parse(this.lblPaidTotal.Text);
                this.lblLeftToPaid.Text = string.Format("{0:0.00}", table[0].Total - d);

                if (!table[0].IsIsPaidNull() && table[0].IsPaid)
                    this.btnPay2.Visible = this.btnPay.Visible = false;
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillingStatementAddPayments).Name + ".aspx?pid=" + this.Request.QueryString["pid"]);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("~/Billings/BillingsList.aspx");
        }

        protected string DataBind_CalculateDate(string date)
        {
            DateTime d1 = DateTime.Parse(date);
            TimeSpan t;
            t = DateTime.Now - d1;

            if (t.Days >= 365)
                return string.Format("{0} año(s)", t.Days / 365);

            return string.Format("{0} dia(s)", t.Days);
        }
    }
}
