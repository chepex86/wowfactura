﻿//Class variable declaration
//var jcBillingStatementAddPayments = new jclassBillingStatementAddPayments();

//ClassName: BillingStatement
function jclassBillingStatementAddPayments() {
    /*
    Name:       AmountToDeposit
    Used in:    BillingStatementAddPayments
    */
    Number.prototype.decimal = function(n) {
        pot = Math.pow(10, parseInt(n));
        return parseInt(this * pot) / pot;
    }

    function replaceComa(value) {
        value += "";

        while (value.indexOf(",") > -1)
            value = value.replace(",", "");

        return value;
    }

    this.amountToDeposit = function(e) {
        /*
        AmountToDepositClientID
        lblAmountToPayText
        lblTotalAmountText
        lblSubAmountText
        lblRestAmountText
        currencyTypeClientID
        exchangeRateMXNClientID
        */
        var total;
        var abonado;
        var deposito;

        try {
            //display update...

            deposito = document.getElementById(e.AmountToDepositClientID);
            deposito = deposito.value;
            deposito = replaceComa(deposito);
            deposito = parseFloat(deposito);
            
            //--------------------------exchange rate
            abonado = jQuery("#" + e.currencyTypeClientID)[0];

            if (abonado.selectedIndex > 0) {
                abonado = jQuery("#" + e.exchangeRateMXNClientID)[0];
                try {
                    abonado = parseFloat(abonado.value);
                }
                catch (ex) {
                    abonado = 1;
                }
                deposito *= abonado;
            }
            //--------------------------

            abonado = document.getElementById(e.lblAmountToPayText);

            if (deposito > 0) {
                total = "" + deposito;

                if (total.indexOf(".") < 0) {
                    abonado.innerText = "$" + total + ".00";
                }
                else {
                    abonado.innerText = "$" + total.substring(0, total.indexOf(".") + 3);
                }
            }
            else {
                abonado.innerText = "$0.00";
            }

            abonado = null; //clear reference
            total = null;

            //calculate todal
            total = document.getElementById(e.lblTotalAmountText);
            total = total.innerText;
            total = total.substring(1, total.length);

            total = replaceComa(total);

            total = parseFloat(total);

            abonado = document.getElementById(e.lblSubAmountText);
            abonado = abonado.innerText;
            abonado = abonado.substring(1, abonado.length);

            abonado = replaceComa(abonado);

            abonado = parseFloat(abonado);

            deposito = abonado + deposito;

            abonado = document.getElementById(e.lblRestAmountText);
            total = Math.round((total - deposito) * Math.pow(10, 2)) / Math.pow(10, 2);
            total = "" + total;
            deposito = total.indexOf(".");
            abonado.innerText = "$" + total.substring(0, deposito + 3);
            total = parseFloat(total);

            deposito = document.getElementById(e.lblTotalAmountText);
            deposito = deposito.innerText;
            deposito = deposito.substring(1, deposito.length);

            deposito = replaceComa(deposito);

            deposito = parseFloat(deposito);

            if (total < 0 || isNaN(total) || total > deposito) {
                abonado.style.color = "red";
            }
            else {
                abonado.style.color = "green";
            }
        }
        catch (ex) {
            abonado = document.getElementById(e.lblRestAmountText);
            abonado.innerText = "Error calculando total.";
        }
    }
}