﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingStatementEditor.ascx.cs"
    Inherits="PACFDManager.BillingStatement.BillingStatementEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div>

    <script src="BillingStatementEditor.js" type="text/javascript"></script>

    <ul>
        <li class="left"><span class="vtip" title="La <b>fecha</b> en que se realizo el abono.">
            <label class="desc">
                <asp:Label ID="lblDate" runat="server" Text="Fecha de pago" />
            </label>
            <span>
                <asp:TextBox ID="txtDate" runat="server" />
                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" TargetControlID="txtDate" />
            </span>
            <div class="validator-msg">
                <asp:RangeValidator ID="txtDate_RangeValidator" runat="server" ErrorMessage="Fecha no valida eje. dd/MM/yyyy"
                    Display="Dynamic" ControlToValidate="txtDate" Type="Date" MaximumValue="31/12/9999"
                    MinimumValue="31/12/1900" />
            </div>
        </span></li>
        <li><span class="vtip" title="Referencia del abono.">
            <label class="desc">
                <asp:Label ID="lblReference" runat="server" Text="Referencia" />
            </label>
            <span>
                <asp:TextBox ID="txtReference" runat="server" />
            </span></span></li>
        <li class="left"><span class="vtip" title="La cantidad que se va a <b>abonar</b>.">
            <label class="desc">
                <asp:Label ID="lblAmount" runat="server" Text="Cantidad" />
            </label>
            <span>
                <asp:TextBox ID="txtAmount" runat="server" />
            </span></span>
            <div class="validator-msg">
                <asp:RangeValidator ID="txtAmount_RangeValidator" runat="server" ErrorMessage="Formato incorrecto eje. 0.00"
                    ControlToValidate="txtAmount" Type="Currency" Display="Dynamic" MaximumValue="9,999,999.99"
                    MinimumValue="0.00" />
            </div>
        </li>
        <li>
            <label class="desc">
                Tipo de cambio
            </label>
            <div>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <span class="vtip" title="Tipo de <b>moneda</b> a usar para <b>abonar</b>.">
                                <asp:DropDownList ID="ddlCurrencyType" runat="server">
                                    <asp:ListItem Selected="True" Text="MXN" Value="0" />
                                    <asp:ListItem Text="USD" Value="1" />
                                </asp:DropDownList>
                            </span>
                        </td>
                        <td>
                            <span id="spanTxtExchangeRateMXN" runat="server" style="display: none"><span class="vtip"
                                title="Tipo de <b>cambio</b> en <b>pesos</b> mexicanos <img src='../Includes/Images/png/mxnflag.png' />.">
                                =<asp:TextBox ID="txtExchangeRateMXN" runat="server" Text="" />
                                MXN </span></span>
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    </ul>
</div>
