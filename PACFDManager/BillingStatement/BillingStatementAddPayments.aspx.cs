﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Text;

namespace PACFDManager.BillingStatement
{
    /// <summary>
    /// Class used to add payments
    /// </summary>
    public partial class BillingStatementAddPayments : PACFDManager.BasePage
    {
        const string MESSAGE_BILLING_MISSID = "MESSAGE_BILLING_MISSID";
        const string MESSAGE_BILLING_UPDATEERROR = "MESSAGE_BILLING_UPDATEERROR";
        const string MESSAGE_BILLING_AMOUNTERROR = "MESSAGE_BILLING_AMOUNTERROR";

        /// <summary>
        /// Billing ID to modify.
        /// </summary>
        private int BillingID
        {
            get
            {
                int id;
                string s;

                s = this.Request.QueryString["pid"];

                if (string.IsNullOrEmpty(s))
                    return 0;

                s = s.Replace(' ', '+');
                s = PACFD.Common.Cryptography.DecryptUnivisitString(s);

                if (!int.TryParse(s, out id))
                    return 0;

                return id;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.BillingID < 1)
            {
                this.WebMessageBox1.ShowMessage("No se encuentra el (pre)comprobante.", true);
                return;
            }

            if (this.IsPostBack)
                return;

            this.BillingStatementEditor1.BillingInformation.BillingID = this.BillingID;
            this.SetJavaScript_CalculateTotals();
            this.LoadTotals();
        }

        private void SetJavaScript_CalculateTotals()
        {
            string s = string.Format("var {0}_jcBillingStatementAddPayments = new jclassBillingStatementAddPayments();"
                , this.ClientID);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "BillingStatementAddPayments", s, true);

            this.BillingStatementEditor1.BillingInformation.AmountOnTextChanged = string.Format("javascript:{0}_jcBillingStatementAddPayments.amountToDeposit({{ {1} }});", this.ClientID
                , string.Format("AmountToDepositClientID:'{0}', lblAmountToPayText:'{1}', lblTotalAmountText:'{2}', lblSubAmountText:'{3}', lblRestAmountText:'{4}'"
                    + ", currencyTypeClientID:'{5}', exchangeRateMXNClientID:'{6}'"
                    , this.BillingStatementEditor1.BillingInformation.AmountToDepositClientID
                    , this.lblAmountToPayText.ClientID
                    , this.lblTotalAmountText.ClientID
                    , this.lblSubAmountText.ClientID
                    , this.lblRestAmountText.ClientID
                    , this.BillingStatementEditor1.BillingInformation.CurrencyTypeClientID
                    , this.BillingStatementEditor1.BillingInformation.ExchangeRateMXNClientID
                ));
        }
        /// <summary>
        /// Load the current amount paid and the billing total.
        /// </summary>
        private void LoadTotals()
        {
            decimal total, paid;

            total = this.GetBillingTotal();
            this.lblTotalAmountText.Text = total.ToString("C");

            paid = this.GetBillingPaidAmount();
            this.lblSubAmountText.Text = paid.ToString("C");

            this.lblTotalMinusText.Text = (total - paid).ToString("C");
        }
        /// <summary>
        /// Get the current total mount paid 
        /// </summary>
        /// <returns>Return a decimal value with the total current amount paid.</returns>
        private decimal GetBillingPaidAmount()
        {
            decimal paytotal = 0;
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            PACFD.Rules.BillingPayments pay = new PACFD.Rules.BillingPayments();

            using (PACFD.DataAccess.BillingPaymentsDataSet.SearchDataTable paytable =
                pay.GetByBillingID(this.BillingID, (DateTime?)null, null, (bool?)null))
            {
                foreach (PACFD.DataAccess.BillingPaymentsDataSet.SearchRow payrow in paytable)
                {
                    if (!payrow.IsCurrencyCodeNull() && payrow.CurrencyCode != "MXN")
                        paytotal += payrow.ExchangeRateMXN * payrow.Amount;
                    else
                        paytotal += payrow.Amount;
                }

                return paytotal;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private decimal GetBillingTotal()
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable = billing.SelectByID(this.BillingID))
            {
                if (billtable.Count < 0)
                    return 0;

                return billtable[0].Total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillingStatementList).Name + ".aspx?pid=" + this.Request.QueryString["pid"]);
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            try
            {
                this.BillingStatementEditor1.InvokeApplyChange();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(ex.Message);
#endif
            }
        }

        protected void BillingStatementEditor1_ApplyChange(object sender, BillingStatementApplyChangesEventArgs e)
        {
            decimal total;
            PACFD.Rules.Billings billing;
            PACFD.Rules.BillingPayments pay = new PACFD.Rules.BillingPayments();
            PACFD.DataAccess.BillingPaymentsDataSet.SearchDataTable searchtable;

            if (this.BillingStatementEditor1.BillingInformation.Amount < 0.01m)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_BILLING_AMOUNTERROR;
                this.WebMessageBox1.ShowMessage("La cantidad abonada no es valida.", true);
                return;
            }

            if (this.BillingStatementEditor1.BillingInformation.Amount + this.GetBillingPaidAmount() > Math.Round(this.GetBillingTotal(), 2))
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_BILLING_AMOUNTERROR;
                this.WebMessageBox1.ShowMessage("La cantidad abonada sobrepasa a la cantidad debida.", true);
                return;
            }

            if (!pay.Update(e.DataTable))
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_BILLING_UPDATEERROR;
                this.WebMessageBox1.ShowMessage("No se pudo agregar el pago.", true);
                return;
            }

            searchtable = pay.GetByBillingID(this.BillingID, (DateTime?)null, null, (bool?)null);
            total = 0;

            foreach (PACFD.DataAccess.BillingPaymentsDataSet.SearchRow row in searchtable)
            {
                if (!row.IsCurrencyCodeNull() && row.CurrencyCode != "MXN")
                    total += row.ExchangeRateMXN * row.Amount;
                else
                    total += row.Amount;
            }

            if (Math.Round(total, 2).ToString() == this.lblTotalAmountText.Text.Replace("$", "").Replace(",", ""))
            {
                billing = new PACFD.Rules.Billings();
                billing.SetPaidTo(this.BillingID, true);
            }

            this.btnCancel_Click(null, EventArgs.Empty);
        }
    }
}
