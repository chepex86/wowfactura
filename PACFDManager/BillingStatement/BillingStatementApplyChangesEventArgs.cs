﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.BillingStatement
{
    public class BillingStatementApplyChangesEventArgs : EventArgs 
    {
        public PACFD.DataAccess.BillingPaymentsDataSet.BillingPaymentsDataTable DataTable { get; private set; }


        public BillingStatementApplyChangesEventArgs(PACFD.DataAccess.BillingPaymentsDataSet.BillingPaymentsDataTable table)
        {
            this.DataTable = table;
        }
    }
}
