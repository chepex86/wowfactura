﻿function jclassBillingStatementEditor(controls) {
    var that = this;

    jQuery(document).ready(function() {
        jQuery("#" + controls.ddlCurrencyType).change(function() { that.ddlCurrencyTypeChange(jQuery(this)[0]); });
    });

    this.ddlCurrencyTypeChange = function(e) {
        if (e.selectedIndex > 0)
        //jQuery("[id$='" + controls.spanTxtExchangeRateMXN + "']").show(400);
            jQuery("#" + controls.spanTxtExchangeRateMXN).show(400);
        else
            jQuery("#" + controls.spanTxtExchangeRateMXN).hide(400);
        //jQuery("[id$='" + controls.spanTxtExchangeRateMXN + "']").hide(400);
    }
}