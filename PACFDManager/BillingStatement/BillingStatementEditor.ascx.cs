﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.BillingStatement
{
    /// <summary>
    /// Billing statemnt editor.
    /// </summary>
    public partial class BillingStatementEditor : System.Web.UI.UserControl
    {
        /// <summary>
        /// Event handler of the apply changes event.
        /// </summary>
        public event ApplyChangeEventHandler ApplyChange;

        /// <summary>
        /// Get the information properties of the control.
        /// </summary>
        public BillingStatementEditorInformation BillingInformation { get { return new BillingStatementEditorInformation(this); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadJavaScript();

            if (this.IsPostBack)
                return;

            this.txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
        /// <summary>
        /// Set the javascript code.
        /// </summary>
        private void LoadJavaScript()
        {
            string s;
            //ScriptReference sc = new ScriptReference("/BillingStatementEditor.js");
            //ScriptManager manager = ScriptManager.GetCurrent(this.Page);

            //if (!manager.Scripts.Contains(sc))
            //    manager.Scripts.Add(sc);

            s = string.Format("var {0}_jcBillingStatementEditor = new jclassBillingStatementEditor({{ {1} }})"
                , this.ClientID
                , string.Format("ddlCurrencyType:'{0}', txtExchangeRateMXN:'{1}', spanTxtExchangeRateMXN:'{2}'"
                    , this.ddlCurrencyType.ClientID
                    , this.txtExchangeRateMXN.ClientID
                    , this.spanTxtExchangeRateMXN.ClientID
                ));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "BillingStatementEditor", s, true);
        }
        /// <summary>
        /// Create and invoke the ApplyChange event.
        /// </summary>
        public void InvokeApplyChange()
        {
            decimal amount = 1;

            PACFD.DataAccess.BillingPaymentsDataSet.BillingPaymentsDataTable table =
                new PACFD.DataAccess.BillingPaymentsDataSet.BillingPaymentsDataTable();

            amount = 
                //this.ddlCurrencyType.SelectedIndex > 0 ?
                //this.BillingInformation.ExchangeRateMXN * this.BillingInformation.Amount : 
                this.BillingInformation.Amount;

            table.AddBillingPaymentsRow(
                this.BillingInformation.BillingID
                , this.BillingInformation.PaidDate
                , this.BillingInformation.Reference
                , amount
                , this.BillingInformation.ExchangeRateMXN
                , this.BillingInformation.CurrencyTypeCode
                );

            this.OnApplyChange(new BillingStatementApplyChangesEventArgs(table));
        }
        /// <summary>
        /// Invoke the ApplyChange event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnApplyChange(BillingStatementApplyChangesEventArgs e)
        {
            if (!this.ApplyChange.IsNull())
                this.ApplyChange(this, e);
        }

        /// <summary>
        /// Class user as agrupate the properties of the BillingStatementEditor class.
        /// </summary>
        public class BillingStatementEditorInformation
        {
            /// <summary>
            /// Owner of the class.
            /// </summary>
            private BillingStatementEditor Parent { get; set; }
            /// <summary>
            /// Get or set the actual currency type selected.
            /// </summary>
            public string CurrencyTypeCode
            {
                get { return this.Parent.ddlCurrencyType.SelectedItem != null ? this.Parent.ddlCurrencyType.SelectedItem.Text : "MXN"; }
                set
                {
                    ListItem l;

                    this.Parent.ddlCurrencyType.ClearSelection();
                    l = this.Parent.ddlCurrencyType.Items.FindByText(value);

                    if (l != null)
                        l.Selected = true;
                    else
                        this.Parent.ddlCurrencyType.SelectedIndex = 0;
                }
            }
            /// <summary>
            /// Get or set the current mxn exchange rate.
            /// </summary>
            public decimal ExchangeRateMXN
            {
                get
                {
                    decimal d = 0;
                    return decimal.TryParse(this.Parent.txtExchangeRateMXN.Text, out d) ? d : 1;
                }
                set { this.Parent.txtExchangeRateMXN.Text = value.ToString(); }
            }
            /// <summary>
            /// Get or set the string reference of the payment.
            /// </summary>
            public string Reference { get { return this.Parent.txtReference.Text; } set { this.Parent.txtReference.Text = value; } }
            /// <summary>
            /// Get or set the decimal ammount of the payment.
            /// </summary>
            public decimal Amount
            {
                get
                {
                    decimal d;
                    return !decimal.TryParse(this.Parent.txtAmount.Text, out d) ? 0 : d;
                }
                set { this.Parent.txtAmount.Text = value.ToString("{0:0.00}"); }
            }
            /// <summary>
            /// Get or set the current billing ID.
            /// </summary>
            public int BillingID
            {
                get
                {
                    int r;
                    object o;

                    o = this.Parent.ViewState[this.Parent.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                    if (o.IsNull())
                        return 0;

                    if (!int.TryParse(o.ToString(), out r))
                        return 0;

                    return r;
                }
                set { this.Parent.ViewState[this.Parent.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
            }
            /// <summary>
            /// Get or set the datetime when the payment is registered.
            /// </summary>
            public DateTime PaidDate
            {
                get
                {
                    DateTime date;

                    if (!DateTime.TryParse(this.Parent.txtDate.Text, out date))
                        date = DateTime.Now;

                    return date;
                }
                set { this.Parent.txtDate.Text = value.ToString("{0:dd/MM/yyyy}"); }
            }
            /// <summary>
            /// Get a string value with the ammount to deposit textbox client ID.
            /// </summary>
            public string AmountToDepositClientID { get { return this.Parent.txtAmount.ClientID; } }
            /// <summary>
            /// Get or set the javascript code to call, when the AmountToDeposit textbox change.
            /// </summary>
            public string AmountOnTextChanged { get { return this.Parent.txtAmount.Attributes["onkeyup"]; } set { this.Parent.txtAmount.Attributes["onkeyup"] = value; } }
            /// <summary>
            /// Get a string with the currency type dropdownlist client ID.
            /// </summary>
            public string CurrencyTypeClientID { get { return this.Parent.ddlCurrencyType.ClientID; } }
            /// <summary>
            /// Get a string with the Exchange rate mxn textbox client ID.
            /// </summary>
            public string ExchangeRateMXNClientID { get { return this.Parent.txtExchangeRateMXN.ClientID; } }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">Owner of the class.</param>
            public BillingStatementEditorInformation(BillingStatementEditor owner) { this.Parent = owner; }
        }
    }

    /// <summary>
    /// Delegate of the apply change event.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the sender.</param>
    public delegate void ApplyChangeEventHandler(object sender, BillingStatementApplyChangesEventArgs e);
}