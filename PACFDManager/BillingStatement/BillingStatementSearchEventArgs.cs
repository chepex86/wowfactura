﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.BillingStatement
{
    /// <summary>
    /// Class used as argument for search billing statements
    /// </summary>
    public class BillingStatementSearchEventArgs
    {
        /// <summary>
        /// Get a nullable datetime used to search
        /// </summary>
        public DateTime? Date { get; private set; }
        /// <summary>
        /// Get a string with the reference to look for.
        /// </summary>
        public string Reference { get; private set; }
        /// <summary>
        /// Gert a nullable boolean with the active selected state.
        /// </summary>
        public bool? Active { get; private set; }


        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="date">Nullable DateTime used to seach.</param>
        /// <param name="reference">Reference to llok for.</param>
        public BillingStatementSearchEventArgs(DateTime? date, string reference, bool? active)
        {
            this.Date = date;
            this.Reference = reference;
            this.Active = active;
        }
    }
}
