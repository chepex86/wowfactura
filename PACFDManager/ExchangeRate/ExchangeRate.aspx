﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="ExchangeRate.aspx.cs"
    Inherits="PACFDManager.ExchangeRate.ExchangeRate" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe60">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitleExchangeRate" runat="server" Text="Ingresar el tipo de cambio manualmente" />
                            </h2>
                            <ul>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleLatestRevisionExchange" runat="server" Text="Ultima revisión de tipo de cambio:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblLatestRevisionExchange" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblExchangeRate" runat="server" Text="Tipo de Cambio:" />
                                    </label>
                                    <span>
                                        <asp:TextBox ID="txtExchangeRate" runat="server" MaxLength="8" Width="70px" />
                                    </span>
                                    <div class="validator-msg">
                                        <asp:RequiredFieldValidator ID="rfvRate" runat="server" ErrorMessage="Este campo es requerido"
                                            ControlToValidate="txtExchangeRate" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revRate" runat="server" ErrorMessage="Solo número o decimales"
                                            ControlToValidate="txtExchangeRate" Display="Dynamic" 
                                            ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,5})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$" 
                                            ValidationGroup="Validators"></asp:RegularExpressionValidator>
                                    </div>
                                </li>
                                <li class="left"></li>
                                <li class="buttons">
                                    <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" ValidationGroup="Validators"
                                        OnClick="btnAccept_Click" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Cancelar" PostBackUrl="~/Default.aspx" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
