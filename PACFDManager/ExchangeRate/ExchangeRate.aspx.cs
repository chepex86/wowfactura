﻿#region using
using System;
using System.Data;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Xml;
using System.Globalization;
using PACFD.Rules.mx.org.banxico.www;
#endregion

namespace PACFDManager.ExchangeRate
{
    public partial class ExchangeRate : PACFDManager.BasePage
    {
        private DgieWS ExchangeRateWS { set; get; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.FillInfo();
        }

        private void FillInfo()
        {
            SystemConfigurationDataSet.SystemConfigurationDataTable t;
            SystemConfiguration sys = new SystemConfiguration();

            t = sys.GetAllSystemConfiguration();

            if (t.Count < 1)
                this.Response.Redirect("~/Default.aspx");

            this.txtExchangeRate.Text = t[0].ExchangeRate.ToString();
            this.lblLatestRevisionExchange.Text = this.FormatDate(t[0].LatestRevisionExchangeRate);

            this.lblLatestRevisionExchange.ForeColor =
                Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")) >
                    Convert.ToInt32(t[0].LatestRevisionExchangeRate.ToString("yyyyMMdd")) ?
                    System.Drawing.Color.FromArgb(172, 0, 48) : System.Drawing.Color.FromArgb(43, 97, 45);

            this.lblLatestRevisionExchange.BackColor =
                Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")) >
                    Convert.ToInt32(t[0].LatestRevisionExchangeRate.ToString("yyyyMMdd")) ?
                    System.Drawing.Color.FromArgb(255, 199, 206) : System.Drawing.Color.FromArgb(198, 239, 206);

            Security.Security.SetExchangeRate();

        }

        private String GetExchangeRate()
        {
            String exchangerate = String.Empty;
            DataSet ds = new DataSet();

            this.ExchangeRateWS = new DgieWS();

            try
            {
                exchangerate = this.ExchangeRateWS.tiposDeCambioBanxico();
                ds = this.ReadXML(exchangerate);

                return ds.Tables["Obs"].Rows[0]["TIME_PERIOD"] + "|" + ds.Tables["Obs"].Rows[0]["OBS_VALUE"];
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return String.Empty;
            }
        }

        private DataSet ReadXML(String strXML)
        {
            DataSet ds = new DataSet();
            XmlDocument xmldoc = new XmlDocument();

            try
            {
                xmldoc.LoadXml(strXML);

                using (XmlReader reader = new XmlNodeReader(xmldoc))
                {
                    ds.ReadXml(reader);
                    reader.Close();
                }

                return ds;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return null;
            }
        }

        #region Format Date
        private String AddZero(int n)
        {
            return n < 10 ? "0" + n.ToString() : n.ToString();
        }

        private String FormatDate(DateTime d)
        {
            CultureInfo ci = new CultureInfo("Es-Es");
            String DayOfWeek = ci.DateTimeFormat.GetDayName(d.DayOfWeek);

            return DayOfWeek + ", " + d.Day + " de " +
                this.FormatDate(d.Month) + " del " + d.Year +
                    ",<br />a las " + this.AddZero(d.Hour) + ":" +
                        this.AddZero(d.Minute) + ":" + this.AddZero(d.Second);
        }

        private String FormatDate(int date)
        {
            String cad = String.Empty;

            switch (date)
            {
                case 1: cad = "Enero"; break;
                case 2: cad = "Febrero"; break;
                case 3: cad = "Marzo"; break;
                case 4: cad = "Abril"; break;
                case 5: cad = "Mayo"; break;
                case 6: cad = "Junio"; break;
                case 7: cad = "Julio"; break;
                case 8: cad = "Agosto"; break;
                case 9: cad = "Septiembre"; break;
                case 10: cad = "Octubre"; break;
                case 11: cad = "Noviembre"; break;
                case 12: cad = "Diciembre"; break;
            }

            return cad;
        }
        #endregion

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments == "0")
                return;
            if (e.CommandArguments == "1")
                this.Response.Redirect("~/Default.aspx");
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            if (!this.AddExchangeRate())
            {
                this.WebMessageBox1.Title = "Mensaje";
                this.WebMessageBox1.CommandArguments = "0";
                this.WebMessageBox1.ShowMessage("No ha sido posible actualizar el tipo de cambio a USD.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
            }
            else
            {
                this.WebMessageBox1.Title = "Mensaje";
                this.WebMessageBox1.CommandArguments = "1";
                this.WebMessageBox1.ShowMessage("El tipo de cambio a USD, ha sido actualizado correctamente.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
            }
        }

        private bool AddExchangeRate()
        {
            SystemConfigurationDataSet.SystemConfigurationDataTable t;
            SystemConfigurationDataSet.SystemConfigurationRow r;
            SystemConfiguration sys = new SystemConfiguration();

            t = sys.GetAllSystemConfiguration();

            try
            {
                if (t.Count < 1)
                    return false;

                r = t[0];

                r.ExchangeRate = Convert.ToDecimal(this.txtExchangeRate.Text.Trim());
                r.LatestRevisionExchangeRate = DateTime.Now;

                if (!sys.Update(t))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                return false;
            }
        }
    }
}