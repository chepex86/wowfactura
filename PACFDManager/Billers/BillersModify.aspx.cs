﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;

namespace PACFDManager.Billers
{
    public partial class BillersModify : PACFDManager.BasePage
    {
        /// <summary>
        /// Get a boolean value with modify behavior of the web page.
        /// </summary>
        private bool ModifyMode
        {
            get
            {
                object o;
                bool r;

                o = this.Request.QueryString["e"];

                if (o.IsNull())
                    return true;

                if (!bool.TryParse(o.ToString(), out r))
                    return true;

                return r;
            }
        }
        /// <summary>
        /// Get an int value with the id to edit.
        /// </summary>
        private int BillerID
        {
            get
            {
                object o;
                int r;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }
        /// <summary>
        /// Get or set a temporal byte array containing an image.
        /// </summary>
        private byte[] TemporalImage
        {
            get
            {
                byte[] r = null;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() == typeof(byte[]))
                    r = (byte[])o;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get or set a PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table.
        /// </summary>
        private PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable BillerTaxDataTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return new PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable();

                return (PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //protected bool IsOneTaxSelected
        //{
        //    get
        //    {
        //        CheckBox box;

        //        foreach (GridViewRow row in this.gdvTaxes.Rows)
        //        {
        //            box = row.FindControl("ckbGridTaxUse") as CheckBox;

        //            if (box.Checked)
        //                return true;
        //        }

        //        return false;
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Billers billers;
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;

            this.pnlIssued.Style["display"] = this.ckbIssued.Checked ? "none" : "block";

            this.ckbIssued.Enabled = this.ModifyMode;

            this.IssuedEditor1.EditorMode =
                this.ModifyMode ? IssuedEditorMode.Edit : IssuedEditorMode.View;

            this.BillersEditor1.EditorMode =
                this.ModifyMode ? BillersEditorMode.Edit : BillersEditorMode.View;

            if (this.IssuedEditor1.EditorMode == IssuedEditorMode.View)
                this.btnAcept.Visible = false;

            if (this.IsPostBack || this.BillerID < 1) //if is post back or biller id < 1 return...
                return;

            //this.BillersEditor1.ClientBillerID = this.CurrentClientID;
            this.BillersEditor1.GroupID = this.CurrentGroupID;
            this.BillersEditor1.Enabled = this.ModifyMode;
            this.BillersEditor1.BillerID = this.BillerID;
            this.BillersEditor1.DataBindBillerID();

            this.IssuedEditor1.BillerID = this.BillerID;
            this.IssuedEditor1.DataBlindBillerID();

            this.Image1.ImageUrl = string.Format("~/ImageHandler.ashx?type=1&BillerID={0}&width=200&height=200",
                this.BillerID.ToString());
            this.Image1.Visible = true;

            billers = new PACFD.Rules.Billers();
            table = billers.SelectByID(this.BillerID);
            billers = null;

            if (table.Count < 1)
            {
                this.WebMessageBox1.CommandArguments = string.Empty;
                this.WebMessageBox1.ShowMessage("No se pudo modificar el usuario, intentelo más tarde.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                return;
            }

            if (!table[0].IsLogoCFDNull())
                this.TemporalImage = table[0].LogoCFD;

            this.ckbIssued.Checked = !table[0].IssuedInDifferentPlace;
            this.pnlIssued.Style["display"] = this.ckbIssued.Checked ? "none" : "block";

            table.Dispose();
            table = null;

            this.IssuedEditor1.EnabledValidator = !this.ckbIssued.Checked;
            //this.DataBindGrid();
        }
        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments == "1")
                this.Response.Redirect(typeof(BillersList).Name + ".aspx");
            else if (e.CommandArguments == "2")
            {
                if (e.DialogResult == WebMessageBoxDialogResultType.No)
                    return;

                this.BillersEditor1.ApplyChanges();
                //this.ModifyBiller();
            }
            else if (e.CommandArguments == string.Empty)
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.WebMessageBox1.Title = "Modificar Empresa";
            this.WebMessageBox1.CommandArguments = "2";
            this.WebMessageBox1.ShowMessage("¿Seguro que deseas modificar esta empresa?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }
        protected void BillersEditor1_ApplyModify(object sender, BillersEditorEventArgs e)
        {
            //PACFD.DataAccess.BillersDataSet dataset = new PACFD.DataAccess.BillersDataSet();
            PACFD.DataAccess.BillersDataSet datasettemp = new PACFD.DataAccess.BillersDataSet();
            PACFD.Rules.Billers billers = new PACFD.Rules.Billers();
            ElectronicBillingType electronic = ElectronicBillingType.Indeterminate;

            this.WebMessageBox1.CommandArguments = "0";

            if (!this.GetEnabledelectronicBillingType(e.BillingDataSet.Billers[0].ElectronicBillingType))
            {
                LogManager.WriteError(new Exception("Can't change biller to a disabled/not avaliable bill."));
                this.WebMessageBox1.ShowMessage("Se trato de cambiar a tipo de factura no habilitado/disponible.", System.Drawing.Color.Red);
                return;
            }

            if (this.ModifyMode == false)
                this.Response.Redirect(typeof(BillersList).Name + ".aspx");

            //dataset.Billers.Clear();
            //dataset.Billers.Merge(this.BillersEditor1.BillerInformation.BillersDataTable, true); 

            datasettemp = e.BillingDataSet;

            e.BillingDataSet.Billers[0].BeginEdit();
            e.BillingDataSet.Billers[0].IssuedInDifferentPlace = !this.ckbIssued.Checked;

            if (e.BillingDataSet.Billers[0].IsLogoCFDNull())
                e.BillingDataSet.Billers[0].LogoCFD = this.TemporalImage;

            e.BillingDataSet.Billers[0].EndEdit();
            e.BillingDataSet.Billers[0].AcceptChanges();
            e.BillingDataSet.Billers[0].SetModified();

            e.BillingDataSet.Issued.Clear();
            e.BillingDataSet.Issued.Merge(this.ckbIssued.Checked ? e.BillingDataSet.Billers.GenerateIssuedFromBiller() :
                this.IssuedEditor1.IssuedInformation.IssuedDataTable, true);
            e.BillingDataSet.Issued[0].AcceptChanges();
            e.BillingDataSet.Issued[0].SetModified();

            if (!billers.Update(e.BillingDataSet))
            {
                //error handler ...
                LogManager.WriteError(new Exception("Error al modificar la empresa."));
                this.WebMessageBox1.ShowMessage("Error al modificar la empresa.", System.Drawing.Color.Red);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico la informacion una empresa.",
                this.UserName), this.CurrentBillerID, datasettemp.GetXml(), e.BillingDataSet.GetXml());
            #endregion

            //dataset.Dispose();
            datasettemp.Dispose();

            LogManager.WriteStackTrace(new Exception("Exito al modificar la empresa."));
            this.WebMessageBox1.CommandArguments = "1";
            this.WebMessageBox1.ShowMessage("Éxito al modificar la empresa.",
                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);

            electronic = (ElectronicBillingType)e.BillingDataSet.Billers[0].ElectronicBillingType;
            SetCurrentBillerID(this.CurrentBillerID, e.BillingDataSet.Billers[0].Name, electronic);
            SetCurrentBranchID(this.CurrentBranchID, CurrentBranchName);
        }
        /// <summary>
        /// get the state of the current enabled bills on the system.
        /// </summary>
        private bool GetEnabledelectronicBillingType(int i)
        {
            switch ((ElectronicBillingType)i)
            {
                case ElectronicBillingType.CFD:
                    if (!CFDEnable)
                        return false;
                    break;
                case ElectronicBillingType.CFDI:
                    if (!CFDIEnable)
                        return false;
                    break;
                case ElectronicBillingType.CBB:
                    if (!CBBEnable)
                        return false;
                    break;
                case ElectronicBillingType.CFD2_2:
                    if (!CFD2_2Enable)
                        return false;
                    break;
                case ElectronicBillingType.CFDI3_2:
                    if (!CFDI3_2Enable)
                        return false;
                    break;
                case ElectronicBillingType.CFDI3_3:
                    if (!CFDI3_3Enable)
                        return false;
                    break;
                case ElectronicBillingType.CFDI4_0:
                    if (!CFDI4_0Enable)
                        return false;
                    break;
                case ElectronicBillingType.Indeterminate:
                default:
                    return false;
            }

            return true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillersList).Name + ".aspx");
        }
        ///// <summary>
        ///// Data bind the tax grid view control, initialize the controls, select the tax of the biller.
        ///// </summary>
        //private void DataBindGrid()
        //{
        //    //PACFD.Rules.Billers billers;
        //    //PACFD.Rules.TaxTypes taxes;
        //    //PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDDataTable selectedtable;
        //    //PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable taxtypes;

        //    //billers = new PACFD.Rules.Billers();
        //    //selectedtable = billers.BillerTax.SelectByBillerID(this.BillerID);
        //    //billers = null;

        //    //taxes = new PACFD.Rules.TaxTypes();
        //    //taxtypes = taxes.SelectAllTaxTypes();
        //    //taxes = null;

        //    //this.OrderTaxesTable(ref selectedtable, ref taxtypes);

        //    //this.gdvTaxes.DataSource =
        //    //    this.BillerTaxDataTable = taxtypes;
        //    //this.gdvTaxes.DataBind();

        //    //this.DataBindInitializeRowsTaxes();
        //    //this.DataBindLastSelectedTaxes(ref selectedtable);

        //    //selectedtable.Dispose();
        //    //selectedtable = null;
        //}
    }
}

///// <summary>
///// Data bind the selected data biller - billertaxes, by the biller.
///// </summary>
//private void DataBindLastSelectedTaxes(ref PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDDataTable selectedtable)
//{
//    int i;
//    int idrop;
//    TextBox textbox;
//    DropDownList drop;
//    HiddenField hidden;
//    CheckBox checkbox;
//    GridViewRow gridrow;
//    HiddenField hiddenpreselected;

//    i = -1;

//    foreach (PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDRow row in selectedtable)
//    {
//        i = -1;
//        i = this.gdvTaxes.Rows.GetRowIndexByTaxTypeID(row.TaxTypeID);

//        if (i < 0)
//            continue;

//        gridrow = this.gdvTaxes.Rows[i];
//        gridrow.BackColor = System.Drawing.Color.LightGreen;
//        drop = gridrow.FindControl("ddlGridTax") as DropDownList;
//        textbox = gridrow.FindControl("txtGridTax") as TextBox;
//        hidden = gridrow.FindControl("hdfBillTaxID") as HiddenField;
//        checkbox = gridrow.FindControl("ckbGridTaxUse") as CheckBox;
//        hiddenpreselected = gridrow.FindControl("hdfPreselected") as HiddenField;

//        hidden.Value = row.BillerTaxID.ToString();
//        checkbox.Checked = true;
//        hiddenpreselected.Value = "1";

//        if (!drop.Visible)
//        {
//            textbox.Text = Math.Round(row.Value, 2).ToString();
//            continue;
//        }

//        idrop = -1;

//        foreach (ListItem listitem in drop.Items)
//        {
//            idrop++;

//            if (listitem.Text != Math.Round(row.Value, 2).ToString())
//                continue;

//            drop.SelectedIndex = idrop;
//            break;
//        }
//    }
//}
///// <summary>
///// Reorder the selected taxes vs the taxestypes, selected taxes go first.
///// </summary>
//private void OrderTaxesTable(ref PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDDataTable selected,
//    ref PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable taxes)
//{
//    List<object[]> list = new List<object[]>();
//    PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllRow order;
//    System.Data.DataRow[] rows = new System.Data.DataRow[selected.Count];
//    PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllRow t;
//    int i;

//    //remove select taxes (billertaxes) from the taxtype list
//    foreach (PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDRow s in selected)
//    {
//        for (i = taxes.Count - 1; i >= 0; i--) //must be in reverse
//        {
//            t = taxes[i];
//            if (t.TaxTypeID != s.TaxTypeID)
//                continue;

//            list.Add(t.ItemArray);
//            taxes.RemoveTaxTypesGetAllRow(t);
//            break;
//        }
//    }
//    //add it at the top list
//    for (i = 0; i < list.Count; i++)
//    {
//        order = taxes.NewTaxTypesGetAllRow();
//        order.ItemArray = list[i];
//        taxes.Rows.InsertAt(order, i);
//        order.AcceptChanges();
//        order = null;
//    }
//}
///// <summary>
///// Initialize the tax controls of the gridview.
///// </summary>
//private void DataBindInitializeRowsTaxes()
//{
//    PACFD.Rules.TaxTypes tax = new PACFD.Rules.TaxTypes();
//    HiddenField hidden, heditable;
//    DropDownList drop;
//    TextBox textbox;

//    foreach (GridViewRow row in this.gdvTaxes.Rows)
//    {
//        if (row.RowType != DataControlRowType.DataRow)
//            continue;

//        hidden = row.FindControl("hdfID") as HiddenField;
//        drop = row.FindControl("ddlGridTax") as DropDownList;
//        heditable = row.FindControl("hdfEditable") as HiddenField;
//        textbox = row.FindControl("txtGridTax") as TextBox;

//        if (heditable.Value.ToBoolean())
//        {
//            drop.Visible = false;
//            continue;
//        }

//        drop.DataSource = tax.SelectTaxValueByTaxTypeID(hidden.Value.ToInt32());
//        drop.DataBind();
//    }

//    tax = null;
//}
///// <summary>
///// Event that up or down the row index.
///// </summary>
//protected void ImageButton_Click(object sender, ImageClickEventArgs e)
//{
//    ImageButton b = sender as ImageButton;
//    string[] data;
//    int i;
//    List<object[]> listrowdata = new List<object[]>();

//    if (b.IsNull())
//        return;

//    data = b.CommandArgument.Split('_');

//    if (data.Length < 2)
//        return;

//    if (data[0] != "up" && data[0] != "down")
//        return;

//    if (!int.TryParse(data[1], out i))
//        return;

//    if (i < 0 && i > this.gdvTaxes.Rows.Count - 1)
//        return;

//    i = this.gdvTaxes.Rows.GetRowIndexByTaxTypeID(i);

//    if (i < 0)
//        return;

//    listrowdata = this.GetTemporalRowData();
//    this.BillerTaxDataTable.RowLevelIndex(i, data[0] == "up" ? true : false);
//    this.gdvTaxes.DataSource = this.BillerTaxDataTable;
//    this.gdvTaxes.DataBind();
//    this.DataBindInitializeRowsTaxes();
//    this.SetTemporalRowData(listrowdata);
//    listrowdata.Clear();
//    listrowdata = null;
//}
///// <summary>
///// Get a List class with an object[] array list with the temporal value of the rows in the grid.
///// </summary>
///// <returns>Return a List class with an object[] array list.</returns>
//private List<object[]> GetTemporalRowData()
//{
//    object[] data;
//    List<object[]> list = new List<object[]>();
//    TextBox textbox;
//    DropDownList dropdown;
//    CheckBox checkbox;
//    HiddenField hiddenid;
//    HiddenField hiddenbillertaxeid;
//    HiddenField hiddenpreselected;

//    foreach (GridViewRow row in this.gdvTaxes.Rows)
//    {
//        if (row.RowType != DataControlRowType.DataRow)
//            continue;

//        checkbox = row.FindControl("ckbGridTaxUse") as CheckBox;
//        dropdown = row.FindControl("ddlGridTax") as DropDownList;
//        textbox = row.FindControl("txtGridTax") as TextBox;
//        hiddenid = row.FindControl("hdfID") as HiddenField;
//        hiddenbillertaxeid = row.FindControl("hdfBillTaxID") as HiddenField;
//        hiddenpreselected = row.FindControl("hdfPreselected") as HiddenField;
//        data = new object[6];

//        if (!checkbox.IsNull())
//            data[0] = checkbox.Checked;

//        if (!dropdown.IsNull())
//            data[1] = dropdown.SelectedItem.IsNull() ? null : dropdown.SelectedItem.Text;

//        if (!textbox.IsNull())
//            data[2] = textbox.Text;

//        if (!hiddenbillertaxeid.IsNull())
//            data[3] = hiddenbillertaxeid.Value;

//        if (!hiddenpreselected.IsNull())
//            data[4] = hiddenpreselected.Value;

//        if (!hiddenid.IsNull()) // *** this value must be the last always. ***
//            data[5] = hiddenid.Value;

//        list.Add(data);
//        data = null;
//        checkbox = null;
//        dropdown = null;
//        textbox = null;
//        hiddenid = null;
//    }

//    return list;
//}
///// <summary>
///// Set the List class with an object[] array list with the temporal value of the rows in the grid.
///// </summary>
///// <param name="list">
///// List class that containg an object[] array, the format of the array value object must be:
///// object[] { boolean, string, string, integer32 }
///// </param>
//private void SetTemporalRowData(List<object[]> list)
//{
//    TextBox textbox;
//    DropDownList dropdown;
//    CheckBox checkbox;
//    HiddenField hiddenid;
//    HiddenField hiddenpreselected;
//    HiddenField hiddenbillertaxeid;
//    GridViewRow row;
//    int index;

//    foreach (object[] o in list)
//    {
//        index = o[o.Length - 1].ToString().ToInt32(); // *** this value must be the last always. ***

//        if (index < 1)
//            continue;

//        index = this.gdvTaxes.Rows.GetRowIndexByTaxTypeID(index);

//        if (index < 0)
//            continue;

//        row = this.gdvTaxes.Rows[index];

//        checkbox = row.FindControl("ckbGridTaxUse") as CheckBox;
//        dropdown = row.FindControl("ddlGridTax") as DropDownList;
//        textbox = row.FindControl("txtGridTax") as TextBox;
//        hiddenid = row.FindControl("hdfID") as HiddenField;
//        hiddenbillertaxeid = row.FindControl("hdfBillTaxID") as HiddenField;
//        hiddenpreselected = row.FindControl("hdfPreselected") as HiddenField;

//        checkbox.Checked = o[0].ToString().ToBoolean();
//        hiddenbillertaxeid.Value = o[3].ToString();
//        hiddenid.Value = o[5].ToString();

//        if (o[4] != null)
//        {
//            if (!string.IsNullOrEmpty(o[4].ToString().Trim()) && o[4].ToString().ToBoolean() && o[4].ToString() == "1")
//            {
//                row.BackColor = System.Drawing.Color.LightGreen;
//                hiddenpreselected.Value = "1";
//            }
//            else
//                hiddenpreselected.Value = "0";
//        }

//        index = 0;

//        foreach (ListItem item in dropdown.Items) //reselect the item in the DropDownList
//        {
//            if (dropdown.Items[index].Text != o[1].ToString())
//            {
//                index++;
//                continue;
//            }

//            dropdown.SelectedIndex = index;
//        }

//        textbox.Text = o[2].ToString();

//        checkbox = null;
//        dropdown = null;
//        textbox = null;
//        hiddenid = null;
//    }
//}
///// <summary>
///// Generate the table to add tax to the biller.
///// </summary>
///// <returns>Return a PACFD.DataAccess.TaxTypesDataSet.BillerTaxesDataTable table.</returns>
//private PACFD.DataAccess.BillersDataSet.BillerTaxesDataTable GenerateTableForTaxes(int billerid)
//{
//    TextBox textbox;
//    DropDownList dropdown;
//    CheckBox checkbox;
//    HiddenField hiddenid, hdfbilltaxid;
//    GridViewRow gridrow;
//    HiddenField hiddenpreselected;
//    PACFD.DataAccess.BillersDataSet.BillerTaxesDataTable table =
//        new PACFD.DataAccess.BillersDataSet.BillerTaxesDataTable();
//    PACFD.DataAccess.BillersDataSet.BillerTaxesRow row;

//    for (int i = 0; i < this.gdvTaxes.Rows.Count; i++)
//    {
//        gridrow = this.gdvTaxes.Rows[i];

//        if (gridrow.RowType != DataControlRowType.DataRow)
//            continue;

//        checkbox = gridrow.FindControl("ckbGridTaxUse") as CheckBox;
//        dropdown = gridrow.FindControl("ddlGridTax") as DropDownList;
//        textbox = gridrow.FindControl("txtGridTax") as TextBox;
//        hiddenid = gridrow.FindControl("hdfID") as HiddenField;
//        hdfbilltaxid = gridrow.FindControl("hdfBillTaxID") as HiddenField;
//        hiddenpreselected = gridrow.FindControl("hdfPreselected") as HiddenField;

//        row = table.NewBillerTaxesRow();
//        row.BeginEdit();
//        row.BillerID = billerid;
//        row.TaxTypeID = hiddenid.Value.ToInt32();
//        row.Value = dropdown.Visible ? dropdown.SelectedItem.Text.ToDecimal() : textbox.Text.ToDecimal();

//        row.BillerTaxID = !string.IsNullOrEmpty(hdfbilltaxid.Value.Trim()) ? hdfbilltaxid.Value.ToInt32() : i * -1;

//        row.TaxIndex = table.Count;
//        table.AddBillerTaxesRow(row);
//        row.AcceptChanges();

//        if (hiddenpreselected.Value == "1")
//            row.SetModified();

//        if (checkbox.Checked && row.RowState == System.Data.DataRowState.Unchanged)
//            row.SetAdded();
//        else if (!checkbox.Checked)
//            row.Delete();

//        row.EndEdit();

//        checkbox = null;
//        dropdown = null;
//        textbox = null;
//        hiddenid = null;
//        hdfbilltaxid = null;
//        row = null;
//    }

//    return table;
//}