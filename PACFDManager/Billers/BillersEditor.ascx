﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillersEditor.ascx.cs"
    Inherits="PACFDManager.Billers.BillersEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../CountrySelector.ascx" TagName="CountrySelector" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>

<script language="javascript" type="text/javascript">
    function reset() {
        ddlBill = document.getElementById('<%=ddlBillType.ClientID%>');
        ddlCure = document.getElementById('<%=ddlCurrency.ClientID%>');

        if (ddlBill.options[0].selected) {
            ddlCure.options[0].selected = true;
        }
    }
</script>

<div class="info">
    <label class="desc">
        <h2>
            <asp:Label ID="lblTitle" runat="server" Text="Empresa" />
        </h2>
    </label>
</div>
<ul>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Nombre:" Width="400px" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre de la empresa con el que está<br />constituida ante el Notario Público ó la razón social.<br /><br /><b>Nota:</b> La razón social está escrita en la cedula de identificación fiscal <b>RFC</b>."%>'>
                        <asp:TextBox ID="txtName" runat="server" Width="400px" MaxLength="70" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblRFC" runat="server" Text="RFC:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el <b>*RFC</b> de la empresa, este es pedido por<br />la <b>SHCP</b> (Secretaría de Hacienda y Crédito Público).<br /><br /><b>•[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?</b><br/><b>•ABCD123456ABC</b><br><br><b>Nota: <i>*</i></b> <i>Registro Federal de Contribuyentes.</i>"%>'>
                        <asp:TextBox ID="txtRFC" runat="server" MaxLength="13" Width="110px" /></span>
                </td>
            </tr>
            <tr> 
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvRFC" runat="server" ControlToValidate="txtRFC"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                        <asp:RegularExpressionValidator ID="revRFC" runat="server" Display="Dynamic" ErrorMessage="RFC no valido. [A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?"
                            ControlToValidate="txtRFC" ValidationExpression="[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?" />
                        
                        <!--
                        
                        [A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?
                        
                        ([A-Z|a-z]|&amp;){3,4}\d{6}\w{3}$
                        
                        -->
                        <!--
                            ([A-Z|a-z]|&amp;){3,4}\d{6}\w{3}$
                            [A-Z,Ñ,&amp;]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?
                            -->
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblAddress" runat="server" Text="Dirección:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " la(s) calle(s) donde se encuentra ubicada fisicamente la empresa."%>'>
                        <asp:TextBox ID="txtAddress" runat="server" Width="350px" MaxLength="100" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblExternalNumber" runat="server" Text="Número externo:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el número exterior del lugar (local, edificio, terreno),<br />en donde se encuentra ubicada la empresa."%>'>
                        <asp:TextBox ID="txtExternalNumber" runat="server" MaxLength="6" Width="45px" ReadOnly="true" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblInternalNumber" runat="server" Text="Número interno:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el número interior del lugar (local, edificio, terreno),<br />en donde se encuentra ubicada la empresa."%>'>
                        <asp:TextBox ID="txtInternalNumber" runat="server" MaxLength="6" Width="45px" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblColony" runat="server" Text="Colonia:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre de la Colonia donde se encuentra ubicada la empresa."%>'>
                        <asp:TextBox ID="txtColony" runat="server" MaxLength="25" Width="145px" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblReference" runat="server" Text="Referencia:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " alguna referencia que ayude a ubicar más fácilmente la empresa.<br />Ej. <b>Una calle, un edificio, un monumento, etc.</b>"%>'>
                        <asp:TextBox ID="txtReference" runat="server" Width="200px" MaxLength="255" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblPhone" runat="server" Text="Teléfono:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " Escriba el número de teléfono fijo de la empresa.<br />Ej. &lt;b&gt;01 612 12 2 22 22&lt;/b&gt;"%>'>
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="20" Width="150px" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblCity" runat="server" Text="Ciudad:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre de la Ciudad en donde<br />se encuentra establecida la empresa. "%>'>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="20" Width="180px" />
                    </span>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblMunicipality" runat="server" Text="Municipio:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre del Municipio en donde<br />se encuentra establecida la empresa." %>'>
                        <asp:TextBox ID="txtMunicipality" runat="server" MaxLength="40" Width="180px" />
                    </span>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvMunicipality" runat="server" ControlToValidate="txtMunicipality"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <uc1:CountrySelector ID="CountrySelector1" runat="server" />
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblZipcode" runat="server" Text="Código postal:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el Código postal de la empresa.<br />Ej. &lt;b&gt;23090&lt;/b&gt;"%>'>
                        <asp:TextBox ID="txtZipcode" runat="server" MaxLength="10" Width="80px" /></span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="rfvZipcode" runat="server" ControlToValidate="txtZipcode"
                            Display="Dynamic" ErrorMessage="Este campo es requerido" />
                        <asp:RegularExpressionValidator ID="revZopcode" runat="server" Display="Dynamic"
                            ErrorMessage="Solo numeros (0 - 9)." ControlToValidate="txtZipcode" ValidationExpression="^\d+$" /></div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblLogo" runat="server" Text="Logotipo:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Selecciona la ruta donde se encuentra almacenado<br />el <b>Logotipo</b>, el cual será usado para identificada la empresa.">
                        <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server" OnUploadedComplete="AsyncFileUpload1_UploadedComplete" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblBillType" runat="server" Text="Tipo de Factura:" Visible="true" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Selecciona el Tipo de Factura con<br />la cual la empresa va a trabajar.">
                        <asp:DropDownList ID="ddlBillType" runat="server" AutoPostBack="True" Visible="true"
                            OnSelectedIndexChanged="ddlBillType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblCurrency" runat="server" Text="Moneda:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span>
                        <asp:DropDownList ID="ddlCurrency" runat="server" Enabled="False">
                            <asp:ListItem Selected="False" Text="MXN" Value="0" />
                            <asp:ListItem Text="USD" Value="1" />
                        </asp:DropDownList>
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblIsDictaminated" runat="server" Text="Dictaminado:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span>
                        <asp:CheckBox ID="cbIsDictaminated" runat="server" Checked="false" Text="" />
                    </span>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblEmail" runat="server" Text="Correo Electrónico:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Introduce una dirección de correo electrónico valida.<br />Ej. <b>nombre@mail.com</b>">
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="200px" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email incorrecto (ejempl@oz.com)"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" /></div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblBankAccount" runat="server" Text="Cuenta bancaria:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Si se cuenta con cuenta bancaria,<br />introduzca") + " el número de cuenta bancaria en el campo.<br />&lt;br /&gt;ej: &lt;b&gt;1100000700&lt;/b&gt;&lt;br /&gt;&lt;br /&gt;&lt;b&gt;Nota:&lt;/b&gt; Este campo es requerido para la exportación a los programas&lt;br /&gt;contables como &lt;i&gt;ContPaq, ContPaq I, Aspel COI.&lt;/i&gt; "%>'>
                        <asp:TextBox ID="txtBankAccount" runat="server" MaxLength="20" Width="150px" /></span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            ControlToValidate="txtBankAccount" runat="server" ErrorMessage="El valor debe ser numerico"
                            ValidationExpression="^\d+$" /></div>
                </td>
            </tr>
        </table>
    </li>
    <li class="clear">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblIvaAccount" runat="server" Text="Cuenta de iva:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Si se cuenta con una cuenta para el IVA,<br />introdusca") + " el número de cuenta para el iva.<br />&lt;br /&gt;ej: &lt;b&gt;1100000700&lt;/b&gt;&lt;br /&gt;&lt;br /&gt;&lt;b&gt;Nota:&lt;/b&gt; Este campo es requerido para la exportación a los programas&lt;br /&gt;contables como &lt;i&gt;ContPaq, ContPaq I, Aspel COI.&lt;/i&gt; "%>'>
                        <asp:TextBox ID="txtIvaAccount" runat="server" MaxLength="20" Width="150px" /></span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                            ControlToValidate="txtIvaAccount" ErrorMessage="El valor debe ser numerico" ValidationExpression="^\d+$" /></div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left" id="liTaxSystem" runat="server" style="height: auto;">
        <div>
            <label class="desc">
                <asp:Label ID="lblTaxSystem" runat="server" Text="Régimen Fiscal" /></label>
            <div class="validator-msg" runat="server" visible="false" id="divErrorTaxSystem"
                style="color: #ff1818; background-color: #feeaea;">
                Régimen fiscal es necesario.
            </div>
            <table>
                <tr>
                    <td style="vertical-align: top;">
                        <span class="vtip" title="Nombre del régimen fiscal en el que se tributa.">
                            <asp:DropDownList ID="ddlTaxSystem" runat="server">
                            </asp:DropDownList>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gdvTaxSystem" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="FiscalRegimeID" Visible="false" />
                                <asp:BoundField DataField="Regime" HeaderText="Nombre del régimen fiscal" />
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <span class="vtip" title="Eliminar todos los régimenes fiscales seleccionados." style="cursor: hand;">
                                            <asp:ImageButton ID="imgbtnDeleteAll" runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                                CausesValidation="false" OnClick="imgbtnDeleteAll_Click" Width="20px" Height="20px" />
                                        </span>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title="<img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar'/> Eliminar régimen fiscal.">
                                            <asp:ImageButton ID="imgbtnDelete" runat="server" CommandArgument='<%#Eval("FiscalRegimeID") %>'
                                                CausesValidation="false" ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="imgbtnDelete_Click" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div hidden>
            <label class="desc">
                Lugar de Expedición</label>
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="txtPlaceDispatch" runat="server" />
                        <div class="validator-msg" runat="server" visible="false" id="divPlaceDispatchError"
                            style="color: #ff1818; background-color: #feeaea;">
                            Lugar de expedición requerido.
                        </div>
                    </td>
                    <td style="vertical-align: top;">
                        <span class="vtip" title="Agregar lugar de expedicion<br />a la empresa.">
                            <asp:Button ID="btnAddPlaceDispatch" runat="server" Text="Agregar" CausesValidation="false"
                                OnClick="btnAddPlaceDispatch_Click" />
                        </span>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gdvPlaceDispatch" runat="server" AutoGenerateColumns="false" OnRowDataBound="gdvPlaceDispatch_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="PlaceDispatchID" Visible="false" />
                    <asp:BoundField DataField="PlaceDispatch" HeaderText="Lugar de expedición" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <span class="vtip" title="Indica el lugar de<br/>expedición activo." style="color: White;">
                                Activo</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title="<img src='../Includes/Images/png/editdelete.png' alt='Eliminar'/> Eliminar lugar de expedición.">
                                <asp:ImageButton ID="imgbtnActivePlace" runat="server" CommandArgument='<%#Eval("PlaceDispatchID") %>'
                                    CausesValidation="false" OnClick="imgbtnActivePlace_Click" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <span class="vtip" title="Eliminar todos los lugares de expedición seleccionados."
                                style="cursor: hand;">
                                <asp:ImageButton ID="imgbtnDeleteAllPlaces" runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                    CausesValidation="false" Width="20px" Height="20px" OnClick="imgbtnDeleteAllPlaces_Click" />
                            </span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title="<img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar'/> Eliminar lugar de expedición.">
                                <asp:ImageButton ID="imgbtnDeletePlace" runat="server" CommandArgument='<%#Eval("PlaceDispatchID") %>'
                                    CausesValidation="false" ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="imgbtnDeletePlace_Click" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </li>
</ul>
