<%@ Page Title="Listado de Empresas" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillersList.aspx.cs" Inherits="PACFDManager.Billers.BillersList" %>

<%@ Register Src="BillersSearch.ascx" TagName="BillersSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc1:BillersSearch ID="BillersSearch1" runat="server" OnSearch="BillersSearch1_Search" />
            </div>
            <br />
            <br />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblBillersListTitle" runat="server" Text="Listado de Empresas" />
                    </h2>
                </div>
                <asp:Button ID="btnAdd" runat="server" Text="Agregar Empresa" CssClass="Button" OnClick="btnAdd_Click" />
                <asp:GridView ID="gdvBillers" runat="server" CssClass="DataGrid" AutoGenerateColumns="False"
                    EmptyDataText="No hay emisores disponibles" OnRowDataBound="gdvBillers_RowDataBound">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="BillerID" />
                        <asp:BoundField HeaderText="Nombre" DataField="Name" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="RFC" DataField="RFC" />
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipCountry" runat="server" ToolTip="Pa�s de origen de la empresa." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Country")%>
                                <br />
                                <%#Eval("State")%>,
                                <%#Eval("Location")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo de Moneda">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lblCurrencyCodeGrid" runat="server" Text='<%#Eval("CurrencyCode")%>'></asp:Label>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo de Factura">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lblElectronicBillingTypeGrid" runat="server" Text='<%#Eval("ElectronicBillingType")%>'></asp:Label>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Detalles">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Detalles' /> para ver<br />los detalles de la empresa seleccionada." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Ver los detalles de la empresa<br /><b><%#Eval("Name")%></b>'>
                                    <asp:ImageButton ID="imbDetails" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                        OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/kfind.png" AlternateText="Detalles"
                                        Visible='<%#Eval("Active").ToString() == "True" ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>' />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la empresa seleccionada." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Modificar la empresa<br /><b><%#Eval("Name")%></b>'>
                                    <asp:ImageButton ID="imbEdit" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                        OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editIcon.png" AlternateText="Editar"
                                        Visible='<%#Eval("Active").ToString() == "True" ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>' /></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Borrar">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editdelete.png' alt='Desactivar' /> para<br />desactivar la empresa seleccionada.<br /><br />Hacer click en la imagen <img src='../Includes/Images/png/apply.png' alt='activar' /> para<br />reactivar la empresa seleccionada." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Desactivar la empresa<br /><b><%#Eval("Name")%></b>'>
                                    <asp:ImageButton ID="imbDelete" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                        OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editdelete.png" AlternateText="Eliminar"
                                        Visible='<%#Eval("Active").ToString() == "True" ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>' />
                                </span>
                                <div id="divEditCompanyReactivate" runat="server" style="text-align: center;" visible='<%#Eval("Active").ToString() == "False" & this.OnDataBindIsSupervisor() ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>'>
                                    <span class="vtip" title='Reactivar la empresa<br /><b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imbReactivate" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                            OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/apply.png" AlternateText="Reactivar empresa" />
                                    </span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                </asp:GridView>
                <br />
                <div style="text-align: right">
                    <span>
                        <asp:Label ID="lblTitleNumberBillers" runat="server" Font-Bold="True" Text="N�mero de Registros:" />
                    </span><span>
                        <asp:Label ID="lblNumberBillers" runat="server" Text="" />
                    </span>
                </div>
                <asp:Button ID="btnAdd2" runat="server" CssClass="Button" Text="Agregar Empresa"
                    OnClick="btnAdd_Click" />
            </div>
            <div>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hflCommand" runat="server" />
</asp:Content>
