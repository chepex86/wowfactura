﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    public partial class IssuedEditor : System.Web.UI.UserControl
    {

        public IssuedEditorInfo IssuedInformation { get; private set; }
        public int BillerID
        {
            get
            {
                object o;
                int r;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        public bool EnabledValidator
        {
            get { return this.rfvAddress.Enabled; }
            set
            {
                this.rfvAddress.Enabled =
                    //this.rfvExternalNumber.Enabled =
                    //this.rfvInternalNumber.Enabled =
                    this.rfvZipcode.Enabled =
                    value;
            }
        }
        public bool Enabled
        {
            get { return this.txtAddress.Enabled; }
            set
            {
                this.txtAddress.Enabled =
                    this.txtColony.Enabled =
                    this.txtExternalNumber.Enabled =
                    this.txtInternalNumber.Enabled =
                    this.txtReference.Enabled =
                    this.txtZipcode.Enabled =
                    this.CountrySelector1.Enabled =
                    this.EditorMode == IssuedEditorMode.View ? false : value;
            }
        }
        public IssuedEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return IssuedEditorMode.Add;

                return o.GetType() == typeof(IssuedEditorMode) ? (IssuedEditorMode)o : IssuedEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;

                if (value == IssuedEditorMode.View)
                    this.Enabled = false;
            }
        }
        public Boolean EnabledValidators
        {
            get { return this.rfvAddress.Enabled; }
            set
            {
                this.rfvAddress.Enabled =
                    //this.rfvExternalNumber.Enabled =
                    //this.rfvInternalNumber.Enabled =
                    this.rfvZipcode.Enabled =
                    !this.Enabled ? false : value;
            }
        }


        public IssuedEditor()
        {
            this.IssuedInformation = new IssuedEditorInfo(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.CountrySelector1.ColumnWidth = 100;
        }
        protected PACFD.DataAccess.BillersDataSet.IssuedDataTable OnGenerateDataTable()
        {
            PACFD.DataAccess.BillersDataSet.IssuedDataTable table;
            PACFD.DataAccess.BillersDataSet.IssuedRow row;

            table = new PACFD.DataAccess.BillersDataSet.IssuedDataTable();
            row = table.NewIssuedRow();
            row.BeginEdit();
            row.Address = this.IssuedInformation.Address;
            row.BillerID = this.EditorMode == IssuedEditorMode.Edit ? this.BillerID : 0;
            row.Colony = this.IssuedInformation.Colony;
            //row.Country = this.IssuedInformation.Country.Value;
            row.ExternalNumber = this.IssuedInformation.ExternalNumber;
            row.InternalNumber = this.IssuedInformation.InternalNumber;
            //row.Location = this.IssuedInformation.Location.Value;
            row.Municipality = "";
            row.Reference = this.IssuedInformation.Reference;
            //row.State = this.IssuedInformation.State.Value;
            row.Zipcode = this.IssuedInformation.Zipcode;

            table.AddIssuedRow(row);
            row.AcceptChanges();

            if (this.EditorMode == IssuedEditorMode.Edit)
                row.SetModified();
            else
                row.SetAdded();

            return table;
        }
        /// <summary>
        /// Data bind a biller information to the control from the billerID.
        /// </summary> 
        public void DataBlindBillerID()
        {
            PACFD.DataAccess.BillersDataSet.IssuedDataTable table;
            PACFD.Rules.Billers billers;

            if (this.BillerID < 1)
                return;

            billers = new PACFD.Rules.Billers();
            table = billers.IssuedTable.SelectByID(this.BillerID);

            if (table.Count < 1)
                return;

            this.IssuedInformation.Address = table[0].Address;
            this.IssuedInformation.Colony = table[0].Colony;

            //this.CountrySelector1.SetCountry(table[0].Country, table[0].State, table[0].Location);

            this.IssuedInformation.ExternalNumber = table[0].ExternalNumber;
            this.IssuedInformation.InternalNumber = table[0].InternalNumber;
            this.IssuedInformation.Reference = table[0].Reference;
            this.IssuedInformation.Zipcode = table[0].Zipcode;
        }
    }
}