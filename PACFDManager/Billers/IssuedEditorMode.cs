﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    public enum IssuedEditorMode
    {
        Add,
        Edit,
        View
    }
}
