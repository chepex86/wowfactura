﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    /// <summary>
    /// Mode editor of the PACFDManager.Billers.BillersEditor control.
    /// </summary>
    public enum BillersEditorMode
    {
        Add = 0,
        Edit = 1,
        View = 2,
    }
}
