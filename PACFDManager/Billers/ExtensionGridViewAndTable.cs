﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    /// <summary>
    /// This class extension only work on Billers namespace 
    /// and with add an modify pages.
    /// </summary>
    internal static class ExtensionGridViewAndTable
    {
        /// <summary>
        /// This method only work for Billers namespace. Get a integer value with the 
        /// row index from an taxtypeid.
        /// </summary>
        /// <param name="id">Id to look for.</param>
        /// <returns>If success return the index value else -1.</returns>
        internal static int GetRowIndexByTaxTypeID(this GridViewRowCollection rows, int id)
        {
            HiddenField h;

            foreach (GridViewRow row in rows)
            {
                h = row.FindControl("hdfID") as HiddenField;

                if (h.IsNull())
                    continue;

                if (id.ToString() == h.Value)
                    return row.RowIndex;

                h = null;
            }

            return -1;
        }
        /// <summary>
        /// Set the index of a row in a table.
        /// </summary>
        /// <param name="index">Index of the row in the table.</param>
        /// <param name="toup">If true up item in the index else down item index.</param>
        internal static void RowLevelIndex(this PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table, int index, bool toup)
        {
            object[] array;
            PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllRow row;

            if (index < 0 || index > table.Rows.Count - 1 || (toup ? index - 1 < 0 : false))
                return;

            row = table.Rows[index] as PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllRow;
            array = row.ItemArray;
            table.Rows.RemoveAt(index);

            row = table.NewTaxTypesGetAllRow();
            row.ItemArray = array;

            table.Rows.InsertAt(row, index + (toup ? -1 : 1));
            row.AcceptChanges();
        }
        //internal static void RowLevelIndex(this PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDDataTable table, int index, bool toup)
        //{
        //    object[] array;
        //    PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDRow row;

        //    if (index < 0 || index > table.Rows.Count - 1 || (toup ? index - 1 < 0 : false))
        //        return;

        //    row = table.Rows[index] as PACFD.DataAccess.BillersDataSet.BillerTaxes_GetByBillerIDRow;
        //    array = row.ItemArray;
        //    table.Rows.RemoveAt(index);

        //    row = table.NewBillerTaxes_GetByBillerIDRow();
        //    row.ItemArray = array;

        //    table.Rows.InsertAt(row, index + (toup ? -1 : 1));
        //    row.AcceptChanges();
        //}
    }
}
