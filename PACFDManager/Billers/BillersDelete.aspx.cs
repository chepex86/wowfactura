﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;

namespace PACFDManager.Billers
{
    public partial class BillersDelete : PACFDManager.BasePage
    {
        /// <summary>
        /// Get an int value with the id to edit from the session.
        /// </summary>
        private int BillerID
        {
            get
            {
                object o;
                int r;

                o = this.Session["id"];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            this.BillersEditor1.Enabled = false;

            if (this.IsPostBack)
                return;

            this.BillersEditor1.BillerID = this.BillerID;
            this.BillersEditor1.DataBindBillerID();
            this.BillersEditor1.EditorMode = BillersEditorMode.View;

            if (this.BillerID < 1)
                this.btnAcept.Enabled = false;
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.WebMessageBox1.ShowMessage("¿Esta seguro que quiere borrar la empresa?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillersList).Name + ".aspx");
        }
    }
}
