﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillersOthersConfigurations.aspx.cs" Inherits="PACFDManager.Billers.BillersOthersConfigurations" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <!-- title form -->
                <div class="info">
                    <h2>
                        <asp:Label ID="lblEmailConfigurationTitle" runat="server" Text="Otras Configuraciones"></asp:Label>
                    </h2>
                </div>
                <ul>
                    <li>
                        <label class="desc">
                            <asp:CheckBox ID="chkRetainMonthlyFolio" runat="server" Text="Apartar folios mensualmente"
                                AutoPostBack="True" />
                        </label>
                    </li>
                    <li >
                        <label class="desc">
                            <asp:Label ID="lblRetainMonthlyFolioFrom" runat="server" Text="Apartir de"></asp:Label>
                        </label>                        
                        <span class="vtip" title="Se aqui se debe seleccionar apartir de que mes se requiere que se aparten los folios">
                           <asp:DropDownList ID=ddlYears runat=server></asp:DropDownList>
                           <asp:DropDownList ID=ddlMonth runat=server></asp:DropDownList>
                        </span></li>
                    <li >
                        <label class="desc">
                            <asp:Label ID="lblRetainMonthlyFolioNumber" runat="server" Text="Cantidad de folios"></asp:Label>
                        </label>                        
                        <span class="vtip" title="Cantidad de folios que se apartaran al iniciar el mes">
                            <asp:TextBox ID="txtRetainMonthlyFolioNumber" runat="server" AutoCompleteType="Disabled" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvRetainMonthlyFolioNumber" runat="server" ControlToValidate="txtRetainMonthlyFolioNumber"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li class="buttons">
                        <div>
                            <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click"
                                ValidationGroup="Validators" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                OnClick="btnCancel_Click" /></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" />
</asp:Content>
