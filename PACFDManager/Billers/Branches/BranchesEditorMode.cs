﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers.Branches
{
    public enum BranchesEditorMode
    {
        Add = 0,
        Edit = 1,
        View = 2,
    }
}
