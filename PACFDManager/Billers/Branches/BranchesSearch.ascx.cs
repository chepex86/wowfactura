﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers.Branches
{
    public partial class BranchesSearch : PACFDManager.BaseUserControl
    {
        public event BranchesSearchEventHandler Search;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.InvokeSearch();
        }

        public virtual void InvokeSearch()
        {
            bool? active;
            int i;
            
            active = this.ddlActive.SelectedIndex < 1 ? null : !int.TryParse(this.ddlActive.SelectedItem.Value, out i) ? null : 
                i < 0 || i > 1 ? null : i == 0 ? (bool?)false : (bool?)true;

            this.OnSearch(new BranchesSearchEventArgs(this.txtName.Text, this.txtCode.Text, active));
        }

        protected virtual void OnSearch(BranchesSearchEventArgs e)
        {
            if (!this.Search.IsNull())
            { this.Search(this, e); }
        }
    }

    public delegate void BranchesSearchEventHandler(object sender, BranchesSearchEventArgs e);

    public class BranchesSearchEventArgs : EventArgs
    {
        public string Name { get; private set; }
        public string Code { get; private set; }
        public bool? Active { get; set; }

        public BranchesSearchEventArgs(string name, string code, bool? active)
        {
            this.Name = name;
            this.Code = code.Length > 19 ? code.Substring(0, 19) : code;
            this.Active = active;
        }
    }
}