﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BranchesModify.aspx.cs" Inherits="PACFDManager.Billers.Branches.BranchesModify" %>

<%@ Register Src="BranchesEditor.ascx" TagName="BranchesEditor" TagPrefix="uc1" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="divContent" runat="server">
                <div id="container" class="wframe70">
                    <div class="form">
                        <div class="formStyles">
                            <uc1:BranchesEditor ID="BranchesEditor1" runat="server" onApplyChanges="BranchesEditor1_ApplyChanges" />
                            <ul>
                                <li class="buttons">
                                    <div>
                                        <asp:Button ID="btnAcept" CssClass="Button" runat="server" Text="Aceptar" OnClick="btnAcept_Click" />
                                        <asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                            Text="Cancelar" OnClick="btnCancel_Click" />
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
