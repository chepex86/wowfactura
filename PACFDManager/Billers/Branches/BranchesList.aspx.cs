﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers.Branches
{
    public partial class BranchesList : PACFDManager.BasePage
    {
        private int BranchTemporalID { set { this.Session["TemporalBrenchID"] = value; } }
        private PACFD.DataAccess.BranchesDataSet.SearchDataTable BranchSearcTable
        {
            get
            {
                object o = this.ViewState[string.Format("{0}_BranchSearcTable", this.UniqueID)];

                return o == null || o.GetType() != typeof(PACFD.DataAccess.BranchesDataSet.SearchDataTable) ? null :
                    o as PACFD.DataAccess.BranchesDataSet.SearchDataTable;
            }
            set { this.ViewState[string.Format("{0}_BranchSearcTable", this.UniqueID)] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            { return; }

            this.BranchesSearch1.InvokeSearch();
        }
        protected void BranchesSearch1_Search(object sender, BranchesSearchEventArgs e)
        {
            PACFD.DataAccess.BranchesDataSet.SearchDataTable table = (new PACFD.Rules.Branches().Search(
                null, e.Code, this.CurrentBillerID, null, null, null, null, null, null, null, null, null, null, e.Name, e.Active));

            this.BranchSearcTable = table;
            this.DataBind();
        }
        public override void DataBind()
        {
            base.DataBind();
            this.gdvBranch.DataSource = this.BranchSearcTable;
            this.gdvBranch.DataBind();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(Branches.BranchesAdd).Name + ".aspx");
        }
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int i;
            ImageButton b;
            PACFD.DataAccess.BranchesDataSet.SearchRow[] rows;

            if ((b = sender as ImageButton) == null)
            { return; }

            if (!int.TryParse(b.CommandArgument, out i) || i < 1)
            { return; }

            switch (b.ID)
            {
                case "imbDelete":
                    (new PACFD.Rules.Branches()).SetActive(i, false);
                    rows = this.BranchSearcTable.Select(string.Format("BranchID={0}", i)) as PACFD.DataAccess.BranchesDataSet.SearchRow[];

                    if (rows.Length > 0)
                    {
                        rows[0].BeginEdit();
                        rows[0].Active = false;
                        rows[0].EndEdit();
                    }

                    this.DataBind();
                    break;
                case "imbReactivate":
                    (new PACFD.Rules.Branches()).SetActive(i, true);

                    rows = this.BranchSearcTable.Select(string.Format("BranchID={0}", i)) as PACFD.DataAccess.BranchesDataSet.SearchRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].BeginEdit();
                        rows[0].Active = true;
                        rows[0].EndEdit();
                    }

                    this.DataBind();
                    break;
                case "imbEdit":
                    this.BranchTemporalID = i;
                    this.Response.Redirect(typeof(Branches.BranchesModify).Name + ".aspx");
                    break;
            }
        }
    }
}
