﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers.Branches
{
    public partial class BranchesModify : PACFDManager.BasePage
    {
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";
        const string MESSAGE_ERROR = "MESSAGE_ERROR";

        private int BranchTemporalID
        {
            get
            {
                int i;
                return this.Session["TemporalBrenchID"] == null || !int.TryParse(this.Session["TemporalBrenchID"].ToString(), out i) ? 0 : i;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            { return; }

            this.BranchesEditor1.BranchID = this.BranchTemporalID;
            this.BranchesEditor1.DataBind();
        }

        protected void BranchesEditor1_ApplyChanges(object sender, BranchesEditorApplyChangesArgs e)
        {
            PACFD.DataAccess.BranchesDataSet.BranchRow row;

            using (PACFD.DataAccess.BranchesDataSet.BranchDataTable table = new PACFD.DataAccess.BranchesDataSet.BranchDataTable())
            {
                row = table.NewBranchRow();
                row.Address = e.Address;
                row.BillerID = e.BillerID;
                row.BranchID = this.BranchTemporalID;
                row.Code = e.Code;
                row.Colony = e.Colony;
                row.Country = e.Country;
                row.ExternalNumber = e.ExternalNumber;
                row.InternalNumber = e.InternalNumber;
                row.Location = e.Location;
                row.Municipality = e.Municipality;
                row.Reference = e.Reference;
                row.State = e.State;
                row.Zipcode = e.Zipcode;
                row.Active = e.Active;
                row.Name = e.Name;

                table.AddBranchRow(row);
                row.AcceptChanges();
                row.SetModified();

                if ((new PACFD.Rules.Branches()).Update(table))
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
                    this.WebMessageBox1.ShowMessage("Exito al modificar sucursal.");
                    return;
                }
            }

            this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
            this.WebMessageBox1.ShowMessage("Error al modificar sucursal.");
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ERROR:
                    break;
                case MESSAGE_SUCCESS:
                default:
                    this.btnCancel_Click(sender, e);
                    break;
            }
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.BranchesEditor1.InvokeChanges();
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(BranchesList).Name + ".aspx"); }
    }
}
