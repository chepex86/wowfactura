﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;

namespace PACFDManager.Billers.Branches
{
    public partial class BranchesEditor : PACFDManager.BaseUserControl
    {
        public event BranchesEditorApplyChangesEventHandler ApplyChanges;

        public Branches.BranchesEditorMode EditorMode { get; set; }
        public int BranchID
        {
            get
            {
                int i;
                return this.ViewState[string.Format("{0}_BranchesEditor", this.UniqueID)] == null || !int.TryParse(this.ViewState[string.Format("{0}_BranchesEditor", this.UniqueID)].ToString(), out i) ? 0 : i;
            }
            set { this.ViewState[string.Format("{0}_BranchesEditor", this.UniqueID)] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public String ToolTipMode() { return this.EditorMode == Branches.BranchesEditorMode.View ? "True" : "False"; }
        protected virtual void OnApplyChanges(BranchesEditorApplyChangesArgs e)
        {
            if (!this.ApplyChanges.IsNull())
            { this.ApplyChanges(this, e); }
        }
        public void InvokeChanges()
        {
            BranchesEditorApplyChangesArgs a = new BranchesEditorApplyChangesArgs(this.txtCode.Text, this.CurrentBillerID, this.txtAddress.Text,
                this.txtExternalNumber.Text, this.txtInternalNumber.Text, this.txtColony.Text, this.txtCity.Text, this.txtReference.Text,
                 this.txtMunicipality.Text, this.CountrySelector1.GetStateName, this.CountrySelector1.GetCountryName, this.txtZipcode.Text
                 , true, this.txtName.Text);

            this.OnApplyChanges(a);
        }

        public override void DataBind()
        {
            BranchesDataSet.BranchDataTable table;

            base.DataBind();

            if (this.BranchID < 1)
            { return; }

            using (table = (new PACFD.Rules.Branches()).SelectByID(this.BranchID))
            {
                this.txtAddress.Text = table[0].Address;
                this.txtCity.Text = table[0].Location;  
                
                this.txtColony.Text = table[0].Colony;
                this.txtExternalNumber.Text = table[0].ExternalNumber;
                this.txtInternalNumber.Text = table[0].InternalNumber;
                this.txtMunicipality.Text = table[0].Municipality;
                this.txtReference.Text = table[0].Reference;
                this.txtZipcode.Text = table[0].Zipcode;
                this.CountrySelector1.SelectState = table[0].State;
                this.CountrySelector1.SelectCountry = table[0].Country;
                
                this.txtName.Text = table[0].Name;
                this.txtCode.Text = table[0].Code;
            }
        }
    }

    public delegate void BranchesEditorApplyChangesEventHandler(object sender, BranchesEditorApplyChangesArgs e);

    public class BranchesEditorApplyChangesArgs : EventArgs
    {
        public string Code { get; private set; }
        public int BillerID { get; private set; }
        public string Address { get; private set; }
        public string ExternalNumber { get; private set; }
        public string InternalNumber { get; private set; }
        public string Colony { get; private set; }
        public string Location { get; private set; }
        public string Reference { get; private set; }
        public string Municipality { get; private set; }
        public string State { get; private set; }
        public string Country { get; private set; }
        public string Zipcode { get; private set; }
        public bool Active { get; private set; }
        public string Name { get; private set; }

        public BranchesEditorApplyChangesArgs(string code, int billerid, string address, string externalnumber, string internalnumber, string colony
            , string location, string reference, string municipality, string state, string country, string zipcode, Boolean active, string name)
        {
            this.Name = name;
            this.Active = active;
            this.Code = code;
            this.BillerID = billerid;
            this.Address = address;
            this.ExternalNumber = externalnumber;
            this.InternalNumber = internalnumber;
            this.Colony = colony;
            this.Location = location;
            this.Reference = reference;
            this.Municipality = municipality;
            this.State = state;
            this.Country = country;
            this.Zipcode = zipcode;
        }
    }
}