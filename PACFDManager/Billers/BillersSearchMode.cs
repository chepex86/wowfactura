﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    /// <summary>
    /// 
    /// </summary>
    public enum SearchMode
    {
        None = 0,
        RFC = 1,
        Name = 2,
        Both = 4,
    }
}
