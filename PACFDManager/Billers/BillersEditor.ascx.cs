﻿using PACFDManager.Billings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    /// <summary>
    /// Class used to edit a Biller
    /// </summary>
    public partial class BillersEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Event handler of the applychanges.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnApplyChangesHandler(object sender, BillersEditorEventArgs e);
        /// <summary>
        /// Event fired when the control has changed the content
        /// </summary>
        public event OnApplyChangesHandler ApplyModify;

        /// <summary>
        /// Get the fiscal regimen list available.
        /// </summary>
        public static List<string> FiscalRegimenDefinitions
        {
            get
            {
                return new List<string>()
                {
                    "Régimen de Consolidación Fiscal",  
                    "Régimen Simplificado",
                    "Régimen General de Ley Personas Morales",
                    "Régimen de las Personas Morales con Fines no Lucrativos",
                    "Régimen Intermedio de las Personas Físicas con Actividades Empresariales",
                    "Régimen de Pequeños Contribuyentes",
                    "Régimen Fiscal Preferente",
                    "Régimen de Arrendamiento"
                };
            }
        }
        /// <summary>
        /// Get the Fiscal Regimen Table with the selected rows.
        /// </summary>
        public PACFD.DataAccess.BillersDataSet.FiscalRegimeDataTable FiscalRegimenTable
        {
            get
            {
                PACFD.DataAccess.BillersDataSet.FiscalRegimeDataTable table =
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]]
                    as PACFD.DataAccess.BillersDataSet.FiscalRegimeDataTable;

                if (table.IsNull())
                {
                    table = new PACFD.DataAccess.BillersDataSet.FiscalRegimeDataTable();
                    this.FiscalRegimenTable = table;
                }

                return table;
            }
            private set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        public PACFD.DataAccess.BillersDataSet.PlaceDispatchDataTable PlaceDispatchTable
        {
            get
            {
                PACFD.DataAccess.BillersDataSet.PlaceDispatchDataTable table =
                  this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]]
                      as PACFD.DataAccess.BillersDataSet.PlaceDispatchDataTable;

                if (table.IsNull())
                {
                    table = new PACFD.DataAccess.BillersDataSet.PlaceDispatchDataTable();
                    this.PlaceDispatchTable = table;
                }

                return table;
            }
            private set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get a BillersInfo with all main biller information.
        /// </summary>
        public BillersEditorInfo BillerInformation { get; private set; }
        /// <summary>
        /// Get or Set the BillersEditorMode editor mode.
        /// </summary>
        public BillersEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return BillersEditorMode.Add;

                return o.GetType() == typeof(BillersEditorMode) ? (BillersEditorMode)o : BillersEditorMode.Add;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or Set an int value with the ID of the Biller to edit.
        /// </summary>
        public int BillerID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or Set an integer value of the ClientID owner of the biller.
        /// </summary>
        public int GroupID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Enabled
        {
            get { return this.txtAddress.Enabled; }
            set
            {
                this.txtColony.ReadOnly =
                this.txtExternalNumber.ReadOnly =
                this.txtInternalNumber.ReadOnly =
                this.txtName.ReadOnly =
                this.txtPhone.ReadOnly =
                this.txtReference.ReadOnly =
                this.txtRFC.ReadOnly =
                this.txtZipcode.ReadOnly =
                this.txtBankAccount.ReadOnly =
                this.txtIvaAccount.ReadOnly =
                this.txtAddress.ReadOnly =
                this.txtCity.ReadOnly =
                this.txtEmail.ReadOnly =
                this.txtMunicipality.ReadOnly =
                this.EditorMode == BillersEditorMode.View ? true : false;
                this.AsyncFileUpload1.Enabled =
                this.CountrySelector1.Enabled =
                this.EditorMode == BillersEditorMode.View ? false : value;
                this.ddlBillType.Enabled = value;
                this.ddlCurrency.Enabled = false;

                //this.ddlBillType.Visible = this.EditorMode == BillersEditorMode.Add ? true : false;
                //this.lblBillTypeShow.Visible = this.EditorMode == BillersEditorMode.Add ? false : true;

                if (this.EditorMode == BillersEditorMode.Edit)
                {
                    if (this.ddlBillType.SelectedValue == "2")
                        this.ddlCurrency.Enabled = true;

                    this.ddlBillType.Enabled = true;
                }
            }
        }
        public bool selected { get; set; }
        /// <summary>
        /// Get the tool tip mode.
        /// </summary>
        /// <returns></returns>
        public String ToolTipMode()
        {
            return this.EditorMode == BillersEditorMode.View ? "True" : "False";
        }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public BillersEditor()
        {
            this.BillerInformation = this.OnGenerateBillersEditorInfo();
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            int i = 1;

            
            this.CountrySelector1.ToolTipCountry = (ToolTipMode() == "True" ? "Muestra" : "Selecciona") + " el nombre del País en donde<br />se encuentra establecida la empresa.";
            this.CountrySelector1.ToolTipState = (ToolTipMode() == "True" ? "Muestra" : "Selecciona") + " el nombre del Estado en donde<br />se encuentra establecida la empresa.";

            if (this.IsPostBack)
                return;

            if (!selected)
            {
                this.ddlTaxSystem.Items.Clear();
                this.ddlTaxSystem.DataTextField = "Descripcion";
                this.ddlTaxSystem.DataValueField = "c_RegimenFiscal";
                this.ddlTaxSystem.DataSource = Helpers.GetTaxSystem();
                this.ddlTaxSystem.DataBind();
                selected = true;
            }
            //this.PlaceDispatchTable = null;

            Dictionary<PACFD.Rules.ElectronicBillingType, string> dick = PACFD.Rules.ElectronicBillingType.CBB.ToDictionary();

            if (this.EditorMode == BillersEditorMode.Edit)
            {
                switch (this.BillerInformation.ElectronicType)
                {
                    case PACFD.Rules.ElectronicBillingType.CFD:
                        if (CFDEnable)
                            this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFD], ((int)PACFD.Rules.ElectronicBillingType.CFD).ToString()));
                        if (CFD2_2Enable)
                            this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFD2_2], ((int)PACFD.Rules.ElectronicBillingType.CFD2_2).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.CFDI:
                        if (CFDIEnable)
                            this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI], ((int)PACFD.Rules.ElectronicBillingType.CFDI).ToString()));
                        if (CFDI3_3Enable)
                            this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI3_3], ((int)PACFD.Rules.ElectronicBillingType.CFDI3_3).ToString()));
                        if (CFDI4_0Enable)
                            this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI4_0], ((int)PACFD.Rules.ElectronicBillingType.CFDI4_0).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.CFD2_2:
                        this.ddlBillType.Items.Add(new ListItem(dick[this.BillerInformation.ElectronicType], ((int)this.BillerInformation.ElectronicType).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                        this.ddlBillType.Items.Add(new ListItem(dick[this.BillerInformation.ElectronicType], ((int)this.BillerInformation.ElectronicType).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                        this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI4_0], ((int)PACFD.Rules.ElectronicBillingType.CFDI4_0).ToString()));
                        this.ddlBillType.Items.Add(new ListItem(dick[this.BillerInformation.ElectronicType], ((int)this.BillerInformation.ElectronicType).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.CFDI4_0:
                        this.ddlBillType.Items.Add(new ListItem(dick[this.BillerInformation.ElectronicType], ((int)this.BillerInformation.ElectronicType).ToString()));
                        break;
                    case PACFD.Rules.ElectronicBillingType.Indeterminate:
                    case PACFD.Rules.ElectronicBillingType.CBB:
                    default:
                        this.ddlBillType.Items.Add(new ListItem(dick[this.BillerInformation.ElectronicType], ((int)this.BillerInformation.ElectronicType).ToString()));
                    break;
                }
            }
            else if (this.EditorMode == BillersEditorMode.Add)
            {
                if (CBBEnable)
                    this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CBB], ((int)PACFD.Rules.ElectronicBillingType.CBB).ToString()));
                if (CFD2_2Enable)
                    this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFD2_2], ((int)PACFD.Rules.ElectronicBillingType.CFD2_2).ToString()));
                if (CFDI3_2Enable)
                    this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI3_2], ((int)PACFD.Rules.ElectronicBillingType.CFDI3_2).ToString()));
                if (CFDI3_3Enable)
                    this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI3_3], ((int)PACFD.Rules.ElectronicBillingType.CFDI3_3).ToString()));
                if (CFDI4_0Enable)
                    this.ddlBillType.Items.Add(new ListItem(dick[PACFD.Rules.ElectronicBillingType.CFDI4_0], ((int)PACFD.Rules.ElectronicBillingType.CFDI4_0).ToString()));
            }


            ListItem l = this.ddlBillType.Items.FindByText(dick[this.CurrentElectronicBillingType]);
            this.ddlBillType.ClearSelection();

            if (!l.IsNull())
                l.Selected = true;
                this.ddlTaxSystem.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlTaxSystem.Visible = true;
            this.ddlTaxSystem.Enabled = true;
            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0) 
                
            {
                this.rfvName.Visible = false;
                //this.rfvAddress.Visible = false;
            }

            ddlBillType_SelectedIndexChanged(null, EventArgs.Empty);

            if (this.EditorMode == BillersEditorMode.Add)
            {
                this.lblTitle.Text = "Agregar empresa";
                /*ScriptManager.GetCurrent(this.Page).Scripts.Add(new ScriptReference()
                {
                    Path = "~/Billers/BillersEditor.js"
                });*/
            }
            else if (this.EditorMode == BillersEditorMode.Edit)
                this.lblTitle.Text = "Editar empresa";
            else
            {
                this.lblTitle.Text = "Detalles de empresa";
                this.ddlTaxSystem.Enabled = false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual BillersEditorInfo OnGenerateBillersEditorInfo() { return new BillersEditorInfo(this); }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected BillersEditorEventArgs OnGenerateBillersEditorEventArgs()
        {
            PACFD.DataAccess.BillersDataSet.BillersRow row;
            PACFD.DataAccess.BillersDataSet billersdataset = new PACFD.DataAccess.BillersDataSet();

            row = billersdataset.Billers.NewBillersRow(); //table.NewBillersRow();
            row.BeginEdit();
            row.BillerID = this.EditorMode == BillersEditorMode.Edit ? this.BillerID : 0;
            row.GroupID = this.GroupID;
            row.RFC = this.BillerInformation.RFC;
            row.Name = this.BillerInformation.Name;
            row.Address = this.BillerInformation.Address;
            row.Phone = this.BillerInformation.Phone;
            row.ExternalNumber = this.BillerInformation.ExternalNumber;
            row.InternalNumber = this.BillerInformation.InternalNumber;
            row.Colony = this.BillerInformation.Colony;
            row.Reference = this.BillerInformation.Reference;
            row.State = this.CountrySelector1.GetStateName;//this.CountrySelector1.State.Value;
            row.Country = this.CountrySelector1.GetCountryName;//this.CountrySelector1.Country.Value;
            row.Municipality = this.txtMunicipality.Text.Trim();
            row.Location = this.txtCity.Text.Trim();//this.BillerInformation.Location.Value;
            row.Active = true;
            row.Zipcode = this.BillerInformation.Zipcode;
            row.LogoCFD = this.BillerInformation.Logo;
            row.ElectronicBillingType = this.BillerInformation.BillType;
            row.CurrencyCode = this.BillerInformation.Currency.Text;

            row.TaxSystem = (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2 ||
                this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3
                || this.BillerInformation.BillType == (int)PACFD.Rules.ElectronicBillingType.CFDI3_3
                || this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0
                || this.BillerInformation.BillType == (int)PACFD.Rules.ElectronicBillingType.CFDI4_0) &&
                (this.liTaxSystem.Visible ) ? this.ddlTaxSystem.SelectedItem.Value : null;

            row.IvaAccount = this.BillerInformation.IvaAccount;
            row.BankAccount = this.BillerInformation.BankAccount;
            row.TaxRatePercentage = 0;
            row.IssuedInDifferentPlace = false;
            row.Email = this.BillerInformation.Email;
            row.IsDictaminated = this.cbIsDictaminated.Checked;//this.BillerInformation.BillType == 0 ? this.cbIsDictaminated.Checked : false;

            if (!this.Session["image"].IsNull())
            {
                Byte[] b = this.Session["image"] as Byte[];

                try { row.LogoCFD = b; }
                catch { }
            }

            row.EndEdit();
            billersdataset.Billers.AddBillersRow(row);
            //table.AddBillersRow(row);
            row.AcceptChanges();

            if (this.EditorMode == BillersEditorMode.Edit)
                row.SetModified();
            else if (this.EditorMode == BillersEditorMode.Add)
                row.SetAdded();

            billersdataset.FiscalRegime.Merge(this.FiscalRegimenTable, true);
            billersdataset.PlaceDispatch.Merge(this.PlaceDispatchTable, true);

            foreach (var item in billersdataset.FiscalRegime)
            {
                switch (item.RowState)
                {
                    case System.Data.DataRowState.Added:
                    case System.Data.DataRowState.Modified:
                    case System.Data.DataRowState.Unchanged:
                        item.SetParentRow(billersdataset.Billers[0]);
                        break;
                    case System.Data.DataRowState.Deleted:
                        break;
                    case System.Data.DataRowState.Detached:
                        break;
                    default:
                        break;
                }
            }

            foreach (var item in billersdataset.PlaceDispatch)
            {
                switch (item.RowState)
                {
                    case System.Data.DataRowState.Added:
                    case System.Data.DataRowState.Modified:
                    case System.Data.DataRowState.Unchanged:
                        item.SetParentRow(billersdataset.Billers[0]);
                        break;
                    case System.Data.DataRowState.Deleted:
                        break;
                    case System.Data.DataRowState.Detached:
                        break;
                    default:
                        break;
                }
            }


            return new BillersEditorEventArgs(billersdataset, this.EditorMode);
        }
        /// <summary>
        /// Bind a biller to the control.
        /// </summary>
        public void DataBindBillerID()
        {
            this.ddlTaxSystem.Items.Clear();
            this.ddlTaxSystem.DataTextField = "Descripcion";
            this.ddlTaxSystem.DataValueField = "c_RegimenFiscal";
            this.ddlTaxSystem.DataSource = Helpers.GetTaxSystem();
            this.ddlTaxSystem.DataBind();
            selected = true;
            PACFD.Rules.Billers billers;
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;
            int i = 0;

            if (this.BillerID < 1)
                return;

            billers = new PACFD.Rules.Billers();
            table = billers.SelectByID(this.BillerID);

            if (table.Count < 1)
                return;

            //if ((PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD)
            //{
            //    this.lblIsDictaminated.Visible =
            //    this.cbIsDictaminated.Visible = true;
            //}
            //else
            //{
            //    this.lblIsDictaminated.Visible =
            //    this.cbIsDictaminated.Visible = false;
            //}
            //this.ViewState["edit"] = "";

            this.BillerInformation.Name = table[0].Name;
            this.BillerInformation.Address = table[0].Address;
            this.BillerInformation.RFC = table[0].RFC;
            this.BillerInformation.Phone = table[0].Phone;
            this.BillerInformation.ExternalNumber = table[0].ExternalNumber;
            this.BillerInformation.InternalNumber = table[0].InternalNumber;
            this.BillerInformation.Colony = table[0].Colony;
            this.BillerInformation.Reference = table[0].Reference;
            this.CountrySelector1.SelectCountry = table[0].Country;
            this.CountrySelector1.SelectState = table[0].State;
            this.txtMunicipality.Text = table[0].Municipality;
            this.txtCity.Text = table[0].Location;
            //this.CountrySelector1.SetCountry(table[0].Country, table[0].State, table[0].Location);
            this.BillerInformation.Zipcode = table[0].Zipcode;

            i = table[0].IsElectronicBillingTypeNull() ? 0 : table[0].ElectronicBillingType;

            this.BillerInformation.BillType = i;
            //this.ddlCurrency.SelectedValue = table[0].CurrencyCode;
            this.ddlCurrency.SelectedIndex = this.ddlCurrency.Items.IndexOf(this.ddlCurrency.Items.FindByText(table[0].CurrencyCode));
            this.BillerInformation.BankAccount = table[0].IsBankAccountNull() ? "0" : table[0].BankAccount;
            this.BillerInformation.IvaAccount = table[0].IsIvaAccountNull() ? "0" : table[0].IvaAccount;
            this.cbIsDictaminated.Checked = table[0].IsIsDictaminatedNull() ? false : table[0].IsDictaminated;
            this.BillerInformation.Email = table[0].IsEmailNull() ? "" : table[0].Email;
            this.BillerInformation.ElectronicType = (PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType;
            this.BillerInformation.BillerID = table[0].BillerID;

            if ((  (PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFD2_2
                || (PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2)
                || (PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3
                || (PACFD.Rules.ElectronicBillingType)table[0].ElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI4_0)
            {
                this.PlaceDispatchTable.Merge((new PACFD.Rules.PlaceDispatch()).SelectByBillerID(this.BillerInformation.BillerID), true);
                this.gdvPlaceDispatch.DataSource = this.PlaceDispatchTable;
                this.gdvPlaceDispatch.DataBind();
                //===================
                var s = this.ddlTaxSystem.Items.FindByValue(table[0].IsTaxSystemNull() ? "" : table[0].TaxSystem);
                if (s != null)
                    s.Selected = true;
                this.FiscalRegimenTable.Merge((new PACFD.Rules.Billers()).FiscalRegimeTable.GetByBillerID(this.BillerInformation.BillerID));
                this.gdvTaxSystem.DataSource = this.FiscalRegimenTable;
                this.gdvTaxSystem.DataBind();

                liTaxSystem.Visible = true;
            }

            if (this.EditorMode == BillersEditorMode.View)
                this.AsyncFileUpload1.Visible = false;
        }
        /// <summary>
        /// Selected billing type.
        /// </summary>
        protected void ddlBillType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = -1;

            if (!int.TryParse(this.ddlBillType.SelectedValue, out i))
                return;

            switch ((PACFD.Rules.ElectronicBillingType)i)
            {
                case PACFD.Rules.ElectronicBillingType.CFD:
                case PACFD.Rules.ElectronicBillingType.CFD2_2:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "reset", "reset();", true);
                    this.ddlCurrency.Enabled = false;
                    this.lblIsDictaminated.Visible = true;
                    this.cbIsDictaminated.Visible = true;
                    break;
                case PACFD.Rules.ElectronicBillingType.CFDI:
                case PACFD.Rules.ElectronicBillingType.CBB:
                case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                case PACFD.Rules.ElectronicBillingType.CFDI4_0:
                    this.ddlCurrency.Enabled = true;
                    this.lblIsDictaminated.Visible = true;
                    this.cbIsDictaminated.Visible = true;
                    this.rfvName.Visible = false;
                    //this.rfvAddress.Visible = false;
                    break;
                default:
                    this.rfvName.Visible = true;
                    // this.rfvAddress.Visible = true;
                    break;
            }

            //switch ((PACFD.Rules.ElectronicBillingType)i)
            //{
            //    case PACFD.Rules.ElectronicBillingType.CFD2_2:
            //    case PACFD.Rules.ElectronicBillingType.CFDI3_2:
            //    case PACFD.Rules.ElectronicBillingType.CFDI3_3:
            //    case PACFD.Rules.ElectronicBillingType.CFDI4_0:

            //        this.rfvName.Visible = false;
            //        //this.rfvAddress.Visible = false;
            //        break;
            //    default:
            //        this.rfvName.Visible = true;
            //        // this.rfvAddress.Visible = true;
            //        break;
            //}

            if (this.EditorMode == BillersEditorMode.Add || this.EditorMode == BillersEditorMode.Edit)
            {
                if (i == 3 || i == 4 || i == 5 || i == 6)
                    liTaxSystem.Visible = true;
                else
                    liTaxSystem.Visible = false;
            }
        }
        /// <summary>
        /// Raise the ApplyModify event generating all the information needed.
        /// </summary>
        public void ApplyChanges()
        {
            int i = 0;
            this.divErrorTaxSystem.Visible = false;
            this.divPlaceDispatchError.Visible = false;
            BillersEditorEventArgs e;

            if (int.TryParse(this.ddlBillType.SelectedValue, out i))
            {
                switch ((PACFD.Rules.ElectronicBillingType)i)
                {
                    case PACFD.Rules.ElectronicBillingType.CFD2_2:
                    case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                    case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                    case PACFD.Rules.ElectronicBillingType.CFDI4_0:

                        this.divErrorTaxSystem.Visible = string.IsNullOrEmpty(this.ddlTaxSystem.SelectedItem.Value);

                        if (this.divErrorTaxSystem.Visible || this.divPlaceDispatchError.Visible)
                            return;
                        break;
                }
            }

            if (!this.ApplyModify.IsNull())
            {
                e = this.OnGenerateBillersEditorEventArgs();
                this.ApplyModify(this, e);
            }
        }

        protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            System.Drawing.Bitmap bitmap;

            if (!this.AsyncFileUpload1.PostedFile.IsNull())
            {
                bitmap = this.AsyncFileUpload1.PostedFile.InputStream.ToBitmap();

                if (!bitmap.IsNull())
                    this.Session["image"] = bitmap.ToByteArray();
                //bitmap.ToSize(200, 200).ToByteArray();
            }
        }
        /// <summary>
        /// Add fiscal regimen to the list of regimes.
        /// </summary>
        protected void btnTaxSystemAdd_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Delete a specific row.
        /// </summary>
        protected void imgbtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            int i;
            System.Data.DataRow[] rs;// = this.FiscalRegimenTable.Select("FiscalRegimeID=" + ((ImageButton)sender).CommandArgument);
            PACFD.DataAccess.BillersDataSet.FiscalRegimeRow row;

            if (!int.TryParse(((ImageButton)sender).CommandArgument, out i))
                return;

            rs = this.FiscalRegimenTable.Select("FiscalRegimeID=" + i.ToString());

            if (rs.IsNull() || rs.Length < 1)
                return;

            row = rs[0] as PACFD.DataAccess.BillersDataSet.FiscalRegimeRow;

            if (row.IsNull())
                return;

            row.Delete();

            //if (this.EditorMode == BillersEditorMode.Add)
            //    row.AcceptChanges();
            //this.FiscalRegimenTable.AcceptChanges();

            this.gdvTaxSystem.DataSource = this.FiscalRegimenTable;
            this.gdvTaxSystem.DataBind();
        }
        /// <summary>
        /// Delete all the Fiscal regime rows selected.
        /// </summary>
        protected void imgbtnDeleteAll_Click(object sender, ImageClickEventArgs e)
        {
            for (int i = this.FiscalRegimenTable.Count - 1; i > -1; --i)
                if (this.FiscalRegimenTable[i].RowState != System.Data.DataRowState.Deleted &&
                  this.FiscalRegimenTable[i].RowState != System.Data.DataRowState.Detached)
                    this.FiscalRegimenTable[i].Delete();

            //this.FiscalRegimenTable.AcceptChanges();
            this.gdvTaxSystem.DataSource = this.FiscalRegimenTable;
            this.gdvTaxSystem.DataBind();
        }
        /// <summary>
        /// Add Place of Dispatch.
        /// </summary>
        protected void btnAddPlaceDispatch_Click(object sender, EventArgs e)
        {
            int i = -1;
            PACFD.DataAccess.BillersDataSet.PlaceDispatchRow row;
            System.Data.DataRow[] rows = null;

            if (string.IsNullOrEmpty(this.txtPlaceDispatch.Text))
            {
                this.divPlaceDispatchError.Visible = true;
                return;
            }

            //foreach (var item in this.PlaceDispatchTable)
            //{
            //    if (item.RowState != System.Data.DataRowState.Added && item.RowState != System.Data.DataRowState.Modified)
            //        continue;

            //    if (item.PlaceDispatch.Equals(this.txtPlaceDispatch.Text))
            //    {
            //        this.divPlaceDispatchError.Visible = true;
            //        return;
            //    }
            //}


            rows = this.PlaceDispatchTable.Select("PlaceDispatch='" + this.txtPlaceDispatch.Text.Trim() + "'");

            if (rows.Length > 0)
            {
                this.divPlaceDispatchError.Visible = true;
                return;
            }

            row = this.PlaceDispatchTable.NewPlaceDispatchRow();
            row.BeginEdit();
            row.Active = false;
            row.BillerID = this.EditorMode == BillersEditorMode.Add ? 0 : this.BillerInformation.BillerID;
            row.PlaceDispatch = this.txtPlaceDispatch.Text.Trim();

            //foreach (var item in this.PlaceDispatchTable)
            //{
            //    if (item.RowState != System.Data.DataRowState.Added && item.RowState != System.Data.DataRowState.Modified)
            //        continue;

            //    if (item.PlaceDispatchID == i || item.PlaceDispatchID == (i * -1))
            //        ++i;
            //}
            rows = this.PlaceDispatchTable.Select("PlaceDispatchID < 0");
            i = -1;

            for (int x = 0; x < rows.Length; ++x)
            {
                if (i == ((PACFD.DataAccess.BillersDataSet.PlaceDispatchRow)rows[x]).PlaceDispatchID)
                {
                    --i;
                    x = -1;
                }
            }

            row.PlaceDispatchID = i;
            row.EndEdit();
            this.PlaceDispatchTable.AddPlaceDispatchRow(row);
            row.AcceptChanges();
            this.txtPlaceDispatch.Text = string.Empty;

            //if (this.EditorMode == BillersEditorMode.Add)
            row.SetAdded();
            //else
            //    row.SetModified();

            this.gdvPlaceDispatch.DataSource = this.PlaceDispatchTable;
            this.gdvPlaceDispatch.DataBind();
        }

        protected void gdvPlaceDispatch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            PACFD.DataAccess.BillersDataSet.PlaceDispatchRow row;
            ImageButton img;

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            row = ((System.Data.DataRowView)e.Row.DataItem).Row as PACFD.DataAccess.BillersDataSet.PlaceDispatchRow;

            if (row == null || row.RowState == System.Data.DataRowState.Detached || row.RowState == System.Data.DataRowState.Deleted)
                return;

            img = e.Row.FindControl("imgbtnActivePlace") as ImageButton;

            if (row.Active)
                img.ImageUrl = string.Format("~/Includes/Images/png/{0}.png", row.Active ? "apply" : "editdelete");
            else
                img.ImageUrl = "~/Includes/Images/png/editdelete.png";
        }
        /// <summary>
        /// Delete a single place of dispatch.
        /// </summary>
        protected void imgbtnDeletePlace_Click(object sender, ImageClickEventArgs e)
        {
            int i = 0;
            System.Data.DataRow[] rows;
            PACFD.DataAccess.BillersDataSet.PlaceDispatchRow row;

            if (!int.TryParse(((ImageButton)sender).CommandArgument, out i))
                return;

            rows = this.PlaceDispatchTable.Select("PlaceDispatchID=" + i.ToString());

            if (rows.IsNull() || rows.Length < 1)
                return;

            row = rows[0] as PACFD.DataAccess.BillersDataSet.PlaceDispatchRow;

            if (row.IsNull())
                return;

            row.Delete();

            //if (this.EditorMode == BillersEditorMode.Add)
            //row.AcceptChanges();

            this.gdvPlaceDispatch.DataSource = this.PlaceDispatchTable;
            this.gdvPlaceDispatch.DataBind();
        }
        /// <summary>
        /// Delete all place of dipatch.
        /// </summary>
        protected void imgbtnDeleteAllPlaces_Click(object sender, ImageClickEventArgs e)
        {
            for (int i = this.PlaceDispatchTable.Count - 1; i > -1; --i)
                if (this.PlaceDispatchTable[i].RowState != System.Data.DataRowState.Deleted &&
                    this.PlaceDispatchTable[i].RowState != System.Data.DataRowState.Detached)
                    this.PlaceDispatchTable[i].Delete();
            //this.PlaceDispatchTable[i].AcceptChanges();

            //this.PlaceDispatchTable.AcceptChanges(); 
            this.gdvPlaceDispatch.DataSource = this.PlaceDispatchTable;
            this.gdvPlaceDispatch.DataBind();
        }
        /// <summary>
        /// Active a place of dispatch from the grid.
        /// </summary>
        protected void imgbtnActivePlace_Click(object sender, ImageClickEventArgs e)
        {
            int i = 0;
            ImageButton b = sender as ImageButton;

            if (b.IsNull() || !int.TryParse(b.CommandArgument, out i))
                return;

            foreach (var item in this.PlaceDispatchTable)
            {
                if (item.RowState != System.Data.DataRowState.Added && item.RowState != System.Data.DataRowState.Modified)
                    continue;

                item.BeginEdit();
                item.Active = item.PlaceDispatchID == i ? true : false;
                item.EndEdit();
                item.AcceptChanges();

                if (item.PlaceDispatchID < 0)
                {
                    item.SetAdded();
                }
                else
                {
                    //if (this.EditorMode == BillersEditorMode.Add)
                    //    item.SetAdded();
                    //else
                    item.SetModified();
                }
                //if (this.EditorMode == BillersEditorMode.Edit)
                //    (new global::PACFD.Rules.PlaceDispatch()).SetActive(this.CurrentBillerID, i, !item.Active);
            }

            this.gdvPlaceDispatch.DataSource = this.PlaceDispatchTable;
            this.gdvPlaceDispatch.DataBind();
        }
    }
}