﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    public partial class BillersOthersConfigurations : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPeriod();
            }

        }
        protected void LoadPeriod()
        {
            ddlYears.Items.Clear();
            for (int i = 2011; i <= (DateTime.Now.Year + 1); i++)
                ddlYears.Items.Add(new ListItem(i.ToString(), i.ToString()));
            ddlYears.SelectedIndex = ddlYears.Items.IndexOf(ddlYears.Items.FindByValue(DateTime.Now.Year.ToString()));

            string month = "";
            DateTime d = new DateTime(DateTime.Now.Year, 1, 1);
            for (int i = 1; i <= 12; i++)
            {
                month = d.ToString("MMM",new System.Globalization.CultureInfo("es-mx"));
                ddlMonth.Items.Add(new ListItem(month, i.ToString()));
                d = d.AddMonths(1);
            }
            ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(DateTime.Now.Month.ToString()));



        }


        protected void btnAcept_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
