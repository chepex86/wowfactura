<%@ Page Title="Modificar empresa." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillersModify.aspx.cs" Inherits="PACFDManager.Billers.BillersModify" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="BillersEditor.ascx" TagName="BillersEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="IssuedEditor.ascx" TagName="IssuedEditor" TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">

    <script language="javascript" type="text/javascript">
        function IssuedHideShow() {
            var obj = document.getElementById('<%=pnlIssued.ClientID%>');

            if (obj == null)
                return;

            obj.style.display = obj.style.display == 'none' ? 'block' : 'none';
            IssuedEnableValidators(obj.style.display == 'none' ? false : true)
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Image CssClass="imgCompany right" Style="position: absolute" ID="Image1" runat="server" />
            <div class="clear">
            </div>
            <div id="container" class="wframe70">
                <div class="form">
                    <div class="formStyles">
                        <uc1:BillersEditor ID="BillersEditor1" runat="server" EditorMode="Edit" OnApplyModify="BillersEditor1_ApplyModify" />
                        <asp:Panel ID="Panel1" runat="server" Style="text-align: left">
                            <asp:CheckBox ID="ckbIssued" OnClick='javascript:IssuedHideShow();' TextAlign="Left"
                                runat="server" Checked="false" Text="�El facturador es el empresa?" Visible="False" />
                        </asp:Panel>
                        <asp:Panel ID="pnlIssued" runat="server">
                            <uc3:IssuedEditor ID="IssuedEditor1" runat="server" />
                        </asp:Panel>
                        <ul>
                            <li class="buttons">
                                <asp:Button ID="btnAcept" runat="server" CssClass="Button" Text="Aceptar" OnClick="btnAcept_Click" />
                                <asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                    Text="Cancelar" OnClick="btnCancel_Click" /></li>
                        </ul>
                    </div>
                </div>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
