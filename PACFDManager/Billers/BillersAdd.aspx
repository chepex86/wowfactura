<%@ Page Title="Agregar empresa." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillersAdd.aspx.cs" Inherits="PACFDManager.Billers.BillersAdd" %>

<%@ Register Src="BillersEditor.ascx" TagName="BillersEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="IssuedEditor.ascx" TagName="IssuedEditor" TagPrefix="uc3" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

    <script language="javascript" type="text/javascript">
        function PACFDManagerBillersBillersAdd_IssuedHideShow() {
            var obj = document.getElementById('<%=pnlIssued.ClientID%>');

            if (obj == null)
                return;

            obj.style.display = obj.style.display == 'none' ? 'block' : 'none';
            IssuedEnableValidators(obj.style.display == 'none' ? false : true)
        }        
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="divContent" runat="server">
                <div id="container" class="wframe70">
                    <div class="form">
                        <div class="formStyles">
                            <uc1:BillersEditor ID="BillersEditor1" runat="server" EditorMode="Add" OnApplyModify="BillersEditor1_ApplyModify" />
                            <asp:Panel ID="Panel1" runat="server" Style="text-align: left">
                                <asp:CheckBox ID="ckbIssued" runat="server" Text="�El facturador es el empresa?"
                                    TextAlign="Left" AutoPostBack="false" OnClick='javascript:PACFDManagerBillersBillersAdd_IssuedHideShow();'
                                    Checked="true" ToolTip="Si el empresa es diferente de la matriz deseleccione esta opci�n."
                                    Visible="False" />
                                <uc4:Tooltip ID="ttpIssued" runat="server" ToolTip="Deselecciona la casilla, si la direcci�n f�sica donde se expiden los comprobantes,<br />es una ubicaci�n diferente a la especificada en la parte de arriba de esta p�gina.<br /><br />Si este es el caso, introduce la informaci�n que se pide en la parte de abajo,<br />que corresponde a la direcci�n f�sica del lugar donde la empresa expide los<br />comprobantes."
                                    Visible="False" />
                            </asp:Panel>
                            <asp:Panel ID="pnlIssued" runat="server" Style="display: none; text-align: left">
                                <uc3:IssuedEditor ID="IssuedEditor1" runat="server" />
                            </asp:Panel>
                            <ul>
                                <li class="buttons">
                                    <div>
                                        <asp:Button ID="btnAcept" CssClass="Button" runat="server" Text="Aceptar" OnClick="btnAcept_Click" />
                                        <asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                            Text="Cancelar" OnClick="btnCancel_Click" />
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click"
                OnCancelClick="btnCancel_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
