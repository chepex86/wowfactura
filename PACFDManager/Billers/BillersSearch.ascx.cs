﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    public partial class BillersSearch : System.Web.UI.UserControl
    {
        public delegate void SearchEventHandler(object sender, Billers.BillersSearchEventArgs e);
        public event SearchEventHandler Search;


        public BillersSearchInfo SearchInformation { get; private set; }


        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public BillersSearch()
        {
            this.SearchInformation = new BillersSearchInfo(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterScriptsForProgressBar();
        }
        protected void OnSearch(Billers.BillersSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b.IsNull())
                return;

            //if (this.btnClean == b)
            //{
            //    this.txtName.Text =
            //        this.txtRFC.Text = string.Empty;
            //    return;
            //}

            if (this.txtName.Text.Trim().Length > 0 && this.txtRFC.Text.Trim().Length > 0)
                this.OnSearch(this.OnGenerateSearchArguments(SearchMode.Both));
            else if (this.txtName.Text.Trim().Length > 0)
                this.OnSearch(this.OnGenerateSearchArguments(SearchMode.Name));
            else if (this.txtRFC.Text.Trim().Length > 0)
                this.OnSearch(this.OnGenerateSearchArguments(SearchMode.RFC));
            else
                this.OnSearch(this.OnGenerateSearchArguments(SearchMode.None));

        }
        protected virtual BillersSearchEventArgs OnGenerateSearchArguments(SearchMode mode)
        {
            return new BillersSearchEventArgs(
                mode == SearchMode.Both || mode == SearchMode.RFC ? this.txtRFC.Text.Trim() : null,
                mode == SearchMode.Both || mode == SearchMode.Name ? this.txtName.Text.Trim() : null,
                mode);
        }
        public virtual void PerformSearch(SearchMode mode)
        {
            this.OnSearch(this.OnGenerateSearchArguments(mode));
        }

        private void RegisterScriptsForProgressBar()
        {
            if (this.Page.ClientScript.IsStartupScriptRegistered(String.Format("LoadBindControls_{0}", this.Page.ClientID)))
                return;

            String scriptToRegister = String.Empty;
            String uProgressByName = String.Empty;
            String uProgressByRFC = String.Empty;

            uProgressByName = String.Format("jsBiller.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.AutoCompleteExtender1.BehaviorID, this.spnByName.ClientID, true);

            uProgressByRFC = String.Format("jsBiller.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.AutoCompleteExtender2.BehaviorID, this.spnByRFC.ClientID, true);

            scriptToRegister = "Sys.Application.add_load(function() {";
            scriptToRegister += String.Format("{2}{0}{2}{1}{2}", uProgressByName, uProgressByRFC, System.Environment.NewLine);
            scriptToRegister += "});";

            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), String.Format("LoadBindControls_{0}", this.Page.ClientID), scriptToRegister, true);
        }
    }
}