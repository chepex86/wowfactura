﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Billers
{
    partial class IssuedEditor
    {
        /// <summary>
        /// 
        /// </summary>
        public class IssuedEditorInfo
        {
            /// <summary>
            /// 
            /// </summary>
            private IssuedEditor Parent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Address
            {
                get { return this.Parent.txtAddress.Text; }
                set { this.Parent.txtAddress.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string ExternalNumber
            {
                get { return this.Parent.txtExternalNumber.Text; }
                set { this.Parent.txtExternalNumber.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string InternalNumber
            {
                get { return this.Parent.txtInternalNumber.Text; }
                set { this.Parent.txtInternalNumber.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Colony
            {
                get { return this.Parent.txtColony.Text; }
                set { this.Parent.txtColony.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            //public ListItem Location
            //{
            //    get { return this.Parent.CountrySelector1.City; }
            //}
            /// <summary>
            /// 
            /// </summary>
            public string Reference
            {
                get { return this.Parent.txtReference.Text; }
                set { this.Parent.txtReference.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            //public ListItem State
            //{
            //    get { return this.Parent.CountrySelector1.State; }
            //}
            /// <summary>
            /// 
            /// </summary>
            //public ListItem Country
            //{
            //    get { return this.Parent.CountrySelector1.Country; }
            //}
            /// <summary>
            /// 
            /// </summary>
            public String Zipcode
            {
                get
                {
                    //int i;

                    //if (!int.TryParse(this.Parent.txtZipcode.Text, out i))
                    //    return 0;

                    //return i;

                    return this.Parent.txtZipcode.Text;
                }
                set { this.Parent.txtZipcode.Text = value.ToString(); }
            }
            /// <summary>
            /// 
            /// </summary>
            public PACFD.DataAccess.BillersDataSet.IssuedDataTable IssuedDataTable
            {
                get { return this.Parent.OnGenerateDataTable(); }
                private set { }
            }


            public IssuedEditorInfo(IssuedEditor owner)
            {
                this.Parent = owner;
            }
        }
    }
}
