﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    /// <summary>
    /// 
    /// </summary>
    public class BillersSearchEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public SearchMode SearchType { get; private set; }
        public string RFC { get; private set; }
        public string Name { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchtype"></param>
        public BillersSearchEventArgs(string rfc, string name, SearchMode searchtype)
        {
            this.SearchType = searchtype;
            this.RFC = rfc;
            this.Name = name;
        } 
    }
}
