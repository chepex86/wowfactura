﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    public class IssuedEditorEventArgs : EventArgs
    {
        public IssuedEditorMode EditorMode { get; private set; }


        public IssuedEditorEventArgs(IssuedEditorMode mode)
        {
            this.EditorMode = mode;
        }
    }
}
