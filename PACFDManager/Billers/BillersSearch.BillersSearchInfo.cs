﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    partial class BillersSearch
    {
        public class BillersSearchInfo
        {
            public BillersSearch Parent { get; private set; }
            public string RFC
            {
                get { return this.Parent.txtRFC.Text; }
                set { this.Parent.txtRFC.Text = value; }
            }
            public string Name
            {
                get { return this.Parent.txtName.Text; }
                set { this.Parent.txtName.Text = value; }
            }


            public BillersSearchInfo(BillersSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
