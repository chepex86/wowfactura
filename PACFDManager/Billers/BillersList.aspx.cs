﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;
using System.Data;

namespace PACFDManager.Billers
{
    public partial class BillersList : PACFDManager.BasePage
    {
        const string MESSAGE_DELETE = "MESSAGE_DELETE";
        const string MESSAGE_REACTIVATE = "MESSAGE_REACTIVATE";

        /// <summary>
        /// Use for store temporal Biller ID's
        /// </summary>
        private int TemporalBillersID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.Billers billers;
            //PACFD.DataAccess.BillersDataSet.Billers_GetForListDataTable table;

            if (this.IsPostBack)
                return;

            //table = new PACFD.DataAccess.BillersDataSet.Billers_GetForListDataTable();
            billers = new PACFD.Rules.Billers();
            //billers.SelectBillersForList(table, this.CurrentGroupID, true);

            if (!this.IsAdministrator)
                this.gdvBillers.DataSource = billers.SelectByRFCName(this.CurrentGroupID, null, null, true);
            else
                this.gdvBillers.DataSource = billers.SelectByRFCName(this.CurrentGroupID, null, null);

            this.gdvBillers.DataBind();

            this.lblNumberBillers.Text = this.gdvBillers.Rows.Count.ToString();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(BillersAdd).Name + ".aspx");
        }

        protected void ImageButton_Click(object sender, EventArgs e)
        {
            ImageButton b = sender as ImageButton;
            int r;

            if (b.IsNull())
                return;

            if (!int.TryParse(b.CommandArgument, out r))
                return;

            this.TemporalBillersID = r;

            switch (b.ID)
            {
                case "imbDetails":
                case "imbEdit":
                    this.Session["id"] = r;
                    this.Response.Redirect(string.Format("{0}{1}",
                        typeof(BillersModify).Name,
                        b.ID == "imbDetails" ? ".aspx?e=false" : ".aspx"));
                    break;
                case "imbDelete":
                    this.WebMessageBox1.CommandArguments = MESSAGE_DELETE;
                    this.WebMessageBox1.ShowMessage("¿Desea eliminar la empresa?", System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
                case "imbReactivate":
                    this.WebMessageBox1.CommandArguments = MESSAGE_REACTIVATE;
                    this.WebMessageBox1.ShowMessage("¿Desea reactivar la empresa?", System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Billers billers;
            PACFD.DataAccess.BillersDataSet.BillersDataTable table;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            billers = new PACFD.Rules.Billers();

            switch (e.CommandArguments)
            {
                case MESSAGE_DELETE:
                    table = billers.SelectByID(this.TemporalBillersID);

                    if (table.Count < 1)
                        return;

                    table[0].Active = false;

                    if (!billers.Update(table))
                    {
                        this.WebMessageBox1.CommandArguments = "-1";
                        this.WebMessageBox1.ShowMessage("No se pudo eliminar la empresa.", true);
                        billers = null;
                        break;
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(table);

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino una empresa del sistema.",
                        this.UserName), this.CurrentBillerID, null, ds.GetXml());
                    #endregion

                    ds.Dispose();

                    billers = null;
                    this.WebMessageBox1.CommandArguments = string.Empty;
                    this.Response.Redirect(typeof(BillersList).Name + ".aspx");
                    break;
                case MESSAGE_REACTIVATE:
                    table = billers.SelectByID(this.TemporalBillersID);

                    if (table.Count < 1)
                        return;

                    table[0].Active = true;

                    if (!billers.Update(table))
                    {
                        this.WebMessageBox1.CommandArguments = "-1";
                        this.WebMessageBox1.ShowMessage("No se pudo reactivar la empresa.", true);
                        billers = null;
                        break;
                    }

                    DataSet dsReactive = new DataSet();
                    dsReactive.Tables.Add(table);

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} reactivo una empresa eliminada.",
                        this.UserName), this.CurrentBillerID, null, dsReactive.GetXml());
                    #endregion

                    dsReactive.Dispose();

                    billers = null;
                    this.WebMessageBox1.CommandArguments = string.Empty;
                    this.Response.Redirect(typeof(BillersList).Name + ".aspx");
                    break;
            }

            this.WebMessageBox1.CommandArguments = string.Empty;
        }

        protected void BillersSearch1_Search(object sender, Billers.BillersSearchEventArgs e)
        {
            PACFD.Rules.Billers billers = new PACFD.Rules.Billers();

            if (!this.IsAdministrator)
                this.gdvBillers.DataSource = billers.SelectByRFCName(this.CurrentGroupID, e.RFC, e.Name, true);
            else
                this.gdvBillers.DataSource = billers.SelectByRFCName(this.CurrentGroupID, e.RFC, e.Name);

            this.gdvBillers.DataBind();

            this.lblNumberBillers.Text = this.gdvBillers.Rows.Count.ToString();
            billers = null;
        }

        protected bool OnDataBindIsSupervisor()
        {
            return this.IsAdministrator;
        }
        /// <summary>
        /// Bind specific data to the grid view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gdvBillers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            int i = 0;
            Dictionary<PACFD.Rules.ElectronicBillingType, string> dick;

            if (row.RowIndex < 0)
                return;

            Label l = e.Row.FindControl("lblElectronicBillingTypeGrid") as Label;
            dick = PACFD.Rules.ElectronicBillingType.CBB.ToDictionary();

            if (!int.TryParse(l.Text, out i))
                l.Text = "Indefinido";
            else
                l.Text = dick[(PACFD.Rules.ElectronicBillingType)i];
        }
    }
}