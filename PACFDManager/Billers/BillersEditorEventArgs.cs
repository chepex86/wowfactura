﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Billers
{
    /// <summary>
    /// Class passed as arguments by the PACFDManager.Billers.BillersEditor control.
    /// </summary>
    public class BillersEditorEventArgs : EventArgs
    {
        public BillersEditorMode EditorMode { get; private set; }
        //public PACFD.DataAccess.BillersDataSet.BillersDataTable Table { get; private set; }
        public PACFD.DataAccess.BillersDataSet BillingDataSet { get; private set; }

        public BillersEditorEventArgs(
            //PACFD.DataAccess.BillersDataSet.BillersDataTable table,
            PACFD.DataAccess.BillersDataSet dataset,
            BillersEditorMode mode)
        {
            this.EditorMode = mode;
            this.BillingDataSet = dataset;
        }
    }
}
