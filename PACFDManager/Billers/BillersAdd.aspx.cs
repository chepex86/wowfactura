﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Billers
{
    public partial class BillersAdd : PACFDManager.BasePage
    {
        const string MESSAGE_QUESTION_ADDNEW = "3";

        /// <summary>
        /// Get or set a PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable table.
        /// </summary>
        private PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable BillerTaxDataTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return new PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable();

                return (PACFD.DataAccess.TaxTypesDataSet.TaxTypesGetAllDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.BillersEditor1.BillerID = this.CurrentBillerID;
            this.BillersEditor1.EditorMode = BillersEditorMode.Add;
            //this.BillersEditor1.ClientBillerID = this.CurrentClientID;
            this.BillersEditor1.GroupID = this.CurrentGroupID;
            this.BillersEditor1.Enabled = true;

            this.pnlIssued.Style["display"] = this.ckbIssued.Checked ? "none" : "block";
            this.IssuedEditor1.EnabledValidator = !this.ckbIssued.Checked;
            this.IssuedEditor1.Enabled = true;
            this.IssuedEditor1.EditorMode = IssuedEditorMode.Add;

            if (this.IsPostBack)
                return;

            //PACFD.Rules.TaxTypes taxes = new PACFD.Rules.TaxTypes();

            //this.gdvTaxes.DataSource =
            //    this.BillerTaxDataTable = taxes.SelectAllTaxTypes();
            //this.gdvTaxes.DataBind();
            //this.DataBindRowsTaxes();
        }
        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_QUESTION_ADDNEW:

                    this.WebMessageBox1.CommandArguments = string.Empty;

                    if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
                    {
                        this.Response.Redirect(typeof(BillersAdd).Name + ".aspx");
                        return;
                    }

                    this.btnCancel_Click(this.btnCancel, EventArgs.Empty);
                    break;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(BillersList).Name + ".aspx"); }
        /// <summary>
        /// 
        /// </summary>
        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.BillersEditor1.ApplyChanges();
        }

        protected void BillersEditor1_ApplyModify(object sender, BillersEditorEventArgs e)
        {
            PACFD.Rules.Billers billers = new PACFD.Rules.Billers();
            //PACFD.DataAccess.BillersDataSet dataset = new PACFD.DataAccess.BillersDataSet();

            this.WebMessageBox1.CommandArguments = string.Empty;

            //dataset.Billers.Clear();
            //dataset.Billers.Merge(this.BillersEditor1.BillerInformation.BillersDataTable, true);

            e.BillingDataSet.Billers[0].BeginEdit();
            e.BillingDataSet.Billers[0].IssuedInDifferentPlace = !this.ckbIssued.Checked;
            e.BillingDataSet.Billers[0].EndEdit();
            e.BillingDataSet.Billers[0].AcceptChanges();
            e.BillingDataSet.Billers[0].SetAdded();

            //dataset.FiscalRegime.Clear();
            //dataset.FiscalRegime.Merge(this.BillersEditor1.FiscalRegimenTable, true);

            e.BillingDataSet.Issued.Clear();
            e.BillingDataSet.Issued.Merge(this.ckbIssued.Checked ? e.BillingDataSet.Billers.GenerateIssuedFromBiller() :
                this.IssuedEditor1.IssuedInformation.IssuedDataTable, true);

            if (!billers.Update(e.BillingDataSet))
            {
                LogManager.WriteError(new Exception("Error al agregar empresa."));
                this.WebMessageBox1.ShowMessage("Error al agregar empresa.", System.Drawing.Color.Red);
                return;
            }

            // Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego una nueva empresa al sistema.",
                this.UserName), this.CurrentBillerID, null, e.BillingDataSet.GetXml());

            this.WebMessageBox1.CommandArguments = MESSAGE_QUESTION_ADDNEW;
            this.WebMessageBox1.ShowMessage("Exito al agregar empresa. ¿Desea agregar otra?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.InitializePDFs(e.BillingDataSet);
        }
        /// <summary>
        /// Initialize the XML used for PDF print.
        /// </summary>
        /// <param name="dataset">DataSet with the PDF to config.</param>
        private void InitializePDFs(PACFD.DataAccess.BillersDataSet dataset)
        {
            const string epb = "Tipo de ElectronicBillingType no encontrado, no se pudo configurar el tipo de impreción.";
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table =
                new PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable();
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesRow row;

            row = table.NewPrintTemplatesRow();
            row.Name = "Predeterminado";

            switch (dataset.Billers[0].ElectronicBillingType.ToElectronicBillingType())
            {
                case ElectronicBillingType.CBB: row.Text = 
                    PACFD.Rules.PrintTemplates.GetStringDefaultCBB(); 
                break;
                case ElectronicBillingType.CFD:
                case ElectronicBillingType.CFD2_2: 
                    row.Text = PACFD.Rules.PrintTemplates.GetStringDefaultCFD(); 
                break;
                case ElectronicBillingType.CFDI:
                    row.Text = PACFD.Rules.PrintTemplates.GetStringDefaultCFDI(); 
                break;
                case ElectronicBillingType.CFDI3_2:
                    row.Text = PACFD.Rules.PrintTemplates.GetStringDefaultCFDI3_2();
                    break;
                case ElectronicBillingType.CFDI3_3:
                    row.Text = PACFD.Rules.PrintTemplates.GetStringDefaultCFDI3_3();
                    break;
                case ElectronicBillingType.CFDI4_0:
                    row.Text = PACFD.Rules.PrintTemplates.GetStringDefaultCFDI4_0(); 
                break;
                default:
                    LogManager.WriteError(new Exception(epb));
                    this.WebMessageBox1.ShowMessage(epb, System.Drawing.Color.Red);
                    return;
            }

            row.PrintTemplateID = -1;
            row.IsBase = false;
            row.ElectronicBillingType = dataset.Billers[0].ElectronicBillingType;
            row.BillerID = dataset.Billers[0].BillerID;
            table.AddPrintTemplatesRow(row);
            row.AcceptChanges();
            row.SetAdded();
            row = null;

            (new PrintTemplates()).Update(table);
        }
    }
}