﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PACFD.Rules;

namespace PACFDManager.Billers
{
    partial class BillersEditor
    {
        /// <summary>
        /// Class used to group all main information about BillersEditor class.
        /// </summary>
        public class BillersEditorInfo
        {
            private const string ELECTRONICTYPE = "BillersEditor.BillersEditorInfo.ElectronicBillingType";
            private const string BILLERID = "BillersEditor.BillersEditorInfo.BillerID";

            public int BillerID
            {
                get
                {
                    object o = this.Parent.Session[BILLERID];

                    if (o.IsNull() || o.GetType() != typeof(int))
                        return 0;

                    return (int)o;
                }
                set { this.Parent.Session[BILLERID] = value; }
            }
            public ElectronicBillingType ElectronicType
            {
                get
                {
                    object o = this.Parent.ViewState[ELECTRONICTYPE];

                    if (o.IsNull() || o.GetType() != typeof(ElectronicBillingType))
                        return ElectronicBillingType.Indeterminate;

                    return (ElectronicBillingType)o;
                }
                set { this.Parent.ViewState[ELECTRONICTYPE] = value; }
            }
            /// <summary>
            /// Get the BillersEditor parent class of the object.
            /// </summary>
            private BillersEditor Parent { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this.Parent.txtName.Text; }
                set { this.Parent.txtName.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Address
            {
                get { return this.Parent.txtAddress.Text; }
                set { this.Parent.txtAddress.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string RFC
            {
                get { return this.Parent.txtRFC.Text.ToUpper(); }
                set { this.Parent.txtRFC.Text = value.ToUpper(); }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Phone
            {
                get { return this.Parent.txtPhone.Text; }
                set { this.Parent.txtPhone.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string ExternalNumber
            {
                get { return this.Parent.txtExternalNumber.Text; }
                set { this.Parent.txtExternalNumber.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string InternalNumber
            {
                get { return this.Parent.txtInternalNumber.Text; }
                set { this.Parent.txtInternalNumber.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Colony
            {
                get { return this.Parent.txtColony.Text; }
                set { this.Parent.txtColony.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            //public ListItem Location { get { return this.Parent.CountrySelector1.City; } }
            /// <summary>
            /// 
            /// </summary>
            public string Reference
            {
                get { return this.Parent.txtReference.Text; }
                set { this.Parent.txtReference.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            //public ListItem State { get { return this.Parent.CountrySelector1.State; } }
            /// <summary>
            /// 
            /// </summary>
            //public ListItem Country { get { return this.Parent.CountrySelector1.Country; } }
            /// <summary>
            /// 
            /// </summary>
            public String Zipcode
            {
                get
                {
                    //int i;

                    //if (!int.TryParse(this.Parent.txtZipcode.Text, out i))
                    //    return 0;

                    //return i;

                    return this.Parent.txtZipcode.Text;
                }
                set { this.Parent.txtZipcode.Text = value.ToString(); }
            }
            /// <summary>
            /// 
            /// </summary>
            public System.Web.UI.WebControls.ListItem Currency { get { return this.Parent.ddlCurrency.SelectedItem; } }
            /// <summary>
            /// 
            /// </summary>
            public int CurrencyIndex
            {
                get { return this.Parent.ddlCurrency.SelectedIndex; }
                set { this.Parent.ddlCurrency.SelectedIndex = value; }
            }
            public int BillType
            {
                get
                {
                    int i = 0;
                    return int.TryParse(this.Parent.ddlBillType.SelectedValue, out i) ? i : 0;
                }
                set
                {
                    Dictionary<PACFD.Rules.ElectronicBillingType, string> dick;

                    //if (this.Parent.ddlBillType.Items.Count < 1)
                    //    this.Parent.BlockBill();

                    dick = PACFD.Rules.ElectronicBillingType.CBB.ToDictionary();

                    this.Parent.ddlBillType.SelectedIndex = this.Parent.ddlBillType.Items.IndexOf(this.Parent.ddlBillType.Items.FindByValue(value.ToString()));
                    //this.Parent.lblBillTypeShow.Text =
                    //    value == 0 ? dick[(PACFD.Rules.ElectronicBillingType)value]
                    //    : value == 1 ? dick[(PACFD.Rules.ElectronicBillingType)value]
                    //    : value == 2 ? dick[(PACFD.Rules.ElectronicBillingType)value]
                    //    : value == 3 ? dick[(PACFD.Rules.ElectronicBillingType)value]
                    //    : value == 4 ? dick[(PACFD.Rules.ElectronicBillingType)value]
                    //    : dick[PACFD.Rules.ElectronicBillingType.Indeterminate];
                }
            }
            /// <summary>                         
            /// 
            /// </summary>
            public System.Web.UI.WebControls.ListItemCollection CurrencyItems { get { return this.Parent.ddlCurrency.Items; } }
            /// <summary>
            /// Get an array of byte with the file uploaded by the user.
            /// </summary>
            public byte[] Logo { get { return this.Parent.AsyncFileUpload1.FileBytes; } }

            ///// <summary>
            ///// Get a PACFD.DataAccess.BillersDataSet.BillersDataTable table generated by the control.
            ///// </summary>
            //public PACFD.DataAccess.BillersDataSet.BillersDataTable BillersDataTable
            //{
            //    get
            //    {
            //        BillersEditorEventArgs b = this.Parent.OnGenerateBillersEditorEventArgs();
            //        return b.Table;
            //    }
            //}

            /// <summary>
            /// Get or ser an integer value with the bank account.
            /// </summary>
            public string BankAccount
            {
                get { return this.Parent.txtBankAccount.Text; }
                set { this.Parent.txtBankAccount.Text = value.ToString(); }
            }
            /// <summary>
            /// Get or set an integer value for export to compaq.
            /// </summary>
            public string IvaAccount
            {
                get { return this.Parent.txtIvaAccount.Text; }
                set { this.Parent.txtIvaAccount.Text = value.ToString(); }
            }
            public string Email
            {
                get { return this.Parent.txtEmail.Text; }
                set { this.Parent.txtEmail.Text = value.ToString(); }
            }


            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">BillersEditor parent of the class.</param>
            public BillersEditorInfo(BillersEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
