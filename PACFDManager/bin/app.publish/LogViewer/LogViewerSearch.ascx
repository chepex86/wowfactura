﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogViewerSearch.ascx.cs"
    Inherits="PACFDManager.LogViewer.LogViewerSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div id="container" class="wframe60">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <span>Búsqueda de Logs</span>
                </h2>
                <ul>
                    <li class="left">
                        <label class="desc">
                            <span>Por Usuario:</span>
                        </label>
                        <span>
                            <asp:DropDownList ID="ddlByUserName" runat="server" />
                        </span></li>
                    <li class="left">
                        <label class="desc">
                            <span>Por Empresa:</span>
                        </label>
                        <span>
                            <asp:DropDownList ID="ddlByBillerName" runat="server" />
                        </span></li>
                </ul>
                <ul>
                    <li class="left">
                        <label class="desc">
                            <asp:CheckBox ID="cbxDisabledDate" runat="server" Checked="true" OnCheckedChanged="cbxDisabledDate_OnCheckedChanged" Text="Usar Fecha" AutoPostBack="True" /></label>
                    </li>
                    <li id="liDate" runat="server" class="left">
                        <label class="desc">
                            <span>Por Fecha:</span>
                        </label>
                        <span>
                            <asp:TextBox ID="txtByDate" runat="server" ReadOnly="True" />
                            <asp:CalendarExtender ID="txtByDate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtByDate" TodaysDateFormat="dd MM yyyy">
                            </asp:CalendarExtender>
                        </span></li>
                    <li class="left">
                        <label class="desc">
                            <span>Por Descripción:</span>
                        </label>
                        <span>
                            <asp:TextBox ID="txtByDescription" runat="server" />
                        </span></li>
                </ul>
                <ul>
                    <li class="left">
                        <label class="desc">
                            <span>Por antes de cambios:</span></label>
                        <span>
                            <asp:DropDownList ID="ddlBefore" runat="server">
                                <asp:ListItem Text="[Todos]" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Lleno" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Vacío" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </span></li>
                    <li class="left">
                        <label class="desc">
                            <span>Por despues de cambios:</span></label>
                        <span>
                            <asp:DropDownList ID="ddlAfter" runat="server">
                                <asp:ListItem Text="[Todos]" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Lleno" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Vacío" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </span></li>
                    <li>
                        <div>
                            <asp:Button ID="btnSearch" runat="server" Text="Buscar" CssClass="Button_Search"
                                OnClick="btnSearch_Click" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
