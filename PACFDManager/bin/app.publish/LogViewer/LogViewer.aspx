<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogViewer.aspx.cs" MasterPageFile="~/Default.Master"
    Inherits="PACFDManager.LogViewer.LogViewer" %>

<%@ Register Src="LogViewerSearch.ascx" TagName="LogViewerSearch" TagPrefix="uc1" %>
<%@ Register Src="~/LogViewer/Viewer.ascx" TagName="Viewer" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">

    <script src="LogViewerScripts.js" type="text/javascript"></script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc1:LogViewerSearch ID="ucSearch" runat="server" />
                <uc2:Viewer ID="ucViewer" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">

        function inicializa() {
            //SyntaxHighlighter.defaults["collapse"] = true;
            SyntaxHighlighter.config["stripBrs"] = true;
            SyntaxHighlighter.defaults["auto-links"] = false;
            SyntaxHighlighter.defaults["toolbar"] = false;
            SyntaxHighlighter.highlight();
        }

        Sys.Application.add_load(function() { inicializa(); });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(
        function() {
            inicializa();
        });
    </script>

</asp:Content>
