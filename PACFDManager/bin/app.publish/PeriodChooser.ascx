﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PeriodChooser.ascx.cs"
    Inherits="PACFDManager.PeriodChooser" %>
<table border="0" cellpadding="0" cellspacing="2">
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="cmbMonth">
            </asp:DropDownList>
        </td>
        <td style="width: 5px;">
        </td>
        <td>
            <asp:DropDownList runat="server" ID="cmbYear">
            </asp:DropDownList>
        </td>
    </tr>
</table>
