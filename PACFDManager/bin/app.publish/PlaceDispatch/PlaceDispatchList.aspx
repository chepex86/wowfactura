﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="PlaceDispatchList.aspx.cs" Inherits="PACFDManager.PlaceDispatch.PlaceDispatchList" %>

<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="txt_left">
                <div class="info">
                    <h2>
                        Listado de Lugar de Comprobante
                    </h2>
                </div>
                <asp:Button ID="btnAdd" runat="server" Text="Agregar Lugar" CssClass="Button" OnClick="btnAdd_Click" />
                <asp:GridView ID="gdvPlaceDispatch" runat="server" CssClass="DataGrid" AutoGenerateColumns="False"
                    EmptyDataText="No hay lugar de expedicion">
                    <Columns>
                        <asp:BoundField DataField="PlaceDispatchID" Visible="false" />
                        <asp:BoundField DataField="BillerID" Visible="false" />
                        <asp:BoundField HeaderText="Lugar" DataField="PlaceDispatch" />
                        <asp:BoundField HeaderText="CP" DataField="ZipCode" />

                        <asp:TemplateField HeaderText="Des/Activar">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="Tooltip1" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editdelete.png' alt='Deactivate' /> para<br />desactivar.<br /><br />Hacer click en la imagen <img src='../Includes/Images/png/apply.png' alt='Activate'/> para<br />activar." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='<%#Eval("Active").ToString() == "1" ? "Desactivar lugar de expedición" : "Activar lugar de expedición"%>'>
                                    <asp:ImageButton ID="ImgBtnActive" runat="server" CommandArgument='<%# Eval("PlaceDispatchID") %>'
                                        CausesValidation="False" OnClick="ImgbtnActive_Click" ImageUrl='<%# GetActiveImageState(Eval("Active"))%>'>
                                    </asp:ImageButton></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                </asp:GridView>
                <asp:Button ID="btnAdd2" runat="server" Text="Agregar Lugar" CssClass="Button" OnClick="btnAdd_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
