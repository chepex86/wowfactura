﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Accesspac.aspx.cs"
    Inherits="PACFDManager.AccessPac.Accesspac" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe60">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Acceso a Pac" />
                            </h2>
                        </div>
                        <ul>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblUsername" runat="server" Text="Nombre de Usuario:" />
                                </label>
                                <span>
                                    <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtUsername" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblPassword" runat="server" Text="Contraseña:" />
                                </label>
                                <span>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="50" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblRepassword" runat="server" Text="Repetir Contraseña:" />
                                </label>
                                <span>
                                    <asp:TextBox ID="txtRepassword" runat="server" TextMode="Password" MaxLength="50" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvRepassword" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtRepassword" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvPassword" runat="server" ErrorMessage="La contraseña es distinta"
                                        ControlToCompare="txtPassword" ControlToValidate="txtRepassword" Display="Dynamic"
                                        ValidationGroup="Validators"></asp:CompareValidator>
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblFileType" runat="server" Text="Tipo de Archivo:" />
                                </label>
                                <span>
                                    <asp:TextBox ID="txtFileType" runat="server" MaxLength="20" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvFileType" runat="server" ErrorMessage="Este campo es requerido"
                                        ValidationGroup="Validators" ControlToValidate="txtFileType" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblUrl" runat="server" Text="Ingresar Url:" />
                                </label>
                                <span>
                                    <asp:TextBox ID="txtUrl" runat="server" Width="400px" MaxLength="150" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtUrl" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li class="buttons">
                                <asp:Button ID="btnAccept" runat="server" CssClass="Button" Text="Aceptar" ValidationGroup="Validators"
                                    OnClick="btnAccept_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Cancelar" CausesValidation="False"
                                    PostBackUrl="~/Default.aspx" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
