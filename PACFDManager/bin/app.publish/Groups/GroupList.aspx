<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="GroupList.aspx.cs" Inherits="PACFDManager.Groups.GroupList" %>

<%@ Register Src="GroupSearch.ascx" TagName="GroupSearch" TagPrefix="uc2" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc3" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <uc2:GroupSearch ID="GroupSearch1" runat="server" />
    <br />
    <br />
    <div class="txt_left">
        <div class="info">
            <h2>
                <asp:Label ID="lblTitleOptions" runat="server" Text="Listado de Grupos"></asp:Label>
            </h2>
        </div>
        <asp:Button ID="btnAddTop" runat="server" Text="Agregar grupo" PostBackUrl="~/Groups/groupAdd.aspx"
            CssClass="Button" />
        <span>
            <asp:GridView ID="gvGroupList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                ForeColor="#333333" CssClass="DataGrid" Font-Size="9pt" HorizontalAlign="Center">
                <RowStyle BackColor="#EFF3FB" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="GroupID" HeaderText="ID" />
                    <asp:BoundField DataField="Name" HeaderText="Nombre del grupo" ItemStyle-HorizontalAlign="Left">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Detalles">
                        <HeaderTemplate>
                            <uc1:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Detalles' /> para ver<br />los detalles del grupo seleccionado." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Ver detalles del grupo<br /><b><%#Eval("Name")%></b>'>
                                <asp:ImageButton ID="lbtnDetail" runat="server" CausesValidation="False" CommandArgument='<%# Eval("GroupID")%>'
                                    ImageUrl="~/Includes/Images/png/kfind.png" OnClick="lbtnDetail_Click" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Editar">
                        <HeaderTemplate>
                            <uc1:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar el grupo seleccionado." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Modificar el grupo<br /><b><%#Eval("Name")%></b>'>
                                <asp:ImageButton ID="lbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("GroupID")%>'
                                    ImageUrl="~/Includes/Images/png/editIcon.png" OnClick="lbtnEdit_Click" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eliminar">
                        <HeaderTemplate>
                            <uc1:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar el grupo seleccionado." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Eliminar el grupo<br /><b><%#Eval("Name")%></b>'>
                                <asp:ImageButton ID="lbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("GroupID")%>'
                                    ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="lbtnDelete_Click" Style="height: 20px" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
        </span>
        <asp:Button ID="btnAddBottom" runat="server" Text="Agregar grupo" PostBackUrl="~/Groups/groupAdd.aspx"
            CssClass="Button" />
    </div>
    <div>
        <uc3:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
    </div>
</asp:Content>
