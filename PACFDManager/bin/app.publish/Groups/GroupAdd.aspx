﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="GroupAdd.aspx.cs" Inherits="PACFDManager.Groups.GroupAdd" %>
<%@ Register src="GroupEditor.ascx" tagname="GroupEditor" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <uc1:GroupEditor ID="GroupEditor1" runat="server" CurrentGroupEditorMode=Add />
</asp:Content>
