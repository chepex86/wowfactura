﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="GroupModify.aspx.cs" Inherits="PACFDManager.Groups.GroupModify" %>

<%@ Register Src="GroupEditor.ascx" TagName="GroupEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <uc1:GroupEditor ID="GroupEditor1" runat="server" CurrentGroupEditorMode="Edit" />
</asp:Content>
