﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="InvoiceReport.aspx.cs"
    Inherits="PACFDManager.Reports.InvoiceReport" EnableEventValidation="false"  %>

<%@ Register Src="ReportViewer.ascx" TagName="ReportViewer" TagPrefix="uc1" %>
<%@ Register Src="SearchFilter.ascx" TagName="SearchFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Reporte de Comprobantes"></asp:Label>
                    </h2>
                </div>
                <div>
                    <uc2:SearchFilter ID="SearchFilter1" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div>
        <uc1:ReportViewer ID="ReportViewer1" runat="server" FileName="~/Reports/CrystalReports/crInvoiceReport.rpt" />
    </div>
</asp:Content>
