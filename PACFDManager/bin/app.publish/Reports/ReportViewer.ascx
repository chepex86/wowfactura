﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.ascx.cs"
    Inherits="PACFDManager.Reports.ReportViewer" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<table style="height: 100%;">
    <tr>
        <td align="right">
            <span class="vtip" title="Selecciona el tipo de archivo al que<br />se exportara el reporte generado.<br /><br />Excel<b>(*.xls)</b>, PDF<b>(*.pdf)</b>,<br />Texto Enriquecido<b>(*.txt)</b>, Word<b>(*.doc)</b>">
                <asp:DropDownList ID="ddlTypes" runat="server">
                    <asp:ListItem Text="Excel" Value="4"></asp:ListItem>
                    <asp:ListItem Text="PDF" Value="5" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="Texto Enriquecido" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Word" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </span>
            <asp:Button ID="btnExport" runat="server" Text="Exportar" CssClass="Button" OnClick="btnExport_Click">
            </asp:Button>
        </td>
    </tr>
    <tr>
        <td align="left">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" HasCrystalLogo="False"
                HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasSearchButton="False"
                HasToggleGroupTreeButton="False" Height="50px" SeparatePages="False"
                Width="350px" DisplayToolbar="False" EnableDrillDown="False" />
        </td>
    </tr>
    <tr>
        <td>
            <CR:CrystalReportSource ID="CRS1" runat="server" CacheDuration="240000">
                <Report>
                    <Parameters>
                        <CR:Parameter ConvertEmptyStringToNull="False" DefaultValue="" Name="TextFilter"
                            ReportName="" />
                    </Parameters>
                </Report>
            </CR:CrystalReportSource>
        </td>
    </tr>
</table>
