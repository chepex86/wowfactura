﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="ProviderMonthlyReportDetails.aspx.cs"
    Inherits="PACFDManager.Reports.ProviderMonthlyReport.ProviderMonthlyReportDetails" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer">
                <tr>
                    <td align="center" class="Titulo" colspan="2">
                        <asp:Label ID="lblTitle" runat="server" Text="Detalle del Reporte"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblTitleReportName" runat="server" Text="Nombre:" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblTitleCreationDate" runat="server" Text="Fecha de Creación:" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblCreationDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblTitleMonth" runat="server" Text="Mes:" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblMonth" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblTitleYear" runat="server" Text="Año:" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <br />
                        <asp:TextBox ID="txtContent" runat="server" Height="124px" ReadOnly="True" TextMode="MultiLine"
                            Width="282px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td align="center" class="Titulo">
                                    <asp:Label ID="lblDownload" runat="server" Text="Descargar Reporte"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:ImageButton ID="imgBtnDownload" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                        OnClick="imgBtnDownload_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Titulo">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <style type="text/css">

                                    a
                                    {
                                        color: #0066AC;
                                    }
                                    a:hover
                                    {
                                        color: #FF6600;
                                    }
                                </style>
                                <td align="center">
                                    <br />
                                    <asp:Button ID="btnReturn" runat="server" CssClass="Button" 
                                        PostBackUrl="~/Reports/ProviderMonthlyReport/ProviderMonthlyReportList.aspx" 
                                        Text="Regresar" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
