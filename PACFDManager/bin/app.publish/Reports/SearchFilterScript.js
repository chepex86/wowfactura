﻿var sFilter = new jclassSearchFilter();

function jclassSearchFilter() {
    this.resetControls = function(ddlConsumer, ddlStatus, ddlCFDState, ddlIsCredit, ddlProductTypes, ddlProductDescription, txtDateStart, txtDateEnd, chkBoxIncludeDates) {
        var Consumer = document.getElementById(ddlConsumer);
        var Status = document.getElementById(ddlStatus);
        var CFDState = document.getElementById(ddlCFDState);
        var IsCredit = document.getElementById(ddlIsCredit);
        var ProductTypes = document.getElementById(ddlProductTypes);
        var ProductDescription = document.getElementById(ddlProductDescription);
        var DateStart = document.getElementById(txtDateStart);
        var DateEnd = document.getElementById(txtDateEnd);
        var chkBox = document.getElementById(chkBoxIncludeDates);

        Consumer.options[0].selected = true;
        Status.options[1].selected = true;
        CFDState.options[0].selected = true;
        IsCredit.options[0].selected = true;
        ProductTypes.options[0].selected = true;
        ProductDescription.options[0].selected = true;

        fec = new Date;
        dia = fec.getDate() < 10 ? '0' + fec.getDate() : fec.getDate();
        mes = fec.getMonth() < 10 ? '0' + fec.getMonth() : fec.getMonth();
        ano = fec.getFullYear();
        dStart = (dia - 1) + '/' + mes + '/' + ano;
        dEnd = dia + '/' + mes + '/' + ano;
        DateStart.value = dStart;
        DateEnd.value = dEnd;

        if (!chkBox.checked) {
            chkBox.click();
        }
    }

    this.DisableValidator = function(chkBoxIncludeDates, rfvStartDate, rfvEndDate) {
        this.Hide(chkBoxIncludeDates);
        var chkBox = document.getElementById(chkBoxIncludeDates);
        var b;
        b = chkBox.checked ? true : false;

        this.onOff(rfvStartDate, b);
        this.onOff(rfvEndDate, b);
    }

    this.Hide = function(chkBoxIncludeDates) {
        var li = document.getElementById('dvDates');
        var chkBox = document.getElementById(chkBoxIncludeDates);
        if (li != null && chkBox != null) {
            li.style.display = chkBox.checked ? 'block' : 'none';
        }
    }

    this.onOff = function(validatorId, active) {
        var validator = document.getElementById(validatorId);
        ValidatorEnable(validator, active);
    }

    this.preventBackspace = function(e) {
        var evt = e || window.event;
        if (evt) {
            var keyCode = evt.charCode || evt.keyCode;
            if (keyCode === 8 || keyCode === 46) {
                if (evt.preventDefault) {
                    evt.preventDefault();
                } else {
                    evt.returnValue = false;
                }
            }
        }
    }

    this.onload = function(chkBoxIncludeDates) {
        this.Hide(chkBoxIncludeDates);
    }
}