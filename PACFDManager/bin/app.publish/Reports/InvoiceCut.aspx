﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="InvoiceCut.aspx.cs" Inherits="PACFDManager.Reports.InvoiceCut" %>

<%@ Register Src="InvoiceCutSearch.ascx" TagName="InvoiceCutSearch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:InvoiceCutSearch ID="InvoiceCutSearch1" runat="server" OnSearch="InvoiceCutSearch1_Search" />
            <br />
            <div class="formStyles">
                <div class="info">
                    <label class="desc">
                        <h2>
                            Corte de facturas
                        </h2>
                    </label>
                </div>
            </div>
            <div class="form">
                <div class="formStyles">
                    <ul>
                        <span id="aDownloadXls" style="width: 100%; text-align: right;" runat="server"><a
                            href="<%= string.Format("DownloadExcel.aspx?downloadtype=invoicecut{0}", this.GetSelectedGroup()) %>"
                            class="Button">
                            <img alt="xls" src="../Includes/Images/png/excel.png" />
                            Descargar Xls. </a></span>
                        <br />
                    </ul>
                </div>
            </div>
            <asp:GridView ID="gdvInvoiceCut" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                AllowPaging="true" PageSize="50" OnPageIndexChanging="gdvInvoiceCut_PageIndexChanging">
                <HeaderStyle CssClass="dgHeader" />
                <PagerSettings Position="TopAndBottom" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
                <EmptyDataTemplate>
                    No hay comprobantes disponibles.
                </EmptyDataTemplate>
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" />
                    <asp:BoundField DataField="Emisor" HeaderText="Emisor" />
                    <asp:BoundField DataField="Type" HeaderText="Tipo" />
                    <asp:BoundField DataField="RfcEmisor" HeaderText="RFC Ems." />
                    <asp:BoundField DataField="Receptor" HeaderText="Cliente" />
                    <asp:BoundField DataField="RfcReceptor" HeaderText="RFC Cln." />
                    <asp:BoundField DataField="Folio" HeaderText="Folio" />
                    <asp:BoundField DataField="UUID" HeaderText="UUID" />
                    <asp:BoundField DataField="ExternalFolio" HeaderText="Folio Extr." />
                    <asp:BoundField DataField="Total" HeaderText="Total" />
                    <asp:BoundField DataField="Money" HeaderText="Moneda" />
                    <asp:BoundField DataField="Date" HeaderText="Fecha" />
                    <asp:BoundField DataField="Status" HeaderText="Estado" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
