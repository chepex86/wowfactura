﻿<%@ Page Title="Listado de Productos" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="ConceptsList.aspx.cs" Inherits="PACFDManager.Concepts.ConceptsList" %>

<%@ Register Src="ConceptsSearch.ascx" TagName="ConceptsSearch" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc4" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:ConceptsSearch ID="ConceptsSearch1" runat="server" OnSearch="ConceptsSearch1_Search" />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        Listado de Productos
                    </h2>
                </div>
                <asp:Button ID="btnAddConcepts" runat="server" CssClass="Button" PostBackUrl="~/Concepts/ConceptsAdd.aspx"
                    Text="Agregar Producto" />
                <asp:GridView ID="gdvConcepts" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    OnPageIndexChanging="gdvConcepts_PageIndexChanging" ForeColor="#333333" GridLines="None"
                    HorizontalAlign="Center" AllowPaging="True" AllowSorting="True" EnableSortingAndPagingCallbacks="True"
                    PageSize="100">
                    <Columns>
                        <asp:BoundField DataField="ConceptID" HeaderText="ID" />
                        <asp:BoundField DataField="Code" HeaderText="Clave" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="UMC">
                            <HeaderTemplate>
                                <span class="vtip" style="cursor: help" title='<b>UMC</b>. Unidad de Medida de Comercialización.'>
                                    UMC </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.IsNullOrEmpty(Eval("UnitType").ToString()) ? "[Sin Asignar]" :  Eval("UnitType").ToString() %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Description" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Precio Unitario">
                            <HeaderTemplate>
                                Precio Unitario
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" style="cursor: help" title='<%#Eval("AppliesTax").ToString() == "True" ? "Aplica Impuesto" : "No Aplica Impuesto"%>'>
                                    <%# String.Format("${0:#,#.00}", Eval("UnitPrice"))  %>
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TaxRatePercentage" DataFormatString="{0:#,#.00 '%'}" ItemStyle-HorizontalAlign="Right"
                            HeaderText="Impuesto" HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Detalles">
                            <HeaderTemplate>
                                <uc4:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Detalles' /> para ver<br />los detalles del producto seleccionado." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Ver detalles del producto<br />con clave <b><%#Eval("Code")%></b>.'>
                                    <asp:ImageButton ID="imgbtnDetail" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ConceptID") %>'
                                        OnClick="imgbtnConcepts_Click" ImageUrl="~/Includes/Images/png/kfind.png" /></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar">
                            <HeaderTemplate>
                                <uc4:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar el producto seleccionado." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Modificar el producto<br />con clave <b><%#Eval("Code")%></b>.'>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ConceptID") %>'
                                        OnClick="imgbtnConcepts_Click" ImageUrl="~/Includes/Images/png/editIcon.png" /></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Eliminar">
                            <HeaderTemplate>
                                <uc4:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar el producto seleccionado." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Eliminar el producto<br />con clave <b><%#Eval("Code")%></b>'>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ConceptID") %>'
                                        OnClick="imgbtnConcepts_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png" /></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <RowStyle BackColor="#EFF3FB" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:Button ID="btnAddConcepts2" runat="server" CssClass="Button" PostBackUrl="~/Concepts/ConceptsAdd.aspx"
                    Text="Agregar Producto" />
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
