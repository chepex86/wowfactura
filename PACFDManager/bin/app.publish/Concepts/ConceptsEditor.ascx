﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConceptsEditor.ascx.cs"
    Inherits="PACFDManager.Concepts.ConceptsEditor" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript" language="javascript">
    function ReadOnly() {
        var hidden = document.getElementById('<%=hfReadOnly.ClientID%>');

        if (hidden.value == '1') {
            return;
        }
    }
</script>

<asp:Panel ID="pnlConceptsOptions" runat="server" Visible="False">
    <div id="container" class="wframe60">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleOptions" runat="server" Text="Titulo"></asp:Label>
                    </h2>
                </div>
                <ul>
                    <li class="">
                        <label class="desc">
                            <asp:Label ID="lblCode" runat="server" Text="Clave:"></asp:Label></label>
                        <span class="vtip" title='<%= ToolTipMode() == "True" ? "Muestra la clave asignada al producto,<br />permite 10 caracteres alfanuméricos.<br />ej:<b>000001ABC3</b> " : "Introduce una clave de 10 caracteres alfanuméricos.<br /> ej: <b>000001ABC3</b>"%>'>
                            <asp:TextBox ID="txtCode" runat="server" Width="95px" MaxLength="10"></asp:TextBox></span>
                        <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li class="">
                        <label class="desc">
                            <asp:Label ID="lblType" runat="server" Text="UMC:"></asp:Label>
                        </label>
                        <span class="vtip" title='<%= ToolTipMode() == "True" ? "Muestra el tipo de UMC asignada a este producto.<br /><b>UMC</b> (Unidad de Medida de Comercialización)" : "Selecciona el tipo de UMC (Unidad de Medida de Comercialización)<br />que más se relacione con el producto."%>'>
                            <asp:TextBox ID="txtProducTypesSearch" runat="server" Width="373px" MaxLength="255" />
                            <asp:AutoCompleteExtender ID="acProductTypes" runat="server"
                                TargetControlID="txtProducTypesSearch" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetClaveUnidadByName"
                                MinimumPrefixLength="2" EnableCaching="true" UseContextKey="True">
                            </asp:AutoCompleteExtender>
                        </span>
                  <%--      <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="ddlProductTypes_RequiredFieldValidator" runat="server"
                                ControlToValidate="ddlProductTypes" Display="Dynamic" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>--%>
                    </li>
                    <li class="">
                        <label class="desc">
                            <asp:Label ID="Label1" runat="server" Text="Clave producto:"></asp:Label>
                        </label>
                        <span class="vtip" title='<%= ToolTipMode() == "True" ? "Muestra la clave de producto o servicio)" : "Selecciona la clave o producto <br />que más se relacione con el producto."%>'>
                            <asp:TextBox ID="txtClaveSearch" runat="server" Width="373px" MaxLength="255" />
                            <asp:AutoCompleteExtender ID="acClavProdSer" runat="server"
                                TargetControlID="txtClaveSearch" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetClaveProdByName"
                                MinimumPrefixLength="2" EnableCaching="true" UseContextKey="True">
                            </asp:AutoCompleteExtender>
                        </span>
                     <%--   <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="ddlClavProdSer_RequiredFieldValidator1" runat="server"
                                ControlToValidate="ddlClavProdSer" Display="Dynamic" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>--%>
                    </li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lblDescription" runat="server" Text="Descripción:"></asp:Label></label>
                        <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra la descripción del producto, la cual<br />permite un maximo de 255 caracteres." : "Introduce una descripción que coincida con el producto."%>'>
                            <asp:TextBox ID="txtDescription" runat="server" Height="100px" MaxLength="1000" TextMode="MultiLine"
                                Width="300px"></asp:TextBox><%--onpaste="return false"--%>
                        </span>
                        <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
                                Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clear">
                        </div>
                    </li>
                    <li><span>
                        <asp:Label ID="lblCountTitle" runat="server" Text="Caracteres restantes:" Visible="False"></asp:Label>
                    </span><span>
                        <asp:Label ID="lblCount" runat="server" Visible="False"></asp:Label>
                    </span></li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lblUnitPrice" runat="server" Text="Precio Unitario:"></asp:Label></label>
                        <span class="vtip" title='<%= ToolTipMode() == "True" ? "Muestra el precio unitario asignado al producto.<br /><br />ej: <b>249</b> ó <b>249.99</b> (Valores correctos)<br /> <b>249.99.99</b> (Valor incorrecto)" : "Introduce solo números, en caso de que el número<br />contenga decimales, puede introducir un [<b>.</b>] (punto).<br /><br />ej: <b>249</b> ó <b>249.99</b> (Valores correctos)<br /> <b>249.99.99</b> (Valor incorrecto)"%>'>
                            <asp:TextBox ID="txtUnitPrice" runat="server" MaxLength="13" Width="95px"></asp:TextBox></span>
                        <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="rfvUnitPrice" runat="server" ControlToValidate="txtUnitPrice"
                                Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revUnitPrice" runat="server" ControlToValidate="txtUnitPrice"
                                ErrorMessage="Formato Incorrecto" ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$"
                                ValidationGroup="Validators" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </li>
                    <li id="li3" runat="server" visible="false">
                        <asp:RequiredFieldValidator ID="rfvTax" runat="server" ControlToValidate="txtTax"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators" Visible="False"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblTax" runat="server" Text="Impuesto:" Visible="False"></asp:Label></label>
                        <asp:HiddenField ID="hfTax" runat="server" />
                        <span>
                            <asp:TextBox ID="txtTax" onkeypress="javascript:OnlyNumbers();" runat="server" Width="100px"
                                Visible="False"></asp:TextBox></span>
                        <asp:RegularExpressionValidator ID="revTax" runat="server" ControlToValidate="txtTax"
                            Display="Dynamic" ErrorMessage="Formato Incorrecto" ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$"
                            ValidationGroup="Validators"></asp:RegularExpressionValidator>
                    </li>
                    <li class="">
                        <div class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra si este producto aplica o no aplica impuesto." : "Marca la casilla si este producto aplica impuesto,&lt;br /&gt;de lo contrario desmarca la casilla (si esta se encuentra marcada)."%>'>
                            <img id="imgBox" runat="server" visible="false" />
                            <div class="checkBox">
                                <asp:CheckBox ID="chkBoxTax" runat="server" Text="Aplicar Impuesto" Checked="True" />
                            </div>
                        </div>
                        <asp:HiddenField ID="hfReadOnly" runat="server" />
                        <asp:HiddenField ID="hfRegScript" runat="server" />
                    </li>
                    <li id="li1" runat="server" visible="false"><span>
                        <asp:DropDownList ID="ddlTaxTemplates" runat="server" OnSelectedIndexChanged="ddlTaxTemplates_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span><span>
                        <uc4:Tooltip ID="tipTaxTemplates" runat="server" ToolTip="Selecciona un Template para visualizar<br />como le afectara al precio el impuesto." />
                    </span></li>
                    <li id="li2" runat="server" visible="false">
                        <label class="desc">
                            <asp:Label ID="lblTotal" runat="server" Text="Precio con Impuesto:"></asp:Label></label>
                        <span>
                            <asp:TextBox ID="txtTotal" runat="server" ReadOnly="True" Width="100px"></asp:TextBox></span>
                        <span>
                            <uc4:Tooltip ID="tipTotal" runat="server" ToolTip="Precio unitario afectado por el impuesto (si es que este aplica)." />
                        </span></li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblAccount" runat="server" Text="Cuenta contable del Producto:"></asp:Label></label>
                        <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra el número de cuenta contable de banco (si se tiene una).<br /><b>Nota:</b> Este campo es requerido para la exportación a los programas<br />contables como <i>ContPaq, ContPaq I, Aspel COI.</i>" : "Introduce un número de cuenta contable de banco (si se tiene una).<br />ej: <b>1100000700</b><br /><br /><b>Nota:</b> Este campo es requerido para la exportación a los programas<br />contables como <i>ContPaq, ContPaq I, Aspel COI.</i>" %>'>
                            <asp:TextBox ID="txtAccount" runat="server" MaxLength="20" Width="210px"></asp:TextBox></span>
                        <asp:RegularExpressionValidator ID="revAccount" runat="server" ControlToValidate="txtAccount"
                            Display="Dynamic" ErrorMessage="Solo números" ValidationExpression="\d+" ValidationGroup="Validators"></asp:RegularExpressionValidator>
                    </li>
                    <li class="buttons">
                        <div>
                            <asp:Button ID="btnAccept" runat="server" OnClick="btnAccept_Click" CssClass="Button"
                                Text="Aceptar" ValidationGroup="Validators" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" CssClass="Button"
                                Text="Cancelar" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Panel>
