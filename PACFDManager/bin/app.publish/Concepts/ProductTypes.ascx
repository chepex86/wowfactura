﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTypes.ascx.cs"
    Inherits="PACFDManager.Concepts.ProductTypes" %>
<asp:DropDownList ID="ddlProductTypes" runat="server">
    <asp:ListItem Selected="True" Value="-1">[Todos]</asp:ListItem>
    <asp:ListItem>[Sin Asignar]</asp:ListItem>
    <asp:ListItem>Acarreos</asp:ListItem>
    <asp:ListItem>Barril</asp:ListItem>
    <asp:ListItem>Botella</asp:ListItem>
    <asp:ListItem>Bote</asp:ListItem>
    <asp:ListItem>Cabeza</asp:ListItem>
    <asp:ListItem>Caja</asp:ListItem>
    <asp:ListItem>Carga</asp:ListItem>
    <asp:ListItem>Cientos</asp:ListItem>
    <asp:ListItem>Decenas</asp:ListItem>
    <asp:ListItem>Dia</asp:ListItem>
    <asp:ListItem>Docenas</asp:ListItem>
    <asp:ListItem>Gramo</asp:ListItem>
    <asp:ListItem>Gramo Neto</asp:ListItem>
    <asp:ListItem>Hrs</asp:ListItem>
    <asp:ListItem>Juego</asp:ListItem>
    <asp:ListItem>Kilo</asp:ListItem>
    <asp:ListItem>Kilowatt</asp:ListItem>
    <asp:ListItem>Kilowatt/Hora</asp:ListItem>
    <asp:ListItem>Litro</asp:ListItem>
    <asp:ListItem>Metro Cuadrado</asp:ListItem>
    <asp:ListItem>M2</asp:ListItem>
    <asp:ListItem>Metro Cubico</asp:ListItem>
    <asp:ListItem>M3</asp:ListItem>
    <asp:ListItem>Metro Lineal</asp:ListItem>    
    <asp:ListItem>Millar</asp:ListItem>
    <asp:ListItem>Paquete</asp:ListItem>
    <asp:ListItem>Par</asp:ListItem>
    <asp:ListItem>Pieza</asp:ListItem>
    <asp:ListItem>Renta</asp:ListItem>
    <asp:ListItem>Rollo</asp:ListItem>
    <asp:ListItem>Saco</asp:ListItem>
    <asp:ListItem>Servicio</asp:ListItem>
    <asp:ListItem>Tmo</asp:ListItem>
    <asp:ListItem>Tonelada</asp:ListItem>
    <asp:ListItem>Viajes</asp:ListItem>
</asp:DropDownList>
