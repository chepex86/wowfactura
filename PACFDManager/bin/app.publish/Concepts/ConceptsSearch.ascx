﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConceptsSearch.ascx.cs"
    Inherits="PACFDManager.Concepts.ConceptsSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ProductTypes.ascx" TagName="ProductTypes" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>

<script src="ConceptsSearchScripts.js" type="text/javascript"></script>

<div id="container" class="wframe80">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitle" runat="server" Text="Búsqueda de Productos" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByCode" runat="server" Text="Por Clave:"></asp:Label>
                    </label>
                    <span class="vtip" title="Escribe la clave del producto o una parte de ella<br />(el <i>autocompletado</i> cargara los resultados que<br />coincidan con el texto introducido).<br /><br />Si no quieres incluir la clave del producto<br />en la búsqueda da click en el botón <b>Limpiar</b>.">
                        <asp:TextBox ID="txtSearchByCode" runat="server" Width="200px" />
                    </span><span>
                        <asp:AutoCompleteExtender ID="txtSearchByCode_AutoCompleteExtender" runat="server"
                            BehaviorID="aceByCode" CompletionInterval="1000" DelimiterCharacters=";" CompletionSetCount="10"
                            FirstRowSelected="true" EnableCaching="true" UseContextKey="True" ServicePath="~/AutoComplete.asmx"
                            TargetControlID="txtSearchByCode" MinimumPrefixLength="1" ServiceMethod="GetConceptsByCode">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByCode" runat="server" style="display: none;">
                        <img runat="server" id="imgLoading_ByCode" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByType" runat="server" Text="Por Tipo:"></asp:Label>
                    </label>
                    <span class="vtip" title="Selecciona el <b>tipo</b> de <b>UMC</b> que necesites buscar, o selecciona<br /><b>Todos</b> para incluir todos los tipos en la búsqueda.<br /><br /><b>* UMC(Unidad de Medida de Comercialización)</b>">
                        <uc1:ProductTypes ID="ddlSearchByType" runat="server" />
                    </span><span></span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByDescription" runat="server" Text="Por Descripción:"></asp:Label>
                    </label>
                    <span class="vtip" title="Escribe la descripción del producto o una parte de ella<br />(el <i>autocompletado</i> cargara los resultados que<br />coincidan con el texto introducido).<br /><br />Si no quieres incluir la descripción del producto<br />en la búsqueda da click en el botón <b>Limpiar</b>.">
                        <asp:TextBox ID="txtSearchByDescription" runat="server" MaxLength="255" Width="200px" />
                        <asp:AutoCompleteExtender ID="txtSearchByDescription_AutoCompleteExtender" runat="server"
                            BehaviorID="aceByDescription" CompletionInterval="1000" DelimiterCharacters=";"
                            CompletionSetCount="10" FirstRowSelected="true" EnableCaching="true" UseContextKey="True"
                            ServicePath="~/AutoComplete.asmx" TargetControlID="txtSearchByDescription" MinimumPrefixLength="1"
                            ServiceMethod="GetConceptsByDescription">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByDesc" runat="server" style="display: none;">
                        <img runat="server" id="imgLoading_ByDescription" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <div>
                        <asp:Button ID="btnSearch" runat="server" Text="Buscar" CssClass="Button_Search"
                            OnClick="btnSearch_Click" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
