﻿--Cambios Version

USE PACFD

Alter table pacfd..systemconfiguration
add  CFDI4_0Enable bit;
GO

update PACFD..SystemConfiguration set CFDI4_0Enable = 1;
GO
GO

ALTER PROCEDURE [dbo].[spSystemConfiguration_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SystemConfigurationID],
		[LatestNewsReview],
		[CFDEnable],
		[CBBEnable],
		[CFDIEnable],
		[LatestRevisionExchangeRate],
		[ExchangeRate],
		[Url],
		[Username],
		[Password],
		[fileType],
		[CFD2_2Enable],
		[CFDI3_2Enable],
		[CFDI3_3Enable],
		[CFDI4_0Enable]
	FROM [SystemConfiguration]

	SET @Err = @@Error

	RETURN @Err
END
GO
ALTER PROCEDURE [dbo].[spSystemConfiguration_GetByID]
(
	@SystemConfigurationID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[SystemConfigurationID],
		[LatestNewsReview],
		[CFDEnable],
		[CBBEnable],
		[CFDIEnable],
		[LatestRevisionExchangeRate],
		[ExchangeRate],
		[Url],
		[Username],
		[Password],
		[fileType],
		[CFD2_2Enable],
		[CFDI3_2Enable],
		[CFDI3_3Enable],
		[CFDI4_0Enable]
	FROM [SystemConfiguration]
	WHERE
		([SystemConfigurationID] = @SystemConfigurationID)

	SET @Err = @@Error

	RETURN @Err
END
GO

-- Procedure
ALTER PROCEDURE [dbo].[spSystemConfiguration_Insert]
(
	@SystemConfigurationID int,
	@LatestNewsReview datetime,
	@CFDEnable bit = NULL,
	@CBBEnable bit = NULL,
	@CFDIEnable bit = NULL,
	@LatestRevisionExchangeRate datetime = NULL,
	@ExchangeRate money = NULL,
	@Url nchar(150) = NULL,
	@Username nchar(50) = NULL,
	@Password nchar(50) = NULL,
	@fileType nvarchar(20) = NULL,
	@CFD2_2Enable	bit = NULL,
	@CFDI3_2Enable	bit = NULL,
	@CFDI3_3Enable	bit = NULL,
	@CFDI4_0Enable	bit = NULL

)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [SystemConfiguration]
	(
		[SystemConfigurationID],
		[LatestNewsReview],
		[CFDEnable],
		[CBBEnable],
		[CFDIEnable],
		[LatestRevisionExchangeRate],
		[ExchangeRate],
		[Url],
		[Username],
		[Password],
		[fileType],
		[CFD2_2Enable],
		[CFDI3_2Enable],
		[CFDI3_3Enable],
		[CFDI4_0Enable]

	)
	VALUES
	(
		@SystemConfigurationID,
		@LatestNewsReview,
		@CFDEnable,
		@CBBEnable,
		@CFDIEnable,
		@LatestRevisionExchangeRate,
		@ExchangeRate,
		@Url,
		@Username,
		@Password,
		@fileType,
		@CFD2_2Enable,
		@CFDI3_2Enable,
		@CFDI3_3Enable,
		@CFDI4_0Enable
	)

	

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[SystemConfigurationID],
		[LatestNewsReview],
		[CFDEnable],
		[CBBEnable],
		[CFDIEnable],
		[LatestRevisionExchangeRate],
		[ExchangeRate],
		[Url],
		[Username],
		[Password],
		[fileType],
		[CFD2_2Enable],
		[CFDI3_2Enable],
		[CFDI3_3Enable],
		[CFDI4_0Enable]
	FROM [SystemConfiguration]
	WHERE
		([SystemConfigurationID] = @SystemConfigurationID)

	SET @Err = @@Error
	
RETURN @Err
END


GO
ALTER PROCEDURE[dbo].[spSystemConfiguration_GetAll]
AS
BEGIN

SET NOCOUNT ON
DECLARE @Err int

SELECT
[SystemConfigurationID],
	[LatestNewsReview],
	[CFDEnable],
	[CBBEnable],
	[CFDIEnable],
	[LatestRevisionExchangeRate],
	[ExchangeRate],
	[Url],
	[Username],
	[Password],
	[fileType],
	[CFD2_2Enable],
	[CFDI3_2Enable],
	[CFDI3_3Enable],
	[CFDI4_0Enable]
FROM[SystemConfiguration]

SET @Err = @@Error

RETURN @Err
END

GO

-- Procedure
ALTER PROCEDURE [dbo].[spSystemConfiguration_Update]
(
	@SystemConfigurationID int,
	@LatestNewsReview datetime,
	@CFDEnable bit = NULL,
	@CBBEnable bit = NULL,
	@CFDIEnable bit = NULL,
	@LatestRevisionExchangeRate datetime = NULL,
	@ExchangeRate money = NULL,
	@Url nchar(150) = NULL,
	@Username nchar(50) = NULL,
	@Password nchar(50) = NULL,
	@fileType nvarchar(20) = NULL,
	@CFD2_2Enable	bit = NULL,
	@CFDI3_2Enable	bit = NULL,
	@CFDI3_3Enable	bit = NULL,
	@CFDI4_0Enable	bit = NULL

)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [SystemConfiguration]
	SET
		[LatestNewsReview] = @LatestNewsReview,
		[CFDEnable] = @CFDEnable,
		[CBBEnable] = @CBBEnable,
		[CFDIEnable] = @CFDIEnable,
		[LatestRevisionExchangeRate] = @LatestRevisionExchangeRate,
		[ExchangeRate] = @ExchangeRate,
		[Url] = @Url,
		[Username] = @Username,
		[Password] = @Password,
		[fileType] = @fileType,
		[CFD2_2Enable] = @CFD2_2Enable,
		[CFDI3_2Enable] =@CFDI3_2Enable,
		[CFDI3_3Enable] = @CFDI3_3Enable,
		[CFDI4_0Enable] =@CFDI4_0Enable 
	WHERE
		[SystemConfigurationID] = @SystemConfigurationID


	SET @Err = @@Error

	RETURN @Err
END
GO

--<!--   RECEPTORS ....>>>>>>


ALTER TABLE Receptors
ADD FiscalRegime NVarChar(50);
GO

ALTER TABLE Receptors
ADD CFDIUse NVarChar(5);
GO

Update Receptors set CFDIUse=UsoCFDI
GO


ALTER PROCEDURE [dbo].[spReceptors_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]
	FROM [Receptors]

	SET @Err = @@Error

	RETURN @Err
END

GO




ALTER PROCEDURE [dbo].[spReceptors_GetByID]
(
	@ReceptorID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]
	FROM [Receptors]
	WHERE
		([ReceptorID] = @ReceptorID)

	SET @Err = @@Error

	RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spReceptors_Insert]
(
	@ReceptorID int = NULL OUTPUT,
	@BillerID int,
	@RFC nvarchar(13),
	@Name nvarchar(255),
	@Address nvarchar(255),
	@Phone nvarchar(50) = NULL,
	@ExternalNumber nvarchar(50),
	@InternalNumber nvarchar(50),
	@Colony nvarchar(255),
	@Location nvarchar(255),
	@Reference nvarchar(255) = NULL,
	@Municipality nvarchar(255) = NULL,
	@State nvarchar(255),
	@Country nvarchar(255),
	@Zipcode nvarchar(10),
	@Email nvarchar(50) = NULL,
	@CFDIUse nvarchar(5) = NULL,
	@FiscalRegime nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [Receptors]
	(
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUSe],
		[FiscalRegime]
	)
	VALUES
	(
		@BillerID,
		@RFC,
		@Name,
		@Address,
		@Phone,
		@ExternalNumber,
		@InternalNumber,
		@Colony,
		@Location,
		@Reference,
		@Municipality,
		@State,
		@Country,
		@Zipcode,
		@Email,
		@CFDIUse,
		@FiscalRegime
	)

	
	SELECT @ReceptorID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]

	FROM [Receptors]
	WHERE
		([ReceptorID] = @ReceptorID)

	SET @Err = @@Error
	
RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spReceptors_Update]
(
	@ReceptorID int,
	@BillerID int,
	@RFC nvarchar(13),
	@Name nvarchar(255),
	@Address nvarchar(255),
	@Phone nvarchar(50) = NULL,
	@ExternalNumber nvarchar(50),
	@InternalNumber nvarchar(50),
	@Colony nvarchar(255),
	@Location nvarchar(255),
	@Reference nvarchar(255) = NULL,
	@Municipality nvarchar(255) = NULL,
	@State nvarchar(255),
	@Country nvarchar(255),
	@Zipcode nvarchar(10),
	@Email nvarchar(50) = NULL,
	@CFDIUse nvarchar(5) = NULL,
	@FiscalRegime nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [Receptors]
	SET
		[BillerID] = @BillerID,
		[RFC] = @RFC,
		[Name] = @Name,
		[Address] = @Address,
		[Phone] = @Phone,
		[ExternalNumber] = @ExternalNumber,
		[InternalNumber] = @InternalNumber,
		[Colony] = @Colony,
		[Location] = @Location,
		[Reference] = @Reference,
		[Municipality] = @Municipality,
		[State] = @State,
		[Country] = @Country,
		[Zipcode] = @Zipcode,
		[Email] = @Email,
		[CFDIUse] = @CFDIUse,
		[FiscalRegime] = @FiscalRegime
	WHERE
		[ReceptorID] = @ReceptorID


	SET @Err = @@Error

	RETURN @Err
END
GO

ALTER PROCEDURE [dbo].[spReceptors_GetByBillersID]
(
	@BillersID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]
	FROM [Receptors]
	WHERE
		([BillerID] = @BillersID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spReceptors_GetByFkBillerID]
(
	@BillerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]
		FROM [Receptors]
	WHERE
		([BillerID] = @BillerID)

	SET @Err = @@Error

	RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spReceptors_GetByNameAndBillersID]
(
	@BillersID int,
	@Name nvarchar(255)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ReceptorID],
		[BillerID],
		[RFC],
		[Name],
		[Address],
		[Phone],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[Email],
		[CFDIUse],
		[FiscalRegime]
	FROM [Receptors]
	WHERE
		([BillerID] = @BillersID)
	AND 
		[Name] LIKE '%'+ @Name +'%'

	SET @Err = @@Error

	RETURN @Err
END

GO


--<--- BILLERS.. -->>>


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spBillings_BillingsReceptor_GetByBillerID]
(
	@BillerID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		b.[BillingID],
		b.[Active],
		b.[BillingDate],
		b.[Serial],
		b.[Folio],
		b.[FolioID],
		b.[Total],
		b.[PreBilling],
		b.[BillingType],
		r.[ReceptorAddress],
		r.[ReceptorName],
		r.[ReceptorRFC],
		r.[Zipcode],
		r.[TaxSystem]
	FROM [Billings] b
	INNER JOIN [BillingsReceptors] r ON b.[BillingID] = r.[BillingID]
	WHERE
		(b.[BillerID] = @BillerID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spBillers_GetForList] --[spBillers_GetForList] 1
(
	@GroupID int,
	@Active bit = null 
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

SELECT
	*
FROM
	(
		SELECT
			B.[BillerID],		
			B.[GroupID] ,
			B.[RFC],
			B.[Name],
			Isnull(B.[ElectronicBillingType],0) [ElectronicBillingType],
			Case
				when ISNULL(B.ElectronicBillingType,0) = 0 then 'CFD'
				when ISNULL(B.ElectronicBillingType,0) = 1 then 'CFDI'
				when ISNULL(B.ElectronicBillingType,0) = 2 then 'CBB'
				when ISNULL(B.ElectronicBillingType,0) = 3 then 'CFD2_2'
				when ISNULL(B.ElectronicBillingType,0) = 4 then 'CFDI3_2'
				when ISNULL(B.ElectronicBillingType,0) = 5 then 'CFDI3_3'
				when ISNULL(B.ElectronicBillingType,0) = 6 then 'CFDI4_0'
				Else '???'
			End  [ElectronicBillingTypeText],
			B.[CurrencyCode],
			CAST(-1 as int) [BranchID],
			CAST ('' AS nvarchar) BranchName
			,CAST ('' AS nvarchar) BranchCode
		FROM 
			[Billers] B 
			
		WHERE
			(@GroupID = GroupID) AND
			 ((@Active IS NULL) OR (ISNULL(B.[Active], 1) = @Active)) 
			 
			 
		UNION ALL
		
		
		SELECT
			B.[BillerID],		
			B.[GroupID] ,
			B.[RFC],
			B.[Name],
			Isnull(B.[ElectronicBillingType],0) [ElectronicBillingType],
			Case
				when ISNULL(B.ElectronicBillingType,0) = 0 then 'CFD'
				when ISNULL(B.ElectronicBillingType,0) = 1 then 'CFDI'
				when ISNULL(B.ElectronicBillingType,0) = 2 then 'CBB'
				when ISNULL(B.ElectronicBillingType,0) = 3 then 'CFD2_2'
				when ISNULL(B.ElectronicBillingType,0) = 4 then 'CFDI3_2'
				when ISNULL(B.ElectronicBillingType,0) = 5 then 'CFDI3_3'
				when ISNULL(B.ElectronicBillingType,0) = 6 then 'CFDI4_0'

				Else '???'
			End  [ElectronicBillingTypeText],
			B.[CurrencyCode],
			BR.[BranchID],
			BR.[Name] AS BranchName
			,isnull(BR.[Code],'') AS BranchCode
		FROM 
			[Billers] B 
			INNER JOIN  [Branches] BR ON B.[BillerID] = BR.[BillerID]
		WHERE
			(@GroupID = GroupID) AND
			 ((@Active IS NULL) OR (ISNULL(B.[Active], 1) = @Active)) AND
			 ((@Active IS NULL) OR (ISNULL(BR.[Active], 1) = @Active))	
	) r
Order by
	r.BillerID
	,r.BranchID
	
	SET @Err = @@Error

	RETURN @Err
END


GO

--<!--  CONFIGURACION DEL PAC A CONEXION REST: TYpe 2 = REST: 

ALTER TABLE PACConfiguration
ADD Token nvarchar(500)  null;

GO

ALTER PROCEDURE [dbo].[spPACConfiguration_Update]
(
	@PACConfigurationID int,
	@BillerID int,
	@Url nvarchar(150),
	@Username nvarchar(50),
	@Password nvarchar(50),
	@FileType nvarchar(20),
	@Type int,
	@Active bit,
	@UrlCancellation nvarchar(150) = NULL,
	@Token nvarchar(500) = null
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [PACConfiguration]
	SET
		[BillerID] = @BillerID,
		[Url] = @Url,
		[Username] = @Username,
		[Password] = @Password,
		[FileType] = @FileType,
		[Type] = @Type,
		[Active] = @Active,
		[UrlCancellation] = @UrlCancellation,
		[Token]=@Token
	WHERE
		[PACConfigurationID] = @PACConfigurationID


	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spPACConfiguration_Insert]
(
	@PACConfigurationID int = NULL OUTPUT,
	@BillerID int,
	@Url nvarchar(150),
	@Username nvarchar(50),
	@Password nvarchar(50),
	@FileType nvarchar(20),
	@Type int,
	@Active bit,
	@UrlCancellation nvarchar(150) = NULL,
	@Token nvarchar(500) = null
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [PACConfiguration]
	(
		[BillerID],
		[Url],
		[Username],
		[Password],
		[FileType],
		[Type],
		[Active],
		[UrlCancellation],
		[Token]
	)
	VALUES
	(
		@BillerID,
		@Url,
		@Username,
		@Password,
		@FileType,
		@Type,
		@Active,
		@UrlCancellation,
		@Token
	)

	
	SELECT @PACConfigurationID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[PACConfigurationID],
		[BillerID],
		[Url],
		[Username],
		[Password],
		[FileType],
		[Type],
		[Active],
		[UrlCancellation],
		[Token]
	FROM [PACConfiguration]
	WHERE
		([PACConfigurationID] = @PACConfigurationID)

	SET @Err = @@Error
	
RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spPACConfiguration_GetByID]
(
	@PACConfigurationID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[PACConfigurationID],
		[BillerID],
		[Url],
		[Username],
		[Password],
		[FileType],
		[Type],
		[Active],
		[UrlCancellation],
		[Token]
	FROM [PACConfiguration]
	WHERE
		([PACConfigurationID] = @PACConfigurationID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spPACConfiguration_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[PACConfigurationID],
		[BillerID],
		[Url],
		[Username],
		[Password],
		[FileType],
		[Type],
		[Active],
		[UrlCancellation],
		[Token]
	FROM [PACConfiguration]

	SET @Err = @@Error

	RETURN @Err
END


GO
-- BILLING RECEPTORS 

ALTER TABLE BillingsReceptors
ADD [CFDIUse] [nvarchar](5) NULL
GO


ALTER PROCEDURE [dbo].[spBillingsReceptors_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingID],
		[ReceptorAddress],
		[ReceptorRFC],
		[ReceptorName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[TaxSystem],
		[CFDIUse]
	FROM [BillingsReceptors]

	SET @Err = @@Error

	RETURN @Err
END
GO

ALTER PROCEDURE [dbo].[spBillingsReceptors_GetByID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingID],
		[ReceptorAddress],
		[ReceptorRFC],
		[ReceptorName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[TaxSystem],
		[CFDIUse]
	FROM [BillingsReceptors]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END

GO
ALTER PROCEDURE [dbo].[spBillingsReceptors_Insert]
(
	@BillingID int,
	@ReceptorAddress nvarchar(255),
	@ReceptorRFC nvarchar(13),
	@ReceptorName nvarchar(255),
	@ExternalNumber nvarchar(50),
	@InternalNumber nvarchar(50),
	@Colony nvarchar(255),
	@Location nvarchar(255),
	@Reference nvarchar(255) = NULL,
	@Municipality nvarchar(255) = NULL,
	@State nvarchar(255),
	@Country nvarchar(255),
	@Zipcode nvarchar(10)='',
	@taxsystem varchar(50) = null,
	@CFDIUse nvarchar(5) = null
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [BillingsReceptors]
	(
		[BillingID],
		[ReceptorAddress],
		[ReceptorRFC],
		[ReceptorName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode],
		[TaxSystem],
		[CFDIUse]
	)
	VALUES
	(
		@BillingID,
		@ReceptorAddress,
		@ReceptorRFC,
		@ReceptorName,
		@ExternalNumber,
		@InternalNumber,
		@Colony,
		@Location,
		@Reference,
		@Municipality,
		@State,
		@Country,
		@Zipcode,
		@taxsystem,
		@CFDIUse
	)

	

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[BillingID],
		[ReceptorAddress],
		[ReceptorRFC],
		[ReceptorName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[Zipcode]
		[taxsystem],
		[CFDIUse]
	FROM [BillingsReceptors]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error
	
RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spBillingsReceptors_Update]
(
	@BillingID int,
	@ReceptorAddress nvarchar(255),
	@ReceptorRFC nvarchar(13),
	@ReceptorName nvarchar(255),
	@ExternalNumber nvarchar(50),
	@InternalNumber nvarchar(50),
	@Colony nvarchar(255),
	@Location nvarchar(255),
	@Reference nvarchar(255) = NULL,
	@Municipality nvarchar(255) = NULL,
	@State nvarchar(255),
	@Country nvarchar(255),
	@Zipcode nvarchar(10)='',
	@taxsystem varchar(50) = null,
	@CFDIUse nvarchar(5) = null
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [BillingsReceptors]
	SET
		[ReceptorAddress] = @ReceptorAddress,
		[ReceptorRFC] = @ReceptorRFC,
		[ReceptorName] = @ReceptorName,
		[ExternalNumber] = @ExternalNumber,
		[InternalNumber] = @InternalNumber,
		[Colony] = @Colony,
		[Location] = @Location,
		[Reference] = @Reference,
		[Municipality] = @Municipality,
		[State] = @State,
		[Country] = @Country,
		[Zipcode] = @Zipcode,
		[TaxSystem] = @taxsystem,
		[CFDIUse] = @CFDIUse
	WHERE
		[BillingID] = @BillingID


	SET @Err = @@Error

	RETURN @Err
END


GO
--- BillingBillers

GO

ALTER PROCEDURE [dbo].[spBillingsBillers_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingID],
		[BillerRFC],
		[BillerAddress],
		[BillerName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[IssuedInDifferentPlace],
		[Zipcode],
		[TaxSystem]
	FROM [BillingsBillers]

	SET @Err = @@Error

	RETURN @Err
END


GO
ALTER PROCEDURE [dbo].[spBillingsBillers_GetByID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingID],
		[BillerRFC],
		[BillerAddress],
		[BillerName],
		[ExternalNumber],
		[InternalNumber],
		[Colony],
		[Location],
		[Reference],
		[Municipality],
		[State],
		[Country],
		[IssuedInDifferentPlace],
		[Zipcode],
		[TaxSystem]
	FROM 
		[BillingsBillers]
	WHERE
		([BillingID] = @BillingID)	

	SET @Err = @@Error

	RETURN @Err
END
GO



ALTER PROCEDURE [dbo].[spBillingsBillers_Update]
(
	@BillingID int,
	@BillerRFC nvarchar(13),
	@BillerAddress nvarchar(255),
	@BillerName nvarchar(255),
	@ExternalNumber nvarchar(50),
	@InternalNumber nvarchar(50),
	@Colony nvarchar(255),
	@Location nvarchar(255),
	@Reference nvarchar(255) = NULL,
	@Municipality nvarchar(255) = NULL,
	@State nvarchar(255),
	@Country nvarchar(255),
	@IssuedInDifferentPlace bit,
	@Zipcode nvarchar(10) = '',
	@TaxSystem nvarchar(50)  = null
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [BillingsBillers]
	SET
		[BillerRFC] = @BillerRFC,
		[BillerAddress] = @BillerAddress,
		[BillerName] = @BillerName,
		[ExternalNumber] = @ExternalNumber,
		[InternalNumber] = @InternalNumber,
		[Colony] = @Colony,
		[Location] = @Location,
		[Reference] = @Reference,
		[Municipality] = @Municipality,
		[State] = @State,
		[Country] = @Country,
		[IssuedInDifferentPlace] = @IssuedInDifferentPlace,
		[Zipcode] = @Zipcode,
		[TaxSystem] = @TaxSystem

	WHERE
		[BillingID] = @BillingID


	SET @Err = @@Error

	RETURN @Err
END

GO

---BillingDetails Conceptos. 

ALTER TABLE BillingsDetails
ADD TaxObject [nvarchar](2)  NULL

Go

ALTER PROCEDURE [dbo].[spBillingsDetails_GetByFkBillingID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingsDetailsID],
		[BillingID],
		[ConceptID],
		[Amount],
		[Description],
		[UnitValue],
		[Unit],
		[IdentificationNumber],
		[Count],
		[AppliesTax],
		[DiscountPercentage],
		[PrintOnlyDescription],
		[TaxType],
		[FactorType],
		[TasaOCuota],
		[Tax],
		[ClaveProdServ],
		[UnitType],
		[Retained],
		[RetainedTasaOCuota],
		[StudentName],
		[StudentCurp],
		[EducationLevel],
		[AutrVOE],
		[ObjetoImp]
	FROM [BillingsDetails]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spBillingsDetails_GetByID]
(
	@BillingsDetailsID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[BillingsDetailsID],
		[BillingID],
		[ConceptID],
		[Amount],
		[Description],
		[UnitValue],
		[Unit],
		[IdentificationNumber],
		[Count],
		[AppliesTax],
		[DiscountPercentage],
		[PrintOnlyDescription],
		[TaxType],
		[FactorType],
		[TasaOCuota],
		[Tax],
		[ClaveProdServ],
		[UnitType],
		[Retained],
		[RetainedTasaOCuota],
		[StudentName],
		[StudentCurp],
		[EducationLevel],
		[AutrVOE],
		[TaxObject]
	FROM [BillingsDetails]
	WHERE
		([BillingsDetailsID] = @BillingsDetailsID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spBillingsDetails_Insert]
(
	@BillingsDetailsID int = NULL OUTPUT,
	@BillingID int,
	@ConceptID int,
	@Amount money,
	@Description nvarchar(1000),
	@UnitValue money,
	@Unit nvarchar(50),
	@IdentificationNumber nvarchar(50),
	@Count money,
	@AppliesTax bit,
	@DiscountPercentage decimal(18,2) = NULL,
	@PrintOnlyDescription bit = NULL,
	@TaxType varchar(50) = NULL,
	@FactorType varchar(100) = NULL,
	@TasaOCuota varchar(50) = NULL,
	@Tax money = 0,
	@ClaveProdServ varchar(50) = NULL,
	@unittype varchar(300) = nULL,
	@Retained money = 0,
	@RetainedTasaOCuota varchar(50) = NULL,
	@StudentName varchar(300) = NULL,
	@StudentCurp varchar(18) = NULL,
	@EducationLevel varchar(300) = NULL,
	@AutrVOE varchar(300) = NULL,
	@TaxObject nvarchar(2) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [BillingsDetails]
	(
		[BillingID],
		[ConceptID],
		[Amount],
		[Description],
		[UnitValue],
		[Unit],
		[IdentificationNumber],
		[Count],
		[AppliesTax],
		[DiscountPercentage],
		[PrintOnlyDescription],
		[TaxType],
		[FactorType],
		[TasaOCuota],
		[Tax],
		[ClaveProdServ],
		[UnitType],
		[Retained],
		[RetainedTasaOCuota],
		[StudentName],
		[StudentCurp],
		[EducationLevel],
		[AutrVOE],
		[TaxObject]
	)
	VALUES
	(
		@BillingID,
		@ConceptID,
		@Amount,
		@Description,
		@UnitValue,
		@Unit,
		@IdentificationNumber,
		@Count,
		@AppliesTax,
		@DiscountPercentage,
		@PrintOnlyDescription,
		@TaxType,
		@FactorType,
		@TasaOCuota,
		@Tax,
		@ClaveProdServ,
		@unittype,
		@Retained,
		@RetainedTasaOCuota,
		@StudentName,
		@StudentCurp,
		@EducationLevel,
		@AutrVOE,
		@TaxObject
	)

	
	SELECT @BillingsDetailsID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[BillingsDetailsID],
		[BillingID],
		[ConceptID],
		[Amount],
		[Description],
		[UnitValue],
		[Unit],
		[IdentificationNumber],
		[Count],
		[AppliesTax],
		[DiscountPercentage],
		[PrintOnlyDescription],
		[TaxType],
		[FactorType],
		[TasaOCuota],
		[Tax],
		[ClaveProdServ],
		[UnitType],
		[Retained],
		[RetainedTasaOCuota],
		[StudentName],
		[StudentCurp],
		[EducationLevel],
		[AutrVOE],
		[TaxObject]
	FROM [BillingsDetails]
	WHERE
		([BillingsDetailsID] = @BillingsDetailsID)

	SET @Err = @@Error
	
RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spBillingsDetails_Update]
(
	@BillingsDetailsID int,
	@BillingID int,
	@ConceptID int,
	@Amount money,
	@Description nvarchar(1000),
	@UnitValue money,
	@Unit nvarchar(50),
	@IdentificationNumber nvarchar(50),
	@Count money,
	@AppliesTax bit,
	@DiscountPercentage decimal(18,2) = NULL,
	@PrintOnlyDescription bit = NULL,
	@TaxType varchar(50) = NULL,
	@FactorType varchar(100) = NULL,
	@TasaOCuota varchar(50) = NULL,
	@Tax money = 0,
	@ClaveProdServ varchar(50) = NULL,
	@unittype varchar (300) = null,
	@Retained money = 0,
	@RetainedTasaOCuota varchar(50) = NULL,
	@StudentName varchar(300) = NULL,
	@StudentCurp varchar(18) = NULL,
	@EducationLevel varchar(300) = NULL,
	@AutrVOE varchar(300) = NULL,
	@TaxObject nvarchar(2) = NULL
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [BillingsDetails]
	SET
		[BillingID] = @BillingID,
		[ConceptID] = @ConceptID,
		[Amount] = @Amount,
		[Description] = @Description,
		[UnitValue] = @UnitValue,
		[Unit] = @Unit,
		[IdentificationNumber] = @IdentificationNumber,
		[Count] = @Count,
		[AppliesTax] = @AppliesTax,
		[DiscountPercentage] = @DiscountPercentage,
		[PrintOnlyDescription] = @PrintOnlyDescription,
		[TaxType] = @TaxType,
		[FactorType] = @FactorType,
		[TasaOCuota] = @TasaOCuota,
		[Tax] = @Tax,
		[ClaveProdServ] = @ClaveProdServ,
		[UnitType] = @unittype,
		[Retained] = @Retained,
		[RetainedTasaOCuota] = @RetainedTasaOCuota,
		[StudentName] = @StudentName,
		[StudentCurp] = @StudentCurp,
		[EducationLevel] = @EducationLevel,
		[AutrVOE] = @AutrVOE,
		[TaxObject] = @TaxObject
	WHERE
		[BillingsDetailsID] = @BillingsDetailsID


	SET @Err = @@Error

	RETURN @Err
END

GO

---  Retencion de Iva, DetainedTax

Alter Table DetainedTaxes
Add FactorType varchar(50) null
GO

Alter Table DetainedTaxes
Add Base money null

GO

ALTER PROCEDURE [dbo].[spDetainedTaxes_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DetainedTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [DetainedTaxes]

	SET @Err = @@Error

	RETURN @Err
END

GO
ALTER PROCEDURE [dbo].[spDetainedTaxes_GetByFkBillingID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DetainedTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [DetainedTaxes]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END

GO


ALTER PROCEDURE [dbo].[spDetainedTaxes_GetByID]
(
	@DetainedTaxeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[DetainedTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [DetainedTaxes]
	WHERE
		([DetainedTaxeID] = @DetainedTaxeID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spDetainedTaxes_Insert]
(
	@DetainedTaxeID int = NULL OUTPUT,
	@TaxRatePercentage money,
	@Name nvarchar(20),
	@BillingID int,
	@Import money,
	@IvaAffected bit,
	@FactorType varchar(50),
	@Base money
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [DetainedTaxes]
	(
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	)
	VALUES
	(
		@TaxRatePercentage,
		@Name,
		@BillingID,
		@Import,
		@IvaAffected,
		@FactorType,
		@Base
	)

	
	SELECT @DetainedTaxeID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[DetainedTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [DetainedTaxes]
	WHERE
		([DetainedTaxeID] = @DetainedTaxeID)

	SET @Err = @@Error
	
RETURN @Err
END


GO

ALTER PROCEDURE [dbo].[spDetainedTaxes_Update]
(
	@DetainedTaxeID int,
	@TaxRatePercentage money,
	@Name nvarchar(20),
	@BillingID int,
	@Import money,
	@IvaAffected bit,
	@FactorType varchar(50),
	@Base money
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [DetainedTaxes]
	SET
		[TaxRatePercentage] = @TaxRatePercentage,
		[Name] = @Name,
		[BillingID] = @BillingID,
		[Import] = @Import,
		[IvaAffected] = @IvaAffected,
		[FactorType] = @FactorType,
		[Base] = @Base
	WHERE
		[DetainedTaxeID] = @DetainedTaxeID


	SET @Err = @@Error

	RETURN @Err
END

GO


---- TransferTaxes o Iva Trasladado. agregado de base

Alter Table TransferTaxes
Add Base money null

GO

ALTER PROCEDURE [dbo].[spTransferTaxes_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[TransferTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [TransferTaxes]

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spTransferTaxes_GetByID]
(
	@TransferTaxeID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[TransferTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [TransferTaxes]
	WHERE
		([TransferTaxeID] = @TransferTaxeID)

	SET @Err = @@Error

	RETURN @Err
END

GO
ALTER PROCEDURE [dbo].[spTransferTaxes_GetByFkBillingID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[TransferTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType]	,
		[Base]
	FROM [TransferTaxes]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END

GO

ALTER PROCEDURE [dbo].[spTransferTaxes_Insert]
(
	@TransferTaxeID int = NULL OUTPUT,
	@TaxRatePercentage money,
	@Name nvarchar(20),
	@BillingID int,
	@Import money,
	@IvaAffected bit,
	@FactorType varchar(50) = null,
	@Base money
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [TransferTaxes]
	(
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	)
	VALUES
	(
		@TaxRatePercentage,
		@Name,
		@BillingID,
		@Import,
		@IvaAffected,
		@FactorType,
		@Base
	)

	
	SELECT @TransferTaxeID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		[TransferTaxeID],
		[TaxRatePercentage],
		[Name],
		[BillingID],
		[Import],
		[IvaAffected],
		[FactorType],
		[Base]
	FROM [TransferTaxes]
	WHERE
		([TransferTaxeID] = @TransferTaxeID)

	SET @Err = @@Error
	
RETURN @Err
END


GO

ALTER PROCEDURE [dbo].[spTransferTaxes_Update]
(
	@TransferTaxeID int,
	@TaxRatePercentage money,
	@Name nvarchar(20),
	@BillingID int,
	@Import money,
	@IvaAffected bit,
	@FactorType varchar(50) = null,
	@Base money
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [TransferTaxes]
	SET
		[TaxRatePercentage] = @TaxRatePercentage,
		[Name] = @Name,
		[BillingID] = @BillingID,
		[Import] = @Import,
		[IvaAffected] = @IvaAffected,
		[FactorType] = @factortype,
		[Base] = @Base
	WHERE
		[TransferTaxeID] = @TransferTaxeID


	SET @Err = @@Error

	RETURN @Err
END

GO








-- <!-- soporte para uso de CFDI 4 como selector de Esquema.  -->'

USE UniFacturaDigital

ALTER TABLE Emisor
ADD RegimenFiscal NVarChar(50);
GO

ALTER TABLE Receptor
ADD RegimenFiscal NVarChar(50);
GO

CREATE TABLE [dbo].[CatExportacion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[c_Exportacion] [nvarchar](2) NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_CatExportacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

Insert into CatExportacion (c_Exportacion,Descripcion) VALUES ('01','No Aplica')
GO
Insert into CatExportacion (c_Exportacion,Descripcion) VALUES ('02','Definitiva')
GO
Insert into CatExportacion (c_Exportacion,Descripcion) VALUES ('03','Temporal')
GO
Insert into CatExportacion (c_Exportacion,Descripcion) VALUES ('04','Definitiva con clave distinta a A1 o cuando existe enajenacién en términos del CFF')
GO



CREATE TABLE [dbo].[CatObjetoImp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[c_ObjetoImp] [nvarchar](2) NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
	[FechaInicioVigencia] [nvarchar](15)  NULL,
 CONSTRAINT [PK_CatObjetoImp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

Insert into CatObjetoImp (c_ObjetoImp,Descripcion,FechaInicioVigencia) VALUES ('01','No objeto de impuesto.','01/01/2022')
GO
Insert into CatObjetoImp (c_ObjetoImp,Descripcion,FechaInicioVigencia) VALUES ('02','Sí objeto de impuesto.','01/01/2022')
GO
Insert into CatObjetoImp (c_ObjetoImp,Descripcion,FechaInicioVigencia) VALUES ('03','Sí objeto del impuesto y no obligado al desglose','01/01/2022')
GO




