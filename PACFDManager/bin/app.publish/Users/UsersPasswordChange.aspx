﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="UsersPasswordChange.aspx.cs" Inherits="PACFDManager.Users.UsersPasswordChange" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Cambiar contraseña" />
                    </h2>
                    <ul>
                        <li>
                            <asp:Label ID="lblActualPassword" runat="server" Text="Contraseña actual." />
                            <br />
                            <asp:TextBox ID="txtActualPassword" runat="server" TextMode="Password" />
                            <div class="validator-msg">
                                <asp:RequiredFieldValidator ID="txtActualPassword_RequiredFieldValidator" runat="server"
                                    ControlToValidate="txtActualPassword" Display="Dynamic" ErrorMessage="Este campo es requerido" />
                            </div>
                        </li>
                        <li>
                            <asp:Label ID="lblNewPassword" runat="server" Text="Nueva contraseña." />
                            <br />
                            <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" />
                            <div class="validator-msg">
                                <asp:RequiredFieldValidator ID="txtNewPassword_RequiredFieldValidator" runat="server"
                                    ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="Este campo es requerido" />
                            </div>
                        </li>
                        <li>
                            <asp:Label ID="lblRepeatPassword" runat="server" Text="Repetir contraseña." />
                            <br />
                            <asp:TextBox ID="txtRepeatPassword" runat="server" TextMode="Password" />
                            <div class="validator-msg">
                                <asp:RequiredFieldValidator ID="txtRepeatPassword_RequiredFieldValidator" runat="server"
                                    ControlToValidate="txtRepeatPassword" Display="Dynamic" ErrorMessage="Este campo es requerido" />
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li class="buttons">
                            <div>
                                <asp:Button ID="btnAcept" CssClass="Button" runat="server" Text="Aceptar" OnClick="btnAcept_Click" />
                                &nbsp;<asp:Button ID="btnCancel" CssClass="Button" CausesValidation="false" runat="server"
                                    Text="Cancelar" PostBackUrl="~/Default.aspx" />
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
