﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="TaxTypeCatalogList.aspx.cs" Inherits="PACFDManager.Administrator.TaxTypeCatalogList" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="txt_left">
        <div class="info">
            <label class="desc">
                <h2>
                    <asp:Label ID="lblTaxesListTitle" runat="server" Text="Catalogo de Impuestos" />
                </h2>
            </label>
        </div>
        <div>
            <br />
            <asp:Button ID="btnAdd" runat="server" Text="Agregar impuesto" OnClick="btnAdd_Click"
                CausesValidation="False" CssClass="Button" />
            <br />
            <br />
            <asp:GridView ID="gdvTaxes" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay impuestos disponibles.">
                <Columns>
                    <asp:BoundField DataField="TaxTypeID" HeaderText="ID" />
                    <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Tipo">
                        <ItemTemplate>
                            <asp:Label ID="lblTrasfer" runat="server" Text='<%#Eval("Transfer").ToString() == "True" ? "Transferido" : "Retenido" %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Editable">
                        <ItemTemplate>
                            <span class="vtip" title='<%#Eval("Editable").ToString() == "True" ? "Aplica" : "No aplica" %>'>
                                <asp:Image ID="imgEditable" runat="server" AlternateText='<%#Eval("Editable").ToString() == "True" ? "Aplica" : "No aplica" %>'
                                    ImageUrl='<%#Eval("Editable").ToString() == "True" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png" %>' /></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requerido">
                        <ItemTemplate>
                            <span class="vtip" title='<%#Eval("Required").ToString() == "True" ? "Aplica" : "No aplica" %>'>
                                <asp:Image ID="imgRequired" runat="server" AlternateText='<%#Eval("Required").ToString() == "True" ? "Aplica" : "No aplica" %>'
                                    ImageUrl='<%#Eval("Required").ToString() == "True" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png" %>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Editar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar el impuesto seleccionado." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Modificar el impuesto<br /><b><%#Eval("Name") + " - " + (Eval("Transfer").ToString() == "True" ? "Transferido" : "Retenido")%></b>'>
                                <asp:ImageButton ID="imgbEdit" runat="server" ImageUrl="~/Includes/Images/png/editIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxTypeID")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eliminar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar el impuesto seleccionado." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Eliminar el impuesto<br /><b><%#Eval("Name") + " - " + (Eval("Transfer").ToString() == "True" ? "Transferido" : "Retenido")%></b>'>
                                <asp:ImageButton ID="imgbDelete" runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxTypeID")%>' Visible='<%#Eval("Fixed").ToString() == "True" ? Convert.ToBoolean("False") : Convert.ToBoolean("True") %>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="dgHeader" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
                <EmptyDataTemplate>
                    No hay impuestos disponibles.
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Button ID="btnAdd2" runat="server" Text="Agregar impuesto" OnClick="btnAdd_Click"
                CausesValidation="False" CssClass="Button" />
            <br />
            <br />
        </div>
        <uc1:WebMessageBox ID="wmbDelete" runat="server" OnClick="wmbDelete_Click" />
    </div>
</asp:Content>
