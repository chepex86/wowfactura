﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="TaxBaseConfigurationModify.aspx.cs" Inherits="PACFDManager.Administrator.TaxBaseConfigurationModify" %>

<%@ Register Src="TaxBaseConfigurationEditor.ascx" TagName="TaxBaseConfigurationEditor"
    TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:TaxBaseConfigurationEditor ID="TaxBaseConfigurationEditor1" runat="server" OnApplyChanges="TaxBaseConfigurationEditor1_ApplyChanges" />
            <div class="form">
                <ul>
                    <li class="buttons">
                        <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button" OnClick="btnCancel_Click"
                            CausesValidation="false" />
                    </li>
                </ul>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
