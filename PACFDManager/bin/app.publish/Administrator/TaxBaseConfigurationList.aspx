﻿<%@ Page Title="Configuración de Comprobantes Base" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="TaxBaseConfigurationList.aspx.cs" Inherits="PACFDManager.Administrator.TaxBaseConfigurationList" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="txt_left">
        <div class="info">
            <label class="desc">
                <h2>
                    <asp:Label ID="lblTaxesListTitle" runat="server" Text="Catalogo de Configuración de Comprobantes Base" />
                </h2>
            </label>
        </div>
        <div>
            <br />
            <asp:Button ID="btnAdd" runat="server" Text="Agregar configuración base" OnClick="btnAdd_Click"
                CssClass="Button" />
            <br />
            <br />
            <asp:GridView ID="gdvTemplates" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay impuestos disponibles."
                OnRowDataBound="gdvTemplates_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="TaxBaseTemplateID" HeaderText="ID" />
                    <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Description" HeaderText="Descripción" />
                    <asp:BoundField DataField="BillingType" HeaderText="Tipo" />
                    <asp:TemplateField HeaderText="Editar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la configuración seleccionada." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Modificar la configuración de<br /><b><%#Eval("Name") + " - " + Eval("BillingType")%></b>'>
                                <asp:ImageButton ID="imgbEdit" runat="server" ImageUrl="~/Includes/Images/png/editIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxBaseTemplateID")%>' />
                                <asp:HiddenField ID="hdfTxTemplateID" runat="server" Value='<%#Eval("TaxBaseTemplateID")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eliminar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar la configuración seleccionada." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Eliminar la configuración de<br /><b><%#Eval("Name") + " - " + Eval("BillingType")%></b>'>
                                <asp:ImageButton ID="imgbDelete" runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxBaseTemplateID")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="dgHeader" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
                <EmptyDataTemplate>
                    No hay impuestos disponibles.
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Button ID="btnAdd2" runat="server" Text="Agregar configuración base" OnClick="btnAdd_Click"
                CssClass="Button" />
            <br />
            <br />
        </div>
        <div>
            <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </div>
    </div>
</asp:Content>
