﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillerSmtpConfiguration.aspx.cs" Inherits="PACFDManager.Administrator.BillerSmtpConfiguration" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

    <script src="BillerSmtpConfiguration.js" type="text/javascript"></script>

    <div id="container">
        <div class="form">
            <div class="formStyles">
                <!-- title form -->
                <div class="info">
                    <h2>
                        <asp:Label ID="lblEmailConfigurationTitle" runat="server" Text="Configuración de correo por empresa" />
                    </h2>
                </div>
                <div>
                    <asp:CheckBox ID="ckbNotUseSmtpConfiguration" runat="server" Text="No usar configuración smtp" />
                    <asp:CheckBox ID="ckbUseBaseEmail" runat="server" Text="Usar configuración base" />
                </div>
                <ul>
                    <li>
                        <h2>
                            <asp:Label ID="lblServerTitle" runat="server" Text="Servidor" />
                        </h2>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblHostTitle" runat="server" Text="Host" />
                        </label>
                        <span class="vtip" title="Nombre del servidor de correo. Ej. <br /><b>•smtp.gmail.com</b><br/><b>•smtp.yahoo.com</b><br /><b>•smtp.live.com</b>">
                            <asp:TextBox ID="txtHost" runat="server" AutoCompleteType="Disabled" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="txtHost_RequiredFieldValidator" runat="server" ControlToValidate="txtHost"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic" />
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPortTitle" runat="server" Text="Puerto"></asp:Label>
                        </label>
                        <span class="vtip" title="Puerto de salida del servidor de correo. Ej. <br/><b>•587</b><br/><b>•465</b>">
                            <asp:TextBox ID="txtPort" runat="server" AutoCompleteType="Disabled" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="txtPort_RequiredFieldValidator" runat="server" ControlToValidate="txtPort"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic" />
                        </div>
                    </li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lblHiddencopyTitle" runat="server" Text="Copia oculta (Bcc)" />
                        </label>
                        <span class="vtip" title="Correo electrónico al cual se enviara una copia oculta. Ej. <br /><b>•administrador@gmail.com.mx</b><br /><b>•juan.perez@oz.com.mx</b>">
                            <asp:TextBox ID="txtBcc" runat="server" />
                        </span></li>
                    <li>
                        <h2>
                            <asp:Label ID="lblCredentialsTitle" runat="server" Text="Credenciales" />
                        </h2>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblEmailTitle" runat="server" Text="Email"></asp:Label>
                        </label>
                        <span class="vtip" title="Correo electrónico que envía el correo. Ej. <br /><b>•administrador@hotmail.com.mx</b><br /><b>•juan.perez@oz.com.mx</b>">
                            <asp:TextBox ID="txtCredentialName" runat="server" />
                        </span>
                        <div class="validator-msg" style="width: 200px">
                            <asp:RequiredFieldValidator ID="txtCredentialName_RequiredFieldValidator" runat="server"
                                ControlToValidate="txtCredentialName" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="txtCredentialName_RegularExpressionValidator"
                                runat="server" ControlToValidate="txtCredentialName" ErrorMessage="Dirección de Correo Electronico no Valida"
                                ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                                ValidationGroup="Validators" meta:resourcekey="revEmailResource1" Display="Dynamic" />
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPasswordTitle" runat="server" Text="Contraseña" />
                        </label>
                        <span class="vtip" title="Contraseña del correo que envía el correo.<br/>Recuerde no revelar la contraseña a nadie sin autorización.">
                            <asp:TextBox ID="txtCredentialPassword" runat="server" TextMode="Password" MaxLength="13" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="txtCredentialPassword_RequiredFieldValidator" runat="server"
                                ControlToValidate="txtCredentialPassword" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" Display="Dynamic" />
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPasswordValidation" runat="server" Text="Contraseña verificación" />
                        </label>
                        <span class="vtip" title="Verifica que la contraseña este bien escrita.<br/>Recuerde no revelar la contraseña a nadie sin autorización.">
                            <asp:TextBox ID="txtCredentialPasswordVerification" runat="server" TextMode="Password"
                                MaxLength="13" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="txtCredentialPasswordVerification_RequiredFieldValidator"
                                runat="server" ControlToValidate="txtCredentialPasswordVerification" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" Display="Dynamic" />
                            <asp:CompareValidator ID="txtCredentialPasswordVerification_CompareValidator" runat="server"
                                ControlToCompare="txtCredentialPassword" ControlToValidate="txtCredentialPasswordVerification"
                                ErrorMessage="Contraseña diferente" ValidationGroup="Validators" meta:resourcekey="cvPasswordConfirmResource1"
                                Display="Dynamic" />
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblEnableSslTitle" runat="server" Text="Habilitar Ssl"></asp:Label>
                        </label>
                        <span class="vtip" title="Habilita el uso de seguridad encriptada <br />Ssl (Secure Socket Layer).">
                            <asp:CheckBox ID="ckbEnableSsl" runat="server" Checked="True" /></span></li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lbldisplayNameTitle" runat="server" Text="Nombre a desplegar" />
                        </label>
                        <span class="vtip" title="Nombre a desplegar en el título del correo. Ej.<br/><b>•Buen día desde Hotel Star Class.</b><br/><b>•Correo de autorización de pago.</b>">
                            <asp:TextBox ID="txtDisplayName" runat="server" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="txtDisplayName_RequiredFieldValidator" runat="server"
                                ControlToValidate="txtDisplayName" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"
                                Display="Dynamic" />
                        </div>
                    </li>
                    <li class="buttons">
                        <div>
                            <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click"
                                ValidationGroup="Validators" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                OnClick="btnCancel_Click" CausesValidation="false" /></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
    <%
        System.Text.StringBuilder sb = new StringBuilder();
        sb.AppendLine("<script type=\"text/javascript\">");
        sb.AppendLine(string.Format("var {0}_jcBillerSmtpConfiguration = new jclassBillerSmtpConfiguration(\"{1}\", \"{2}\", \"{3}\");"
            , this.ClientID
            , string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                this.txtHost_RequiredFieldValidator.ClientID
                , this.txtPort_RequiredFieldValidator.ClientID
                , this.txtCredentialName_RequiredFieldValidator.ClientID
                , this.txtCredentialName_RegularExpressionValidator.ClientID
                , this.txtCredentialPassword_RequiredFieldValidator.ClientID
                , this.txtCredentialPasswordVerification_RequiredFieldValidator.ClientID
                , this.txtCredentialPasswordVerification_CompareValidator.ClientID
                , this.txtDisplayName_RequiredFieldValidator.ClientID
                )
            , string.Format("{0},{1},{2}"
                , this.ckbEnableSsl.ClientID
                , this.ckbUseBaseEmail.ClientID
                , this.ckbNotUseSmtpConfiguration.ClientID
                )
            , string.Format("{0},{1},{2},{3},{4},{5},{6}"
                , this.txtHost.ClientID
                , this.txtPort.ClientID
                , this.txtBcc.ClientID
                , this.txtCredentialName.ClientID
                , this.txtCredentialPassword.ClientID
                , this.txtCredentialPasswordVerification.ClientID
                , this.txtDisplayName.ClientID
                )
            ));
        sb.AppendLine("</script>");
        this.Response.Write(sb.ToString());
    %>
</asp:Content>
