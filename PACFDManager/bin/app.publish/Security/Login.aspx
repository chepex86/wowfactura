﻿<%@ Page Language="C#" MasterPageFile="~/MpSecurity.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="PACFDManager.Security.Login" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">

    <script src="jclassSecurity.js" type="text/javascript"></script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="dvLogin" style="display: block;">
                <div class="secure-box">
                    <h2>
                        Administración</h2>
                    <br />
                    Por seguridad debes ingresar tu Usuario y Contraseña
                    <br />
                    para entrar al sistema.
                    <br />
                    <br />
                    <div class="secure-box-name">
                        <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName"
                            Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="Red" ValidationGroup="Validator"></asp:RequiredFieldValidator>
                        Usuario:</div>
                    <div class="secure-box-field">
                        <asp:TextBox ID="txtUserName" class="form-login" ToolTip="Username" Style="width: 200px;"
                            runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtUserName"
                            Display="Dynamic" ErrorMessage="Formato de dirección de correo incorrecta" ForeColor="Red"
                            ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                            ValidationGroup="Validator"></asp:RegularExpressionValidator>
                    </div>
                    <div class="secure-box-name">
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                            Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="Red" ValidationGroup="Validator"></asp:RequiredFieldValidator>
                        Contraseña:</div>
                    <div class="secure-box-field">
                        <asp:TextBox ID="txtPassword" class="form-login" Style="width: 200px;" runat="server"
                            TextMode="Password" ToolTip="Password"></asp:TextBox>
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false" />
                    </div>
                    <span class="secure-box-options"><a id="aRecoveryPassword" runat="server">Olvid&oacute;
                        su contrase&ntilde;a?</a></span>
                    <asp:ImageButton ID="btnIn" runat="server" ImageUrl="~/Includes/assets/images/log-btn.png"
                        Style="margin: 25px 88px; cursor: pointer" OnClick="btnIn_Click" ValidationGroup="Validator" />
                </div>
            </div>
            <div id="dvRecovery" style="display: none;">
                <div class="secure-box">
                    <h2>
                        Recordar Contraseña
                    </h2>
                    <br />
                    Ingresa tu Usuario y se te enviará a tu correo
                    <br />
                    la contraseña.
                    <br />
                    <br />
                    <div class="secure-box-name">
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="Red" ValidationGroup="Validators1"></asp:RequiredFieldValidator>
                        Usuario:
                    </div>
                    <div class="secure-box-field">
                        <asp:TextBox ID="txtEmail" class="form-login" runat="server" Style="width: 200px;"
                            meta:resourcekey="txtEmailResource1"></asp:TextBox>
                    </div>
                    <div class="validator-msg">
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="Dynamic" ErrorMessage="Formato de dirección de correo incorrecta" ForeColor="Red"
                            ValidationGroup="Validators1"></asp:RegularExpressionValidator>
                    </div>
                    <span class="secure-box-options"><a id="aReturn" runat="server">Regresar al Login</a></span>
                    <!--REMOVER ESTE OBJETO-->
                    <%--<asp:LinkButton ID="lnkBtnRemember" runat="server" meta:resourcekey="lnkBtnRememberResource1"
                        OnClick="lnkBtnRemember_Click" ValidationGroup="Validators1">Recordar</asp:LinkButton>--%>
                    <!--REMOVER ESTE OBJETO-->
                    <asp:ImageButton ID="imgBtnRemember" runat="server" ImageUrl="~/Includes/assets/images/rmb-btn.png"
                        Style="margin: 25px 88px; cursor: pointer" OnClick="lnkBtnRemember_Click" ValidationGroup="Validator" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
