﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PasswordUnlock.aspx.cs"
    Inherits="PACFDManager.Security.PasswordUnlock" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Desbloquear Usuario"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblEmail" runat="server" Text="Correo:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="vtip" title="Introduce la dirección de correo valida<br />que necesites recuperar la contraseña.<br />ej: <b>nombre@mail.com</b>">
                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="200px" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                    ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                                    ErrorMessage="Cadena de Correo No Valida" ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                                                    ValidationGroup="Validators" meta:resourcekey="revEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li><span class="chBox"><span class="vtip" title="Selecciona esta opción para que sea<br />enviado un correo electronico a la<br />dirección introducida con la contraseña.">
                                <asp:CheckBox ID="cbSendEmail" runat="server" Checked="true" Font-Bold="true" Text="Enviar correo" /></span></span>
                                <span>
                                    <asp:Label ID="lblSendEmail" Visible="false" runat="server" ForeColor="Green" Font-Bold="true"
                                        Text="Correo enviado exitosamente." />
                                </span><span>
                                    <asp:Label ID="lblSendEmailError" Visible="false" runat="server" ForeColor="Red"
                                        Font-Bold="true" Text="Ocurrio un problema al tratar de enviar el correo." />
                                </span></li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblPasswordTitle" runat="server" Text="Contraseña:"></asp:Label>
                                </label>
                                <span>
                                    <asp:Label ID="lblPassword" runat="server" Font-Bold="True"></asp:Label>
                                </span></li>
                            <li><span>
                                <asp:Label ID="lblMessage" runat="server" CssClass="Validator"></asp:Label>
                            </span></li>
                            <li class="buttons">
                                <div>
                                    <asp:Button ID="btnUnlock" runat="server" Text="Desbloquear" CssClass="Button" OnClick="btnUnlock_Click"
                                        ValidationGroup="Validators" />
                                    <asp:Button ID="btnOther" runat="server" CssClass="Button" Text="Desbloquear otro"
                                        Visible="false" OnClick="bntOther_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
