﻿var jcSecurity = new jclassSecurity();

function jclassSecurity() {
    this.ShowHiddeRecoverPassword = function() {
        var dvLogin = document.getElementById('dvLogin');
        var dvRecovery = document.getElementById('dvRecovery');

        if (dvRecovery != null)
            dvRecovery.style.display = (dvLogin.style.display == 'block') ? 'block' : 'none';

        if (dvLogin != null)
            dvLogin.style.display = (dvLogin.style.display == 'block') ? 'none' : 'block';
    }
}