﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PasswordChange.aspx.cs"
    Inherits="PACFDManager.Security.ChangePassword" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center" id="BookingContainer">
                <tr>
                    <td class="Titulo" colspan="2">
                        <asp:Label ID="lblTitle" runat="server" Text="Cambiar Contraseña" 
                            meta:resourcekey="lblTitleResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblCurrentPassword" runat="server" Text="Contraseña Actual:" 
                            meta:resourcekey="lblCurrentPasswordResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCurrentPassword" runat="server" MaxLength="50" 
                            Width="200px" meta:resourcekey="txtCurrentPasswordResource1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCurrentPassword" runat="server" 
                            ControlToValidate="txtCurrentPassword" ErrorMessage="*" 
                            ValidationGroup="Validators" 
                            meta:resourcekey="rfvCurrentPasswordResource1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblNewPassword" runat="server" Text="Contraseña Nueva:" 
                            meta:resourcekey="lblNewPasswordResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtNewPassword" runat="server" MaxLength="50" Width="200px" 
                            meta:resourcekey="txtNewPasswordResource1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" 
                            ControlToValidate="txtNewPassword" ErrorMessage="*" 
                            ValidationGroup="Validators" meta:resourcekey="rfvNewPasswordResource1" 
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblRepeatPassword" runat="server" Text="Repetir Contraseña:" 
                            meta:resourcekey="lblRepeatPasswordResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtRepeatPassword" runat="server" MaxLength="50" Width="200px" 
                            meta:resourcekey="txtRepeatPasswordResource1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvRepeatPassword" runat="server" 
                            ControlToValidate="txtRepeatPassword" ErrorMessage="*" 
                            ValidationGroup="Validators" meta:resourcekey="rfvRepeatPasswordResource1" 
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvRepeatPassword" runat="server" 
                            ControlToCompare="txtNewPassword" ControlToValidate="txtRepeatPassword" 
                            ErrorMessage="La contraseña es diferente" ValidationGroup="Validators" 
                            meta:resourcekey="cvRepeatPasswordResource1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnChangePassword" runat="server" CssClass="Button" 
                            Text="Cambiar" onclick="btnChangePassword_Click" 
                            ValidationGroup="Validators" 
                            meta:resourcekey="btnChangePasswordResource1" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Cancelar" 
                            meta:resourcekey="btnCancelResource1" />
                    </td>
                </tr>
                                <tr>
                    <td  colspan="2" align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="Validator" 
                            meta:resourcekey="lblMessageResource1"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
