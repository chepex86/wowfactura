﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchFilter.ascx.cs"
    Inherits="PACFDManager.Exportation.SearchFilter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>

<script src="SearchFilterScript.js" type="text/javascript"></script>

<ul>
    <li class="left"><span>
        <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate"
            Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator></span>
        <label class="desc">
            <asp:Label ID="lblDate" runat="server" Text="Fecha:"></asp:Label>
        </label>
        <span class="vtip" title="Selecciona la fecha que quieres que se genere el reporte.">
            <asp:TextBox ID="txtStartDate" runat="server" Width="87px"></asp:TextBox><asp:CalendarExtender
                ID="txtStartDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtStartDate">
            </asp:CalendarExtender>
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblExport" runat="server" Text="Exportar a:"></asp:Label></label>
        <span class="vtip" title="Selecciona el programa contable al que se exportara el reporte.<br /><b>ContPaq</b> • <b>ContPaq I</b> • <b>Aspel COI</b>">
            <asp:DropDownList ID="ddlExport" runat="server" Width="91px">
                <asp:ListItem>ContPaq</asp:ListItem>
                <asp:ListItem>ContPaq I</asp:ListItem>
                <asp:ListItem>Aspel Coi</asp:ListItem>
            </asp:DropDownList>
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblCurrency" runat="server" Text="Tipo de Cambio a USD:"></asp:Label></label><span
                class="vtip" title="Introduce el tipo de cambio a <b>USD</b> (United States dollar).<br /><br />ej: <b>12.93</b> ó <b>13.2345</b> (Valores correctos)<br /><b>13.23.1</b> (Valor incorrecto)">
                <asp:TextBox ID="txtCurrency" runat="server" MaxLength="10" Width="87px"></asp:TextBox>
            </span>
        <div class="validator-msg">
            <asp:RequiredFieldValidator ID="rfvCurrency" runat="server" ControlToValidate="txtCurrency"
                Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cvCurrency" runat="server" ControlToValidate="txtCurrency"
                Display="Dynamic" ErrorMessage="Solo números" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        </div>
    </li>
    <li><span>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
    </span></li>
    <li class="buttons">
        <div>
            <asp:Button ID="btnGenerate" runat="server" CssClass="Button" OnClick="btnGenerate_Click"
                Text="Generar" ValidationGroup="Validators" />
        </div>
    </li>
</ul>
