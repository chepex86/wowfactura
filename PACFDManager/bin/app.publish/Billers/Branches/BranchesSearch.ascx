﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BranchesSearch.ascx.cs"
    Inherits="PACFDManager.Billers.Branches.BranchesSearch" %>
<div id="container" class="wframe60">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblSearchTitle" runat="server" Text="Búsqueda de sucursal" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Por Nombre" />
                    </label>
                    <span class="vtip" title="Búsqueda por nombre de sucursal.">
                        <asp:TextBox ID="txtName" runat="server" /></span> </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblCode" runat="server" Text="Por Clave" />
                    </label>
                    <span class="vtip" title="Búsqueda por codigo de sucursal.">
                        <asp:TextBox ID="txtCode" runat="server" MaxLength="20" /></span> </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblActive" runat="server" Text="Activo" />
                    </label>
                    <span class="vtip" title="Búsqueda por nombre de sucursal.">
                        <asp:DropDownList ID="ddlActive" runat="server">
                            <asp:ListItem Selected="True" Text="Seleccionar" Value="-1" />
                            <asp:ListItem Text="Activo" Value="1" />
                            <asp:ListItem Text="Inactivo" Value="0" />
                        </asp:DropDownList>
                    </span></li>
                <li class="left">
                    <asp:Button CssClass="Button_Search" ID="btnSearchName" runat="server" Text="Buscar"
                        OnClick="btnSearch_Click" />
                </li>
            </ul>
        </div>
    </div>
</div>
