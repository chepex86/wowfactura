﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BranchesList.aspx.cs" Inherits="PACFDManager.Billers.Branches.BranchesList" %>

<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="BranchesSearch.ascx" TagName="BranchesSearch" TagPrefix="uc2" %>
<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc2:BranchesSearch ID="BranchesSearch1" runat="server" OnSearch="BranchesSearch1_Search" />
            </div>
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblBrancgesListTitle" runat="server" Text="Listado de Sucursales" />
                    </h2>
                </div>
                <asp:Button ID="btnAdd" runat="server" Text="Agregar Sucursal" CssClass="Button"
                    OnClick="btnAdd_Click" />
                <asp:GridView ID="gdvBranch" runat="server" CssClass="DataGrid" AutoGenerateColumns="False"
                    EmptyDataText="No hay emisores disponibles">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="BranchID" />
                        <asp:BoundField HeaderText="Codigo" DataField="Code" />
                        <asp:BoundField HeaderText="Nombre" DataField="Name" ItemStyle-HorizontalAlign="Left">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Editar">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la empresa seleccionada." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Modificar la empresa<br /><b><%#Eval("Name")%></b>'>
                                    <asp:ImageButton ID="imbEdit" CommandArgument='<%#Eval("BranchID")%>' runat="server"
                                        OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editIcon.png" AlternateText="Editar" /></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Borrar">
                            <HeaderTemplate>
                                <uc3:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/editdelete.png' alt='Desactivar' /> para<br />desactivar la sucursal seleccionada.<br /><br />Hacer click en la imagen <img src='../../Includes/Images/png/apply.png' alt='activar' /> para<br />reactivar la sucursal seleccionada." />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='Desactivar la sucursal<br /><b><%#Eval("Name")%></b>'>
                                    <asp:ImageButton ID="imbDelete" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                        OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editdelete.png" AlternateText="Eliminar"
                                        Visible='<%#Eval("Active").ToString() == "True" ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>' />
                                </span>
                                <div id="divEditBranchReactivate" runat="server" style="text-align: center;" visible='<%#Eval("Active").ToString() == "False" ? Convert.ToBoolean("true") :  Convert.ToBoolean("false")%>'>
                                    <span class="vtip" title='Reactivar la empresa<br /><b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imbReactivate" CommandArgument='<%#Eval("BillerID")%>' runat="server"
                                            OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/apply.png" AlternateText="Reactivar empresa" />
                                    </span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:Button ID="btnAdd2" runat="server" CssClass="Button" Text="Agregar Sucursal"
                    OnClick="btnAdd_Click" />
            </div>
            <div>
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
