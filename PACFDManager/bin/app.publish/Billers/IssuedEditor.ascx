﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IssuedEditor.ascx.cs"
    Inherits="PACFDManager.Billers.IssuedEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../CountrySelector.ascx" TagName="CountrySelector" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>

<script type="text/javascript">

    function IssuedEnableValidators(e) {
        ValidatorEnable(document.getElementById('<%=rfvAddress.ClientID%>'), e);
        ValidatorEnable(document.getElementById('<%=rfvZipcode.ClientID%>'), e);
    }

</script>

<ul>
    <li>
        <label class="desc">
            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                Display="Dynamic" ErrorMessage="*" />
            <asp:Label ID="lblAddress" runat="server" Text="Dirección:" />
        </label>
        <span>
            <asp:TextBox ID="txtAddress" runat="server" Width="300px" MaxLength="100" />
        </span><span>
            <uc2:Tooltip ID="ttpAddress" runat="server" ToolTip="Introduce las calles del lugar donde<br />la empresa expide los comprobantes" />
        </span></li>
    <li>
        <label class="desc">
            <asp:Label ID="lblExternalNumber" runat="server" Text="Número externo:" />
        </label>
        <span>
            <asp:TextBox ID="txtExternalNumber" runat="server" MaxLength="6" 
            Width="45px" />
        </span>
        <uc2:Tooltip ID="ttpExternalNumber" runat="server" ToolTip="Introduce el Número exterior del lugar (edificio, local, terreno),<br />en donde la empresa expide los comprobantes. " />
    </li>
    <li>
        <label class="desc">
            <asp:Label ID="lblInternalNumber" runat="server" Text="Número interno:" />
        </label>
        <span>
            <asp:TextBox ID="txtInternalNumber" runat="server" MaxLength="6" 
            Width="45px" />
        </span>
        <uc2:Tooltip ID="ttpInternalNumber" runat="server" ToolTip="Introduce el Número interior del lugar (edificio, local, terreno),<br />en donde la empresa expide los comprobantes." />
    </li>
    <li>
        <label class="desc">
            <asp:Label ID="lblColony" runat="server" Text="Colonia:" />
        </label>
        <span>
            <asp:TextBox ID="txtColony" runat="server" MaxLength="25" Width="200px"></asp:TextBox>
        </span>
        <uc2:Tooltip ID="Tooltip1" runat="server" ToolTip="Introduce el nombre de la Colonia de donde<br />la empresa expide los comprobantes." />
    </li>
    <li>
        <uc1:CountrySelector ID="CountrySelector1" runat="server" ToolTipCountry="Selecciona el nombre del País de donde<br />la empresa expide los comprobantes."
            ToolTipState="Selecciona el nombre del Estado de donde<br />la empresa expide los comprobantes."
            ToolTipCity="Selecciona el nombre de la Ciudad de donde<br />la empresa expide los comprobantes." />
    </li>
    <li>
        <label class="desc">
            <asp:Label ID="lblReference" runat="server" Text="Referencia:" />
        </label>
        <span>
            <asp:TextBox ID="txtReference" runat="server" MaxLength="255" 
            Width="200px" />
        </span>
        <uc2:Tooltip ID="ttpReference" runat="server" ToolTip="Introduce alguna referencia que ayude a ubicar más fácilmente<br />el lugar donde la empresa expide los comprobantes.<br />Ej. <b>Una calle, un edificio, un monumento, etc.</b>" />
    </li>
    <li>
        <label class="desc">
            <asp:RequiredFieldValidator ID="rfvZipcode" runat="server" ControlToValidate="txtZipcode"
                Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
            <asp:Label ID="lblZipcode" runat="server" Text="Código postal:" />
        </label>
        <span>
            <asp:TextBox ID="txtZipcode" runat="server" MaxLength="10" Width="80px" />
        </span>
        <uc2:Tooltip ID="ttpZipcode" runat="server" ToolTip="Introduce el Código postal de donde<br />la empresa expide los comprobantes." />
    </li>
</ul>
