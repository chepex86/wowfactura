﻿
if (jcPACFD != null && jcPACFD.BillersEditor == null) {

    function jclassBillersEditor() {
        var that = this;
        var params = jcPACFD.getStartingJavaScriptTagParameters();

        this.onHideTaxSystem = function(cmboid, validid, visible) {
            var c = $get(cmboid);

            c.style.display = visible ? "block" : "none";
            ValidatorEnable(validid, visible);
        }
    }

    jcPACFD.BillersEditor = new jclassBillersSearch();
}