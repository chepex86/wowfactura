﻿<%@ Page Title="Eliminar empresa." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillersDelete.aspx.cs" Inherits="PACFDManager.Billers.BillersDelete" %>

<%@ Register Src="BillersEditor.ascx" TagName="BillersEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <table>
        <tr>
            <td>
                <div id="container">
                    <div class="form">
                        <div class="formStyles">
                            <div class="info">
                                <label class="desc">
                                    <h2>
                                        <asp:Label ID="lblTitle" runat="server" Text="¿Desea elimiar al empresa?" Font-Size="24px"></asp:Label>
                                    </h2>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
                <uc1:BillersEditor ID="BillersEditor1" runat="server" EditorMode="View" />
            </td>
        </tr>
        <tr style="text-align: center">
            <td>
                <table>
                    <tr>
                        <td style="width: 100%">
                        </td>
                        <td>
                            <asp:Button ID="btnAcept" runat="server" Text="Eliminar" OnClick="btnAcept_Click" />
                        </td>
                        <td style="text-align: center">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
