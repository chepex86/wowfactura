﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingTaxTypeSelector.ascx.cs"
    Inherits="PACFDManager.Billings.BillingTaxTypeSelector" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<ul>
    <li>
        <label class="desc">
            <a class="vtip" title="Régimen fiscal a usar en el comprobante.<br />.">
                <asp:Label ID="lblFiscalRegime" runat="server" Text="Régimen Fiscal" Visible="false" />
            </a>
        </label>
        <span><a class="vtip" title="Régimen fiscal a usar en el comprobante.">
            <asp:DropDownList ID="ddlFiscalRegime" runat="server" Visible="false" OnSelectedIndexChanged="ddlFiscalRegime_OnChange" AutoPostBack="true">
            </asp:DropDownList>
        </a></span>

    </li>
    <li>
        <label class="desc">
            <a class="vtip" title="La configuración a usar en el comprobante.<br />La configuración específica los impuestos a mostrar.">
                <asp:Label ID="lblTaxTemplate" runat="server" Text="Configuración" />
            </a>
        </label>
        <span><a class="vtip" title="La configuración a usar en el comprobante.<br />La configuración específica los impuestos a mostrar.">
            <asp:DropDownList ID="ddlBillingTaxTemplate" runat="server">
            </asp:DropDownList>
        </a></span></li>
</ul>
<div>
    <asp:Literal ID="ltrDescription" runat="server" />
</div>
<br />
<br />
<div>
    <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click" />
    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
        CssClass="Button" />
</div>
