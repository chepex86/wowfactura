﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingPDFXMLList.aspx.cs"
    Inherits="PACFDManager.Billings.BillingPDFXMLList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divQuestion" runat="server">
        ¿Esta seguro que desea generar una lista con loas archivos de PDF y XML?
        <br />
        <asp:Button ID="btnAcept" runat="server" Text="Aceptar" OnClick="btnAcept_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" />
    </div>
    <div id="divListado" runat="server">
        <div>
            Listado de Archivos PDF y XML por empresa.
        </div>
        <asp:GridView ID="gdvFiles" runat="server" EnableViewState="false" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a id="aPDF" runat="server" rel="_self" visible='<%# this.IsFormatVisible("0") %>' href='<%# "XML/DownloadPage.aspx?type=pdf&b=" + this.DataBindEncryptBillingAndBillerID(Eval("BillingID").ToString()) + "&a=" + this.DataBindEncryptBillingAndBillerID(Eval("BillerID").ToString()) + "o=false"%>'>
                            <%#string.Format("PDF: {0}, {1} - {2}", this.GetBillerRFC(), this.Eval("Serial"), this.Eval("Folio"))%>
                        </a>
                        <br />
                        <a id="aXML" runat="server" rel="_self" visible='<%# this.IsFormatVisible("1") %>' href='<%# "XML/DownloadPage.aspx?type=xml&b=" + this.DataBindEncryptBillingAndBillerID(Eval("BillingID").ToString()) + "&a=" + this.DataBindEncryptBillingAndBillerID(Eval("BillerID").ToString()) + "o=false"%>'>
                            <%#string.Format("XML: {0}, {1} - {2}", this.GetBillerRFC(), this.Eval("Serial"), this.Eval("Folio")) %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
