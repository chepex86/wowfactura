<%@ Page Title="Listado de comprobantes." Language="C#" MasterPageFile="~/Default.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="BillingsList.aspx.cs"
    Inherits="PACFDManager.Billings.BillingsList" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="BillingsSearch.ascx" TagName="BillingsSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="BillingsList_Load" style="position: fixed; top: 45%; left: 45%; border-width: 1px;
        border-style: solid; display: none; background-color: #ecf0f8;">
        <img src="../Includes/Images/ajax-loader.gif" alt="Cargando..." />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc1:BillingsSearch ID="BillingsSearch1" runat="server" OnSearch="BillingsSearch1_Search" />
            </div>
            <br />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblBillingsListTitle" runat="server" Text="Listado de Comprobantes" /></h2>
                </div>
                <br />
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: right">
                            <span class="vtip" title="Agrega un nuevo comprobante <b>sellado</b>.">
                                <asp:Button ID="btnAdd" runat="server" Text="Agregar comprobante" OnClick="btnAdd_Click"
                                    CausesValidation="False" CssClass="Button" CommandArgument="0" />
                            </span>&nbsp;&nbsp;<span class="vtip" title="Agrega precomprobante <b>no foliado</b>.">
                                <asp:Button ID="btnPreBillingNotSerialFolio" runat="server" Text="Agregar precomprobante no foliado"
                                    OnClick="btnAdd_Click" CausesValidation="False" CssClass="Button" CommandArgument="2" />
                            </span>&nbsp;&nbsp;<span class="vtip" title="Agrega un precomprobante <b>foliado</b>.">
                                <asp:Button ID="btnPreBilling" runat="server" Text="Agregar precomprobante foliado"
                                    CausesValidation="False" CssClass="Button" OnClick="btnAdd_Click" CommandArgument="1" />
                            </span>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gdvBillings" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                    AllowPaging="true" PageSize="50" AllowSorting="true" OnPageIndexChanging="gdvBillings_PageIndexChanging"
                    OnSorting="gdvBillings_Sorting" OnRowDataBound="gdvBillings_RowDataBound">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="vtip" title="Numero de control interno en el sistema." style="cursor: help;">
                                    <%# Eval("BillingID")  %>
                                </span>
                                <br />
                                <span class="vtip" title="Muestra el numero se <b>serie</b> - <b>folio</b> usados."
                                    style="cursor: help; color: #007d00">
                                    <%#Eval("Serial") + "-" + Eval("Folio")%>
                                </span>
                                <br />
                                <span class="vtip" title="Muestra el <b>folio externo</b>." style="cursor: help;
                                    color: #00638f">
                                    <%# Eval("ExternalFolio") %>
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="UUID">
                            <HeaderTemplate>
                                <span class="vtip" title="Muestra el numero de <b>folio fiscal</b> usado." style="width: 100%">
                                    <a style="color: White; width: 100%" href='<%#GetSortJavaScript("UUID")%>'>
                                        <asp:Label ID="lblUIIDHeader" runat="server" Text="UUID" />
                                        <asp:Image ID="imgUUIDColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                            Visible='<%#GetColumnSortedVisible("UUID") %>' />
                                    </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUUID" runat="server" Text='<%#string.Format("{0}", Eval("UUID"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cliente">
                            <HeaderTemplate>
                                <span class="vtip" title="Muestra el <b>RFC</b>, <b>nombre</b> del cliente y la direcci�n fisica registrada ante el <b>SAT</b>.">
                                    <asp:Label ID="lblClientHeader" runat="server" Text="Cliente" />
                                </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="text-align: left">
                                    <asp:Label ID="lblReceptorRFC" runat="server" Text='<%#Eval("ReceptorRFC")%>' />
                                </div>
                                <div style="text-align: left">
                                    <asp:Label ID="lblReceptorName" runat="server" Text='<%# PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString())%>' />
                                    <br />
                                    <asp:Label ID="lblReceptorAddress" runat="server" Text='<%#Eval("ReceptorAddress")%>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comprobante" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <span class="vtip" title="Ordenar por tipo de <b>(pre)comprobante</b>."><a style="color: White;
                                    width: 100%" href='<%#GetSortJavaScript("TaxTemplateName")%>'>
                                    <asp:Label ID="lblBillingTaxTemplateNameHeader" runat="server" Text="Comprobante" />
                                    <asp:Image ID="imgTaxTemplateColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                        Visible='<%#GetColumnSortedVisible("TaxTemplateName") %>' />
                                </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title="Indica el tipo de <b>(pre)comprobante</b>." style="cursor: help;
                                    color: #00638f">
                                    <%#Eval("TaxTemplateName") %>
                                </span>
                                <br />
                                <span class="vtip" title="Indica si es de tipo ingreso o egreso." style="cursor: help;
                                    color: #007d00"><b>
                                        <%#Eval("BillingType").ToString().ToLower() == "ingreso" || Eval("BillingType").ToString().ToLower() == "i" ? "Ingreso" : "Egreso" %></b> </span>
                                <br />
                                <span class="vtip" title="Indica si es un <b>precomprobante</b> o un <b>comprobante</b>."
                                    style="cursor: help; color: #00638f">
                                    <%#Eval("PreBilling").ToString() == "True" ? "Pre-Comprobante" : "Comprobante" %>
                                </span>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <span class="vtip" title="Fecha de creaci�n del <b>(pre)comprobante</b>."><a style="color: White;
                                    width: 100%" href='<%#GetSortJavaScript("BillingDate")%>'>
                                    <asp:Label ID="lblPreBillingDateHeader" runat="server" Text="Fecha" />
                                    <asp:Image ID="imgBillingDateColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                        Visible='<%#GetColumnSortedVisible("BillingDate") %>' />
                                </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBillingDate" runat="server" Text='<%#string.Format("{0:dddd dd/MMM/yyyy HH:mm:ss}", Eval("BillingDate")) %>' />
                                <div id="divChangeDate" runat="server" visible='<%# (((int)PACFDManager.Security.Security.CurrentElectronicBillingType) == 2) %>'>
                                    <br />
                                    <span class="vtip" title="Modifica la fecha del <b>comprobante cbb</b>.">
                                        <asp:Button ID="ibtnChangeDate" runat="server" Text="Editar fecha" CssClass="Button"
                                            Visible='<%# (((int)PACFDManager.Security.Security.CurrentElectronicBillingType) == 2) %>'
                                            PostBackUrl='<%#string.Format("CBBDateModify.aspx?cbb={0}", PACFD.Common.Cryptography.EncryptUnivisitString(Eval("BillingID").ToString()))%>' />
                                    </span>
                                </div>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <HeaderTemplate>
                                <span class="vtip" title="Cantidad total en el <b>(pre)comprobante</b>"><a style="color: White;
                                    width: 100%" href='<%#GetSortJavaScript("Total")%>'>
                                    <asp:Label ID="lblTotalHeader" runat="server" Text="Total" />
                                    <asp:Image ID="imgTotalColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                        Visible='<%#GetColumnSortedVisible("Total") %>' />
                                </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='<%#string.Format("<b>{0}</b><br />Forma de pago: <b>{1}</b>.", PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()), Eval("PaymentForm")) %>'
                                    style="cursor: help;">
                                    <asp:Label ID="lblTotal" runat="server" Text='<%#string.Format("{0:c} {1}", Eval("Total"), Eval("CurrencyCode"))%>' />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pagado">
                            <HeaderTemplate>
                                <span class="vtip" title="Indica si el <b>(pre)comprobante</b> a sido pagado en su totalidad.">
                                    <asp:Label ID="lblIsPaidHeader" runat="server" Text='Pagado' />
                                </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="ltrPaid" runat="server" Text='<%#this.DataBind_IsPaid(this.GetDataItem()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Act.">
                            <HeaderTemplate>
                                <span class="vtip" title="Muestra el <b>estado</b> actual del (pre)comprobante.<br /><br />Imagen <img src='../Includes/Images/png/apply.png' /> Activo.<br />Imagen <img src='../Includes/Images/png/editdelete.png' /> Cancelado.">
                                    <a style="color: White; width: 100%" href='<%#GetSortJavaScript("Active")%>'>
                                        <asp:Label ID="lblActiveHeader" runat="server" Text="Act." />
                                        <asp:Image ID="imgActiveColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                            Visible='<%#GetColumnSortedVisible("Active") %>' />
                                    </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='<%# Eval("Active").ToString() == "True" ?  
                                string.Format("(Pre)Comprobante: <b>{0}</b>.<br />Estado: <b>Activo</b>.", Eval("Serial") + "-" + Eval("Folio")) : string.Format("ID: <b>{0}</b>.<br />Estado: <b>Cancelado</b>.", Eval("Serial") + "-" + Eval("Folio")) %>'>
                                    <asp:Image ID="imgActive" runat="server" AlternateText='<%#Eval("Active")%>' ImageUrl='<%#Eval("Active").ToString() == "True" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png"%>' />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sellado">
                            <HeaderTemplate>
                                <span class="vtip" title='<%# GetSealHeadTitleTip() %>'><a style="color: White; width: 100%"
                                    href='<%#GetSortJavaScript("PreBilling")%>'>
                                    <asp:Label ID="lblSealHeader" runat="server" Text='<%# GetSealHeadTitle() %>' />
                                    <asp:Image ID="imgSealColumn" runat="server" ImageUrl='<%#GetSortedIsUp().ToString() == "True" ? "~/Includes/Images/Png/upArrow.png" : "~/Includes/Images/Png/downArrow.png" %>'
                                        Visible='<%#GetColumnSortedVisible("PreBilling")   %>' />
                                </a></span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="vtip" title='<%#string.Format("Tipo: <b>{0}</b>.<br />Estado: <b>{1}</b>", Eval("BillingType"), Eval("PreBilling").ToString() == "True" ? "Sellar precomprobante." : "Comprobante sellado.")%>'>
                                    <asp:ImageButton ID="imbSeal" runat="server" OnClick="ImageButton_Click" AlternateText='<%#Eval("PreBilling").ToString() == "True" ? "Sellar precomprobante." : "Comprobante sellado." %>'
                                        Visible='<%#Convert.ToBoolean(Eval("Active")) %>' CommandArgument='<%#Eval("PreBilling").ToString() == "True" ? Eval("BillingID") : 0%>' />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <span class="vtip" title='<%#string.Format("<img src=\"../Includes/Images/png/kfind.png\" /> Ver detalles del <b>{0}</b>.<br />Serie: <b>{1}</b>", Eval("PreBilling").ToString() == "True" ? "pre comprobante" : "comprobante",  Eval("Serial") + "-" + Eval("Folio")) %>'>
                                                <asp:ImageButton ID="imbDetails" CommandArgument='<%#Eval("BillingID")%>' runat="server"
                                                    OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/kfind.png" AlternateText="Detalles" />
                                            </span>
                                        </td>
                                        <td>
                                            <span class="vtip" title='<%#string.Format("<img src=\"../Includes/Images/png/pdf.png\" /> Descargar archivo <b>PDF</b>.<br />Serie: <b>{0}</b>.<br />Cliente: <b>{1}</b>", Eval("Serial") + "-" + Eval("Folio"),  PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()))%>'>
                                                <a rel="_self" href='<%# "XML/DownloadPage.aspx?type=pdf&b=" + this.DataBindEncryptBillingAndBillerID(Eval("BillingID").ToString()) + "&a=" + this.DataBindEncryptBillingAndBillerID(Eval("BillerID").ToString()) + "o=false"%>'>
                                                    <img alt="Descarga PDF" src="../Includes/Images/png/pdf.png" />
                                                </a></span>
                                        </td>
                                        <td>
                                            <span class="vtip" title='<%#string.Format("<img src=\"../Includes/Images/png/pdf_copy.png\" /> Descargar copia del archivo <b>PDF</b>.<br />Serie: <b>{0}</b>.<br />Cliente: <b>{1}</b>", Eval("Serial") + "-" + Eval("Folio"),  PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()))%>'>
                                                <a rel="_self" href='<%# "XML/DownloadPage.aspx?type=pdf&b=" + this.DataBindEncryptBillingAndBillerID(Eval("BillingID").ToString()) + "&a=" + this.DataBindEncryptBillingAndBillerID(Eval("BillerID").ToString()) + "o=false&iscopy=true"%>'>
                                                    <img alt="Descarga copia de PDF" src="../Includes/Images/png/pdf_copy.png" />
                                                </a></span>
                                        </td>
                                        <td>
                                            <span class="vtip" title='<%#string.Format("<img src=\"../Includes/Images/png/sendMail.png\" /> Enviar correo a: <b>{0}</b>",  PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()))%>'>
                                                <asp:ImageButton ID="imbEmailBilling" runat="server" AlternateText="Enviar email"
                                                    ImageUrl="~/Includes/Images/png/sendMail.png" CommandArgument='<%#Eval("BillingID")%>'
                                                    OnClick="ImageButton_Click" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="vtip" title='<b>Cancela</b> el comprobante o precomprobante.<br />Para <b>reactivar</b> el comprobante podria requerir <br />de un proceso de validaci�n ante el <b>SAT</b>.<br /><br /><b>Nota: SAT</b> (Servicio de Administraci�n Tributaria.)<br /><img src="../Includes/Images/png/SAT2_29x13.png"/> <img src="../Includes/Images/png/SAT29x10.png" /> http://www.sat.gob.mx/ '>
                                                <asp:ImageButton ID="imbCancel" CommandArgument='<%#Eval("BillingID")%>' runat="server"
                                                    OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png" Visible='<%# Convert.ToBoolean(Eval("Active")) %>' />
                                            </span>
                                        </td>
                                        <td>
                                            <span class="vtip" title="El comprobante no esta <b>addendado</b>.<br />Click en la imagen <img src='../Includes/Images/png/addenda.png' /> para aplicar una addenda.">
                                                <asp:ImageButton ID="imbAddendum" runat="server" OnClick="ImageButton_Click" CommandArgument='<%#Eval("BillingID")%>'
                                                    Visible='<%#!GetAddendumApplied(Eval("BillingID"))%>' />
                                            </span><span class="vtip" title="Comprobante <b>addendado</b>">
                                                <asp:Image ID="imgAppliedAddendum" runat="server" ImageUrl="~/Includes/Images/png/apply.png"
                                                    Visible='<%#GetAddendumApplied(Eval("BillingID"))%>' />
                                            </span>
                                        </td>
                                        <td>
                                            <span class="vtip" title='<%#string.Format("Descargar archivo <b>XML</b>.<br />Serie: <b>{0}</b>.<br />Cliente: <b>{1}</b>", Eval("Serial") + "-" + Eval("Folio"),  PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()))%>'>
                                                <a rel="_self" href='<%# "XML/DownloadPage.aspx?type=xml&b=" + this.DataBindEncryptBillingAndBillerID(Eval("BillingID").ToString()) + "&a=" + this.DataBindEncryptBillingAndBillerID(Eval("BillerID").ToString()) + "o=false"%>'>
                                                    <img alt="Descarga XML" src="../Includes/Images/png/xml.png" />
                                                </a></span>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <PagerSettings Position="TopAndBottom" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                    <EmptyDataTemplate>
                        No hay comprobantes disponibles.
                    </EmptyDataTemplate>
                </asp:GridView>
                <div style="text-align: right; width: 100%">
                    <table style="float: right;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                N�mero de Registros:
                            </td>
                            <td>
                                <asp:Label ID="lblNumberBillings" runat="server" Text="0" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total:
                            </td>
                            <td>
                                <asp:Label ID="lblGlobalTotals" runat="server" Text="$0.00" />
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: right">
                            <span class="vtip" title="Agrega un nuevo comprobante <b>sellado</b>.">
                                <asp:Button ID="btnAdd2" runat="server" Text="Agregar comprobante" OnClick="btnAdd_Click"
                                    CausesValidation="False" CssClass="Button" CommandArgument="0" />
                            </span>&nbsp;&nbsp;<span class="vtip" title="Agrega precomprobante <b>no foliado</b>."><asp:Button
                                ID="btnPreBillingNotSerialFolio2" runat="server" Text="Agregar precomprobante no foliado"
                                OnClick="btnAdd_Click" CausesValidation="False" CssClass="Button" CommandArgument="2" />
                            </span>&nbsp;&nbsp;<span class="vtip" title="Agrega un comprobante <b>foliado</b>."><asp:Button
                                ID="btnPreBilling2" runat="server" Text="Agregar precomprobante foliado" CausesValidation="False"
                                CssClass="Button" OnClick="btnAdd_Click" CommandArgument="1" />
                            </span>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <uc2:WebMessageBox ID="wmbCancelBilling" runat="server" OnClick="wmbCancelBilling_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
