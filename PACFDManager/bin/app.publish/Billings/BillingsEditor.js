﻿function AutocompleteLocation_itemSelected(sender, e) {
    document.getElementById(hflArticleSearchID).value = e.get_value();
    __doPostBack(hflArticleSearchID, "");
}

function jclassBillingsEditor(aspx) {
    var that = this;

    //when mxn is selected hide exchange rate text box
    this.OnCurrencyTypeChanged = function() {
        var x = jQuery("#" + aspx.ddlCurrencyType);

        if (x.length < 1)
            return;

        switch (x[0].selectedIndex) {
            default:
            case 0:
                jQuery("#" + aspx.lblExchangeRate).css("display", "none");
                ValidatorEnable($get(aspx.txtExchangeRate_RangeValidator), false);
                break;
            case 1:
                jQuery("#" + aspx.lblExchangeRate).css("display", "block");
                ValidatorEnable($get(aspx.txtExchangeRate_RangeValidator), true);
                break;
        }
    }

    this.OnddlPaymentMethodChanged = function() {
        var x = jQuery("#" + aspx.ddlPaymentMethod);

        if (x.length < 1)
            return;

        if (x[0].selectedIndex < 3) {
            jQuery("#" + aspx.txtAccountNumberPayment).css("display", "none");
            jQuery("#" + aspx.lblAccountNumberPayment).css("display", "none");
        }
        else {
            jQuery("#" + aspx.txtAccountNumberPayment).css("display", "block");
            jQuery("#" + aspx.lblAccountNumberPayment).css("display", "block");
        }
    }

    //this.OnShowDateSelector = function(e) {
    //}

    //fired the changed field for concept search
    //    this.AutoCompletSelectedItem = function(sender) {
    //        var o = document.getElementById(sender);

    //        if (o != null)
    //            o.value = "1";

    //        __doPostBack(sender, "");
    //    }

    //hidde (!) image when a concept is found.
    this.OnClientShow = function() {
        var x = jQuery("#" + aspx.spanNotConceptFound);

        if (x.length < 1)
            return;

        x.css("display", "none");
    }
    //show (!) image when a concept is found.
    this.OnClientHidde = function() {
        var e = null;
        var x = jQuery("#" + aspx.spanNotConceptFound);

        if (x.length < 1)
            return;

        x.css("display", "block");

        for (var i = 0; i < x[0].children.length; i++) {
            if (x[0].children[i].id == "ainnerTip") {
                e = x[0].children[i];
                break;
            }
        }

        if (e != null) {
            x = jQuery("#" + aspx.txtAddConceptDescription);

            if (x[0].value.length > 0)
                e.title = "Concepto no encontrado.<br />Intente con otra palabra.";
            else
                e.title = "No hay ningun concepto a buscar.<br />Escriba en el campo el concepto a buscar.";
        }
    }
/*
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
        OnddlPaymentMethodChanged();
    });
    */
}