﻿
if (jcPACFD != null && jcPACFD != undefined) {
    function jclassBillingsAdd() {
        var that = this;
        var params = jcPACFD.getStartingJavaScriptTagParameters();

        this.OpenPreviewWindow = function(url) {
            var l = window.open(url);
        }

        this.onInitializeRequest = function() {
            var x = jQuery('[id$="divValidationsKeysIsNotValid"]');
            var b = 0;

            if (x.length > 0 && typeof Page_IsValid != 'undefined') {

                ValidatorUpdateIsValid();

                x[0].style.display = Page_IsValid ? "none" : "Block";
                x[0].innerHtml = "Uno o más campos son invalidos";
            }
        }
    }

    if (jcPACFD.jcBillingsAdd == null || jcBillingsAdd == undefined)
        jcPACFD.jcBillingsAdd = new jclassBillingsAdd();
}