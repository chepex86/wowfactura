﻿

if (jcPACFD.BillingList == null || jcPACFD.BillingList == undefined) {

    function BillingListJclass() {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var param = jcPACFD.getStartingJavaScriptTagParameters();

        this.onInitializeRequest = function() { jQuery('[id$="BillingsList_Load"]')[0].style.display = "block"; }
        this.onEndRequest = function() { jQuery('[id$="BillingsList_Load"]')[0].style.display = "none"; }

        prm.add_initializeRequest(this.onInitializeRequest);
        prm.add_endRequest(this.onEndRequest);
    }

    jcPACFD.BillingList = new BillingListJclass();
}