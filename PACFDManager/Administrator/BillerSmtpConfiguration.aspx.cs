﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Common;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class BillerSmtpConfiguration : PACFDManager.BasePage
    {
        const string MESSAGE_ERRORUPDATEINSERT = "MESSAGE_ERRORUPDATEINSERT";
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.ckbNotUseSmtpConfiguration.Checked = true;
            this.DataBind();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.txtHost_RequiredFieldValidator.Enabled =
                            this.txtPort_RequiredFieldValidator.Enabled =
                            this.txtCredentialName_RequiredFieldValidator.Enabled =
                            this.txtCredentialName_RegularExpressionValidator.Enabled =
                            this.txtCredentialPassword_RequiredFieldValidator.Enabled =
                            this.txtCredentialPasswordVerification_RequiredFieldValidator.Enabled =
                            this.txtCredentialPasswordVerification_CompareValidator.Enabled =
                            this.txtDisplayName_RequiredFieldValidator.Enabled = !(this.ckbUseBaseEmail.Checked || this.ckbNotUseSmtpConfiguration.Checked);

            this.ckbEnableSsl.Style["display"] =
                this.txtCredentialName.Style["display"] =
                this.txtPort.Style["display"] =
                this.txtHost.Style["display"] =
                this.txtDisplayName.Style["display"] =
                this.txtCredentialPasswordVerification.Style["display"] =
                this.txtCredentialPassword.Style["display"] =
                this.txtBcc.Style["display"] = (this.ckbUseBaseEmail.Checked || this.ckbNotUseSmtpConfiguration.Checked) ? "none" : "block";

            this.txtDisplayName.Style["display"] = this.ckbNotUseSmtpConfiguration.Checked ? "none" : "block";

            if (this.IsPostBack)
                return;

            this.ckbUseBaseEmail.Attributes["onclick"] =
                string.Format("javascript:{0}_jcBillerSmtpConfiguration.UseBaseEmail_Click()", this.ClientID);
            this.ckbNotUseSmtpConfiguration.Attributes["onclick"] =
                string.Format("javascript:{0}_jcBillerSmtpConfiguration.SmtpConfiguration_Click()", this.ClientID);
        }

        public override void DataBind()
        {
            PACFD.Rules.BillerSmtpConfiguration smtp;

            base.DataBind();

            smtp = new PACFD.Rules.BillerSmtpConfiguration();

            using (PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable table = smtp.SelectByBillerID(this.CurrentBillerID))
            {
                if (table.Count < 1)
                    return;

                if (table[0].UseBase)
                    this.ckbNotUseSmtpConfiguration.Checked = !table[0].IsSmtpEnabled;

                if ((this.ckbUseBaseEmail.Checked = table[0].UseBase))
                    return;

                this.ckbEnableSsl.Checked = table[0].EnableSsl;
                this.txtPort.Text = table[0].Port.ToString();
                this.txtHost.Text = table[0].Host;
                this.txtDisplayName.Text = table[0].Name;
                this.txtCredentialPassword.Text = PACFD.Common.Cryptography.DecryptUnivisitString(table[0].PasswordCredential);
                this.txtBcc.Text = table[0].IsBccNull() ? string.Empty : table[0].Bcc;
                this.txtCredentialName.Text = table[0].NameCredential;
                this.ckbUseBaseEmail.Checked = table[0].UseBase;

            }
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            PACFD.Rules.BillerSmtpConfiguration b = new PACFD.Rules.BillerSmtpConfiguration();
            PACFD.Rules.SmtpConfiguration smtprul;
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable smtptable;
            bool isnew = false;

            this.WebMessageBox1.CommandArguments = MESSAGE_ERRORUPDATEINSERT;
            PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable table = b.SelectByBillerID(this.CurrentBillerID);
            PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable tableTemp = table;

            if (table.Count < 1)
            {
                table = new PACFD.DataAccess.BillerSmtpConfigurationDataSet.BillerSmtpConfigurationDataTable();
                isnew = true;
            }

            if (this.ckbUseBaseEmail.Checked || this.ckbNotUseSmtpConfiguration.Checked)
            {
                if (table.Count < 1)
                {
                    table.AddBillerSmtpConfigurationRow(string.Empty, 0, string.Empty, string.Empty, string.Empty, false, string.Empty, false, this.CurrentBillerID);
                }
                else
                {
                    table[0].EnableSsl = false;
                    table[0].Bcc =
                    table[0].Host =
                    table[0].Name =
                    table[0].NameCredential =
                    table[0].PasswordCredential = string.Empty;
                    table[0].Port = 0;
                    table[0].UseBase = false;
                }

                if (this.ckbUseBaseEmail.Checked)
                {
                    smtprul = new PACFD.Rules.SmtpConfiguration();

                    using (smtptable = smtprul.Select())
                    {
                        if (smtptable.Count < 0)
                        {
                            this.WebMessageBox1.ShowMessage("No hay configuración base.", true);
                            table.Dispose();
                            table = null;
                            return;
                        }

                        table[0].Bcc = smtptable[0].Bcc;
                        table[0].EnableSsl = smtptable[0].EnableSsl;
                        table[0].Host = smtptable[0].Host;
                        table[0].Name = smtptable[0].Name;
                        table[0].NameCredential = smtptable[0].NameCredential;
                        table[0].PasswordCredential = smtptable[0].PasswordCredential;
                        table[0].Port = smtptable[0].Port;
                        table[0].UseBase = true;
                    }
                }
            }
            else
            {
                if (!this.Page.IsValid)
                {
                    table.Dispose();
                    table = null;
                    this.WebMessageBox1.ShowMessage("Hay un error en el llenado de los campos.", true);
                    return;
                }

                if (table.Count < 1)
                {
                    table.AddBillerSmtpConfigurationRow(this.txtHost.Text, int.Parse(this.txtPort.Text), this.txtCredentialName.Text,
                           PACFD.Common.Cryptography.EncryptUnivisitString(this.txtCredentialPassword.Text),
                           this.txtBcc.Text, this.ckbEnableSsl.Checked, this.txtDisplayName.Text, false,
                           this.CurrentBillerID);
                }
                else
                {
                    table[0].Bcc = this.txtBcc.Text;
                    table[0].EnableSsl = this.ckbEnableSsl.Checked;
                    table[0].Host = this.txtHost.Text;
                    table[0].Name = this.txtDisplayName.Text;
                    table[0].NameCredential = this.txtCredentialName.Text;
                    table[0].PasswordCredential = PACFD.Common.Cryptography.EncryptUnivisitString(this.txtCredentialPassword.Text);
                    table[0].Port = int.Parse(this.txtPort.Text);
                    table[0].UseBase = this.ckbUseBaseEmail.Checked;
                }
            }

            table[0].AcceptChanges();

            if (isnew)
                table[0].SetAdded();
            else
                table[0].SetModified();

            if (!b.Update(table))
            {
                table.Dispose();
                table = null;
                this.WebMessageBox1.ShowMessage("Hay un error en el llenado de los campos.", true);
                return;
            }

            DataSet ds = new DataSet();
            DataSet dsTemp = new DataSet();
            ds.Tables.Add(table);
            dsTemp.Tables.Add(tableTemp);

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} actualizo la configuración del smtp.",
                this.UserName), this.CurrentBillerID, (isnew ? null : dsTemp.GetXml()), ds.GetXml());
            #endregion

            ds.Dispose();
            dsTemp.Dispose();

            this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
            this.WebMessageBox1.ShowMessage("Configuración exitosa de smtp.", false);
            table.Dispose();
            table = null;
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_SUCCESS:
                    this.btnCancel_Click(null, EventArgs.Empty);
                    break;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("~/" + typeof(PACFDManager.Default).Name + ".aspx");
        }
    }
}