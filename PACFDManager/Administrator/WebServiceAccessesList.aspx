﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="WebServiceAccessesList.aspx.cs" Inherits="PACFDManager.Administrator.WebServiceAccessesList" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe50">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Acceso a Webservice" />
                            </h2>
                        </div>
                        <div>
                            <div style="text-align: left;">
                                <asp:Button ID="btnAdd" runat="server" Text="Agregar" CssClass="Button" OnClick="btnAdd_Click" />
                            </div>
                            <asp:GridView ID="gdvWeb" runat="server" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="vtip" title="Nombre del usuario y empresa ligada.">
                                                <asp:Label ID="lblUserTitle" runat="server" Text="Nombre y Empresa" />
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblUser" runat="server" Text='<%# string.Format("{0} - {1}", Eval("Name"), Eval("Biller"))  %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="vtip" title="Estado activo actual del usuario.">
                                                <asp:Label ID="lblActiveTitle" runat="server" Text="Estado" />
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="vtip" title='<%# string.Format("El estado del usuario es <b>{0}</b>,<br />hacer click aqui para cambiar su estado.", Eval("Active").ToString() == "True" ? "Activo" : "Inactivo") %>'>
                                                <asp:ImageButton ID="imbActive" CommandArgument='<%#string.Format("{0},{1}" , Eval("Active"), Eval("WebServiceAccessesID"))%>'
                                                    runat="server" ImageUrl='<%# Eval("Active").ToString() == "True" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png"%>'
                                                    OnClick="imbActive_Click" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="vtip" title="Elimina por completo el usuario.">
                                                <asp:Label ID="lblActiveTitle" runat="server" Text="Modificar" />
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="vtip" title='<%#string.Format("Para modificar al usuario <b>{0}</b>,<br />hacer click aquí.",Eval("Name"))  %>'>
                                                <asp:ImageButton ID="imbEdit" CommandArgument='<%#Eval("WebServiceAccessesID")%>'
                                                    runat="server" ImageUrl="~/Includes/Images/png/editIcon.png" OnClick="imbEdit_Click" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <div class="vtip" title="Elimina por completo el usuario.">
                                                <asp:Label ID="lblActiveTitle" runat="server" Text="Eliminar" />
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="vtip" title='<%#string.Format("Para eliminar completamente el usuario <b>{0}</b>,<br />hacer click aquí.",Eval("Name"))  %>'>
                                                <asp:ImageButton ID="imbCancel" CommandArgument='<%#Eval("WebServiceAccessesID")%>'
                                                    runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="imbCancel_Click" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                            <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
                            <div style="text-align: left;">
                                <asp:Button ID="btnAdd2" runat="server" Text="Agregar" CssClass="Button" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
