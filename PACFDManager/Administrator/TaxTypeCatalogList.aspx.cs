﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTypeCatalogList : PACFDManager.BasePage
    {
        private int TemporalTaxTypeID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.TaxTypes tax = new PACFD.Rules.TaxTypes();

            this.gdvTaxes.DataSource = tax.SelectAllTaxTypes();
            this.gdvTaxes.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e) { Response.Redirect(typeof(TaxTypeCatalogAdd).Name + ".aspx"); }

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton b = sender as ImageButton;
            int id;

            if (b.IsNull())
                return;

            if (!int.TryParse(b.CommandArgument, out id))
                return;

            this.TemporalTaxTypeID = id;

            switch (b.ID)
            {
                case "imgbEdit":
                    this.Session["a"] = this.TemporalTaxTypeID;
                    this.Response.Redirect(typeof(TaxTypeCatalogModify).Name + ".aspx");
                    break;
                case "imgbDelete":
                    this.wmbDelete.CommandArguments = "delete";
                    this.wmbDelete.ShowMessage("¿Desea eliminar el impuesto?",
                        System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
            }
        }

        protected void wmbDelete_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.TaxTypes taxtype;
            PACFD.Rules.TaxTemplate template;
            //PACFD.DataAccess.TaxTypesDataSet.TaxTypesDataTable taxtable;            
            PACFD.DataAccess.TaxTemplatesDataSet.IsTaxTypeUsedDataTable istaxusedtemplate;
            PACFD.DataAccess.TaxTypesDataSet taxtypedataset;
            PACFD.DataAccess.TaxTypesDataSet taxtypedatasettemp;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            switch (this.wmbDelete.CommandArguments)
            {
                case "delete":

                    if (this.TemporalTaxTypeID < 1)
                        return;

                    template = new PACFD.Rules.TaxTemplate();
                    istaxusedtemplate = template.SelectTaxTypesInUse(this.TemporalTaxTypeID);

                    if (istaxusedtemplate.Count > 0)
                    {
                        this.wmbDelete.CommandArguments = string.Empty;
                        this.wmbDelete.ShowMessage("Impuesto en uso por configuración.",
                            System.Drawing.Color.Blue, WebMessageBoxButtonType.Accept);
                        istaxusedtemplate.Dispose();
                        istaxusedtemplate = null;
                        break;
                    }

                    istaxusedtemplate = null;
                    taxtype = new PACFD.Rules.TaxTypes();
                    taxtypedatasettemp = taxtypedataset = taxtype.SelectTaxByTaxTypeID(this.TemporalTaxTypeID);

                    if (taxtypedataset.TaxTypes[0].Name.ToLower() == "iva" || taxtypedataset.TaxTypes[0].Name.ToLower() == "ieps")
                    {
                        this.wmbDelete.CommandArguments = string.Empty;
                        this.wmbDelete.ShowMessage("No se puede eliminar este impuesto",
                            System.Drawing.Color.Blue, WebMessageBoxButtonType.Accept);
                        taxtype = null;
                        taxtypedataset.Dispose();
                        taxtypedataset = null;
                        break;
                    } 

                    for (int i = 0; i < taxtypedataset.TaxTypes.Count; i++)
                    {
                        if (taxtypedataset.TaxTypes[i].RowState == System.Data.DataRowState.Deleted)
                            continue;

                        if (taxtypedataset.TaxTypes[i].TaxTypeID != this.TemporalTaxTypeID)
                            continue;

                        for (int i2 = 0; i2 < taxtypedataset.TaxValues.Count; i2++)
                        {
                            if (taxtypedataset.TaxValues[i2].RowState == System.Data.DataRowState.Deleted)
                                continue;

                            if (taxtypedataset.TaxValues[i2].TaxTypeID != taxtypedataset.TaxTypes[i].TaxTypeID)
                                continue;

                            taxtypedataset.TaxValues[i2].BeginEdit();
                            taxtypedataset.TaxValues[i2].Delete();
                            taxtypedataset.TaxValues[i2].EndEdit();
                        }

                        taxtypedataset.TaxTypes[i].BeginEdit();
                        taxtypedataset.TaxTypes[i].Delete();
                        taxtypedataset.TaxTypes[i].EndEdit();
                        break;
                    }

                    if (!taxtype.Update(taxtypedataset))
                    {
                        this.wmbDelete.CommandArguments = string.Empty;
                        this.wmbDelete.ShowMessage(
                            "No se puede eliminar este impuesto. Posible problema: Este impuesto esta siendo usado por una configuración.",
                            System.Drawing.Color.Blue, WebMessageBoxButtonType.Accept);
                        return;
                    }

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino un tipo de impuesto del catalogo.",
                        this.UserName), this.CurrentBillerID, taxtypedatasettemp.GetXml(), taxtypedataset.GetXml());
                    #endregion

                    taxtype = null;
                    taxtypedataset = null;

                    this.wmbDelete.CommandArguments = string.Empty;

                    this.Response.Redirect(typeof(TaxTypeCatalogList).Name + ".aspx");
                    break;
            }
        }
    }
}