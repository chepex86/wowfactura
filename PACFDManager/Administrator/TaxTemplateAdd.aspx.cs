﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
using System.Data;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTemplateAdd : PACFDManager.BasePage
    {
        const string MESSAGE_QUESTION_ADDNEW = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.TaxTemplateEditor1.EditorMode = TaxTemplateEditorMode.Add;

            if (!this.Request.QueryString["template"].IsNull())
            {
                this.Session["templateId"] = this.Request.QueryString["template"].ToString();
                this.TaxTemplateEditor1.EditorMode = TaxTemplateEditorMode.SelectBase;
            }

            //if (this.CurrentBillerID < 1)
            //    this.Response.Redirect("..\\Default.aspx");

            if (this.IsPostBack)
                return;

            this.TaxTemplateEditor1.DataBindTaxes();
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments == MESSAGE_QUESTION_ADDNEW && e.DialogResult == WebMessageBoxDialogResultType.Yes)
            {
                this.Response.Redirect(typeof(TaxTemplateAdd).Name + ".aspx");
                return;
            }

            this.btnCancel_Click(this, EventArgs.Empty);
        }

        protected void TaxTemplateEditor1_ApplyChanges(object sender, TaxTemplateApplyChangesEventArgs e)
        {
            PACFD.Rules.TaxTemplate template;

            if (e.EditorMode != TaxTemplateEditorMode.Add && e.EditorMode != TaxTemplateEditorMode.SelectBase)
                return;

            template = new PACFD.Rules.TaxTemplate();

            if (!template.Update(e.DataSet))
            {
                template = null;
                this.WebMessageBox1.CommandArguments = string.Empty;
                this.WebMessageBox1.ShowMessage("No se pudo registrar la configuración.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego una configuración de impuesto al sistema.",
                this.UserName), this.CurrentBillerID, null, e.DataSet.GetXml());
            #endregion

            this.WebMessageBox1.CommandArguments = MESSAGE_QUESTION_ADDNEW;
            this.WebMessageBox1.ShowMessage("Registro de configuración éxitoso. ¿Desea agragar otra configuración?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                this.TaxTemplateEditor1.InvokeApplyChanges();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxTemplateList).Name + ".aspx"); }
    }
}