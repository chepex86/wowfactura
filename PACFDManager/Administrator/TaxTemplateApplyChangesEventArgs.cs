﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public class TaxTemplateApplyChangesEventArgs : EventArgs
    {
        public PACFD.DataAccess.TaxTemplatesDataSet DataSet { get; private set; }
        //public System.Web.UI.WebControls.ListItemCollection AvailableTaxes { get; private set; }
        public TaxTemplateEditorMode EditorMode { get; private set; }


        public TaxTemplateApplyChangesEventArgs(PACFD.DataAccess.TaxTemplatesDataSet dataset,
            /*System.Web.UI.WebControls.ListItemCollection availabletax,*/ TaxTemplateEditorMode editormode)
        {
            this.DataSet = dataset;
            //this.AvailableTaxes = availabletax;
            this.EditorMode = editormode;
        }
    }
}
