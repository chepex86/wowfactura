﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTypeCatalogModify : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id;

            this.TaxTypeCatalogEditor1.EditorMode = TaxTypeCatalogEditorMode.Edit;

            if (this.IsPostBack)
                return;

            if (this.Session["a"].IsNull())
                return;

            if (!int.TryParse(this.Session["a"].ToString(), out id))
                return;

            this.TaxTypeCatalogEditor1.TaxTypeID = id;
            this.TaxTypeCatalogEditor1.DataBindFromTaxType();
            this.Session["a"] = null;
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxTypeCatalogList).Name + ".aspx"); }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            PACFD.Rules.TaxTypes taxtypes = new PACFD.Rules.TaxTypes();
            PACFD.DataAccess.TaxTypesDataSet dataset = null;
            PACFD.DataAccess.TaxTypesDataSet datasettemp = null;

            try
            {
                datasettemp = dataset = this.TaxTypeCatalogEditor1.GenerateTaxType();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return;
            }

            if (!taxtypes.Update(dataset))
            {
                this.WebMessageBox1.ShowMessage("No se pudo modificar el impuesto.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico un tipo de impuesto del catalogo.",
                this.UserName), this.CurrentBillerID, datasettemp.GetXml(), dataset.GetXml());
            #endregion

            this.Response.Redirect(typeof(TaxTypeCatalogList).Name + ".aspx");
        }
    }
}