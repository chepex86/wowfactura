﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class SmtpConfiguration : PACFDManager.BasePage
    {
        public int SmtpID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }

        public string PassWord
        {
            get
            {
                string r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                r = o.ToString().Trim();

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {
                MaintainValuePassword();
            }
            else
            {
                this.DataBindSmtp();
                MaintainValuePassword();
            }

        }

        private void MaintainValuePassword()
        {
            txtCredentialPassword.Attributes.Add("value", PassWord);
            txtCredentialPasswordVerification.Attributes.Add("value", PassWord);
        }

        private void DataBindSmtp()
        {

            PACFD.Rules.SmtpConfiguration c;
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable t;

            c = new PACFD.Rules.SmtpConfiguration();
            using (t = c.Select())
            {
                c = null;

                PassWord = string.Empty;

                if (t.Count < 1)
                    return;

                this.txtBcc.Text = t[0].Bcc;
                this.txtCredentialName.Text = t[0].NameCredential;

                this.txtCredentialPassword.Text = PassWord =
                    PACFD.Common.Cryptography.DecryptUnivisitString(t[0].PasswordCredential);

                this.txtDisplayName.Text = t[0].Name;
                this.txtHost.Text = t[0].Host;
                this.txtPort.Text = t[0].Port.ToString();
                this.ckbEnableSsl.Checked = t[0].EnableSsl;

                this.SmtpID = t[0].SmtpConfigurationId;
                this.ChkBoxDisabled.Checked = !t[0].IsSmtpEnabled;
            }
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            PACFD.Rules.SmtpConfiguration c;
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable t =
                new PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationDataTable();
            PACFD.DataAccess.SmtpConfigurationDataSet.SmtpConfigurationRow row;


            if (this.txtCredentialPassword.Text != this.txtCredentialPasswordVerification.Text)
            {
                this.WebMessageBox1.ShowMessage("El password no conicide.", true);
                return;
            }

            row = t.NewSmtpConfigurationRow();

            if (this.ChkBoxDisabled.Checked)
            {
                row.PasswordCredential =
                row.Host =
                row.Name =
                row.Bcc =
                row.NameCredential = string.Empty;
                row.Port = 0;
                row.EnableSsl = this.ckbEnableSsl.Checked;
            }
            else
            {
                row.Bcc = this.txtBcc.Text;
                row.NameCredential = this.txtCredentialName.Text;

                row.PasswordCredential =
                    PACFD.Common.Cryptography.EncryptUnivisitString(this.txtCredentialPassword.Text);

                row.Name = this.txtDisplayName.Text;
                row.Host = this.txtHost.Text;
                row.Port = this.txtPort.Text.ToInt32();
                row.EnableSsl = this.ckbEnableSsl.Checked;
            }

            row.SmtpConfigurationId = this.SmtpID;

            t.AddSmtpConfigurationRow(row);
            row.AcceptChanges();

            if (this.SmtpID < 1)
                row.SetAdded();
            else
                row.SetModified();

            c = new PACFD.Rules.SmtpConfiguration();

            if (!c.Update(t))
            {
                this.WebMessageBox1.ShowMessage("No se pudo actualizar la configuracion.", true);
                return;
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(t);

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} mofico la configuración del smtp del cliente {1}.",
                this.UserName, this.CurrentBillerName), this.CurrentBillerID, null, ds.GetXml());
            #endregion

            ds.Dispose();

            c = null;
            this.btnCancel_Click(null, EventArgs.Empty);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("~/" + typeof(PACFDManager.Select).Name + ".aspx");
        }

        #region Disable email configuration
        protected void ChkBoxDisabled_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ChkBoxDisabled.Checked)
            {
                this.WebMessageBox1.Title = "Deshabilitar Configuración";
                this.WebMessageBox1.ShowMessage("Si continuas se perderan los datos actuales, ¿Deseas continuar?",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
            }
        }
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "CleanFormScript", "Clear();", true);
            }
        }
        protected void WebMessageBox1_OnCancelClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myScript", "UnChecked();", true);
        }
        #endregion
    }
}