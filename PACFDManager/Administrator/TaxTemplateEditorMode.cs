﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public delegate void TaxTemplateApplyChanges(object sender, TaxTemplateApplyChangesEventArgs e);


    public enum TaxTemplateEditorMode
    {
        Add = 0,
        Edit,
        SelectBase = 2,
    }
  
}
