﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class WebServiceAccessesList : PACFDManager.BasePage
    {
        const string MESSAGE_ACTIVE = "MESSAGE_ACTIVE";

        public int WebServiceTemporalID
        {
            get
            {
                object o;
                int r;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        public bool WebServiceTemporalActive
        {
            get
            {
                object o;
                bool r;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return false;

                if (!Boolean.TryParse(o.ToString(), out r))
                    return false;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.DataBind();
        }
        /// <summary>
        /// 
        /// </summary>
        public override void DataBind()
        {
            PACFD.Rules.WebServiceAccesses w = new PACFD.Rules.WebServiceAccesses();

            base.DataBind();

            this.gdvWeb.DataSource = w.SelectAllWithCompanyName();
            this.gdvWeb.DataBind();
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.WebServiceAccesses w = new PACFD.Rules.WebServiceAccesses();

            switch (e.CommandArguments)
            {
                case MESSAGE_ACTIVE:
                    w.SetActive(this.WebServiceTemporalID, !this.WebServiceTemporalActive);

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} {1} el acceso al webservice de un usuario.",
                        this.UserName, this.WebServiceTemporalActive ? "activo" : "desactivo"), this.CurrentBillerID, 
                            null, String.Format("WebServiceTemporalID = {0}", this.WebServiceTemporalID));
                    #endregion

                    this.DataBind();
                    break;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(WebServiceAccessesAdd).Name + ".aspx");
        }

        protected void imbActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton c = sender as ImageButton;

            if (c.IsNull())
                return;

            this.WebMessageBox1.CommandArguments = MESSAGE_ACTIVE;
            this.WebServiceTemporalID = c.CommandArgument.Split(',')[1].ToString().ToInt32();
            this.WebServiceTemporalActive = c.CommandArgument.Split(',')[0].ToString() == true.ToString() ? true : false;

            if (c.CommandArgument.Split(',')[0].ToString() == true.ToString())
                this.WebMessageBox1.ShowMessage("¿Desea desactivar este usuario?", System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
            else
                this.WebMessageBox1.ShowMessage("¿Desea activar este usuario?", System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void imbCancel_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void imbEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton c = sender as ImageButton;

            if (c.IsNull())
                return;

            this.Session["id"] = c.CommandArgument;
            this.Response.Redirect(typeof(WebServiceAccessesModify).Name + ".aspx");
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    //PACFD.WebServices.BillingWebService bb = new PACFD.WebServices.BillingWebService();


        //    //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        //    //doc.Load(@"C:\Users\Pablo\Desktop\XML PACFD ejemplo2.xml");
        //    //doc = bb.InsertBilling(doc);
        //    //doc = bb.GetBilling("Administrador", "123", string.Empty, "11");
        //    //doc = bb.CancelBilling("Administrador", "123", string.Empty, "11");
        //}
    }
}