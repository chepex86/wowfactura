﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="TaxTemplateSelection.aspx.cs"
    Inherits="PACFDManager.Administrator.TaxTemplateSelection" %>

<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="txt_left">
                <div class="info">
                    <label class="desc">
                        <h2>
                            <asp:Label ID="lblTaxesListTitle" runat="server" Text="Catalogo de Configuración de Comprobantes Base"></asp:Label>
                        </h2>
                    </label>
                </div>
                <div>
                    <br />
                    <asp:GridView ID="gdvTemplates" runat="server" AutoGenerateColumns="False" 
                        EmptyDataText="No hay impuestos disponibles." 
                        onrowdatabound="gdvTemplates_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="TaxBaseTemplateID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="BillingType" HeaderText="Tipo" />
                            <asp:TemplateField HeaderText="Editar">
                                <HeaderTemplate>
                                       Seleccionar
                                    <%--<uc1:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la configuración seleccionada." />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Seleccionar la configuración de Compobante Base<br /><b><%#Eval("Name") + " - " + Eval("BillingType")%></b>'>
                                        <asp:LinkButton ID="lnkBtnSelect" runat="server" CommandArgument='<%#Eval("TaxBaseTemplateID")%>'
                                            Text="Seleccionar" OnClick="lnkBtnSelect_OnClick"></asp:LinkButton>
                                        <asp:HiddenField ID="hdfTxTemplateID" runat="server" Value='<%#Eval("TaxBaseTemplateID")%>' />
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="dgHeader" />
                        <RowStyle CssClass="dgItem" />
                        <AlternatingRowStyle CssClass="dgAlternate" />
                        <EmptyDataTemplate>
                            No hay impuestos disponibles.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
