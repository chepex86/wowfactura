﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Administrator
{
    public delegate void WebServiceApplyChangesEventHandler(object sender, WebServiceAccessesAddEventArgs e);

    public partial class WebServiceAccessesEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// handler event of the ApplyChanges event.
        /// </summary>
        public event WebServiceApplyChangesEventHandler ApplyChanges;



        /// <summary>
        /// Get or set a string value with the name of the user.
        /// </summary>
        public string UserNameText
        {
            get { return this.txtName.Text; }
            set { this.txtName.Text = value; }
        }
        /// <summary>
        /// Get or set a string value with the password of the user.
        /// </summary>
        public string UserPassword
        {
            get { return this.txtPassword.Text; }
            set { this.txtPassword.Text = value; }
        }
        /// <summary>
        /// Get or set the reentered password text.
        /// </summary>
        public string UserReenterPassword
        {
            get { return this.txtPasswordReenter.Text; }
            set { this.txtPasswordReenter.Text = value; }
        }
        /// <summary>
        /// Get or set a boolean value with the active status of the user.
        /// </summary>
        public bool UserActive
        {
            get { return this.ckbActive.Checked; }
            set { this.ckbActive.Checked = value; }
        }
        /// <summary>
        /// Get or set the editor mode of the control.
        /// </summary>
        public WebServiceAccessesEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return WebServiceAccessesEditorMode.Add;

                if (o.GetType() != typeof(WebServiceAccessesEditorMode))
                    return WebServiceAccessesEditorMode.Add;

                return (WebServiceAccessesEditorMode)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set an integer value of the web service ID to edit.
        /// </summary>
        public int WebServiceAccessesID
        {
            get
            {
                object o;
                int r;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        private string PasswordTemporal
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                return o.ToString();
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.EditorMode == WebServiceAccessesEditorMode.Edit)
            {
                this.txtPassword.Attributes["value"] = this.PasswordTemporal;
                this.txtPasswordReenter.Attributes["value"] = this.PasswordTemporal;
            }
        }
        /// <summary>
        /// Data bind the companies to the drop down list.
        /// </summary>
        public override void DataBind()
        {
            PACFD.Rules.Billers bill = new PACFD.Rules.Billers();
            PACFD.Rules.WebServiceAccesses w = new PACFD.Rules.WebServiceAccesses();

            base.DataBind();
            this.ddlGroup.Items.Clear();

            using (PACFD.DataAccess.GroupsDataSet.GroupsDataTable group = (new PACFD.Rules.Groups()).SelectBySearching(null, true))
            {
                this.ddlGroup.DataSource = group;
                this.ddlGroup.DataBind();
            }

            this.ddlGroup.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlGroup.SelectedIndex = 0;
            this.ddlGroup_SelectedIndexChanged(null, EventArgs.Empty);

            if (this.EditorMode == WebServiceAccessesEditorMode.Edit && this.WebServiceAccessesID > 0)
            {
                using (PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table = w.SelectByID(this.WebServiceAccessesID))
                {
                    this.txtName.Text = table[0].Name;
                    this.PasswordTemporal =
                        this.txtPassword.Text =
                        PACFD.Common.Cryptography.DecryptUnivisitString(table[0].Password);

                    this.InitializeDropDownLists(table);
                }
            }
        }
        private void InitializeDropDownLists(PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table)
        {
            int iprintid = 0
                , ibillerid = 0
                , groupid = 0
                ;
            ListItem l;

            if (table[0].IsPrintTemplateIDNull())
                return;

            iprintid = table[0].PrintTemplateID;

            using (PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable printtable = (new PACFD.Rules.PrintTemplates()).SelectByID(iprintid))
            {
                if (printtable.Count < 1)
                    return;

                ibillerid = printtable[0].BillerID;
            }

            using (PACFD.DataAccess.BillersDataSet.BillersDataTable billertable = (new PACFD.Rules.Billers()).SelectByID(ibillerid))
            {
                if (billertable.Count < 1)
                    return;

                if (billertable[0].IsGroupIDNull())
                    return;

                groupid = billertable[0].GroupID;
            }

            if ((l = this.ddlGroup.Items.FindByValue(groupid.ToString())).IsNull())
                return;

            this.ddlGroup.ClearSelection();
            l.Selected = true;
            this.ddlGroup_SelectedIndexChanged(null, EventArgs.Empty);
            l = null;

            if ((l = this.ddlBiller.Items.FindByValue(ibillerid.ToString())).IsNull())
                return;

            this.ddlBiller.ClearSelection();
            l.Selected = true;
            this.ddlBiller_SelectedIndexChanged(null, EventArgs.Empty);
            l = null;

            if ((l = this.ddlPrintDocument.Items.FindByValue(iprintid.ToString())).IsNull())
                return;

            this.ddlPrintDocument.ClearSelection();
            l.Selected = true;
        }


        /// <summary>
        /// Fire the event ApplyChanges.
        /// </summary>
        /// <param name="e">Arguments send by the control.</param>
        protected virtual void OnApplyChanges(WebServiceAccessesAddEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// Generate the WebServiceAccessesDataTable and fire the ApplyChanges event.
        /// </summary>
        /// <exception cref="System.Exception">Password are different.</exception>
        public void InvokeApplyChanges()
        {
            PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesRow row;
            PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table =
                new PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable();

            if (this.UserPassword.Trim() != this.UserReenterPassword.Trim())
                throw new Exception("Password are different.");

            if (this.ddlGroup.SelectedIndex < 1 || this.ddlGroup.SelectedItem.Value.ToInt32() < 1)
                throw new Exception("Not group selected.");

            if (this.ddlBiller.SelectedIndex < 1 || this.ddlBiller.SelectedItem.Value.ToInt32() < 1)
                throw new Exception("Not biller selected.");

            if (this.ddlPrintDocument.SelectedIndex < 1 || this.ddlPrintDocument.SelectedItem.Value.ToInt32() < 1)
                throw new Exception("Not print template selected.");

            row = table.NewWebServiceAccessesRow();
            row.BeginEdit();
            row.WebServiceAccessesID = this.EditorMode == WebServiceAccessesEditorMode.Add ? 0 : this.WebServiceAccessesID;
            row.Name = this.txtName.Text;
            row.Password = PACFD.Common.Cryptography.EncryptUnivisitString(this.txtPassword.Text.Trim());
            row.BillerID = this.ddlBiller.SelectedItem.Value.ToInt32();
            row.Active = this.ckbActive.Checked;
            row.PrintTemplateID = this.ddlPrintDocument.SelectedItem.Value.ToInt32();
            row.EndEdit();
            table.AddWebServiceAccessesRow(row);
            row.AcceptChanges();

            if (this.EditorMode == WebServiceAccessesEditorMode.Add)
                row.SetAdded();
            else
                row.SetModified();

            this.OnApplyChanges(new WebServiceAccessesAddEventArgs(table));
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = 0;

            this.ddlBiller.Items.Clear();

            if (this.ddlGroup.SelectedIndex > 0)
            {
                if (int.TryParse(this.ddlGroup.SelectedItem.Value, out id))
                {
                    using (PACFD.DataAccess.BillersDataSet.GetByGroupIDDataTable table = (new PACFD.Rules.Billers()).SelectByGroupID(id))
                    {
                        this.ddlBiller.DataSource = table;
                        this.ddlBiller.DataBind();
                    }
                }
            }

            this.ddlBiller.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlBiller.SelectedIndex = 0;
            this.ddlBiller_SelectedIndexChanged(sender, e);
        }

        protected void ddlBiller_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = 0;

            this.ddlPrintDocument.Items.Clear();

            if (this.ddlBiller.SelectedIndex > 0)
            {
                if (int.TryParse(this.ddlBiller.SelectedItem.Value, out id))
                {
                    using (PACFD.DataAccess.PrintTemplatesDataSet.GetByBillerIDAndElectronicBillingTypeDataTable printtable =
                        (new PACFD.Rules.PrintTemplates()).SelectByBillerIDAndElectronicBillingType(id, (PACFD.Rules.ElectronicBillingType?)null))
                    {
                        this.ddlPrintDocument.DataSource = printtable;
                        this.ddlPrintDocument.DataBind();
                    }
                }
            }

            this.ddlPrintDocument.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            this.ddlPrintDocument.SelectedIndex = 0;
        }
    }
}