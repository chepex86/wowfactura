﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Administrator
{
    public partial class ManualReseal : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.CurrentBillerID < 1)
                this.Response.Redirect("~/Default.aspx");
            if(!this.IsPostBack)
                this.LoadSerials();
        }

        private void LoadSerials()
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            PACFD.DataAccess.BillingsDataSet.GetSerialsByBillerIDDataTable ta;
            
            ta = bill.GetSerialsByBillerID(this.CurrentBillerID);

            if (ta.Count < 1)
            {
                this.WebMessageBox1.Title = "Mensaje";
                this.WebMessageBox1.CommandArguments = "WITHOUT_SERIES_OR_FOLIOS";
                this.WebMessageBox1.ShowMessage("No se encontró ninguna serie ni folio.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
            }

            this.ddlSerial.DataTextField = "Serial";
            this.ddlSerial.DataValueField = "BillingID";

            this.ddlSerial.DataSource = ta;
            this.ddlSerial.DataBind();
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            this.WebMessageBox1.Title = "Alerta";
            this.WebMessageBox1.CommandArguments = "RESEAL_PREBILLING";
            this.WebMessageBox1.ShowMessage("Estas a punto de sellar manualmente un precombrobante, ¿Deseas continuar?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.AcceptCancel);
        }

        private bool Reseal()
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            bool result = false;

            int billingId = int.Parse(ddlSerial.SelectedValue);

            result = bill.HandSealCFD(
                    billingId,                    
                    this.txtUUID.Text,
                    this.txtStampingDate.Text,
                    this.txtSATSeal.Text,
                    this.txtNoCertSAT.Text,
                    this.txtCFDSeal.Text,
                    this.txtVersion.Text
                    );

            if(result)
                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} sello manualmente el precomprobante {1}.",
                    this.UserName, this.ddlSerial.SelectedItem.Text), this.CurrentBillerID, null, null);
                #endregion

            return result;
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.DialogResult)
            {
                case WebMessageBoxDialogResultType.Accept:
                    if (e.CommandArguments == "WITHOUT_SERIES_OR_FOLIOS")
                    {
                        this.Response.Redirect("~/Default.aspx");
                    }
                    else if (e.CommandArguments == "RESEAL_PREBILLING")
                    {
                        if (this.Reseal())
                        {
                            this.WebMessageBox1.Title = "Mensaje";
                            this.WebMessageBox1.CommandArguments = "SUCCESS_RESEAL_PREBILLING";
                            this.WebMessageBox1.ShowMessage("El sellado se realizo exitosamente.",
                                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                        }
                        else
                        {
                            this.WebMessageBox1.Title = "Mensaje";
                            this.WebMessageBox1.CommandArguments = "FAIL_RESEAL_PREBILLING";
                            this.WebMessageBox1.ShowMessage("Ocurrio un problema al tratar de sellar manualmente el precomprobante.<br /><br />Verifica que los datos introducidos sean correctos y vuelve a intentarlo.",
                                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                        }
                    }
                    else if(e.CommandArguments == "SUCCESS_RESEAL_PREBILLING")
                    {
                        this.Response.Redirect("~/Administrator/ManualReseal.aspx");
                    }
                    else if (e.CommandArguments == "FAIL_RESEAL_PREBILLING")
                    {
                        return;
                    }
                    break;
                case WebMessageBoxDialogResultType.Cancel:
                    break;
            }
        }

        protected void btnReSealCFD_Click(object sender, EventArgs e)
        {
            PACFD.Rules.Billings bill = new PACFD.Rules.Billings();
            bill.ReSealCFD();
        }
    }
}