﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="WebServiceAccessesAdd.aspx.cs" Inherits="PACFDManager.Administrator.WebServiceAccessesAdd" %>

<%@ Register Src="WebServiceAccessesEditor.ascx" TagName="WebServiceAccessesEditor"
    TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container" class="wframe50">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Acceso a Webservice" />
                    </h2>
                </div>
                <uc1:WebServiceAccessesEditor ID="WebServiceAccessesEditor1" runat="server" OnApplyChanges="WebServiceAccessesEditor1_ApplyChanges" />
                <br />
                <div style="text-align: right;">
                    <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button" OnClick="btnCancel_Click"
                        CausesValidation="false" />
                </div>
            </div>
        </div>
    </div>
    <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
