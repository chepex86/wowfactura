﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Administrator
{
    /// <summary>
    /// Control used to edit the tax value table.
    /// </summary>
    public partial class TaxTypeCatalogEditor : System.Web.UI.UserControl
    {
        /// <summary>
        /// Get or set an integer value with the taxtype ID to be bind to the control.
        /// </summary>
        public int TaxTypeID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get the editor mode of the control.
        /// </summary>
        public TaxTypeCatalogEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return TaxTypeCatalogEditorMode.Add;

                return o.GetType() == typeof(TaxTypeCatalogEditorMode) ? (TaxTypeCatalogEditorMode)o : TaxTypeCatalogEditorMode.Add;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable TaxValueDataSetTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return null;

                if (o.GetType() != typeof(PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable))
                    return null;

                return (PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable)o;
            }
            private set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get or set the tax value to be added to the list.
        /// </summary>
        public decimal TaxValue
        {
            get
            {
                decimal r;

                if (!decimal.TryParse(this.txtValueText.Text, out r))
                    return 0;

                return r;
            }
            set { this.txtValueText.Text = value.ToString(); }
        }
        /// <summary>
        /// Get a boolean value indicating if the tax is fixed (can be deleted or not).
        /// </summary>
        public bool IsTaxFixed
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return false;

                if (o.GetType() != typeof(bool))
                    return false;

                return bool.Parse(o.ToString());
            }
            private set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            string js = string.Format(
                "<script type='text/javascript'>"
                + "\n function {0}_HideShow(ck1){1}"
                + "\n var dv1 = document.getElementById('{2}');"
                + "\n if(ck1 == 0)"
                + "\n    dv1.style.display='none';"
                + "\n else"
                + "\n    dv1.style.display='block';"
                + "\n{3}\n</script>",
                this.ClientID,
                "{",
                this.divTaxValues.ClientID,
                "}"
                );

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript_" + this.ClientID, js);
            this.rbtnEditable.Attributes["onclick"] = string.Format("javascript:{0}_HideShow(0);", this.ClientID);
            this.rbtnSelect.Attributes["onclick"] = string.Format("javascript:{0}_HideShow(1);", this.ClientID);

            if (this.IsPostBack)
                return;
        }
        /// <summary>
        /// Delete a row field.
        /// </summary>
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int index = 0;
            ImageButton b = sender as ImageButton;

            if (b.IsNull() || this.gdvTaxValues.Rows.Count < 1)
                return;

            if ((index = this.GetRowIndexByValue(b.CommandArgument)) > -1)
            {
                this.TaxValueDataSetTable[index].Delete();
                this.DataBindTaxValues();
            }
        }
        /// <summary>
        /// Create a PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable table
        /// used to storage the taxvalue rows.
        /// </summary>
        private PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable OnCreateTaxValueGridDataTable()
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable table =
                new PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable();
            this.TaxValueDataSetTable = table;

            return table;
        }
        /// <summary>
        /// Get a PACFD.DataAccess.TaxTypesDataSet data set with the TaxType and TaxValue.
        /// </summary>
        /// <returns>PACFD.DataAccess.TaxTypesDataSet dataset.</returns>
        /// <exception cref="System.Exception">If the name field is empty.</exception>
        public virtual PACFD.DataAccess.TaxTypesDataSet GenerateTaxType()
        {
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow row;
            PACFD.DataAccess.TaxTypesDataSet.TaxTypesRow taxtyperow;
            PACFD.DataAccess.TaxTypesDataSet dataset = new PACFD.DataAccess.TaxTypesDataSet();

            if (string.IsNullOrEmpty(this.txtName.Text))
                throw new Exception("Name is empty.");

            taxtyperow = dataset.TaxTypes.NewTaxTypesRow();
            taxtyperow.BeginEdit();
            taxtyperow.TaxTypeID = this.EditorMode == TaxTypeCatalogEditorMode.Edit ? this.TaxTypeID : -1;
            taxtyperow.Name = this.txtName.Text;
            taxtyperow.Required = this.ckbRequired.Checked;
            taxtyperow.Transfer = this.rbtnTransfer.Checked ? true : false;//this.ckbTransfered.Checked;
            taxtyperow.IvaAffected = false;
            taxtyperow.Editable = this.rbtnEditable.Checked ? true : false;
            taxtyperow.Fixed = this.IsTaxFixed;
            taxtyperow.EndEdit();
            dataset.TaxTypes.AddTaxTypesRow(taxtyperow);
            taxtyperow.AcceptChanges();

            if (this.EditorMode == TaxTypeCatalogEditorMode.Edit)
                taxtyperow.SetModified();
            else
                taxtyperow.SetAdded();

            dataset.TaxValues.Clear();

            if (this.TaxValueDataSetTable != null)
                dataset.TaxValues.Merge(this.TaxValueDataSetTable.Copy(), true);

            if (this.EditorMode != TaxTypeCatalogEditorMode.Edit)
            {
                foreach (var item in dataset.TaxValues)
                {
                    item.SetParentRow(taxtyperow);
                }
            }

            if (this.rbtnEditable.Checked && !this.TaxValueDataSetTable.IsNull() && this.TaxValueDataSetTable.Rows.Count > 0)
            {
                for (int i = dataset.TaxValues.Rows.Count - 1; i > -1; i--)
                {
                    row = dataset.TaxValues.Rows[i] as PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow;
                    row.BeginEdit();
                    row.Delete();
                    row.EndEdit();
                }
            }

            if (this.rbtnSelect.Checked && this.gdvTaxValues.Rows.Count < 1)
                throw new Exception("No taxed in data set.");

            return dataset;
        }
        /// <summary>
        /// 
        /// </summary>
        protected void btnAddTaxValue_Click(object sender, EventArgs e)
        {
            decimal ir;
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow row;

            this.divTaxValues.Style["display"] = "block";

            if (string.IsNullOrEmpty(this.txtValueText.Text))
                return;

            if (!decimal.TryParse(this.txtValueText.Text, out ir))
                return;

            if (this.TaxValueDataSetTable.IsNull())
                this.TaxValueDataSetTable = new PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable();

            foreach (PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow r in this.TaxValueDataSetTable)
            {
                if (r.RowState == System.Data.DataRowState.Deleted)
                    continue;

                if (r.Value.ToString().Trim() == this.txtValueText.Text.Trim())
                    return;
            }

            row = this.TaxValueDataSetTable.NewTaxValuesRow();
            row.BeginEdit();
            row.TaxTypeID = this.EditorMode == TaxTypeCatalogEditorMode.Add ? 0 : this.TaxTypeID;
            row.TaxValueID = this.TaxValueDataSetTable.Count > 0 ? this.GetNewID(this.TaxValueDataSetTable) : 0;
            row.Value = this.txtValueText.Text.ToDecimal();
            row.EndEdit();
            this.TaxValueDataSetTable.AddTaxValuesRow(row);
            row.AcceptChanges();

            row.SetAdded();

            this.DataBindTaxValues();
            this.TaxValue = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        private void DataBindTaxValues()
        {
            this.gdvTaxValues.DataSource = this.TaxValueDataSetTable;
            this.gdvTaxValues.DataBind();
        }
        /// <summary>
        /// Data bind a tax type from the data base.
        /// </summary>
        public void DataBindFromTaxType()
        {
            if (this.TaxTypeID < 1)
                return;

            PACFD.Rules.TaxTypes taxes = new PACFD.Rules.TaxTypes();
            PACFD.DataAccess.TaxTypesDataSet dataset = taxes.SelectTaxByTaxTypeID(this.TaxTypeID);
            this.TaxValueDataSetTable = new PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable();
            this.TaxValueDataSetTable.Merge(dataset.TaxValues.Copy(), true);
            taxes = null;

            if (dataset.TaxTypes.Count < 1)
            {
                dataset.TaxTypes.Dispose();
                dataset.TaxValues.Dispose();
                dataset = null;
                return;
            }

            this.txtName.Enabled =
                this.ckbRequired.Enabled =
                this.rbtnRetained.Enabled =
                this.rbtnTransfer.Enabled = false;

            this.txtName.Text = dataset.TaxTypes[0].Name;
            this.IsTaxFixed = dataset.TaxTypes[0].Fixed;

            this.rbtnSelect.Checked =
                !(this.rbtnEditable.Checked = dataset.TaxTypes[0].Editable);

            this.rbtnRetained.Checked =
                !(this.rbtnTransfer.Checked = dataset.TaxTypes[0].Transfer);

            if (rbtnSelect.Checked)
                this.divTaxValues.Style["display"] = "block";

            if (!this.rbtnEditable.Checked)
                this.DataBindTaxValues();
        }
        /// <summary>
        /// Get a new ID not used.
        /// </summary>
        /// <param name="table">Taxes table used to generate the new ID.</param>
        /// <returns>Return an integer ID not used.</returns>
        private int GetNewID(PACFD.DataAccess.TaxTypesDataSet.TaxValuesDataTable table)
        {
            int id = 0;
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow item;

            for (int i = 0; i < table.Count; i++)
            {
                item = table[i];

                if (item.RowState == System.Data.DataRowState.Deleted)
                    continue;

                if (item.TaxValueID != id)
                    continue;

                id--;
                i = 0;
            }

            return id;
        }
        /// <summary>
        /// Get the row index by the text.
        /// </summary>
        /// <param name="value">Text value of the row.</param>
        /// <returns>Get the row integer index of the row.</returns>
        private int GetRowIndexByValue(string value)
        {
            int index = -1;

            foreach (PACFD.DataAccess.TaxTypesDataSet.TaxValuesRow r in this.TaxValueDataSetTable)
            {
                index++;

                if (r.RowState == System.Data.DataRowState.Deleted)
                    continue;

                if (Math.Round(r.Value, 2).ToString() == value)
                    return index;
            }

            return -1;
        }
    }
}