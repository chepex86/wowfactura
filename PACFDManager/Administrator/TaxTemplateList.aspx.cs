﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTemplateList : PACFDManager.BasePage
    {
        private int TemporalTaxTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.TaxTemplate template;

            //if (this.CurrentBillerID < 1)
            //    this.Response.Redirect("..\\Default.aspx");

            if (this.IsPostBack)
                return;

            template = new PACFD.Rules.TaxTemplate();
            this.gdvTemplates.DataSource = template.SelectForListByBillerID(this.CurrentBillerID);
            this.gdvTemplates.DataBind();
            //this.DataBindDropDownList();
        }

        protected void btnAdd_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxTemplateAdd).Name + ".aspx"); }

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int id;
            ImageButton b = sender as ImageButton;

            if (b.IsNull())
                return;

            id = b.CommandArgument.ToInt32();
            this.TemporalTaxTemplateID = id;

            switch (b.ID)
            {
                case "imgbDelete":
                    this.WebMessageBox1.CommandArguments = "delete";
                    this.WebMessageBox1.ShowMessage("¿Desea eliminar la configuración?",
                        System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
                case "imgbEdit":
                    this.Session["taxtemplateid"] = id;
                    this.Response.Redirect(typeof(TaxTemplateModify).Name + ".aspx");
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.TaxTemplate tem = new PACFD.Rules.TaxTemplate();
            PACFD.DataAccess.TaxTemplatesDataSet taxdataset;
            PACFD.DataAccess.TaxTemplatesDataSet taxdatasettemp;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            switch (e.CommandArguments)
            {
                case "delete":
                    taxdatasettemp = taxdataset = tem.SelectByTaxTemplateID(this.TemporalTaxTemplateID);

                    for (int i = taxdataset.TaxTemplateTaxType.Count - 1; i >= 0; i--)
                    {
                        taxdataset.TaxTemplateTaxType[i].BeginEdit();
                        taxdataset.TaxTemplateTaxType[i].Delete();
                        taxdataset.TaxTemplateTaxType[i].EndEdit();
                    }

                    tem.Update(taxdataset);

                    taxdataset.TaxTemplates[0].BeginEdit();
                    taxdataset.TaxTemplates[0].Delete();
                    taxdataset.TaxTemplates[0].EndEdit();

                    tem.Update(taxdataset);

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino una configuración de impuesto.",
                        this.UserName), this.CurrentBillerID, taxdatasettemp.GetXml(), taxdataset.GetXml());
                    #endregion

                    taxdataset.Dispose();
                    taxdatasettemp.Dispose();

                    this.WebMessageBox1.CommandArguments = string.Empty;
                    this.Response.Redirect(typeof(TaxTemplateList).Name + ".aspx");
                    break;
            }
        }
    }
}