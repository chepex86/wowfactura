﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Administrator
{
    public partial class TaxBaseConfigurationEditor : PACFDManager.BaseUserControl
    {
        public event TaxBaseConfigurationApplyChanges ApplyChanges;



        /// <summary>
        /// Get the editor mode of the control.
        /// </summary>
        public TaxBaseConfigurationEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return TaxBaseConfigurationEditorMode.Add;

                return o.GetType() == typeof(TaxBaseConfigurationEditorMode) ? (TaxBaseConfigurationEditorMode)o : TaxBaseConfigurationEditorMode.Add;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set an integer value with the taxtemplate ID to be bind to the control.
        /// </summary>
        public int TaxBaseTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }
        /// <summary>
        /// Get or set a PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplateTaxBaseTypeDataTable table.
        /// </summary>
        private PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable TaxBaseTemplateTaxBaseTypeDataTable
        {
            get
            {
                PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable table;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                {
                    table = new PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable();
                    this.TaxBaseTemplateTaxBaseTypeDataTable = table;
                    return table;
                }

                return (PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        private PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable AvailableTaxBaseTemplateTaxBaseTypeDataTable
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return new PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable();

                return (PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable)o;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;
        }
        /// <summary>
        /// 
        /// </summary>
        public void DataBindTaxes()
        {
            int i;
            PACFD.Rules.TaxBaseTemplate tem;
            PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesDataTable table;
            //PACFD.Rules.PrintTemplates p;

            //p = new PACFD.Rules.PrintTemplates();
            //this.ddlPrintTemplate.DataSource = p.SelectByBillerIDAndElectronicBillingType(this.CurrentBillerID, this.CurrentElectronicBillingType);
            //this.ddlPrintTemplate.DataTextField = "Name";
            //this.ddlPrintTemplate.DataValueField = "PrintTemplateID";
            //this.ddlPrintTemplate.DataBind();
            //this.ddlPrintTemplate.Items.Insert(0, new ListItem("[Seleccionar]", "0"));
            //p = null;

            if (this.EditorMode == TaxBaseConfigurationEditorMode.Add)
            {
                tem = new PACFD.Rules.TaxBaseTemplate();
                this.AvailableTaxBaseTemplateTaxBaseTypeDataTable = tem.SelectTaxBaseTypeInOrOut(this.TaxBaseTemplateID, false);
                tem = null;

                this.gdvAvaliableTaxes.DataSource = this.AvailableTaxBaseTemplateTaxBaseTypeDataTable;
                this.gdvAvaliableTaxes.DataBind();
            }
            else if (this.EditorMode == TaxBaseConfigurationEditorMode.Edit)
            {
                tem = new PACFD.Rules.TaxBaseTemplate();

                table = tem.SelectByID(this.TaxBaseTemplateID);

                if (table.Count < 1)
                    throw new Exception("Not TaxTemplate found by the id: " + this.TaxBaseTemplateID.ToString());

                this.txtName.Text = table[0].Name;
                this.txtDescription.Text = table[0].Description;

                //i = -1;

                //foreach (ListItem item in this.ddlPrintTemplate.Items)
                //{
                //    i++;

                //    /*if (item.Value != table[0].PrintTemplateID.ToString())
                //        continue;*/

                //    this.ddlPrintTemplate.SelectedIndex = i;
                //    break;
                //}

                for (i = 0; i < this.ddlBillingType.Items.Count; i++)
                {
                    if (this.ddlBillingType.Items[i].Text != table[0].BillingType)
                        continue;

                    this.ddlBillingType.SelectedIndex = i;
                    break;
                }

                table.Dispose();
                table = null;

                this.AvailableTaxBaseTemplateTaxBaseTypeDataTable = tem.SelectTaxBaseTypeInOrOut(this.TaxBaseTemplateID, false);

                this.gdvAvaliableTaxes.DataSource = this.AvailableTaxBaseTemplateTaxBaseTypeDataTable;
                this.gdvAvaliableTaxes.DataBind();

                this.TaxBaseTemplateTaxBaseTypeDataTable = tem.SelectTaxBaseTypeInOrOut(this.TaxBaseTemplateID, true);
                this.gdvTaxes.DataSource = this.TaxBaseTemplateTaxBaseTypeDataTable;
                this.gdvTaxes.DataBind();
                tem = null;
                this.InitializeGridRowsTaxesSelected();
            }

            tem = null;
        }
        /// <summary>
        /// Initialize rows for taxes selected.
        /// </summary>
        private void InitializeGridRowsTaxesSelected()
        {
            int i = 0;
            HiddenField hiddentaxtypeid;
            HiddenField hiddentaxtemtaxtypeid;
            HiddenField hiddeneditable;
            HiddenField hiddenvalue;
            CheckBox checkbox;
            DropDownList drop;
            TextBox textbox;
            PACFD.Rules.TaxTypes taxtypes = new PACFD.Rules.TaxTypes();
            PACFD.DataAccess.TaxTypesDataSet.TaxValuesGetByTypeIDDataTable table;

            foreach (GridViewRow grow in this.gdvTaxes.Rows)
            {
                checkbox = grow.FindControl("ckbGridTaxUse") as CheckBox;
                drop = grow.FindControl("ddlGridTax") as DropDownList;
                textbox = grow.FindControl("txtGridTax") as TextBox;
                hiddentaxtypeid = grow.FindControl("hdfTaxTypeID") as HiddenField;
                hiddentaxtemtaxtypeid = grow.FindControl("hdfTaxTemplateTaxTypeID") as HiddenField;
                hiddeneditable = grow.FindControl("hdfEditable") as HiddenField;
                hiddenvalue = grow.FindControl("hdfValue") as HiddenField;

                drop.Visible =
                    !(textbox.Visible = hiddeneditable.Value.ToBoolean());

                if (drop.Visible)
                {
                    table = taxtypes.SelectTaxValueByTaxTypeID(hiddentaxtypeid.Value.ToInt32());

                    if (table.Count < 1)
                        continue;

                    drop.DataSource = table;
                    drop.DataTextField = "Value";
                    drop.DataBind();

                    for (i = 0; i < drop.Items.Count; i++)
                    {
                        if (drop.Items[i].Text == hiddenvalue.Value)
                        {
                            drop.SelectedIndex = i;
                            break;
                        }
                    }
                }
                else
                {
                    textbox.Text = hiddenvalue.Value;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="Exception">General exception class.</exception>
        public void GenerateTemplate()
        {
            TaxBaseConfigurationApplyChangesEventArgs args;
            PACFD.DataAccess.TaxBaseTemplatesDataSet dataset = new PACFD.DataAccess.TaxBaseTemplatesDataSet();
            PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplatesRow temrow;

            if (string.IsNullOrEmpty(this.txtName.Text))
                throw new Exception("Name can't be empty.");

            //if (this.ddlPrintTemplate.SelectedIndex < 1)
            //    throw new Exception("Need select a Billing type.");

            temrow = dataset.TaxBaseTemplates.NewTaxBaseTemplatesRow();
            //temrow.BillerID = this.CurrentBillerID;
            temrow.BillingType = this.ddlBillingType.SelectedItem.Text;
            temrow.Name = this.txtName.Text;
            temrow.Description = this.txtDescription.Text.Trim();
            temrow.TaxBaseTemplateID = this.EditorMode == TaxBaseConfigurationEditorMode.Add ? 0 : this.TaxBaseTemplateID;
            //temrow.PrintTemplateID = this.ddlPrintTemplate.SelectedItem.Value.ToInt32();            
            dataset.TaxBaseTemplates.AddTaxBaseTemplatesRow(temrow);
            temrow.AcceptChanges();

            if (this.EditorMode == TaxBaseConfigurationEditorMode.Add)
                temrow.SetAdded();
            else
                temrow.SetModified();

            this.GenerateRowsForTaxBaseTemplateTaxBaseTypeFromGrid(ref dataset);
            args = new TaxBaseConfigurationApplyChangesEventArgs(dataset, /*this.ckblAvaliableTaxes.Items,*/ this.EditorMode);
            this.OnApplyChanges(args);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataset"></param>
        private void GenerateRowsForTaxBaseTemplateTaxBaseTypeFromGrid(ref PACFD.DataAccess.TaxBaseTemplatesDataSet dataset)
        {
            int i = 0;
            HiddenField hiddentaxtype;
            HiddenField hiddentaxtemtaxtypeid;
            CheckBox checkbox;
            DropDownList drop;
            TextBox textbox;
            PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplateTaxTypeRow taxtemtaxtype;

            foreach (GridViewRow grow in this.gdvTaxes.Rows)
            {
                if (grow.RowType != DataControlRowType.DataRow)
                    continue;

                checkbox = grow.FindControl("ckbGridTaxUse") as CheckBox;
                drop = grow.FindControl("ddlGridTax") as DropDownList;
                textbox = grow.FindControl("txtGridTax") as TextBox;
                hiddentaxtype = grow.FindControl("hdfTaxTypeID") as HiddenField;
                hiddentaxtemtaxtypeid = grow.FindControl("hdfTaxTemplateTaxTypeID") as HiddenField;

                taxtemtaxtype = dataset.TaxBaseTemplateTaxType.NewTaxBaseTemplateTaxTypeRow();
                taxtemtaxtype.TaxBaseTemplateTaxTypeID = hiddentaxtemtaxtypeid.Value.ToInt32();
                taxtemtaxtype.TaxBaseTemplateID = this.TaxBaseTemplateID;
                taxtemtaxtype.TaxTypeID = hiddentaxtype.Value.ToInt32();
                taxtemtaxtype.TaxIndex = i++;

                taxtemtaxtype.Value = drop.Visible ?
                    drop.SelectedValue.ToDecimal() : textbox.Text.ToDecimal();

                dataset.TaxBaseTemplateTaxType.AddTaxBaseTemplateTaxTypeRow(taxtemtaxtype);

                taxtemtaxtype.AcceptChanges();

                if (taxtemtaxtype.TaxBaseTemplateTaxTypeID > 0)
                    taxtemtaxtype.SetModified();
                else if (taxtemtaxtype.TaxBaseTemplateTaxTypeID < 1)
                    taxtemtaxtype.SetAdded();
                else
                    taxtemtaxtype.Delete();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnApplyChanges(TaxBaseConfigurationApplyChangesEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton b = sender as ImageButton;
            string[] data;
            int i;
            List<object[]> listrowdata = new List<object[]>();

            if (b.IsNull())
                return;

            data = b.CommandArgument.Split('_');

            if (data.Length < 2)
                return;

            if (data[0] != "up" && data[0] != "down")
                return;

            if (!int.TryParse(data[1], out i))
                return;

            if (i < 0 && i > this.gdvTaxes.Rows.Count - 1)
                return;

            i = this.gdvTaxes.Rows.GetRowIndexByTaxTypeID(i);

            if (i < 0)
                return;

            listrowdata = this.GetTemporalRowData();
            this.TaxBaseTemplateTaxBaseTypeDataTable.RowLevelIndex(i, data[0] == "up" ? true : false);
            this.gdvTaxes.DataSource = this.TaxBaseTemplateTaxBaseTypeDataTable;
            this.gdvTaxes.DataBind();
            this.InitializeGridRowsTaxesSelected();
            this.SetTemporalRowData(listrowdata);
            listrowdata.Clear();
            listrowdata = null;
        }
        /// <summary>
        /// Get a List class with an object[] array list with the temporal value of the rows in the grid.
        /// </summary>
        /// <returns>Return a List class with an object[] array list</returns>
        private List<object[]> GetTemporalRowData()
        {
            object[] data;
            List<object[]> list = new List<object[]>();
            TextBox textbox;
            DropDownList dropdown;
            //CheckBox checkbox;
            HiddenField hiddenid;
            HiddenField hiddentransfer;

            foreach (GridViewRow row in this.gdvTaxes.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                    continue;

                //checkbox = row.FindControl("ckbGridTaxUse") as CheckBox;
                dropdown = row.FindControl("ddlGridTax") as DropDownList;
                textbox = row.FindControl("txtGridTax") as TextBox;
                hiddenid = row.FindControl("hdfTaxTypeID") as HiddenField;
                hiddentransfer = row.FindControl("hdfTransfer") as HiddenField;
                data = new object[5];

                //if (!checkbox.IsNull())
                data[0] = null;//checkbox.Checked;

                if (!dropdown.IsNull())
                    data[1] = dropdown.SelectedItem.IsNull() ? null : dropdown.SelectedItem.Text;

                if (!textbox.IsNull())
                    data[2] = textbox.Text;

                if (!hiddentransfer.IsNull())
                    data[3] = hiddentransfer.Value;

                if (!hiddenid.IsNull())
                    data[4] = hiddenid.Value;

                list.Add(data);
                data = null;
                //checkbox = null;
                dropdown = null;
                textbox = null;
                hiddenid = null;
            }

            return list;
        }
        /// <summary>
        /// Set the List class with an object[] array list with all the temporal value of the rows in the grid.
        /// </summary>
        /// <param name="list">
        /// List class that contains an object[] array, the format of the array value object must be:
        /// object[] { boolean, string, string, integer32 }
        /// </param>
        private void SetTemporalRowData(List<object[]> list)
        {
            TextBox textbox;
            DropDownList dropdown;
            CheckBox checkbox;
            HiddenField hiddenid;
            HiddenField hiddentransfer;
            GridViewRow row;
            int index;

            foreach (object[] o in list)
            {
                index = o[o.Length - 1].ToString().ToInt32();

                if (index < 1)
                    continue;

                index = this.gdvTaxes.Rows.GetRowIndexByTaxTypeID(index);

                if (index < 0)
                    continue;

                row = this.gdvTaxes.Rows[index];

                checkbox = row.FindControl("ckbGridTaxUse") as CheckBox;
                dropdown = row.FindControl("ddlGridTax") as DropDownList;
                textbox = row.FindControl("txtGridTax") as TextBox;
                hiddenid = row.FindControl("hdfTaxTypeID") as HiddenField;
                hiddentransfer = row.FindControl("hdfTransfer") as HiddenField;

                index = 0;

                foreach (ListItem item in dropdown.Items)
                {
                    if (dropdown.Items[index].Text != o[1].ToString())
                    {
                        index++;
                        continue;
                    }

                    dropdown.SelectedIndex = index;
                }

                textbox.Text = o[2].ToString();
                hiddentransfer.Value = o[3].ToString();

                checkbox = null;
                dropdown = null;
                textbox = null;
                hiddenid = null;
            }
        }

        protected void btnAvailableGridTaxUse_CheckedChanged(object sender, EventArgs e)
        {
            Button button, b2;
            Label labelname;
            int id = 0;
            HiddenField h;
            HiddenField hiddentransfer;
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutRow row;
            GridViewRow gridrow;
            List<object[]> temporaldata;

            button = sender as Button;
            labelname = null;
            hiddentransfer = h = null;
            gridrow = null;

            if (button.IsNull())
                return;

            for (int i = 0; i < this.TaxBaseTemplateTaxBaseTypeDataTable.Rows.Count; i++)
            {
                row = this.TaxBaseTemplateTaxBaseTypeDataTable[i];

                if (row.RowState == System.Data.DataRowState.Deleted)
                    continue;

                if (row.TaxBaseTemplateTaxTypeID != id)
                    continue;

                id--;
                i = -1;
            }

            foreach (GridViewRow r in this.gdvAvaliableTaxes.Rows)
            {
                b2 = r.FindControl("btnTaxAdd") as Button;

                if (b2.IsNull())
                    continue;

                if (b2 != button)
                    continue;

                gridrow = r;
                h = r.FindControl("hdfAvailableTaxTypeID") as HiddenField;
                hiddentransfer = r.FindControl("hdfTransfer") as HiddenField;
                labelname = r.FindControl("lblAvailableGridTaxUse") as Label;
                break;
            }

            if (gridrow.IsNull())
                return;

            if (h.IsNull())
                return;

            row = null;
            row = this.TaxBaseTemplateTaxBaseTypeDataTable.NewGetTaxBaseTypeInAndOutRow();
            row.BeginEdit();
            row.Value = 0;
            row.Transfer = hiddentransfer.Value.ToBoolean();
            row.Name = labelname.Text;
            row.Fixed =
                row.Required =
                row.IvaAffected = false;
            row.Editable = (gridrow.FindControl("hdfAvailableEditable") as HiddenField).Value.ToBoolean();
            row.TaxIndex = 0;
            row.TaxBaseTemplateTaxTypeID = id;
            row.TaxTypeID = h.Value.ToInt32();
            row.EndEdit();

            this.TaxBaseTemplateTaxBaseTypeDataTable.AddGetTaxBaseTypeInAndOutRow(row);
            row.AcceptChanges();
            row.SetAdded();

            foreach (PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutRow r in this.AvailableTaxBaseTemplateTaxBaseTypeDataTable)
            {
                if (r.TaxTypeID != h.Value.ToInt32())
                    continue;

                r.Delete();
                r.AcceptChanges();
                break;
            }

            temporaldata = this.GetTemporalRowData();

            this.gdvAvaliableTaxes.DataSource = this.AvailableTaxBaseTemplateTaxBaseTypeDataTable;
            this.gdvAvaliableTaxes.DataBind();

            this.gdvTaxes.DataSource = this.TaxBaseTemplateTaxBaseTypeDataTable;
            this.gdvTaxes.DataBind();

            this.InitializeGridRowsTaxesSelected();

            this.SetTemporalRowData(temporaldata);
            temporaldata.Clear();
            temporaldata = null;
        }

        protected void btnGridTaxUse_CheckedChanged(object sender, EventArgs e)
        {
            Button button, b2;
            HiddenField hiddentaxtypeid;
            HiddenField hiddentransfer;
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutRow row;
            GridViewRow gridrow;
            List<object[]> temporaldata;
            Label lblname;

            button = sender as Button;
            lblname = null;
            hiddentransfer =
                hiddentaxtypeid = null;

            gridrow = null;

            if (button.IsNull())
                return;

            foreach (GridViewRow r in this.gdvTaxes.Rows)
            {
                b2 = r.FindControl("btnInTaxRemove") as Button;

                if (b2.IsNull())
                    continue;

                if (button != b2)
                    continue;

                gridrow = r;
                hiddentaxtypeid = r.FindControl("hdfTaxTypeID") as HiddenField;
                hiddentransfer = r.FindControl("hdfTransfer") as HiddenField;
                lblname = r.FindControl("lblInTaxName") as Label;
                break;
            }

            if (hiddentaxtypeid.IsNull())
                return;

            if (gridrow.IsNull())
                return;

            row = null;
            row = this.AvailableTaxBaseTemplateTaxBaseTypeDataTable.NewGetTaxBaseTypeInAndOutRow();
            row.BeginEdit();
            row.Value = 0;
            row.Transfer = hiddentransfer.Value.ToBoolean();
            row.Name = lblname.Text;
            row.Fixed =
                row.Required =
                row.IvaAffected = false;
            row.Editable = (gridrow.FindControl("hdfEditable") as HiddenField).Value.ToBoolean();
            row.TaxIndex = 0;
            row.TaxBaseTemplateTaxTypeID = (gridrow.FindControl("hdfTaxTemplateTaxTypeID") as HiddenField).Value.ToInt32();
            row.TaxTypeID = hiddentaxtypeid.Value.ToInt32();
            row.EndEdit();

            this.AvailableTaxBaseTemplateTaxBaseTypeDataTable.AddGetTaxBaseTypeInAndOutRow(row);
            row.AcceptChanges();

            foreach (PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutRow r in this.TaxBaseTemplateTaxBaseTypeDataTable)
            {
                if (r.RowState == System.Data.DataRowState.Deleted)
                    continue;

                if (r.TaxTypeID != hiddentaxtypeid.Value.ToInt32())
                    continue;

                r.Delete();

                if (r.RowState != System.Data.DataRowState.Detached)
                    r.AcceptChanges();
                break;
            }

            temporaldata = this.GetTemporalRowData();

            this.gdvAvaliableTaxes.DataSource = this.AvailableTaxBaseTemplateTaxBaseTypeDataTable;
            this.gdvAvaliableTaxes.DataBind();

            this.gdvTaxes.DataSource = this.TaxBaseTemplateTaxBaseTypeDataTable;
            this.gdvTaxes.DataBind();

            this.InitializeGridRowsTaxesSelected();

            this.SetTemporalRowData(temporaldata);
            temporaldata.Clear();
            temporaldata = null;
        }
    }
}