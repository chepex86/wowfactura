﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using System.Data;

namespace PACFDManager.Administrator
{
    public partial class TaxTemplateSelection : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.LoadData();
        }

        protected void lnkBtnSelect_OnClick(object sender, EventArgs e)
        {
            LinkButton s = sender as LinkButton;
            this.Response.Redirect(String.Format(typeof(TaxTemplateAdd).Name + ".aspx?template={0}", s.CommandArgument));
        }

        private bool LoadData()
        {
            try
            {
                PACFD.Rules.TaxBaseTemplate template = new PACFD.Rules.TaxBaseTemplate();
                TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable table;

                table = template.SelectForList();
                this.gdvTemplates.DataSource = table;

                if (table.Count < 1)
                {
                    DataTable dt = new TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable();
                    DataRow dr = dt.NewRow();
                    dr["TaxBaseTemplateID"] = -1;
                    dr["Name"] = String.Empty;
                    dr["BillingType"] = String.Empty;
                    dr["Description"] = String.Empty;

                    dt.Rows.Add(dr);

                    this.ViewState["Empty"] = true;

                    this.gdvTemplates.DataSource = dt;
                }

                this.gdvTemplates.DataBind();

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        protected void gdvTemplates_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowIndex > -1)
            {
                if (!this.ViewState["Empty"].IsNull() && Convert.ToBoolean(this.ViewState["Empty"]))
                {
                    for (int j = 0; j < row.Cells.Count; j += 1)
                        row.Cells[j].Visible = false;
                    this.ViewState["Empty"] = null;
                }
            }
        }
    }
}