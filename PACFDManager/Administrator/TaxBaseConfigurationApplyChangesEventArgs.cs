﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public class TaxBaseConfigurationApplyChangesEventArgs : EventArgs
    {
        public PACFD.DataAccess.TaxBaseTemplatesDataSet DataSet { get; private set; }        
        public TaxBaseConfigurationEditorMode EditorMode { get; private set; }

        public TaxBaseConfigurationApplyChangesEventArgs(PACFD.DataAccess.TaxBaseTemplatesDataSet dataset,
             TaxBaseConfigurationEditorMode editormode)
        {
            this.DataSet = dataset;
            this.EditorMode = editormode;
        }
    }
}