﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTypeCatalogAdd : PACFDManager.BasePage
    {
        const string MESSAGE_QUESTION_ADDNEW = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.TaxTypeCatalogEditor21.EditorMode = TaxTypeCatalogEditorMode.Add;
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            PACFD.Rules.TaxTypes taxtypes = new PACFD.Rules.TaxTypes();
            PACFD.DataAccess.TaxTypesDataSet dataset = null;
            PACFD.DataAccess.TaxTypesDataSet datasettemp = null;

            try
            {
                datasettemp = dataset = this.TaxTypeCatalogEditor21.GenerateTaxType();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return;
            }

            if (!taxtypes.Update(dataset))
            {
                this.WebMessageBox1.ShowMessage("No se pudo registrar el impuesto.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un tipo de impuesto al catalogo.",
                this.UserName), this.CurrentBillerID, datasettemp.GetXml(), dataset.GetXml());
            #endregion

            dataset.Dispose();
            datasettemp.Dispose();

            this.WebMessageBox1.CommandArguments = MESSAGE_QUESTION_ADDNEW;
            this.WebMessageBox1.ShowMessage("Exíto al registrar impuesto. ¿Desea agregar otro?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Yes && e.CommandArguments == MESSAGE_QUESTION_ADDNEW)
            {
                this.Response.Redirect(typeof(TaxTypeCatalogAdd).Name + ".aspx");
                return;
            }

            this.btnCancel_Click(this, EventArgs.Empty);
        }

        protected void btnCancel_Click(object sender, EventArgs e) { Response.Redirect(typeof(TaxTypeCatalogList).Name + ".aspx"); }
    }
}
