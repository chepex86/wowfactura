﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="TaxTypeCatalogModify.aspx.cs" Inherits="PACFDManager.Administrator.TaxTypeCatalogModify"
    EnableViewState="true" %>

<%@ Register Src="TaxTypeCatalogEditor.ascx" TagName="TaxTypeCatalogEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <uc1:TaxTypeCatalogEditor ID="TaxTypeCatalogEditor1" runat="server" />
                        <br />
                        <ul>
                            <li class="buttons">
                                <asp:Button ID="btnAcept" runat="server" Text="Aceptar" OnClick="btnAcept_Click"
                                    CssClass="Button" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
                                    CssClass="Button" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
