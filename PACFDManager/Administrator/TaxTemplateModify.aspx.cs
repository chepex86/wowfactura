﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxTemplateModify : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.TaxTemplateEditor1.EditorMode = TaxTemplateEditorMode.Edit;

            if (this.IsPostBack)
                return;

            if (this.Session["taxtemplateid"].IsNull())
            {
                LogManager.WriteError(new Exception("TaxTemplate is null."));
                this.WebMessageBox1.ShowMessage("No se encontro la configuración.", true);
                return;
            }

            this.TaxTemplateEditor1.EditorMode = TaxTemplateEditorMode.Edit;
            this.TaxTemplateEditor1.TaxTemplateID = this.Session["taxtemplateid"].ToString().ToInt32();

            try
            {
                this.TaxTemplateEditor1.DataBindTaxes();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage("No se encontro la configuración.", true);
            }

            this.Session["taxtemplateid"] = null;
        }

        protected void TaxTemplateEditor1_ApplyChanges(object sender, TaxTemplateApplyChangesEventArgs e)
        {
            PACFD.Rules.TaxTemplate tem = new PACFD.Rules.TaxTemplate();

            DataSet taxdatasettemp = e.DataSet;

            if (e.EditorMode != TaxTemplateEditorMode.Edit)
                return;

            this.UpdateDataSetChanges(e.DataSet);

            if (!tem.Update(e.DataSet))
            {
                this.WebMessageBox1.ShowMessage("No se pudo actualizar la configuración.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico una configuración de impuesto.",
                this.UserName), this.CurrentBillerID, taxdatasettemp.GetXml(), e.DataSet.GetXml());
            #endregion

            taxdatasettemp.Dispose();

            this.WebMessageBox1.CommandArguments = "-1";
            this.WebMessageBox1.ShowMessage("Configuración actualizada.", System.Drawing.Color.Green);
        }

        private void UpdateDataSetChanges(PACFD.DataAccess.TaxTemplatesDataSet dataset)
        {
            PACFD.Rules.TaxTemplate tax = new PACFD.Rules.TaxTemplate();
            PACFD.DataAccess.TaxTemplatesDataSet.GetTaxTypeInAndOutDataTable dold;
            PACFD.DataAccess.TaxTemplatesDataSet.TaxTemplateTaxTypeRow row;
            bool found = false;

            dold = tax.SelectTaxTypeInOrOut(dataset.TaxTemplates[0].TaxTemplateID, true);

            for (int i = 0; i < dold.Count; i++)
            {
                for (int i2 = 0; i2 < dataset.TaxTemplateTaxType.Count; i2++)
                {
                    if (dataset.TaxTemplateTaxType[i2].RowState == System.Data.DataRowState.Deleted)
                        continue;

                    if (dold[i].TaxTemplateTaxTypeID != dataset.TaxTemplateTaxType[i2].TaxTemplateTaxTypeID)
                        continue;

                    found = true;
                    break;
                }

                if (!found)
                {
                    row = dataset.TaxTemplateTaxType.NewTaxTemplateTaxTypeRow();
                    row.BeginEdit();

                    row.TaxTemplateTaxTypeID = dold[i].TaxTemplateTaxTypeID;
                    row.TaxTemplateID = this.TaxTemplateEditor1.TaxTemplateID;
                    row.TaxTypeID = dold[i].TaxTypeID;
                    row.TaxIndex = dold[i].TaxIndex;
                    row.Value = dold[i].Value;

                    row.EndEdit();
                    dataset.TaxTemplateTaxType.AddTaxTemplateTaxTypeRow(row);
                    row.AcceptChanges();
                    row.Delete();
                }

                found = false;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            //if (e.CommandArguments != "-1")
            this.btnCancel_Click(null, EventArgs.Empty);
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                this.TaxTemplateEditor1.InvokeApplyChanges();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxTemplateList).Name + ".aspx"); }
    }
}