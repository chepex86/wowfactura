﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public enum WebServiceAccessesEditorMode
    {
        Add = 0,
        Edit = 1
    }
}
