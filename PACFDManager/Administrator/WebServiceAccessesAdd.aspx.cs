﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class WebServiceAccessesAdd : PACFDManager.BasePage
    {
        const string MESSAGE_ADDED = "MESSAGE_ADDED";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.WebServiceAccessesEditor1.EditorMode = WebServiceAccessesEditorMode.Add;
            this.WebServiceAccessesEditor1.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(WebServiceAccessesList).Name + ".aspx");
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            try
            {
                this.WebServiceAccessesEditor1.InvokeApplyChanges();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage("Error al crear acceso.", true);
            }
        }

        protected void WebServiceAccessesEditor1_ApplyChanges(object sender, WebServiceAccessesAddEventArgs e)
        {
            PACFD.Rules.WebServiceAccesses web = new PACFD.Rules.WebServiceAccesses();

            if (!web.Update(e.DataTable))
            {
                this.WebMessageBox1.ShowMessage("Error al crear acceso.", true);
                return;
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(e.DataTable);

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo acceso al web service.",
                this.UserName), this.CurrentBillerID, null, ds.GetXml());
            #endregion

            ds.Dispose();

            this.WebMessageBox1.CommandArguments = MESSAGE_ADDED;
            this.WebMessageBox1.ShowMessage("Acceso agregado.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ADDED:
                    this.btnCancel_Click(null, EventArgs.Empty);
                    break;
            }
        }
    }
}