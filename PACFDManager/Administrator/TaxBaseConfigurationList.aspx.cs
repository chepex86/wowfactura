﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxBaseConfigurationList : PACFDManager.BasePage
    {
        private int TemporalTaxBaseTemplateID
        {
            get
            {
                int r;
                object o;

                o = this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return 0;

                if (!int.TryParse(o.ToString(), out r))
                    return 0;

                return r;
            }
            set { this.Session[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PACFD.Rules.TaxBaseTemplate template;

            //if (this.CurrentBillerID < 1)
            //    this.Response.Redirect("..\\Default.aspx");

            if (this.IsPostBack)
                return;

            template = new PACFD.Rules.TaxBaseTemplate();
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable table;

            table = template.SelectForList();
            this.gdvTemplates.DataSource = table;

            if (table.Count < 1)
            {
                System.Data.DataTable dt = new PACFD.DataAccess.TaxBaseTemplatesDataSet.GetNameTaxTypesDataTable();
                System.Data.DataRow dr = dt.NewRow();
                dr["TaxBaseTemplateID"] = -1;
                dr["Name"] = String.Empty;
                dr["BillingType"] = String.Empty;
                dr["Description"] = String.Empty;

                dt.Rows.Add(dr);

                this.ViewState["Empty"] = true;
                this.gdvTemplates.DataSource = dt;
            }

            this.gdvTemplates.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxBaseConfigurationAdd).Name + ".aspx"); }

        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            int id;
            ImageButton b = sender as ImageButton;

            if (b.IsNull())
                return;

            id = b.CommandArgument.ToInt32();
            this.TemporalTaxBaseTemplateID = id;

            switch (b.ID)
            {
                case "imgbDelete":
                    this.WebMessageBox1.CommandArguments = "delete";
                    this.WebMessageBox1.ShowMessage("¿Desea eliminar la configuración?",
                        System.Drawing.Color.Blue, WebMessageBoxButtonType.YesNo);
                    break;
                case "imgbEdit":
                    this.Session["taxbasetemplateid"] = id;
                    this.Response.Redirect(typeof(TaxBaseConfigurationModify).Name + ".aspx");
                    break;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.TaxBaseTemplate tem = new PACFD.Rules.TaxBaseTemplate();
            PACFD.DataAccess.TaxBaseTemplatesDataSet taxdataset;
            PACFD.DataAccess.TaxBaseTemplatesDataSet taxdatasettemp;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            switch (e.CommandArguments)
            {
                case "delete":
                    taxdatasettemp = taxdataset = tem.SelectByTaxBaseTemplateID(this.TemporalTaxBaseTemplateID);

                    for (int i = taxdataset.TaxBaseTemplateTaxType.Count - 1; i >= 0; i--)
                    {
                        taxdataset.TaxBaseTemplateTaxType[i].BeginEdit();
                        taxdataset.TaxBaseTemplateTaxType[i].Delete();
                        taxdataset.TaxBaseTemplateTaxType[i].EndEdit();
                    }

                    tem.Update(taxdataset);

                    taxdataset.TaxBaseTemplates[0].BeginEdit();
                    taxdataset.TaxBaseTemplates[0].Delete();
                    taxdataset.TaxBaseTemplates[0].EndEdit();

                    tem.Update(taxdataset);

                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino una configuración de impuesto base.",
                        this.UserName), this.CurrentBillerID, taxdatasettemp.GetXml(), taxdataset.GetXml());
                    #endregion

                    taxdataset.Dispose();
                    taxdatasettemp.Dispose();

                    this.WebMessageBox1.CommandArguments = string.Empty;
                    this.Response.Redirect(typeof(TaxBaseConfigurationList).Name + ".aspx");
                    break;
            }
        }

        protected void gdvTemplates_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowIndex > -1)
            {
                if (!this.ViewState["Empty"].IsNull() && Convert.ToBoolean(this.ViewState["Empty"]))
                {
                    for (int j = 0; j < row.Cells.Count; j += 1)
                        row.Cells[j].Visible = false;
                    this.ViewState["Empty"] = null;
                }
            }
        }
    }
}