﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxBaseConfigurationAdd : PACFDManager.BasePage
    {
        const string MESSAGE_QUESTION_ADDNEW = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.TaxBaseConfigurationEditor1.EditorMode = TaxBaseConfigurationEditorMode.Add;

            if (this.IsPostBack)
                return;

            this.TaxBaseConfigurationEditor1.DataBindTaxes();
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments == MESSAGE_QUESTION_ADDNEW && e.DialogResult == WebMessageBoxDialogResultType.Yes)
            {
                this.Response.Redirect(typeof(TaxBaseConfigurationAdd).Name + ".aspx");
                return;
            }

            this.btnCancel_Click(this, EventArgs.Empty);
        }

        protected void TaxBaseConfigurationEditor1_ApplyChanges(object sender, TaxBaseConfigurationApplyChangesEventArgs e)
        {
            PACFD.Rules.TaxBaseTemplate template;

            if (e.EditorMode != TaxBaseConfigurationEditorMode.Add)
                return;

            template = new PACFD.Rules.TaxBaseTemplate();

            DataSet dsTemp = e.DataSet;

            if (!template.Update(e.DataSet))
            {
                template = null;
                this.WebMessageBox1.CommandArguments = string.Empty;
                this.WebMessageBox1.ShowMessage("No se pudo registrar la configuración.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego una nueva configuración base al sistema.",
                this.UserName), this.CurrentBillerID, dsTemp.GetXml(), e.DataSet.GetXml());
            #endregion

            dsTemp.Dispose();

            this.WebMessageBox1.CommandArguments = MESSAGE_QUESTION_ADDNEW;
            this.WebMessageBox1.ShowMessage("Registro de configuración éxitoso. ¿Desea agragar otra configuración?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                this.TaxBaseConfigurationEditor1.GenerateTemplate();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxBaseConfigurationList).Name + ".aspx"); }
    }
}