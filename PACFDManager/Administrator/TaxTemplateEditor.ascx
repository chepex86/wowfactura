﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxTemplateEditor.ascx.cs"
    Inherits="PACFDManager.Administrator.TaxTemplateEditor" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<div id="container" class="wframe50">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTaxesTemplateTitle" runat="server" Text="Configuración de comprobantes" />
                </h2>
            </div>
            <ul>
                <li>
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Nombre" />
                    </label>
                    <span class="vtip" title="Nombre de la plantilla.<br />El nombre se mostrará al<br />crear un nuevo comprobante.">
                        <asp:TextBox ID="txtName" runat="server" />
                    </span></li>
                <li>
                    <label class="desc">
                        <asp:Label ID="lblBillingType" runat="server" Text="Tipo de comprobante" CssClass="desc" />
                    </label>
                    <span class="vtip" title="Define el tipo de comprobante <b>Ingreso</b> o <b>Egreso</b>">
                        <asp:DropDownList ID="ddlBillingType" runat="server">
                            <asp:ListItem Value="1">ingreso</asp:ListItem>
                            <asp:ListItem Value="2">egreso</asp:ListItem>
                        </asp:DropDownList>
                    </span></li>
                <li style="float: left;">
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblPrintTemplate" runat="server" Text="Configuración de impresión" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title="Define el tipo de configuración se usará al crear el PDF.">
                                    <asp:DropDownList ID="ddlPrintTemplate" runat="server">
                                    </asp:DropDownList>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="ddlPrintTemplate_RequiredFieldValidator" runat="server"
                                        Display="Dynamic" ControlToValidate="ddlPrintTemplate" InitialValue="0" ErrorMessage="Selecione reporte valido." />
                                </div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li id="liApplyTaxes" runat="server">
                    <label class="desc">
                        <asp:Label ID="lblTaxInclude" runat="server" Text="Conceptos incluyen impuestos" />
                    </label>
                    <span class="vtip" title="Define el tipo de desglosado al crear el comprobante.">
                        <asp:DropDownList ID="ddlConceptsTaxIncluded" runat="server">
                            <asp:ListItem Selected="false" Text="No" Value="false" />
                            <asp:ListItem Text="Si" Value="true" />
                        </asp:DropDownList>
                    </span></li>
                <li>
                    <label class="desc">
                        <asp:Label ID="Label1" runat="server" Text="Descripción" CssClass="desc" />
                    </label>
                    <span class="vtip" title="Descripción del tipo de plantilla.<br />Esta descripción se muestra al posar el mouse sobre el<br/>menú de plantillas en la página de nuevo comprobante.">
                        <asp:TextBox ID="txtDescription" runat="server" Height="71px" Width="201px" MaxLength="255"
                            TextMode="MultiLine"></asp:TextBox>
                    </span></li>
            </ul>
        </div>
    </div>
</div>
<br />
<table style="width: 100%">
    <tr>
        <td valign="top">
            <label class="desc">
                <span class="vtip" title='Impuesto disponibles en la base de datos.<br /><br />Para agregar un impuesto al comprobante<br />seleccione la casilla del impuesto.'>
                    <asp:Label ID="lblAvailableTaxes" runat="server" Text="Impuestos disponibles."></asp:Label>
                </span>
            </label>
        </td>
        <td valign="top">
            <label class="desc">
                <span class="vtip" title='Impuestos disponibles al momento el comprobante.<br /><br />Los impuestos en este listado se mostrarán al momento<br />de crear un comprobante.<br /><br />Para quitar un impuesto de la lista deseleccione<br />la casilla del impuesto.'>
                    <asp:Label ID="lblTaxesInUse" runat="server" Text="Impuesto en uso."></asp:Label></span>
            </label>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <asp:GridView ID="gdvAvaliableTaxes" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblAvailableGridTaxUse" runat="server" Text='<%#Eval("Name").ToString()%>'
                                ToolTip="Seleccione esta opción si desea usar este impuesto al faturar." />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tipo">
                        <ItemTemplate>
                            <asp:Label ID="lblTaxType" runat="server" Text='<%#Eval("Transfer").ToString() == "True" ? "Transferido" : "Retenido"%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <uc1:Tooltip ID="ttpAddButton" runat="server" ToolTip="Agrega el impuesto a la configuración." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title="Seleccione esta opción si desea usar<br />este impuesto al emitir el comprobante.">
                                <asp:Button ID="btnTaxAdd" runat="server" Text="Agregar" OnClick="btnAvailableGridTaxUse_CheckedChanged" /></span>
                            <asp:HiddenField ID="hdfAvailableTaxTypeID" runat="server" Value='<%#Eval("TaxTypeID")%>' />
                            <asp:HiddenField ID="hdfAvailableEditable" runat="server" Value='<%#Eval("Editable")%>' />
                            <asp:HiddenField ID="hdfTransfer" runat="server" Value='<%#Eval("Transfer")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
        <td valign="top">
            <asp:GridView ID="gdvTaxes" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay impuestos configurados.">
                <Columns>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblInTaxName" runat="server" Text='<%#Eval("Name").ToString()%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tipo">
                        <ItemTemplate>
                            <asp:Label ID="lblTaxInType" runat="server" Text='<%#Eval("Transfer").ToString() == "True" ? "Transferido" : "Retenido"%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Valor">
                        <ItemTemplate>
                            <span class="vtip" title="Porcentajes de impuestos.<br />Seleccione la opción a desplegar<br />en el comprobante.">
                                <asp:DropDownList ID="ddlGridTax" runat="server" DataTextFormatString="{0:0.00}" />
                            </span><span class="vtip" title="Escriba el valor a desplegar en el comprobante.">
                                <asp:TextBox ID="txtGridTax" Width="50px" runat="server" /></span>
                            <br />
                            <asp:RangeValidator ID="rnvGridTaxValue" runat="server" ErrorMessage="Solo % (0.00 - 100.00)."
                                MaximumValue="100.00" MinimumValue="0.00" Type="Double" ControlToValidate="txtGridTax"
                                Display="Dynamic" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <uc1:Tooltip ID="ttpRemoveButton" runat="server" ToolTip="Remueve el impuesto de la lista de configuración." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title="Seleccione esta opción si no desea usar<br />este impuesto al emitir el comprobante.">
                                <asp:Button ID="btnInTaxRemove" runat="server" Text="Quitar" OnClick="btnGridTaxUse_CheckedChanged" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Orden">
                        <ItemTemplate>
                            <table style="border-width: 0px;" cellpadding="0px" cellspacing="0px">
                                <tr>
                                    <td class="vtip" title="Subir desplegado de impuesto.">
                                        <asp:ImageButton ID="imgbTaxUp" runat="server" ImageUrl="~/Includes/Images/png/upArrow.png"
                                            AlternateText="Up" CommandArgument='<%#"up_" + Eval("TaxTypeID").ToString()%>'
                                            OnClick="ImageButton_Click" CausesValidation="false" />
                                    </td>
                                    <td class="vtip" title="Bajar desplegado de impuesto.">
                                        <asp:ImageButton ID="imgbTaxDown" runat="server" ImageUrl="~/Includes/Images/png/downArrow.png"
                                            AlternateText="Down" CommandArgument='<%#"down_" + Eval("TaxTypeID").ToString()%>'
                                            OnClick="ImageButton_Click" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hdfTaxTypeID" runat="server" Value='<%#Eval("TaxTypeID")%>' />
                            <asp:HiddenField ID="hdfEditable" runat="server" Value='<%#Eval("Editable")%>' />
                            <asp:HiddenField ID="hdfTaxTemplateTaxTypeID" runat="server" Value='<%#Eval("TaxTemplateTaxTypeID")%>' />
                            <asp:HiddenField ID="hdfValue" runat="server" Value='<%#Eval("Value")%>' />
                            <asp:HiddenField ID="hdfTransfer" runat="server" Value='<%#Eval("Transfer")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="dgHeader" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
            </asp:GridView>
        </td>
    </tr>
</table>
