﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public class WebServiceAccessesAddEventArgs
    {
        public PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable DataTable { get; private set; }
        public string PasswordUncripted
        {
            get
            {
                if (this.DataTable.Count < 1)
                    return string.Empty;

                return PACFD.Common.Cryptography.DecryptUnivisitString(this.DataTable[0].Password);
            }
        }


        public WebServiceAccessesAddEventArgs(PACFD.DataAccess.WebServiceAccessesDataSet.WebServiceAccessesDataTable table)
        {
            this.DataTable = table;
        }
    }
}
