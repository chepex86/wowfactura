﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxTypeCatalogEditor.ascx.cs"
    Inherits="PACFDManager.Administrator.TaxTypeCatalogEditor" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<div class="info">
    <h2>
        <asp:Label ID="lblTaxesListTitle" runat="server" Text="Impuestos" />
    </h2>
</div>
<ul>
    <li>
        <label class="desc">
            <asp:Label ID="lblName" runat="server" Text="Nombre" />
        </label>
        <span class="vtip" title="Introduce el nombre del impuesto.">
            <asp:TextBox ID="txtName" runat="server" />
        </span></li>
    <li>
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblFiledType" runat="server" Text="Tipo de campo" />
                        <uc1:Tooltip ID="ttpFiledType" runat="server" ToolTip="Define sí el campo de impuesto es <b>seleccionable</b> o <b>editable</b>." />
                    </label>
                </td>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblBillingType" runat="server" Text="Tipo de impuesto." />
                        <uc1:Tooltip ID="ttpBillingType" runat="server" ToolTip="Define sí el impuesto es <b>trasferido</b> o <b>retenido</b>,<br />sí debe ser seleccionado/llenado <b>forzosamente</b> o <b>no</b>." />
                    </label>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:RadioButton ID="rbtnEditable" runat="server" Text="Editable" GroupName="radioButton_Editable"
                        Checked="true" />
                    <br />
                    <asp:RadioButton ID="rbtnSelect" runat="server" Text="Seleccionable" GroupName="radioButton_Editable" />
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbtnTransfer" runat="server" GroupName="G_Transfer_Tax" Checked="true"
                                    Text="Transferido" />
                            </td>
                            <td>
                                <asp:RadioButton ID="rbtnRetained" runat="server" GroupName="G_Transfer_Tax" Text="Retenido" />
                            </td>
                        </tr>
                    </table>
                    <asp:CheckBox ID="ckbRequired" runat="server" Text="Requerido" Checked="true" />
                    <br />
                </td>
            </tr>
        </table>
    </li>
    <li  style="height:auto;">
        <div id="divTaxValues" runat="server" style="display: none">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblValueTitle" runat="server" Text="Valor"></asp:Label>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtValueText" runat="server" />
                                    <br />
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Solo valor positivos (0.0 - 100)"
                                        ControlToValidate="txtValueText" MinimumValue="0.0" MaximumValue="100" Type="Double" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAddTaxValue" runat="server" Text="Agregar" CssClass="Button" OnClick="btnAddTaxValue_Click" />
                                </td>
                                <td>
                                    <uc1:Tooltip ID="ttpAddTaxValue" runat="server" ToolTip="Agrega el valor capturado en<br />la lista de valores seleccionables." />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <asp:GridView ID="gdvTaxValues" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField ControlStyle-Width="0px" HeaderText="Valor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValue" runat="server" Text='<%#Convert.ToDecimal(Eval("Value"))%>'></asp:Label>
                                        <!-- Math.Round( , 2).ToString()-->
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" AlternateText="Eliminar" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                            CommandArgument='<%#Convert.ToDecimal(Eval("Value"))%>' OnClick="ImageButton_Click" />
                                            <!--Math.Round(  , 2)-->
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="dgHeader" />
                            <RowStyle CssClass="dgItem" />
                            <AlternatingRowStyle CssClass="dgAlternate" />
                            <EmptyDataTemplate>
                                No hay impuestos disponibles.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </li>
</ul>
