﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Administrator
{
    public partial class TaxBaseConfigurationModify : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.TaxBaseConfigurationEditor1.EditorMode = TaxBaseConfigurationEditorMode.Edit;

            if (this.IsPostBack)
                return;

            if (this.Session["taxbasetemplateid"].IsNull())
            {
                LogManager.WriteError(new Exception("TaxBaseTemplate is null."));
                this.WebMessageBox1.ShowMessage("No se encontro la configuración.", true);
                return;
            }

            this.TaxBaseConfigurationEditor1.EditorMode = TaxBaseConfigurationEditorMode.Edit;
            this.TaxBaseConfigurationEditor1.TaxBaseTemplateID = this.Session["taxbasetemplateid"].ToString().ToInt32();

            try
            {
                this.TaxBaseConfigurationEditor1.DataBindTaxes();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage("No se encontro la configuración.", true);
            }

            this.Session["taxbasetemplateid"] = null;
        }

        protected void TaxBaseConfigurationEditor1_ApplyChanges(object sender, TaxBaseConfigurationApplyChangesEventArgs e)
        {
            PACFD.Rules.TaxBaseTemplate tem = new PACFD.Rules.TaxBaseTemplate();

            if (e.EditorMode != TaxBaseConfigurationEditorMode.Edit)
                return;

            this.Update(e.DataSet);

            DataSet taxdatasettemp = e.DataSet;

            if (!tem.Update(e.DataSet))
            {
                this.WebMessageBox1.ShowMessage("No se pudo actualizar la configuración.", true);
                return;
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico una configuración de impuesto base.",
                this.UserName), this.CurrentBillerID, taxdatasettemp.GetXml(), e.DataSet.GetXml());
            #endregion

            taxdatasettemp.Dispose();

            this.WebMessageBox1.CommandArguments = "1";
            this.WebMessageBox1.ShowMessage("Configuración actualizada.", System.Drawing.Color.Green);
        }

        private void Update(PACFD.DataAccess.TaxBaseTemplatesDataSet dataset)
        {

            PACFD.Rules.TaxBaseTemplate template = new PACFD.Rules.TaxBaseTemplate();
            PACFD.DataAccess.TaxBaseTemplatesDataSet.GetTaxBaseTypeInAndOutDataTable table;
            PACFD.DataAccess.TaxBaseTemplatesDataSet.TaxBaseTemplateTaxTypeRow row;
            bool flag = false;

            table = template.SelectTaxBaseTypeInOrOut(dataset.TaxBaseTemplates[0].TaxBaseTemplateID, true);

            for (int i = 0; i < table.Count; i += 1)
            {
                for (int j = 0; j < dataset.TaxBaseTemplateTaxType.Count; j += 1)
                {
                    if (dataset.TaxBaseTemplateTaxType[j].RowState == System.Data.DataRowState.Deleted)
                        continue;

                    if (table[i].TaxBaseTemplateTaxTypeID != dataset.TaxBaseTemplateTaxType[j].TaxBaseTemplateTaxTypeID)
                        continue;

                    flag = true;
                    break;
                }

                if (!flag)
                {
                    row = dataset.TaxBaseTemplateTaxType.NewTaxBaseTemplateTaxTypeRow();
                    row.BeginEdit();

                    row.TaxBaseTemplateTaxTypeID = table[i].TaxBaseTemplateTaxTypeID;
                    row.TaxBaseTemplateID = this.TaxBaseConfigurationEditor1.TaxBaseTemplateID;
                    row.TaxTypeID = table[i].TaxTypeID;
                    row.TaxIndex = table[i].TaxIndex;
                    row.Value = table[i].Value;

                    row.EndEdit();
                    dataset.TaxBaseTemplateTaxType.AddTaxBaseTemplateTaxTypeRow(row);
                    row.AcceptChanges();
                    row.Delete();
                }

                flag = false;
            }
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments != "-1")
                this.btnCancel_Click(null, EventArgs.Empty);
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                this.TaxBaseConfigurationEditor1.GenerateTemplate();
            }
            catch (Exception ex)
             {
                LogManager.WriteError(ex);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e) { this.Response.Redirect(typeof(TaxBaseConfigurationList).Name + ".aspx"); }
    }
}