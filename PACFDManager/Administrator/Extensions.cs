﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Administrator
{

    /// <summary>
    /// This class extension only work on PACFDManager.Administrator namespace 
    /// and with add an modify pages.
    /// </summary>
    internal static class ExtensionGridViewAndTable
    {
        /// <summary>
        /// Get a integer value with the row index from an taxtypeid.
        /// </summary>
        /// <param name="id">Id to look for.</param>
        /// <returns>If success return the index value else -1.</returns>
        internal static int GetRowIndexByTaxTypeID(this GridViewRowCollection rows, int id)
        {
            HiddenField h;

            foreach (GridViewRow row in rows)
            {
                h = row.FindControl("hdfTaxTypeID") as HiddenField;

                if (h.IsNull())
                    continue;

                if (id.ToString() == h.Value)
                    return row.RowIndex;

                h = null;
            }

            return -1;
        }
        /// <summary>
        /// Set the index of a row in a table.
        /// </summary>
        /// <param name="index">Index of the row in the table.</param>
        /// <param name="toup">If true, up item in the index else down item index.</param>
        internal static void RowLevelIndex(this System.Data.DataTable table, int index, bool toup)
        {
            object[] array;
            System.Data.DataRow row;

            if (index < 0 || index > table.Rows.Count - 1 || (toup ? index - 1 < 0 : false))
                return;

            row = table.Rows[index];

            if (row.RowState == System.Data.DataRowState.Deleted)
                return;

            array = row.ItemArray;
            table.Rows.RemoveAt(index);

            row = table.NewRow();
            row.ItemArray = array;

            table.Rows.InsertAt(row, index + (toup ? -1 : 1));
            row.AcceptChanges();
        }
    }
}
