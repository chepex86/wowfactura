﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebServiceAccessesEditor.ascx.cs"
    Inherits="PACFDManager.Administrator.WebServiceAccessesEditor" %>
<ul>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblGroup" runat="server" Text="Grupo:" />
        </label>
        <span class="vtip" title="Grupo.">
            <asp:DropDownList ID="ddlGroup" runat="server" DataTextField="Name" DataValueField="GroupID"
                OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" AutoPostBack="true" />
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblCompany" runat="server" Text="Empresa:" />
        </label>
        <span class="vtip" title="Empresa vinculada.">
            <asp:DropDownList ID="ddlBiller" runat="server" DataValueField="BillerID" DataTextField="Name"
                OnSelectedIndexChanged="ddlBiller_SelectedIndexChanged" AutoPostBack="true" />
        </span></li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Nombre:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Nombre de usuario.">
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="200px" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="txtName_RequiredFieldValidator" runat="server" ErrorMessage="Nombre requerido."
                            ControlToValidate="txtName" Display="Dynamic" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblPassword" runat="server" Text="Contraseña:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Contraseña del usuario.">
                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" Width="200px" TextMode="Password" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="txtPassword_RequiredFieldValidator" runat="server"
                            ErrorMessage="Contraseña requerida." ControlToValidate="txtPassword" Display="Dynamic" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <table>
            <tr>
                <td>
                    <label class="desc">
                        <asp:Label ID="lblPasswordReenter" runat="server" Text="Reentrar Contraseña:" />
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="vtip" title="Contraseña del usuario.">
                        <asp:TextBox ID="txtPasswordReenter" runat="server" MaxLength="50" Width="200px"
                            TextMode="Password" />
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="validator-msg">
                        <asp:CompareValidator ID="txtPasswordReenter_CompareValidator" runat="server" ErrorMessage="Contraseña no coincide."
                            ControlToValidate="txtPassword" ControlToCompare="txtPasswordReenter" Display="Dynamic" />
                    </div>
                </td>
            </tr>
        </table>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblUserActive" runat="server" Text="Usuario activo." />
        </label>
        <label class="desc">
            <asp:CheckBox ID="ckbActive" runat="server" Checked="true" TextAlign="Left" />
        </label>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblPrintTemplate" runat="server" Text="PDF de impreción:" />
        </label>
        <span class="vtip" title="Selecciona el tipo de impresion al<br />crear un comprobante por webservice.">
            <asp:DropDownList ID="ddlPrintDocument" runat="server" DataValueField="PrintTemplateID"
                DataTextField="Name" />
        </span></li>
</ul>
