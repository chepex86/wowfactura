﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Administrator
{
    public delegate void TaxBaseConfigurationApplyChanges(object sender, TaxBaseConfigurationApplyChangesEventArgs args);

    public enum TaxBaseConfigurationEditorMode
    {
        Add = 0,
        Edit
    }
}
