﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="TaxTemplateModify.aspx.cs" Inherits="PACFDManager.Administrator.TaxTemplateModify" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="TaxTemplateEditor.ascx" TagName="TaxTemplateEditor" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:TaxTemplateEditor ID="TaxTemplateEditor1" runat="server" OnApplyChanges="TaxTemplateEditor1_ApplyChanges" />
            <div class="form">
                <ul>
                    <li class="buttons">
                        <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button" OnClick="btnCancel_Click"
                            CausesValidation="false" />
                        <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
                    </li>
                </ul>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
