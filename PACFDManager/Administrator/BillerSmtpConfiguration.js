﻿//
//class
//
function jclassBillerSmtpConfiguration(valist, ckblist, txtlist) {
    var that = this; //convention declaration

    var txtHost_RequiredFieldValidator = document.getElementById(valist.split(",")[0]);
    var txtPort_RequiredFieldValidator = document.getElementById(valist.split(",")[1]);
    var txtCredentialName_RequiredFieldValidator = document.getElementById(valist.split(",")[2]);
    var txtCredentialName_RegularExpressionValidator = document.getElementById(valist.split(",")[3]);
    var txtCredentialPassword_RequiredFieldValidator = document.getElementById(valist.split(",")[4]);
    var txtCredentialPasswordVerification_RequiredFieldValidator = document.getElementById(valist.split(",")[5]);
    var txtCredentialPasswordVerification_CompareValidator = document.getElementById(valist.split(",")[6]);
    var txtDisplayName_RequiredFieldValidator = document.getElementById(valist.split(",")[7]);

    var validatorsList = valist;

    var ckbEnableSsl = document.getElementById(ckblist.split(",")[0]);
    var ckbUseBaseEmail = document.getElementById(ckblist.split(",")[1]);
    var ckbUseSmtpConfiguration = document.getElementById(ckblist.split(",")[2]);

    var checkboxList = ckblist;

    var txtHost = document.getElementById(txtlist.split(",")[0]);
    var txtPort = document.getElementById(txtlist.split(",")[1]);
    var txtBcc = document.getElementById(txtlist.split(",")[2]);
    var txtCredentialName = document.getElementById(txtlist.split(",")[3]);
    var txtCredentialPassword = document.getElementById(txtlist.split(",")[4]);
    var txtCredentialPasswordVerification = document.getElementById(txtlist.split(",")[5]);
    var txtDisplayName = document.getElementById(txtlist.split(",")[6]);

    var textboxList = txtlist;

    this.UseBaseEmail_Click = function() {
        var c = ckbUseBaseEmail; //document.getElementById(chkbox);
        var enabled = !c.checked;

        if (ckbUseSmtpConfiguration.checked) {
            c.checked = false;
            return;
        }

        SetValidatorsEnabled(enabled);
        SetTextBoxVisible(enabled ? "block" : "none");
        ValidatorEnable(txtDisplayName_RequiredFieldValidator, true);
        txtDisplayName.style.display = "block";
        ckbEnableSsl.style.display = enabled ? "block" : "none";
    }

    function SetValidatorsEnabled(enabled) {
        for (var i = 0; i < validatorsList.split(",").length; i++) {
            ValidatorEnable(document.getElementById(validatorsList.split(",")[i]), enabled);
        }
    }

    function SetTextBoxVisible(display) {
        for (var i = 0; i < textboxList.split(",").length; i++) {
            document.getElementById(textboxList.split(",")[i]).style.display = display;
        }
    }

    this.SmtpConfiguration_Click = function() {
        var enabled = !ckbUseSmtpConfiguration.checked;

        this.UseBaseEmail_Click();
        SetValidatorsEnabled(enabled);
        SetTextBoxVisible(enabled ? "block" : "none");
        ckbEnableSsl.style.display = enabled ? "block" : "none";
        ckbUseBaseEmail.checked = false;
    }
}