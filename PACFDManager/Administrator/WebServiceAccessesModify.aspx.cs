﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
using System.Data;
#endregion

namespace PACFDManager.Administrator
{
    public partial class WebServiceAccessesModify : PACFDManager.BasePage
    {
        const string MESSAGE_ADDED = "MESSAGE_ADDED";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
                return;

            this.WebServiceAccessesEditor1.WebServiceAccessesID = this.Session["id"].ToString().ToInt32();
            this.WebServiceAccessesEditor1.EditorMode = WebServiceAccessesEditorMode.Edit;
            this.WebServiceAccessesEditor1.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(WebServiceAccessesList).Name + ".aspx");
        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            try
            {
                this.WebServiceAccessesEditor1.InvokeApplyChanges();       
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                this.WebMessageBox1.ShowMessage("Error al modificar acceso.", true);
            }
        }

        protected void WebServiceAccessesEditor1_ApplyChanges(object sender, WebServiceAccessesAddEventArgs e)
        {
            PACFD.Rules.WebServiceAccesses web = new PACFD.Rules.WebServiceAccesses();
            DataTable dt = new DataTable();

            dt = e.DataTable;

            if (!web.Update(e.DataTable))
            {
                this.WebMessageBox1.ShowMessage("Error al modificar acceso.", true);
                return;
            }

            DataSet ds = new DataSet();

            ds.Tables.Add(e.DataTable);
            ds.Dispose();

            this.WebMessageBox1.CommandArguments = MESSAGE_ADDED;
            this.WebMessageBox1.ShowMessage("Acceso modificado con éxito.", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ADDED:
                    this.btnCancel_Click(null, EventArgs.Empty);
                    break;
            }
        }
    }
}