<%@ Page Title="P�gina de inicio" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PACFDManager.Default1" %>

<%@ Register Src="News.ascx" TagName="News" TagPrefix="uc1" %>
<%@ Register Src="WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td valign="top">
                        <div <%--style="margin-left: 100px;"--%>>
                            <table class="container_l">
                                <tr>
                                    <td class="img_information" colspan="2">
                                        <asp:Image ID="imgLogo" CssClass="imgCompany" runat="server" ImageUrl="~/Includes/Images/jpg/Default/no_image-125x125.jpg" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="Label8" runat="server" Text="ESTATUS"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trDCTitle" runat="server" visible="false">
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="Label5" runat="server" Text="Certificado Digital"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trDCState" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleStateCertificate" runat="server" Text="Estado:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblStateCertificate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trDCExpiration" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleExpirationDate" runat="server" Text="Fecha de Expiraci�n:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblExpirationCertificate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="Label9" runat="server" Text="Folios"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTitleSerial" runat="server" Text="Serie de los Folios:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblSerial" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTitleFoliosUnused" runat="server" Text="Folios Disponibles:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblUnused" runat="server" Font-Bold="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTitleFoliosUsed" runat="server" Text="Folios Usados:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblFoliosUsed" runat="server" Font-Bold="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTitleFoliosCanceled" runat="server" Text="Folios Cancelados:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblCanceled" runat="server" Font-Bold="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trExchangeRate1" runat="server" visible="false">
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="lblTitleExchangeRateDate" runat="server" Text="Tipo de Cambio" />
                                    </td>
                                </tr>
                                <tr id="trExchangeRate2" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleExchangeRate" runat="server" Text="Tipo de Cambio a USD:" />
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblExchangeRate" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trExchangeRate3" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleLatestRevisionExchangeRates" runat="server" Text="Ultima revisi�n del Tipo de Cambio:" />
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblLatestRevisionExchangeRates" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trReportTitle" runat="server" visible="false">
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="Label7" runat="server" Text="Reportes"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trReportInfo" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleLastReportGenerated" runat="server" Text="Ultimo Reporte Generado:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblLastReportGenerated" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trReportDate" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleDateOfCreation" runat="server" Text="Fecha de Creaci�n:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblDateOfCreation" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trAlert1" runat="server" visible="false">
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="lblTitleAlert" runat="server" Text="Alerta" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trAlert2" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleAlertSent" runat="server" Text="Falta Enviar al SAT el                                               Reporte Mensual:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAlertReportSent" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trAlert3" runat="server" visible="false">
                                    <td>
                                        <asp:Label ID="lblTitleAlertGenerate" runat="server" Text="Falta Generar el Reporte Mensual:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAlertReportGenerate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="tr1" runat="server">
                                    <td class="Titulo" colspan="2">
                                        <asp:Label ID="lblTitleBackup" runat="server" Text="Respaldo BDD"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="tr2" runat="server">
                                    <td>
                                        <asp:Label ID="lblTitleBackupName" runat="server" Text="Nombre:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblBackupName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="tr3" runat="server">
                                    <td>
                                        <asp:Label ID="lblTitleBackupDate" runat="server" Text="Fecha de Respaldo:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblBackupDate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="tr4" runat="server">
                                    <td>
                                        <asp:Label ID="lblTitleBackupPath" runat="server" Text="Ruta:"></asp:Label>
                                    </td>
                                    <td class="lbl_bold">
                                        <asp:Label ID="lblBackupPath" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="container_r">
                            <uc1:News ID="News1" runat="server" />
                        </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div>
                <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
