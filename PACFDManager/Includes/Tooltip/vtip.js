/*
* Vertigo Tip by www.vertigo-project.com
* Requires jQuery
* Modify by Pablo Mendoza A. for .Net Ajaxs
*/
this.vtip = function() {
    this.xOffset = -10; // x distance from mouse
    this.yOffset = 30; // y distance from mouse

    jQuery(".vtip").each(function(index) { var x = jQuery(this); x.attr("class", "vtip:" + x.attr("title")); x.attr("title", ""); });

    jQuery('[class*="vtip:"]').unbind().hover(function(e) {
        this.t = this.className;
        this.t = this.t.substring(this.t.indexOf("vtip:", 0) + "vtip:".length, this.t.length);
        //this.t = this.title;
        //this.title = '';
        this.top = (e.pageY + yOffset);
        this.left = (e.pageX + xOffset);

        jQuery('body').append('<p id="vtip"><img id="vtipArrow" />' + this.t + '</p>');
        //$('p#vtip #vtipArrow').attr("src", 'Ascx/ToolTip/images/vtip_arrow.png');
        jQuery('p#vtip #vtipArrow').attr("src", CurrentUrlAplication + 'Includes/Tooltip/images/vtip_arrow.png');
        jQuery('p#vtip').css("top", this.top + "px").css("left", this.left + "px").fadeIn("fast");
    }, function() {
        //this.title = this.t;
        jQuery("p#vtip").fadeOut("fast").remove();
    }).mousemove(function(e) {
        this.top = (e.pageY + yOffset);
        this.left = (e.pageX + xOffset);
        jQuery("p#vtip").css("top", this.top + "px").css("left", this.left + "px");
    });
};

jQuery(document).ready(function($) { vtip(); if (Sys != undefined && Sys != null) { Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() { vtip(); }); } }) 