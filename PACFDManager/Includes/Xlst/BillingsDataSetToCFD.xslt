﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" encoding="utf-8" />
  <xsl:template match="/">
    <Comprobante xsi:schemaLocation="http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv2.xsd " xmlns="http://www.sat.gob.mx/cfd/2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <xsl:attribute name="version" >2.0</xsl:attribute>
      <xsl:if test="/BillingsDataSet/Billings/Serial != ''">
        <xsl:attribute name="serie">
          <xsl:value-of select="/BillingsDataSet/Billings/Serial"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Folio != ''">
        <xsl:attribute name="folio">
          <xsl:value-of select="/BillingsDataSet/Billings/Folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingDate != ''">
        <xsl:attribute name="fecha">
          <xsl:variable name="varFecha" select="/BillingsDataSet/Billings/BillingDate"/>
          <xsl:value-of select="substring($varFecha,1,19) "/>

        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Seal != ''">
        <xsl:attribute name="sello">
          <xsl:value-of select="/BillingsDataSet/Billings/Seal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/ApprovalNumber != ''">
        <xsl:attribute name="noAprobacion">
          <xsl:value-of select="/BillingsDataSet/Billings/ApprovalNumber"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/ApprovalYear != ''">
        <xsl:attribute name="anoAprobacion">
          <xsl:value-of select="/BillingsDataSet/Billings/ApprovalYear"/>
        </xsl:attribute>
      </xsl:if>

      <xsl:if test="/BillingsDataSet/Billings/PaymentForm != ''">
        <xsl:attribute name="formaDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentForm"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/CertificateNumber != ''">
        <xsl:attribute name="noCertificado">
          <xsl:value-of select="/BillingsDataSet/Billings/CertificateNumber"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Certificate != ''">
        <xsl:attribute name="certificado">
          <xsl:value-of select="/BillingsDataSet/Billings/Certificate"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentTerms != ''">
        <xsl:attribute name="condicionesDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentTerms"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/SubTotal != ''">
        <xsl:attribute name="subTotal">
          <xsl:value-of select="/BillingsDataSet/Billings/SubTotal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Discount != ''">
        <xsl:attribute name="descuento">
          <xsl:value-of select="/BillingsDataSet/Billings/Discount"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/DiscountDescription != ''">
        <xsl:attribute name="motivoDescuento">
          <xsl:value-of select="/BillingsDataSet/Billings/DiscountDescription"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Total != ''">
        <xsl:attribute name="total">
          <xsl:value-of select="/BillingsDataSet/Billings/Total"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentMethod != ''">
        <xsl:attribute name="metodoDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentMethod"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingType != ''">
        <xsl:attribute name="tipoDeComprobante">
          <xsl:value-of select="/BillingsDataSet/Billings/BillingType"/>
        </xsl:attribute>
      </xsl:if>
      <Emisor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerRFC != ''">
          <xsl:attribute name="rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerName != ''">
          <xsl:attribute name="nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerName"/>
          </xsl:attribute>
        </xsl:if>
        <DomicilioFiscal>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerAddress != ''">
            <xsl:attribute name="calle">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerAddress"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/ExternalNumber != ''">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/ExternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/InternalNumber != ''">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/InternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Colony != ''">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Colony"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Location != ''">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Location"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Reference != ''">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Reference"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Municipality != ''">
            <xsl:attribute name="municipio">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Municipality"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/State != ''">
            <xsl:attribute name="estado">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/State"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Country != ''">
            <xsl:attribute name="pais">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Country"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Zipcode != ''">
            <xsl:attribute name="codigoPostal">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Zipcode"/>
            </xsl:attribute>
          </xsl:if>
        </DomicilioFiscal>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/IssuedInDifferentPlace = 'true'">
          <ExpedidoEn>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Address != ''">
              <xsl:attribute name="calle">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Address"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/ExternalNumber != ''">
              <xsl:attribute name="noExterior">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/ExternalNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/InternalNumber != ''">
              <xsl:attribute name="noInterior">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/InternalNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Colony != ''">
              <xsl:attribute name="colonia">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Colony"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Location != ''">
              <xsl:attribute name="localidad">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Location"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Reference != ''">
              <xsl:attribute name="referencia">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Reference"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Municipality != ''">
              <xsl:attribute name="municipio">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Municipality"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/State != ''">
              <xsl:attribute name="estado">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/State"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Country != ''">
              <xsl:attribute name="pais">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Country"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Zipcode != ''">
              <xsl:attribute name="codigoPostal">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Zipcode"/>
              </xsl:attribute>
            </xsl:if>
          </ExpedidoEn>
        </xsl:if>

      </Emisor>
      <Receptor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC != ''">
          <xsl:attribute name="rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName != ''">
          <xsl:attribute name="nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName"/>
          </xsl:attribute>
        </xsl:if>
        <Domicilio>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorAddress != ''">
            <xsl:attribute name="calle">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorAddress"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ExternalNumber != ''">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ExternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/InternalNumber != ''">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/InternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Colony != ''">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Colony"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Location != ''">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Location"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Reference != ''">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Reference"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Municipality != ''">
            <xsl:attribute name="municipio">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Municipality"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/State != ''">
            <xsl:attribute name="estado">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/State"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Country != ''">
            <xsl:attribute name="pais">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Country"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Zipcode != ''">
            <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Zipcode != '-1'">
              <xsl:attribute name="codigoPostal">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Zipcode"/>
              </xsl:attribute>
            </xsl:if>
          </xsl:if>
        </Domicilio>
      </Receptor>
      <Conceptos>
        <xsl:for-each select="/BillingsDataSet/Billings/BillingsDetails">
          <Concepto>
            <xsl:if test="Count != ''">
              <xsl:attribute name="cantidad">
                <xsl:value-of select="Count"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Unit != ''">
              <xsl:attribute name="unidad">
                <xsl:value-of select="Unit"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="IdentificationNumber != ''">
              <xsl:attribute name="noIdentificacion">
                <xsl:value-of select="IdentificationNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Description != ''">
              <xsl:attribute name="descripcion">
                <xsl:value-of select="Description"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="UnitValue != ''">
              <xsl:attribute name="valorUnitario">
                <xsl:value-of select="UnitValue"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Amount != ''">
              <xsl:attribute name="importe">
                <xsl:value-of select="Amount"/>
              </xsl:attribute>
            </xsl:if>
          </Concepto>
        </xsl:for-each>
      </Conceptos>
      <Impuestos>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalTransfer != ''">
          <xsl:attribute name="totalImpuestosTrasladados">
            <xsl:value-of select="/BillingsDataSet/Billings/Taxes/TotalTransfer"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained != ''">
          <xsl:attribute name="totalImpuestosRetenidos">
            <xsl:value-of select="/BillingsDataSet/Billings/Taxes/TotalDetained"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/DetainedTaxes != ''">
          <Retenciones>
            <xsl:for-each select="/BillingsDataSet/Billings/Taxes/DetainedTaxes">
              <Retencion>
                <xsl:if test="Name != ''">
                  <xsl:attribute name="impuesto">
                    <xsl:value-of select="Name"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="Import != ''">
                  <xsl:attribute name="importe">
                    <xsl:value-of select="Import"/>
                  </xsl:attribute>
                </xsl:if>
              </Retencion>
            </xsl:for-each>
          </Retenciones>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TransferTaxes != ''">
          <Traslados>
            <xsl:for-each select="/BillingsDataSet/Billings/Taxes/TransferTaxes">
              <Traslado>
                <xsl:if test="Name != ''">
                  <xsl:attribute name="impuesto">
                    <xsl:value-of select="Name"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="TaxRatePercentage != ''">
                  <xsl:attribute name="tasa">
                    <xsl:value-of select="TaxRatePercentage"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="Import != ''">
                  <xsl:attribute name="importe">
                    <xsl:value-of select="Import"/>
                  </xsl:attribute>
                </xsl:if>
              </Traslado>
            </xsl:for-each>
          </Traslados>
        </xsl:if>

      </Impuestos>
    </Comprobante>
  </xsl:template>
</xsl:stylesheet>
