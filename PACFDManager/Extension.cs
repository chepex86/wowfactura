﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager
{
    public static class ElectronicBillingType_Extension
    {
        public static string GetTitle(this PACFD.Rules.ElectronicBillingType value)
        {
            if (value < 0)
                return value.ToDictionary()[PACFD.Rules.ElectronicBillingType.Indeterminate];

            return value.ToDictionary()[value];
        }

        public static Dictionary<PACFD.Rules.ElectronicBillingType, string> ToDictionary(this PACFD.Rules.ElectronicBillingType value)
        {
            Dictionary<PACFD.Rules.ElectronicBillingType, string> dick = new Dictionary<PACFD.Rules.ElectronicBillingType, string>();
            dick.Add(PACFD.Rules.ElectronicBillingType.CFD, "CFD 2.0");
            dick.Add(PACFD.Rules.ElectronicBillingType.CFDI, "CFDI 3.0");
            dick.Add(PACFD.Rules.ElectronicBillingType.CBB, "CBB 1.0");
            dick.Add(PACFD.Rules.ElectronicBillingType.CFD2_2, "CFD 2.2");
            dick.Add(PACFD.Rules.ElectronicBillingType.CFDI3_2, "CFDI 3.2");
            dick.Add(PACFD.Rules.ElectronicBillingType.CFDI3_3, "CFDI 3.3");
            dick.Add(PACFD.Rules.ElectronicBillingType.CFDI4_0, "CFDI 4.0");

            dick.Add(PACFD.Rules.ElectronicBillingType.Indeterminate, "Indeterminado");
            return dick;
        }

        public static PACFD.Rules.ElectronicBillingType ToElectronicBillingType(this int value)
        {
            switch (value)
            {
                case 0: return PACFD.Rules.ElectronicBillingType.CFD;
                case 1: return PACFD.Rules.ElectronicBillingType.CFDI;
                case 2: return PACFD.Rules.ElectronicBillingType.CBB;
                case 3: return PACFD.Rules.ElectronicBillingType.CFD2_2;
                case 4: return PACFD.Rules.ElectronicBillingType.CFDI3_2;
                case 5: return PACFD.Rules.ElectronicBillingType.CFDI3_3;
                case 6: return PACFD.Rules.ElectronicBillingType.CFDI4_0;

                default:
                case 99:
                    return PACFD.Rules.ElectronicBillingType.Indeterminate;
            }
        }
    }

    public static class Extension
    {
        /// <summary>
        /// Get a boolean value with the instance state of an object.
        /// </summary>
        /// <param name="sender">Object to be evaluate.</param>
        /// <returns>If null return true else false.</returns>
        public static bool IsNull(this object sender)
        {
            return (sender == null);
        }

        public static string ToLetterString(this PACFD.Common.CurrencyType value)
        {
            switch (value)
            {
                default:
                case PACFD.Common.CurrencyType.None: return string.Empty;
                case PACFD.Common.CurrencyType.MXN: return "MXN";
                case PACFD.Common.CurrencyType.USD: return "USD";
            }
        }
        public static PACFD.Common.CurrencyType ToCurrencyCode(this int value)
        {
            switch (value)
            {
                default: return PACFD.Common.CurrencyType.None;
                case 1: return PACFD.Common.CurrencyType.MXN;
                case 2: return PACFD.Common.CurrencyType.USD;
            }
        }

        public static string ToLetters(this decimal value, PACFD.Common.CurrencyType currency)
        {
            PACFD.Common.LetterConverter l = new PACFD.Common.LetterConverter();

            return l.NumberToLetter(value, currency);
        }

        public static bool ToBoolean(this string value)
        {
            bool b = false;

            if (!Boolean.TryParse(value, out b))
                return true;

            return b;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToFirstUpper(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;

            value = value.Remove(0, 1) + value[0].ToString().ToUpper();

            return value;
        }
        /// <summary>
        /// Get an integer value from a strng. If fail return 0
        /// </summary>
        /// <param name="value">String to be converted.</param>
        /// <returns>If fail return 0 else return the converted value.</returns>
        public static int ToInt32(this string value)
        {
            int i;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!int.TryParse(value, out i))
                return 0;

            return i;
        }

        public static float ToFloat(this string value)
        {
            float f;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!float.TryParse(value, out f))
                return 0;

            return f;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this string value)
        {
            decimal d;

            if (string.IsNullOrEmpty(value))
                return 0;

            if (!decimal.TryParse(value, out d))
                return 0;

            return d;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            foreach (char c in value)
                if (c < '0' && c > '9')
                    return false;

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static System.Drawing.Color HexStringToColor(this string hex)
        {
            hex = hex.Replace("#", string.Empty);

            if (hex.Length != 6)
                throw new Exception(hex +
                    " is not a valid 6-place hexadecimal color code.");

            string r, g, b;

            r = hex.Substring(0, 2);
            g = hex.Substring(2, 2);
            b = hex.Substring(4, 2);

            return System.Drawing.Color.FromArgb(HexStringToBase10Int(r),
                                                 HexStringToBase10Int(g),
                                                 HexStringToBase10Int(b));

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static int HexStringToBase10Int(string hex)
        {
            int base10value = 0;

            try { base10value = System.Convert.ToInt32(hex, 16); }
            catch { base10value = 0; }

            return base10value;
        }
        /// <summary>
        /// Resize a Bitmap.
        /// </summary>
        /// <param name="bitmap">Bitmap to resize</param>
        /// <param name="width">New width of the bitmap</param>
        /// <param name="height">New height of the bitmap</param>
        /// <returns>If success return a resize image else return the same bitmap</returns>
        public static System.Drawing.Bitmap ToSize(this System.Drawing.Bitmap bitmap, int width, int height)
        {
            System.Drawing.Graphics g;
            System.Drawing.Bitmap newbitmap;

            if (bitmap == null)
                return bitmap;

            newbitmap = new System.Drawing.Bitmap(width, height);
            g = System.Drawing.Graphics.FromImage(newbitmap);
            g.DrawImage(bitmap, 0, 0, width, height);
            g.Dispose();

            return newbitmap;
        }
        /// <summary>
        /// Resize an image.
        /// </summary>
        /// <param name="bitmap">Image to resize</param>
        /// <param name="width">New width of the image</param>
        /// <param name="height">New height of the image</param>
        /// <returns>If success return a resize image else return the same image</returns>
        public static System.Drawing.Image ToSize(this System.Drawing.Image image, int width, int height)
        {
            return ToSize(image, width, height);
        }
        /// <summary>
        /// Convert a System.Drawing.Bitmap to an array.
        /// </summary>
        /// <param name="bitmap">System.Drawing.Bitmap to be convert.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this System.Drawing.Bitmap bitmap)
        {
            byte[] b;
            System.IO.StreamReader r;
            System.IO.MemoryStream stream;

            if (bitmap.IsNull())
                return new byte[] { };

            stream = new System.IO.MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            r = new System.IO.StreamReader(stream);
            b = new byte[stream.Length];
            stream.Seek(0, System.IO.SeekOrigin.Begin); //go to begin
            stream.Read(b, 0, b.Length);

            stream.Close();
            stream.Dispose();
            stream = null;
            r.Close();
            r.Dispose();
            r = null;

            return b;
        }
        /// <summary>
        /// Get a System.Drawing.Bitmap instance from a stream.
        /// </summary>
        /// <param name="stream">Stream with the image to be loaded.</param>
        /// <returns>System.Drawing.Bitmap  value.</returns>
        public static System.Drawing.Bitmap ToBitmap(this System.IO.Stream stream)
        {
            System.Drawing.Bitmap b = null;

            stream.Seek(0, System.IO.SeekOrigin.Begin);

            try
            {
                b = new System.Drawing.Bitmap(stream);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return b;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billtable"></param>
        /// <returns></returns>
        public static PACFD.DataAccess.BillersDataSet.IssuedDataTable GenerateIssuedFromBiller(this PACFD.DataAccess.BillersDataSet.BillersDataTable billtable)
        {
            PACFD.DataAccess.BillersDataSet.IssuedDataTable table;
            PACFD.DataAccess.BillersDataSet.IssuedRow row;

            table = new PACFD.DataAccess.BillersDataSet.IssuedDataTable();

            foreach (PACFD.DataAccess.BillersDataSet.BillersRow billrow in billtable)
            {
                row = table.NewIssuedRow();

                //row.BeginEdit();
                row.Address = billrow.Address;
                row.BillerID = billrow.BillerID;
                row.Colony = billrow.Colony;
                row.Country = billrow.Country;
                row.ExternalNumber = billrow.ExternalNumber;
                row.InternalNumber = billrow.InternalNumber;
                row.Location = billrow.Location;
                row.Municipality = billrow.IsMunicipalityNull() ? String.Empty : billrow.Municipality;
                row.Reference = billrow.IsReferenceNull() ? string.Empty : billrow.Reference;
                row.State = billrow.State;
                row.Zipcode = billrow.Zipcode;
                //row.EndEdit();

                table.AddIssuedRow(row);

                //row.SetAdded();
            }

            return table;
        }

        public static string ToStringTax(this PACFD.Common.TaxesType tax)
        {
            string s;

            switch (tax)
            {
                case PACFD.Common.TaxesType.ISRDetained:
                    s = "ISR";
                    break;
                case PACFD.Common.TaxesType.IVADetained:
                case PACFD.Common.TaxesType.IVATransfer:
                default:
                    s = "IVA";
                    break;
            }

            return s;
        }

        public static PACFD.Common.TaxesType ToEnumTax(this string tax)
        {
            switch (tax.ToLower())
            {
                case "isr":
                    return PACFD.Common.TaxesType.ISRDetained;
                case "iva":
                default:
                    return PACFD.Common.TaxesType.IVATransfer;
            }
        }

        public static PACFD.Common.TaxesType ToEnumTax(this int tax)
        {
            switch (tax)
            {
                case 0:
                    return PACFD.Common.TaxesType.IVATransfer;
                case 1:
                    return PACFD.Common.TaxesType.IVADetained;
                case 2:
                    return PACFD.Common.TaxesType.ISRDetained;
                default:
                    return PACFD.Common.TaxesType.IVATransfer;
            }
        }
        /// <summary>
        /// Get a System.IO.Stream value with the byte[] array write inside.
        /// </summary>
        /// <param name="value">byte[] to be write in the stream.</param>
        /// <returns>System.IO.Stream value.</returns>
        public static System.IO.Stream ToStream(this byte[] value)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();

            if (value.Length < 1)
                return stream;

            stream.Write(value, 0, value.Length);
            stream.Seek(0, System.IO.SeekOrigin.Begin);

            return stream;
        }
        /// <summary>
        /// Get an integer value with the index item of a determinated value string.
        /// </summary>
        /// <param name="list">ListItemCollection to searh into.</param>
        /// <param name="value">Value to look for.</param>
        /// <returns>If item is found return the index value else return -1.</returns>
        public static int GetRowIndexByValue(this System.Web.UI.WebControls.ListItemCollection list, string value)
        {
            int index = -1;

            foreach (System.Web.UI.WebControls.ListItem item in list)
            {
                index++;

                if (item.Value == value)
                    return index;
            }

            return -1;
        }
    }
}