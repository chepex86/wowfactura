﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PACFD.Common;
using PACFD.Rules;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Web.UI.HtmlControls;

namespace PACFDManager
{
    public class BasePage : System.Web.UI.Page
    {
        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            #region Controlling of Functionality
            if (!IsPostBack && Security.Security.IsLogged)
            {
                Type currentType = this.GetType().BaseType;

                //Verificar los permisos
                #region Billers
                if (typeof(Billers.BillersList) == currentType)
                    CheckPermission(2);
                else if (typeof(Billers.BillersAdd) == currentType)
                    CheckPermission(2);
                else if (typeof(Billers.BillersDelete) == currentType)
                    CheckPermission(2);
                else if (typeof(Billers.BillersList) == currentType)
                    CheckPermission(2);
                #endregion
                #region DigitalCertificates
                else if (typeof(DigitalCertificates.DigitalCertificatesList) == currentType)
                    CheckPermission(3);
                else if (typeof(DigitalCertificates.DigitalCertificatesAdd) == currentType)
                    CheckPermission(3);
                #endregion
                #region Series
                else if (typeof(Series.SeriesAdd) == currentType)
                    CheckPermission(3);
                else if (typeof(Series.SeriesDetails) == currentType)
                    CheckPermission(3);
                else if (typeof(Series.SeriesList) == currentType)
                    CheckPermission(3);
                #endregion
                #region Reports Provider
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportDetails) == currentType)
                    CheckPermission(3);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportGenerated) == currentType)
                    CheckPermission(3);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportList) == currentType)
                    CheckPermission(3);
                else if (typeof(Reports.BillerMonthlyReport.DownloadReport) == currentType)
                    CheckPermission(3);
                #endregion

                //Veridicacion de funcionalidad ** que este seleccionado Cliente o Emisor para trabajar..

                #region Root
                if (typeof(PACFDManager.Default1) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Administrator
                else if (typeof(Administrator.BillerSmtpConfiguration) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Administrator.TaxTemplateList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Administrator.TaxTemplateAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Administrator.TaxTemplateModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Billers
                else if (typeof(Billers.BillersList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(Billers.BillersAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(Billers.BillersDelete) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(Billers.BillersList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(PlaceDispatch.PlaceDispatchList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(PlaceDispatch.PlaceDispatchAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.Group);
                else if (typeof(Billers.Branches.BranchesList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billers.Branches.BranchesModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billers.Branches.BranchesAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region DigitalCertificates
                else if (typeof(DigitalCertificates.DigitalCertificatesList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(DigitalCertificates.DigitalCertificatesAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Billings
                else if (typeof(Billings.BillingsAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.BillingsDelete) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.BillingsDetails) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.BillingsList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.BillingsModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.BillingImporting) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.XML.PrintTemplateAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.XML.PrintTemplateList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.XML.PrintTemplateModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.XML.DownloadPage) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Billings.Addendum.AddendumApply) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region BillingStatement
                else if (typeof(BillingStatement.BillingStatementList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(BillingStatement.BillingStatementAddPayments) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Series
                else if (typeof(Series.SeriesAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Series.SeriesDetails) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Series.SeriesList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Concepts
                else if (typeof(Concepts.ConceptsAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Concepts.ConceptsDelete) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Concepts.ConceptsList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Concepts.ConceptsModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Concepts.ConceptsImport) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Receptor
                else if (typeof(Receptors.ReceptorsList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Receptors.ReceptorsAdd) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Receptors.ReceptorsDelete) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Receptors.ReceptorsEditor) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Receptors.ReceptorsSearch) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Reports Billers
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportDetails) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportGenerated) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.DownloadReport) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Reports Provider
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportDetails) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportGenerated) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.BillerMonthlyReportList) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.BillerMonthlyReport.DownloadReport) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Reports
                else if (typeof(Reports.Report_001) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.Report_002) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.Report_003) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                else if (typeof(Reports.Report_004) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region Exportation
                else if (typeof(Exportation.Exportation) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
                #region PACConfiguration
                else if (typeof(PACFDManager.PacConfiguration.PacConfigurationModify) == currentType)
                    CheckGroupAndBiller(FunctionAccess.GroupCompany);
                #endregion
            }
            else if (!IsPostBack)
            {
                Type currentType = this.GetType().BaseType;
                if (typeof(PACFDManager.Security.Login) != currentType && typeof(PACFDManager.Security.LogOut) != currentType)
                {
                    Response.Redirect("~/Security/LogOut.aspx");
                    return;
                }
            }
            #endregion
            base.OnLoad(e);

        }

        protected override void OnPreInit(EventArgs e)
        {
            if (!HaveCurrentBrand)
            {
                string virtualXmlPath = string.Format("~/Brand.xml");
                string xmlPath = HttpContext.Current.Request.MapPath(virtualXmlPath);
                if (System.IO.File.Exists(xmlPath))
                {
                    DataSet ds = new DataSet();
                    ds.ReadXml(xmlPath);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        HaveCurrentBrand = true;

                        DataRow[] drs = ds.Tables["Brand"].Select(string.Format("ID = '{0}'", 1));
                        if (drs.Length > 0)
                        {
                            CurrentBrand = drs[0];
                        }
                        foreach (DataRow dr in ds.Tables["Brand"].Rows)
                        {
                            object ob = Request;
                            if (dr.Table.Columns.Contains("Directory") && dr["Directory"] != null)
                            {
                                if (dr["Directory"].ToString().Trim().Length > 0 && Request.QueryString != null && Request.QueryString.ToString().Length > 0 && Request.QueryString.ToString().Contains(dr["Directory"].ToString()))
                                {
                                    CurrentBrand = dr;
                                    break;
                                }
                            }
                        }
                    }
                }
            }


            if (Page != null && Page.Master != null && HaveCurrentBrand)
            {
                string pathCCS = "";
                string dataSupport = "";
                if (CurrentBrand != null)
                {
                    if (CurrentBrand["PathCCS"] != null)
                        pathCCS = CurrentBrand["PathCCS"].ToString();

                    if (CurrentBrand["DataSupport"] != null)
                        dataSupport = CurrentBrand["DataSupport"].ToString();
                }

                HtmlHead Header = Master.FindControl("Head1") as HtmlHead;

                if (Header != null && Header.FindControl("PACFDDefaultCss") != null)
                { Header.Controls.Remove(Header.FindControl("PACFDDefaultCss")); }

                System.Web.UI.HtmlControls.HtmlLink lnkCss = new System.Web.UI.HtmlControls.HtmlLink();
                lnkCss.Attributes["rel"] = "stylesheet";
                lnkCss.Attributes["type"] = "text/css";
                lnkCss.ID = "EventDefaultCss";
                lnkCss.Href = string.Format(@"~{0}/assets/layout.css", pathCCS);
                Header.Controls.AddAt(0, lnkCss);

                if (Master.FindControl("footer") != null)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl div = Master.FindControl("footer") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (div != null)
                    {
                        dataSupport = dataSupport.Replace("src=\"~", string.Format("src=\"{0}", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath)));
                        div.InnerHtml = dataSupport;
                    }
                }
            }
            base.OnPreInit(e);
        }

        private void CheckGroupAndBiller(FunctionAccess level)
        {
            switch (level)
            {
                case FunctionAccess.Group:
                    if (CurrentGroupID == -1)
                    {
                        Response.Redirect(string.Format("~/Select.aspx?op={0}&page={1}", Cryptography.EncryptUnivisitString("needCli"), this.Title));
                        return;
                    }
                    break;
                case FunctionAccess.GroupCompany:
                    if (CurrentGroupID == -1 || CurrentBillerID == -1)
                    {
                        if (CurrentGroupID == -1)
                        { Response.Redirect(string.Format("~/Select.aspx?op={0}&page={1}", Cryptography.EncryptUnivisitString("needCliBill"), this.Title)); }
                        else
                        { Response.Redirect(string.Format("~/Select.aspx?op={0}&page={1}", Cryptography.EncryptUnivisitString("needBill"), this.Title)); }

                        return;
                    }
                    break;
            }
        }

        private void CheckPermission(int requiredLevel)
        {
            if (RolLevel == -1 || RolLevel > requiredLevel)
            {
                Response.Redirect(string.Format("~/Security/AccessDenied.aspx?page={0}", this.Title));
                return;
            }
        }

        public static void SetCurrentGroupID(int groupID, string groupName)
        {
            HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupID")] = groupID;
            HttpContext.Current.Session[string.Format("PACFDManager-CurrentGroupName")] = groupName;
        }

        public static void SetCurrentBillerID(int billerID, string billerName, ElectronicBillingType BillingType)
        {
            HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")] = billerID;
            HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerName")] = billerName;
            HttpContext.Current.Session[string.Format("PACFDManager-ElectronicBillingType")] = BillingType;
        }

        public static void SetCurrentBranchID(int branchID, String branchName)
        {
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchID")] = branchID;
            HttpContext.Current.Session[String.Format("PACFDManager-CurrentBranchName")] = branchName;
        }
        #endregion

        #region Property
        /// <summary>
        /// Get the current electronic billing type.
        /// </summary>
        public virtual ElectronicBillingType CurrentElectronicBillingType
        {
            get { return Security.Security.CurrentElectronicBillingType; }
        }

        public string UserName
        {
            get { return Security.Security.UserName; }
        }
        public int UserID
        {
            get { return Security.Security.UserID; }
        }
        public bool IsAdministrator
        {
            get { return Security.Security.IsAdministrator; }
        }
        public bool IsSAT
        {
            get { return Security.Security.IsSAT; }
        }
        public bool IsAdvancedClient
        {
            get { return Security.Security.IsAdvancedClient; }
        }

        public bool IsBasicClient
        {
            get { return Security.Security.IsBasicClient; }
        }

        public int CurrentGroupID
        {
            get { return Security.Security.CurrentGroupID; }
        }

        public string CurrentGroupName
        {
            get { return Security.Security.CurrentGroupName; }
        }
        public int CurrentBillerID
        {
            get
            {
                try
                {
                    return (int)HttpContext.Current.Session[string.Format("PACFDManager-CurrentBillerID")];
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return -1;
            }
        }
        public int CurrentBranchID
        {
            get
            {
                return Security.Security.CurrentBranchID;
            }
        }
        public String CurrentBranchName
        {
            get
            {
                return Security.Security.CurrentBranchName;
            }
        }

        public bool HaveBranch
        {
            get
            {

                return Security.Security.HaveBranch;
            }
        }

        public bool HaveCurrentBrand
        {
            get
            {
                object o = HttpContext.Current.Session[string.Format("PACFDManager-HaveCurrentBrand")];
                return o.IsNull() || o.GetType() != typeof(bool) ? false : (bool)o;
            }
            set { HttpContext.Current.Session[string.Format("PACFDManager-HaveCurrentBrand")] = value; }
        }

        public DataRow CurrentBrand
        {
            get
            {
                try
                {
                    return (DataRow)HttpContext.Current.Session[string.Format("PACFDManager-CurrentBrand")];
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                }
                return null;
            }
            set
            {
                HttpContext.Current.Session[string.Format("PACFDManager-CurrentBrand")] = value;
            }
        }

        public string CurrentBillerName
        {
            get
            {
                return Security.Security.CurrentBillerName;
            }
        }

        public static int RolLevel
        {
            get
            {
                return Security.Security.RolLevel;
            }
        }

        public static bool CFDEnable { get { return Security.Security.CFDEnable; } }
        public static bool CBBEnable { get { return Security.Security.CBBEnable; } }
        public static bool CFDIEnable { get { return Security.Security.CFDIEnable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.2 is enabled.
        /// </summary>
        public static bool CFDI3_2Enable { get { return Security.Security.CFDI3_2Enable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 3.2 is enabled.
        /// </summary>
        public static bool CFDI3_3Enable { get { return Security.Security.CFDI3_3Enable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFDI 4.0 is enabled.
        /// </summary>
        public static bool CFDI4_0Enable { get { return Security.Security.CFDI4_0Enable; } }
        /// <summary>
        /// Get a boolean value indicating if the CFD 2.2 is enabled.
        /// </summary>
        public static bool CFD2_2Enable { get { return Security.Security.CFD2_2Enable; } }

        public Decimal CurrentExchangeRate { get { return Security.Security.CurrentExchangeRate; } }

        #endregion

        protected override void InitializeCulture()
        {
            if (Session["SystemCulture"] == null)
                Session["SystemCulture"] = "es-MX";
            if (Session["IdIdioma"] == null)
                Session["IdIdioma"] = 1;
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["SystemCulture"].ToString(), false);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["SystemCulture"].ToString(), false);
            base.InitializeCulture();
        }
    }

    enum FunctionAccess
    {
        None = 0,
        Group = 1,
        GroupCompany = 2,
    }
}