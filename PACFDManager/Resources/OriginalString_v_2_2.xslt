﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:cfd="http://www.sat.gob.mx/cfd/2" xmlns:ecc="http://www.sat.gob.mx/ecc" xmlns:psgecfd="http://www.sat.gob.mx/psgecfd" xmlns:donat="http://www.sat.gob.mx/donat" xmlns:divisas="http://www.sat.gob.mx/divisas" xmlns:detallista="http://www.sat.gob.mx/detallista" xmlns:ecb="http://www.sat.gob.mx/ecb" xmlns:implocal="http://www.sat.gob.mx/implocal" xmlns:terceros="http://www.sat.gob.mx/terceros" xmlns:iedu="http://www.sat.gob.mx/iedu" xmlns:ventavehiculos="http://www.sat.gob.mx/ventavehiculos" xmlns:pfic="http://www.sat.gob.mx/pfic" xmlns:tpe="http://www.sat.gob.mx/TuristaPasajeroExtranjero" xmlns:leyendasFisc="http://www.sat.gob.mx/leyendasFiscales" xmlns:psgcfdsp="http://www.sat.gob.mx/psgcfdsp">

  <xsl:output method="text" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template name="Requerido">
    <xsl:param name="valor"/>|<xsl:call-template name="ManejaEspacios">
      <xsl:with-param name="s" select="$valor"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template name="Opcional">
    <xsl:param name="valor"/>
    <xsl:if test="$valor">
      |<xsl:call-template name="ManejaEspacios">
        <xsl:with-param name="s" select="$valor"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <xsl:template name="ManejaEspacios">
    <xsl:param name="s"/>
    <xsl:value-of select="normalize-space(string($s))"/>
  </xsl:template>

  <xsl:template match="ecc:EstadoDeCuentaCombustible">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tipoOperacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numeroDeCuenta"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@subTotal"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@total"/>
    </xsl:call-template>

    <xsl:apply-templates select="./ecc:Conceptos"/>
  </xsl:template>


  <xsl:template match="ecc:Conceptos">

    <xsl:for-each select="./ecc:ConceptoEstadoDeCuentaCombustible">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="ecc:Traslados">

    <xsl:for-each select="./ecc:Traslado">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="ecc:ConceptoEstadoDeCuentaCombustible">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@identificador"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fecha"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@rfc"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@claveEstacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@cantidad"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@nombreCombustible"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@folioOperacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@valorUnitario"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
    <xsl:for-each select="./ecc:Traslados">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="ecc:Traslado">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@impuesto"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tasa"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="psgecfd:PrestadoresDeServiciosDeCFD">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@nombre"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@rfc"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@noCertificado"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fechaAutorizacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@noAutorizacion"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="donat:Donatarias">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@noAutorizacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fechaAutorizacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@leyenda"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="divisas:Divisas">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tipoOperacion"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="ecb:EstadoDeCuentaBancario">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numeroCuenta"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@nombreCliente"/>
    </xsl:call-template>
    <xsl:for-each select="ecb:Movimientos/ecb:MovimientoECBFiscal">
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@fecha"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@RFCenajenante"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@Importe"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="detallista:detallista">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@documentStructureVersion"/>
    </xsl:call-template>
    <xsl:for-each select="detallista:orderIdentification/detallista:referenceIdentification">
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="."/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="detallista:orderIdentification/detallista:ReferenceDate"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="detallista:buyer/detallista:gln"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="detallista:seller/detallista:gln"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="detallista:seller/detallista:alternatePartyIdentification"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="detallista:totalAmount/detallista:Amount"/>
    </xsl:call-template>
    <xsl:for-each select="detallista:TotalAllowanceCharge/detallista:specialServicesType">
      <xsl:call-template name="Opcional">
        <xsl:with-param name="valor" select="."/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:for-each select="detallista:TotalAllowanceCharge/detallista:Amount">
      <xsl:call-template name="Opcional">
        <xsl:with-param name="valor" select="."/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  <xsl:template match="implocal:ImpuestosLocales">

    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@TotaldeRetenciones"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@TotaldeTraslados"/>
    </xsl:call-template>
    <xsl:for-each select="implocal:RetencionesLocales">
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@ImpLocRetenido"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@TasadeRetencion"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@Importe"/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:for-each select="implocal:TrasladosLocales">
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@ImpLocTrasladado"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@TasadeTraslado"/>
      </xsl:call-template>
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@Importe"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  <xsl:template match="terceros:PorCuentadeTerceros">
    <!--
Iniciamos el tratamiento de los atributos del complemento concepto Por cuenta de Terceros
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@rfc"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@nombre"/>
    </xsl:call-template>
    <!--
Iniciamos el tratamiento de los atributos de la información fiscal del complemento de terceros
-->
    <xsl:apply-templates select=".//terceros:InformacionFiscalTercero"/>
    <!--
Manejo de los atributos de la información aduanera del complemento de terceros
-->
    <xsl:for-each select=".//terceros:InformacionAduanera">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <!--
Manejo de los atributos de la cuenta predial del complento de terceros
-->
    <xsl:if test="./terceros:CuentaPredial">
      <xsl:apply-templates select="./terceros:CuentaPredial"/>
    </xsl:if>
    <!-- Manejador de nodos tipo Impuestos -->
    <xsl:for-each select=".//terceros:Retenciones/terceros:Retencion">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <xsl:for-each select=".//terceros:Traslados/terceros:Traslado">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!-- Manejador de nodos tipo Retencion -->
  <xsl:template match="terceros:Retencion">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@impuesto"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Traslado -->
  <xsl:template match="terceros:Traslado">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@impuesto"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tasa"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Información Aduanera -->
  <xsl:template match="terceros:InformacionAduanera">
    <!-- Manejo de los atributos de la información aduanera -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numero"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fecha"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@aduana"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Información CuentaPredial -->
  <xsl:template match="terceros:CuentaPredial">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numero"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Domicilio fiscal -->
  <xsl:template match="terceros:InformacionFiscalTercero">
    <!--
Iniciamos el tratamiento de los atributos del Domicilio Fiscal
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@calle"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@noExterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@noInterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@colonia"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@localidad"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@referencia"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@municipio"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@estado"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@pais"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@codigoPostal"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="iedu:instEducativas">
    <!--
Iniciamos el tratamiento de los atributos de instEducativas
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@nombreAlumno"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@CURP"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@nivelEducativo"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@autRVOE"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@rfcPago"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="ventavehiculos:VentaVehiculos">
    <!--
Iniciamos el tratamiento de los atributos del complemento concepto VentaVehiculos
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@ClaveVehicular"/>
    </xsl:call-template>
    <!--
Manejo de los atributos de la información aduanera del complemento de terceros
-->
    <xsl:for-each select=".//ventavehiculos:InformacionAduanera">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!-- Manejador de nodos tipo Información Aduanera -->
  <xsl:template match="ventavehiculos:InformacionAduanera">
    <!-- Manejo de los atributos de la información aduanera -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numero"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fecha"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@aduana"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="pfic:PFintegranteCoordinado">
    <!--
Iniciamos el tratamiento de los atributos de pfic:PFintegranteCoordinado
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@ClaveVehicular"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@Placa"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@RFCPF"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="tpe:TuristaPasajeroExtranjero">
    <!--
Iniciamos el tratamiento de los atributos de tpe:TuristaPasajeroExtranjero
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fechadeTransito"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tipoTransito"/>
    </xsl:call-template>
    <xsl:apply-templates select="./tpe:datosTransito"/>
  </xsl:template>
  <!-- Manejador de nodos tipo datosTransito -->
  <xsl:template match="tpe:datosTransito">
    <!--
Iniciamos el tratamiento de los atributos de los datos de Transito
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@Via"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@TipoId"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@NumeroId"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@Nacionalidad"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@EmpresaTransporte"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@IdTransporte"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="leyendasFisc:LeyendasFiscales">
    <!--
Iniciamos el tratamiento de los atributos del complemento LeyendasFiscales
-->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <!-- Manejo de los atributos de las leyendas Fiscales -->
    <xsl:for-each select="./leyendasFisc:Leyenda">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!--
Manejador de nodos tipo Información de las leyendas
-->
  <xsl:template match="leyendasFisc:Leyenda">
    <!-- Manejo de los atributos de la leyenda -->
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@disposicionFiscal"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@norma"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@textoLeyenda"/>
    </xsl:call-template>
  </xsl:template>





  <!-- CCCCCCCCCCCCCCCCCCCCCCCOOOOOOOOOOOOOOOOOOOOOOOOOOOMMMMMMMMMMMMMMMMMMMMMMMMMPPPPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOBBBBBBBBBBANNNNNNNNNNNNNNNNNNNNNNNTTTTTTTTTTTTTTTTTEEEeee -->

  <!-- Aquí iniciamos el procesamiento de la cadena original con su | inicial y el terminador || -->
  <xsl:template match="/">
    |<xsl:apply-templates select="/cfd:Comprobante"/>||
  </xsl:template>
  <!--  Aquí iniciamos el procesamiento de los datos incluidos en el comprobante -->
  <xsl:template match="cfd:Comprobante">
    <!-- Iniciamos el tratamiento de los atributos de comprobante -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@version"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@serie"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@folio"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fecha"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@noAprobacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@anoAprobacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tipoDeComprobante"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@formaDePago"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@condicionesDePago"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@subTotal"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@descuento"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@total"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@metodoDePago"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@LugarExpedicion"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@NumCtaPago"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@TipoCambio"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@Moneda"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@FolioFiscalOrig"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@SerieFolioFiscalOrig"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@FechaFolioFiscalOrig"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@MontoFolioFiscalOrig"/>
    </xsl:call-template>
    <!--
			Llamadas para procesar al los sub nodos del comprobante
		-->
    <xsl:apply-templates select="./cfd:Emisor"/>
    <xsl:apply-templates select="./cfd:Receptor"/>
    <xsl:apply-templates select="./cfd:Conceptos"/>
    <xsl:apply-templates select="./cfd:Impuestos"/>
    <xsl:apply-templates select="./cfd:Complemento"/>
  </xsl:template>
  <!-- Manejador de nodos tipo Emisor -->
  <xsl:template match="cfd:Emisor">
    <!-- Iniciamos el tratamiento de los atributos del Emisor -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@rfc"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@nombre"/>
    </xsl:call-template>
    <!--
			Llamadas para procesar al los sub nodos del comprobante
		-->
    <xsl:apply-templates select="./cfd:DomicilioFiscal"/>
    <xsl:if test="./cfd:ExpedidoEn">
      <xsl:call-template name="Domicilio">
        <xsl:with-param name="Nodo" select="./cfd:ExpedidoEn"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:for-each select="./cfd:RegimenFiscal">
      <xsl:call-template name="Requerido">
        <xsl:with-param name="valor" select="./@Regimen"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  <!-- Manejador de nodos tipo Receptor -->
  <xsl:template match="cfd:Receptor">
    <!-- Iniciamos el tratamiento de los atributos del Receptor -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@rfc"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@nombre"/>
    </xsl:call-template>
    <!--
			Llamadas para procesar al los sub nodos del Receptor
		-->
    <xsl:call-template name="Domicilio">
      <xsl:with-param name="Nodo" select="./cfd:Domicilio"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Conceptos -->
  <xsl:template match="cfd:Conceptos">
    <!-- Llamada para procesar los distintos nodos tipo Concepto -->
    <xsl:for-each select="./cfd:Concepto">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!-- Manejador de nodos tipo Impuestos -->
  <xsl:template match="cfd:Impuestos">
    <xsl:for-each select="./cfd:Retenciones/cfd:Retencion">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@totalImpuestosRetenidos"/>
    </xsl:call-template>
    <xsl:for-each select="./cfd:Traslados/cfd:Traslado">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@totalImpuestosTrasladados"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Retencion -->
  <xsl:template match="cfd:Retencion">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@impuesto"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Traslado -->
  <xsl:template match="cfd:Traslado">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@impuesto"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@tasa"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Complemento -->
  <xsl:template match="cfd:Complemento">
    <xsl:for-each select="./*">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!--
		Manejador de nodos tipo Concepto
	-->
  <xsl:template match="cfd:Concepto">
    <!-- Iniciamos el tratamiento de los atributos del Concepto -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@cantidad"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@unidad"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@noIdentificacion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@descripcion"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@valorUnitario"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@importe"/>
    </xsl:call-template>
    <!--
			Manejo de los distintos sub nodos de información aduanera de forma indistinta 
			a su grado de dependencia
		-->
    <xsl:for-each select=".//cfd:InformacionAduanera">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <!-- Llamada al manejador de nodos de Cuenta Predial en caso de existir -->
    <xsl:if test="./cfd:CuentaPredial">
      <xsl:apply-templates select="./cfd:CuentaPredial"/>
    </xsl:if>
    <!-- Llamada al manejador de nodos de ComplementoConcepto en caso de existir -->
    <xsl:if test="./cfd:ComplementoConcepto">
      <xsl:apply-templates select="./cfd:ComplementoConcepto"/>
    </xsl:if>
  </xsl:template>
  <!-- Manejador de nodos tipo Información Aduanera -->
  <xsl:template match="cfd:InformacionAduanera">
    <!-- Manejo de los atributos de la información aduanera -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numero"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@fecha"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@aduana"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Información CuentaPredial -->
  <xsl:template match="cfd:CuentaPredial">
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@numero"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo ComplementoConcepto -->
  <xsl:template match="cfd:ComplementoConcepto">
    <xsl:for-each select="./*">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
  <!-- Manejador de nodos tipo domicilio fiscal -->
  <xsl:template match="cfd:DomicilioFiscal">
    <!-- Iniciamos el tratamiento de los atributos del Domicilio Fiscal -->
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@calle"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@noExterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@noInterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@colonia"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@localidad"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="./@referencia"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@municipio"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@estado"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@pais"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="./@codigoPostal"/>
    </xsl:call-template>
  </xsl:template>
  <!-- Manejador de nodos tipo Domicilio -->
  <xsl:template name="Domicilio">
    <xsl:param name="Nodo"/>
    <!-- Iniciamos el tratamiento de los atributos del Domicilio  -->
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@calle"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@noExterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@noInterior"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@colonia"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@localidad"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@referencia"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@municipio"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@estado"/>
    </xsl:call-template>
    <xsl:call-template name="Requerido">
      <xsl:with-param name="valor" select="$Nodo/@pais"/>
    </xsl:call-template>
    <xsl:call-template name="Opcional">
      <xsl:with-param name="valor" select="$Nodo/@codigoPostal"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
