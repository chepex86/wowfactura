﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" encoding="utf-8" />
  <xsl:template match="/">
    <cfdi:Comprobante   xmlns:cfdi="http://www.sat.gob.mx/cfd/3"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                      xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd">
      
      <xsl:attribute name="version" >3.2</xsl:attribute>
      <xsl:if test="/BillingsDataSet/Billings/Serial != ''">
        <xsl:attribute name="serie">
          <xsl:value-of select="/BillingsDataSet/Billings/Serial"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Folio != ''">
        <xsl:attribute name="folio">
          <xsl:value-of select="/BillingsDataSet/Billings/Folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingDate != ''">
        <xsl:attribute name="fecha">
          <xsl:variable name="varFecha" select="/BillingsDataSet/Billings/BillingDate"/>
          <xsl:value-of select="substring($varFecha,1,19) "/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Seal != ''">
        <xsl:attribute name="sello">
          <xsl:value-of select="/BillingsDataSet/Billings/Seal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentForm != ''">
        <xsl:attribute name="formaDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentForm"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/CertificateNumber != ''">
        <xsl:attribute name="noCertificado">
          <xsl:value-of select="/BillingsDataSet/Billings/CertificateNumber"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Certificate != ''">
        <xsl:attribute name="certificado">
          <xsl:value-of select="/BillingsDataSet/Billings/Certificate"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentTerms != ''">
        <xsl:attribute name="condicionesDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentTerms"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/SubTotal != ''">
        <xsl:attribute name="subTotal">
          <xsl:value-of select="/BillingsDataSet/Billings/SubTotal"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Discount &gt; 0">
        <xsl:attribute name="descuento">
          <xsl:value-of select="/BillingsDataSet/Billings/Discount"/>
        </xsl:attribute>
        <xsl:attribute name="motivoDescuento">Descuento</xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/DiscountDescription != ''">
        <xsl:attribute name="motivoDescuento">
          <xsl:value-of select="/BillingsDataSet/Billings/DiscountDescription"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/Total != ''">
        <xsl:attribute name="total">
          <xsl:value-of select="/BillingsDataSet/Billings/Total"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PaymentMethod != ''">
        <xsl:attribute name="metodoDePago">
          <xsl:value-of select="/BillingsDataSet/Billings/PaymentMethod"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/BillingType != ''">
        <xsl:attribute name="tipoDeComprobante">
          <xsl:value-of select="/BillingsDataSet/Billings/BillingType"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/PlaceDispatch != ''">
        <xsl:attribute name="LugarExpedicion">
          <xsl:value-of select="/BillingsDataSet/Billings/PlaceDispatch"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/CurrencyCode != ''">
        <xsl:attribute name="Moneda">
          <xsl:value-of select="/BillingsDataSet/Billings/CurrencyCode"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/ExchangeRateMXN != ''">
        <xsl:attribute name="TipoCambio">
          <xsl:value-of select="/BillingsDataSet/Billings/ExchangeRateMXN"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/BillingsDataSet/Billings/AccountNumberPayment != ''">
        <xsl:attribute name="NumCtaPago">
          <xsl:value-of select="/BillingsDataSet/Billings/AccountNumberPayment"/>
        </xsl:attribute>
      </xsl:if>
      <cfdi:Emisor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerRFC != ''">
          <xsl:attribute name="rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/BillerName != ''">
          <xsl:attribute name="nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerName"/>
          </xsl:attribute>
        </xsl:if>
        <cfdi:DomicilioFiscal>
          <xsl:attribute name="calle">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/BillerAddress"/>
          </xsl:attribute>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/ExternalNumber != ''">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/ExternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/InternalNumber != ''">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/InternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Colony != ''">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Colony"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Location != ''">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Location"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Reference != ''">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Reference"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Municipality != ''">
            <xsl:attribute name="municipio">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Municipality"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/State != ''">
            <xsl:attribute name="estado">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/State"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Country != ''">
            <xsl:attribute name="pais">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Country"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsBillers/Zipcode != ''">
            <xsl:attribute name="codigoPostal">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsBillers/Zipcode"/>
            </xsl:attribute>
          </xsl:if>
        </cfdi:DomicilioFiscal>
        <xsl:if test="/BillingsDataSet/Billings/BillingsIssued != ''">
          <cfdi:ExpedidoEn>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Address != ''">
              <xsl:attribute name="calle">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Address"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/ExternalNumber != ''">
              <xsl:attribute name="noExterior">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/ExternalNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/InternalNumber != ''">
              <xsl:attribute name="noInterior">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/InternalNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Colony != ''">
              <xsl:attribute name="colonia">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Colony"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Location != ''">
              <xsl:attribute name="localidad">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Location"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Reference != ''">
              <xsl:attribute name="referencia">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Reference"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Municipality != ''">
              <xsl:attribute name="municipio">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Municipality"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/State != ''">
              <xsl:attribute name="estado">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/State"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Country != ''">
              <xsl:attribute name="pais">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Country"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/BillingsIssued/Zipcode != ''">
              <xsl:attribute name="codigoPostal">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsIssued/Zipcode"/>
              </xsl:attribute>
            </xsl:if>
          </cfdi:ExpedidoEn>
        </xsl:if>
        <xsl:for-each select="/BillingsDataSet/Billings/BillingFiscalRegime">
          <cfdi:RegimenFiscal>
            <xsl:if test="Regime != ''">
              <xsl:attribute name="Regimen">
                <xsl:value-of select="Regime"/>
              </xsl:attribute>
            </xsl:if>
          </cfdi:RegimenFiscal>
        </xsl:for-each>
      </cfdi:Emisor>
      <cfdi:Receptor>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC != ''">
          <xsl:attribute name="rfc">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorRFC"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName != ''">
          <xsl:attribute name="nombre">
            <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorName"/>
          </xsl:attribute>
        </xsl:if>
        <cfdi:Domicilio>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ReceptorAddress != ''">
            <xsl:attribute name="calle">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ReceptorAddress"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/ExternalNumber != ''">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/ExternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/InternalNumber != ''">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/InternalNumber"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Colony != ''">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Colony"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Location != ''">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Location"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Reference != ''">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Reference"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Municipality != ''">
            <xsl:attribute name="municipio">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Municipality"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/State != ''">
            <xsl:attribute name="estado">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/State"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Country != ''">
            <xsl:attribute name="pais">
              <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Country"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Zipcode != ''">
            <xsl:if test="/BillingsDataSet/Billings/BillingsReceptors/Zipcode != '-1'">
              <xsl:attribute name="codigoPostal">
                <xsl:value-of select="/BillingsDataSet/Billings/BillingsReceptors/Zipcode"/>
              </xsl:attribute>
            </xsl:if>
          </xsl:if>
        </cfdi:Domicilio>
      </cfdi:Receptor>
      <cfdi:Conceptos>
        <xsl:for-each select="/BillingsDataSet/Billings/BillingsDetails">
          <cfdi:Concepto>
            <xsl:if test="CountString != ''">
              <xsl:attribute name="cantidad">
                <xsl:value-of select="CountString"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Unit != ''">
              <xsl:attribute name="unidad">
                <xsl:value-of select="Unit"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="IdentificationNumber != ''">
              <xsl:attribute name="noIdentificacion">
                <xsl:value-of select="IdentificationNumber"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Description != ''">
              <xsl:attribute name="descripcion">
                <xsl:value-of select="Description"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="UnitValue != ''">
              <xsl:attribute name="valorUnitario">
                <xsl:value-of select="UnitValue"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="Amount != ''">
              <xsl:attribute name="importe">
                <xsl:value-of select="Amount"/>
              </xsl:attribute>
            </xsl:if>

          </cfdi:Concepto>
        </xsl:for-each>
      </cfdi:Conceptos>
      <cfdi:Impuestos>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalTransfer &gt; 0">
          <xsl:attribute name="totalImpuestosTrasladados">
            <xsl:value-of select="/BillingsDataSet/Billings/Taxes/TotalTransfer"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained &gt; 0">
          <xsl:attribute name="totalImpuestosRetenidos">
            <xsl:value-of select="/BillingsDataSet/Billings/Taxes/TotalDetained"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalDetained &gt; 0">
          <cfdi:Retenciones>
            <xsl:for-each select="/BillingsDataSet/Billings/Taxes/DetainedTaxes">
              <cfdi:Retencion>
                <xsl:if test="Name != ''">
                  <xsl:attribute name="impuesto">
                    <xsl:value-of select="Name"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="Import != ''">
                  <xsl:attribute name="importe">
                    <xsl:value-of select="Import"/>
                  </xsl:attribute>
                </xsl:if>
              </cfdi:Retencion>
            </xsl:for-each>
          </cfdi:Retenciones>
        </xsl:if>
        <xsl:if test="/BillingsDataSet/Billings/Taxes/TotalTransfer &gt; 0">
          <cfdi:Traslados>
            <xsl:for-each select="/BillingsDataSet/Billings/Taxes/TransferTaxes">
              <cfdi:Traslado>
                <xsl:if test="Name != ''">
                  <xsl:attribute name="impuesto">
                    <xsl:value-of select="Name"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="TaxRatePercentage != ''">
                  <xsl:attribute name="tasa">
                    <xsl:value-of select="TaxRatePercentage"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="Import != ''">
                  <xsl:attribute name="importe">
                    <xsl:value-of select="Import"/>
                  </xsl:attribute>
                </xsl:if>
              </cfdi:Traslado>
            </xsl:for-each>
          </cfdi:Traslados>
        </xsl:if>
      </cfdi:Impuestos>
      <xsl:if test="/BillingsDataSet/Billings/UUID != ''">
        <cfdi:Complemento>
          <tfd:TimbreFiscalDigital xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd"  xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" >
            <xsl:if test="/BillingsDataSet/Billings/VersionSat != ''">
              <xsl:attribute name="version">
                <xsl:value-of select="/BillingsDataSet/Billings/VersionSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/UUID != ''">
              <xsl:attribute name="UUID">
                <xsl:value-of select="/BillingsDataSet/Billings/UUID"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/DateStamped != ''">
              <xsl:attribute name="FechaTimbrado">
                <xsl:variable name="varSalida" select="/BillingsDataSet/Billings/DateStamped"/>
                <xsl:value-of select="substring($varSalida,1,19) "/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/Seal != ''">
              <xsl:attribute name="selloCFD">
                <xsl:value-of select="/BillingsDataSet/Billings/Seal"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/CertificateNumberSat != ''">
              <xsl:attribute name="noCertificadoSAT">
                <xsl:value-of select="/BillingsDataSet/Billings/CertificateNumberSat"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/BillingsDataSet/Billings/SealSat != ''">
              <xsl:attribute name="selloSAT">
                <xsl:value-of select="/BillingsDataSet/Billings/SealSat"/>
              </xsl:attribute>
            </xsl:if>
          </tfd:TimbreFiscalDigital>
        </cfdi:Complemento>
      </xsl:if>
    </cfdi:Comprobante>
  </xsl:template>
</xsl:stylesheet>
