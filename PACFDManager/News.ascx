﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="PACFDManager.News" %>
<%@ Register Src="PeriodChooser.ascx" TagName="PeriodChooser" TagPrefix="uc1" %>
<h1>
    NOTICIAS
</h1>
<div class="News" style="text-align: justify; margin-top: 50px;">
    <asp:Literal ID="ltrNews" runat="server"></asp:Literal>
</div>
