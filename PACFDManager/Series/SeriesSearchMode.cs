﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Series
{
    public enum SeriesSearchMode
    {
        Year = 0,
        Serial = 1,
        Active = 2,
    }
}
