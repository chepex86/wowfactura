﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeriesEditor.ascx.cs"
    Inherits="PACFDManager.Series.SeriesEditor" %>
<%@ Register Src="SeriesSearch.ascx" TagName="SeriesSearch" TagPrefix="uc2" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Panel ID="pnlFolioOptions" runat="server" Visible="False" meta:resourcekey="pnlFolioOptionsResource1">
    <div id="container" class="wframe80">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleOptions" runat="server" Text="Titulo"></asp:Label>
                    </h2>
                </div>
                <ul>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblNoSeries" runat="server" Text="No. Serie:" meta:resourcekey="lblNoSeriesResource1"></asp:Label></label>
                        <span class="vtip" title="Introduce un valor alfanumérico para seriar el bloque de folios. &lt;br /&gt;ej: <b>AAA</b> ó <b>AB1</b> (Máximo 10 caracteres)">
                            <asp:TextBox ID="txtNoSeries" runat="server" Width="75px" MaxLength="10"></asp:TextBox></span>
                        <div class="validator-msg">
                            <asp:RegularExpressionValidator ID="revNoSeries" runat="server" ControlToValidate="txtNoSeries"
                                Display="Dynamic" ErrorMessage="Solo letras o un dejar en blanco" ValidationExpression="([\s]?[A-Z]*[a-z]*)"
                                ValidationGroup="Validators"></asp:RegularExpressionValidator>
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblApprovalYear" runat="server" Text="Año de Aprobación:" meta:resourcekey="lblApprovalYearResource1"></asp:Label></label>
                        <span class="vtip" title="Selecciona el año en el que fueron aprobados los folios.">
                            <asp:DropDownList ID="ddlApprovalYear" runat="server" meta:resourcekey="ddlApprovalYearResource1">
                            </asp:DropDownList>
                        </span></li>
                    <li class="left">
                        <asp:RequiredFieldValidator ID="rfvApprovalNo" runat="server" ControlToValidate="txtApprovalNo"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvApprovalNoResource1"
                            ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblApprovalNo" runat="server" Text="No. Aprobación:" meta:resourcekey="lblApprovalNoResource1"></asp:Label></label>
                        <span class="vtip" title="Introduce el número de aprobación.<br />ej: <b>1000700200</b>">
                            <asp:TextBox ID="txtApprovalNo" runat="server" Width="75px" meta:resourcekey="txtApprovalNoResource1"
                                MaxLength="10"></asp:TextBox></span>
                        <asp:RegularExpressionValidator ID="revApprovalNo" runat="server" ControlToValidate="txtApprovalNo"
                            ErrorMessage="Solo Números" ValidationExpression="\d*" ValidationGroup="Validators"
                            meta:resourcekey="revApprovalNoResource1" Display="Dynamic"></asp:RegularExpressionValidator>
                    </li>
                    <li class="left">
                        <asp:RequiredFieldValidator ID="rfvStart" runat="server" ControlToValidate="txtStart"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvStartResource1" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblStart" runat="server" Text="Inicio:" meta:resourcekey="lblStartResource1"></asp:Label></label>
                        <span class="vtip" title="Introduce el índice con el que se enumeraran los folios autorizados.<br />ej: <b>1</b><br /><br />En caso de que la serie ya exista, debe introducir<br />el índice del último folio que termino la serie anterior. <br /><br />ej: (Folio anterior finalizo en <b>500</b>) entonces introducir <b>501</b>">
                            <asp:TextBox ID="txtStart" runat="server" Width="55px" meta:resourcekey="txtStartResource1"
                                MaxLength="7"></asp:TextBox></span></li>
                    <li class="left">
                        <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ControlToValidate="txtEnd"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvEndResource1" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblEnd" runat="server" Text="Fin:" meta:resourcekey="lblEndResource1"></asp:Label></label>
                        <span class="vtip" title="Introduce el índice en el cual terminara el bloque de folios actual.<br />ej: Si se van a autorizar <b>10</b> folios, el índice inicial seria <b>1</b> y el final <b>10</b>">
                            <asp:TextBox ID="txtEnd" runat="server" Width="55px" meta:resourcekey="txtEndResource1"
                                MaxLength="7"></asp:TextBox></span>
                        <asp:CompareValidator ID="cvEnd" runat="server" ControlToCompare="txtStart" ControlToValidate="txtEnd"
                            ErrorMessage="El valor tiene que ser mayor al de Inicio" Operator="GreaterThan"
                            ValidationGroup="Validators" meta:resourcekey="cvEndResource1" Display="Dynamic"
                            Type="Integer"></asp:CompareValidator>
                        <asp:Label ID="lblStartError" runat="server" ForeColor="Red" meta:resourcekey="lblStartErrorResource1"
                            Visible="False"></asp:Label>
                    </li>
                    <li class="buttons clear">
                        <div>
                            <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click"
                                ValidationGroup="Validators" meta:resourcekey="btnAcceptResource1" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="Button" meta:resourcekey="btnCancelResource1"
                                OnClick="btnCancel_Click" Text="Cancelar" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlFolioList" runat="server" Visible="False" meta:resourcekey="pnlFolioListResource1">
    <uc2:SeriesSearch ID="ucSeriesSearch" runat="server" />
    <br />
    <br />
    <div class="txt_left">
        <div class="info">
            <h2>
                <asp:Label ID="lblTitleList" runat="server" Text="Listado de Folios" />
            </h2>
        </div>
        <asp:Button ID="btnAddFolioTop" runat="server" CssClass="Button" meta:resourcekey="btnAddFolioTopResource1"
            PostBackUrl="~/Series/SeriesAdd.aspx" Text="Agregar Folio" />
        <span>
            <asp:GridView ID="gvFoliosList" runat="server" CellPadding="4" ForeColor="#333333"
                GridLines="None" AutoGenerateColumns="False" OnRowDataBound="gvFoliosList_RowDataBound"
                HorizontalAlign="Center" meta:resourcekey="gvFoliosListResource1">
                <RowStyle BackColor="#EFF3FB" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SerialID" HeaderText="ID" />
                    <asp:BoundField DataField="Serial" HeaderText="Serie" meta:resourcekey="BoundFieldResource1" />
                    <asp:BoundField DataField="AprovationYear" HeaderText="Año Aprobación" meta:resourcekey="BoundFieldResource3" />
                    <asp:BoundField DataField="AprovationNumber" HeaderText="Núm.Aprobación" meta:resourcekey="BoundFieldResource4" />
                    <asp:BoundField DataField="Start" HeaderText="Inicio" meta:resourcekey="BoundFieldResource5" />
                    <asp:BoundField DataField="End" HeaderText="Fin" meta:resourcekey="BoundFieldResource6" />
                    <asp:BoundField DataField="Active" HeaderText="Estado" meta:resourcekey="BoundFieldResource2" />
                    <asp:TemplateField HeaderText="Detalles" meta:resourcekey="TemplateFieldResource1">
                        <HeaderTemplate>
                            <uc3:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Detalle' /> para ver<br />los el detalle de los folios seleccionados." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Ver detalles de la serie <b><%#Eval("Serial")%></b><br />del folio <b><%#Eval("Start")%></b> al <b><%#Eval("End")%></b>.'>
                                <asp:ImageButton ID="lbtnDetail" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SerialID") %>'
                                    ImageUrl="~/Includes/Images/png/kfind.png" OnClick="lbtnDetail_Click" />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <uc3:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para eliminar<br />el bloque de folios seleccionados.<br /><br />Solo es posible eliminar los bloques de<br />folios que nunca han sido utilizados.<br />Además se debe encontrar en<br />estado <b>No Activo</b>." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Eliminar el bloque de folios<br /><b><%#Eval("Start")%></b> al <b><%#Eval("End")%></b> de la serie <b><%#Eval("Serial")%></b>.'  >
                                <asp:ImageButton ID="ibDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("SerialID")%>'
                                    ImageUrl="~/Includes/Images/png/deleteIcon.png" OnClick="ibDelte_OnClick" /></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Des/Activar" meta:resourcekey="TemplateFieldResource2">
                        <HeaderTemplate>
                            <uc3:Tooltip ID="tipActivate" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editdelete.png' alt='Desactivar' /> para<br />desactivar la serie de folios seleccionada.<br /><br />Hacer click en la imagen <img src='../Includes/Images/png/apply.png' alt='Activar' /> para<br />activar la serie de folios seleccionada." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='<%#Eval("Active").ToString() == "True" ? "Desactivar la serie<br />de folios <b>" + Eval("Serial") + "</b>" : "Activar la serie<br />de folios <b>" + Eval("Serial") + "</b>"%>'>
                                <asp:ImageButton ID="lbtnActive" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SerialID") %>'
                                    ImageUrl="~/Includes/Images/png/apply.png" meta:resourcekey="lbtnActiveResource1"
                                    OnClick="lbtnActive_Click" /></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
        </span>
        <asp:Button ID="btnAddFolioBottom" runat="server" Text="Agregar Folio" PostBackUrl="~/Series/SeriesAdd.aspx"
            CssClass="Button" meta:resourcekey="btnAddFolioBottomResource1" />
    </div>
</asp:Panel>
<div>
    <uc1:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
</div>
