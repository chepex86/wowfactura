﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SeriesDetails.aspx.cs"
    Inherits="PACFDManager.Series.SeriesDetails" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Image CssClass="imgCompany right" Style="position: absolute" ID="imgQR" runat="server"
                Visible="false" />
            <div id="container" class="wframe60">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitleDetails" runat="server" Text="Detalles de Serie" meta:resourcekey="lblTitleDetailsResource1"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li class="left"><span>
                                <asp:Label ID="lblTitleSeries" runat="server" Text="No.Serie:" Font-Bold="True" meta:resourcekey="lblTitleSeriesResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblSeries" runat="server" meta:resourcekey="lblSeriesResource1"></asp:Label></span>
                                <span>
                                    <asp:Label ID="lblTitleState" runat="server" Font-Bold="True" Text="Estado:" meta:resourcekey="lblTitleStateResource1"></asp:Label>
                                </span><span>
                                    <asp:Label ID="lblState" runat="server" meta:resourcekey="lblStateResource1"></asp:Label>
                                </span></li>
                            <li class="left"><span>
                                <asp:Label ID="lblTitleApprovalYear" runat="server" Font-Bold="True" Text="Año de aprobación:"
                                    meta:resourcekey="lblTitleApprovalYearResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblApprovalYear" runat="server" meta:resourcekey="lblApprovalYearResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblTitleApprovalNo" runat="server" Font-Bold="True" Text="No. Aprobación:"
                                    meta:resourcekey="lblTitleApprovalNoResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblApprovalNo" runat="server" meta:resourcekey="lblApprovalNoResource1"></asp:Label>
                            </span></li>
                            <li class="left"><span>
                                <asp:Label ID="lblTitleStart" runat="server" Font-Bold="True" Text="Inicio:" meta:resourcekey="lblTitleStartResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblStart" runat="server" meta:resourcekey="lblStartResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblTitleEnd" runat="server" Font-Bold="True" Text="Fin:" meta:resourcekey="lblTitleEndResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblEnd" runat="server" meta:resourcekey="lblEndResource1"></asp:Label>
                            </span></li>
                            <li class="left" id="liCertificate" runat="server"><span>
                                <asp:Label ID="lblTitleCertificate" runat="server" Font-Bold="True" Text="No. Certificado:"
                                    meta:resourcekey="lblTitleCertificateResource1"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblCertificate" runat="server" meta:resourcekey="lblCertificateResource1"></asp:Label>
                            </span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div id="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleList" runat="server" Text="Listado de Folios" meta:resourcekey="lblTitleListResource1"></asp:Label>
                    </h2>
                </div>
                <span>
                    <asp:GridView ID="gvFoliosDetails" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        CssClass="DataGrid" ForeColor="#333333" GridLines="None" OnRowDataBound="gvFoliosDetails_RowDataBound"
                        meta:resourcekey="gvFoliosDetailsResource1" AllowPaging="True" AllowSorting="True"
                        EnableSortingAndPagingCallbacks="True" OnPageIndexChanging="gvFoliosDetails_PageIndexChanging"
                        PageSize="20">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:BoundField DataField="Folio" HeaderText="Folio" meta:resourcekey="BoundFieldResource1" />
                            <asp:BoundField DataField="Canceled" HeaderText="Cancelado" meta:resourcekey="BoundFieldResource2" />
                            <asp:BoundField DataField="Used" HeaderText="Utilizado" meta:resourcekey="BoundFieldResource3" />
                            <asp:BoundField DataField="FilledDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha de Llenado"
                                HtmlEncode="False" meta:resourcekey="BoundFieldResource4" />
                            <asp:BoundField DataField="StartDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha de Inicio"
                                HtmlEncode="False" meta:resourcekey="BoundFieldResource5" />
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </span>
                <asp:Button ID="btnReturn" runat="server" CssClass="Button" PostBackUrl="~/Series/SeriesList.aspx"
                    Text="Aceptar" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
