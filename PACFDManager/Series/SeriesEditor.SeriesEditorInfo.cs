﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Series
{
	partial class SeriesEditor
	{
        /// <summary>
        /// Class used to group all main information about SeriesEditor class.
        /// </summary>
        public class SeriesEditorInfo
        {
            /// <summary>
            /// Get the SeriesEditor parent class of the object.
            /// </summary>
            public SeriesEditor Parent { get; private set; }
            ///<summary>
            ///
            /// </summary>
            public string NoSeries
            {
                get { return this.Parent.txtNoSeries.Text; }
                set { this.Parent.txtNoSeries.Text = value; }
            }
            ///<summary>
            ///
            /// </summary>
            public DropDownList ApprovalYear { get { return this.Parent.ddlApprovalYear; } }
            ///<summary>
            ///
            /// </summary>
            public string NoApproval
            {
                get { return this.Parent.txtApprovalNo.Text; }
                set { this.Parent.txtApprovalNo.Text = value; }
            }
            /// <summary>
            /// 
            /// </summary>
            public string Start
            {
                get { return this.Parent.txtStart.Text; }
                set { this.Parent.txtStart.Text = value; }
            }
            ///<summary>
            ///
            /// </summary>
            public string End
            {
                get { return this.Parent.txtEnd.Text; }
                set { this.Parent.txtEnd.Text = value; }
            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">SeriesEditor parent of the class.</param>
            public SeriesEditorInfo(SeriesEditor owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
	}
}
