﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Series
{
    public partial class SeriesDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.CurrentElectronicBillingType == ElectronicBillingType.CFDI)
            {
                this.lblApprovalYear.Visible =
                this.lblTitleApprovalYear.Visible =
                this.lblTitleApprovalNo.Visible =
                this.lblApprovalNo.Visible = false;
            }

            this.Title = this.GetLocalResourceObject("SeriesDetail").ToString();

            if (!IsPostBack)
                this.LoadData();
        }
        /// <summary>
        /// Load concept information by ConceptID.
        /// </summary>
        protected void LoadData()
        {
            if (Request.QueryString["SerialID"].IsNull())
                this.Response.Redirect(typeof(SeriesList).Name + ".aspx");

            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet.Serial_DetailsDataTable ta;

            ta = series.Serial_Details(Convert.ToInt32(Request.QueryString["SerialID"]));
            this.LoadSerialInfo(ta);

            this.gvFoliosDetails.DataMember = ta.TableName;
            this.gvFoliosDetails.DataSource = ta;
            this.gvFoliosDetails.DataBind();

            if (CBBEnable && CurrentElectronicBillingType == ElectronicBillingType.CBB)
            {
                this.liCertificate.Visible = false;
                this.imgQR.Visible = true;
                this.imgQR.ImageUrl = "~/" + typeof(ImageHandler).Name + ".ashx?type=4&SerialID=" + ta[0].SerialID;
            }

            ta.Dispose();
        }
        /// <summary>
        /// Load Serial Info
        /// </summary>
        /// <param name="t">SerialDataSet.Serial_DetailsDataTable t</param>
        protected void LoadSerialInfo(SerialDataSet.Serial_DetailsDataTable t)
        {
            if (!(t.Count > 0))
            { /*send message*/}
            this.lblSeries.Text = !t[0].Serial.IsNull() ? t[0].Serial : "-";
            this.lblState.Text = !t[0].Active.IsNull() ? (t[0].Active ?
                this.GetLocalResourceObject("Active").ToString() :
                this.GetLocalResourceObject("NotActive").ToString()) : "-";
            this.lblApprovalYear.Text = !t[0].AprovationYear.IsNull() ? t[0].AprovationYear.ToString() : "-";
            this.lblApprovalNo.Text = !t[0].AprovationNumber.IsNull() ? t[0].AprovationNumber.ToString() : "-";
            this.lblStart.Text = !t[0].Start.IsNull() ? t[0].Start.ToString() : "-";
            this.lblEnd.Text = !t[0].End.IsNull() ? t[0].End.ToString() : "-";
        }
        /// <summary>
        /// Change the default value depending on the cell that contains for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvFoliosDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowIndex > -1)
            {
                row.Cells[1].Text = (row.Cells[1].Text == "True" ? this.GetLocalResourceObject("Yes").ToString() : "No");
                row.Cells[2].Text = (row.Cells[2].Text == "True" ? this.GetLocalResourceObject("Yes").ToString() : "No");
                row.Cells[3].Text = (row.Cells[3].Text.Replace("&nbsp;", String.Empty) == String.Empty ? "-" :
                    Convert.ToDateTime(row.Cells[3].Text).ToString("dd/MM/yyyy"));
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesDetails.gvFoliosDetails_PageIndexChanging event
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvFoliosDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvFoliosDetails.PageIndex = e.NewPageIndex;
            this.LoadData();
        }
    }
}