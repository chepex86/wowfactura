﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Series
{
    public partial class SeriesAdd : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.GetLocalResourceObject("AddSeries").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucSeriesEditor_ApplyChanges(object sender, SeriesEditorEventArgs e)
        {
            if (e.EditorMode != SeriesEditorMode.Add) return;
            else
            {
                if (!Add(e))
                    this.ucSeriesEditor.StartIndexError =
                    this.GetLocalResourceObject("CompareValidatorMessage").ToString() + ViewState["End"].ToString();
                else
                {
                    this.ucSeriesEditor.StartIndexErrorVisible = false;
                    this.WebMessageBox1.Title = "Agregar Folios";
                    this.WebMessageBox1.ShowMessage("Los Folios han sido agregados exitosamente.<br /><br />Para poder expedir comprobantes debe activar una serie del Listado de Folios",
                        System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                }
                ViewState["End"] = null;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesAdd.WebMessageBox.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(Object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Accept)
                this.Response.Redirect(typeof(SeriesList).Name + ".aspx");

        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucSeriesEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect("~/Series/SeriesList.aspx");
        }
        /// <summary>
        /// Inserts a new row with the series information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Add(SeriesEditorEventArgs e)
        {
            if (CheckForValidIndex(e.BillerID, e.Series, e.Start) != 0)
                return false;

            String Series = e.Series.Trim() == String.Empty ? " " : e.Series;

            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.SerialRow drSeries = ds.Serial.NewSerialRow();

            String BeforeDataSet = ds.GetXml();

            drSeries.BillerID = e.BillerID;
            if (this.HaveBranch)
                drSeries.BranchID = this.CurrentBranchID;
            else
                drSeries.SetBranchIDNull();
            drSeries.Serial = Series;
            drSeries.Active = e.Active;
            drSeries.AprovationYear = e.AprovationYear;
            drSeries.AprovationNumber = e.AprovationNumber;
            drSeries.Start = e.Start;
            drSeries.End = e.End;
            drSeries.IsFolioCBB = false;

            ds.Serial.AddSerialRow(drSeries);

            SerialDataSet.FoliosRow drFolio;

            for (long i = e.Start; i <= e.End; i += 1)
            {
                drFolio = ds.Folios.NewFoliosRow();
                drFolio.Folio = i.ToString();
                drFolio.Canceled =
                drFolio.Used = false;
                drFolio.Completed = false;
                drFolio.StartDate = System.DateTime.Now;
                drFolio.SetParentRow(drSeries);
                ds.Folios.AddFoliosRow(drFolio);
            }
            if (!series.UpdateFolios(ds))
                return false;

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo bloque de folios [{1}-{2}] de la serie [{3}].",
                this.UserName, e.Start, e.End, Series), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
            #endregion

            LogManager.WriteStackTrace(new Exception("Éxito al Agregar Serie."));
            return true;
        }
        /// <summary>
        /// Validates that the index should be introduced in correct succession.
        /// </summary>
        /// <param name="BillerID">int BillerID</param>
        /// <param name="serial">string serial</param>
        /// <param name="indexStart">long indexStart</param>
        /// <returns>Returns the succession value that you must have the index, if the process fails returns -1.</returns>
        private long CheckForValidIndex(int BillerID, string serial, long indexStart)
        {
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.Serial_GetLastIDDataTable ta;
            try
            {
                ta = series.SearchingLastSerialID(BillerID, this.CurrentBranchID, serial);
                if (ta.Count > 0)
                {
                    if (indexStart <= ta[0].End)
                    {
                        ViewState["End"] = ta[0].End;
                        return ta[0].End;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                return -1;
#endif
            }
            return 0;
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucSeriesEditor.EditorMode = SeriesEditorMode.Add;
            this.ucSeriesEditor.AddMode = true;
        }
    }
}