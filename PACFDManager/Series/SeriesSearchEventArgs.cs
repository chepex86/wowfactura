﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Series
{
    public class SeriesSearchEventArgs : EventArgs
    {
        public int? Year { get; private set; }
        public string Serial { get; private set; }
        public int? Active { get; private set; }
        public SeriesSearchMode SearchMode { get; private set; }

        public SeriesSearchEventArgs(int? year, string serial, int? active, SeriesSearchMode editmode)
        {
            this.SearchMode = editmode;
            this.Year = year;
            this.Serial = serial;
            this.Active = active;
        }

        public SeriesSearchEventArgs(int? year, string serial, int? active)
        {
            this.Year = year;
            this.Serial = serial;
            this.Active = active;
        }
    }
}
