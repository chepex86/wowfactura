﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Series
{
    public partial class SeriesSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the Search Event Handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void SearchEventHandler(object sender, SeriesSearchEventArgs e);
        /// <summary>
        /// Event handler for the concepts Search.
        /// </summary>
        public event SearchEventHandler Search;
        /// <summary>
        /// Get or Set a SeriesSearchMode value with the control mode.
        /// </summary>
        public SeriesSearchMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return SeriesSearchMode.Year;

                return o.GetType() == typeof(SeriesSearchMode) ? (SeriesSearchMode)o : SeriesSearchMode.Year;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.CurrentElectronicBillingType == ElectronicBillingType.CFDI)
            {
                this.lblByYear.Visible =
                this.ddlSearchByYear.Visible = false;
            }

            if (IsPostBack)
                return;

            this.FillDDLYear();
            this.LoadSeries();
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesSearch.OnSearch event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(SeriesSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesSearch.Search_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int? i = null;
            String c = String.Empty;
            SeriesSearchEventArgs a;

            a = new SeriesSearchEventArgs((this.ddlSearchByYear.SelectedValue != "-1" ? Convert.ToInt32(this.ddlSearchByYear.SelectedValue) : i),
                                          (this.ddlSeries.SelectedValue != "-1" ? this.ddlSeries.SelectedValue : c),
                                          (this.ddlSearchByState.SelectedValue != "-1" ? Convert.ToInt32(this.ddlSearchByState.SelectedValue) : i));

            this.OnSearch(a);

            #region old code
            /*
            Button b = sender as Button;

            if (b.IsNull())
                return;

            if (b == this.btnSearchByDate)
            {
                this.EditorMode = SeriesSearchMode.Year;
                a = new SeriesSearchEventArgs(Convert.ToInt32(this.ddlSearchByYear.SelectedValue), String.Empty, null, this.EditorMode);
                this.OnSearch(a);
            }
            else if (b == this.btnSearchByFolio)
            {
                this.EditorMode = SeriesSearchMode.Serial;
                a = new SeriesSearchEventArgs(null, this.ddlSeries.SelectedValue, null, this.EditorMode);
                this.OnSearch(a);
            }
            else if (b == this.btnSearchByState)
            {
                this.EditorMode = SeriesSearchMode.Active;
                a = new SeriesSearchEventArgs(null, String.Empty, Convert.ToInt32(this.ddlSearchByState.SelectedValue), this.EditorMode);
                this.OnSearch(a);
            }*/
            #endregion
        }
        /// <summary>
        /// Find and fill the DropDownList with the existing series.
        /// </summary>
        protected void LoadSeries()
        {
            this.ddlSeries.Items.Clear();
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.Serial_GetByFkBillerIDCustomDataTable ta;
            ta = series.SelectByIDBiller(this.CurrentBillerID, this.CurrentBranchID);
            
            ListItem l = new ListItem();
            l.Value = "-1";
            l.Text = "[Todos]";

            this.ddlSeries.Items.Add(l);
            
            if (ta.Count < 1)
                return;

            for (int i = 0; i < ta.Count; i += 1)
                this.ddlSeries.Items.Add(ta[i].Serial);

            //else { }mandar mensaje!! ***Pendiente!
        }
        /// <summary>
        /// Fill the DropDownList with the current year and two years consecutively.
        /// </summary>
        protected void FillDDLYear()
        {
            this.ddlSearchByYear.Items.Clear();
            int y = System.DateTime.Now.Year;

            ListItem l = new ListItem();
            l.Value = "-1";
            l.Text = "[Todos]";

            this.ddlSearchByYear.Items.Add(l);

            for (int i = y; i >= y - 2; i -= 1)
                this.ddlSearchByYear.Items.Add(i.ToString());
        }
    }
}