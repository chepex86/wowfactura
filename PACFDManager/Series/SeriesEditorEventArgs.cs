﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Series
{
    public class SeriesEditorEventArgs
    {
        public SeriesEditorMode EditorMode { get; private set; }
        public int BillerID { get; set; }
        public string Series { get; set; }
        public bool Active { get; set; }
        public int AprovationYear { get; set; }
        public int AprovationNumber { get; set; }
        public long Start { get; set; }
        public long End { get; set; }

        public SeriesEditorEventArgs(int billerid, string series, bool active, int apyear, int apnumber,
            long start, long end, SeriesEditorMode editorMode)
        {
            this.BillerID = billerid;
            this.Series = series;
            this.Active = active;
            this.AprovationYear = apyear;
            this.AprovationNumber = apnumber;
            this.Start = start;
            this.End = end;
            this.EditorMode = editorMode;
        }
    }
}
