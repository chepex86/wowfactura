﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeriesSearch.ascx.cs"
    Inherits="PACFDManager.Series.SeriesSearch" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>

<%--<script language="javascript" type="text/javascript">
    function reseta() {
        var ddl1 = document.getElementById('<%=ddlSearchByYear.ClientID%>');
        var ddl2 = document.getElementById('<%=ddlSeries.ClientID%>');
        var ddl3 = document.getElementById('<%=ddlSearchByState.ClientID%>');

        ddl1.options[0].selected = true;
        ddl2.options[0].selected = true;
        ddl3.options[0].selected = true;
    }
</script>--%>

<div id="container" class="wframe60">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitle" runat="server" Text="Búsqueda de Folios" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblBySeries" runat="server" Text="Por Serie:"></asp:Label>
                    </label>
                    <span class="vtip" title="Selecciona la <b>Serie</b> que necesites buscar, o selecciona<br /><b>Todos</b> para incluir todas las series en la búsqueda.">
                        <asp:DropDownList ID="ddlSeries" runat="server" AutoPostBack="True" Width="81px">
                        </asp:DropDownList>
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByYear" runat="server" Text="Por Año:"></asp:Label>
                    </label>
                    <span class="vtip" title="Selecciona el <b>año</b> que necesites buscar, o selecciona<br /><b>Todos</b> para incluir todos los años en la búsqueda.">
                        <asp:DropDownList ID="ddlSearchByYear" runat="server" AutoPostBack="True" Width="81px">
                        </asp:DropDownList>
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByStatus" runat="server" Text="Por Estado:"></asp:Label>
                    </label>
                    <span class="vtip" title="Selecciona el <b>Estado</b> del folio que necesites buscar, o selecciona<br /><b>Todos</b> para incluir todos los estados en la búsqueda.<br /><br /><b>Nota:</b> Por default se buscaran los folios con estado <b>Activo</b>.">
                        <asp:DropDownList ID="ddlSearchByState" runat="server">
                            <asp:ListItem Selected="True" Value="-1">[Todos]</asp:ListItem>
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                            <asp:ListItem Value="0">No Activo</asp:ListItem>
                        </asp:DropDownList>
                    </span></li>
                <li class="left">&nbsp;<%--<asp:Button ID="btnReset" runat="server" CausesValidation="False"
                    CssClass="Button_Search" EnableViewState="False" Text="Limpiar" OnClientClick="javascript:reseta();" />--%>
                    <asp:Button ID="btnSearch" runat="server" CssClass="Button_Search" OnClick="btnSearch_Click"
                        Text="Buscar" />
                </li>
            </ul>
        </div>
    </div>
</div>
