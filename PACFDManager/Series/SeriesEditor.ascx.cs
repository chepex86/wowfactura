﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.Data;
using PACFD.Rules;
#endregion

namespace PACFDManager.Series
{
    public partial class SeriesEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Delegate for the control Apply Changes.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        public delegate void ApplyChangesEventHandler(object sender, SeriesEditorEventArgs e);
        /// <summary>
        /// Event handler for the control Apply Changes.
        /// </summary>
        public event ApplyChangesEventHandler ApplyChanges;
        /// <summary>
        /// Event handler for the control Cancel Changes.
        /// </summary>
        public event EventHandler CancelClick;
        /// <summary>
        /// Get a SeriesEditorInfo with all main information.
        /// </summary>
        public SeriesEditorInfo SeriesInformation { get; private set; }
        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        public SeriesEditor()
        {
            this.SeriesInformation = new SeriesEditorInfo(this);
        }
        /// <summary>
        ///  Get or Set a SeriesEditorMode value with the control mode.
        /// </summary>
        public SeriesEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return SeriesEditorMode.Add;

                return o.GetType() == typeof(SeriesEditorMode) ? (SeriesEditorMode)o : SeriesEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Generate a new instance of the class of type SeriesInfo to control the fields of the control.
        /// </summary>
        /// <returns>Returns a SeriesInfo class.</returns>
        protected virtual SeriesEditorInfo OngenerateSeriesInformation()
        {
            return new SeriesEditorInfo(this);
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.FillDDLYear();

            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI || this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_2 || this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_3)
            {
                this.gvFoliosList.Columns[2].Visible =
                this.gvFoliosList.Columns[3].Visible =
                this.lblApprovalYear.Visible =
                this.lblApprovalNo.Visible =
                this.txtApprovalNo.Visible =
                this.revApprovalNo.Enabled =
                this.rfvApprovalNo.Enabled =
                this.ddlApprovalYear.Visible = false;
            }
            if (this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CBB)
            {
                this.btnAddFolioTop.PostBackUrl = "~/FoliosCBB/FoliosCBBAdd.aspx";
                this.btnAddFolioBottom.PostBackUrl = "~/FoliosCBB/FoliosCBBAdd.aspx";
            }
            this.ucSeriesSearch.Search += new SeriesSearch.SearchEventHandler(ucSeriesSearch_Search);
        }
        /// <summary>
        /// Event fired by the delegate of the user control ucSeriesSearch. 
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        void ucSeriesSearch_Search(object sender, SeriesSearchEventArgs e)
        {
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            this.gvFoliosList.DataSource = series.SelectBySearching(this.CurrentBillerID, this.CurrentBranchID, e.Year, e.Serial, e.Active);
            this.gvFoliosList.DataBind();

            if (this.gvFoliosList.Rows.Count < 1)
            {
                DataTable dt = new SerialDataSet.SerialDataTable();
                DataRow dr = dt.NewRow();
                dr["BillerID"] = -1;
                dr["Serial"] = String.Empty;
                dr["Active"] = false;
                dr["AprovationYear"] = -1;
                dr["AprovationNumber"] = -1;
                dr["Start"] = -1;
                dr["End"] = -1;
                dr["IsFolioCBB"] = 0;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvFoliosList.DataSource = dt;
                this.gvFoliosList.DataBind();
            }
        }
        /// <summary>
        /// Fills the datagrid with table gvFoliosList Serial_GetBySearching.
        /// </summary>
        /// <param name="table">SerialDataSet.Serial_GetBySearchingDataTable parameter</param>
        public void FillGrid(SerialDataSet.Serial_GetBySearchingDataTable table)
        {
            this.gvFoliosList.DataSource = table;
            this.gvFoliosList.DataMember = table.TableName;
            this.gvFoliosList.DataBind();
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.ApplyChanges event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnApplyChanges(SeriesEditorEventArgs e)
        {
            if (!this.ApplyChanges.IsNull())
                this.ApplyChanges(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.CancelClik event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.btnAccept_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.OnApplyChanges(this.OnGenerateAcceptArguments());
        }
        /// <summary>
        /// Fires the PACFDManager.Series.SeriesEditor.btnCancel_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
        /// <summary>
        /// Generate a new instance of the class SeriesEditorEventArgs.
        /// </summary>
        /// <returns>
        /// Returns a new instance of SeriesEditorEventArgs, 
        /// the parameters entered into the editor of series.
        /// </returns>
        protected SeriesEditorEventArgs OnGenerateAcceptArguments()
        {
            return new SeriesEditorEventArgs(this.CurrentBillerID,
                                            this.txtNoSeries.Text, false,
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ||
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ? -1 : Convert.ToInt32(this.ddlApprovalYear.SelectedValue),
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI ||
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_2 ||
                                            this.CurrentElectronicBillingType == PACFD.Rules.ElectronicBillingType.CFDI3_3 ? -1 : Convert.ToInt32(this.txtApprovalNo.Text),
                                            Convert.ToInt64(this.txtStart.Text),
                                            Convert.ToInt64(this.txtEnd.Text),
                                            SeriesEditorMode.Add);
        }
        /// <summary>
        /// Fills the FillDDLYear with the current year and two years before to current year.
        /// </summary>
        protected void FillDDLYear()
        {
            this.ddlApprovalYear.Items.Clear();
            int y = System.DateTime.Now.Year;
            for (int i = y; i >= y - 2; i -= 1)
                this.ddlApprovalYear.Items.Add(i.ToString());
        }
        /// <summary>
        /// Redirects to the series detail page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lbtnDetail_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            Response.Redirect("~/Series/SeriesDetails.aspx?SerialID=" + s.CommandArgument);
        }
        protected void ibDelte_OnClick(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;

            this.WebMessageBox1.Title = "Eliminar Folios";
            this.WebMessageBox1.ShowMessage("¿Deseas eliminar este bloque de folios?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["SerialID"] = s.CommandArgument + "_1";
        }
        /// <summary>
        /// Change the status from active to inactive, and backwards.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void lbtnActive_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            if (s.CommandName == "true")
            {
                this.WebMessageBox1.Title = this.GetLocalResourceObject("ActivateSeries").ToString();
                this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Message1").ToString(),
                    System.Drawing.Color.Red, WebMessageBoxButtonType.YesNo);
            }
            else
            {
                this.WebMessageBox1.Title = this.GetLocalResourceObject("DeactivateSeries").ToString();
                this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Message2").ToString(),
                    System.Drawing.Color.Red, WebMessageBoxButtonType.YesNo);
            }
            this.ViewState["SerialID"] = s.CommandArgument + "_2";
        }
        /// <summary>
        /// If you accept the delete confirmation to the selected serie, otherwise returns to the list of series.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (this.ViewState["SerialID"].IsNull())
                return;

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();

            series.SelectByIDSerial(ds.Serial, Convert.ToInt32(this.ViewState["SerialID"].ToString().Split('_')[0]));
            SerialDataSet.SerialRow drSerial = ds.Serial[0];

            String BeforeDataSet = ds.GetXml();

            if (this.ViewState["SerialID"].ToString().Split('_')[1] == "1")
            {
                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino el bloque de folios [{1}/{2}] de la serie {3}.",
                    this.UserName, drSerial.Start, drSerial.End, drSerial.Serial), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
                #endregion

                drSerial.Delete();
                if (!series.UpdateSerial(ds.Serial))
                {
                    ds.Dispose();
                    this.ViewState["SerialID"] = null;
                    LogManager.WriteStackTrace(new Exception("Error al tratar de eliminar los folios"));
                    this.WebMessageBox1.Title = "Eliminar Folios";
                    this.WebMessageBox1.ShowMessage("Ocurrio un problema al tratar de eliminar los folios.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }

                LogManager.WriteStackTrace(new Exception("Éxito al Eliminar Folios."));
                this.Response.Redirect(typeof(SeriesList).Name + ".aspx");
            }
            else if (this.ViewState["SerialID"].ToString().Split('_')[1] == "2")
            {
                bool active = false;

                if (ds.Serial.Count > 0)
                {
                    drSerial.Active = (drSerial.Active == true ? false : true);
                    active = drSerial.Active;
                }
                if (!series.UpdateSerial(ds.Serial))
                {
                    ds.Dispose();
                    this.ViewState["SerialID"] = null;
                    LogManager.WriteStackTrace(new Exception("Error al tratar de desactivar serial"));
                    this.WebMessageBox1.Title = this.GetLocalResourceObject("DeactivateSeries").ToString();
                    this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("Error").ToString(), System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} {1} el bloque de folios [{2}/{3}] de la serie {4}",
                    this.UserName, (active ? "activo" : "desactivo"), drSerial.Start, drSerial.End, drSerial.Serial), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
                #endregion

                LogManager.WriteStackTrace(new Exception("Éxito al Des/Activar Serie."));
                this.Response.Redirect(typeof(SeriesList).Name + ".aspx");
            }
        }
        /// <summary>
        /// Change the default value depending on the cell that contains for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvFoliosList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            ImageButton i = e.Row.FindControl("lbtnActive") as ImageButton;
            ImageButton delete = e.Row.FindControl("ibDelete") as ImageButton;

            if (row.RowIndex > -1)
            {
                PACFD.Rules.Series serie = new PACFD.Rules.Series();
                PACFD.DataAccess.SerialDataSet.Folios_GetFoliosUsedDataTable folio;
                folio = serie.GetFoliosUsed(Convert.ToInt32(delete.CommandArgument));

                if (folio[0].Used > 0)
                    delete.Visible = false;

                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int j = 0; j < row.Cells.Count; j += 1)
                        row.Cells[j].Visible = false;
                    ViewState["Empty"] = null;
                }
                if (!ViewState["State"].IsNull())
                {
                    if (Convert.ToInt32(ViewState["State"].ToString()) > 0)
                    {
                        if (row.Cells[6].Text == "True")
                        {
                            row.Cells[9].Enabled =
                            i.Visible = true;
                            row.Cells[6].BackColor = System.Drawing.Color.FromArgb(198, 239, 206);//Green
                            row.Cells[6].ForeColor = System.Drawing.Color.FromArgb(43, 97, 45);
                            delete.Visible = false;
                        }
                        else
                        {
                            row.Cells[9].Enabled =
                            i.Visible = false;
                            row.Cells[6].BackColor = System.Drawing.Color.FromArgb(255, 199, 206);//Red
                            row.Cells[6].ForeColor = System.Drawing.Color.FromArgb(172, 0, 48);
                        }
                    }
                    else
                    {
                        row.Cells[9].Enabled = true;
                        row.Cells[6].BackColor = System.Drawing.Color.FromArgb(255, 199, 206);//Red
                        row.Cells[6].ForeColor = System.Drawing.Color.FromArgb(172, 0, 48);
                    }

                    i.CommandName = (row.Cells[6].Text == "True" ? "false" : "true");
                    i.ImageUrl = (row.Cells[6].Text == "True" ? "~/Includes/Images/png/editdelete.png" : "~/Includes/Images/png/apply.png");
                    row.Cells[6].Text = (row.Cells[6].Text == "True" ? "Activo" : "No Activo");
                }
            }
            else
            {
                PACFD.Rules.Series series = new PACFD.Rules.Series();
                SerialDataSet.Serial_GetActiveDataTable ta;
                ta = series.SerialGetActiveByIDBiller(this.CurrentBillerID, this.CurrentBranchID);
                ViewState["State"] = ta.Count;
                ta.Dispose();
            }
        }
        /// <summary>
        /// Configurations for the editor.
        /// </summary>
        #region Properties
        /// <summary>
        /// Sets value the visible property of the listing.
        /// </summary>
        protected bool VisibleList
        {
            set
            {
                this.pnlFolioList.Visible = value;
            }
        }
        /// <summary>
        /// Sets value the visible property of the options: Add, Details, Modify or Edit.
        /// </summary>
        protected bool VisibleOptions
        {
            set
            {
                this.pnlFolioOptions.Visible = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: Add.
        /// </summary>
        public bool AddMode
        {
            set
            {
                this.lblTitleOptions.Text = "Agregar Folios";//this.GetLocalResourceObject("FoliosAuthorization").ToString();
                this.btnAccept.Text = "Aceptar";//this.GetLocalResourceObject("Authorize").ToString();
                this.VisibleOptions = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor: List.
        /// </summary>
        public bool ListMode
        {
            set
            {
                this.VisibleList = value;
            }
        }
        /// <summary>
        /// Change the visible property to the value received.
        /// </summary>
        public bool StartIndexErrorVisible
        {
            set
            {
                this.lblStartError.Visible = value;
            }
        }
        /// <summary>
        /// Displays the error message or hidden depending on the value received.
        /// </summary>
        public string StartIndexError
        {
            set
            {
                this.lblStartError.Text = value;
                this.StartIndexErrorVisible = true;
            }
        }
        #endregion
    }
}