﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Series
{
    public partial class SeriesList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.GetLocalResourceObject("SeriesList").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucSeriesEditor.EditorMode = SeriesEditorMode.List;
            this.ucSeriesEditor.ListMode = true;
            this.LoadList();
        }
        /// <summary>
        /// Load the concept list.
        /// </summary>
        private void LoadList()
        {
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            this.ucSeriesEditor.FillGrid(series.SelectBySearching(
                this.CurrentBillerID, this.CurrentBranchID, null, String.Empty, null));
        }
    }
}