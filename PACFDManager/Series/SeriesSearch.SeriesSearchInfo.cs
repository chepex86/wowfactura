﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Series
{
    partial class SeriesSearch
    {
        /// <summary>
        /// Class used to group all main information about SeriesSearch class.
        /// </summary>
        public class SeriesSearchInfo
        {
            /// <summary>
            /// Get the SeriesSearch parent class of the object.
            /// </summary>
            public SeriesSearch Parent { get; private set; }
            /// <summary>
            /// 
            /// </summary>
            public DropDownList Series { get { return this.Parent.ddlSeries; } }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">SeriesSearch parent of the class.</param>
            public SeriesSearchInfo(SeriesSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
