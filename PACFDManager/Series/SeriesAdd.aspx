﻿<%@ Page Title="Agregar Folios" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="SeriesAdd.aspx.cs" Inherits="PACFDManager.Series.SeriesAdd" %>

<%@ Register Src="SeriesEditor.ascx" TagName="SeriesEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:SeriesEditor ID="ucSeriesEditor" runat="server" OnCancelClick="ucSeriesEditor_CancelClick"
                OnApplyChanges="ucSeriesEditor_ApplyChanges" />
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
