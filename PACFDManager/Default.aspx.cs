﻿#region using
using System;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Xml;
using System.Drawing;
using System.Globalization;
using PACFD.Rules.mx.org.banxico.www;
#endregion

namespace PACFDManager
{
    public partial class Default1 : BasePage
    {
        private DgieWS ExchangeRateWS { set; get; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.ValidateBasicUser();
            this.LoadData();
        }

        private void LoadData()
        {
            this.FillsFoliosInfo();

            if (this.CurrentElectronicBillingType != ElectronicBillingType.CBB)
            {
                this.FillsCertificateInfo();

                if (this.CurrentElectronicBillingType == ElectronicBillingType.CFD || this.CurrentElectronicBillingType == ElectronicBillingType.CFD2_2)
                {
                    this.FillsReportsInfo();
                    this.AlertReport();
                }
            }

            this.FillsBackupInfo();
            this.GetLogo();

            if (!this.IsAdministrator)
                this.HideBackupSection();

            //if (this.CurrentElectronicBillingType != ElectronicBillingType.CFD)
            this.FillsExchangeRates();
        }

        private void FillsExchangeRates()
        {
            this.trExchangeRate1.Visible =
            this.trExchangeRate2.Visible =
            this.trExchangeRate3.Visible = true;

            SystemConfigurationDataSet.SystemConfigurationDataTable ta;
            SystemConfigurationDataSet.SystemConfigurationRow row;
            SystemConfiguration sysconfig = new SystemConfiguration();
            String exchrate = String.Empty;
            String url = "ExchangeRate/ExchangeRate.aspx";

            ta = sysconfig.GetAllSystemConfiguration();
            row = ta[0];

            if ((ta[0].IsExchangeRateNull() || ta[0].ExchangeRate == 0) || ta[0].IsLatestRevisionExchangeRateNull() || this.CompareDates(ta[0].LatestRevisionExchangeRate))
            {
                exchrate = this.GetExchangeRate();

                if (exchrate.Trim() == String.Empty || exchrate.Trim() == "|" || exchrate.Split('|')[1] == "N/E")
                {
                    String msj = "1 USD = " + ta[0].ExchangeRate + " MXN";
                    String dat = this.FormatDate(Convert.ToDateTime(ta[0].LatestRevisionExchangeRate));

                    this.lblExchangeRate.Text = String.Format("<a style='color: Red' href='{0}'>{1}</a>", url, msj);
                    this.lblLatestRevisionExchangeRates.Text =
                        String.Format("<a style='color: Red' href='{0}'>{1}</a>", url, dat);

                    this.WebMessageBox1.Title = "Alerta";
                    this.WebMessageBox1.CommandArguments = "1";
                    this.WebMessageBox1.ShowMessage("No ha sido posible obtener el tipo de cambio del día de hoy.<br /><br />Por favor ingresarlo manualmente.",
                        System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                }
                else
                {
                    this.lblExchangeRate.Text = String.Format("<a href='{1}'>1 USD = {0} MXN</a>", exchrate.Split('|')[1], url);
                    this.lblLatestRevisionExchangeRates.Text = String.Format("<a href='{1}'>{0}</a>",
                        this.FormatDate(Convert.ToDateTime(exchrate.Split('|')[0])), url);

                    row.ExchangeRate = Convert.ToDecimal(exchrate.Split('|')[1]);
                    row.LatestRevisionExchangeRate = Convert.ToDateTime(exchrate.Split('|')[0]);


                    if (!sysconfig.Update(ta))
                    {
                        //...
                    }
                }
            }
            else
            {
                this.lblExchangeRate.Text = String.Format("<a href='{1}'>1 USD = {0} MXN</a>", ta[0].ExchangeRate.ToString(), url);
                this.lblLatestRevisionExchangeRates.Text = String.Format("<a href='{1}'>{0}</a>",
                    this.FormatDate(ta[0].LatestRevisionExchangeRate), url);
            }

            Security.Security.SetExchangeRate();
        }

        private bool FillsCertificateInfo()
        {
            this.trDCTitle.Visible =
            this.trDCState.Visible =
            this.trDCExpiration.Visible = true;

            DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable t;
            t = this.GetActive();

            if (t.IsNull())
            {
                this.lblExpirationCertificate.Text = "-";
                this.lblStateCertificate.Text = "No Activo";
                return false;
            }

            this.lblExpirationCertificate.Text =
                this.GetDate(Convert.FromBase64String(t[0].Certificate)).ToString("dd/MM/yyyy");
            this.lblStateCertificate.Text = "Activo";

            return true;
        }

        private bool FillsFoliosInfo()
        {
            SerialDataSet.Serial_GetInfoByBillerIDDataTable t;
            t = this.GetInfo();

            if (t.IsNull())
            {
                this.lblSerial.Text = "No hay series";
                this.lblUnused.Text = "-";
                this.lblFoliosUsed.Text = "-";
                this.lblCanceled.Text = "-";
                return false;
            }

            int u = t[0].Unused;

            this.lblUnused.ForeColor = u >= 5 ? Color.Green : (u >= 1 ? Color.Orange : Color.Red);
            this.lblSerial.Text = t[0].Serial;
            this.lblUnused.Text = t[0].Unused.ToString();
            this.lblFoliosUsed.Text = t[0].Used.ToString();
            this.lblCanceled.Text = t[0].Canceled.ToString();

            return true;
        }

        private bool FillsBackupInfo()
        {
            BackupLocalDataSet.BackupLocal_GetInfoDataTable t;
            t = this.GetBackupInfo();

            if (t.IsNull())
            {
                lblBackupName.Text = "No hay respaldos";
                lblBackupDate.Text = "-";
                lblBackupPath.Text = "-";
                return false;
            }

            lblBackupName.Text = t[0].Name;
            lblBackupDate.Text = t[0].Date.ToString("dd/MM/yyyy");
            lblBackupPath.Text = t[0].Path.Replace(t[0].Name + ".bak", String.Empty);

            return true;
        }

        private BackupLocalDataSet.BackupLocal_GetInfoDataTable GetBackupInfo()
        {
            PACFD.Rules.Backup backup = new PACFD.Rules.Backup();
            BackupLocalDataSet.BackupLocal_GetInfoDataTable ta;
            ta = backup.GetInfo(this.CurrentBillerID);

            return ta.Count < 1 ? null : ta;
        }

        private SerialDataSet.Serial_GetInfoByBillerIDDataTable GetInfo()
        {
            PACFD.Rules.Series serie = new PACFD.Rules.Series();
            SerialDataSet.Serial_GetInfoByBillerIDDataTable ta;
            int? branchID = null;
            if (this.HaveBranch) branchID = CurrentBranchID;
            ta = serie.SerialGetInfoByBillerID(this.CurrentBillerID, branchID);

            return ta.Count < 1 ? null : ta;
        }

        private DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable GetActive()
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            DigitalCertificatesDataSet.DigitalCertificates_GetActiveDataTable ta;
            ta = certificate.GetActiveByBillerID(this.CurrentBillerID);

            return ta.Count < 1 ? null : ta;
        }

        private DateTime GetDate(byte[] buffer)
        {
            DateTime dt = new DateTime();
            X509Certificate2 m_cer = new X509Certificate2(buffer);
            if (m_cer.IsNull())
                return new DateTime(1900, 1, 1);

            dt = m_cer.NotAfter;
            return dt;
        }

        #region Reports
        private bool FillsReportsInfo()
        {
            this.trReportTitle.Visible =
            this.trReportInfo.Visible =
            this.trReportDate.Visible = true;

            ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable t;
            t = this.LastReportGenerated();

            if (t.IsNull())
            {
                lblLastReportGenerated.Text = "No hay reportes";
                lblDateOfCreation.Text = "-";
                return false;
            }

            string m = t[0].Month.ToString();
            string y = t[0].Year.ToString();

            this.lblLastReportGenerated.Text = 2 + t[0].RFC + (t[0].Month < 10 ? "0" + m : m) + y;
            this.lblDateOfCreation.Text = t[0].CreationDate.ToString("dd/MM/yyyy");

            return true;
        }

        private void AlertReport()
        {
            if (IsPostBack)
                return;

            this.trAlert1.Visible =
            this.trAlert2.Visible =
            this.trAlert3.Visible = true;

            bool aux1 = true, aux2 = true;

            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.MonthlyReports_GetFirstBillingByBillerIDDataTable ta;
            ta = report.SelectFirstBillingByBillerID(this.CurrentBillerID);

            if (ta.Count < 1)
            {
                this.trAlert1.Visible = false;
                return;
            }

            aux1 = this.SearchReportSent(ta[0].BillingDate);
            aux2 = this.SearchReportGenerate(ta[0].BillingDate);

            if (!aux1 && !aux2)
                this.trAlert1.Visible = false;
        }

        private ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable LastReportGenerated()
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.BillerMonthlyReports_LastReportGenerated_ByBillerIDDataTable ta;
            ta = report.LastReportGenerated(this.CurrentBillerID);

            return ta.Count < 1 ? null : ta;
        }

        private bool SearchReportSent(DateTime date)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.MonthlyReports_GetAlertSentReportDataTable ta;

            int year = date.Year;
            int month = date.Month;

            ta = report.SelectAlertSent(this.CurrentBillerID, month, year,
                 DateTime.Now.AddMonths(-1).Month, (DateTime.Now.Month == 1 ? DateTime.Now.AddYears(-1).Year : DateTime.Now.Year));

            if (ta.Count < 1)
            {
                this.trAlert2.Visible = false;
                return false;
            }

            String url = "Reports/BillerMonthlyReport/BillerMonthlyReportList.aspx";
            this.lblAlertReportSent.Text = String.Format("<a style='color: Red' href='{2}'>{0} / {1}</a>",
                                                         this.FormatDate(ta[0].Month), ta[0].Year, url);
            return true;
        }

        private bool SearchReportGenerate(DateTime date)
        {
            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            ReportsDataSet.MonthlyReports_GetByDateDataTable ta;

            int year = date.Year;
            int month = date.Month;
            bool flag = true;

            String[] monthArray;
            int cont = 0;

            ta = report.SelectAlertGenerate(this.CurrentBillerID, month, year, DateTime.Now.Month,
                            (DateTime.Now.Month == 1 ? DateTime.Now.Year - 1 : DateTime.Now.Year));

            DateTime dt1 = new DateTime(date.Year, date.Month, 1);
            DateTime dt2 = DateTime.Now.AddMonths(-1);

            dt2 = new DateTime(dt2.Year, dt2.Month, 1);

            while (dt1 <= dt2)
            {
                cont += 1;
                dt1 = dt1.AddMonths(1);
            }

            monthArray = new String[cont];

            if (monthArray.Length < 1)
            {
                trAlert3.Visible = false;
                return false;
            }

            String cad = String.Empty;
            cont = 0;

            dt1 = new DateTime(date.Year, date.Month, 1);

            for (int i = 0; dt1.Month <= (DateTime.Now.Month - 1) || dt1.Year < DateTime.Now.Year; i++)
            {
                if (cont < ta.Count && dt1.Month == ta[cont].Month)
                {
                    monthArray[i] = dt1.Month.ToString() + "_" + dt1.Year.ToString() + "_1";
                    cont += 1;
                }
                else
                    monthArray[i] = dt1.Month.ToString() + "_" + dt1.Year.ToString() + "_0";

                dt1 = dt1.AddMonths(1);
            }

            for (int i = 0; i < monthArray.Length; i++)
            {
                if (monthArray[i].Split('_')[2] == "0")
                {
                    month = Convert.ToInt32(monthArray[i].Split('_')[0]);
                    year = Convert.ToInt32(monthArray[i].Split('_')[1]);
                    flag = false;
                    break;
                }
            }

            if (flag)
            {
                trAlert3.Visible = false;
                return false;
            }

            String url = String.Format("Reports/BillerMonthlyReport/BillerMonthlyReportGenerated.aspx" +
                                       "?month={0}&year={1}", month, year);

            this.lblAlertReportGenerate.Text = String.Format("<a style='color: Red' href='{2}'>{0} / {1}</a>",
                                                            this.FormatDate(month), year, url);
            return true;
        }
        #endregion

        private void GetLogo()
        {
            this.imgLogo.ImageUrl = typeof(ImageHandler).Name + ".ashx?type=1&BillerID=" + this.CurrentBillerID;
        }

        private void HideBackupSection()
        {
            this.tr1.Visible =
            this.tr2.Visible =
            this.tr3.Visible =
            this.tr4.Visible = false;
        }

        #region Format Date
        private String FormatDate(DateTime d)
        {
            CultureInfo ci = new CultureInfo("Es-Es");
            String DayOfWeek = ci.DateTimeFormat.GetDayName(d.DayOfWeek);

            return DayOfWeek + ", " + d.Day + " de " +
                this.FormatDate(d.Month) + " del " + d.Year +
                    ",<br />a las " + this.AddZero(d.Hour) + ":" +
                        this.AddZero(d.Minute) + ":" + this.AddZero(d.Second);
        }

        private String AddZero(int n)
        {
            return n < 10 ? "0" + n.ToString() : n.ToString();
        }

        private String FormatDate(int date)
        {
            String cad = String.Empty;

            switch (date)
            {
                case 1: cad = "Enero"; break;
                case 2: cad = "Febrero"; break;
                case 3: cad = "Marzo"; break;
                case 4: cad = "Abril"; break;
                case 5: cad = "Mayo"; break;
                case 6: cad = "Junio"; break;
                case 7: cad = "Julio"; break;
                case 8: cad = "Agosto"; break;
                case 9: cad = "Septiembre"; break;
                case 10: cad = "Octubre"; break;
                case 11: cad = "Noviembre"; break;
                case 12: cad = "Diciembre"; break;
            }

            return cad;
        }
        #endregion

        #region Validate Basic User
        private void ValidateBasicUser()
        {
            if (!this.IsBasicClient)
                return;

            if (CFDEnable && this.CurrentElectronicBillingType == ElectronicBillingType.CFD)
                return;

            if (CBBEnable && this.CurrentElectronicBillingType == ElectronicBillingType.CBB)
                return;

            if (CFDIEnable && this.CurrentElectronicBillingType == ElectronicBillingType.CFDI)
                return;

            if (CFD2_2Enable && this.CurrentElectronicBillingType == ElectronicBillingType.CFD2_2)
                return;

            if (CFDI3_2Enable && this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_2)
                return;

            if (CFDI3_3Enable && this.CurrentElectronicBillingType == ElectronicBillingType.CFDI3_3)
                return;
            if (CFDI4_0Enable && this.CurrentElectronicBillingType == ElectronicBillingType.CFDI4_0)
                return;
            this.WebMessageBox1.Title = "Alerta";
            this.WebMessageBox1.CommandArguments = "0";
            this.WebMessageBox1.ShowMessage("El sistema actual no cuenta con este tipo de facturación "
                    + this.CurrentElectronicBillingType.ToString() + ".",
                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }
        #endregion

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.CommandArguments == "1")
                this.Response.Redirect("~/ExchangeRate/ExchangeRate.aspx");
            if (e.CommandArguments == "0")
                this.Response.Redirect("~/Security/LogOut.aspx");
        }

        #region Exchange Rate
        private String GetExchangeRate()
        {
            PACFD.Rules.ExchangeRate er = new PACFD.Rules.ExchangeRate();
            DataTable dt = er.ExchangeRate1USDtoMXN();

            try
            {
                DateTime d = Convert.ToDateTime(dt.Rows[0]["TIME_PERIOD"]);

                if (Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")) > Convert.ToInt32((d.ToString("yyyyMMdd"))))
                    return "|";

                return dt.Rows[0]["TIME_PERIOD"] + "|" + dt.Rows[0]["OBS_VALUE"];
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                return String.Empty;
            }
        }

        public bool CompareDates(DateTime dt1)
        {
            DateTime dt2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
            return dt1 >= dt2 ? false : true;
        }

        private DataSet ReadXML(String strXML)
        {
            DataSet ds = new DataSet();
            XmlDocument xmldoc = new XmlDocument();

            try
            {
                xmldoc.LoadXml(strXML);

                using (XmlReader reader = new XmlNodeReader(xmldoc))
                {
                    ds.ReadXml(reader);
                    reader.Close();
                }

                return ds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                return null;
            }
        }

        #endregion
    }
}