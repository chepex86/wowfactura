﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
#endregion

namespace PACFDManager.AccessPac
{
    public partial class Accesspac : PACFDManager.BasePage
    {
        private String CurrentPassword
        {
            get
            {
                if (!ViewState[String.Format("{0}-CurrentPassword", ClientID)].IsNull())
                    return ViewState[String.Format("{0}-CurrentPassword", ClientID)].ToString();

                return String.Empty;
            }
            set { ViewState[String.Format("{0}-CurrentPassword", ClientID)] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                this.GetData();

            this.txtPassword.Attributes["value"] = this.CurrentPassword.Trim();
            this.txtRepassword.Attributes["value"] = this.CurrentPassword.Trim();
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            if (!this.AddAccessPac())
                this.WebMessageBox1.ShowMessage("No ha sido posible actualizar el Acceso a Pac.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
            else
                this.WebMessageBox1.ShowMessage("Se actualizo con exito el Acceso a Pac.",
                 System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);

            this.WebMessageBox1.Title = "Mensaje";
        }

        private bool AddAccessPac()
        {
            SystemConfiguration system = new SystemConfiguration();
            SystemConfigurationDataSet.SystemConfigurationDataTable table;
            SystemConfigurationDataSet.SystemConfigurationRow row;

            bool result = false;

            table = system.GetAllSystemConfiguration();

            if (table.Count < 1)
                return false;

            try
            {
                row = table[0];

                row.Username = this.txtUsername.Text.Trim();
                row.Password = Cryptography.EncryptUnivisitString(this.txtPassword.Text.Trim());
                row.fileType = this.txtFileType.Text.Trim();
                row.Url = this.txtUrl.Text.Trim();

                result = system.Update(table);

                DataSet ds = new DataSet();
                ds.Tables.Add(table);

                //Add new entry to log system
                if (result)
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} actualizo el Acceso a Pac.",
                        this.UserName), this.CurrentBillerID, null, ds.GetXml());

                ds.Dispose();

                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
                return false;
            }
        }

        private void GetData()
        {
            SystemConfiguration system = new SystemConfiguration();
            SystemConfigurationDataSet.SystemConfigurationDataTable table;

            table = system.GetAllSystemConfiguration();

            if (table.Count < 1)
                return;

            if (!table[0].IsUsernameNull())
                this.txtUsername.Text = table[0].Username;

            if (!table[0].IsPasswordNull())
            {
                this.txtPassword.Text = Cryptography.DecryptUnivisitString(table[0].Password);
                this.txtRepassword.Text = Cryptography.DecryptUnivisitString(table[0].Password);
                this.CurrentPassword = Cryptography.DecryptUnivisitString(table[0].Password);
            }

            if (!table[0].IsfileTypeNull())
                this.txtFileType.Text = table[0].fileType;

            if (!table[0].IsUrlNull())
                this.txtUrl.Text = table[0].Url;
        }

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {

        }
    }
}