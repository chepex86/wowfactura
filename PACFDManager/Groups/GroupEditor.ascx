﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupEditor.ascx.cs"
    Inherits="PACFDManager.Groups.GroupEditor" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div id="container" class="wframe40">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitleOptions" runat="server" Text="Titulo"></asp:Label></h2>
            </div>
            <ul>
                <li>
                    <label class="desc">
                        <asp:Label ID="lblId" runat="server" Text="ID:"></asp:Label></label>
                    <span><span>
                        <asp:TextBox ID="txtID" runat="server" MaxLength="255" Width="50px" ReadOnly="True"></asp:TextBox></span>
                    </span></li>
                <li>
                    <table>
                        <tr>
                            <td>
                                <label class="desc">
                                    <asp:Label ID="lblName" runat="server" Text="Nombre:"></asp:Label></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra el Nombre del Grupo." : "Introduce el Nombre del Grupo."%>'>
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="255" Width="200px"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                    </table>
                </li>
                <li class="buttons">
                    <div>
                        <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click"
                            ValidationGroup="Validators" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="Button" OnClick="btnCancel_Click"
                            Text="Cancelar" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick"
        OnCancelClick="WebMessageBox1_OnCancelClick" />
</div>
