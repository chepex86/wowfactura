﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.DataAccess.GroupsDataSetTableAdapters;
using PACFD.Rules;

namespace PACFDManager.Groups
{
    public enum GroupEditorMode : int
    {
        Add = 0,
        Edit = 1,
        View = 2,
        None = 99
    }

    public partial class GroupEditor : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            Loadfunctionality();
        }

        #region Metods
        private void Loadfunctionality()
        {
            lblTitleOptions.Text = string.Empty;
            txtName.Text = string.Empty;
            txtID.Text = string.Empty;
            txtName.Visible = false;
            btnAccept.Visible = false;
            btnCancel.Visible = false;
            if (CurrentGroupEditorMode == GroupEditorMode.Add)
            {
                lblTitleOptions.Text = "Nuevo Grupo";
                txtName.Visible = true;
                btnAccept.Visible = true;
                btnCancel.Visible = true;
                this.lblId.Visible =
                this.txtID.Visible = false;
            }
            else if (CurrentGroupEditorMode == GroupEditorMode.Edit)
            {
                lblTitleOptions.Text = "Editando Grupo";
                txtName.Visible = true;
                btnAccept.Visible = true;
                btnCancel.Visible = true;
                if (!SetID())
                    Response.Redirect("~/Default.aspx");
                else if (!LoadData())
                    Response.Redirect("~/Default.aspx");
                txtName.Enabled = true;
            }
            else if (CurrentGroupEditorMode == GroupEditorMode.View)
            {
                lblTitleOptions.Text = "Detalle del Grupo";
                txtName.Visible = true;
                btnAccept.Visible = true;
                btnCancel.Visible = true;
                txtName.Visible = true;
                btnAccept.Visible = true;
                btnCancel.Visible = true;
                if (!SetID())
                    Response.Redirect("~/Default.aspx");
                else if (!LoadData())
                    Response.Redirect("~/Default.aspx");

                //this.tipName.Visible =
                txtName.ReadOnly = true;
            }
        }
        private bool SetID()
        {
            int id = -1;
            if (Request.QueryString["Id"] != null)
            {
                if (int.TryParse(Request.QueryString["Id"].ToString(), out id))
                {
                    GroupID = id;
                    return true;

                }
            }
            return false;
        }
        private bool LoadData()
        {
            GroupsTableAdapter adapter = new GroupsTableAdapter();
            GroupsDataSet ds = new GroupsDataSet();
            adapter.Fill(ds.Groups, GroupID);
            if (ds.Groups.Count > 0)
            {
                GroupsDataSet.GroupsRow dr = ds.Groups[0];
                txtID.Text = dr.GroupID.ToString();
                txtName.Text = dr.Name;
                return true;
            }
            return false;


        }
        public String ToolTipMode()
        {
            return GroupEditorMode.View == this.CurrentGroupEditorMode ? "True" : "False";
        }
        #endregion

        #region property
        public GroupEditorMode CurrentGroupEditorMode
        {
            set
            {
                Session[string.Format("{0}-CurrentGroupEditorMode", ClientID)] = value;
            }
            get
            {
                if (Session[string.Format("{0}-CurrentGroupEditorMode", ClientID)] != null)
                    return (GroupEditorMode)Session[string.Format("{0}-CurrentGroupEditorMode", ClientID)];
                else
                    return GroupEditorMode.None;
            }
        }

        public int GroupID
        {
            set
            {                
                Session[string.Format("{0}-ID", ClientID)] = value;
            }
            get
            {
                if (Session[string.Format("{0}-ID", ClientID)] != null)
                    return (int)Session[string.Format("{0}-ID", ClientID)];
                else
                    return -1;
            }
        }
        #endregion

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            if (CurrentGroupEditorMode == GroupEditorMode.Add)
            {
                PACFD.Rules.Groups rules = new PACFD.Rules.Groups();
                GroupsDataSet ds = new GroupsDataSet();

                GroupsDataSet.GroupsRow dr = ds.Groups.NewGroupsRow();
                dr.Name = txtName.Text.Trim();
                dr.Active = true;

                ds.Groups.AddGroupsRow(dr);
                if (rules.Update(ds.Groups))
                {
                    //Response.Redirect("~/Groups/GroupList.aspx");
                    this.WebMessageBox1.Title = "Agregar Grupo";
                    this.WebMessageBox1.ShowMessage("El grupo ha sido agregado exitosamente,<br />¿Deseas agregar otro?",
                        System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
                }
                else
                {
                    LogManager.WriteStackTrace(new Exception("Error al tratar de agregar el grupo"));
                    this.WebMessageBox1.Title = "Agregar grupo";
                    WebMessageBox1.ShowMessage("Error al tratar de agregar el grupo", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }

            }
            else if (CurrentGroupEditorMode == GroupEditorMode.Edit)
            {

                PACFD.Rules.Groups rules = new PACFD.Rules.Groups();
                GroupsDataSet ds = new GroupsDataSet();
                rules.SelectByID(ds.Groups, GroupID);
                GroupsDataSet.GroupsRow dr = ds.Groups[0];
                dr.Name = txtName.Text.Trim();

                if (rules.Update(ds.Groups))
                {
                    Response.Redirect("~/Groups/GroupList.aspx");
                }
                else
                {
                    LogManager.WriteStackTrace(new Exception("Error al tratar de editar el grupo"));
                    this.WebMessageBox1.Title = "Editar grupo";
                    WebMessageBox1.ShowMessage("Error al tratar de editar el grupo", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }
            }
            else if (CurrentGroupEditorMode == GroupEditorMode.View)
            {
                Response.Redirect("~/Groups/GroupList.aspx");
            }
        }

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Yes)
                this.Response.Redirect(typeof(GroupAdd).Name + ".aspx");
        }

        protected void WebMessageBox1_OnCancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(GroupList).Name + ".aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/Groups/GroupList.aspx"));
        }
    }
}