﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
#endregion

namespace PACFDManager.Groups
{
    public partial class GroupList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GroupSearch1.Search += new GroupSearch.SearchEventHandler(GroupSearch_Search);
            
            if (!IsPostBack)
                this.GroupSearch1.BeginSearch();
        }

        void GroupSearch_Search(object sender, GroupsSearchEventArgs e)
        {
            if (e.DataGroups != null)
            {
                this.gvGroupList.DataSource = e.DataGroups.Groups;
                this.gvGroupList.DataBind();
            }            
        }

        protected void lbtnDetail_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;
            this.Response.Redirect(String.Format("~/Groups/GroupDetails.aspx?Id={0}", s.CommandArgument));
        }

        protected void lbtnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;
            this.Response.Redirect(String.Format("~/Groups/GroupModify.aspx?Id={0}", s.CommandArgument));
        }

        protected void lbtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;
            this.WebMessageBox1.Title = "Eliminar Producto";
            this.WebMessageBox1.ShowMessage("¿Seguro que desea eliminar este grupo?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
            this.ViewState["GroupID"] = s.CommandArgument;
        }

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Accept)
                return;

            if (this.ViewState["GroupID"] != null)
            {
                int id = -1;
                int.TryParse(this.ViewState["GroupID"].ToString(), out id);

                PACFD.Rules.Groups rules = new PACFD.Rules.Groups();
                PACFD.DataAccess.GroupsDataSet ds = new PACFD.DataAccess.GroupsDataSet();
                rules.SelectByID(ds.Groups, id);

                String BeginDataSet = ds.GetXml();
                String GroupName = String.Empty;

                if (ds.Groups.Count > 0)
                {
                    PACFD.DataAccess.GroupsDataSet.GroupsRow dr = ds.Groups[0];
                    GroupName = dr.Name;
                    dr.Delete();
                }
                if (!rules.Update(ds.Groups))
                {
                    LogManager.WriteStackTrace(new Exception("Error al tratar de eliminar el grupo"));
                    this.WebMessageBox1.Title = "Eliminar grupo";
                    WebMessageBox1.ShowMessage("Error al tratar de eliminar el grupo. <br />Verifica que este grupo no tenga ligada alguna empresa.", System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                    return;
                }
                else
                {
                    #region Add new entry to log system
                    PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} elimino el grupo {1} del sistema.",
                        this.UserName, GroupName), this.CurrentBillerID, BeginDataSet, ds.GetXml());
                    #endregion

                    this.ViewState["GroupID"] = null;
                    Response.Redirect("~/Groups/GroupList.aspx");
                }           
            }
        }
    }
}