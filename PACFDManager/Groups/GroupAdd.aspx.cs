﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace PACFDManager.Groups
{
    public partial class GroupAdd : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GroupEditor1.CurrentGroupEditorMode = GroupEditorMode.Add;
        }
    }
}