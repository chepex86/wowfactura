﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.DataAccess.GroupsDataSetTableAdapters;
using PACFD.Rules;


namespace PACFDManager.Groups
{
    public partial class GroupSearch : BaseUserControl
    {
        /// <summary>
        /// Delegate for the Search Event Handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments send by the object caller.</param>
        /// 
        public delegate void SearchEventHandler(object sender, GroupsSearchEventArgs e);
        /// <summary>
        /// Event handler for the concepts Search.
        /// </summary>
        public event SearchEventHandler Search;
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.BeginSearch();
            this.RegisterScriptsForProgressBar();
        }

        private void RegisterScriptsForProgressBar()
        {
            if (this.Page.ClientScript.IsStartupScriptRegistered(String.Format("LoadBindControls_{0}", this.Page.ClientID)))
                return;

            String scriptToRegister = String.Empty;
            String uProgressByRFC = String.Empty;

            uProgressByRFC = String.Format("jsGroup.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtName_AutoCompleteExtender.BehaviorID, this.spnByName.ClientID, true);

            scriptToRegister = "Sys.Application.add_load(function() {";
            scriptToRegister += String.Format("{1}{0}{1}", uProgressByRFC, System.Environment.NewLine);
            scriptToRegister += "});";

            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), String.Format("LoadBindControls_{0}", this.Page.ClientID), scriptToRegister, true);
        }

        public void BeginSearch()
        {
            GroupsDataSet ds = new GroupsDataSet();
            PACFD.Rules.Groups rules = new PACFD.Rules.Groups();
            string name = null;

            if (this.txtName.Text.Trim() != String.Empty)
                name = this.txtName.Text.Trim();

            rules.SelectBySearching(ds.Groups, name, true);
            GroupsSearchEventArgs args = new GroupsSearchEventArgs(ds);
            OnSearch(args);
        }

        protected void OnSearch(GroupsSearchEventArgs e)
        {
            if (!this.Search.IsNull())
                this.Search(this, e);
        }
    }
}
