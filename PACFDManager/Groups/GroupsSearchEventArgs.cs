﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Groups
{
    public class GroupsSearchEventArgs : EventArgs 
    {
        
        
        public PACFD.DataAccess.GroupsDataSet  DataGroups { get; private set; }



        public GroupsSearchEventArgs( PACFD.DataAccess.GroupsDataSet dsGroups)
        {
            this.DataGroups = dsGroups;
      
        }
    }
}
