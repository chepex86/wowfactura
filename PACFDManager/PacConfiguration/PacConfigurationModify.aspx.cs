﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
#endregion

namespace PACFDManager.PacConfiguration
{
    public partial class PacConfigurationModify : PACFDManager.BasePage
    {
        const string MESSAGE_ERROR = "MESSAGE_ERROR";
        const string MESSAGE_UPDATE = "MESSAGE_UPDATE";

        public int PacConfigurationID
        {
            get
            {
                int r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                { return 0; }

                return !int.TryParse(o.ToString(), out r) ? 0 : r;
            }
            set { this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value; }
        }

        public string PassWord
        {
            get
            {
                string r;
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return string.Empty;

                r = o.ToString().Trim();

                return r;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }

        private void MaintainValuePassword()
        {
            this.txtPassword.Attributes.Add("value", PassWord);
            this.txtRepassword.Attributes.Add("value", PassWord);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            { return; }

            this.DataBind();
            this.MaintainValuePassword();
        }

        public override void DataBind()
        {
            ListItem l;
            base.DataBind();

            using (PACFD.DataAccess.PacConfigurationDataSet.GetByBillerIDDataTable table =
                (new PACFD.Rules.PacConfiguration()).SelectByBillerID(this.CurrentBillerID))
            {
                if (table.Count < 1)
                { return; }

                this.txtFileType.Text = table[0].FileType;
                this.txtUrl.Text = table[0].Url;
                this.txtUrlCancelation.Text = !table[0].IsUrlCancellationNull() ? table[0].UrlCancellation : string.Empty;
                this.txtToken.Text = !table[0].IsTokenNull() ? table[0].Token : string.Empty;

                this.txtUsername.Text = table[0].Username;
                this.ckbDisabled.Checked = table[0].Active;
                this.txtRepassword.Text =
                    this.txtPassword.Text = PassWord = Cryptography.DecryptUnivisitString(table[0].Password);
                this.ddlType.ClearSelection();

                if ((l = this.ddlType.Items.FindByValue(table[0].Type.ToString())) != null)
                { l.Selected = true; }
                else
                { this.ddlType.SelectedIndex = 0; }

                //this.txtType.Text = table[0].Type.ToString();
                this.PacConfigurationID = table[0].PACConfigurationID;
            }

            this.SetEnable(this.ckbDisabled.Checked);
        }

        void SetEnable(bool enable)
        {
            this.txtFileType.ReadOnly =
                this.txtPassword.ReadOnly =
                this.txtPassword.ReadOnly =
                this.txtRepassword.ReadOnly =
                this.txtToken.ReadOnly =
                //this.txtType.ReadOnly =                
                this.txtUrl.ReadOnly =
                this.txtUsername.ReadOnly = !enable;

            /*if (!enable)
            {
                
                this.txtFileType.Text =
                   this.PassWord =
                   this.txtPassword.Text =
                   this.txtPassword.Text =
                   this.txtRepassword.Text =
                    //this.txtType.Text =
                   this.txtUrl.Text =
                   this.txtUsername.Text = string.Empty;
                this.MaintainValuePassword();
                this.ddlType.SelectedIndex = 0;
            }*/

            this.ddlType.Enabled =
            this.txtFileType_RequiredFieldValidator.Enabled =
                this.txtPassword_RequiredFieldValidator.Enabled =
                this.txtRepassword_CompareValidator.Enabled =
                this.txtRepassword_RequiredFieldValidator.Enabled =
                //this.txtType_RangeValidator.Enabled =
                //this.txtType_RequiredFieldValidator.Enabled =
                this.txtUrl_RequiredFieldValidator.Enabled =
                this.txtUrlCancelation_RequiredFieldValidator.Enabled =
                this.txtUsername_RequiredFieldValidator.Enabled =
                //this.txtType_RangeValidator.Enabled =
                //this.txtType_RequiredFieldValidator.Enabled = 
                this.ddlType_RequiredFieldValidator.Enabled =
                enable;
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            bool added = false;

            if (this.txtUrl.Text.Trim().Length < 6 || !this.IsUrlValid())
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("La dirección url no es valida.", true, WebMessageBoxButtonType.Accept);
                return;
            }

            if (this.txtUrlCancelation.Text.Trim().Length < 6 || !this.IsUrlCancelationValid())
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("La dirección url de cancalación no es valida.", true, WebMessageBoxButtonType.Accept);
                return;
            }

            if (!this.Page.IsValid)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.ShowMessage("La pagina tiene campos con errores.", true, WebMessageBoxButtonType.Accept);
                return;
            }

            using (PACFD.DataAccess.PacConfigurationDataSet.PacConfigurationDataTable table =
                (new PACFD.Rules.PacConfiguration()).SelectByID(this.PacConfigurationID))
            {
                if (table.Count < 1)
                {
                    table.AddPacConfigurationRow(string.Empty, string.Empty, string.Empty, string.Empty, 0, true, 0, string.Empty,string.Empty);
                    added = true;
                }

                table[0].PACConfigurationID = this.PacConfigurationID;
                table[0].BillerID = this.CurrentBillerID;
                table[0].Url = this.txtUrl.Text.Trim();
                table[0].UrlCancellation = this.txtUrlCancelation.Text.Trim();
                table[0].Token = this.txtToken.Text.Trim();

                table[0].Username = this.txtUsername.Text;
                table[0].Password = Cryptography.EncryptUnivisitString(this.txtPassword.Text);
                table[0].FileType = this.txtFileType.Text;
                table[0].Type = int.Parse(this.ddlType.SelectedIndex < -1 ? "1" : this.ddlType.SelectedItem.Value);
                table[0].Active = this.ckbDisabled.Checked;
                table[0].AcceptChanges();

                if (added)
                { table[0].SetAdded(); }
                else
                { table[0].SetModified(); }

                if (!(new PACFD.Rules.PacConfiguration()).Update(table))
                {
                    this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                    this.WebMessageBox1.ShowMessage("No se pudo configurar del PAC", true, WebMessageBoxButtonType.Accept);
                    return;
                }
            }

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} actualizo la configuración del PAC con id={1}.",
                this.UserName, this.PacConfigurationID), this.CurrentBillerID, null, null);
            #endregion

            this.WebMessageBox1.CommandArguments = MESSAGE_UPDATE;
            this.WebMessageBox1.ShowMessage("Configuración del PAC actualizada con éxito.", false, WebMessageBoxButtonType.Accept);
        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                case MESSAGE_ERROR:
                    break;
                case MESSAGE_UPDATE:
                    this.Response.Redirect("~/Default.aspx");
                    break;
            }

            this.WebMessageBox1.CommandArguments = string.Empty;
        }

        protected void ckbDisabled_CheckedChanged(object sender, EventArgs e)
        { this.SetEnable(this.ckbDisabled.Checked); }

        protected void btnCancel_Click(object sender, EventArgs e)
        { this.Response.Redirect("~/Default.aspx"); }

        bool IsUrlValid()
        {
            Uri u;
            return Uri.TryCreate(this.txtUrl.Text, UriKind.RelativeOrAbsolute, out u);
        }

        bool IsUrlCancelationValid()
        {
            Uri u;
            return Uri.TryCreate(this.txtUrlCancelation.Text, UriKind.RelativeOrAbsolute, out u);
        }
    }
}