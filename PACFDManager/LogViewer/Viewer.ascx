﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Viewer.ascx.cs" Inherits="PACFDManager.LogViewer.Viewer" %>
<br />
<style type="text/css">
    .AspNet-GridView table tbody tr.AspNet-GridView-Alternate td
    {
        background: inherit;
    }
</style>
<div class="form">
    <div class="formStyles">
        <asp:GridView ID="gvContent" runat="server" OnRowDataBound="gvContent_OnRowDataBound"
            CssClass="fail" AutoGenerateColumns="False" BorderStyle="None" EnableTheming="True"
            ShowHeader="False" UseAccessibleHeader="False" AllowPaging="True" AllowSorting="True"
            EnableSortingAndPagingCallbacks="True" OnPageIndexChanging="gvContent_PageIndexChanging">
            <RowStyle BackColor="White" BorderColor="White" BorderStyle="None" CssClass="fail" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    Fecha:
                                </label>
                                <span>
                                    <%#FormatDate(Eval("ModificationDate").ToString())%>
                                </span></li>
                            <li class="left">
                                <label class="desc">
                                    Usuario:
                                </label>
                                <span>
                                    <%#Eval("UserName")%>
                                </span></li>
                        </ul>
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    <span id="spnBefore" runat="server">Antes:</span>
                                </label>
                                <span>
                                    <input type="checkbox" runat="server" id="cbxBeforeXML" /></span> </li>
                            <li class="left">
                                <label class="desc">
                                    <span id="spnAfter" runat="server">Despues:</span></label><span>
                                        <input type="checkbox" runat="server" id="cbxAfterXML" /></span></li>
                            <li class="left">
                                <label class="desc">
                                    Descripción:
                                </label>
                                <span>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>' />
                                </span></li>
                        </ul>
                        <div id="divBeforeXML" runat="server" style="width: 900px; border: solid 1px #CCC;">
                            <h4>
                                <label class="desc">
                                    &nbsp;&nbsp;Antes:
                                </label>
                            </h4>
                            <pre id="preBefore" runat="server" class="brush: xml;">  
<%#FormatText(Eval("DataSetBefore").ToString())%>
                        </pre>
                        </div>
                        <br />
                        <div id="divAfterXML" runat="server" style="width: 900px; border: solid 1px #CCC;">
                            <h4>
                                <label class="desc">
                                    &nbsp;&nbsp;Despues:
                                </label>
                            </h4>
                            <pre id="preAfter" runat="server" class="brush: xml;">
<%#FormatText(Eval("DataSetAfter").ToString())%>
                        </pre>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle BorderStyle="None" />
            <SelectedRowStyle BackColor="White" BorderColor="White" BorderStyle="None" CssClass="fail" />
            <EditRowStyle BackColor="White" BorderColor="White" BorderStyle="None" CssClass="fail"
                Wrap="False" />
        </asp:GridView>
    </div>
</div>
