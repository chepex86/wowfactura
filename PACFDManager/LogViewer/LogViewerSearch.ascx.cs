﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Rules;
using PACFD.Common;
using PACFD.DataAccess;
using System.Data;
using System.Web.UI.HtmlControls;
#endregion

namespace PACFDManager.LogViewer
{
    public partial class LogViewerSearch : BaseUserControl
    {
        public delegate void SearchEventHandler(object sender, LogViewerEventArgs e);
        public event SearchEventHandler Search;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadUsers();
                this.LoadBillers();
                this.txtByDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                //this.cbxDisabledDate.Attributes["onclick"] =
                //    String.Format("javascript:jsLogViewer.DisabledDate('{0}');", this.cbxDisabledDate.ClientID);
            }

            this.txtByDate_CalendarExtender.Format = "dd/MM/yyyy";
        }

        protected void OnSearch(LogViewerEventArgs e)
        {
            if (!this.Search.IsNull())
            { this.Search(this, e); }
        }

        private bool LoadUsers()
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet.Users_GetBySearchingDataTable dt = new UsersDataSet.Users_GetBySearchingDataTable();

            DataTable table = new DataTable();
            DataRow row;

            table.Columns.Add("UserName");
            table.Columns.Add("UserID");

            try
            {
                dt = user.SelectUserBySearching(null, null);

                row = table.NewRow();
                row["UserName"] = "[Todos]";
                row["UserID"] = "-1";
                table.Rows.Add(row);

                for (int i = 0; i < dt.Count; i++)
                {
                    row = table.NewRow();
                    row["UserName"] = dt[i].UserName;
                    row["UserID"] = dt[i].UserID;
                    table.Rows.Add(row);
                }

                this.ddlByUserName.Items.Clear();

                this.ddlByUserName.DataTextField = "UserName";
                this.ddlByUserName.DataValueField = "UserID";

                this.ddlByUserName.DataSource = table;
                this.ddlByUserName.DataBind();

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        protected bool LoadBillers()
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.SelectBillersAllDataTable dt;

            DataTable table = new DataTable();
            DataRow row;

            table.Columns.Add("BillerName");
            table.Columns.Add("BillerID");

            try
            {
                dt = biller.SelectAll();

                row = table.NewRow();
                row["BillerName"] = "[Todos]";
                row["BillerID"] = "0";
                table.Rows.Add(row);

                row = table.NewRow();
                row["BillerName"] = "Sin empresa";
                row["BillerID"] = "-1";
                table.Rows.Add(row);

                for (int i = 0; i < dt.Count; i++)
                {
                    row = table.NewRow();
                    row["BillerName"] = dt[i].Name;
                    row["BillerID"] = dt[i].BillerID;
                    table.Rows.Add(row);
                }

                this.ddlByBillerName.Items.Clear();

                this.ddlByBillerName.DataTextField = "BillerName";
                this.ddlByBillerName.DataValueField = "BillerID";

                this.ddlByBillerName.DataSource = table;
                this.ddlByBillerName.DataBind();

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime? date = null;
            DateTime d;
            String description = null;

            int? userid = null;
            int? billerid = null;

            bool? before = null;
            bool? after = null;

            if (this.ddlBefore.SelectedItem.Value != "-1")
                before = this.ddlBefore.SelectedItem.Value == "1" ? true : false;

            if (this.ddlAfter.SelectedItem.Value != "-1")
                after = this.ddlAfter.SelectedItem.Value == "1" ? true : false;

            if (!String.IsNullOrEmpty(this.txtByDescription.Text.Trim()))
                description = this.txtByDescription.Text.Trim();

            if (this.cbxDisabledDate.Checked && !String.IsNullOrEmpty(this.txtByDate.Text.Trim()))
                date = DateTime.TryParse(this.txtByDate.Text.Trim(), out d) ? d : (DateTime?)null;

            if (this.ddlByUserName.SelectedItem.Value != "-1")
                userid = Convert.ToInt32(this.ddlByUserName.SelectedItem.Value);

            if (this.ddlByBillerName.SelectedItem.Value != "0")
                billerid = Convert.ToInt32(this.ddlByBillerName.SelectedItem.Value);

            LogViewerEventArgs a = new LogViewerEventArgs(date, userid, null, billerid, description, before, after);

            this.OnSearch(a);
        }

        protected void cbxDisabledDate_OnCheckedChanged(Object sender, EventArgs e)
        {
            this.liDate.Style["display"] = this.cbxDisabledDate.Checked ? "block" : "none";                
        }
    }
}