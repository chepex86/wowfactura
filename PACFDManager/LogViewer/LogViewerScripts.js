﻿var jsLogViewer = new jclassLogViewer();

function jclassLogViewer() {
    this.ShowHide = function(check, element) {
        var e = document.getElementById(element);
        var c = document.getElementById(check);

        e.style.display = c.checked ? 'block' : 'none';
    }

    this.DisabledDate = function(val) {
        var li = document.getElementById("liDate");
        var cBox = document.getElementById(val);
        alert(cBox.checked);
        li.style.display = cBox.checked ? 'block' : 'none';
    }
}