﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

namespace PACFDManager.LogViewer
{
    public partial class Viewer : BaseUserControl
    {
        public Object DataSource 
        {
            set
            {
                this.gvContent.DataSource = value;
                this.Session["grid-source"] = value;
            }
        }

        public override void DataBind()
        { 
            this.gvContent.DataBind();
            this.gvContent.Visible = this.gvContent.Rows.Count < 1 ? false : true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterScripts();
        }

        private void RegisterScripts()
        {
            String script = String.Empty;

            script += "<script src='../Includes/SyntaxHighLighter/scripts/shCore.js' type='text/javascript'></script>";
            script += "<script src='../Includes/SyntaxHighLighter/scripts/shBrushXml.js' type='text/javascript'></script>";
            script += "<link href='../Includes/SyntaxHighLighter/styles/shThemeEclipse.css' rel='stylesheet' type='text/css' />";
            script += "<link href='../Includes/SyntaxHighLighter/styles/shCore.css' rel='stylesheet' type='text/css' />";
            script += "<script type='text/javascript'>";
            script += "SyntaxHighlighter.all();";
            script += "</script>";

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "script_css", script, false);
        }

        protected String FormatText(String text)
        {
            return "\n\r" + text.Replace("<", "&#60;").Replace(">", "&#62;").Trim();
        }

        protected String FormatDate(String date)
        {
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
        }

        protected void gvContent_OnRowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex < 0)
                return;

            HtmlControl cbxBefore = e.Row.FindControl("cbxBeforeXML") as HtmlControl;
            HtmlControl cbxAfter = e.Row.FindControl("cbxAfterXML") as HtmlControl;

            HtmlControl divBefore = e.Row.FindControl("divBeforeXML") as HtmlControl;
            HtmlControl divAfter = e.Row.FindControl("divAfterXML") as HtmlControl;

            HtmlControl spnBefore = e.Row.FindControl("spnBefore") as HtmlControl;
            HtmlControl spnAfter = e.Row.FindControl("spnAfter") as HtmlControl;

            HtmlGenericControl preBefore = e.Row.FindControl("preBefore") as HtmlGenericControl;
            HtmlGenericControl preAfter = e.Row.FindControl("preAfter") as HtmlGenericControl;

            if (cbxBefore.IsNull() || cbxAfter.IsNull() || divBefore.IsNull() || spnAfter.IsNull() ||
                spnBefore.IsNull() || divAfter.IsNull() || preBefore.IsNull() || preAfter.IsNull()) return;

            if (String.IsNullOrEmpty(preBefore.InnerHtml.Replace(System.Environment.NewLine, String.Empty).Trim().ToString()))
                cbxBefore.Visible = spnBefore.Visible = divBefore.Visible = false;
            else
                cbxBefore.Attributes["onclick"] = String.Format("javascript:jsLogViewer.ShowHide('{0}', '{1}')", cbxBefore.ClientID, divBefore.ClientID);

            if (String.IsNullOrEmpty(preAfter.InnerHtml.Replace(System.Environment.NewLine, String.Empty).Trim().ToString()))
                cbxAfter.Visible = spnAfter.Visible = divAfter.Visible = false;
            else
                cbxAfter.Attributes["onclick"] = String.Format("javascript:jsLogViewer.ShowHide('{0}', '{1}')", cbxAfter.ClientID, divAfter.ClientID);

            divBefore.Style["display"] = divAfter.Style["display"] = "none";
        }

        protected void gvContent_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            this.gvContent.PageIndex = e.NewPageIndex;
            this.gvContent.DataSource = this.Session["grid-source"] as Object;
            this.gvContent.DataBind(); 
        }
    }
}