﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.Rules;
using PACFD.DataAccess;
#endregion

namespace PACFDManager.LogViewer
{
    public partial class LogViewer : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                this.LoadCurrentlyLogs();

            this.ucSearch.Search += new LogViewerSearch.SearchEventHandler(ucSearch_Search);
        }

        void ucSearch_Search(object sender, LogViewerEventArgs e)
        {
            this.SearchLogs(e);
        }

        private void LoadCurrentlyLogs()
        {
            PACFDLog log = new PACFDLog();

            this.ucViewer.DataSource =
                    log.GetLogsByFilter(null, null, DateTime.Now, null, null, null);

            this.ucViewer.DataBind();
        }

        private bool SearchLogs(LogViewerEventArgs e)
        {
            PACFDLog log = new PACFDLog();

            try
            {
                this.ucViewer.DataSource = 
                    log.GetLogsByFilter(e.UserID, e.BillerID, e.Date, e.Description, e.Before, e.After);

                this.ucViewer.DataBind();

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}