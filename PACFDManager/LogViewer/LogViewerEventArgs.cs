﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace PACFDManager.LogViewer
{
    public class LogViewerEventArgs : EventArgs
    {
        public DateTime? Date { get; private set; }
        public int? UserID { get; private set; }
        public int? GroupID { get; private set; }
        public int? BillerID { get; private set; }
        public String Description { get; private set; }
        public bool? Before { get; private set; }
        public bool? After { get; private set; }

        public LogViewerEventArgs(DateTime? date, int? userid, int? groupid, int? billerid, String description, bool? before, bool? after)
        {
            this.Date = date;
            this.UserID = userid;
            this.GroupID = groupid;
            this.BillerID = billerid;
            this.Description = description;
            this.Before = before;
            this.After = after;
        }
    }
}