﻿------------------------------------------------
--********  CANCELACION CFDI 4.0  ************--
------------------------------------------------

USE [PACFD]
GO

/****** Object:  Table [dbo].[BillingCancelationDetail]    Script Date: 09/02/2023 10:11:52 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BillingCancelationDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BillingID] [int] NOT NULL,
	[NewUUID] [nvarchar](50) NULL,
	[Reason] [nchar](2) NULL,
	[RequestDate] [datetime] NOT NULL,
	[ResponseDate] [datetime] NULL,
	[Status] [tinyint] NOT NULL,
	[Notes] [nvarchar](500) NULL,
 CONSTRAINT [PK_BillingCancelationDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[BillingCancelationDetail]  WITH CHECK ADD  CONSTRAINT [FK_BillingCancelationDetail_Billings] FOREIGN KEY([BillingID])
REFERENCES [dbo].[Billings] ([BillingID])
GO

ALTER TABLE [dbo].[BillingCancelationDetail] CHECK CONSTRAINT [FK_BillingCancelationDetail_Billings]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'01
Comprobante emitido con errores con relación.
02
Comprobante emitido con errores sin relación.
03
No se llevó a cabo la operación.
04
Operación nominativa relacionada en una factura global.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingCancelationDetail', @level2type=N'COLUMN',@level2name=N'Reason'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'200, Cancelado
201, En proceso
202, Previemente Cancelado
203, No Encontrado o no corresponde al emisor
204, No Aplicable para cancelacion
205, No Existe
206, No corresponde a un sector primario
102, Plazo vencido
103, Solicitud rechazada
104, Cancelado sin aceptación
105, Cancelado con aceptación
106, Error en UUID de sustitución.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingCancelationDetail', @level2type=N'COLUMN',@level2name=N'Status'
GO

USE [PACFD]
GO
/****** Object:  StoredProcedure [dbo].[spBillingsAllCustomer_GetByID]    Script Date: 09/02/2023 10:40:48 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Procedure

ALTER PROCEDURE [dbo].[spBillingsAllCustomer_GetByID] (
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int
	
	
--///[Billings]		
	SELECT
		*
	FROM 
		[Billings]
	WHERE
		([BillingID] = @BillingID)
		
--///BillingsBillers
	SELECT
		bb.*
		, br.Email
	FROM 
		BillingsBillers bb
	INNER JOIN Billings bi ON bi.BillingID = @BillingID
	INNER JOIN Billers br ON br.BillerID = bi.BillerID 
	WHERE
		(bb.[BillingID] = @BillingID)
--///BillingsDetails		
	SELECT
		*
	FROM 
		BillingsDetails
	WHERE
		([BillingID] = @BillingID)		
--///BillingsIssued		
	SELECT
		*
	FROM 
		BillingsIssued
	WHERE
		([BillingID] = @BillingID)						
--///BillingsReceptors		
	SELECT
		*
	FROM 
		BillingsReceptors
	WHERE
		([BillingID] = @BillingID)		
--///Taxes		
	SELECT
		*
	FROM 
		Taxes
	WHERE
		([BillingID] = @BillingID)		
--///TransferTaxes		
	SELECT
		*
	FROM 
		TransferTaxes
	WHERE
		([BillingID] = @BillingID)	
--///DetainedTaxes		
	SELECT
		*
	FROM 
		DetainedTaxes
	WHERE
		([BillingID] = @BillingID)	
		
--///BillingFiscalRegime		
	SELECT
		*
	FROM 
		BillingFiscalRegime
	WHERE
		([BillingID] = @BillingID)
		

--///BillingsDetailNotes
	SELECT
		*
	FROM 
		BillingsDetailNotes
	WHERE
		([BillingID] = @BillingID)

--///billing fields
	SELECT
		*
	FROM 
		[dbo].[BillingFields]
	WHERE
		([BillingID] = @BillingID)

--///billing GeneralPeriods
	SELECT
		*
	FROM 
		[dbo].[BillingGeneralPeriod]
	WHERE
		([BillingID] = @BillingID)

		
--///billing CancellationDetail
	SELECT
		*
	FROM 
		[dbo].[BillingCancelationDetail]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END


GO



USE [Unifacturadigital]
GO

/****** Object:  Table [dbo].[CatMotivosCancelacion]    Script Date: 09/02/2023 10:16:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatMotivosCancelacion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[c_Motivo] [nchar](2) NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_CatMotivosCancelacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

insert into [CatMotivosCancelacion] (
	[c_Motivo],
	[Descripcion])
	values ('01','Comprobantes emitidos con errores con relación.')
GO
insert into [CatMotivosCancelacion] (
	[c_Motivo],
	[Descripcion])
	values ('02','Comprobantes emitidos con errores sin relación.')
GO
insert into [CatMotivosCancelacion] (
	[c_Motivo],
	[Descripcion])
	values ('03','No se llevó a cabo la operación.')
GO
insert into [CatMotivosCancelacion] (
	[c_Motivo],
	[Descripcion])
	values ('04','Operación nominativa relacionada en una factura global.')
GO