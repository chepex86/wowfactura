﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
#endregion

namespace PACFDManager
{
    public partial class News : PACFDManager.BaseUserControl
    {
        protected void Page_Load(Object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            
            LoadNews();
        }
        private void LoadNews()
        {
            DataSet dsNew = GetXMLDataSet("http://www.sat.gob.mx/sitio_internet/NovedadesSAT.rss");

            if (dsNew == null)
                return;

            if (dsNew.Tables.IndexOf("News") == -1)
                return;

            DateTime date = new DateTime();
            String title = String.Empty;
            String link = String.Empty;
            String description = String.Empty;
            String news = String.Empty;

            int max = 0;
            const int maxnews = 12;

            foreach (DataRow dr in dsNew.Tables["News"].Rows)
            {
                max++;

                if (max > maxnews)
                    break;

                if (!dr.IsNull("PublicationDate"))
                    date = DateTime.Parse(dr["PublicationDate"].ToString());
                if (!dr.IsNull("Title"))
                    title = dr["Title"].ToString();
                if (!dr.IsNull("Link"))
                    link = dr["Link"].ToString();
                if (!dr.IsNull("Description"))
                    description = dr["Description"].ToString();
                
                news += String.Format(@"
                            <h3>{0} - {1}</h3>
                            <span>{2}</span><br>   
                            <a href=""{3}"" target=""_blank"">Mas detalle</a><p />"
                    , date.ToString("yyyy/MM/dd")
                    , title
                    , description
                    , link);
            }

            this.ltrNews.Text = news;
        }

        public static DataSet GetXMLDataSet(String URL)
        {
            using (DataSet ds = new DataSet())
            {
                PACFD.Rules.News news = new PACFD.Rules.News();
                ds.Tables.Add(news.GetAllNews());
                return ds;
            }
            #region ...
            /*try
              {
                  HttpWebRequest xmlRequest = (HttpWebRequest)WebRequest.Create(URL);
                  WebResponse xmlResponse = xmlRequest.GetResponse();
                  Stream responseStream = xmlResponse.GetResponseStream();
                  DataSet xmlData = new DataSet();
                  xmlData.ReadXml(responseStream);
                  return xmlData;
              }
              catch (Exception ex)
              {
                  LogManager.WriteWarning(ex);
                  return null;
              }   */
            #endregion
        }
    }
}