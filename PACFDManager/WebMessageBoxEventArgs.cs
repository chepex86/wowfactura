﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager
{
    /// <summary>
    /// 
    /// </summary>
    public class WebMessageBoxEventArgs : EventArgs
    {
        /// <summary>
        /// Result returned by the dialog box.
        /// </summary>
        public WebMessageBoxDialogResultType DialogResult { get; set; }
        /// <summary>
        /// The type of buttons used in the dialog box.
        /// </summary>
        public WebMessageBoxButtonType Buttons { get; set; }
        /// <summary>
        /// Get a string with additional arguments sended.
        /// </summary>
        public string CommandArguments { get; private set; }

        /// <summary>
        /// Create a new instance of the class.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="buttons"></param>
        public WebMessageBoxEventArgs(WebMessageBoxDialogResultType result, WebMessageBoxButtonType buttons, string commandargs)
        {
            this.DialogResult = result;
            this.Buttons = buttons;
            this.CommandArguments = commandargs;
        }
    }
}
