﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager
{
    /// <summary>
    /// Type of button to be display in the dialog
    /// </summary>
    public enum WebMessageBoxButtonType
    {
        AcceptCancel = 1,
        YesNo,
        ContinueIgnore,
        Accept
    }

    /// <summary>
    /// Type of answer
    /// </summary>
    public enum WebMessageBoxDialogResultType
    {
        Accept = 1,
        Cancel,
        Yes,
        No,
        Continue,
        Ignore
    }
    /// <summary>
    /// The type of dialog box 
    /// </summary>
    public enum WebMessageBoxDialogType
    {
        Normal,
        Warning,
        Stop,
        Question,
        Error
    }
}
