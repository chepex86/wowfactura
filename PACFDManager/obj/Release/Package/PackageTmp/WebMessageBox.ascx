﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebMessageBox.ascx.cs"
    Inherits="PACFDManager.WebMessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="udpMessages" runat="server" Visible="false" RenderMode="Inline">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server">
            <div class="ccPopup">
                <div class="ccPopupClose">
                    &nbsp;
                </div>
                <div class="ccPopupTitle">
                    <asp:Label ID="lblTitle" runat="server" Text="Mensaje"></asp:Label></div>
                <div class="<%="ccPopupQuestionIcon"%>">
                    <%//NOTA: ccPopupInfoIcon ccPopupErrorIcon ccPopupWarningIcon posible iconos, pueden cambiar la clase con una variable publica %>
                    &nbsp;</div>
                <div class="ccPopupMessage">
                    <asp:Label ID="lblMessages" runat="server"></asp:Label></div>
                <table id="tbExchangeRate" runat="server" visible="false">
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblTitleLatestExchangeRate" runat="server" Text="Ultima revisión del tipo de cambio:" />
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLatestExchangeRate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblTitleExchangeRate" runat="server" Text="Tipo de cambio:" />
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtExchangeRate" runat="server" Width="80px" />
                        </td>
                    </tr>
                </table>
                <div style="text-align: center; width: 100%; padding: 5px 0; clear: both;">
                    <asp:Button ID="btnAceptar" runat="server" CssClass="ccPopupButton" OnClick="btnAceptar_Click"
                        Text="Aceptar" CausesValidation="False" />
                    <asp:Button ID="btnCancelar" runat="server" CssClass="ccPopupButton" OnClick="btnCancelar_Click"
                        Text="NO" Visible="False" CausesValidation="False" />
                </div>
            </div>
            <!--
            QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  
            <asp:Image ID="imgIcon" runat="server" ImageUrl="~/Includes/Images/deleteIcon.jpg" />
            QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  QUITAR ESTO  -->
        </asp:Panel>
        <asp:ModalPopupExtender BehaviorID="ModalBehaviour" ID="mpeMessages" runat="server"
            BackgroundCssClass="popupbackground" PopupControlID="Panel1" TargetControlID="Panel1">
        </asp:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<%--<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebMessageBox.ascx.cs"
    Inherits="PACFDManager.WebMessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="udpMessages" runat="server" Visible="false" RenderMode="Inline">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server">
            <table class="WebMessageBoxTable" border="0" cellpadding="5" cellspacing="0" style="width: 300px">
                <tr>
                    <td class="Titulo" style="height: 21px; text-align: center">
                        <asp:Label ID="lblTitle" runat="server" Text="Mensaje"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: center">
                        <table style="text-align: center;">
                            <tr>
                                <td>
                                    <asp:Image ID="imgIcon" runat="server" ImageUrl="~/Includes/Images/deleteIcon.jpg" />
                                </td>
                                <td>
                                    <asp:Label ID="lblMessages" runat="server" Font-Bold="True" Font-Size="Small"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: center">
                        <asp:Button ID="btnAceptar" runat="server" CssClass="Button" OnClick="btnAceptar_Click"
                            Text="Aceptar" CausesValidation="False" />
                        <asp:Button ID="btnCancelar" runat="server" CssClass="Button" OnClick="btnCancelar_Click"
                            Text="Cancelar" Visible="False" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:ModalPopupExtender ID="mpeMessages" runat="server" BackgroundCssClass="popupbackground"
            PopupControlID="Panel1" TargetControlID="Panel1">
        </asp:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
--%>