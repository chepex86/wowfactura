﻿<%@ Page Title="Listado de Folios" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="SeriesList.aspx.cs" Inherits="PACFDManager.Series.SeriesList" %>

<%@ Register Src="SeriesEditor.ascx" TagName="SeriesEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:SeriesEditor ID="ucSeriesEditor" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
