﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="TestCss.aspx.cs" Inherits="PACFDManager.TestCss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <table border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td colspan="2">
                Telefonos:
            </td>
        </tr>
        <tr>
            <td align="right" style="float:right;text-align:right" >
                Sin costo en M&eacute;xico:
            </td>
            <td align="right">
                01 800 8367570
            </td>
        </tr>
        <tr>
             <td align="right">
                Desde U.S.A :
            </td>
             <td align="right">
                1877 4371714
            </td>
        </tr>
        <tr>
            <td align="right">
                Resto del Mundo:
             <td align="right">
           
                314 33 55951
            </td>
        </tr>
    </table>
    <div id="breadcrumb">
        <a href="homepage.cfm">Home</a> / <a href="devtodo">Section Name</a> / <strong>Page
            Name</strong>
    </div>
    <img id="Img1" title="HOLA MUNDO!" class="vtip" runat="server" src="~/Includes/Images/png/questionIcon.png"
        alt="Ayuda" />
    <h1>
        Heading 1 Consect etuer adipisci ngon.</h1>
    <h2>
        Heading 2 Consect etuer adipisci ngon.</h2>
    <h3>
        Heading 3 Consect etuer adipisci ngon.</h3>
    <h4>
        Heading 4 Consect etuer adipisci ngon.</h4>
    <h5>
        Heading 5 Consect etuer adipisci ngon.</h5>
    <h6>
        Heading 6 Consect etuer adipisci ngon.</h6>
    <hr />
    <p>
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
    </p>
    <hr />
    <p class="highlight">
        A paragraph of text with class="highlight", A paragraph of text with class="highlight",
        A paragraph of text with class="highlight", A paragraph of text with class="highlight",
        A paragraph of text with class="highlight",
    </p>
    <hr />
    <p class="subdued">
        A paragraph of text with class="subdued", A paragraph of text with class="subdued",
        A paragraph of text with class="subdued", A paragraph of text with class="subdued",
        A paragraph of text with class="subdued",
    </p>
    <hr />
    <p class="error">
        A paragraph of text with class="error", A paragraph of text with class="error",
        A paragraph of text with class="error", A paragraph of text with class="error",
        A paragraph of text with class="error",
    </p>
    <hr />
    <p class="success">
        A paragraph of text with class="success", A paragraph of text with class="success",
        A paragraph of text with class="success", A paragraph of text with class="success",
        A paragraph of text with class="success",
    </p>
    <hr />
    <p class="caption">
        A paragraph of text with class="caption", A paragraph of text with class="caption",
        A paragraph of text with class="caption", A paragraph of text with class="caption",
        A paragraph of text with class="caption",
    </p>
    <hr />
    <p>
        <small>A paragraph of text with the text with &lt;small&gt; tags, A paragraph of text
            with the text with &lt;small&gt; tags, A paragraph of text with the text with &lt;small&gt;
            tags, A paragraph of text with the text with &lt;small&gt; tags, A paragraph of
            text with the text with &lt;small&gt; tags, </small>
    </p>
    <hr />
    <p>
        <strong>A paragraph of text with the text with &lt;strong&gt; tags, A paragraph of text
            with the text with &lt;strong&gt; tags, A paragraph of text with the text with &lt;strong&gt;
            tags, A paragraph of text with the text with &lt;strong&gt; tags, A paragraph of
            text with the text with &lt;strong&gt; tags, </strong>
    </p>
    <hr />
    <div class="featurebox">
        <h3>
            A h3 level heading inside a "featurebox" div</h3>
        <p>
            A normal paragraph of text within a div with a class="featurebox", A normal paragraph
            of text within a div with a class="featurebox", A normal paragraph of text within
            a div with a class="featurebox", A normal paragraph of text within a div with a
            class="featurebox", A normal paragraph of text within a div with a class="featurebox",
            A normal paragraph of text within a div with a class="featurebox" <a href="devtodo"
                class="morelink" title="A h3 level heading inside a featurebox div">More <span>about:
                    A h3 level heading inside a featurebox div</span></a></p>
    </div>
    <hr />
    <ul>
        <li><a href="devtodo">A list of links</a></li>
        <li><a href="devtodo">A list of links</a></li>
        <li><a href="devtodo">A list of links</a></li>
    </ul>
    <hr />
    <p>
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
        A normal paragraph of text, A normal paragraph of text, A normal paragraph of text,
    </p>
    <ul class="related">
        <li><a href="devtodo">A list of related links</a></li>
        <li><a href="devtodo">A list of related links</a></li>
        <li><a href="devtodo">A list of related links</a></li>
    </ul>
    <hr />
    <ul>
        <li>An unordered list</li>
        <li>An unordered list</li>
        <li>An unordered list</li>
    </ul>
    <hr />
    <ol>
        <li>An ordered list</li>
        <li>An ordered list</li>
        <li>An ordered list</li>
    </ol>
    <hr />
    <dl>
        <dt>A definition list - and this is the dt</dt>
        <dd>
            A definition list - and this is the dd, A definition list - and this is the dd,
            A definition list - and this is the dd, A definition list - and this is the dd,
            A definition list - and this is the dd,
        </dd>
        <dt>A definition list - and this is the dt</dt>
        <dd>
            A definition list - and this is the dd, A definition list - and this is the dd,
            A definition list - and this is the dd, A definition list - and this is the dd,
            A definition list - and this is the dd,
        </dd>
    </dl>
    <hr />
    <h4>
        <span class="date">29 July 2005</span> Headline and associate teaser</h4>
    <p>
        Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Donec pharetra. <a href="devtodo" class="morelink" title="Headline and associate teaser">
            More <span>about: Headline and associate teaser</span></a></p>
    <h4>
        <span class="date">29 July 2005</span> Headline and associate teaser</h4>
    <p>
        Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Donec pharetra. <a href="devtodo" class="morelink" title="Headline and associate teaser">
            More <span>about: Headline and associate teaser</span></a></p>
    <hr />
    <h4>
        <span class="date">29 July 2005</span> Headline and associate teaser and thumbnail</h4>
    <p>
        <span class="thumbnail"><a href="devtodo">
            <img src="images/thumb_100wide.gif" alt="Demo" width="100" height="75" /></a></span>Lorem
        ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing elit.
        Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Donec pharetra. <a href="devtodo" class="morelink" title="Headline and associate teaser">
            More <span>about: Headline and associate teaser</span></a></p>
    <hr />
    <h4>
        <span class="date">29 July 2005</span> Headline and associate teaser and thumbnail</h4>
    <p>
        <span class="thumbnail"><a href="devtodo">
            <img src="images/thumb_100wide.gif" alt="Demo" width="100" height="75" /></a></span>Lorem
        ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing elit.
        Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Lorem ipsum dolor sit amsum dolor sit amet, consectetet, consectetuer adipiscing
        elit. Donec pharetra. <a href="devtodo" class="morelink" title="Headline and associate teaser">
            More <span>about: Headline and associate teaser</span></a></p>
    <hr />
    <div class="pagination">
        <p>
            <span><strong>Previous</strong></span> <span>1</span> <a href="devtodo">2</a> <a
                href="devtodo">3</a> <a href="devtodo">4</a> <a href="devtodo">5</a> <a href="devtodo">
                    <strong>Next</strong></a></p>
        <h4>
            Page 1 of 5</h4>
    </div>
    <hr />
    <h1>
        Search Results</h1>
    <div id="resultslist-wrap">
        <ol>
            <li>
                <dl>
                    <dt><a href="devtodo">Fringilla tortor. Sed ullamcorper imperdiet metus.</a></dt>
                    <dd class="desc">
                        Maecenas nec ante vitae turpis interdum placerat. Duis in nisi. Mauris at enim.
                        Ut vestibulum erat at tellus.</dd>
                    <dd class="filetype">
                        HTML</dd>
                    <dd class="date">
                        22 April 2005</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt><a href="devtodo">Fringilla tortor. Sed ullamcorper imperdiet metus.</a></dt>
                    <dd class="desc">
                        Maecenas nec ante vitae turpis intecenas nec ante vitae turpis interdum placerat.
                        Duis in nisi. Mauris cenas nec ante vitae turpis interdum placerat. Duis in nisi.
                        Mauris cenas nec ante vitae turpis interdum placerat. Duis in nisi. Mauris cenas
                        nec ante vitae turpis interdum placerat. Duis in nisi. Mauris rdum placerat. Duis
                        in nisi. Mauris at enim. Ut vestibulum erat at tellus.</dd>
                    <dd class="filetype">
                        HTML</dd>
                    <dd class="date">
                        22 April 2005</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt><a href="devtodo">Fringilla tortor. Sed ullamcorper imperdiet metus.</a></dt>
                    <dd class="desc">
                        Maecenas nec ante vitae turpis interdum placerat. Duis in nisi. Mauris at enim.
                        Ut vestibulum erat at tellus.</dd>
                    <dd class="filetype">
                        PDF</dd>
                    <dd class="date">
                        22 April 2005</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt><a href="devtodo">Fringilla tortor. Sed ullamcorper imperdiet metus.</a></dt>
                    <dd class="desc">
                        Maecenas nec ante vitae turpis interdum placerat. Duis in nisi. Mauris cenas nec
                        ante vitae turpis interdum placerat. Duis in nisi. Mauris cenas nec ante vitae turpis
                        interdum placerat. Duis in nisi. Mauris at enim. Ut vestibulum erat at tellus.</dd>
                    <dd class="filetype">
                        WORD</dd>
                    <dd class="date">
                        22 April 2005</dd>
                </dl>
            </li>
        </ol>
    </div>
</asp:Content>
