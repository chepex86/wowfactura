<%@ Page Title="Listado de Certificados Digitales" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="DigitalCertificatesList.aspx.cs" Inherits="PACFDManager.DigitalCertificates.DigitalCertificatesList" %>

<%@ Register Src="DigitalCertificatesEditor.ascx" TagName="DigitalCertificatesEditor"
    TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:DigitalCertificatesEditor ID="DigitalCertificatesEditor1" runat="server" />
            <br />
            <br />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleList" runat="server" Text="Listado de Certificados Digitales" />
                    </h2>
                </div>
                <asp:Button ID="btnAddDigitalCertificateTop" runat="server" CssClass="Button" PostBackUrl="~/DigitalCertificates/DigitalCertificatesAdd.aspx"
                    Text="Agregar Certificado Digital" />
                <span>
                    <asp:GridView ID="gvDigitalCertificates" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="#333333" HorizontalAlign="Center" OnRowDataBound="gvDigitalCertificates_RowDataBound">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:BoundField DataField="DigitalCertificateID" HeaderText="ID" />
                            <asp:BoundField DataField="CertificateName" HeaderText="Certificado" />
                            <asp:BoundField DataField="CertificateValidityBegin" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Valido Desde" HtmlEncode="False" />
                            <asp:BoundField DataField="CertificateValidityEnd" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderText="Valido Hasta" HtmlEncode="False" />
                            <asp:BoundField DataField="PrivateKeyName" HeaderText="Llave Privada" />
                            <asp:BoundField DataField="Status" HeaderText="Estado" />
                            <asp:TemplateField HeaderText="Detalles">
                                <HeaderTemplate>
                                    <uc3:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Details' /> para ver<br />los detalles del Certificado seleccionado." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Ver detalles del certificado<br /><b><%#Eval("CertificateName")%></b>'>
                                        <a runat="server" target="_self" href="DigitalCertificatesDetails.aspx" id="aDetails">
                                            <img id="Img3" alt="Details" border="0" src="~/Includes/Images/png/kfind.png" runat="server" /></a></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descargar *.cer">
                                <HeaderTemplate>
                                    <uc3:Tooltip ID="tipDownloadCer" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/certificate.png' alt='Certificate'/><br />para descargar el Certificado Digital." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Descargar el certificado<br /><b><%#Eval("CertificateName")%></b>'>
                                        <a runat="server" target="_self" href="DownloadFiles.aspx" id="aDownLoadCer">
                                            <img id="Img1" alt="Download" border="0" src="~/Includes/Images/png/certificate.png"
                                                runat="server" /></a></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descargar *.key">
                                <HeaderTemplate>
                                    <uc3:Tooltip ID="tipDownloadKey" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/certificate-key.png' alt='Key' /><br />para descargar la llave privada." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Descargar la llave privada<br /><b><%#Eval("PrivateKeyName")%></b>'>
                                        <a runat="server" target="_self" href="DownloadFiles.aspx" id="aDownLoadKey">
                                            <img id="Img2" alt="Download" border="0" src="~/Includes/Images/png/certificate-key.png"
                                                runat="server" /></a></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Des/Activar">
                                <HeaderTemplate>
                                    <uc3:Tooltip ID="Tooltip1" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editdelete.png' alt='Deactivate' /> para<br />desactivar el certificado seleccionado.<br /><br />Hacer click en la imagen <img src='../Includes/Images/png/apply.png' alt='Activate'/> para<br />activar el certificado seleccionado." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='<%#Eval("Status").ToString() == "1" ? "Desactivar el certificado<br /><b>" + Eval("CertificateName") + "</b>" : "Activar el certificado<br /><b>" + Eval("CertificateName") + "</b>"%>'>
                                        <asp:ImageButton ID="ImgBtnActive" runat="server" CommandArgument='<%# Eval("DigitalCertificateID") %>'
                                            CausesValidation="False" OnClick="ImgBtnActive_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png">
                                        </asp:ImageButton></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </span>
                <asp:Button ID="btnAddDigitalCertificateBottom" runat="server" CssClass="Button"
                    PostBackUrl="~/DigitalCertificates/DigitalCertificatesAdd.aspx" Text="Agregar Certificado Digital" />
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
