﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BranchesEditor.ascx.cs"
    Inherits="PACFDManager.Billers.Branches.BranchesEditor" %>
<%@ Register Src="../../CountrySelector.ascx" TagName="CountrySelector" TagPrefix="uc1" %>
<div class="info">
    <label class="desc">
        <h2>
            <asp:Label ID="lblTitle" runat="server" Text="Sucursal" />
        </h2>
    </label>
</div>
<ul>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblName" runat="server" Text="Nombre:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " Nombre de la sucursal."%>'>
            <asp:TextBox ID="txtName" runat="server" Width="350px" MaxLength="255" />
        </span>
        <div class="validator-msg">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
        </div>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblCode" runat="server" Text="Codigo:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el codigo unico de sucursal."%>'>
            <asp:TextBox ID="txtCode" runat="server" MaxLength="10" Width="180px" />
        </span>
        <div class="validator-msg" style="width: 180px">
            <asp:RequiredFieldValidator ID="txtCode_RequiredFieldValidator" runat="server" ControlToValidate="txtCity"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
        </div>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblAddress" runat="server" Text="Dirección:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " la(s) calle(s) donde se encuentra ubicada fisicamente la empresa."%>'>
            <asp:TextBox ID="txtAddress" runat="server" Width="350px" MaxLength="100" /> 
        </span>
        <div class="validator-msg">
            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
        </div>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblColony" runat="server" Text="Colonia:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre de la Colonia donde se encuentra ubicada la empresa."%>'>
            <asp:TextBox ID="txtColony" runat="server" MaxLength="25" Width="145px" />
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblExternalNumber" runat="server" Text="Número externo:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el número exterior del lugar (local, edificio, terreno),<br />en donde se encuentra ubicada la empresa."%>'>
            <asp:TextBox ID="txtExternalNumber" runat="server" MaxLength="6" Width="45px"  />
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblInternalNumber" runat="server" Text="Número interno:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el número interior del lugar (local, edificio, terreno),<br />en donde se encuentra ubicada la empresa."%>'>
            <asp:TextBox ID="txtInternalNumber" runat="server" MaxLength="6" Width="45px" />
        </span></li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblCity" runat="server" Text="Ciudad:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre de la Ciudad en donde<br />se encuentra establecida la empresa. "%>'>
            <asp:TextBox ID="txtCity" runat="server" MaxLength="20" Width="180px" />
        </span>
        <div class="validator-msg" style="width: 180px">
            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
        </div>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblMunicipality" runat="server" Text="Municipio:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el nombre del Municipio en donde<br />se encuentra establecida la empresa." %>'>
            <asp:TextBox ID="txtMunicipality" runat="server" MaxLength="40" Width="180px" />
        </span>
        <div class="validator-msg" style="width: 180px">
            <asp:RequiredFieldValidator ID="rfvMunicipality" runat="server" ControlToValidate="txtMunicipality"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
        </div>
    </li>
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblReference" runat="server" Text="Referencia:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " alguna referencia que ayude a ubicar más fácilmente la empresa.<br />Ej. <b>Una calle, un edificio, un monumento, etc.</b>"%>'>
            <asp:TextBox ID="txtReference" runat="server" Width="200px" MaxLength="255" />
        </span></li>
    <uc1:CountrySelector ID="CountrySelector1" runat="server" />
    <li class="left">
        <label class="desc">
            <asp:Label ID="lblZipcode" runat="server" Text="Código postal:" />
        </label>
        <span class="vtip" title='<%=(ToolTipMode() == "True" ? "Muestra" : "Escriba") + " el Código postal de la empresa.<br />Ej. &lt;b&gt;23090&lt;/b&gt;"%>'>
            <asp:TextBox ID="txtZipcode" runat="server" MaxLength="10" Width="80px" /></span>
        <div class="validator-msg">
            <asp:RequiredFieldValidator ID="rfvZipcode" runat="server" ControlToValidate="txtZipcode"
                Display="Dynamic" ErrorMessage="Este campo es requerido" />
            <asp:RegularExpressionValidator ID="revZopcode" runat="server" Display="Dynamic"
                ErrorMessage="Solo numeros (0 - 9)." ControlToValidate="txtZipcode" ValidationExpression="^\d+$" /></div>
    </li>
</ul>
