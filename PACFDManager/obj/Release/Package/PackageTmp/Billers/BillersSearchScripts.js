﻿var jsBiller = new jclassBillersSearch();

function jclassBillersSearch() {
    var that = this;

    this.LoadBindControls_OnPageLoad = function(autocompleteBehaviorID, uProgressID, uProgressDynamicLayout) {

        $find(autocompleteBehaviorID).add_populated(function() { that.showev(uProgressID, uProgressDynamicLayout); });
        $find(autocompleteBehaviorID).add_populating(function() { that.populatingev(uProgressID); });
        $find(autocompleteBehaviorID).add_hiding(function() { that.onhiding(uProgressID, uProgressDynamicLayout); });
    }

    this.onhiding = function(uProgressID, uProgressDynamicLayout) {

        var updateProgress = $get(uProgressID);
        var dynamicLayout = uProgressDynamicLayout;

        updateProgress.style.display = dynamicLayout ? 'none' : 'block';
    }

    this.showev = function(uProgressID, uProgressDynamicLayout) {

        var updateProgress = $get(uProgressID);
        var dynamicLayout = uProgressDynamicLayout;

        updateProgress.style.display = dynamicLayout ? 'none' : 'block';
    }

    this.populatingev = function(uProgressID) {

        var updateProgress = $get(uProgressID);

        updateProgress.style.display = 'block';
    }
}