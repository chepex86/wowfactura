﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillersSearch.ascx.cs"
    Inherits="PACFDManager.Billers.BillersSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>

<script src="BillersSearchScripts.js" type="text/javascript"></script>

<div id="container" class="wframe60">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblSearchTitle" runat="server" Text="Búsqueda de empresa" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Por Nombre" />
                    </label>
                    <span class="vtip" title="Busqueda por nombre de empresa.">
                        <asp:TextBox ID="txtName" runat="server" /></span> <span id="spnByName" runat="server"
                            style="display: none;">
                            <img runat="server" id="imgLoading_ByName" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                        </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblRFC" runat="server" Text="Por RFC" />
                    </label>
                    <span class="vtip" title="Busqueda por *RFC de la empresa,<br />El RFC es pedido por hacienda. Ej.: <br/><br /><b>•ABC123456ABC</b><br/><b>•ABCD123456ABC</b><br><br><b>Nota: <i>*</i></b> <i>Registro Federal de Contribuyentes.</i>">
                        <asp:TextBox ID="txtRFC" runat="server" />
                    </span><span id="spnByRFC" runat="server" style="display: none;">
                        <img runat="server" id="imgLoading_ByRFC" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <asp:Button CssClass="Button_Search" ID="btnSearchName" runat="server" Text="Buscar"
                        OnClick="btnSearch_Click" />
                </li>
            </ul>
        </div>
    </div>
</div>
<asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtName"
    BehaviorID="aceByName" CompletionInterval="1000" DelimiterCharacters=";" CompletionSetCount="10"
    FirstRowSelected="true" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetBillersByName"
    MinimumPrefixLength="1" EnableCaching="true" UseContextKey="True">
</asp:AutoCompleteExtender>
<asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtRFC"
    BehaviorID="aceByRFC" CompletionInterval="1000" DelimiterCharacters=";" CompletionSetCount="10"
    FirstRowSelected="true" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetBillersRFCs"
    MinimumPrefixLength="1" EnableCaching="true" UseContextKey="True">
</asp:AutoCompleteExtender>
