<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DigitalCertificatesProviderList.aspx.cs"
    Inherits="PACFDManager.Provider.DigitalCertificatesProviderList" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <a runat="server" id="atest"></a>
            <table id="BookingContainer">
                <tr>
                    <td align="center" class="Titulo">
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de Certificados" meta:resourcekey="lblTitleResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnAddDigitalCertificateTop" runat="server" CssClass="Button" meta:resourcekey="btnAddDigitalCertificateTopResource1"
                            PostBackUrl="~/Provider/DigitalCertificatesProviderAdd.aspx" Text="Agregar Certificado Digital" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvCertificateList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" HorizontalAlign="Center" OnRowDataBound="gvCertificateList_RowDataBound"
                            meta:resourcekey="gvCertificateListResource1">
                            <RowStyle BackColor="#EFF3FB" />
                            <Columns>
                                <asp:BoundField DataField="ProviderDigitalCertificateID" HeaderText="ID" />
                                <asp:BoundField DataField="CertificateName" HeaderText="Certificado" meta:resourcekey="BoundFieldResource2" />
                                <asp:TemplateField HeaderText="Descargar *.cer" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <a runat="server" target="_self" href="DownloadFilesProvider.aspx" id="aDownLoadCer">
                                            <img id="imgDownloadCer" runat="server" title="Descargar" alt="Descargar Certificado"
                                                border="0" src="~/Includes/Images/png/filesave-48.png" /></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PrivateKeyName" HeaderText="Llave Privada" meta:resourcekey="BoundFieldResource3" />
                                <asp:TemplateField HeaderText="Descargar *.key" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <a runat="server" target="_self" href="DownloadFilesProvider.aspx" id="aDownLoadKey">
                                            <img id="imgDownloadKey" runat="server" title="Descargar" alt="Descargar Llave Privada"
                                                border="0" src="~/Includes/Images/png/filesave-48.png" /></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Status" HeaderText="Estado" meta:resourcekey="BoundFieldResource1" />
                                <asp:TemplateField HeaderText="Detalles" meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <a runat="server" target="_self" href="DigitalCertificatesProviderDetails.aspx" id="aDetails">
                                            <img id="imgDetails" runat="server" alt="Detalles" title="Detalles" border="0" src="~/Includes/Images/png/kfind.png" /></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Des/Activar" meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgBtnActive" runat="server" CommandArgument='<%# Eval("ProviderDigitalCertificateID") %>'
                                            CausesValidation="False" OnClick="ImgBtnActive_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                            meta:resourcekey="ImgBtnActiveResource1" ToolTip="Des/Activar"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnAddDigitalCertificateBottom" runat="server" CssClass="Button"
                            Text="Agregar Certificado Digital" PostBackUrl="~/Provider/DigitalCertificatesProviderAdd.aspx"
                            meta:resourcekey="btnAddDigitalCertificateBottomResource1" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
