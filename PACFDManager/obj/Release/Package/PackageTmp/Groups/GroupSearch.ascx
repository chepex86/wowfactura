﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupSearch.ascx.cs"
    Inherits="PACFDManager.Groups.GroupSearch" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script src="GroupSearchScripts.js" type="text/javascript"></script>

<div id="container" class="wframe40">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitleOptions" runat="server" Text="Búsqueda de Grupos"></asp:Label>
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Por Nombre:"></asp:Label>
                    </label>
                    <span class="vtip" title="Escribe el nombre del grupo o una parte de el<br />(el <l>autocompletado</l> cargara los resultados que<br />coincidan con el texto introducido).<br /><br />Si no quieres incluir el nombre del grupo<br />en la búsqueda da click en el botón <b>Limpiar</b>.">
                        <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="txtName_AutoCompleteExtender" runat="server" EnableCaching="true"
                            UseContextKey="True" ServicePath="~/AutoComplete.asmx" MinimumPrefixLength="1"
                            ServiceMethod="GetGroupName" TargetControlID="txtName">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByName" runat="server" style="display: none;">
                        <img id="imgLoading_ByName" runat="server" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <div>
                        <asp:Button ID="btnAccept" runat="server" Text="Buscar" CssClass="Button_Search"
                            OnClick="btnAccept_Click" ValidationGroup="Validators" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
