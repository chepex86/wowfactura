﻿------------------------------------------------
--********************************************--
------------------------------------------------

USE [PACFD]
GO

/****** Object:  Table [dbo].[BillingGeneralPeriod]    Script Date: 26/01/2023 10:22:13 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BillingGeneralPeriod](
	[BillingGeneralPeriodID] [int] IDENTITY(1,1) NOT NULL,
	[BillingID] [int] NOT NULL,
	[Period] [nchar](2) NOT NULL,
	[Month] [nchar](2) NOT NULL,
	[Year] [int] NOT NULL,
 CONSTRAINT [PK_BillingGeneralPeriod] PRIMARY KEY CLUSTERED 
(
	[BillingGeneralPeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[BillingGeneralPeriod]  WITH CHECK ADD  CONSTRAINT [FK_BillingGeneralPeriod_Billings] FOREIGN KEY([BillingID])
REFERENCES [dbo].[Billings] ([BillingID])
GO

ALTER TABLE [dbo].[BillingGeneralPeriod] CHECK CONSTRAINT [FK_BillingGeneralPeriod_Billings]
GO


USE [PACFD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_Insert]
(
	@BillingID int,
	@Period nchar(2),
	@Month nchar(2),
	@Year int,
	@BillingGeneralPeriodID int = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	INSERT
	INTO [BillingGeneralPeriod]
	(
		[Period],
		[Month],
		[Year],
		[BillingID]
	)
	VALUES
	(
		@Period,
		@Month,
		@Year,
		@BillingID
	)

	
	SELECT @BillingGeneralPeriodID = SCOPE_IDENTITY()

	--Seleccionamos todas las filas para que se actualize el dataset
	SELECT
		*
	FROM [BillingGeneralPeriod]
	WHERE
		([BillingGeneralPeriodID] = @BillingGeneralPeriodID)

	SET @Err = @@Error
	
RETURN @Err
END
GO

USE [PACFD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_Update]
(
	@BillingID int,
	@Period nchar(2),
	@Month nchar(2),
	@Year int,
	@BillingGeneralPeriodID int
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	UPDATE [BillingGeneralPeriod]
	SET
		[Period] = @Period,
		[Month] = @Month,
		[Year] = @Year,
		[BillingID] = @BillingID
	WHERE
		[BillingGeneralPeriodID] = @BillingGeneralPeriodID

	SET @Err = @@Error

	RETURN @Err
END

GO


USE [PACFD]
GO
/****** Object:  StoredProcedure [dbo].[spBillingGeneralPeriod_GetByFkBillingID]    Script Date: 26/01/2023 10:19:12 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_GetByFkBillingID]
(
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		 * FROM [BillingGeneralPeriod]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END
GO

USE [PACFD]
GO
/****** Object:  StoredProcedure [dbo].[spBillingGeneralPeriod_GetByID]   Script Date: 26/01/2023 10:19:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_GetByID]
(
	@BillingGeneralPeriodID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		*
	FROM [BillingGeneralPeriod]
	WHERE
		([BillingGeneralPeriodID] = @BillingGeneralPeriodID)

	SET @Err = @@Error

	RETURN @Err
END
GO

USE [PACFD]
GO
/****** Object:  StoredProcedure [dbo].[spBillingGeneralPeriod_GetAll]  Script Date: 26/01/2023 10:19:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_GetAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		* 
	FROM [BillingGeneralPeriod]

	SET @Err = @@Error

	RETURN @Err
END

GO

USE [PACFD]
GO
/****** Object:  StoredProcedure  [dbo].[spBillingGeneralPeriod_Exists]    Script Date: 26/01/2023 10:19:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_Exists]
(
	@BillingGeneralPeriodID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT COUNT(*)
	FROM [BillingGeneralPeriod]
	WHERE
		([BillingGeneralPeriodID] = @BillingGeneralPeriodID)

	SET @Err = @@Error

	RETURN @Err
END

GO

USE [PACFD]
GO
/****** Object:  StoredProcedure  [dbo].[spBillingGeneralPeriod_Delete]    Script Date: 26/01/2023 10:19:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBillingGeneralPeriod_Delete]
(
	@BillingGeneralPeriodID int
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	DELETE
	
	FROM [BillingGeneralPeriod]
	WHERE
		[BillingGeneralPeriodID] = @BillingGeneralPeriodID
	SET @Err = @@Error

	RETURN @Err
END


GO


USE [PACFD]
GO
/****** Object:  StoredProcedure [dbo].[spBillingsAllCustomer_GetByID]    Script Date: 30/01/2023 11:56:54 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Procedure

ALTER PROCEDURE [dbo].[spBillingsAllCustomer_GetByID] (
	@BillingID int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int
	
	
--///[Billings]		
	SELECT
		*
	FROM 
		[Billings]
	WHERE
		([BillingID] = @BillingID)
		
--///BillingsBillers
	SELECT
		bb.*
		, br.Email
	FROM 
		BillingsBillers bb
	INNER JOIN Billings bi ON bi.BillingID = @BillingID
	INNER JOIN Billers br ON br.BillerID = bi.BillerID 
	WHERE
		(bb.[BillingID] = @BillingID)
--///BillingsDetails		
	SELECT
		*
	FROM 
		BillingsDetails
	WHERE
		([BillingID] = @BillingID)		
--///BillingsIssued		
	SELECT
		*
	FROM 
		BillingsIssued
	WHERE
		([BillingID] = @BillingID)						
--///BillingsReceptors		
	SELECT
		*
	FROM 
		BillingsReceptors
	WHERE
		([BillingID] = @BillingID)		
--///Taxes		
	SELECT
		*
	FROM 
		Taxes
	WHERE
		([BillingID] = @BillingID)		
--///TransferTaxes		
	SELECT
		*
	FROM 
		TransferTaxes
	WHERE
		([BillingID] = @BillingID)	
--///DetainedTaxes		
	SELECT
		*
	FROM 
		DetainedTaxes
	WHERE
		([BillingID] = @BillingID)	
		
--///BillingFiscalRegime		
	SELECT
		*
	FROM 
		BillingFiscalRegime
	WHERE
		([BillingID] = @BillingID)
		

--///BillingsDetailNotes
	SELECT
		*
	FROM 
		BillingsDetailNotes
	WHERE
		([BillingID] = @BillingID)

--///billing fields
	SELECT
		*
	FROM 
		[dbo].[BillingFields]
	WHERE
		([BillingID] = @BillingID)

--///billing fields
	SELECT
		*
	FROM 
		[dbo].[BillingGeneralPeriod]
	WHERE
		([BillingID] = @BillingID)

	SET @Err = @@Error

	RETURN @Err
END


GO


/*****/

USE [Unifacturadigital]
GO

/****** Object:  Table [dbo].[CatMeses]    Script Date: 26/01/2023 06:52:24 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------
--********************************************--
------------------------------------------------

CREATE TABLE [dbo].[CatMeses](
	[id] [int] NOT NULL,
	[c_Meses] [nchar](2) NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
	[FechaInicioVigencia] [nvarchar](15) NOT NULL,
	[FechaFinVigencia] [nvarchar](15) NULL,
 CONSTRAINT [PK_CatMeses] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------
--********************************************--
------------------------------------------------

CREATE TABLE [dbo].[CatPeriodicidad](
	[id] [int] NOT NULL,
	[c_Periodicidad] [nchar](2) NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
	[FechaInicioVigencia] [nvarchar](15) NOT NULL,
	[FechaFinVigencia] [nvarchar](15) NULL,
 CONSTRAINT [PK_CatPeriodicidad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

------------------------------------------------
--********************************************--
------------------------------------------------

----  DATA FEED. -----

USE [Unifacturadigital]
GO

insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (1,'01','Enero','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (2,'02','Febrero','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (3,'03','Marzo','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (4,'04','Abril','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (5,'05','Mayo','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (6,'06','Junio','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (7,'07','Julio','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (8,'08','Agosto','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (9,'09','Septiembre','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (10,'10','Octubre','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (11,'11','Noviembre','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (12,'12','Diciembre','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (13,'13','Enero-Febrero','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (14,'14','MArzo-Abril','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (15,'15','Mayo-Junio','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (16,'16','Julio-Agosto','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (17,'17','Septiembre-Octubre','01/01/2022',null)
	GO
insert into CatMeses ([id],
	[c_Meses],
	[Descripcion],
	[FechaInicioVigencia],
	[FechaFinVigencia]) 
	values (18,'18','Noviembre-Diciembre','01/01/2022',null)
	GO


/**
	Catalogo de periodicidad. 
**/

insert into [CatPeriodicidad] ([id],
	[c_periodicidad],
	[Descripcion], 
	[FechaInicioVigencia])
	values (1,'01','Diario','01/01/2022')
	GO
	insert into [CatPeriodicidad] ([id],
		[c_periodicidad],
	[Descripcion], 
	[FechaInicioVigencia])
	values (2,'02','Semanal','01/01/2022')
	GO
	insert into [CatPeriodicidad] ([id],
		[c_periodicidad],
	[Descripcion], 
	[FechaInicioVigencia])
	values (3,'03','Quincenal','01/01/2022')
	GO
	insert into [CatPeriodicidad] ([id],
		[c_periodicidad],
	[Descripcion], 
	[FechaInicioVigencia])
	values (4,'04','Mensual','01/01/2022')
	GO
	insert into [CatPeriodicidad] ([id],
		[c_periodicidad],
	[Descripcion], 
	[FechaInicioVigencia])
	values (5,'05','Bimestral','01/01/2022')
	GO

/**
	Nuevos usos CFDI. 
**/

insert into [CatUsoCFDI] ([id],
	[c_UsoCFDI],
	[Descripcion], 
	[Fisica], 
	[Moral], 
	[FechaInicioVigencia], 
	[FechaFinVigencia])
	values (23,'CP01','Pagos','Sí','Sí','01/01/2022','')
	GO
insert into [CatUsoCFDI] ([id],
	[c_UsoCFDI],
	[Descripcion], 
	[Fisica], 
	[Moral], 
	[FechaInicioVigencia], 
	[FechaFinVigencia])
	values (24,'CN01','Nómina','Sí','No','01/01/2022','')
	GO
insert into [CatUsoCFDI] ([id],
	[c_UsoCFDI],
	[Descripcion], 
	[Fisica], 
	[Moral], 
	[FechaInicioVigencia], 
	[FechaFinVigencia])
	values (25,'S01','Sin Efectos Fiscales','Sí','Sí','01/01/2022','')
	GO

