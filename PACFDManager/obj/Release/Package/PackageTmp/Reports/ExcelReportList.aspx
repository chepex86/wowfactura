<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="ExcelReportList.aspx.cs" Inherits="PACFDManager.Reports.ExcelReportList" %>

<%@ Register Src="ExcelReportSearch.ascx" TagName="ExcelReportSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:ExcelReportSearch ID="ExcelReportSearch1" runat="server" OnSearch="ExcelReportSearch1_Search" />
            <br />
            <div class="formStyles">
                <div class="info">
                    <label class="desc">
                        <h2>
                            Estado de cuenta por cliente.
                        </h2>
                    </label>
                </div>
            </div>
            <div id="divSearchData" runat="server">
                <div class="form">
                    <div class="formStyles">
                        <ul>
                            <li>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Cliente:
                                                    </h2>
                                                </label>
                                            </div>
                                            <asp:Label ID="lblReceptorName" runat="server" Text="" Font-Bold="true" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Fecha:
                                                    </h2>
                                                </label>
                                            </div>
                                            <asp:Label ID="lblActualDate" runat="server" Text='<%=DateTime.Now.ToString("dd/MMM/yyyy") %>'
                                                Font-Bold="true" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Descargar Archivos:
                                                    </h2>
                                                </label>
                                            </div>
                                            <span style="width: 100%; text-align: right;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="<%= 
                                                                string.Format("DownloadExcel.aspx?downloadtype=excelreport&idate={0}&edate={1}",
                                                                this.ExcelReportSearch1.ExcelInformation.InitialDate == null ? DateTime.Now.ToString("ddMMMMyyyy")
                                                                : ((DateTime)this.ExcelReportSearch1.ExcelInformation.InitialDate).ToString("ddMMMMyyyy")
                                                                ,
                                                                this.ExcelReportSearch1.ExcelInformation.EndDate == null ? DateTime.Now.ToString("ddMMMMyyyy")
                                                                : ((DateTime)this.ExcelReportSearch1.ExcelInformation.EndDate).ToString("ddMMMMyyyy"))                                                 %>"
                                                                class="Button">
                                                                <img alt="xls" src="../Includes/Images/png/excel.png" />
                                                                Descargar Xls. </a>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <!--pdf-->
                                                            <a href="<%= 
                                                                string.Format("DownloadExcel.aspx?downloadtype=pdfreport&idate={0}&edate={1}",
                                                                this.ExcelReportSearch1.ExcelInformation.InitialDate == null ? DateTime.Now.ToString("ddMMMMyyyy")
                                                                : ((DateTime)this.ExcelReportSearch1.ExcelInformation.InitialDate).ToString("ddMMMMyyyy")
                                                                ,
                                                                this.ExcelReportSearch1.ExcelInformation.EndDate == null ? DateTime.Now.ToString("ddMMMMyyyy")
                                                                : ((DateTime)this.ExcelReportSearch1.ExcelInformation.EndDate).ToString("ddMMMMyyyy"))                                                 %>"
                                                                class="Button">
                                                                <img alt="xls" src="../Includes/Images/png/pdf-opt.png" />
                                                                Descargar Pdf. </a>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
                <br />
                <asp:GridView ID="gdvExcelReport" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                    AllowPaging="true" OnRowDataBound="gdvExcelReport_RowDataBound" PageSize="50"
                    OnPageIndexChanging="gdvExcelReport_PageIndexChanging">
                    <HeaderStyle CssClass="dgHeader" />
                    <PagerSettings Position="TopAndBottom" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                    <EmptyDataTemplate>
                        No hay comprobantes disponibles.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="BillingDate" HeaderText="Fecha" DataFormatString="{0:dd/MMMM/yyyy}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Abonado">
                            <HeaderTemplate>
                                <a class="vtip" title="Serie y folio del comprobante.">Serie-Folio</a>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# string.Format("{0} - {1}", Eval("Serial"),  Eval("Folio"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:c}" />
                        <asp:TemplateField HeaderText="Abonado">
                            <ItemTemplate>
                                <asp:Label ID="lblPaidTotal" runat="server" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Adeudo">
                            <ItemTemplate>
                                <asp:Label ID="lblToPaid" runat="server" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
