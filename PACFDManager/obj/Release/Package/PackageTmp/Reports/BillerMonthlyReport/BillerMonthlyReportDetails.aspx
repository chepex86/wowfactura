﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="BillerMonthlyReportDetails.aspx.cs"
    Inherits="PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportDetails" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<%@ Register Src="../../DigitalCertificates/DigitalCertificatesEditor.ascx" TagName="DigitalCertificatesEditor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Detalle del Reporte"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li><span>
                                <asp:Label ID="lblTitleReportName" runat="server" Font-Bold="True" Text="Nombre:"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblReportName" runat="server"></asp:Label>
                            </span></li>
                            <li><span>
                                <asp:Label ID="lblTitleCreationDate" runat="server" Font-Bold="True" Text="Fecha de Creación:"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblCreationDate" runat="server"></asp:Label>
                            </span></li>
                            <li><span>
                                <asp:Label ID="lblTitleMonth" runat="server" Font-Bold="True" Text="Mes:"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblMonth" runat="server"></asp:Label>
                            </span></li>
                            <li><span>
                                <asp:Label ID="lblTitleYear" runat="server" Font-Bold="True" Text="Año:"></asp:Label>
                            </span><span>
                                <asp:Label ID="lblYear" runat="server"></asp:Label>
                            </span></li>
                        </ul>
                        <div>
                            <asp:TextBox ID="txtContent" runat="server" Height="124px" ReadOnly="True" TextMode="MultiLine"
                                Width="525px"></asp:TextBox>
                        </div>
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="center" class="Titulo">
                                        <asp:Label ID="lblDownload" runat="server" Text="Descargar Reporte"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:ImageButton ID="imgBtnDownload" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                            OnClick="imgBtnDownload_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <ul>
                            <li class="buttons">
                                <div>
                                    <asp:Button ID="btnReturn" runat="server" CssClass="Button" PostBackUrl="~/Reports/BillerMonthlyReport/BillerMonthlyReportList.aspx"
                                        Text="Aceptar" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
