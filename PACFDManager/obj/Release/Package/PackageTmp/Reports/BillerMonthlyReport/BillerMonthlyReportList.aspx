﻿<%@ Page Title="Listado de Reportes Mensuales para el SAT" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillerMonthlyReportList.aspx.cs" Inherits="PACFDManager.Reports.BillerMonthlyReport.BillerMonthlyReportList" %>

<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitleData" runat="server" Text="Datos de la Empresa"></asp:Label>
                </h2>
                <asp:Label ID="lblTitleName" runat="server" Font-Bold="True" Text="Razón Social:"></asp:Label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblTitleRFC" runat="server" Font-Bold="True" Text="R.F.C.:"></asp:Label>
                <asp:Label ID="lblRFC" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblTitleAddress" runat="server" Font-Bold="True" Text="Dirección:"></asp:Label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
            <br />
            <br />
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de Reportes Mensuales para el SAT"></asp:Label>
                    </h2>
                </div>
                <asp:Button ID="btnGeneratesReportTop" runat="server" CssClass="Button" PostBackUrl="~/Reports/BillerMonthlyReport/BillerMonthlyReportGenerated.aspx"
                    Text="Generar Reporte" />
                <span>
                    <center>
                        <asp:GridView ID="gvReportList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" HorizontalAlign="Center" OnRowDataBound="gvReportList_RowDataBound">
                            <RowStyle BackColor="#EFF3FB" />
                            <Columns>
                                <asp:BoundField DataField="CreationDate" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false"
                                    HeaderText="Fecha de Creación" />
                                <asp:TemplateField HeaderText="Reporte">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%#GenerateReportName((Byte)Eval("Month"), (int)Eval("Year"))%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enviado">
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="lnkBtnSent" runat="server" OnClick="lnkBtnSent_OnClick" CommandArgument='<%# Eval("MonthlyReportID") %>'>Marcar como Enviado</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SentDate" HeaderText="Fecha de Envío" DataFormatString="{0:dd/MM/yyyy}"
                                    HtmlEncode="false" />
                                <asp:TemplateField HeaderText="Detalles">
                                    <HeaderTemplate>
                                        <uc1:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/kfind.png' alt='Detalles' /> para ver<br />los detalles del reporte seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Ver detalles del reporte<br /><b><%#FormatDate(Convert.ToInt32(Eval("Month"))) + " / " + Eval("Year")%></b>'>
                                            <a id="aDetails" runat="server" href="BillerMonthlyReportDetails.aspx" target="_self">
                                                <img id="imgDetails" alt="Details" border="0" src="~/Includes/Images/png/kfind.png"
                                                    runat="server" /></a> </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Descargar">
                                    <HeaderTemplate>
                                        <uc1:Tooltip ID="tipDownload" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/filesave-48.png' alt='Descargar' /> para<br />descargar el reporte seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Descargar el reporte<br /><b><%#FormatDate(Convert.ToInt32(Eval("Month"))) + " / " + Eval("Year")%></b>'>
                                            <a id="aDownload" runat="server" href="DownloadReport.aspx" target="_self">
                                                <img id="imgDownload" alt="Download" border="0" runat="server" src="~/Includes/Images/png/filesave-48.png" /></a>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar">
                                    <HeaderTemplate>
                                        <uc1:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar el reporte seleccionado. " />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgBtnDelete" runat="server" OnClick="imgBtnDelete_OnClick"
                                            ImageUrl="~/Includes/Images/png/deleteIcon.png" CommandArgument='<%#Eval("Year") + "_" + Eval("Month")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </center>
                </span>
                <asp:Button ID="btnGeneratesReportBottom" runat="server" CssClass="Button" PostBackUrl="~/Reports/BillerMonthlyReport/BillerMonthlyReportGenerated.aspx"
                    Text="Generar Reporte" />
            </div>
            <div>
                <uc2:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
