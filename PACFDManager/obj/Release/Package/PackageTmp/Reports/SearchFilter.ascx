﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchFilter.ascx.cs"
    Inherits="PACFDManager.Reports.SearchFilter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Concepts/ProductTypes.ascx" TagName="ProductTypes" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>

<script src="SearchFilterScript.js" type="text/javascript"></script>

<div id="dvReport_001" runat="server">
    <ul style="overflow:inherit">
        <li class="left">
            <label class="desc">
                <asp:Label ID="lblConsumer" runat="server" Text="Cliente:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona un cliente especifico ó selecciona <b>Todos</b><br />para obtener un reporte con información más generalizada.">
                <asp:DropDownList ID="ddlConsumer" runat="server">
                </asp:DropDownList>
            </span></li>
        <li class="left">
            <label class="desc">
                <asp:Label ID="lblStatus" runat="server" Text="Estatus del Comprobante:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona uno de los 2 estados del comprobante <b>Activo</b> ó <b>No Activo</b>,<br />selecciona <b>Todos</b> para generar el reporte con comprobantes en ambos estados.">
                <asp:DropDownList ID="ddlStatus" runat="server">
                    <asp:ListItem Value="Todos">[Todos]</asp:ListItem>
                    <asp:ListItem Selected="True">Activo</asp:ListItem>
                    <asp:ListItem>No Activo</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li id="liBillType" runat="server" class="left">
            <label class="desc">
                <asp:Label ID="lblCFDState" runat="server" Text="Tipo de Comprobante:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona uno de los 2 tipos de comprobante <b>Comprobante</b> ó <b>Pre-comprobante</b>,<br />selecciona <b>Todos</b> para generar el reporte con comprobantes de ambos tipos.">
                <asp:DropDownList ID="ddlCFDState" runat="server">
                    <asp:ListItem Value="Todos">[Todos]</asp:ListItem>
                    <asp:ListItem>Pre-Comprobante</asp:ListItem>
                    <asp:ListItem>Comprobante</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li>
            <label class="desc">
                <asp:Label ID="lblIsCredit" runat="server" Text="De Contado:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona <b>Si</b> en caso de que el comprobante sea de Contado, en caso de que<br />el comprobante sea a Crédito selecciona <b>No</b>, selecciona <b>Todos</b> para generar<br />un reporte con ambos comprobantes.">
                <asp:DropDownList ID="ddlIsCredit" runat="server">
                    <asp:ListItem Value="Todos">[Todos]</asp:ListItem>
                    <asp:ListItem>Si</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li class="left">
            <label class="desc">
                <asp:Label ID="lblUMC" runat="server" Text="Tipo de Producto:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona una <b>UMC (Unidad de Medida de Comercialización)</b> especifica<br />ó selecciona <b>Todos</b> para obtener un reporte con información más generalizada.">
                <asp:DropDownList ID="ddlProductTypes" runat="server">
                    <asp:ListItem Value="Todos">[Todos]</asp:ListItem>
                    <asp:ListItem>Barril</asp:ListItem>
                    <asp:ListItem>Botella</asp:ListItem>
                    <asp:ListItem>Cabeza</asp:ListItem>
                    <asp:ListItem>Caja</asp:ListItem>
                    <asp:ListItem>Cientos</asp:ListItem>
                    <asp:ListItem>Decenas</asp:ListItem>
                    <asp:ListItem>Docenas</asp:ListItem>
                    <asp:ListItem>Gramo</asp:ListItem>
                    <asp:ListItem>Gramo Neto</asp:ListItem>
                    <asp:ListItem>Juego</asp:ListItem>
                    <asp:ListItem>Kilo</asp:ListItem>
                    <asp:ListItem>Kilowatt</asp:ListItem>
                    <asp:ListItem>Kilowatt/Hora</asp:ListItem>
                    <asp:ListItem>Litro</asp:ListItem>
                    <asp:ListItem>Metro Cuadrado</asp:ListItem>
                    <asp:ListItem>Metro Cubico</asp:ListItem>
                    <asp:ListItem>Metro Lineal</asp:ListItem>
                    <asp:ListItem>Millar</asp:ListItem>
                    <asp:ListItem>Paquete</asp:ListItem>
                    <asp:ListItem>Par</asp:ListItem>
                    <asp:ListItem>Pieza</asp:ListItem>
                    <asp:ListItem>Servicio</asp:ListItem>
                    <asp:ListItem>Tonelada</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li>
            <label class="desc">
                <asp:Label ID="lblProductDescription" runat="server" Text="Descripción del Producto:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona una descripción especifica ó selecciona <b>Todos</b><br />para obtener un reporte con información más generalizada.">
                <asp:DropDownList ID="ddlProductDescription" runat="server">
                </asp:DropDownList>
            </span></li>
        <li class="left">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <span class="chBox"><span class="vtip" title="Marca la casilla <b>Incluir Fechas</b> para generar el reporte entre un rango de fechas especifico,<br />de lo contrario el reporte se generara con todas las fechas existentes.">
                            <asp:CheckBox ID="chkBoxIncludeDates" runat="server" Text="Incluir Fechas" Checked="True" /></span></span>
                    </td>
                </tr>
                <tr>
                    <td id="dvDates">
                        <label class="desc">
                            <asp:Label ID="lblDateRange" runat="server" Text="Rango de fechas:"></asp:Label>
                        </label>
                        <span class="vtip" title="Selecciona de que fecha a que fecha se generara en el reporte.<br />ej: Del <b>01/09/2010</b> al <b>30/09/2010</b>" >
                            <asp:TextBox ID="txtDateStart" runat="server" Width="70px"></asp:TextBox>
                            <asp:CalendarExtender ID="txtDateStart_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtDateStart" TodaysDateFormat="dd MM yyyy">
                            </asp:CalendarExtender>
                        </span><span>
                            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtDateStart"
                                ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </span><span>
                            <asp:Label ID="lblTo" runat="server" Text="al"></asp:Label></span> <span class="vtip"
                                title="Selecciona de que fecha a que fecha se generara en el reporte.<br />ej: Del <b>01/09/2010</b> al <b>30/09/2010</b>">
                                <asp:TextBox ID="txtDateEnd" runat="server" Width="70px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtDateEnd_CalendarExtender" runat="server" Enabled="True"
                                    TargetControlID="txtDateEnd">
                                </asp:CalendarExtender>
                            </span><span>
                                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtDateEnd"
                                    ErrorMessage="*" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator></span>
                    </td>
                </tr>
            </table>
        </li>
        <li class="buttons">
            <div>
                <input id="btnReset" type="button" runat="server" class="Button" value="Limpiar" />
                <asp:Button ID="btnReport_001" runat="server" CssClass="Button" OnClick="btnGenerate_Click"
                    Text="Generar Reporte" ValidationGroup="Validators" />
            </div>
        </li>
    </ul>
</div>
<div id="dvReport_002" runat="server">
    <ul>
        <li class="left">
            <label class="desc">
                <asp:Label ID="lblProductType" runat="server" Text="Tipo de Producto:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona una <b>UMC (Unidad de Medida de Comercialización)</b> especifica<br />ó selecciona <b>Todos</b> para obtener un reporte con información más generalizada.">
                <asp:DropDownList ID="ddlProductTypes0" runat="server">
                    <asp:ListItem Value="Todos">[Todos]</asp:ListItem>
                    <asp:ListItem>Barril</asp:ListItem>
                    <asp:ListItem>Botella</asp:ListItem>
                    <asp:ListItem>Cabeza</asp:ListItem>
                    <asp:ListItem>Caja</asp:ListItem>
                    <asp:ListItem>Cientos</asp:ListItem>
                    <asp:ListItem>Decenas</asp:ListItem>
                    <asp:ListItem>Docenas</asp:ListItem>
                    <asp:ListItem>Gramo</asp:ListItem>
                    <asp:ListItem>Gramo Neto</asp:ListItem>
                    <asp:ListItem>Juego</asp:ListItem>
                    <asp:ListItem>Kilo</asp:ListItem>
                    <asp:ListItem>Kilowatt</asp:ListItem>
                    <asp:ListItem>Kilowatt/Hora</asp:ListItem>
                    <asp:ListItem>Litro</asp:ListItem>
                    <asp:ListItem>Metro Cuadrado</asp:ListItem>
                    <asp:ListItem>Metro Cubico</asp:ListItem>
                    <asp:ListItem>Metro Lineal</asp:ListItem>
                    <asp:ListItem>Millar</asp:ListItem>
                    <asp:ListItem>Paquete</asp:ListItem>
                    <asp:ListItem>Par</asp:ListItem>
                    <asp:ListItem>Pieza</asp:ListItem>
                    <asp:ListItem>Servicio</asp:ListItem>
                    <asp:ListItem>Tonelada</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li>
            <label class="desc">
                <asp:Label ID="AppliesTax" runat="server" Text="Aplica Impuesto:"></asp:Label>
            </label>
            <span class="vtip" title="Selecciona <b>Aplica</b> para obtener solo los productos donde aplique el impuesto<br />de lo contrario selecciona <b>No Aplica</b>, ó selecciona <b>Todos</b><br />para obtener un reporte con información más generalizada.">
                <asp:DropDownList ID="ddlAppliesTax" runat="server">
                    <asp:ListItem Value="-1" Selected="True">[Todos]</asp:ListItem>
                    <asp:ListItem Value="0">No Aplica</asp:ListItem>
                    <asp:ListItem Value="1">Aplica</asp:ListItem>
                </asp:DropDownList>
            </span></li>
        <li class="buttons">
            <div>
                <asp:Button ID="btnReport_002" runat="server" CssClass="Button" OnClick="btnGenerate_Click"
                    Text="Generar Reporte" />
            </div>
        </li>
    </ul>
</div>
<div id="dvReport_003" runat="server">
    <ul>
        <li class="buttons">
            <div>
                <asp:Button ID="btnReport_003" runat="server" Text="Generar Reporte" CssClass="Button"
                    OnClick="btnGenerate_Click" />
            </div>
        </li>
    </ul>
</div>
<div id="dvInvoiceReport" runat="server">
    <ul>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <li class="left">
                    <label class="desc">
                        Grupo:
                    </label>
                    <span>
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span></li>
                <li>
                    <label class="desc">
                        <asp:Label ID="lblBillerTitle" runat="server" Visible="false" Text="Empresa:" />
                    </label>
                    <span>
                        <asp:DropDownList ID="ddlBiller" Visible="false" runat="server">
                        </asp:DropDownList>
                    </span></li>
                <li class="left">
                    <label class="desc">
                        Tipo de Facturación:
                    </label>
                    <span>
                        <asp:DropDownList ID="ddlElectronicBillingType" runat="server">
                            <asp:ListItem Value="-1">[Todos]</asp:ListItem>
                            <asp:ListItem Value="0">CFD</asp:ListItem>
                            <asp:ListItem Value="1">CFDI</asp:ListItem>
                            <asp:ListItem Value="2">CBB</asp:ListItem>
                        </asp:DropDownList>
                    </span></li>
                <li><span>
                    <asp:CheckBox ID="cbxEnableDates" runat="server" Text="Incluir rango de fechas:"
                        Checked="true" AutoPostBack="True" OnCheckedChanged="cbxEnableDates_CheckedChanged" />
                </span></li>
                <li id="liDatesIR" runat="server" class="left">
                    <label class="desc">
                        Rango de Fechas:
                    </label>
                    <span>
                        <asp:TextBox ID="txtStart" runat="server" Width="70px" ReadOnly="True" />
                        <asp:CalendarExtender ID="txtStart_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtStart">
                        </asp:CalendarExtender>
                    </span><span>al</span><span>
                        <asp:TextBox ID="txtEnd" runat="server" Width="70px" ReadOnly="True" />
                        <asp:CalendarExtender ID="txtEnd_CalendarExtender" runat="server" Enabled="true"
                            TargetControlID="txtEnd">
                        </asp:CalendarExtender>
                    </span></li>
            </ContentTemplate>
        </asp:UpdatePanel>
        <li class="buttons">
            <div>
                <asp:Button ID="btnInvoiceReport" runat="server" Text="Generar Reporte" CssClass="Button"
                    OnClick="btnGenerate_Click" />
            </div>
        </li>
    </ul>
</div>
