<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="SendBillingMail.aspx.cs" Inherits="PACFDManager.Billings.SendBillingMail" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <label class="desc">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Enviar correo de comprobante." />
                        </h2>
                    </label>
                </div>
                <div>
                    <ul>
                        <li class="left">
                            <label class="desc">
                                <a class="vtip" title="Incluir los archivos en el correo electronico.">
                                    <asp:Label ID="lblIncludeFiles" runat="server" Text="Incluir archivos." />
                                </a>
                            </label>
                            <span>
                                <asp:CheckBox ID="ckbIncludeFiles" runat="server" Checked="true" />
                            </span></li>
                        <li class="left">
                            <label class="desc">
                                <a class="vtip" title="Incluir botones de descarga.">
                                    <asp:Label ID="lblDownloadButtons" runat="server" Text="Incluir botones de descarga." />
                                </a>
                            </label>
                            <span>
                                <asp:CheckBox ID="ckbDownloadButtons" runat="server" Checked="true" />
                            </span></li>
                    </ul>
                    <ul>
                        <li>
                            <label class="desc">
                                <a class="vtip" title="Destinatario(s) del correo electronico.">
                                    <asp:Label ID="lblTo" runat="server" Text="Para" />
                                </a>
                            </label>
                            <span><a class="vtip" title="Destinatario(s) del correo electronico.">
                                <asp:TextBox ID="txtTo" runat="server" Width="500px" /></a></span>
                            <br />
                            <div class="validator-msg">
                                <asp:RequiredFieldValidator ID="rfvTo" runat="server" ErrorMessage="Este campo es requerido"
                                    ControlToValidate="txtTo" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="txtTo_RegularExpressionValidator" runat="server"
                                    Display="Dynamic" ErrorMessage="La(s) direcci�n(es) de correo electronico es(son) incorrecta(s)."
                                    ControlToValidate="txtTo" ValidationExpression="^(\s*,?\s*[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})+\s*$" />
                            </div>
                        </li>
                        <li>
                            <label class="desc">
                                <a class="vtip" title="Mensaje a mostrar en el titulo del correo.">
                                    <asp:Label ID="lblDescription" runat="server" Text="Asunto" />
                                </a>
                            </label>
                            <span><a class="vtip" title="Mensaje a mostrar en el titulo del correo.">
                                <asp:TextBox ID="txtDescription" runat="server" Width="500px" />
                            </a></span></li>
                    </ul>
                </div>
                <div>
                    <a class="vtip" title="Enviar correo a destinatario(s).">
                        <asp:Button ID="btnAcept" runat="server" Text="Aceptar" OnClick="btnAcept_Click"
                            CssClass="Button" />
                    </a>&nbsp; <a class="vtip" title="Regresa al listado de (pre)comprobantes.">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
