<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="PrintTemplateList.aspx.cs" Inherits="PACFDManager.Billings.XML.PrintTemplateList" %>

<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de configuraciones de PDF" />
                    </h2>
                </div>
                <div>
                    <asp:Button ID="btnAdd" runat="server" Text="Agregar" OnClick="btnAdd_Click" CssClass="Button" />
                </div>
                <div>
                    <asp:GridView ID="gdvPrintTemplate" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay comprobantes.">
                        <Columns>
                            <asp:BoundField DataField="PrintTemplateID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Nombre" />
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <uc1:Tooltip ID="ttpTest" runat="server" ToolTip="Hacer click en la imagen<img src='../../Includes/Images/png/pdf.png' alt='Editar' /> para<br />ver la prueba de configuración de impreción." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Ver prueba de configuración de impresión<br />del PDF <b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imbTest" CommandArgument='<%#Eval("PrintTemplateID")%>' runat="server"
                                            OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/pdf.png" AlternateText="Prueba" /></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Editar">
                                <HeaderTemplate>
                                    <uc1:Tooltip ID="ttpEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar el archivo de impresión del PDF." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Modificar el archivo de impresión<br />del PDF <b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imbEdit" CommandArgument='<%#Eval("PrintTemplateID")%>' runat="server"
                                            OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/editIcon.png" AlternateText="Editar"
                                            Visible='<%# Eval("IsBase").ToString() == "True" ? Convert.ToBoolean("False") : Convert.ToBoolean("True") %>' /></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Eliminar">
                                <HeaderTemplate>
                                    <uc1:Tooltip ID="ttpDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/deleteIcon.png/ alt='Eliminar' /> para<br />elimina el archivo de impresión." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Eliminar el archivo de impresión<br />del PDF <b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imbDelete" CommandArgument='<%#Eval("PrintTemplateID")%>' runat="server"
                                            OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png" AlternateText="Eliminar"
                                            Visible='<%# Eval("IsBase").ToString() == "True" ? Convert.ToBoolean("False") : Convert.ToBoolean("True") %>' />
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="dgHeader" />
                        <RowStyle CssClass="dgItem" />
                        <AlternatingRowStyle CssClass="dgAlternate" />
                        <EmptyDataTemplate>
                            No hay comprobantes disponibles.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div>
                    <asp:Button ID="btnAdd2" runat="server" Text="Agregar" OnClick="btnAdd_Click" CssClass="Button" />
                </div>
            </div>
        </div>
    </div>
    <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
