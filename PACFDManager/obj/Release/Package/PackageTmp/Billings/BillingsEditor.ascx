﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsEditor.ascx.cs"
    Inherits="PACFDManager.Billings.BillingsEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<div class="facturaDivMain">
    <!--<script type="text/javascript" src="~/Billings/BillingsEditor.js"></script>-->
    <div class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblEmisorTitle" runat="server" Text="Datos de la empresa" />
        </div>
        <asp:Label ID="lblCompanyMainTitle" runat="server" Text="" CssClass="darkLabel" />
        <br />
        <asp:Label ID="lblCompanySubTitle" runat="server" Text="" />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyRFCTitle" runat="server" Text="RFC:" CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanyRFCText" runat="server" Text="" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCompabyBillingTitle" runat="server" Text="Serie y Folio:" CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanySerieFolio" runat="server" CssClass="label" />
                </td>
                <td>
                    <asp:Label ID="lblBillingExternalFolioTitle" runat="server" Text="Folio externo:"
                        CssClass="darkLabel" />
                    &nbsp;<a class="vtip" title="El <b>Folio externo</b> a usar en el (pre)comprobante (opcional).">
                        <asp:TextBox ID="txtBillingExternalFolioText" runat="server" MaxLength="15" />
                    </a>
                    <asp:Label ID="lblBillingExternalFolioText" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyYearAndFolioTitle" runat="server" Text="Año de aprobación:"
                        CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanyYear" runat="server" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyAprovalNumberTitle" runat="server" Text="Número de aprobación:"
                        CssClass="darkLabel" />&nbsp;
                    <asp:Label ID="lblCompanyAprovalNumberText" runat="server" Text="" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCertificateNumberSATTitle" runat="server" Text="Certificado del SAT:"
                        CssClass="darkLabel" Visible="false" />&nbsp;
                    <asp:Label ID="lblCertificateNumberSAT" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblUUIDTitle" runat="server" Text="Folio fiscal:" CssClass="darkLabel"
                        Visible="false" />&nbsp;
                    <asp:Label ID="lblUUID" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblStampedDateTitle" runat="server" Text="Fecha de estampado:" CssClass="darkLabel"
                        Visible="false" />&nbsp;
                    <asp:Label ID="lblStampedDate" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCertificateNumberTitle" runat="server" Text="Certificado de Empresa:"
                        CssClass="darkLabel" Visible="false" />&nbsp;
                    <asp:Label ID="lblCertificateNumber" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTaxSystemTitle" runat="server" Text="Régimen Fiscal:" CssClass="darkLabel"
                        Visible="false" />&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div id="divReceptor" class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblReceptorSearchTite" runat="server" Text="Datos del cliente" />
        </div>
        <div id="divReceptorSearch" runat="server">
            <div class="facturaDivSelect">
                <div class="title" style="margin-bottom: 10px;">
                    <asp:Label ID="lblReceptorSelectTitle" runat="server" Text="Selección de cliente" />
                </div>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblReceptorSearchTitle" runat="server" Text="Cliente:" CssClass="darkLabel" />
                        </td>
                        <td>
                            <a class="vtip" title="Escriba el nombre del cliente<br />a buscar para generar el comprobante.">
                                <asp:TextBox ID="txtReceptorSearch" runat="server" Width="373px" MaxLength="255" />
                            </a>
                            <asp:AutoCompleteExtender ID="txtReceptorSearch_AutoCompleteExtender" runat="server"
                                TargetControlID="txtReceptorSearch" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetReceptorByName"
                                MinimumPrefixLength="1" EnableCaching="true" UseContextKey="True">
                            </asp:AutoCompleteExtender>
                        </td>
                        <td>
                            <a class="vtip" title="Seleccionar un <b>cliente</b> a usar para<br />generar el comprobante digital.">
                                <asp:DropDownList ID="ddlReceptorSearch" runat="server" Width="300px">
                                </asp:DropDownList>
                            </a>
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="btnAceptReceptorSearch" runat="server" CssClass="Button" Text="Aceptar"
                                OnClick="btnAceptReceptorSearch_Click" CausesValidation="False" />
                            <span>&nbsp; <a class="vtip" title="Agrega un <b>nuevo cliente</b> a la lista de clientes.">
                                <asp:Button ID="btnReceptorAddNew" runat="server" OnClick="btnReceptorAddNew_Click"
                                    Text="Agregar nuevo" CssClass="Button" CausesValidation="False" />
                            </a></span>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gdvReceptor" runat="server" CssClass="DataGrid" AutoGenerateColumns="false"
                    EmptyDataText="No hay receptores.">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Nombre" />
                        <asp:BoundField DataField="Address" HeaderText="Dirección" />
                        <asp:BoundField DataField="RFC" HeaderText="RFC" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnReceptorSelected" runat="server" Text="Seleccionar" CommandArgument='<%#Eval("ReceptorID") %>'
                                    OnClick="btnReceptorSelected_Click" CssClass="Button" CausesValidation="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                </asp:GridView>
            </div>
        </div>
        <br />
        <!--Receptor Name layout-->
        <table style="text-align: left">
            <tr>
                <td>
                    <asp:Label ID="lblReceptorNameTitle" runat="server" Text="Nombre:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorNameText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor RFC layout-->
            <tr>
                <td>
                    <asp:Label ID="lblRceptorRFCTitle" runat="server" Text="RFC:" CssClass="darkLabel" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorRFCText" runat="server" CssClass="label" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorCPTitle" runat="server" Text="C.P.:" CssClass="darkLabel" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorCPText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor Address layout-->
            <tr>
                <td>
                    <asp:Label ID="lblReceptorAddressTitle" runat="server" Text="Dirección:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorAddressText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor City layout-->
            <tr>
                <td>
                    <asp:Label ID="lblReceptorCityTitle" runat="server" Text="Ciudad:" CssClass="darkLabel" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorCityText" runat="server" Text="" CssClass="label" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorFiscalRegimeTitle" runat="server" Text="Regimen Fiscal:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorFiscalRegimeText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
        </table>
        <%-- <asp:Button ID="btnSelectAnotherReceptor" runat="server" Text="Seleccionar otro"
                CssClass="Button" OnClick="btnSelectAnotherReceptor_Click" Visible="false" />--%>
        <br />
        <a class="vtip" title="<b>Deselecciona</b> el cliente actual<br />y permite seleccionar <b>otro</b>.">
            <asp:LinkButton ID="btnSelectAnotherReceptor" runat="server" Text="Seleccionar otro"
                CssClass="Button" OnClick="btnSelectAnotherReceptor_Click" Visible="false" CausesValidation="false" />
        </a>&nbsp; &nbsp; <a class="vtip" title="<b>Edita</b> el cliente actual.">
            <asp:LinkButton ID="btnClientEdit" runat="server" Text="Editar cliente" CssClass="Button"
                OnClick="btnClientEdit_Click" Visible="false" CausesValidation="false" />
        </a>
        <br />
    </div>
    <div class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblBillingDataTitle" runat="server" Text="Datos del comprobante" />
        </div>
        <table style="text-align: left">
            <!--Receptor Conditions layout-->
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Forma de pago:" CssClass="darkLabel" />
                </td>
                <td>
                    <a class="vtip" title="Forma de <b>pago</b> a usar en el (pre)comprobante.">
                        <asp:DropDownList ID="ddlPaymentForm" runat="server">
                            <asp:ListItem Value="PUE">Pago en una sola exhibición</asp:ListItem>
                            <asp:ListItem Value="PPD">Pago en parcialidades o diferido</asp:ListItem>
                        </asp:DropDownList>
                        <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="ddlPaymentForm_RequiredFieldValidator" runat="server"
                                Display="Dynamic" ControlToValidate="ddlPaymentForm" InitialValue="0" ErrorMessage="Forma de pago requerido." />
                        </div>
                    </a>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPaymentType" runat="server" Text="Metodo de pago:" CssClass="darkLabel" />
                </td>
                <td>
                    <a class="vtip" title="Forma de <b>pago</b> a usar en el (pre)comprobante.">
                        <asp:DropDownList ID="ddlPaymentMethod" runat="server">
                            <asp:ListItem Value="0">[Seleccionar]</asp:ListItem>
                            <asp:ListItem Value="99">Por definir</asp:ListItem>
                            <asp:ListItem Value="01">Efectivo</asp:ListItem>
                            <asp:ListItem Value="02">Cheques Nominativos</asp:ListItem>
                            <asp:ListItem Value="03">Transferencias electronicas de Fondos</asp:ListItem>
                            <asp:ListItem Value="04">Tarjeta de Crédito</asp:ListItem>
                            <asp:ListItem Value="05">Monedero Electronico</asp:ListItem>
                            <asp:ListItem Value="28">Tarjeta de Debito</asp:ListItem>
                            <asp:ListItem Value="29">Tarjeta de Servicio</asp:ListItem>
                            <asp:ListItem Value="30">Aplicación de anticipos</asp:ListItem>
							<asp:ListItem Value="31">Intermediario Pagos</asp:ListItem>
                        </asp:DropDownList>
                        <div class="validator-msg">
                            <asp:RequiredFieldValidator ID="ddlPaymentMethod_RequiredFieldValidator" runat="server"
                                Display="Dynamic" ControlToValidate="ddlPaymentMethod" InitialValue="0" ErrorMessage="Metodo de pago requerido." />
                        </div>
                    </a>
                </td>
                <td>
                    <asp:Label ID="lblIsCredit" runat="server" Text="Es de contado:" CssClass="darkLabel" />
                    &nbsp; <a class="vtip" title="Indica si el metodo de pago es de <b>contado</b> o <b>no</b>.">
                        <asp:DropDownList ID="ddlIsCredit" runat="server">
                            <asp:ListItem>Si</asp:ListItem>
                            <asp:ListItem>No</asp:ListItem>
                        </asp:DropDownList>
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAccountNumberPayment" runat="server" Text="Información adicional al metodo de pago:"
                        CssClass="darkLabel" />
                </td>
                <td>
                    <a class="vtip" title="Atributo Opcional para incorporar al menos los cuatro últimos digitos del número de cuenta con la que se realizó el pago.">
                        <asp:TextBox ID="txtAccountNumberPayment" runat="server" MaxLength="50" />
                    </a>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblReceptorConditionsTitle" runat="server" Text="Condiciones:" CssClass="darkLabel" />
                </td>
                <td>
                    <a class="vtip" title="<b>Condiciones de pago</b> a usar en el (pre)comprobante.">
                        <asp:TextBox ID="txtReceptorConditions" runat="server" MaxLength="50" />
                    </a>
                </td>
                <td>
                    <div runat="server" id="divDateSelector" visible="false">
                        <asp:Label ID="lblDateSelector" runat="server" Text="Fecha de comprobante:" CssClass="darkLabel" />
                        &nbsp; <a class="vtip" title="Selecciona la fecha de creación<br/>de la factura. en formato dd/MM/yyyy hh:mm tt">
                            <asp:CheckBox ID="ckbDateSelector" runat="server" OnCheckedChanged="ckbDateSelector_CheckedChanged"
                                CausesValidation="false" AutoPostBack="true" />
                            <br />
                            <asp:TextBox ID="txtDateSelector" runat="server" Visible="false " />
                        </a>
                        <asp:CalendarExtender ID="txtDateSelector_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtDateSelector">
                        </asp:CalendarExtender>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCurrencyType" runat="server" Text="Factura en:" CssClass="darkLabel" />
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0" style="text-align: left;">
                        <tr>
                            <td style="width: 0px">
                                <a class="vtip" title="Tipo de moneda.">
                                    <asp:DropDownList ID="ddlCurrencyType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurrencyType_SelectedIndexChanged">
                                        <asp:ListItem Value="0">MXN</asp:ListItem>
                                        <asp:ListItem Value="1">USD</asp:ListItem>
                                    </asp:DropDownList>
                                </a>
                            </td>
                            <td align="left">
                                <span id="spanExchangeRate" runat="server" style="display: block;"><a class="vtip"
                                    title="Tipo de cambio para<br/>convertir a pesos.">=
                                    <asp:TextBox ID="txtExchangeRate" runat="server" Width="50px" MaxLength="10" />
                                    MXN </a></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div class="validator-msg">
                        <asp:RangeValidator ID="txtExchangeRate_RangeValidator" runat="server" ErrorMessage="Solo decimales."
                            ControlToValidate="txtExchangeRate" Display="Dynamic" Type="Double" MinimumValue="0.00" />
                    </div>
                </td>
            </tr>
        </table>
        <table id="tablePlaceDispatch" runat="server" cellpadding="0" cellspacing="0" style="text-align: left">
            <tr>
                <td>
                    <asp:Label ID="lblPlaceDispatch" runat="server" Text="Lugar de Expedición:" CssClass="darkLabel" />
                </td>
                <td style="text-align: left">
                    <a class="vtip" title="Lugar de expedición.">
                        <asp:DropDownList ID="ddlPlaceDispatch" runat="server" DataTextField="PlaceDispatch"
                            DataValueField="PlaceDispatchID" />
                    </a>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div class="validator-msg">
                        <asp:RequiredFieldValidator ID="ddlPlaceDispatch_RequiredFieldValidator" runat="server"
                            Display="Dynamic" ControlToValidate="ddlPlaceDispatch" InitialValue="0" ErrorMessage="Lugar de expedición requerido." />
                    </div>
                </td>
                <td>
                  
                </td>
            </tr>
        </table> 
        <!--  DATOS AGREGADO CFDI 4.0 PUBLICO EN GENERAL            -->         
        <div runat="server" id="divGlobalInvoiceData" visible="false">
            <asp:Label ID="lblGlobalInvoiceTitle" runat="server" Text="Factura Global a Publico en General" CssClass="title darkLabel" />
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblGlobalInvoicePeriod" runat="server" Text="Señale la Periodicidad:" CssClass="darkLabel" />
                    </td>
                    <td>
                        <asp:DropDownList ID="dblGlobalInvoicePeriod" runat="server" DataTextField="Descripcion"
                            DataValueField="c_Periodicidad" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblGlobalInvoiceMonth" runat="server" Text="Mes al que corresponde:" CssClass="darkLabel" />
                    </td>
                    <td>
                        <asp:DropDownList ID="dblGlobalInvoiceMonth" runat="server" DataTextField="Descripcion"
                            DataValueField="c_Meses" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblGlobalInvoiceYear" runat="server" Text="Año al que Corresponde:" CssClass="darkLabel" />
                    </td>
                    <td>
                        <asp:DropDownList ID="dblGlobalInvoiceYear" runat="server" DataTextField="Descripcion"
                            DataValueField="c_Periodicidad" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:GridView ID="gdvTaxes" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay impuestos configurados."
            ShowHeader="false" BorderWidth="0px" BorderStyle="None" Style="width: 100%">
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:RequiredFieldValidator ID="rfvGridTaxNeed" runat="server" ErrorMessage="*" Display="Dynamic"
                            ControlToValidate="txtGridTax" Visible='<%#Convert.ToBoolean(Eval("Editable")) && Convert.ToBoolean(Eval("Required"))%>' />
                        <a class="vtip" title='<%#"Detalles de impuesto.<br /><br /><b>Nombre: </b>" + (Eval("Transfer").ToString() == "True" ? ""  : "Ret. ").ToString() + Eval("Name").ToString() + "<br /><b>Requerido: </b>" + (Eval("Required").ToString() == "True" ? "Si" : "No").ToString() + "<br /><b>Valor inicial:</b> " + Eval("Value").ToString()%>'>
                            <asp:Label ID="lblGridTaxName" runat="server" Text='<%#(Eval("Transfer").ToString() == "True" ? ""  : "Ret. ").ToString() + Eval("Name").ToString() + ":"%>'
                                CssClass="darkLabel" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a class="vtip" title="Porcentajes de impuestos.">
                            <asp:DropDownList ID="ddlGridTax" runat="server" DataTextField="Value" DataTextFormatString="{0:0.00#######}"
                                AutoPostBack="true" BackColor="LightYellow" OnSelectedIndexChanged="Taxes_CalculateTotals" />
                        </a><a class="vtip" title="Escriba el valor del porcentaje.">
                            <asp:TextBox ID="txtGridTax" runat="server" Visible='<%#Convert.ToBoolean(Eval("Editable"))%>'
                                AutoPostBack="true" BackColor="LightYellow" OnTextChanged="Taxes_CalculateTotals" /></a><br />
                        <div class="validator-msg">
                            <asp:RangeValidator ID="rnvGridTaxValue" runat="server" ErrorMessage="Solo % (0.00 - 100.00)."
                                MaximumValue="100.00" MinimumValue="0.00" Type="Double" Visible='<%#Convert.ToBoolean(Eval("Editable"))%>'
                                ControlToValidate="txtGridTax" Display="Dynamic" />
                        </div>
                        <asp:HiddenField ID="hdfID" runat="server" Value='<%#Eval("TaxTypeID")%>' />
                        <asp:HiddenField ID="hdfEditable" runat="server" Value='<%#Eval("Editable")%>' />
                        <asp:HiddenField ID="hdfTaxTranfer" runat="server" Value='<%#Eval("Transfer")%>' />
                        <asp:HiddenField ID="hdfTaxIvaAffected" runat="server" Value='<%#Eval("IvaAffected")%>' />
                        <asp:HiddenField ID="hdfTaxRequired" runat="server" Value='<%#Eval("Required")%>' />
                        <asp:HiddenField ID="hdfTaxValue" runat="server" Value='<%#Eval("Value")%>' />

                        <asp:HiddenField ID="hdfFactorType" runat="server" Value='<%#Eval("FactorType")==null ? "" : Eval("FactorType")%>' />
                        <asp:HiddenField ID="hdfAmountBase" runat="server" Value='<%#Eval("AmountBase")==null ? "" : Eval("AmountBase")%>' />

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <table>
            <tr>
                <td>
                    <asp:GridView ID="gdvFiscalRegime" runat="server" AutoGenerateColumns="False" Visible="false">
                        <Columns>
                            <asp:BoundField DataField="FiscalRegimeID" Visible="False" />
                            <asp:BoundField DataField="Regime" HeaderText="Régimen(es) fiscal(es)" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblArticlesTitle" runat="server" Text="Productos" />
        </div>
        <div class="facturaDivSelect">
            <div class="title" style="margin-bottom: 10px;">
                <asp:Label ID="lblProductTitle" runat="server" Text="Selección de productos" />
            </div>
            <div id="divSearchArticle" runat="server">
                <table border="0" cellpadding="1" cellspacing="1" width="100%">
                    <tr>
                        <td>
                            <a class="vtip" title="Escriba en el campo <b>&quot;Descripción&quot;</b> para<br />buscar el artículo deseado. Ej.<br><b>•Pernos</b><br><b>•Tornillos</b><br><b>•Pantalla</b>">
                                <asp:Label ID="lblAddConceptDescription" runat="server" Text="Descripción" />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Código del producto.">
                                <asp:Label ID="lblAddConceptCode" runat="server" Text="Código" />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Tipo de producto.">
                                <asp:Label ID="lblAddConceptType" runat="server" Text="Tipo" />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Incluir aplicación de impuestos.">
                                <asp:Label ID="lblAddConceptApplyTaxes" runat="server" Text="Ap." />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Cantidad de productos a agregar.">
                                <asp:Label ID="lblAddConceptAmount" runat="server" Text="Cantidad" />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Cantidad de descuento a aplicar.">
                                <asp:Label ID="lblAddConceptDiscount" runat="server" Text="Dto. %" />
                            </a>
                        </td>
                        <td>
                            <a class="vtip" title="Cambiar el precio del producto.">
                                <asp:Label ID="lblAddConceptPrice" runat="server" Text="Precio $" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 20px;">
                                        <span id="spanNotConceptFound" runat="server"><a id="ainnerTip" class="vtip" title="No hay ningun concepto a buscar.<br />Escriba en el campo el concepto a buscar.">
                                            <img id="imgConceptNotFound" runat="server" src="../Includes/Images/png/alert__.png"
                                                alt="Conceptor no encontrado." />
                                        </a></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAddConceptDescription" runat="server" Width="400px" BackColor="lightyellow"
                                            TextMode="MultiLine" MaxLength="255" />
                                    </td>
                                </tr>
                            </table>
                            <asp:AutoCompleteExtender ID="txtAddConceptDescription_AutoCompleteExtender" runat="server"
                                BehaviorID="txtSearchArticle_AutoCompleteExtender" DelimiterCharacters=";,:"
                                Enabled="True" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetConceptsByBillersIDAndDescription"
                                FirstRowSelected="true" MinimumPrefixLength="1" EnableCaching="true" TargetControlID="txtAddConceptDescription"
                                CompletionInterval="1" UseContextKey="True" OnClientItemSelected="AutocompleteLocation_itemSelected" />
                            <asp:HiddenField ID="hflArticleSearch" runat="server" OnValueChanged="hflArticleSearch_ValueChanged" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddConceptCode" runat="server" Enabled="False" Width="50px" MaxLength="10" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddConceptType" runat="server" Enabled="false" Width="50px" MaxLength="30" />
                        </td>
                        <td>
                            <asp:CheckBox ID="ckbAddConceptApplyTaxes" runat="server" Enabled="false" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddConceptAmount" runat="server" Width="50px" Enabled="false"
                                Text="1" MaxLength="10" />
                            <div class="validator-msg">
                                <asp:RegularExpressionValidator ID="revAddConceptAmount2" runat="server" ErrorMessage="cantidad incorrecta"
                                    ControlToValidate="txtAddConceptAmount" Display="Dynamic" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(\.[0-9]{0,4})?$"
                                    ValidationGroup="ConcepAddValidationGroup"></asp:RegularExpressionValidator>
                            </div>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddConceptDiscount" runat="server" Width="50px" MaxLength="10" />
                            <div class="validator-msg">
                                <asp:RangeValidator ID="rgvAddconceptDiscount" runat="server" ErrorMessage="Solo decimales con &lt;br /&gt;2 digitos (0.0 - 100)."
                                    ControlToValidate="txtAddConceptDiscount" Display="Dynamic" Type="Double" MinimumValue="0.00"
                                    MaximumValue="100.00" ValidationGroup="ConcepAddValidationGroup" />
                            </div>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddConceptPrice" runat="server" Width="50px" Enabled="false"
                                MaxLength="10" />
                            <div class="validator-msg">
                                <asp:RangeValidator ID="revAddConceptPrice" runat="server" ErrorMessage="Solo decimales."
                                    ControlToValidate="txtAddConceptPrice" Display="Dynamic" Type="Double" MinimumValue="0.00"
                                    ValidationGroup="ConcepAddValidationGroup" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblStudentName" runat="server" Text="Nombre de almuno" Visible="false" />
                        </td>
                        <td colspan="4" >
                            <asp:Label ID="lblCurp" runat="server" Text="CURP del almuno" Visible="false" />
                        </td>
                        <td >
                            <asp:Label ID="lblAcademyLvl" runat="server" Text="Tipo de colegiatura" Visible="false" />
                        </td>
                        <td>
                            <asp:Label ID="lblAutRVOE" runat="server" Text="clave del centro de trabajo o el reconocimiento de validez oficial de estudios en los términos de la Ley General de Educación que tenga la institución educativa privada donde se realiza el pago" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="vtip" title="Nombre del alumno.">
                                <br />
                                 <asp:TextBox ID="txtStudentName" runat="server" Width="200px" Enabled="true"
                                    MaxLength="300" Visible="false"/>
                            </a>
                        </td>
                        <td colspan="4" >
                            <br />
                            <a class="vtip" title="Curp del alumno">
                                <asp:TextBox ID="txtCurp" runat="server" Width="149px" Enabled="true"
                                    MaxLength="18" Visible="false"/>
                        </td>
                        <td >
                            <br />
                            <asp:DropDownList ID="ddAcademyLvl" runat="server" Width="150px" Visible="false"> 
                                <asp:ListItem Value="Preescolar">Preescolar</asp:ListItem>
                                <asp:ListItem Value="Primaria">Primaria</asp:ListItem>
                                <asp:ListItem Value="Secundaria">Secundaria</asp:ListItem>
                                <asp:ListItem Value="Profesional técnico">Profesional técnico</asp:ListItem>
                                <asp:ListItem Value="Bachillerato o su equivalente">Bachillerato o su equivalente</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <a class="vtip" title=" clave del centro de trabajo o el reconocimiento de validez oficial de estudios en los términos de la Ley General de Educación que tenga la institución educativa privada donde se realiza el pago">
                                <br />
                                 <asp:TextBox ID="txtAutRVOE" runat="server" Width="150px" Enabled="true"
                                    MaxLength="300" Visible="false"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="vtip" title="Agregar un nuevo producto a la lista de productos.">
                                <br />
                                 <asp:DropDownList ID="ddlFactor" runat="server" Width="300px">
                                     <asp:ListItem Value="1">Tasa</asp:ListItem>
                                     <asp:ListItem Value="2">Cuota</asp:ListItem>
                                     <asp:ListItem Value="3">Exento</asp:ListItem>
                                </asp:DropDownList>
                            </a>
                        </td>
                        <td colspan="5" >
                            <br />
                            <a class="vtip" title="Agrega el producto a la lista de productos en el comprobante.">
                                <asp:DropDownList ID="ddlTaxType" runat="server" Width="150px">
                                    <asp:ListItem Value="002">IVA</asp:ListItem>
                                    <asp:ListItem Value="001">ISR</asp:ListItem>
                                    
                                    <asp:ListItem Value="003">IEPS</asp:ListItem>
                                </asp:DropDownList>
                        </td>
                        <td >
                            <br />
                            <asp:DropDownList ID="ddlTax" runat="server" Width="150px">
                                <asp:ListItem Value="0.16">16%</asp:ListItem>
                                <asp:ListItem Value="0">0%</asp:ListItem>
                                <asp:ListItem Value="0.003">1%</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="vtip" title="Agregar un nuevo producto a la lista de productos.">
                                <br />
                                <%--<asp:Button ID="btnConceptAddNewArticleToDadaBase" runat="server" Text="Nuevo producto a base de datos"
                                    CssClass="Button" OnClick="btnConceptAddNewArticleToDadaBase_Click" CausesValidation="False" />--%>
                                <asp:LinkButton ID="btnConceptAddNewArticleToDadaBase" runat="server" Text="Nuevo producto a base de datos"
                                    CssClass="Button" OnClick="btnConceptAddNewArticleToDadaBase_Click" CausesValidation="False" />
                            </a>
                        </td>
                        <td colspan="6" style="text-align: right">
                            <br />
                            <a class="vtip" title="Agrega el producto a la lista de productos en el comprobante.">
                                <%--<asp:Button ID="btnAddConcept" runat="server" CssClass="Button" Text="Agregar a comprobante"
                                    OnClick="btnAddConcept_Click" Enabled="False" />--%>
                                <asp:LinkButton ID="btnAddConcept" runat="server" CssClass="Button" Text="Agregar a comprobante"
                                    OnClick="btnAddConcept_Click" Enabled="False" ValidationGroup="ConcepAddValidationGroup" />
                            </a>&nbsp; <a class="vtip" title="Deselecciona el producto actualmente seleccionado.">
                                <%--<asp:Button ID="btnAddConceptCancel" runat="server" CssClass="Button" Text="seleccionar otro"
                                    Enabled="False" OnClick="btnAddConcept_Click" CausesValidation="False" />--%>
                                <asp:LinkButton ID="btnAddConceptCancel" runat="server" CssClass="Button" Text="seleccionar otro"
                                    Enabled="False" OnClick="btnAddConcept_Click" CausesValidation="False" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!--Article grid layout-->
        <div class="title" style="margin-bottom: 10px; margin-top: 10px;">
            <a class="vtip" title="Listado actual de productos en el comprobante.">
                <asp:Label ID="lblBillingArticlesTitle" runat="server" Text="Productos en el comprobante" />
            </a>
        </div>
        <asp:GridView ID="gdvArticles" runat="server" AutoGenerateColumns="false" CssClass="DataGrid"
            EmptyDataText="No hay productos." Width="100%">
            <Columns>
                <asp:BoundField HeaderText="Cantidad" DataField="Count" />
                <asp:BoundField HeaderText="Tipo" DataField="Name" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Descripción" DataField="Description" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="Descuento" DataField="Discount" DataFormatString="%{0}" />
                <asp:BoundField HeaderText="P. Unitario" DataField="Amount" DataFormatString="{0:C2}"
                    ItemStyle-HorizontalAlign="right" />
                <asp:BoundField HeaderText="Importe" DataField="ImporteTotal" DataFormatString="{0:C2}"
                    ItemStyle-HorizontalAlign="right" />
                <asp:TemplateField HeaderText="Apl Imp.">
                    <ItemTemplate>
                        <center>
                            <asp:CheckBox ID="ckbTaxes" runat="server" Enabled="false" Checked='<%#Eval("ApplyTaxes")%>'
                                AutoPostBack="true" />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="lblDeleteArticleFromListTitle" runat="server" Text="Borrar" Visible='<%#this.Enabled %>' />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:ImageButton ID="imbDelete" CommandArgument='<%#Container.DataItemIndex + 1%>'
                            runat="server" OnClick="ImageButton_Click" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                            AlternateText="Eliminar" CausesValidation="false" Visible="<%#this.Enabled%>" />
                        <asp:HiddenField ID="hflArticleID" runat="server" Value='<%#Eval("ArticleID")%>' />
                        <asp:HiddenField ID="hflBillingDetailsID" runat="server" Value='<%#Eval("BillingsDetailsID")%>' />
                        <asp:HiddenField ID="hflTaxRatePercentage" runat="server" Value='<%#Eval("TaxRatePercentage")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="dgHeader" />
            <RowStyle CssClass="dgItem" />
            <AlternatingRowStyle CssClass="dgAlternate" />
            <EmptyDataTemplate>
                No hay productos en el comprobante.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    <div id="TemporalNotCalculatedConcepNoVisible" runat="server" visible="false">
        <div id="divNotCalculableConcepts" runat="server">
            <div class="title">
                Conceptos detalle no calculables, estos conceptos NO AFECTA al calculo del total,
                subtotal, iva, etc.
            </div>
            <table id="tableNotCalculableConceps" runat="server">
                <tr>
                    <td>
                        Cantidad:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountNotCalculableConcept" runat="server" Width="50px" />
                    </td>
                    <td>
                        Codigo:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCodeNotCalculableConcept" runat="server" Width="50px" />
                    </td>
                    <td>
                        Descripción:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDescriptionNotCalculableConcept" runat="server" />
                    </td>
                    <td>
                        Precio:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPriceNotCalculableConcept" runat="server" Width="50px" />
                    </td>
                    <td>
                        Importe:
                    </td>
                    <td>
                        <asp:TextBox ID="txtImportNotCalculableConcept" runat="server" Width="50px" />
                    </td>
                    <td>
                        <asp:Button ID="btnNotCalculableConcept" runat="server" Text="Agregar" CssClass="Button"
                            CausesValidation="False" OnClick="btnNotCalculableConcept_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gdvNotCalculableConceps" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="Count" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="Code" HeaderText="Codigo" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="Description" HeaderText="Descripción" />
                    <asp:BoundField DataField="Price" HeaderText="P. Unitario" DataFormatString="{0:C2}"
                        ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="Import" HeaderText="Importe" DataFormatString="{0:C2}"
                        ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <span class="vtip" title="Click en <img src='../Includes/Images/png/deleteIcon.png' /> para elimina un concepto no calculable.">
                                Borrar </span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ImageUrl="~/Includes/Images/png/deleteIcon.png" runat="server" ID="imgbtnDeleteNotCalculableConcept"
                                CommandArgument='<%# Eval("BillingDetailNoteID") %>' OnClick="imgbtnDeleteNotCalculableConcept_Click"
                                CausesValidation="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <!--Billing totals and string total-->
    <div id="divTotals" runat="server" class="facturaDivContent">
        <div class="title">
            Totales</div>
        <!--Total layout style="width: 100%; text-align: left"-->
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td>
                    <a class="vtip" title="Cantidad en letra del total en el comprobante.">
                        <asp:Label ID="lblTotalByHand" runat="server" Text="Total en letra:" />
                    </a>
                    <br />
                    <asp:TextBox ID="txtTotalByHand" runat="server" TextMode="MultiLine" Width="550px"
                        Enabled="False" Height="139px" />
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                --------------------
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="vtip" title="Muestra la cantidad de los totales del comprobante.">
                                    <asp:Literal ID="ltrTotalsDetailView" runat="server" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                --------------------
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divObservation" runat="server" class="facturaDivContent">
        <div class="title">
            Observaciones
        </div>
        <br />
        <a class="vtip" title="<b>Observaciones</b> del (pre)comprobante (opcional).">
            <asp:TextBox ID="txtBillingObservation" runat="server" MaxLength="1000" TextMode="MultiLine"
                Width="550px" Height="70px" />
        </a>
    </div>
     <uc2:WebMessageBox ID="WebMessageBox1" runat="server"  />
</div>
