﻿<%@ Page Title="Modificar comprobante." Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="BillingsModify.aspx.cs" Inherits="PACFDManager.Billings.BillingsModify" %>

<%@ Register Src="BillingsEditor.ascx" TagName="BillingsEditor" TagPrefix="uc1" %>
<%@ Register Src="BillingsArticleSearch.ascx" TagName="BillingsArticleSearch" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblTitle" runat="server" Text="Facturación." Font-Size="24px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:BillingsEditor ID="BillingsEditor1" runat="server" />
                        <uc2:BillingsArticleSearch ID="BillingsArticleSearch1" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
