﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="AddendumList.aspx.cs" Inherits="PACFDManager.Billings.Addendum.AddendumList" %>

<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Listado de configuración de addenda" />
                    </h2>
                </div>
                <div>
                    <asp:Button ID="btnAdd" runat="server" Text="Agregar" CssClass="Button" CausesValidation="false"
                        PostBackUrl="~/Billings/Addendum/AddendumAdd.aspx" />
                </div>
                <br />
                <div>
                    <asp:GridView ID="gdvAddendum" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                        AllowPaging="true" PageSize="50">
                        <Columns>
                            <asp:BoundField DataField="AddendumID" HeaderText="ID" />
                            <asp:BoundField DataField="Name" HeaderText="Nombre" />
                            <asp:BoundField DataField="Description" HeaderText="Descripción" />
                            <asp:TemplateField HeaderText="Editar">
                                <HeaderTemplate>
                                    <uc2:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la configuración seleccionada." />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="vtip" title='Modificar la configuración de<br /><b><%#Eval("Name")%></b>'>
                                        <asp:ImageButton ID="imgbEdit" runat="server" ImageUrl="~/Includes/Images/png/editIcon.png"
                                            OnClick="ImageButton_Click" CommandArgument='<%#Eval("AddendumID")%>' CausesValidation="false" />
                                        <%--<asp:HiddenField ID="hdfTxTemplateID" runat="server" Value='<%-#Eval("TaxTemplateID")%>' />--%>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>                           
                        </Columns>
                    </asp:GridView>
                </div>
                <div>
                    <asp:Button ID="btnAdd2" runat="server" Text="Agregar" CausesValidation="false" CssClass="Button"
                        PostBackUrl="~/Billings/Addendum/AddendumAdd.aspx" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
