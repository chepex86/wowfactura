<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="AddendumApply.aspx.cs" Inherits="PACFDManager.Billings.Addendum.AddendumApply" %>

<%@ Register Src="../../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<%@ Register Src="../../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewSelectAddendum" runat="server">
            <div id="container" class="wframe80">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <label class="desc">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Addendar comprobante" />
                                </h2>
                            </label>
                        </div>
                        <ul>
                            <li>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Nombre del emisor
                                                    </h2>
                                                </label>
                                            </div>
                                            <asp:Label ID="lblBillerName" runat="server" Text="" />
                                        </td>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Nombre del receptor
                                                    </h2>
                                                </label>
                                            </div>
                                            <asp:Label ID="lblReceptorName" runat="server" Text="" />
                                        </td>
                                        <td>
                                            <div class="info">
                                                <label class="desc">
                                                    <h2>
                                                        Serie y/o folio
                                                    </h2>
                                                </label>
                                            </div>
                                            <asp:Label ID="lblSerialFolio" runat="server" Text="" />
                                        </td>
                                    </tr>
                                </table>
                            </li>
                        </ul>
                        <br />
                        <div>
                            <asp:GridView ID="gdvAddendum" runat="server" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                                AllowPaging="true" PageSize="50">
                                <Columns>
                                    <asp:BoundField DataField="AddendumID" HeaderText="ID" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Nombre">
                                        <HeaderTemplate>
                                            <span class="vtip" title='Muestra el <b>nombre</b> de la addenda.'>Nombre </span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("Name")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <HeaderTemplate>
                                            <span class="vtip" title='Muestra la <b>descripción</b> de la addenda.'>Descripción
                                            </span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("Description")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Editar">
                                        <HeaderTemplate>
                                            <uc2:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />aplicar la configuración seleccionada." />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <span class="vtip" title='Aplicar configuración <br /><b><%#Eval("Name")%></b> a comprobante.'>
                                                <asp:ImageButton ID="imgbEdit" runat="server" ImageUrl="~/Includes/Images/png/editIcon.png"
                                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("AddendumID")%>' CausesValidation="false" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewApplyData" runat="server">
            <div id="Div1" class="wframe80">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <label class="desc">
                                <h2>
                                    <asp:Label ID="Label1" runat="server" Text="Addendar comprobante" />
                                </h2>
                            </label>
                        </div>
                        <div id="divData" runat="server">
                        </div>
                        <br />
                        <div>
                            <asp:Button ID="btnAccept" runat="server" Text="Acaptar" OnClick="btnAccept_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CausesValidation="false"
                                OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
    <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
</asp:Content>
