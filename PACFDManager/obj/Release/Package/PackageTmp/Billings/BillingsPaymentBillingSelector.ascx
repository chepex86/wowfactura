﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsPaymentBillingSelector.ascx.cs" 
    Inherits="PACFDManager.Billings.BillingsPaymentBillingSelector" %>

<%@ Register Src="BillingsSearch.ascx" TagName="BillingsSearch" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
    <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblBillingsListTitle" runat="server" Text="Listado de Comprobantes" /></h2>
                </div>
                <span>
                    <asp:GridView ID="gdvBillings" runat="server" CssClass="DataGrid" AutoGenerateColumns="False" EmptyDataText="No hay comprobantes."
                        AllowPaging="true" PageSize="50" AllowSorting="true" PagerSettings-Position="Top" OnPageIndexChanging="gdvBillings_PageIndexChanging" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRow" runat="server" />
                                    <asp:Label ID="lblBillingID" runat="server" Text='<%#Eval("BillingID")%>' Visible="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <span class="vtip" title="Numero de control interno en el sistema." style="cursor: help;">
                                        <%# Eval("BillingID")  %>
                                    </span>
                                    <br />
                                    <span class="vtip" title="Muestra el numero se <b>serie</b> - <b>folio</b> usados."
                                        style="cursor: help; color: #007d00">
                                        <%#Eval("Serial") + "-" + Eval("Folio")%>
                                    </span>
                                    <br />
                                    <span class="vtip" title="Muestra el <b>folio externo</b>." style="cursor: help;
                                        color: #00638f">
                                        <%# Eval("ExternalFolio") %>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UUID">
                                <ItemTemplate>
                                    <asp:Label ID="lblUUID" runat="server" Text='<%#string.Format("{0}", Eval("UUID"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillingDate" runat="server" Text='<%#string.Format("{0:dddd dd/MMM/yyyy HH:mm:ss}", Eval("BillingDate")) %>' />
                                    <div id="divChangeDate" runat="server" visible='<%# (((int)PACFDManager.Security.Security.CurrentElectronicBillingType) == 2) %>'>
                                        <br />
                                        <span class="vtip" title="Modifica la fecha del <b>comprobante cbb</b>.">
                                            <asp:Button ID="ibtnChangeDate" runat="server" Text="Editar fecha" CssClass="Button"
                                                Visible='<%# (((int)PACFDManager.Security.Security.CurrentElectronicBillingType) == 2) %>'
                                                PostBackUrl='<%#string.Format("CBBDateModify.aspx?cbb={0}", PACFD.Common.Cryptography.EncryptUnivisitString(Eval("BillingID").ToString()))%>' />
                                        </span>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <span class="vtip" title='<%#string.Format("<b>{0}</b><br />Forma de pago: <b>{1}</b>.", PACFD.Common.Encode.HtmlEncode(Eval("ReceptorName").ToString()), Eval("PaymentForm")) %>'
                                        style="cursor: help;">
                                        <asp:Label ID="lblTotal" runat="server" Text='<%#string.Format("{0:c} {1}", Eval("Total"), Eval("CurrencyCode"))%>' />
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="dgHeader" />
                        <RowStyle CssClass="dgItem" />
                        <AlternatingRowStyle CssClass="dgAlternate" />

                    </asp:GridView>
                </span>
                
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: right">
                            <span class="vtip" title="Seleccionar <b></b>.">
                                <asp:Button ID="btnSelect" runat="server" Text="Selecionar facturas" OnClick="ImageButton_Click"
                                    CausesValidation="False" CssClass="Button" CommandArgument="0" />
                            </span>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <br />
            </div>