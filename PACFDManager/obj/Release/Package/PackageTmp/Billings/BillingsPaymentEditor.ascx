﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsPaymentEditor.ascx.cs"
    Inherits="PACFDManager.Billings.BillingsPaymentEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<div class="facturaDivMain">
    <script type="text/javascript" src="./BillingsEditor.js"></script>
    <div class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblEmisorTitle" runat="server" Text="Datos de la empresa" />
        </div>
        <asp:Label ID="lblCompanyMainTitle" runat="server" Text="" CssClass="darkLabel" />
        <br />
        <asp:Label ID="lblCompanySubTitle" runat="server" Text="" />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyRFCTitle" runat="server" Text="RFC:" CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanyRFCText" runat="server" Text="" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCompabyBillingTitle" runat="server" Text="Serie y Folio:" CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanySerieFolio" runat="server" CssClass="label" />
                </td>
                <td>
                    <asp:Label ID="lblBillingExternalFolioTitle" runat="server" Text="Folio externo:"
                        CssClass="darkLabel" />
                    &nbsp;<a class="vtip" title="El <b>Folio externo</b> a usar en el (pre)comprobante (opcional).">
                        <asp:TextBox ID="txtBillingExternalFolioText" runat="server" MaxLength="15" />
                    </a>
                    <asp:Label ID="lblBillingExternalFolioText" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyYearAndFolioTitle" runat="server" Text="Año de aprobación:"
                        CssClass="darkLabel" />
                    &nbsp;<asp:Label ID="lblCompanyYear" runat="server" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCompanyAprovalNumberTitle" runat="server" Text="Número de aprobación:"
                        CssClass="darkLabel" />&nbsp;
                    <asp:Label ID="lblCompanyAprovalNumberText" runat="server" Text="" CssClass="label" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCertificateNumberSATTitle" runat="server" Text="Certificado del SAT:"
                        CssClass="darkLabel" Visible="false" />&nbsp;
                    <asp:Label ID="lblCertificateNumberSAT" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblUUIDTitle" runat="server" Text="Folio fiscal:" CssClass="darkLabel"
                        Visible="false" />&nbsp;
                    <asp:Label ID="lblUUID" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblStampedDateTitle" runat="server" Text="Fecha de estampado:" CssClass="darkLabel"
                        Visible="false" />&nbsp;
                    <asp:Label ID="lblStampedDate" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblCertificateNumberTitle" runat="server" Text="Certificado de Empresa:"
                        CssClass="darkLabel" Visible="false" />&nbsp;
                    <asp:Label ID="lblCertificateNumber" runat="server" Text="" CssClass="label" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>

                </td>
            </tr>
        </table>
    </div>
    <div id="divReceptor" class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblReceptorSearchTite" runat="server" Text="Datos del cliente" />
        </div>
        <div id="divReceptorSearch" runat="server">
            <div class="facturaDivSelect">
                <div class="title" style="margin-bottom: 10px;">
                    <asp:Label ID="lblReceptorSelectTitle" runat="server" Text="Selección de cliente" />
                </div>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblReceptorSearchTitle" runat="server" Text="Cliente:" CssClass="darkLabel" />
                        </td>
                        <td>
                            <a class="vtip" title="Escriba el nombre del cliente<br />a buscar para generar el comprobante.">
                                <asp:TextBox ID="txtReceptorSearch" runat="server" Width="373px" MaxLength="255" />
                            </a>
                            <asp:AutoCompleteExtender ID="txtReceptorSearch_AutoCompleteExtender" runat="server"
                                TargetControlID="txtReceptorSearch" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetReceptorByName"
                                MinimumPrefixLength="1" EnableCaching="true" UseContextKey="True">
                            </asp:AutoCompleteExtender>
                        </td>
                        <td>
                            <a class="vtip" title="Seleccionar un <b>cliente</b> a usar para<br />generar el comprobante digital.">
                                <asp:DropDownList ID="ddlReceptorSearch" runat="server" Width="300px">
                                </asp:DropDownList>
                            </a>
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="btnAceptReceptorSearch" runat="server" CssClass="Button" Text="Aceptar"
                                OnClick="btnAceptReceptorSearch_Click" CausesValidation="False" />
                            <span>&nbsp; <a class="vtip" title="Agrega un <b>nuevo cliente</b> a la lista de clientes.">
                                <asp:Button ID="btnReceptorAddNew" runat="server" OnClick="btnReceptorAddNew_Click"
                                    Text="Agregar nuevo" CssClass="Button" CausesValidation="False" />
                            </a></span>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gdvReceptor" runat="server" CssClass="DataGrid" AutoGenerateColumns="false"
                    EmptyDataText="No hay receptores.">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Nombre" />
                        <asp:BoundField DataField="Address" HeaderText="Dirección" />
                        <asp:BoundField DataField="RFC" HeaderText="RFC" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnReceptorSelected" runat="server" Text="Seleccionar" CommandArgument='<%#Eval("ReceptorID") %>'
                                    OnClick="btnReceptorSelected_Click" CssClass="Button" CausesValidation="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                </asp:GridView>
            </div>
        </div>
        <br />
        <!--Receptor Name layout-->
        <table style="text-align: left">
            <tr>
                <td>
                    <asp:Label ID="lblReceptorNameTitle" runat="server" Text="Nombre:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorNameText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor RFC layout-->
            <tr>
                <td>
                    <asp:Label ID="lblRceptorRFCTitle" runat="server" Text="RFC:" CssClass="darkLabel" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorRFCText" runat="server" CssClass="label" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorCPTitle" runat="server" Text="C.P.:" CssClass="darkLabel" />
                </td>
                <td>
                    <asp:Label ID="lblReceptorCPText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor Address layout-->
            <tr>
                <td>
                    <asp:Label ID="lblReceptorAddressTitle" runat="server" Text="Dirección:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorAddressText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
            <!--Receptor City layout-->
            <tr>
                <td>
                    <asp:Label ID="lblReceptorCityTitle" runat="server" Text="Ciudad:" CssClass="darkLabel" />
                </td>
                <td colspan="3">
                    <asp:Label ID="lblReceptorCityText" runat="server" Text="" CssClass="label" />
                </td>
            </tr>
        </table>
        <%-- <asp:Button ID="btnSelectAnotherReceptor" runat="server" Text="Seleccionar otro"
                CssClass="Button" OnClick="btnSelectAnotherReceptor_Click" Visible="false" />--%>
        <br />
        <a class="vtip" title="<b>Deselecciona</b> el cliente actual<br />y permite seleccionar <b>otro</b>.">
            <asp:LinkButton ID="btnSelectAnotherReceptor" runat="server" Text="Seleccionar otro"
                CssClass="Button" OnClick="btnSelectAnotherReceptor_Click" Visible="false" CausesValidation="false" />
        </a>&nbsp; &nbsp; <a class="vtip" title="<b>Edita</b> el cliente actual.">
            <asp:LinkButton ID="btnClientEdit" runat="server" Text="Editar cliente" CssClass="Button"
                OnClick="btnClientEdit_Click" Visible="false" CausesValidation="false" />
        </a>
        <br />
    </div>
    <div class="facturaDivContent">
        <div class="title">
            <asp:Label ID="lblBillingDataTitle" runat="server" Text="Datos del comprobante" />
        </div>
        <table style="width: 100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <!--Receptor Conditions layout-->
                    <table>
                        <tr>
                            <td>
                                <div runat="server" id="divDateSelector">
                                    <asp:Label ID="lblDateSelector" runat="server" Text="Fecha de pago:" CssClass="darkLabel" />
                                    &nbsp; 
                                        <asp:TextBox ID="txtDateSelector" runat="server" Visible="True " />
                                    <asp:CalendarExtender ID="txtDateSelector_CalendarExtender" runat="server" Enabled="True"
                                        TargetControlID="txtDateSelector">
                                    </asp:CalendarExtender>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPaymentType" runat="server" Text="Metodo de pago:" CssClass="darkLabel" />
                                <a class="vtip" title="Forma de <b>pago</b> a usar en el (pre)comprobante.">
                                    <asp:DropDownList ID="ddlPaymentMethod" runat="server">
                                        <asp:ListItem Value="01">Efectivo</asp:ListItem>
                                        <asp:ListItem Value="05">Monedero Electronico</asp:ListItem>
                                        <asp:ListItem Value="02">Cheques Nominativos</asp:ListItem>
                                        <asp:ListItem Value="03">Transferencias electronicas de Fondos</asp:ListItem>
                                        <asp:ListItem Value="04">Tarjeta de Crédito</asp:ListItem>
                                        <asp:ListItem Value="28">Tarjeta de Debito</asp:ListItem>
                                        <asp:ListItem Value="29">Tarjeta de Servicio</asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="validator-msg">
                                        <asp:RequiredFieldValidator ID="ddlPaymentMethod_RequiredFieldValidator" runat="server"
                                            Display="Dynamic" ControlToValidate="ddlPaymentMethod" InitialValue="0" ErrorMessage="Metodo de pago requerido." />
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblReference" runat="server" Text="Número referencia:"
                                    CssClass="darkLabel" />
                                <a class="vtip" title="Numero de operación.">
                                    <asp:TextBox ID="txtReference" runat="server" MaxLength="100" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblBankName" runat="server" Text="Nombre banco:"
                                    CssClass="darkLabel" />
                                <a class="vtip" title="Atributo Opcional para incorporar ael nombre de banco.">
                                    <asp:TextBox ID="txtBankName" runat="server" MaxLength="50" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="rfcBancoOrdenante" runat="server" Text="Banco Ordenante: (Opcional)"
                                    CssClass="darkLabel" />
                                 <asp:TextBox ID="txtRfcEmisorOrd" runat="server" Width="400px" MaxLength="100" />

                                <asp:AutoCompleteExtender ID="txtInstRecepSearch_AutoCompleteExtender" runat="server"
                                    BehaviorID="txtInstRecepSearch_AutoCompleteExtender" DelimiterCharacters=";,;"
                                    Enabled="True" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetInstitutionsByName"
                                    FirstRowSelected="true" MinimumPrefixLength="1" EnableCaching="true" TargetControlID="txtRfcEmisorOrd"
                                    CompletionInterval="1" UseContextKey="True" OnClientItemSelected="AutocompleteLocation_itemSelected" />
                                <asp:HiddenField ID="hflInstSearch" runat="server" OnValueChanged="hflInstSearch_ValueChanged" />
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Número de cuenta Ordenante: (Opcional)"
                                    CssClass="darkLabel" />
                                 <asp:TextBox ID="txtNoCuentaOrd" runat="server" MaxLength="50" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Banco Beneficiario: (Opcional)"
                                    CssClass="darkLabel" />
                                <asp:TextBox ID="txtRfcEmisorBen" runat="server" Width="400px" MaxLength="100" />
                                        
                                <asp:AutoCompleteExtender ID="txtInstBenSearch_AutoCompleteExtender" runat="server"
                                    BehaviorID="txtInstBenSearch_AutoCompleteExtender" DelimiterCharacters=";,;"
                                    Enabled="True" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetInstitutionsByName"
                                    FirstRowSelected="true" MinimumPrefixLength="1" EnableCaching="true" TargetControlID="txtRfcEmisorBen"
                                    CompletionInterval="1" UseContextKey="True" OnClientItemSelected="AutocompleteLocation_itemSelected" />
                                <asp:HiddenField ID="HiddenField1" runat="server" OnValueChanged="hflInstSearch_ValueChanged" />
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Número de cuenta Beneficiario: (Opcional)"
                                    CssClass="darkLabel" />
                                <asp:TextBox ID="txtNoCuentaBen" runat="server" MaxLength="50" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Cantidad:"
                                    CssClass="darkLabel" />
                                <a class="vtip" title="Cantidad a pagar.">
                                    <asp:TextBox ID="txtTotal" runat="server" MaxLength="50" />
                                </a>
                            </td>
                            <td>
                                <asp:Label ID="lblCurrencyType" runat="server" Text="Factura en:" CssClass="darkLabel" />
                                <table cellpadding="0" cellspacing="0" style="text-align: left;">
                                    <tr>
                                        <td style="width: 0px">
                                            <a class="vtip" title="Tipo de moneda.">
                                                <asp:DropDownList ID="ddlCurrencyType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurrencyType_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">MXN</asp:ListItem>
                                                    <asp:ListItem Value="1">USD</asp:ListItem>
                                                </asp:DropDownList>
                                            </a>
                                        </td>
                                        <td align="left">
                                            <span id="spanExchangeRate" runat="server" style="display: block;"><a class="vtip"
                                                title="Tipo de cambio para<br/>convertir a pesos.">=
                                                <asp:TextBox ID="txtExchangeRate" runat="server" Width="50px" MaxLength="10" />
                                                MXN </a></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPlaceDispatch" runat="server" Text="Lugar de Expedición:" CssClass="darkLabel" />
                                <a class="vtip" title="Lugar de expedición.">
                                                <asp:DropDownList ID="ddlPlaceDispatch" runat="server" DataTextField="PlaceDispatch"
                                                    DataValueField="PlaceDispatchID" />
                                            </a>
                                <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="ddlPlaceDispatch_RequiredFieldValidator" runat="server"
                                                    Display="Dynamic" ControlToValidate="ddlPlaceDispatch" InitialValue="0" ErrorMessage="Lugar de expedición requerido." />
                                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gdvFiscalRegime" runat="server" AutoGenerateColumns="False" Visible="false">
                                                <Columns>
                                                    <asp:BoundField DataField="FiscalRegimeID" Visible="False" />
                                                    <asp:BoundField DataField="Regime" HeaderText="Régimen(es) fiscal(es)" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="validator-msg">
                                    <asp:RangeValidator ID="txtExchangeRate_RangeValidator" runat="server" ErrorMessage="Solo decimales."
                                        ControlToValidate="txtExchangeRate" Display="Dynamic" Type="Double" MinimumValue="0.00" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="float: left;" >
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalAmountTitle" runat="server" Text="Total:" />
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblTotalAmountText" runat="server" Text="0.00" /> MXN
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label ID="lblSubAmountTitle" runat="server" Text="Abonado:" /> 
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblSubAmountText" runat="server" Text="$0.00" /> MXN
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-bottom-style: dashed; border-bottom-width: 1px">
                                <!--row used only for style-->
                                &nbsp;
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label ID="lblTotalMinusTitle" runat="server" Text="Restan:" Font-Bold="true" />
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblTotalMinusText" runat="server" Text="0.00" Font-Bold="true" /> MXN
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
    </div>
    <div id="divXmls" class="facturaDivContent">
        <div class="title" style="margin-bottom: 10px; margin-top: 10px;">
            <a class="vtip" title="Listado actual de facturas en el comprobante.">
                <asp:Label ID="lblBillings" runat="server" Text="Facturas en el comprobante" />
            </a>
        </div>
        <asp:GridView ID="gvSelectedBillings" runat="server" AutoGenerateColumns="false" CssClass="DataGrid"
            EmptyDataText="No hay facturas." Width="100%">
            <Columns>
                <asp:BoundField HeaderText="UUID" DataField="UUID" />
                <asp:BoundField HeaderText="Total" DataField="Total" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText="BillingDate" DataField="BillingDate" ItemStyle-HorizontalAlign="Left" />
            </Columns>
            <HeaderStyle CssClass="dgHeader" />
            <RowStyle CssClass="dgItem" />
            <AlternatingRowStyle CssClass="dgAlternate" />
            <EmptyDataTemplate>
                No hay facturas en el comprobante.
            </EmptyDataTemplate>
        </asp:GridView>
        <br />
    </div>

    <div id="divObservation" runat="server" class="facturaDivContent">
        <div class="title">
            Observaciones
        </div>
        <br />
        <a class="vtip" title="<b>Observaciones</b> del (pre)comprobante (opcional).">
            <asp:TextBox ID="txtBillingObservation" runat="server" MaxLength="1000" TextMode="MultiLine"
                Width="550px" Height="70px" />
        </a>
    </div>
</div>
