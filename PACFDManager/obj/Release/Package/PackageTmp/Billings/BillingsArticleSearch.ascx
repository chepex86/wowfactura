﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsArticleSearch.ascx.cs"
    Inherits="PACFDManager.Billings.BillingsArticleSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<table>
    <!--Article to search layout-->
    <tr>
        <td>
            <!--Article to search layout-->
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="lblArticle" runat="server" Text="Articulo:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtArticle" runat="server" Width="100%"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="txtArticle_AutoCompleteExtender" runat="server" TargetControlID="txtArticle"
                            ServicePath="~/AutoComplete.asmx" ServiceMethod="GetConceptsByBillersIDAndDescription"
                            MinimumPrefixLength="1" EnableCaching="true" UseContextKey="True">
                        </asp:AutoCompleteExtender>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!---->
    <tr>
        <td style="text-align: right">
            <asp:Button ID="btnSelect2" runat="server" Text="Seleccionar" OnClick="btnSelect_Click"
                CausesValidation="true" />
            &nbsp;<asp:Button ID="btnSearch" runat="server" Text="Buscar" OnClick="btnSearch_Click" />
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <asp:GridView ID="gdvArticles" runat="server" CssClass="DataGrid" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="Nombre" DataField="UnitType" />
                    <asp:BoundField HeaderText="Nombre" DataField="Description" />
                    <asp:BoundField HeaderText="Costo" DataField="UnitPrice" DataFormatString="{0:C2}" />
                    <asp:TemplateField HeaderText="Cantidad">
                        <ItemTemplate>
                            <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ErrorMessage="*" ControlToValidate="txtAmount"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtAmount" Width="50px" Text="1" runat="server" MaxLength="8"></asp:TextBox>
                            <br />
                            <asp:RegularExpressionValidator ID="revAmount" runat="server" ErrorMessage="Cantidad erronea."
                                Display="Dynamic" ControlToValidate="txtAmount" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Seleccionar">
                        <ItemTemplate>
                            <asp:CheckBox ID="ckbSelect" runat="server" Text="Seleccionar" />
                            <asp:HiddenField ID="hflID" runat="server" Value='<%#Eval("ConceptID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="dgHeader" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
            </asp:GridView>
        </td>
    </tr>
    <!--Cancel command buttons-->
    <tr>
        <td style="text-align: right">
            <br />
            <asp:Button ID="btnSelect" runat="server" Text="Seleccionar" OnClick="btnSelect_Click"
                CausesValidation="true" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
                CausesValidation="false" />
        </td>
    </tr>
</table>
