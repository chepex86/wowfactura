﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingsSearch.ascx.cs"
    Inherits="PACFDManager.Billings.BillingsSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../PeriodChooser.ascx" TagName="PeriodChooser" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<div id="container" class="wframe90">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblSearchTitle" runat="server" Text="Busqueda de Comprobantes" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <div class="chBox">
                        <a class="vtip" title="Incluir en busqueda la fecha de expedición.">
                            <asp:CheckBox ID="ckbSearchDate" runat="server" Text="Buscar por fecha" TextAlign="Right" />
                        </a>
                    </div>
                    <span><a class="vtip" title="Si esta opción esta seleccionada, <br />la fecha es incluida en la búsqueda.">
                        <uc1:PeriodChooser ID="PeriodChooser1" runat="server" />
                    </a></span>
                    <%--<span class="">
                        <uc2:Tooltip ID="ttpPeriodChooser" runat="server" ToolTip="Si esta opción esta seleccionada, <br />la fecha es incluida en la búsqueda." />
                    </span>--%></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblFolioSearcgTitle" runat="server" Text="Buscar por folio" />
                    </label>
                    <span><a class="vtip" title="Incluir en búsqueda la serie.">
                        <asp:DropDownList ID="ddlSerie" runat="server">
                        </asp:DropDownList>
                    </a></span>
                    <%--<span>
                        <uc2:Tooltip ID="ttpSerie" runat="server" ToolTip="Búsqueda por serie y folio." />
                    </span>--%>
                    <span>
                        <div id="divFolioField" runat="server" style="display: none">
                            <table>
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="lblFolioTitle" runat="server" Text="Numero:" />
                                    </td>
                                    <td>
                                        &nbsp;<asp:TextBox ID="txtFolio" runat="server" MaxLength="4" Width="30px" />
                                        <br />
                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtFolio"
                                            Display="Dynamic" ErrorMessage="Formato incorecto (0 - 9999)" MaximumValue="9999"
                                            MinimumValue="0" Type="Integer" />
                                    </td>
                                    <td>
                                        <uc2:Tooltip ID="ttpFolio" runat="server" ToolTip="Si el número de folio esta en blanco,<br />la búsqueda regresara todos los folios<br />con la serie seleccionada." />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblReceptor" runat="server" Text="Buscar por cliente" />
                    </label>
                    <span><a class="vtip" title="Incluir en búsqueda el nombre del cliente.">
                        <asp:TextBox ID="txtReceptor" runat="server" />
                    </a></span><span>
                        <asp:AutoCompleteExtender ID="txtReceptor_AutoCompleteExtender" runat="server" TargetControlID="txtReceptor"
                            ServicePath="~/AutoComplete.asmx" MinimumPrefixLength="1" EnableCaching="true"
                            UseContextKey="True" ServiceMethod="GetReceptorByName">
                        </asp:AutoCompleteExtender>
                        <%--<uc2:Tooltip ID="ttpReceptor" runat="server" ToolTip="Búsqueda por el nombre del cliente." />--%>
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblPrebilling" runat="server" Text="Búsqueda por (pre)comprobante" />
                    </label>
                    <span><a class="vtip" title="Incluir en búsqueda el tipo de comprobante.">
                        <asp:DropDownList ID="ddlPreBillingType" runat="server">
                            <asp:ListItem Value="0" Selected="true" Text="[Todos]" />
                            <asp:ListItem Value="1" Text="Precomprobante" />
                            <asp:ListItem Value="2" Text="Comprobante" />
                        </asp:DropDownList>
                    </a></span>
                    <%--<uc2:Tooltip ID="ttpIsPreBilling" runat="server" ToolTip="Búsqueda por tipo de comprobante." />--%>
                </li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblIsPaid" runat="server" Text="Pagado" />
                    </label>
                    <span>
                        <asp:DropDownList ID="ddlIsPaid" runat="server">
                            <asp:ListItem Selected="True" Text="[Todos]" Value="0" />
                            <asp:ListItem Text="Si" Value="1" />
                            <asp:ListItem Text="No" Value="2" />
                        </asp:DropDownList>
                    </span></li>
                <li id="liUUID" runat="server" class="left">
                    <label class="desc">
                        <asp:Label ID="lblUUID" runat="server" Text="UUID" />
                    </label>
                    <span>
                        <asp:TextBox ID="txtUUID" runat="server" />
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblExternalFolio" runat="server" Text="Folio Externo" />
                    </label>
                    <span>
                        <asp:TextBox ID="txtExternalFolio" runat="server" MaxLength="15" />
                    </span></li>
                <li class="left">
                    <label class="desc">
                        Internal ID
                    </label>
                    <span>
                        <asp:TextBox ID="txtBillingID" runat="server" MaxLength="15" />
                    </span></li>
            </ul>
            <div style="text-align: right">
                <asp:Button ID="btnClean" runat="server" Text="Limpiar" OnClick="btnSearch_Click"
                    CssClass="Button_Search" />
                <asp:Button ID="btnSearchByReceptor" runat="server" Text="Buscar" CssClass="Button_Search"
                    OnClick="btnSearch_Click" />
            </div>
            <br />
        </div>
    </div>
</div>
