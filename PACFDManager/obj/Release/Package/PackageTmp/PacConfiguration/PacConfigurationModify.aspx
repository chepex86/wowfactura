<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="PacConfigurationModify.aspx.cs" Inherits="PACFDManager.PacConfiguration.PacConfigurationModify" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe70">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <label class="desc">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Configuración de PAC" />
                                </h2>
                            </label>
                        </div>
                        <div>
                            <asp:CheckBox ID="ckbDisabled" runat="server" Text="Activo" AutoPostBack="True" Checked="True"
                                OnCheckedChanged="ckbDisabled_CheckedChanged" />
                        </div>
                        <ul>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblUsername" runat="server" Text="Nombre de Usuario:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtUsername_RequiredFieldValidator" runat="server"
                                                    ErrorMessage="Este campo es requerido" ControlToValidate="txtUsername" Display="Dynamic"
                                                    ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblPassword" runat="server" Text="Contraseña:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="50" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtPassword_RequiredFieldValidator" runat="server"
                                                    ErrorMessage="Este campo es requerido" ControlToValidate="txtPassword" Display="Dynamic"
                                                    ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblRepassword" runat="server" Text="Repetir Contraseña:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtRepassword" runat="server" TextMode="Password" MaxLength="50" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtRepassword_RequiredFieldValidator" runat="server"
                                                    ErrorMessage="Este campo es requerido" ControlToValidate="txtRepassword" Display="Dynamic"
                                                    ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="txtRepassword_CompareValidator" runat="server" ErrorMessage="La contraseña es distinta"
                                                    ControlToCompare="txtPassword" ControlToValidate="txtRepassword" Display="Dynamic"
                                                    ValidationGroup="Validators"></asp:CompareValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblFileType" runat="server" Text="Tipo de Archivo:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtFileType" runat="server" MaxLength="20" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtFileType_RequiredFieldValidator" runat="server"
                                                    ErrorMessage="Este campo es requerido" ValidationGroup="Validators" ControlToValidate="txtFileType"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblToken" runat="server" Text="Token:"  Width="400px" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtToken" runat="server" MaxLength="500" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblUrl" runat="server" Text="Ingresar Url:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtUrl" runat="server" Width="400px" MaxLength="150" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtUrl_RequiredFieldValidator" runat="server" ErrorMessage="Este campo es requerido."
                                                    ControlToValidate="txtUrl" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <label class="desc">
                                                <asp:Label ID="lblUrlCancelation" runat="server" Text="Ingresar Url de cancelación:" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:TextBox ID="txtUrlCancelation" runat="server" Width="400px" MaxLength="150" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="validator-msg">
                                                <asp:RequiredFieldValidator ID="txtUrlCancelation_RequiredFieldValidator" runat="server"
                                                    ErrorMessage="Este campo es requerido." ControlToValidate="txtUrlCancelation"
                                                    Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblType" runat="server" Text="Tipo:" />
                                </label>
                                <span>
                                    <asp:DropDownList ID="ddlType" runat="server">
                                        <asp:ListItem Selected="true" Text="Pac" Value="1" />
                                    </asp:DropDownList>
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="ddlType_RequiredFieldValidator" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="ddlType" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                </div>
                            </li>
                            <li class="buttons">
                                <asp:Button ID="btnAccept" runat="server" CssClass="Button" Text="Aceptar" ValidationGroup="Validators"
                                    OnClick="btnAccept_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Cancelar" CausesValidation="False"
                                    PostBackUrl="~/Default.aspx" OnClick="btnCancel_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
