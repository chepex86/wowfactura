<%@ Page Title="Lista de Pagos" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillingStatementList.aspx.cs" Inherits="PACFDManager.BillingStatement.BillingStatementList" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="txt_left">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblBillersListTitle" runat="server" Text="Lista de Pagos" />
                    </h2>
                </div>
                <div>
                    <asp:Button ID="btnPay" runat="server" Text="Pagar" CssClass="Button" OnClick="btnPay_Click" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Regresar" CssClass="Button"
                        OnClick="btnCancel_Click" CausesValidation="false" />
                </div>
                <asp:GridView ID="gdvBillingStatement" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Fecha de pago" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <span class="vtip" title="Fecha en que se realiz� el pago.">
                                    <asp:Label ID="lblDatePaymentHeader" runat="server" Text="Fecha" />
                                </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="text-align: center;" class="vtip" title='<%#string.Format("Pagado hace {0}", DataBind_CalculateDate(Eval("PaidDate").ToString())) %>'>
                                    <asp:Label ID="lblDatePayment" runat="server" Text='<%#string.Format("{0:yyyy/MM/dd}", Eval("PaidDate")) %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Referencia">
                            <HeaderTemplate>
                                <span class="vtip" title="Referencia usada en las busquedas.">
                                    <asp:Label ID="lblReferenceHeader" runat="server" Text="Referencia" />
                                </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblReference" runat="server" Text='<%#Eval("Reference") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cambio">
                            <ItemTemplate>
                                <div style="text-align: right;">
                                    <%# string.Format("{0:c} {1}"
                                    , decimal.Parse(Eval("ExchangeRateMXN").ToString()) > 1 ? Eval("ExchangeRateMXN") : ""
                                    , decimal.Parse(Eval("ExchangeRateMXN").ToString()) > 1 ? "MXN" : ""
                               )%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right">
                            <HeaderTemplate>
                                <span class="vtip" title="Cantidad abonada al <b>(pre)comprobante</b>.">
                                    <asp:Label ID="lblAmountHeader" runat="server" Text="Cantidad" />
                                </span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="text-align: right;" class="vtip" title='<%#(Eval("IsPaid").ToString().ToLower() == "false" ? "<b>(Pre)Comprobante</b> aun por pagar." : "<b>(Pre)Comprobante</b> pagado.") 
                                + "<br />Abonado el: <b>" + string.Format("{0:yyyy/MM/dd}", Eval("PaidDate")) + "</b>"
                                + (Eval("CurrencyCode").ToString() != "MXN" ? "<br />Tipo de cambio: <b>" + Eval("ExchangeRateMXN").ToString() + "</b>." : "" )
                                %>'>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#String.Format("{0:C}", Eval("Amount")) %>' />
                                    <%#Eval("CurrencyCode").ToString() %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="dgHeader" />
                    <RowStyle CssClass="dgItem" />
                    <AlternatingRowStyle CssClass="dgAlternate" />
                    <EmptyDataTemplate>
                        No hay comprobantes disponibles.
                    </EmptyDataTemplate>
                </asp:GridView>
                <div>
                    <asp:Button ID="btnPay2" runat="server" Text="Pagar" CssClass="Button" OnClick="btnPay_Click" />
                    &nbsp;<asp:Button ID="btnCancel2" runat="server" Text="Regresar" CssClass="Button"
                        OnClick="btnCancel_Click" CausesValidation="false" />
                    <span style="float: right">
                        <table>
                            <tr>
                                <td>
                                    (Pre)Comprobante
                                </td>
                                <td style="text-align: right;">
                                    <span class="vtip" title="Cantidad total a pagar.">$<asp:Label ID="lblBillingTotal"
                                        runat="server" Text="0.00" />
                                        MXN</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Abonado
                                </td>
                                <td style="text-align: right;">
                                    <span class="vtip" title="Cantidad total abonada al <b>(pre)comprobante</b>.">$<asp:Label
                                        ID="lblPaidTotal" runat="server" Text="0.00" />
                                        MXN </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom-width: 1px; border-bottom-style: dashed;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Total a pagar
                                </td>
                                <td style="text-align: right;">
                                    <span class="vtip" title="Cantidad faltante para terminar de pagar el <b>(pre)comprobante</b>.">
                                        $<asp:Label ID="lblLeftToPaid" runat="server" Text="0.00" />
                                        MXN </span>
                                </td>
                            </tr>
                        </table>
                    </span>
                </div>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
