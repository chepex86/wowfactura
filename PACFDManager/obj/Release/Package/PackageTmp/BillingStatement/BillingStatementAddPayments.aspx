﻿<%@ Page Title="Agregar Pago" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="BillingStatementAddPayments.aspx.cs" Inherits="PACFDManager.BillingStatement.BillingStatementAddPayments" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="BillingStatementEditor.ascx" TagName="BillingStatementEditor" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">

    <script type="text/javascript" src="BillingStatementAddPayments.js"></script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe90">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblAddPaid" runat="server" Text="Agregar Pago" />
                            </h2>
                        </div>
                        <div>
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <uc2:BillingStatementEditor ID="BillingStatementEditor1" runat="server" OnApplyChange="BillingStatementEditor1_ApplyChange" />
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblTotalAmountTitle" runat="server" Text="Total:" />
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="lblTotalAmountText" runat="server" Text="0.00" /> MXN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblTotalMinusTitle" runat="server" Text="Restan:" Font-Bold="true" />
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="lblTotalMinusText" runat="server" Text="0.00" Font-Bold="true" /> MXN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblSubAmountTitle" runat="server" Text="Abonado:" /> 
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="lblSubAmountText" runat="server" Text="$0.00" /> MXN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblAmountToPayTitle" runat="server" Text="Deposito:" />
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="lblAmountToPayText" runat="server" Text="$0.00" /> MXN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="border-bottom-style: dashed; border-bottom-width: 1px">
                                                    <!--row used only for style-->
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRestAmountTitle" runat="server" Text="SubTotal:" />
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:Label ID="lblRestAmountText" runat="server" Text="$0.00" /> MXN
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <ul>
                            <li class="buttons">
                                <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                    OnClick="btnCancel_Click" CausesValidation="false" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <uc1:WebMessageBox ID="WebMessageBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
