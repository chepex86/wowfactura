<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="TaxTypeCatalogAdd.aspx.cs" Inherits="PACFDManager.Administrator.TaxTypeCatalogAdd" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<%@ Register Src="TaxTypeCatalogEditor.ascx" TagName="TaxTypeCatalogEditor" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <uc3:TaxTypeCatalogEditor ID="TaxTypeCatalogEditor21" runat="server" />
                        <ul>
                            <li class="buttons">
                                <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAceptar_Click" />
                                &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                    CausesValidation="false" OnClick="btnCancel_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click"
                OnCancelClick="btnCancel_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
