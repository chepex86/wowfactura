<%@ Page Title="Configuración de Comprobantes" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="TaxTemplateList.aspx.cs" Inherits="PACFDManager.Administrator.TaxTemplateList" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="txt_left">
        <div class="info">
            <label class="desc">
                <h2>
                    <asp:Label ID="lblTaxesListTitle" runat="server" Text="Catalogo de Configuración de Comprobantes" />
                </h2>
            </label>
        </div>
        <div>
            <br />
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <asp:Button ID="btnAdd" runat="server" Text="Agregar configuración" OnClick="btnAdd_Click"
                            CssClass="Button" />
                    </td>
                    <td style="text-align: right">
                        <asp:Button ID="btnSelect1" runat="server" Text="Seleccionar plantilla base" 
                            CssClass="Button" CausesValidation="False" 
                            PostBackUrl="~/Administrator/TaxTemplateSelection.aspx" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="gdvTemplates" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay impuestos disponibles.">
                <Columns>
                    <asp:BoundField DataField="TaxTemplateID" HeaderText="ID" />
                    <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Description" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="BillingType" HeaderText="Tipo" />
                    <asp:TemplateField HeaderText="Editar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar la configuración seleccionada." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Modificar la configuración de<br /><b><%#Eval("Name") + " - " + Eval("BillingType")%></b>'>
                                <asp:ImageButton ID="imgbEdit" runat="server" ImageUrl="~/Includes/Images/png/editIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxTemplateID")%>' />
                                <asp:HiddenField ID="hdfTxTemplateID" runat="server" Value='<%#Eval("TaxTemplateID")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eliminar">
                        <HeaderTemplate>
                            <uc2:Tooltip ID="tipDelete" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Eliminar' /> para<br />eliminar la configuración seleccionada." />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span class="vtip" title='Eliminar la configuración de<br /><b><%#Eval("Name") + " - " + Eval("BillingType")%></b>'>
                                <asp:ImageButton ID="imgbDelete" runat="server" ImageUrl="~/Includes/Images/png/deleteIcon.png"
                                    OnClick="ImageButton_Click" CommandArgument='<%#Eval("TaxTemplateID")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="dgHeader" />
                <RowStyle CssClass="dgItem" />
                <AlternatingRowStyle CssClass="dgAlternate" />
                <EmptyDataTemplate>
                    No hay impuestos disponibles.
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <table style="width: 100%">
                <tr>
                    <td style="text-align: left">
                        <asp:Button ID="btnAdd2" runat="server" Text="Agregar configuración" OnClick="btnAdd_Click"
                            CssClass="Button" />
                    </td>
                    <td style="text-align: right">
                        <asp:Button ID="btnSelect2" runat="server" Text="Seleccionar plantilla base" 
                            CssClass="Button" CausesValidation="False" 
                            PostBackUrl="~/Administrator/TaxTemplateSelection.aspx" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
        <div>
            <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
        </div>
    </div>
</asp:Content>
