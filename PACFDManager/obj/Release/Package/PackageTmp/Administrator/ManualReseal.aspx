<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManualReseal.aspx.cs" Inherits="PACFDManager.Administrator.ManualReseal"
    MasterPageFile="~/Default.Master" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" style="width: 500px">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitle" runat="server" Text="Sellado manual" />
                            </h2>
                        </div>
                        <div>
                            <asp:DropDownList ID="ddlSerial" runat="server" />
                        </div>
                        <ul>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblVersion" runat="server" Text="Versi�n:" /></label>
                                <span>
                                    <asp:TextBox ID="txtVersion" runat="server" Width="50px" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvVersion" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtVersion" Display="Dynamic" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblNoCertSAT" runat="server" Text="No.Certificado SAT:" /></label>
                                <span>
                                    <asp:TextBox ID="txtNoCertSAT" runat="server" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvNoCertSAT" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtNoCertSAT" Display="Dynamic" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblStampingDate" runat="server" Text="Fecha de timbrado:" /></label>
                                <span>
                                    <asp:TextBox ID="txtStampingDate" runat="server" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvStampingDate" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtStampingDate" Display="Dynamic" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblUUID" runat="server" Text="UUID:" /></label>
                                <span>
                                    <asp:TextBox ID="txtUUID" runat="server" Width="400px" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvUUID" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtUUID" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblSATSeal" runat="server" Text="Sello SAT:" /></label>
                                <span>
                                    <asp:TextBox ID="txtSATSeal" runat="server" Width="400px" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvSealSAT" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtSATSeal" Display="Dynamic" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li>
                                <label class="desc">
                                    <asp:Label ID="lblCFDSeal" runat="server" Text="Sello CFD:" /></label>
                                <span>
                                    <asp:TextBox ID="txtCFDSeal" runat="server" Width="400px" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvSealCFD" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="txtCFDSeal" Display="Dynamic" ValidationGroup="Validators" />
                                </div>
                            </li>
                            <li class="buttons">
                                <div>
                                    <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" 
                                        ValidationGroup="Validators" onclick="btnAcept_Click" />
                                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                        CausesValidation="false" PostBackUrl="~/Default.aspx" />
                                </div>
                            </li>
                            <li>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>
                                    <asp:Button ID="btnReSealCFD" runat="server" onclick="btnReSealCFD_Click" 
                                        Text="Re Seal CFD" />
                                </p>
                                <p>&nbsp;</p>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <uc1:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
