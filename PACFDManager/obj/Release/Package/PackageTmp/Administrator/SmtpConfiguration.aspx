<%@ Page Title="Configuraci�n de Env�o de Correo Electronico" Language="C#" MasterPageFile="~/Default.Master"
    AutoEventWireup="true" CodeBehind="SmtpConfiguration.aspx.cs" Inherits="PACFDManager.Administrator.SmtpConfiguration" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
<script type="text/javascript">  
    function UnChecked() {
        var box = document.getElementById("<%=ChkBoxDisabled.ClientID%>");
        box.checked = false;
        __doPostBack("<%=ChkBoxDisabled.ClientID%>", "");
    }
    function Clear() {
        var box = document.getElementById("<%=ChkBoxDisabled.ClientID%>");
        var host = document.getElementById("<%=txtHost.ClientID%>");
        var port = document.getElementById("<%=txtPort.ClientID%>");
        var bcc = document.getElementById("<%=txtBcc.ClientID%>");
        var cName = document.getElementById("<%=txtCredentialName.ClientID%>");
        var cPass = document.getElementById("<%=txtCredentialPassword.ClientID%>");
        var cPassVer = document.getElementById("<%=txtCredentialPasswordVerification.ClientID%>");
        var ssl = document.getElementById("<%=ckbEnableSsl.ClientID%>");
        var display = document.getElementById("<%=txtDisplayName.ClientID%>");
        var btn = document.getElementById("<%=btnAcept.ClientID%>");

        this.onOff("<%=rfvHost.ClientID%>", false);
        this.onOff("<%=rfvPort.ClientID%>", false);
        this.onOff("<%=rfvEmail.ClientID%>", false);
        this.onOff("<%=revEmail.ClientID%>", false);
        this.onOff("<%=rfvPassword.ClientID%>", false);
        this.onOff("<%=rfvCredentialPasswordVerification.ClientID%>", false);
        this.onOff("<%=cvCredentialPasswordVerification.ClientID%>", false);
        this.onOff("<%=rfvDisplayName.ClientID%>", false);

        host.value = ""; port.value = ""; bcc.value = ""; cName.value = ""; cPass.value = "";
        cPassVer.value = ""; ssl.checked = false; display.value = ""; btn.CausesValidation = false;
    }
    function onOff(validatorId, active) {
        var validator = document.getElementById(validatorId);
        ValidatorEnable(validator, active);
    }
</script>
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <!-- title form -->
                <div class="info">
                    <h2>
                        <asp:Label ID="lblEmailConfigurationTitle" runat="server" Text="Configuraci�n de correo"></asp:Label>
                    </h2>
                </div>
                <div>
                    <asp:CheckBox ID="ChkBoxDisabled" runat="server" Text="Deshabilitar la Configuraci�n de Correo"
                        OnCheckedChanged="ChkBoxDisabled_CheckedChanged" AutoPostBack="True" />
                </div>
                <ul>
                    <li>
                        <h2>
                            <asp:Label ID="lblServerTitle" runat="server" Text="Servidor"></asp:Label>
                        </h2>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblHostTitle" runat="server" Text="Host"></asp:Label>
                        </label>
                        <span class="vtip" title="Nombre del servidor de correo. Ej. <br /><b>�smtp.gmail.com</b><br/><b>�smtp.yahoo.com</b><br /><b>�smtp.live.com</b>">
                            <asp:TextBox ID="txtHost" runat="server" AutoCompleteType="Disabled" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvHost" runat="server" ControlToValidate="txtHost"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPortTitle" runat="server" Text="Puerto"></asp:Label>
                        </label>
                        <span class="vtip" title="Puerto de salida del servidor de correo. Ej. <br/><b>�587</b><br/><b>�465</b>">
                            <asp:TextBox ID="txtPort" runat="server" AutoCompleteType="Disabled" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvPort" runat="server" ControlToValidate="txtPort"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lblHiddencopyTitle" runat="server" Text="Copia oculta (Bcc)"></asp:Label>
                        </label>
                        <span class="vtip" title="Correo electr�nico al cual se enviara una copia oculta. Ej. <br /><b>�administrador@gmail.com.mx</b><br /><b>�juan.perez@oz.com.mx</b>">
                            <asp:TextBox ID="txtBcc" runat="server" />
                        </span></li>
                    <li>
                        <h2>
                            <asp:Label ID="lblCredentialsTitle" runat="server" Text="Credenciales"></asp:Label>
                        </h2>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblEmailTitle" runat="server" Text="Email"></asp:Label>
                        </label>
                        <span class="vtip" title="Correo electr�nico que env�a el correo. Ej. <br /><b>�administrador@hotmail.com.mx</b><br /><b>�juan.perez@oz.com.mx</b>">
                            <asp:TextBox ID="txtCredentialName" runat="server" />
                        </span>
                        <div class="validator-msg" style="width: 200px">
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtCredentialName"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtCredentialName"
                                ErrorMessage="Direcci�n de Correo Electronico no Valida" ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                                ValidationGroup="Validators" meta:resourcekey="revEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPasswordTitle" runat="server" Text="Contrase�a"></asp:Label>
                        </label>
                        <span class="vtip" title="Contrase�a del correo que env�a el correo.<br/>Recuerde no revelar la contrase�a a nadie sin autorizaci�n.">
                            <asp:TextBox ID="txtCredentialPassword" runat="server" TextMode="Password" MaxLength="13" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtCredentialPassword"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblPasswordValidation" runat="server" Text="Contrase�a verificaci�n"></asp:Label>
                        </label>
                        <span class="vtip" title="Verifica que la contrase�a este bien escrita.<br/>Recuerde no revelar la contrase�a a nadie sin autorizaci�n.">
                            <asp:TextBox ID="txtCredentialPasswordVerification" runat="server" TextMode="Password"
                                MaxLength="13" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvCredentialPasswordVerification" runat="server"
                                ControlToValidate="txtCredentialPasswordVerification" ErrorMessage="Este campo es requerido"
                                ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvCredentialPasswordVerification" runat="server" ControlToCompare="txtCredentialPassword"
                                ControlToValidate="txtCredentialPasswordVerification" ErrorMessage="Contrase�a diferente"
                                ValidationGroup="Validators" meta:resourcekey="cvPasswordConfirmResource1" Display="Dynamic"></asp:CompareValidator>
                        </div>
                    </li>
                    <li class="left">
                        <label class="desc">
                            <asp:Label ID="lblEnableSslTitle" runat="server" Text="Habilitar Ssl"></asp:Label>
                        </label>
                        <span class="vtip" title="Habilita el uso de seguridad encriptada <br />Ssl (Secure Socket Layer).">
                            <asp:CheckBox ID="ckbEnableSsl" runat="server" Checked="True" /></span></li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lbldisplayNameTitle" runat="server" Text="Nombre a desplegar"></asp:Label>
                        </label>
                        <span class="vtip" title="Nombre a desplegar en el t�tulo del correo. Ej.<br/><b>�Buen d�a desde Hotel Star Class.</b><br/><b>�Correo de autorizaci�n de pago.</b>">
                            <asp:TextBox ID="txtDisplayName" runat="server" />
                        </span>
                        <div class="validator-msg" style="width: 120px">
                            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                                ErrorMessage="Este campo es requerido" ValidationGroup="Validators" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </li>
                    <li class="buttons">
                        <div>
                            <asp:Button ID="btnAcept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAcept_Click"
                                ValidationGroup="Validators" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="Button"
                                OnClick="btnCancel_Click" /></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <uc1:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" OnCancelClick="WebMessageBox1_OnCancelClick"
        runat="server" />
</asp:Content>
