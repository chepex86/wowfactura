﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceptorsSearch.ascx.cs"
    Inherits="PACFDManager.Receptors.ReceptorsSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>

<script src="ReceptorsSearchScripts.js" type="text/javascript"></script>

<div id="container" class="wframe50">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblReceptorTitle" runat="server" Text="Búsqueda de Clientes" Font-Size="18px" />
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblName" runat="server" Text="Por nombre" />
                    </label>
                    <span class="vtip" title="Busca un cliente por el nombre.">
                        <asp:TextBox ID="txtName" runat="server" />
                        <asp:AutoCompleteExtender ID="txtName_AutoCompleteExtender" runat="server" TargetControlID="txtName"
                            ServicePath="~/AutoComplete.asmx" ServiceMethod="GetReceptorByName" MinimumPrefixLength="1"
                            EnableCaching="true" UseContextKey="True">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByName" runat="server" style="display: none;">
                        <img id="imgLoading_ByName" runat="server" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblRFC" runat="server" Text="Por RFC" />
                    </label>
                    <span class="vtip" title="Busca un cliente por su RFC. Ej. <br /><b>•ABC123456ACB</b><br /><b>•ABCD123456ACB</b>">
                        <asp:TextBox ID="txtRFC" runat="server" />
                        <asp:AutoCompleteExtender ID="txtRFC_AutoCompleteExtender" runat="server" TargetControlID="txtRFC"
                            ServicePath="~/AutoComplete.asmx" ServiceMethod="GetReceptorsRFCs" MinimumPrefixLength="1"
                            EnableCaching="true" UseContextKey="True">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByRFC" runat="server" style="display: none;">
                        <img id="imgLoading_ByRFC" runat="server" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <asp:Button CssClass="Button_Search" ID="btnSearchRFC" runat="server" Text="Buscar"
                        OnClick="btnSearch_Click" />
                </li>
            </ul>
        </div>
    </div>
</div>
