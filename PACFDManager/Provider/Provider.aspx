﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Provider.aspx.cs"
    Inherits="PACFDManager.Provider.Provider" Culture="auto" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <table>
        <tr>
            <td>
                <div id="container">
                    <div class="form">
                        <div class="formStyles">
                            <div class="info">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Alta de Proveedor"></asp:Label>
                                </h2>
                            </div>
                            <ul>
                                <li>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <label class="desc">
                                        <asp:Label ID="lblName" runat="server" Text="Nombre:"></asp:Label></label>
                                    <span>
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="255" Width="300px"></asp:TextBox></span>
                                    <span>
                                        <uc1:Tooltip ID="tipName" runat="server" ToolTip="Introduce el nombre o razón social del proveedor." />
                                    </span></li>
                                <li>
                                    <asp:RequiredFieldValidator ID="rfvCFDVersion" runat="server" ControlToValidate="txtCFDVersion"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <label class="desc">
                                        <asp:Label ID="lblCFDVersion" runat="server" Text="Versión CFD:"></asp:Label></label>
                                    <span>
                                        <asp:TextBox ID="txtCFDVersion" runat="server" MaxLength="20" Width="200px"></asp:TextBox></span>
                                    <span>
                                        <uc1:Tooltip ID="tipCFDVersion" runat="server" ToolTip="Introduce la versión del Certificado Digital.&lt;br /&gt;ej: &lt;b&gt;2.0&lt;/b&gt;" />
                                    </span></li>
                                <li>
                                    <asp:RequiredFieldValidator ID="rfvRFC" runat="server" ControlToValidate="txtRFC"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <label class="desc">
                                        <asp:Label ID="lblRFC" runat="server" Text="R.F.C.:"></asp:Label></label>
                                    <span>
                                        <asp:TextBox ID="txtRFC" runat="server" MaxLength="13" Width="200px"></asp:TextBox></span>
                                    <span>
                                        <uc1:Tooltip ID="tipRFC" runat="server" ToolTip="Introduce un RFC valido con el siguiente formato.&lt;br /&gt;ej: &lt;b&gt;XXXX999999NNN&lt;/b&gt;<br />Donde:<br /><b>X</b> es un caracter de la <b>A</b> - <b>Z</b><br /><b>9</b> es un número del <b>0</b> - <b>9</b><br /><b>N</b> es un caracter alfanumérico." />
                                    </span>
                                    <asp:RegularExpressionValidator ID="revRFC" runat="server" ControlToValidate="txtRFC"
                                        Display="Dynamic" ErrorMessage="Formato del RFC no valido" ValidationExpression="[A-Z|a-z]{3,4}\d{6}\w{3}$"
                                        ValidationGroup="Validators"></asp:RegularExpressionValidator>
                                </li>
                                <li>
                                    <asp:RequiredFieldValidator ID="rfvAuthorizationDate" runat="server" ControlToValidate="txtAuthorizationDate"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <label class="desc">
                                        <asp:Label ID="lblAuthorizationDate" runat="server" Text="Fecha de Autorización"></asp:Label></label>
                                    <span>
                                        <asp:TextBox ID="txtAuthorizationDate" runat="server" Width="100px"></asp:TextBox>
                                        <asp:CalendarExtender ID="txtAuthorizationDate_CalendarExtender" runat="server" Enabled="True"
                                            TargetControlID="txtAuthorizationDate">
                                        </asp:CalendarExtender>
                                    </span><span>
                                        <uc1:Tooltip ID="tipAuthorizationDate" runat="server" ToolTip="Selecciona la fecha de autorización." />
                                    </span></li>
                                <li>
                                    <asp:RequiredFieldValidator ID="rfvAuthorizationNumber" runat="server" ControlToValidate="txtAuthorizationNumber"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    <label class="desc">
                                        <asp:Label ID="lblAuthorizationNumber" runat="server" Text="Número de Autorización:"></asp:Label></label>
                                    <span>
                                        <asp:TextBox ID="txtAuthorizationNumber" runat="server" Width="100px" MaxLength="20"></asp:TextBox></span>
                                    </span><uc1:Tooltip ID="tipAuthorizationNumber" runat="server" ToolTip="Introduce el número de autorización." />
                                    </span> </li>
                                <li class="buttons">
                                    <asp:Button ID="btnAccept" runat="server" CssClass="Button" OnClick="btnAccept_Click"
                                        Text="Agregar" ValidationGroup="Validators" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Cancelar" PostBackUrl="~/Provider/DigitalCertificatesProviderList.aspx" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
