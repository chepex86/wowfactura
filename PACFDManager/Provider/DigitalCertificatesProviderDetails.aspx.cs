﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.Provider
{
    public partial class DigitalCertificatesProviderDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.ProgressBar.Visible = false;
            if (IsPostBack)
                return;
            if (Request.QueryString["ProviderDigitalCertificateID"].IsNull())
                Response.Redirect(Request.UrlReferrer.ToString());

            this.Title = this.GetLocalResourceObject("DigitalCertificateDetail").ToString();
            ViewState["ProviderDigitalCertificateID"] = Request.QueryString["ProviderDigitalCertificateID"];
            this.LoadData(Convert.ToInt32(Request.QueryString["ProviderDigitalCertificateID"]));
        }
        /// <summary>
        /// Load information for Digital Certificate detail.
        /// </summary>
        /// <param name="DigitalCertificateID">ID Digital Certificate</param>
        protected void LoadData(int DigitalCertificateID)
        {
            PACFD.Rules.Provider certificate = new PACFD.Rules.Provider();
            ProviderDataSet.ProviderDigitalCertificatesDataTable ta;
            ta = certificate.GetDigitalCertificatesProviderByID(DigitalCertificateID);
            if (ta.Count < 1)
                return;
            lblCertificateName.Text = ta[0].CertificateName;
            lblPrivateKeyName.Text = ta[0].PrivateKeyName;
            lblStatus.Text = (ta[0].Status == 1 ? this.GetLocalResourceObject("Active").ToString() : 
                                                  this.GetLocalResourceObject("NotActive").ToString());                                          
            lblCcp.Text = Cryptography.DecryptUnivisitString(ta[0].Ccp);
            lblCertificateNumber.Text = ta[0].CertificateNumber;
            ta.Dispose();
        }
        /// <summary>
        /// Redirects and specifies that the file to download is the Certificate, which corresponds to DigitalCertificateID sent.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnCer_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(String.Format("~/Provider/DownloadFilesProvider.aspx?ProviderDigitalCertificateID={0}&FileType=0", ViewState["ProviderDigitalCertificateID"].ToString()));
        }
        /// <summary>
        /// Redirects and specifies that the file to download is the Key, which corresponds to DigitalCertificateID sent.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnKey_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(String.Format("~/Provider/DownloadFilesProvider.aspx?ProviderDigitalCertificateID={0}&FileType=1", ViewState["ProviderDigitalCertificateID"].ToString()));
        }
    }
}