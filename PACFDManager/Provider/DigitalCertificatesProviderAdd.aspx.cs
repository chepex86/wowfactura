﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Security.Cryptography.X509Certificates;
#endregion

namespace PACFDManager.Provider
{
    public partial class DigitalCertificatesProviderAdd : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = this.GetLocalResourceObject("AddDigitalCertificate").ToString();
            if (!IsPostBack)
                if (!this.SeekProvider())
                    this.Response.Redirect(typeof(Provider).Name + ".aspx");
        }
        /// <summary>
        /// Looking for a provider.
        /// </summary>
        /// <returns>Returns true if found a provider, otherwise returns false.</returns>
        private bool SeekProvider()
        {
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet.ProviderDataTable ta = provider.GetProvider();

            return ta.Count < 1 ? false : true;
        }
        /// <summary>
        /// Stored in the database of digital certificates Provider.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        private bool LoadData()
        {
            PACFD.Rules.DigitalCertificates certificate = new PACFD.Rules.DigitalCertificates();
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet ds = new ProviderDataSet();
            ProviderDataSet.ProviderDigitalCertificatesRow drCert = ds.ProviderDigitalCertificates.NewProviderDigitalCertificatesRow();
            String e = String.Empty, n = String.Empty, c = String.Empty, k = String.Empty;

            c = this.FileUpload(this.fuCertificate);
            k = this.FileUpload(this.fuPrivateKey);

            //if (!certificate.ValidatePrivateKey(this.fuPrivateKey.FileName, k, this.txtCcp.Text.Trim()))
            //{
            //    e = "No ha sido posible cargar los archivos debido a que:<br/>" +
            //        "- El archivo [" + this.fuPrivateKey.FileName + "] no es válido ó está dañado.<br/>" +
            //        "- Verifique también que la llave pública sea la correcta.";
            //    this.ErrorMessage(e);
            //    return false;
            //}
            if (!this.ValidateCertificate(Convert.FromBase64String(c)))
            {
                e = String.Empty;
                e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                    "- El archivo [" + this.fuCertificate.FileName + "] no es válido ó está dañado.<br/>";
                this.ErrorMessage(e);
                return false;
            }

            drCert.ProviderID = 1;
            drCert.Certificate = c;
            drCert.CertificateName = this.fuCertificate.FileName;

            n = GetCertificateNumber(Convert.FromBase64String(c));

            if (n.Length != 20)
            {
                e = String.Empty;
                e = "No ha sido posible cargar los archivos debido a que:<br/>" +
                    "- El archivo [" + this.fuCertificate.FileName + "] no es válido ó está dañado ya que.<br/>" +
                    "no ha sido posible leer el numero de certificado.";
                this.ErrorMessage(e);
                return false;
            }

            drCert.CertificateNumber = n;
            drCert.PrivtaeKey = k;
            drCert.PrivateKeyName = this.fuPrivateKey.FileName;
            drCert.Ccp = Cryptography.EncryptUnivisitString(this.txtCcp.Text);
            drCert.Status = 0;

            ds.ProviderDigitalCertificates.AddProviderDigitalCertificatesRow(drCert);
            provider.UpdateDigitalCertificates(ds.ProviderDigitalCertificates);

            return true;
        }
        /// <summary>
        /// Converts a file to an array of bytes and then converted to string.
        /// </summary>
        /// <param name="fu">FileUpload file</param>
        /// <returns>Returns the file submitted become character string or null depending on the outcome of the operation.</returns>
        private string FileUpload(FileUpload fu)
        {
            int size;
            byte[] buffer;

            if (fu.PostedFile.IsNull() && fu.FileName == String.Empty)
                return null;

            if (fu.PostedFile.ContentLength < 1)
                return null;

            if (fu.PostedFile.InputStream.Length > int.MaxValue)
                return null;

            int.TryParse(fu.PostedFile.InputStream.Length.ToString(), out size);

            if (size == 0)
                return null;

            buffer = new byte[size];

            fu.PostedFile.InputStream.Read(buffer, 0, size);

            return Convert.ToBase64String(buffer);
        }
        /// <summary>
        /// Fires the PACFDManager.Provider.DigitalCertificatesProviderAdd.btnLoad_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (!this.LoadData())
                return;
            Response.Redirect(typeof(DigitalCertificatesProviderList).Name + ".aspx");
        }
        /// <summary>
        /// Gets the valid date range of a digital certificate.
        /// </summary>
        /// <param name="buffer">byte[] buffer</param>
        /// <returns>DateTime[]</returns>
        private bool ValidateCertificate(byte[] buffer)
        {
            try
            {
                X509Certificate2 m_cer = new X509Certificate2(buffer);
                return !m_cer.IsNull() ? true : false;
            }
            catch
            {
                return false;
            }    
        }
        /// <summary>
        /// Get the number of the certificate in a file [*.cer] valid.
        /// </summary>
        /// <param name="buffer">byte[] buffer</param>
        /// <returns>Returns the number of the certificate as a string of length 20.</returns>
        private string GetCertificateNumber(byte[] buffer)
        {
            try
            {
                X509Certificate2 m_cer = new X509Certificate2(buffer);
                if (!m_cer.IsNull())
                {
                    string dHex = m_cer.SerialNumber;
                    string aux = string.Empty;
                    string result = string.Empty;
                    while (dHex.Length > 0)
                    {
                        aux = System.Convert.ToChar(System.Convert.ToUInt32(dHex.Substring(0, 2), 16)).ToString();
                        result += aux;
                        dHex = dHex.Substring(2, dHex.Length - 2);
                    }
                    return result;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return string.Empty;
            }
        }
        /// <summary>
        /// Displays an error message on screen.
        /// </summary>
        /// <param name="message">string Error Message.</param>
        private void ErrorMessage(string message)
        {
            this.lblError.Text = String.Empty;
            this.lblError.Text = message;
            this.lblError.Visible = true;
        }
    }
}