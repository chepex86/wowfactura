﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
using System.Web.UI.HtmlControls;
using System.Data;
#endregion

namespace PACFDManager.Provider
{
    public partial class DigitalCertificatesProviderList : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.GetLocalResourceObject("ListofDigitalCertificatesProvider").ToString();
            if (!this.LoadData())
                this.Response.Redirect(typeof(DigitalCertificatesProviderAdd).Name + ".aspx");
        }
        /// <summary>
        /// Brings all the corresponding rows of the database to fill the list of Digital Certificates Provider.
        /// </summary>
        protected bool LoadData()
        {
            PACFD.Rules.Provider certificate = new PACFD.Rules.Provider();
            ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable ta;
            ta = certificate.GetAllDigitalCertificatesProvider();

            if (ta.Count < 1)
                return false;

            this.gvCertificateList.DataMember = ta.TableName;
            this.gvCertificateList.DataSource = ta;
            this.gvCertificateList.DataBind();
            ta.Dispose();

            if (this.gvCertificateList.Rows.Count < 1)
            {
                DataTable dt = new ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDDataTable();
                DataRow dr = dt.NewRow();
                dr["ProviderDigitalCertificateID"] = -1;
                dr["ProviderID"] = -1;
                dr["Certificate"] = String.Empty;
                dr["PrivateKeyName"] = String.Empty;
                dr["PrivtaeKey"] = String.Empty;
                dr["Ccp"] = String.Empty;
                dr["Status"] = 1;
                dr["CertificateNumber"] = String.Empty;
                dr["CertificateName"] = String.Empty;
                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvCertificateList.DataSource = dt;
                this.gvCertificateList.DataBind();
                dt.Dispose();
            }
            return true;
        }
        /// <summary>
        /// Calls message control to confirm that it will de/activate the digital certificate.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ImgBtnActive_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            String c = (s.CommandName == "1" ? this.GetLocalResourceObject("Activate").ToString() : 
                                               this.GetLocalResourceObject("Deactivate").ToString());

            this.WebMessageBox1.Title = c + " " + this.GetLocalResourceObject("Certificate").ToString();
            this.WebMessageBox1.ShowMessage(this.GetLocalResourceObject("QuestionPart1").ToString() + " " + c
                                    + " " + this.GetLocalResourceObject("QuestionPart2").ToString(), 
                                    System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["ProviderDigitalCertificateID"] = s.CommandArgument;
        }
        /// <summary>
        /// Changes the status of Digital Certificate to Active / Not Active
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Provider certificate = new PACFD.Rules.Provider();
            ProviderDataSet ds = new ProviderDataSet();
            String c = "";

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
                return;

            certificate.GetDigitalCertificatesProviderByID(ds.ProviderDigitalCertificates, Convert.ToInt32(ViewState["ProviderDigitalCertificateID"]));
            if (ds.ProviderDigitalCertificates.Count > 0)
            {
                ProviderDataSet.ProviderDigitalCertificatesRow drCert = ds.ProviderDigitalCertificates[0];
                drCert.Status = (drCert.Status == 1 ? 0 : 1);
                c = (drCert.Status == 1 ? this.GetLocalResourceObject("Deactivate").ToString() :
                                          this.GetLocalResourceObject("Activate").ToString());                                     
            }
            if (!certificate.UpdateDigitalCertificates(ds.ProviderDigitalCertificates))
            {
                LogManager.WriteStackTrace(new Exception("Error al tratar de " + c + " Certificado"));
                return;
            }
            this.ViewState["ClientID"] = null;
            LogManager.WriteStackTrace(new Exception("Éxito al " + c + " Certificado"));
            this.Response.Redirect(typeof(DigitalCertificatesProviderList).Name + ".aspx");
        }
        /// <summary>
        /// Changes the image depending of value on the cell that contains and 
        /// the value of the property enable, for each row generated.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void gvCertificateList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            ImageButton i = e.Row.FindControl("ImgBtnActive") as ImageButton;

            if (row.RowIndex > -1)
            {
                /*if (!ViewState["State"].IsNull())
                {
                    row.Cells[7].Enabled = (Convert.ToInt32(ViewState["State"].ToString()) > 0) ?
                                           (row.Cells[1].Text == "1" ? true : false) : true;
                    i.CommandName = (row.Cells[1].Text == "1" ? "0" : "1");
                    i.ImageUrl = (row.Cells[1].Text == "1" ? "~/Includes/Images/png/apply.png" : "~/Includes/Images/png/editdelete.png");
                    row.Cells[1].Text = (row.Cells[1].Text == "1" ? this.GetLocalResourceObject("Active").ToString() : 
                                                                    this.GetLocalResourceObject("NotActive").ToString());
                }*/

                if (!ViewState["State"].IsNull())
                {
                    if (Convert.ToInt32(ViewState["State"].ToString()) > 0)
                    {
                        if (row.Cells[5].Text == "1")
                        {
                            row.Cells[7].Enabled =
                            i.Visible = true;
                            row.Cells[5].BackColor = System.Drawing.Color.LightGreen;
                        }
                        else
                        {
                            row.Cells[7].Enabled =
                            i.Visible = false;
                            row.Cells[5].BackColor = System.Drawing.Color.LightCoral;
                        }
                    }
                    else
                    {
                        row.Cells[7].Enabled = true;
                        row.Cells[5].BackColor = System.Drawing.Color.LightCoral;
                    }

                    i.CommandName = (row.Cells[5].Text == "1" ? "0" : "1");
                    i.ImageUrl = (row.Cells[5].Text == "1" ? "~/Includes/Images/png/editdelete.png" : "~/Includes/Images/png/apply.png");
                    i.ToolTip = row.Cells[5].Text == "1" ? "Desactivar" : "Activar";
                    row.Cells[5].Text = (row.Cells[5].Text == "1" ? this.GetLocalResourceObject("Active").ToString() :
                                                                    this.GetLocalResourceObject("NotActive").ToString());
                }

                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int j = 0; j < row.Cells.Count; j += 1)
                        row.Cells[j].Visible = false;
                    ViewState["Empty"] = null;
                }
            }
            else
            {
                PACFD.Rules.Provider certificate = new PACFD.Rules.Provider();
                ProviderDataSet.ProviderDigitalCertificates_GetActiveDataTable ta;
                ta = certificate.GetActive();
                ViewState["State"] = ta.Count;
            }

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            HtmlAnchor key = e.Row.FindControl("aDownLoadKey") as HtmlAnchor;
            HtmlAnchor cer = e.Row.FindControl("aDownLoadCer") as HtmlAnchor;
            HtmlAnchor det = e.Row.FindControl("aDetails") as HtmlAnchor;

            ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDRow dr = ((System.Data.DataRowView)e.Row.DataItem).Row as ProviderDataSet.ProviderDigitalCertificates_GetByFkProviderIDRow;

            key.HRef = string.Format("~/Provider/DownloadFilesProvider.aspx?ProviderDigitalCertificateID={0}&FileType=1", dr.ProviderDigitalCertificateID);
            cer.HRef = string.Format("~/Provider/DownloadFilesProvider.aspx?ProviderDigitalCertificateID={0}&FileType=0", dr.ProviderDigitalCertificateID);
            det.HRef = string.Format("~/Provider/DigitalCertificatesProviderDetails.aspx?ProviderDigitalCertificateID={0}", dr.ProviderDigitalCertificateID);
        }
    }
}