﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Provider
{
    public partial class Provider : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.SearchProvider() ? this.GetLocalResourceObject("ModifyProvider").ToString() : 
                                                  this.GetLocalResourceObject("AddedProvider").ToString();

            this.txtAuthorizationDate_CalendarExtender.Format = "dd/MM/yyyy";
        }
        /// <summary>
        /// Fills out the provider  information, if no exist calls the method add provider.
        /// </summary>
        /// <returns>Returns true if the provider exists, otherwise returns false.</returns>
        private bool SearchProvider()
        {
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet.ProviderDataTable ta;
            ta = provider.GetProvider();
            ViewState["Provider"] = false;

            if (ta.Count < 1)
                return false;

            this.lblTitle.Text = this.GetLocalResourceObject("ModifyProvider").ToString();
            this.txtName.Text = ta[0].Name;
            this.txtCFDVersion.Text = ta[0].CFDVersion;
            this.txtRFC.Text = ta[0].RFC;
            this.txtAuthorizationDate.Text = ta[0].AuthorizationDate.ToString(this.GetLocalResourceObject("FormatDate").ToString());
            this.txtAuthorizationNumber.Text = ta[0].AuthorizationNumber;
            this.btnAccept.Text = this.GetLocalResourceObject("Modify").ToString();

            ViewState["Provider"] = true;
            return true;
        }
        /// <summary>
        /// Modify current provider information 
        /// </summary>
        /// <returns>Returns true if the information could be changed, otherwise returns false.</returns>
        private bool ModifyData()
        {
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet ds = new ProviderDataSet();

            provider.GetProvider();

            if (ds.Provider.Count < 1)
                return false;

            ProviderDataSet.ProviderRow drProvider = ds.Provider[0];
            drProvider.Name = this.txtName.Text;
            drProvider.CFDVersion = this.txtCFDVersion.Text;
            drProvider.RFC = this.txtRFC.Text;
            drProvider.AuthorizationDate = Convert.ToDateTime(this.txtAuthorizationDate.Text);
            drProvider.AuthorizationNumber = this.txtAuthorizationNumber.Text;

            if (!provider.UpdateProviders(ds.Provider))
                return false;

            LogManager.WriteStackTrace(new Exception("Éxito al Modificar Proveedor."));
            return true;
        }
        /// <summary>
        /// Add a provider.
        /// </summary>
        /// <returns>Returns true if the provider could be added, otherwise returns false.</returns>
        private bool AddProvider()
        {
            PACFD.Rules.Provider provider = new PACFD.Rules.Provider();
            ProviderDataSet ds = new ProviderDataSet();
            ProviderDataSet.ProviderRow drProvider = ds.Provider.NewProviderRow();

            drProvider.Name = this.txtName.Text;
            drProvider.CFDVersion = this.txtCFDVersion.Text;
            drProvider.RFC = this.txtRFC.Text;
            drProvider.AuthorizationDate = Convert.ToDateTime(this.txtAuthorizationDate.Text);
            drProvider.AuthorizationNumber = this.txtAuthorizationNumber.Text;

            ds.Provider.AddProviderRow(drProvider);

            if (!provider.UpdateProviders(ds.Provider))
                return false;

            return true;
        }
        /// <summary>
        /// Fires the PACFDManager.Provider.Provider.btnAccept_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            if (ViewState["Provider"].IsNull())
                return;

            if (Convert.ToBoolean(ViewState["Provider"]))
                this.ModifyData();
            else
                this.AddProvider();

            Response.Redirect(typeof(DigitalCertificatesProviderList).Name + ".aspx");
        }
    }
}