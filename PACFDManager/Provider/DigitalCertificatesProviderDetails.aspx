<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DigitalCertificatesProviderDetails.aspx.cs"
    Inherits="PACFDManager.Provider.DigitalCertificatesProviderDetails" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ MasterType VirtualPath="~/Default.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer">
                <tr>
                    <td align="center" class="Titulo">
                        <asp:Label ID="lblTitleDetails" runat="server" 
                            Text="Detalle del Certificado Digital" 
                            meta:resourcekey="lblTitleDetailsResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblTitleCertificateName" runat="server" Font-Bold="True" 
                                        Text="Certificado:" meta:resourcekey="lblTitleCertificateNameResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblCertificateName" runat="server" 
                                        meta:resourcekey="lblCertificateNameResource1"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Label ID="lblTitleStatus" runat="server" Font-Bold="True" Text="Estado:" 
                                        meta:resourcekey="lblTitleStatusResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblStatus" runat="server" meta:resourcekey="lblStatusResource1"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblTitlePrivateKeyName" runat="server" Font-Bold="True" 
                                        Text="Llave Privada:" meta:resourcekey="lblTitlePrivateKeyNameResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblPrivateKeyName" runat="server" 
                                        meta:resourcekey="lblPrivateKeyNameResource1"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Label ID="lblTitleCcp" runat="server" Font-Bold="True" 
                                        Text="Contrase�a Llave Privada:" meta:resourcekey="lblTitleCcpResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblCcp" runat="server" meta:resourcekey="lblCcpResource1"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblTitleCertificateNumber" runat="server" Font-Bold="True" 
                                        Text="N�mero de Certificado:"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblCertificateNumber" runat="server"></asp:Label>
                                </td>
                                <td align="right">
                                    &nbsp;</td>
                                <td align="left">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Titulo" colspan="4">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblDownloadCertificate" runat="server" 
                                        Text="Descargar Certificado" meta:resourcekey="lblDownloadCertificateResource1"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblDownloadPrivateKey" runat="server" 
                                        Text="Descargar Llave Privada" 
                                        meta:resourcekey="lblDownloadPrivateKeyResource1"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:ImageButton ID="ImgBtnCer" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                        OnClick="ImgBtnCer_Click" meta:resourcekey="ImgBtnCerResource1" 
                                        ToolTip="Descargar Certificado" />
                                </td>
                                <td align="center">
                                    <asp:ImageButton ID="ImgBtnKey" runat="server" ImageUrl="~/Includes/Images/png/filesave-48.png"
                                        OnClick="ImgBtnKey_Click" meta:resourcekey="ImgBtnKeyResource1" 
                                        ToolTip="Descargar Llave Privada" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
