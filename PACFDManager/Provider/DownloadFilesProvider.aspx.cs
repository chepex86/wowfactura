﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Common;
#endregion

namespace PACFDManager.Provider
{
    public partial class DownloadFilesProvider : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (this.LoadFile())
                    Response.Redirect(Request.UrlReferrer.ToString());
        }
        /// <summary>
        /// Generates the file to download, based on the arguments received by QueryString:
        /// ProviderDigitalCertificateID & FileType.
        /// </summary>
        /// <returns>Returns a boolean true if the file was generated successfully, otherwise returns false.</returns>
        protected bool LoadFile()
        {
            if (Request.QueryString["ProviderDigitalCertificateID"].IsNull() || Request.QueryString["FileType"].IsNull())
                return false;

            Byte[] file = SeekFile(Convert.ToInt32(Request.QueryString["ProviderDigitalCertificateID"]), 
                                   Convert.ToInt32(Request.QueryString["FileType"]));
            if (file == null)
                return false;

            Response.AddHeader("Content-disposition", "attachment; filename=" + ViewState["Name"].ToString());
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(file);
            Response.End();
            ViewState["Name"] = null;

            return true;
        }
        /// <summary>
        /// Finds the string that is need to generate the file and converts it into an array of bytes.
        /// </summary>
        /// <param name="DigitalCertificateID">Provider Digital Certificate ID.</param>
        /// <param name="FileType">Specifies whether the file to be look for is a *.cer or *.key</param>
        /// <returns>Return the array of bytes needed to build the requested file.</returns>
        protected Byte[] SeekFile(Int32 DigitalCertificateID, Int32 FileType)
        {
            PACFD.Rules.Provider certificate = new PACFD.Rules.Provider();
            ProviderDataSet.ProviderDigitalCertificatesDataTable ta;
            ta = certificate.GetDigitalCertificatesProviderByID(DigitalCertificateID);
            Byte[] file;

            if (ta.Count < 1)
                return null;

            file = new Byte[ta[0].Certificate.Length];
            file = (FileType == 0 ? Convert.FromBase64String(ta[0].Certificate) : Convert.FromBase64String(ta[0].PrivtaeKey));
            ViewState["Name"] = (FileType == 0 ? ta[0].CertificateName : ta[0].PrivateKeyName);
            ta.Dispose();

            return file;
        }
    }
}
