﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DigitalCertificatesProviderAdd.aspx.cs"
    Inherits="PACFDManager.Provider.DigitalCertificatesProviderAdd" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="BookingContainer" width="750">
                <tr>
                    <td align="center" class="Titulo" colspan="2">
                        <asp:Label ID="lblAddCertificate" runat="server" Text="Agregar Certificado" meta:resourcekey="lblAddCertificateResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:RequiredFieldValidator ID="rfvCertificate" runat="server" ControlToValidate="fuCertificate"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvCertificateResource1"
                            ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblCertificate" runat="server" Text="Certificado:" meta:resourcekey="lblCertificateResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fuCertificate" runat="server" meta:resourcekey="fuCertificateResource1" />
                        <uc2:Tooltip ID="tipCertificate" runat="server" ToolTip="Seleccionar la ruta donde se encuentra el certificado digital&lt;br /&gt;(el archivo con extensión &lt;b&gt;*.cer&lt;/b&gt;)" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:RequiredFieldValidator ID="rfvPrivateKey" runat="server" ControlToValidate="fuPrivateKey"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvPrivateKeyResource1"
                            ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblPrivateKey" runat="server" Text="Llave Privada:" meta:resourcekey="lblPrivateKeyResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fuPrivateKey" runat="server" meta:resourcekey="fuPrivateKeyResource1" />
                        <uc2:Tooltip ID="tipPrivateKey" runat="server" ToolTip="Seleccionar la ruta donde se encuentra la llave privada&lt;br /&gt;(el archivo con extensión &lt;b&gt;*.key&lt;/b&gt;)" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="rfvCcp" runat="server" ControlToValidate="txtCcp"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvCcpResource1" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <asp:Label ID="lblCcp" runat="server" Text="Llave Publica:" meta:resourcekey="lblCcpResource1"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCcp" runat="server" Width="230px" meta:resourcekey="txtCcpResource1"></asp:TextBox>
                        <uc2:Tooltip ID="tipPublicKey" runat="server" ToolTip="Introducir la llave pública.&lt;br /&gt;ej: &lt;b&gt;a0123456789&lt;/b&gt;" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnLoad" runat="server" CssClass="Button" Text="Cargar" OnClick="btnLoad_Click"
                                        meta:resourcekey="btnLoadResource1" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="Button" meta:resourcekey="btnCancelResource1"
                                        Text="Cancelar" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoad" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
