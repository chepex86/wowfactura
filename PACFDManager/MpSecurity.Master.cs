﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager
{
    public partial class MpSecurity : System.Web.UI.MasterPage
    {
        public global::System.Web.UI.UpdateProgress ProgressBar { get { return this.UpdateProgress1; } }

        /// <summary>
        /// Registry a button to do click when a item has been selected from an autocompletextender control.
        /// </summary>
        /// <param name="button">Button to do click event.</param>
        public void RegistryAutoCompletExtenderOnSelectedItem(Button button, AjaxControlToolkit.AutoCompleteExtender autocompletex)
        {
            string javafun = this.ClientID + "_AutoCompletExtenderOnSelectedItem";
            autocompletex.OnClientItemSelected = javafun;

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), javafun,
                string.Format("function {0}(){{ PACFDManager_fbtnClickForAutoCompletExtender_SelectedItem('{1}'); }}", javafun, button.ClientID), true);
        }
        /// <summary>
        /// Registry a button to do click when a item has been selected from an autocompletextender control.
        /// </summary>
        /// <param name="button">HiidenField to do change event.</param>
        public void RegistryAutoCompletExtenderOnSelectedItem(HiddenField hidden, AjaxControlToolkit.AutoCompleteExtender autocompletex)
        {
            string javafun = this.ClientID + "_" + hidden.ClientID + "_AutoCompletExtenderOnSelectedItem";
            autocompletex.OnClientItemSelected = javafun;

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), javafun,
                string.Format("function {0}(){{ PACFDManager_fhflChangeForAutoCompletExtender_SelectItem('{1}'); }}", javafun, hidden.ClientID), true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void RegistryToolTip(UpdatePanel panel)
        {
            //String jquery = "<script src=\"" + ResolveClientUrl("~/Includes/Tooltip/jquery.js")
            //+ "\" originalAttribute=\"src\" originalPath=\"" + ResolveClientUrl("~/Includes/Tooltip/jquery.js") +
            //"\" type=\"text/javascript\" ></script>";

            String jquery = string.Format("<script src=\"{0}\" originalAttribute=\"src\" originalPath=\"{1}\" type=\"text/javascript\" ></script>"
                , ResolveClientUrl("~/Includes/Tooltip/jquery.js"), ResolveClientUrl("~/Includes/Tooltip/jquery.js"));

            //String vtip = "<script src=\"" + ResolveClientUrl("~/Includes/Tooltip/vtip.js")
            //+ "\" originalAttribute=\"src\" originalPath=\"" + ResolveClientUrl("~/Includes/Tooltip/vtip.js") +
            //"\" type=\"text/javascript\" ></script>";

            String vtip = string.Format("<script src=\"{0}\" originalAttribute=\"src\" originalPath=\"{1}\" type=\"text/javascript\" ></script>"
                , ResolveClientUrl("~/Includes/Tooltip/vtip.js"), ResolveClientUrl("~/Includes/Tooltip/vtip.js"));

            ScriptManager.RegisterClientScriptBlock(panel, panel.GetType(), "ScriptJqueryTooltip", jquery + vtip, false);
        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void btnES_Click(object sender, ImageClickEventArgs e)
        {
            Session["SystemCulture"] = "es-MX";
            Session["IdIdioma"] = 1;
            Response.Redirect(Request.Path);
        }

        protected void btnEN_Click(object sender, ImageClickEventArgs e)
        {
            Session["SystemCulture"] = "en-US";
            Session["IdIdioma"] = 2;
            Response.Redirect(Request.Path);
        }
    }
}
