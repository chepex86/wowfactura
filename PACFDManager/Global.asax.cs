﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

using System.Web.Security;
using System.Web.SessionState;

namespace PACFDManager
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol |= (System.Net.SecurityProtocolType)3072;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol |= (System.Net.SecurityProtocolType)3072;

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //Fires upon attempting to authenticate the use
            if (!(HttpContext.Current.User == null))
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity.GetType() == typeof(FormsIdentity))
                    {
                        FormsIdentity fi = (FormsIdentity)HttpContext.Current.User.Identity;
                        FormsAuthenticationTicket fat = fi.Ticket;

                        String[] astrRoles = fat.UserData.Split('|');
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(fi, astrRoles);
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {

       } 

        protected void Session_End(object sender, EventArgs e)
        {
           

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}