<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="FoliosCBBAdd.aspx.cs"
    Inherits="PACFDManager.FoliosCBB.FoliosCBBAdd" %>

<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="container" class="wframe40">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitleCBB" runat="server" Text="Agregar Folios"></asp:Label>
                            </h2>
                        </div>
                        <ul>
                            <li class="left">
                                <label class="desc">
                                    <asp:Label ID="lblPathCBB" runat="server" Text="Seleccionar la ruta de la imagen con el CBB"></asp:Label>
                                </label>
                                <span class="vtip" title="Selecciona la ruta de la imagen del Codigo de Barras Bidimensional.">
                                    <asp:FileUpload ID="fuCBB" runat="server" />
                                </span>
                                <div class="validator-msg">
                                    <asp:RequiredFieldValidator ID="rfvCBB" runat="server" ErrorMessage="Este campo es requerido"
                                        ControlToValidate="fuCBB" Display="Dynamic" ValidationGroup="Validators"></asp:RequiredFieldValidator></div>
                            </li>
                            <li class="buttons clear">
                                <div>
                                    <asp:Button ID="btnLoad" CssClass="Button" runat="server" Text="Aceptar" ValidationGroup="Validators"
                                        OnClick="btnLoad_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div id="cont" runat="server" visible="false">
                <div class="form">
                    <div class="formStyles">
                        <div class="info">
                            <h2>
                                <asp:Label ID="lblTitleData" runat="server" Text="Alta de Folios C�digo de Barras Bidimensional" />
                            </h2>
                        </div>
                        <div id="dvError" runat="server">
                            <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red"></asp:Label>
                        </div>
                        <div id="dvData" runat="server">
                            <ul>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleRFC" runat="server" Text="RFC:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblRFC" runat="server" /></span> </li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleApprovalNumber" runat="server" Text="N�mero de Aprobaci�n:" /></label>
                                    <span>
                                        <asp:Label ID="lblApprivalNumber" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleStart" runat="server" Text="Inicio:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblStart" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleEnd" runat="server" Text="Fin:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblEnd" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleSerie" runat="server" Text="Serie:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblSerie" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleDeliveryDate" runat="server" Text="Fecha de Expedici�n:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblDeliveryDate" runat="server" />
                                    </span></li>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleExpirationDate" runat="server" Text="Fecha de Expiraci�n:" />
                                    </label>
                                    <span>
                                        <asp:Label ID="lblExpirationDate" runat="server" />
                                    </span></li>
                            </ul>
                            <ul>
                                <li class="left">
                                    <label class="desc">
                                        <asp:Label ID="lblTitleTxt" runat="server" Text="Iniciar desde el folio:" />
                                    </label>
                                    <span>
                                        <asp:TextBox ID="txtStart" runat="server" Width="50px" MaxLength="7"></asp:TextBox>
                                    </span>
                                    <div class="validator-msg">
                                        <asp:RegularExpressionValidator ID="revStart" runat="server" ErrorMessage="Solo se permiten n�meros"
                                            ControlToValidate="txtStart" ValidationExpression="\d*" ValidationGroup="Validators2"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvStart" runat="server" ErrorMessage="Este campo es requerido"
                                            ControlToValidate="txtStart" Display="Dynamic" ValidationGroup="Validators2"></asp:RequiredFieldValidator>
                                    </div>
                                </li>
                                <li class="buttons clear">
                                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Agregar" CssClass="Button"
                                        CausesValidation="false" /><%--ValidationGroup="Validators2"--%>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <uc1:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoad" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
