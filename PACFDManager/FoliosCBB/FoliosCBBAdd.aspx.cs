﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.Common;
using PACFD.DataAccess;
using PACFD.Rules;
using System.Text;
using System.Data;
using PACFD.Rules.QRCodeLib;
using PACFD.Rules.QRCodeLib.data;
using System.Drawing;
#endregion

namespace PACFDManager.FoliosCBB
{
    public partial class FoliosCBBAdd : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private bool LoadData()
        {
            QRCodeDecoder decoder = new QRCodeDecoder();
            String[] parameters = new String[7];
            String decodedString = String.Empty;
            Bitmap image, cloneimage;
            string c;

            if (this.fuCBB.PostedFile.IsNull())
            {
                this.dvData.Visible = false;
                this.dvError.Visible = true;
                this.lblError.Text = "Archivo de imagen no valida.";
                return false;
            }

            image = this.fuCBB.PostedFile.InputStream.ToBitmap();

            if (image.IsNull())
            {
                this.dvData.Visible = false;
                this.dvError.Visible = true;
                this.lblError.Text = "Archivo de imagen no valida.";
                return false;
            }

            cloneimage = this.ValidateImageSize(image);
            this.ViewState["image"] = image;

            try
            {
                decodedString = decoder.decode(new QRCodeBitmapImage(new Bitmap(cloneimage)));
                c = this.ValidateCBB(decodedString, ref parameters);
                this.cont.Visible = true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                this.dvData.Visible = false;
                this.dvError.Visible = true;
                this.lblError.Text = "No es posible leer el Código de Barras Bidimensional, es posible que el archivo este dañado<br />ó que no tenga el formato correcto.";
                return false;
            }

            if (string.IsNullOrEmpty(c))
            {
                this.lblRFC.Text = parameters[0];
                this.lblApprivalNumber.Text = parameters[1];
                this.lblStart.Text = parameters[2];
                this.lblEnd.Text = parameters[3];
                this.lblSerie.Text = parameters[4].Trim() == String.Empty ? "-" : parameters[4];
                this.lblDeliveryDate.Text = parameters[5];
                this.lblExpirationDate.Text = parameters[6];
                this.txtStart.Text = parameters[2];
                this.dvData.Visible = true;
                this.dvError.Visible = false;
                return true;
            }

            this.lblError.Text = c;
            this.dvData.Visible = false;
            this.dvError.Visible = true;
            return false;
        }

        public String ValidateCBB(String cbb, ref String[] parameters)
        {
            PACFD.Rules.Billers biller = new PACFD.Rules.Billers();
            BillersDataSet.BillersDataTable table;
            DateTime dt = new DateTime();

            try
            {
                parameters[0] = cbb.Split('|')[1]; //Clave en el RFC del contribuyente emisor.
                parameters[1] = cbb.Split('|')[2]; //Número de aprobación.
                parameters[2] = cbb.Split('|')[3]; //Inicio (Rango de folios asignados).
                parameters[3] = cbb.Split('|')[4]; //Fin (Rango de folios asignados).
                parameters[4] = cbb.Split('|')[5]; //Serie de los folios.
                parameters[5] = cbb.Split('|')[6]; //Fecha de la asignación de los folios.
                parameters[6] = cbb.Split('|')[7]; //Vigencia, la cual será de dos años contados a partir de la fecha de aprobación de la asignación de folios.              

                table = biller.SelectByID(this.CurrentBillerID);

                if (table.Count < 1)
                    return "El RFC en el CBB es diferente al RFC de la empresa.";
                if (table[0].RFC.Trim() != parameters[0].Trim())
                    return "El RFC en el CBB es diferente al RFC de la empresa.";
                if (!this.IsNumeric(parameters[1].Trim()))
                    return "El número de aprobación contiene caracteres no validos.";
                if ((long)Convert.ToDouble(parameters[2]) >= (long)Convert.ToDouble(parameters[3]))
                    return "El rango de folios es incorrecto, el número de inicio debe ser mayor al número final.";
                long x = CheckForValidIndex(this.CurrentBillerID, parameters[4], (long)Convert.ToDouble(parameters[2]));
                if (x != 0)
                    return "Ya existe una serie " + (parameters[4] == String.Empty ? "-" : parameters[4]) +
                        ", la serie debe continuar a partir del folio número " + (x + 1).ToString();
                if (!DateTime.TryParse(parameters[5].Trim(), out dt))
                    return "La fecha de expedición de los folios no contiene el formato correcto (dd/MM/yyyy hh:mm:ss).";
                if (Convert.ToDateTime(parameters[6]) <= DateTime.Now)
                    return "La fecha de expiración de los folios no contiene el formato correcto (dd/MM/yyyy).";
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return "La cadena en el CBB no contiene el formato correcto.<br />Contenido del CBB: [" + cbb + "]";
            }

            return String.Empty;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (!this.LoadData())
                return;
        }

        private long CheckForValidIndex(int BillerID, String serial, long indexStart)
        {
            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.Serial_GetLastIDDataTable ta;

            ta = series.SearchingLastSerialID(BillerID, this.CurrentBranchID, serial);

            if (ta.Count < 1)
                return 0;

            if (indexStart > ta[0].End)
                return 0;

            this.ViewState["End"] = ta[0].End;
            return ta[0].End;
        }

        public bool IsNumeric(String str)
        {
            bool isNumber;
            double isItNumeric;
            isNumber = Double.TryParse(str, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out isItNumeric);
            return isNumber;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (!this.Add())
                return;

            this.WebMessageBox1.Title = "Agregar Folios CBB";
            this.WebMessageBox1.ShowMessage("El bloque de folios ha sido agregado exitosamente.<br /><br />Para poder expedir comprobantes debe activar un bloque de folios del Listado de Folios.",
                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
        }

        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult == WebMessageBoxDialogResultType.Accept)
                this.Response.Redirect("~/Series/SeriesList.aspx");
        }

        private bool Add()
        {
            String Series = this.lblSerie.Text.Trim() == String.Empty ? " " : this.lblSerie.Text.Trim();

            PACFD.Rules.Series series = new PACFD.Rules.Series();
            SerialDataSet ds = new SerialDataSet();
            SerialDataSet.SerialRow drSeries = ds.Serial.NewSerialRow();

            try
            {
                if (!this.ViewState["image"].IsNull())
                {
                    Bitmap image = (Bitmap)this.ViewState["image"];
                    drSeries.QRImage = image.ToByteArray();
                }

                drSeries.BillerID = this.CurrentBillerID;
                drSeries.Serial = Series;
                drSeries.Active = false;
                drSeries.AprovationYear = Convert.ToDateTime(this.lblDeliveryDate.Text.Trim()).Year;
                drSeries.AprovationNumber = Convert.ToInt32(this.lblApprivalNumber.Text.Trim());
                drSeries.Start = Convert.ToInt32(this.txtStart.Text.Trim());
                drSeries.End = Convert.ToInt32(this.lblEnd.Text.Trim());
                drSeries.ExpirationDate = Convert.ToDateTime(this.lblExpirationDate.Text.Trim());
                drSeries.IsFolioCBB = true;
                drSeries.FromFolio = Convert.ToInt32(this.txtStart.Text.Trim());
                drSeries.DeliveryDate = DateTime.Now;

                ds.Serial.AddSerialRow(drSeries);

                SerialDataSet.FoliosRow drFolio;

                for (long i = Convert.ToInt64(this.txtStart.Text.Trim()); i <= Convert.ToInt64(this.lblEnd.Text.Trim()); i += 1)
                {
                    drFolio = ds.Folios.NewFoliosRow();
                    drFolio.Folio = i.ToString();
                    drFolio.Canceled =
                    drFolio.Used = false;
                    drFolio.Completed = false;
                    drFolio.StartDate = System.DateTime.Now;
                    drFolio.SetParentRow(drSeries);
                    ds.Folios.AddFoliosRow(drFolio);
                }
                if (!series.UpdateFolios(ds))
                    return false;

                #region Add new entry to log system
                PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un bloque de folio a travez de una imagen CBB.",
                    this.UserName), this.CurrentBillerID, null, ds.GetXml());
                #endregion

                LogManager.WriteStackTrace(new Exception("Éxito al Agregar Serie de Folios CBB."));

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }
        }

        private System.Drawing.Bitmap ValidateImageSize(System.Drawing.Bitmap qr)
        {
            System.Drawing.Bitmap result = new Bitmap(250, 250);

            using (Graphics g = Graphics.FromImage(result))
            {
                g.Clear(Color.White);
                g.DrawImage(qr, 0, 0, 250, 250);
            }

            return result;
        }
    }
}