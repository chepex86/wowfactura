﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountrySelector.ascx.cs"
    Inherits="PACFDManager.CountrySelector" %>
<%@ Register Src="Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>
<li class="left">
    <label class="desc">
        <asp:Label ID="lblCountry" runat="server" Text="País:" />
    </label>
    <span class="vtip" runat="server" id="spnCountry">
        <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
            AutoPostBack="true">
            <asp:ListItem Text="México" Value="0" />
            <asp:ListItem Text="Otro..." Value="1" />
        </asp:DropDownList>
        <asp:TextBox ID="txtCountry" runat="server" Visible="false" 
        MaxLength="255" />
        <div class="validator-msg">
            <asp:RequiredFieldValidator ID="txtCountry_RequiredFieldValidator" runat="server"
                Display="Dynamic" ControlToValidate="txtCountry" ErrorMessage="El campo es requerido."
                Visible="false" />
        </div>
    </span></li>
<li class="left">
    <label class="desc">
        <asp:Label ID="lblState" runat="server" Text="Estado:" />
    </label>
    <span class="vtip" runat="server" id="spnState">
        <asp:DropDownList ID="ddlState" runat="server">
            <asp:ListItem>Aguascalientes</asp:ListItem>
            <asp:ListItem>Baja California</asp:ListItem>
            <asp:ListItem>Baja California Sur</asp:ListItem>
            <asp:ListItem>Chihuahua</asp:ListItem>
            <asp:ListItem>Colima</asp:ListItem>
            <asp:ListItem>Campeche</asp:ListItem>
            <asp:ListItem>Chiapas</asp:ListItem>
            <asp:ListItem>Coahuila</asp:ListItem>
            <asp:ListItem>Ciuda de México</asp:ListItem>
            <asp:ListItem>Durango</asp:ListItem>
            <asp:ListItem>Guanajuato</asp:ListItem>
            <asp:ListItem>Guerrero</asp:ListItem>
            <asp:ListItem>Hidalgo</asp:ListItem>
            <asp:ListItem>Jalisco</asp:ListItem>
            <asp:ListItem>Michoacan</asp:ListItem>
            <asp:ListItem>Morelos</asp:ListItem>
            <asp:ListItem>Estado Mexico</asp:ListItem>
            <asp:ListItem>Nayarit</asp:ListItem>
            <asp:ListItem>Nuevo Leon</asp:ListItem>
            <asp:ListItem>Oaxaca</asp:ListItem>
            <asp:ListItem>Puebla</asp:ListItem>
            <asp:ListItem>Quintana Roo</asp:ListItem>
            <asp:ListItem>Queretaro</asp:ListItem>
            <asp:ListItem>Sinaloa</asp:ListItem>
            <asp:ListItem>Sonora</asp:ListItem>
            <asp:ListItem>San Luis Potosi</asp:ListItem>
            <asp:ListItem>Tabasco</asp:ListItem>
            <asp:ListItem>Tlaxcala</asp:ListItem>
            <asp:ListItem>Tamaulipas</asp:ListItem>
            <asp:ListItem>Veracruz</asp:ListItem>
            <asp:ListItem>Yucatan</asp:ListItem>
            <asp:ListItem>Zacatecas</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtState" runat="server" Visible="false" MaxLength="255" />
    </span></li>
