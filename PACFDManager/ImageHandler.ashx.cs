﻿#region Usings
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PACFD.Common;
using System.Drawing;
using System.Drawing.Imaging;
using PACFD.DataAccess;
using PACFD.Rules;
#endregion

namespace PACFDManager
{
    /// <summary>
    /// Descripción breve de $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://www.univisit.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ImageHandler : IHttpHandler
    {
        /// <summary>
        /// Page petition main entry
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            Bitmap image = null;
            int TypeImage;

            if (context.Request.QueryString["type"].IsNull())
                return;//???

            if (!int.TryParse(context.Request.QueryString["type"], out TypeImage))
                return;

            if (TypeImage == 1)
                image = this.GetImageForLogo(context);
            else if (TypeImage == 2)
            { /* ... Imagen para la factura*/}
            else if (TypeImage == 3)
                image = this.GetImageForQr(context);
            else if (TypeImage == 4)
                image = this.GetImageForQr_Serial(context);

            if (image.IsNull())
                return;

            image = this.NewBitmap(image); //create a new image for grant write access
            image = this.CalculateSize(context, image); //recalculate size
            context.Response.ContentType = "image/jpeg";
            image.Save(context.Response.OutputStream, ImageFormat.Jpeg);
            image.Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Bitmap GetImageForLogo(HttpContext context)
        {
            Bitmap img;
            MemoryStream memorystream;
            BillersDataSet.BillersDataTable ta;
            PACFD.Rules.Billers biller;
            int billerid = 0;

            try
            {
                img = new Bitmap(context.Request.MapPath("~/Includes/Images/jpg/Default/no_image-125x125.jpg"));
            }
            catch (Exception)
            {
                img = new Bitmap(120, 120);
            }

            if (context.Request.QueryString["BillerID"].IsNull()) //if not event id return default image
                return img;

            if (!int.TryParse(context.Request.QueryString["BillerID"], out billerid))
                return img;

            if (billerid < 1)
                return img;

            biller = new PACFD.Rules.Billers();
            ta = biller.SelectByID(billerid);
            biller = null;

            if (ta.Count < 1)
                return img;

            if (ta[0].IsLogoCFDNull())
                return img;

            if (ta[0].LogoCFD.Length < 1)
                return img;

            try
            {
                memorystream = new MemoryStream();
                memorystream.Write(ta[0].LogoCFD, 0, ta[0].LogoCFD.Length); //get data from buffer
                img = new Bitmap(memorystream); //create image from memory
                memorystream.Dispose(); //free memory
                memorystream = null;
            }
            catch (Exception ex) //can't read or get data from buffer
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
            }

            ta.Dispose(); //free memory
            ta = null;

            return img;
        }
        private Bitmap GetImageForQr(HttpContext context)
        {
            Bitmap img;
            MemoryStream memorystream;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable table;
            PACFD.Rules.Billings bill;
            int billid = 0;

            try
            {
                img = new Bitmap(context.Request.MapPath("~/Includes/Images/jpg/Default/no_image-125x125.jpg"));
            }
            catch (Exception)
            {
                img = new Bitmap(120, 120);
            }

            if (context.Request.QueryString["BillerID"].IsNull()) //if not event id return default image
                return img;

            if (!int.TryParse(context.Request.QueryString["BillerID"], out billid))
                return img;

            if (billid < 1)
                return img;

            bill = new PACFD.Rules.Billings();

            using (table = bill.SelectByID(billid))
            {
                if (table.Count < 1)
                    return img;

                if (table[0].IsQrImageNull())
                    return img;

                if (table[0].QrImage.Length < 1)
                    return img;

                try
                {
                    using (memorystream = new MemoryStream())
                    {
                        memorystream.Write(table[0].QrImage, 0, table[0].QrImage.Length); //get data from buffer
                        img = new Bitmap(memorystream); //create image from memory
                    }
                }
                catch (Exception ex) //can't read or get data from buffer
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                    LogManager.WriteError(ex);
                }
            }

            return img;
        }

        private Bitmap GetImageForQr_Serial(HttpContext context)
        {
            Bitmap img;
            MemoryStream memorystream;
            PACFD.DataAccess.SerialDataSet.SerialDataTable table;
            PACFD.Rules.Series serial;
            int serid = 0;

            try
            {
                img = new Bitmap(context.Request.MapPath("~/Includes/Images/jpg/Default/no_image-125x125.jpg"));
            }
            catch (Exception)
            {
                img = new Bitmap(120, 120);
            }

            if (context.Request.QueryString["SerialID"].IsNull()) //if not event id return default image
                return img;

            if (!int.TryParse(context.Request.QueryString["SerialID"], out serid))
                return img;

            if (serid < 1)
                return img;

            serial = new PACFD.Rules.Series();

            using (table = serial.SelectByIDSerial(serid))
            {
                if (table.Count < 1)
                    return img;

                if (table[0].IsQRImageNull())
                    return img;

                if (table[0].QRImage.Length < 1)
                    return img;

                try
                {
                    using (memorystream = new MemoryStream())
                    {
                        memorystream.Write(table[0].QRImage, 0, table[0].QRImage.Length); //get data from buffer
                        img = new Bitmap(memorystream); //create image from memory
                    }
                }
                catch (Exception ex) //can't read or get data from buffer
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                        + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                    LogManager.WriteError(ex);
                }
            }

            return img;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Create a new image from nother, for grant user access
        /// </summary>
        /// <param name="old">Old image to by copy in the new canvas</param>
        /// <returns>return an image with new user access</returns>
        protected Bitmap NewBitmap(Bitmap old)
        {
            Bitmap newbitmap;
            Graphics g;

            newbitmap = new Bitmap(old.Width, old.Height); //copy size
            g = Graphics.FromImage(newbitmap); //create graphic class
            g.DrawImage(old, 0, 0, old.Width, old.Height); //draw old image in a new canvas
            g.Dispose();

            return newbitmap;
        }
        /// <summary>
        /// Create a new image from a determinate size
        /// </summary>
        /// <param name="context">Thread of the app</param>
        /// <param name="image">Image to resize</param>
        /// <returns>If success return an image resized else return the same image</returns>
        protected Bitmap CalculateSize(HttpContext context, Bitmap image)
        {
            int w, h;

            try
            {
                //if % width or % height is request
                if (context.Request.QueryString["pw"] != null || context.Request.QueryString["ph"] != null)
                {
                    if (context.Request.QueryString["pw"] == null) //width is missing
                    {
                        int.TryParse(context.Request.QueryString["ph"].ToString(), out h); //get height

                        if (h == 0)
                            return image; //error

                        image = image.ToSize((image.Width * h) / image.Height, h); //resize
                    }
                    else
                    {
                        int.TryParse(context.Request.QueryString["pw"].ToString(), out w); //get width

                        if (w == 0)
                            return image; //error

                        image = image.ToSize(w, (image.Height * w) / image.Width); //resize
                    }

                    return image; //return image resized
                }
            }
            catch
            {
                return image; //an error ocurred
            }

            w = image.Width;
            h = image.Height;

            //if not % is needed but width is request
            if (context.Request.QueryString["width"] != null)
                int.TryParse(context.Request.QueryString["width"].ToString(), out w);

            //if not % is needed but height is request
            if (context.Request.QueryString["height"] != null)
                int.TryParse(context.Request.QueryString["height"].ToString(), out h);

            //resize depend on w and h
            image = w != image.Width || h != image.Height ? image.ToSize(
                w > 1 ? w : image.Width,
                h > 1 ? h : image.Height
                ) : image;

            return image; //result image
        }
    }
}
