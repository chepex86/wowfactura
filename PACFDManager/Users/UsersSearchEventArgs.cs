﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Users
{
    /// <summary>
    /// Delegate for the Search Event Handler.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the object caller.</param>
    public delegate void UserSearchEventHandler(object sender, UsersSearchEventArgs e);

    public class UsersSearchEventArgs : EventArgs
    {
        public int? Role { get; private set; }
        public string Email { get; private set; }
        public UsersSearchMode SearchMode { get; private set; }

        public UsersSearchEventArgs(int role, string email, UsersSearchMode editmode)
        {
            this.SearchMode = editmode;
            this.Role = role;
            this.Email = email;
        }
        public UsersSearchEventArgs(int? role, string email)
        {
            this.Role = role;
            this.Email = email;
        }
    }
}
