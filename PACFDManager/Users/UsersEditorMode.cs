﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFDManager.Users
{
    public enum UsersEditorMode
    {
        Add = 0,
        Details = 1,
        List = 2,
        Modify = 3,
    }
}
