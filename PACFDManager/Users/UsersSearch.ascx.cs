﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersSearch : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Event handler for the customers Search.
        /// </summary>
        public event UserSearchEventHandler Search;
        /// <summary>
        /// Get or Set a CustomerSearchMode value with the control mode.
        /// </summary>
        public UsersSearchMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return UsersSearchMode.Role;

                return o.GetType() == typeof(UsersSearchMode) ? (UsersSearchMode)o : UsersSearchMode.Role;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterScriptsForProgressBar();

            if (this.IsPostBack)
            { return; }

            if (this.IsAdvancedClient)
            {
                this.ddlSearchByRole.Visible = false;
                this.lblByRole.Visible = false;
            }
        }
        private void RegisterScriptsForProgressBar()
        {
            if (this.Page.ClientScript.IsStartupScriptRegistered(String.Format("LoadBindControls_{0}", this.Page.ClientID)))
                return;

            String scriptToRegister = String.Empty;
            String uProgressByCode = String.Empty;

            uProgressByCode = String.Format("jsUser.LoadBindControls_OnPageLoad('{0}', '{1}', '{2}');",
                this.txtSearchByName_AutoCompleteExtender.BehaviorID, this.spnByName.ClientID, true);

            scriptToRegister = "Sys.Application.add_load(function() {";
            scriptToRegister += String.Format("{1}{0}{1}", uProgressByCode, System.Environment.NewLine);
            scriptToRegister += "});";

            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), String.Format("LoadBindControls_{0}", this.Page.ClientID), scriptToRegister, true);
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersSearch.OnSearch event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected void OnSearch(UsersSearchEventArgs e)
        {
            if (!this.Search.IsNull())
            { this.Search(this, e); }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersSearch.Search_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UsersSearchEventArgs a;
            String c = String.Empty;
            int? rol = null;

            if (this.IsAdvancedClient)
            { rol = 3; }
            else
            {
                if (this.ddlSearchByRole.SelectedIndex > 0)
                { rol = this.ddlSearchByRole.SelectedItem != null ? this.ddlSearchByRole.SelectedItem.Value.ToInt32() : (int?)null; }
            }

            a = new UsersSearchEventArgs(rol, this.txtSearchByName.Text.Trim() != c ? this.txtSearchByName.Text.Trim() : c
                );
            this.OnSearch(a);
        }
    }
}