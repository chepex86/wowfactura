﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace PACFDManager.Users
{
    /// <summary>
    /// Delegate for the control Apply Changes.
    /// </summary>
    /// <param name="sender">Sender of the event.</param>
    /// <param name="e">Arguments send by the object caller.</param>
    public delegate void UserEditorApplyChangesEventHandler(object sender, UsersEditorEventArgs e);

    public class UsersEditorEventArgs : EventArgs
    {
        public UsersEditorMode EditorMode { get; private set; }
        public String Email { get; private set; }
        public String Password { get; private set; }
        public int Role { get; private set; }
        public bool State { get; private set; }
        public int Level { get; private set; }
        public int GroupID { get; private set; }
        public int BillerID { get; private set; }
        public int[] BillersID { get; private set; }
        public int[] BranchsID { get; private set; }

        public UsersEditorEventArgs(String email, String password, int role, bool state, int level, int groupID, int billerID, int[] billersid, int[] branchsid, UsersEditorMode editorMode)
        {
            this.Email = email;
            this.Password = password;
            this.Role = role;
            this.State = state;
            this.Level = level;
            this.GroupID = groupID;
            this.BillerID = billerID;
            this.EditorMode = editorMode;
            this.BillersID = billersid;
            this.BranchsID = branchsid;
        }
    }
}