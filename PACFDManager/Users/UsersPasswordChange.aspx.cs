﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager.Users
{
    public partial class UsersPasswordChange : PACFDManager.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAcept_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtActualPassword.Text) || string.IsNullOrEmpty(this.txtNewPassword.Text) || string.IsNullOrEmpty(this.txtRepeatPassword.Text))
            {
                this.ShowMessageBox(true);
                this.WebMessageBox1.CommandArguments = "0";
                return;
            }

            if (!string.Equals(this.txtNewPassword.Text, this.txtRepeatPassword.Text))
            {
                this.ShowMessageBox(true);
                this.WebMessageBox1.CommandArguments = "0";
                return;
            }

            if (PACFDManager.Security.Security.ChangePassword(this.UserName, this.txtActualPassword.Text, this.txtNewPassword.Text))
            {
                this.ShowMessageBox(false);
                this.WebMessageBox1.CommandArguments = "1";
                return;
            }

            this.ShowMessageBox(true);
            this.WebMessageBox1.CommandArguments = "0";

        }

        protected void WebMessageBox1_Click(object sender, WebMessageBoxEventArgs e)
        {
            switch (e.CommandArguments)
            {
                default:
                case "0":
                    break;
                case "1":
                    this.Response.Redirect("~");
                    break;
            }
        }

        void ShowMessageBox(bool error)
        {
            this.WebMessageBox1.ShowMessage(error ? "No se pudo cambiar la contraseña." : "Contraseña cambiada correctamente.", WebMessageBoxButtonType.Accept);
        }
    }
}
