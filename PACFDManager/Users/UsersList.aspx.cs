﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersList : PACFDManager.BasePage
    {
        const string MESSAGE_DEACTIVEUSER = "MESSAGE_DEACTIVEUSER";
        const string MESSAGE_ACTIVEUSER = "MESSAGE_ACTIVEUSER";

        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.gvUsersList.Columns[4].Visible = false;

            if (IsPostBack)
            { return; }

            this.Title = this.GetLocalResourceObject("UsersList").ToString();
            this.ucUsersSearch_Search(this.UsersSearch1, new UsersSearchEventArgs((int?)null, null));
        }
        /// <summary>
        /// Event fired by the delegate of the user control ucUsersSearch.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucUsersSearch_Search(object sender, UsersSearchEventArgs e)
        {
            UsersDataSet.Users_GetBySearchingDataTable search;
            //PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            System.Data.DataRow[] rows;
            search = (new PACFD.Rules.Users()).SelectUserBySearching(e.Role, e.Email);

            if (this.IsAdvancedClient)
            { rows = search.Select(string.Format("Role={0} AND GroupID={1}", 3, this.CurrentGroupID)); }
            else
            { rows = search.Select(""); }

            this.gvUsersList.DataSource = rows;//search;
            this.gvUsersList.DataBind();

            if (this.gvUsersList.Rows.Count < 1)
            {
                System.Data.DataTable dt = new UsersDataSet.Users_GetBySearchingDataTable();
                System.Data.DataRow dr = dt.NewRow();
                dr["UserID"] = -1;
                dr["UserName"] = String.Empty;
                dr["Password"] = String.Empty;
                dr["Role"] = -1;
                dr["Active"] = false;
                dr["GroupName"] = String.Empty;
                dr["BillerName"] = String.Empty;
                dr["ActiveText"] = String.Empty;
                dr["RoleName"] = String.Empty;

                dt.Rows.Add(dr);

                ViewState["Empty"] = true;

                this.gvUsersList.DataSource = dt;
                this.gvUsersList.DataBind();
            }
        }
        protected void gvUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowIndex > -1)
            {
                if (!ViewState["Empty"].IsNull() && Convert.ToBoolean(ViewState["Empty"]))
                {
                    for (int i = 0; i < row.Cells.Count; i += 1)
                        row.Cells[i].Visible = false;
                    ViewState["Empty"] = null;
                }
            }
        }
        protected void lbtnDetail_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            Response.Redirect("~/Users/UsersDetails.aspx?UserID=" + s.CommandArgument);
        }
        protected void lbtnEdit_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;
            Response.Redirect("~/Users/UsersModify.aspx?UserID=" + s.CommandArgument);
        }
        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            ImageButton s = sender as ImageButton;

            this.WebMessageBox1.CommandArguments = MESSAGE_DEACTIVEUSER;
            this.WebMessageBox1.Title = "Eliminar usuario";
            this.WebMessageBox1.ShowMessage("¿Desea desactivar al usuario?", System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["UserID"] = s.CommandArgument;
        }
        protected void WebMessageBox1_OnClick(object sender, WebMessageBoxEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds;// = new UsersDataSet();

            if (e.DialogResult == WebMessageBoxDialogResultType.No)
            { return; }

            switch (e.CommandArguments)
            {
                case MESSAGE_DEACTIVEUSER:
                case MESSAGE_ACTIVEUSER:

                    if (this.ViewState["UserID"].IsNull())
                    { return; }

                    using (ds = new UsersDataSet())
                    {
                        user.SelectByIDUsers(ds.Users, Convert.ToInt32(ViewState["UserID"]));

                        String BeforeDataSet = ds.GetXml();

                        if (ds.Users.Count < 1)
                        { return; }

                        UsersDataSet.UsersRow drUsers = ds.Users[0];
                        drUsers.Active = e.CommandArguments == MESSAGE_ACTIVEUSER ? true : false;

                        if (!user.UpdateUsers(ds.Users))
                        {
                            LogManager.WriteStackTrace(new Exception(string.Format("Error al tratar de {0} Usuario"
                                , e.CommandArguments == MESSAGE_ACTIVEUSER ? "activar" : "desactivar")));
                            this.WebMessageBox1.Title = string.Format("{0} usuario"
                                , e.CommandArguments == MESSAGE_ACTIVEUSER ? "Activar" : "Desactivar");
                            this.WebMessageBox1.ShowMessage(
                                string.Format("No se pudo {0} el usuario.", e.CommandArguments == MESSAGE_ACTIVEUSER ? "activar" : "desactivar"),
                                System.Drawing.Color.Red, WebMessageBoxButtonType.Accept);
                            return;
                        }

                        #region Add new entry to log system
                        PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} {1} la cuenta del usuario {2}.",
                            this.UserName, (e.CommandArguments == MESSAGE_ACTIVEUSER ? "activo" : "desactivo"), ds.Users[0].UserName), 
                            this.CurrentBillerID, BeforeDataSet, ds.GetXml());
                        #endregion
                    }

                    this.ViewState["UserID"] = null;
                    LogManager.WriteStackTrace(new Exception(string.Format("Éxito al {0} Usuario."
                        , e.CommandArguments == MESSAGE_ACTIVEUSER ? "activar" : "desactivar")));
                    this.Response.Redirect(typeof(UsersList).Name + ".aspx");
                    break;
            }
        }
        protected void lbtnActivate_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton s = sender as ImageButton;

            this.WebMessageBox1.CommandArguments = MESSAGE_ACTIVEUSER;
            this.WebMessageBox1.Title = "Activar usuario";
            this.WebMessageBox1.ShowMessage("¿Desea activar al usuario?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);

            this.ViewState["UserID"] = s.CommandArgument;
        }
    }
}