﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersModify : PACFDManager.BasePage
    {
        const string MESSAGE_ERROR = "MESSAGE_ERROR";
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            if (Request.QueryString["UserID"].IsNull())
                return;
            ViewState["UserID"] = Request.QueryString["UserID"];

            this.Title = this.GetLocalResourceObject("ModifyUser").ToString();
            this.CustomizeEditor();
            this.LoadData(Convert.ToInt32(ViewState["UserID"]));
        }
        /// <summary>
        /// Load user information.
        /// </summary>
        /// <param name="UserID">int UserID</param>
        private void LoadData(int UserID)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.UsersDataTable ta;
            ta = user.SelectByIDUsers(UserID);
            if (ta != null && ta.Count > 0)
            {
                UsersDataSet.UsersRow dr = ta[0];
                int level = -1;
                int groupID = -1;
                int billerID = -1;

                if (!dr.IsLevelNull())
                    level = dr.Level;

                if (!dr.IsGroupIDNull())
                    groupID = dr.GroupID;

                if (!dr.IsBillerIDNull())
                    billerID = dr.BillerID;

                this.ucUsersEditor.ModifyLoadData(ta[0].UserID, 
                    ta[0].UserName, ta[0].Password, ta[0].Role, level, groupID, billerID);
            }
        }
        /// <summary>
        /// Modify the user information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        private bool Modify(UsersEditorEventArgs e)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();

            user.SelectByIDUsers(ds.Users, Convert.ToInt32(this.ViewState["UserID"]));

            String BeforeDataSet = ds.GetXml();

            if (ds.Users.Count < 1)
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.Title = "Modificar Usuario";
                this.WebMessageBox1.ShowMessage("Ocurrio un problema al tratar de modificar el usuario.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                return false;
            }

            int userid = ds.Users[0].UserID;

            if (!user.DeleteAllUserBiller(userid))
            {
                this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                this.WebMessageBox1.Title = "Modificar Usuario";
                this.WebMessageBox1.ShowMessage("Ocurrio un problema al tratar de modificar el usuario.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                return false;
            }
            
            UsersDataSet.UsersRow drUsers = ds.Users[0];
            drUsers.UserName = e.Email;
            if (e.Password != String.Empty)
                drUsers.Password = Cryptography.EncryptUnivisitString(e.Password);
            drUsers.Role = e.Role;
            drUsers.Level = e.Level;
            drUsers.BillerID = e.BillerID;
            drUsers.GroupID = e.GroupID;

            user.UpdateUsers(ds.Users);

            if (e.Role != 3)
            {
                LogManager.WriteStackTrace(new Exception("Éxito al Modificar Usuario."));
                //this.Response.Redirect(typeof(UsersList).Name + ".aspx");
                this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
                this.WebMessageBox1.Title = "Modificar Usuario";
                this.WebMessageBox1.ShowMessage("El Usuario ha sido modificado exitosamente.",
                    System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                return true;
            }

            UsersDataSet.UserBillerRow drUserBiller;

            for (int i = 0; i < e.BillersID.Length; i++)
            {
                if (e.BillersID[i] != -1)
                {
                    drUserBiller = ds.UserBiller.NewUserBillerRow();

                    drUserBiller.BillerID = e.BillersID[i];
                    drUserBiller.BranchID = e.BranchsID[i];
                    drUserBiller.UserID = userid;
                    ds.UserBiller.AddUserBillerRow(drUserBiller);
                }
            }

            (new PACFD.Rules.Users()).Update_UserBiller(ds.UserBiller);

            #region Add new entry to log system
            PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} modifico información del usuario {1}.",
                this.UserName, ds.Users[0].UserName), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
            #endregion

            LogManager.WriteStackTrace(new Exception("Éxito al Modificar Usuario."));

            this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
            this.WebMessageBox1.Title = "Modificar Usuario";
            this.WebMessageBox1.ShowMessage("El Usuario ha sido modificado exitosamente.",
                System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);

            //this.Response.Redirect(typeof(UsersList).Name + ".aspx");
            return true;
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucUsersEditor_ApplyChanges(object sender, UsersEditorEventArgs e)
        {
            if (e.EditorMode != UsersEditorMode.Modify) return;
            else this.Modify(e);
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucUsersEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(UsersList).Name + ".aspx");
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucUsersEditor.EditorMode = UsersEditorMode.Modify;
            this.ucUsersEditor.ModifyMode = true;
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersModify.WebMessageBox.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(Object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult != WebMessageBoxDialogResultType.Accept)
            { return; }

            switch (e.CommandArguments)
            {
                case MESSAGE_ERROR:
                    return;
                case MESSAGE_SUCCESS:
                    this.Response.Redirect(typeof(UsersList).Name + ".aspx");
                    break;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersModify.WebMessageBox.OnCancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnCancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(UsersList).Name + ".aspx");
        }
    }
}