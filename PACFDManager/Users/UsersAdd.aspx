﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="UsersAdd.aspx.cs" Inherits="PACFDManager.Users.UsersAdd" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="UsersEditor.ascx" TagName="UsersEditor" TagPrefix="uc1" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick"
                OnCancelClick="WebMessageBox1_OnCancelClick" />
            <uc1:UsersEditor ID="ucUsersEditor" runat="server" OnCancelClick="ucUsersEditor_CancelClick"
                OnApplyChanges="ucUsersEditor_ApplyChanges" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
