﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="UsersList.aspx.cs" Inherits="PACFDManager.Users.UsersList" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="UsersEditor.ascx" TagName="UsersEditor" TagPrefix="uc1" %>
<%@ Register Src="UsersSearch.ascx" TagName="UsersSearch" TagPrefix="uc2" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc4" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--<uc1:UsersEditor ID="ucUsersEditor" runat="server" />--%>
            <asp:Panel ID="pnlUsersList" runat="server">
                <uc2:UsersSearch ID="UsersSearch1" runat="server" OnSearch="ucUsersSearch_Search" />
                <br />
                <br />
                <div class="txt_left">
                    <div class="info">
                        <h2>
                            <asp:Label ID="lblTitleList" runat="server" Text="Listado de Usuarios"></asp:Label>
                        </h2>
                    </div>
                    <asp:Button ID="btnAddUsersTop" runat="server" CssClass="Button" PostBackUrl="~/Users/UsersAdd.aspx"
                        Text="Agregar Usuario" />
                    <span>
                        <asp:GridView ID="gvUsersList" runat="server" AutoGenerateColumns="False" CssClass="DataGrid"
                            CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="gvUsersList_RowDataBound"
                            meta:resourcekey="gvUsersListResource1" HorizontalAlign="Center">
                            <RowStyle BackColor="#EFF3FB" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="UserID" HeaderText="ID" />
                                <asp:BoundField DataField="UserName" HeaderText="Correo" />
                                <asp:BoundField DataField="RoleName" HeaderText="Rol" />
                                <asp:BoundField DataField="GroupName" HeaderText="Grupo" />
                                <asp:BoundField DataField="BillerName" HeaderText="Empresa" />
                                <asp:BoundField DataField="ActiveText" HeaderText="Estado" />
                                <asp:TemplateField HeaderText="Detalles">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipDetails" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/kfind.png' alt='Detalles' /> para ver<br />los detalles del usuario seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Ver detalles del usuario<br /><b><%#Eval("UserName")%></b>'>
                                            <asp:ImageButton ID="lbtnDetail" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UserID") %>'
                                                ImageUrl="~/Includes/Images/png/kfind.png" meta:resourcekey="lbtnDetailResource1"
                                                OnClick="lbtnDetail_Click" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Editar" meta:resourcekey="TemplateFieldResource2">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipEdit" runat="server" ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/editIcon.png' alt='Editar' /> para<br />editar el usuario seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='Modificar el usuario<br /><b><%#Eval("UserName")%></b>'>
                                            <asp:ImageButton ID="lbtnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UserID") %>'
                                                ImageUrl="~/Includes/Images/png/editIcon.png" meta:resourcekey="lbtnEditResource1"
                                                OnClick="lbtnEdit_Click" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar" meta:resourcekey="TemplateFieldResource3">
                                    <HeaderTemplate>
                                        <uc3:Tooltip ID="tipDelete" runat="server" 
                                        ToolTip="Hacer click en la imagen <img src='../Includes/Images/png/deleteIcon.png' alt='Desactivar' /> para<br />desactivar el usuario seleccionado.<br /> Hacer click en la imagen <img src='../Includes/Images/png/apply.png' alt='Activar' /> para<br />activar el usuario seleccionado." />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <span class="vtip" title='<%#string.Format("{0} el usuario<br /><b>{1}</b>", Eval("Active").ToString() == "True" ? "Desactiva" : "Activa", Eval("UserName"))%>'>
                                            <asp:ImageButton ID="lbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UserID") %>'
                                                ImageUrl="~/Includes/Images/png/editdelete.png" meta:resourcekey="lbtnDeleteResource1"
                                                OnClick="lbtnDelete_Click" Visible='<%#Convert.ToBoolean(Eval("Active").ToString() == "True" ? "True" : "False")%>'/>
                                            <asp:ImageButton ID="lbtnActivate" runat="server" CausesValidation="False" CommandArgument='<%# Eval("UserID") %>'
                                                ImageUrl="~/Includes/Images/png/apply.png" 
                                            Visible='<%#Convert.ToBoolean(Eval("Active").ToString() == "False" ? "True" : "False")%>' 
                                            onclick="lbtnActivate_Click" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                    </span>
                    <asp:Button ID="btnAddUsersBottom" runat="server" Text="Agregar Usuario" PostBackUrl="~/Users/UsersAdd.aspx"
                        CssClass="Button" />
                </div>
            </asp:Panel>
            <uc4:WebMessageBox ID="WebMessageBox1" runat="server" OnClick="WebMessageBox1_OnClick" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
