﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Default.Master" CodeBehind="UsersDetails.aspx.cs"
    Inherits="PACFDManager.Users.UsersDetails" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Src="UsersEditor.ascx" TagName="UsersEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:UsersEditor ID="ucUsersEditor" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
