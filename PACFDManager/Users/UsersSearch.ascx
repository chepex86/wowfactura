﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersSearch.ascx.cs"
    Inherits="PACFDManager.Users.UsersSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc1" %>

<script src="UsersSearchScripts.js" type="text/javascript"></script>

<div id="container" class="wframe50">
    <div class="form">
        <div class="formStyles">
            <div class="info">
                <h2>
                    <asp:Label ID="lblTitle" runat="server" Text="Búsqueda de Usuarios"></asp:Label>
                </h2>
            </div>
            <ul>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByName" runat="server" Text="Por Correo:"></asp:Label>
                    </label>
                    <span class="vtip" title="Escribe el nombre del Usuario o una parte de el<br />(el <i>autocompletado</i> cargara los resultados que<br />coincidan con el texto introducido).<br /><br />Si no quieres incluir el nombre del Usuario<br />en la búsqueda da clicl en el botón <b>Limpiar</b>.">
                        <asp:TextBox ID="txtSearchByName" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="txtSearchByName_AutoCompleteExtender" runat="server"
                            BehaviorID="aceByName" CompletionInterval="1000" DelimiterCharacters=";" CompletionSetCount="10"
                            FirstRowSelected="true" EnableCaching="true" UseContextKey="True" ServicePath="~/AutoComplete.asmx"
                            TargetControlID="txtSearchByName" MinimumPrefixLength="1" ServiceMethod="GetUserByName">
                        </asp:AutoCompleteExtender>
                    </span><span id="spnByName" runat="server" style="display: none;">
                        <img runat="server" id="imgLoading_ByName" alt="Buscando..." src="~/Includes/Images/loading_small.gif" />
                    </span></li>
                <li class="left">
                    <label class="desc">
                        <asp:Label ID="lblByRole" runat="server" Text="Por Rol:"></asp:Label>
                    </label>
                    <span class="vtip" title="Selecciona el <b>Rol</b> que necesites buscar, o selecciona<br /><b>Todos</b> para incluir todos los roles en la búsqueda.">
                        <asp:DropDownList ID="ddlSearchByRole" runat="server">
                            <asp:ListItem Selected="True" Value="-1">[Todos]</asp:ListItem>
                            <asp:ListItem Value="0">Administrador</asp:ListItem>
                            <asp:ListItem Value="1">SAT</asp:ListItem>
                            <asp:ListItem Value="2">Usuario Avanzado</asp:ListItem>
                            <asp:ListItem Value="3">Usuario Basico</asp:ListItem>
                        </asp:DropDownList>
                    </span></li>
                <li class="left">
                    <div>
                        <asp:Button ID="btnSearch" runat="server" Text="Buscar" CssClass="Button_Search"
                            OnClick="btnSearch_Click" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
