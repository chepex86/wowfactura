﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PACFDManager.Users
{
    partial class UsersSearch
    {
        /// <summary>
        /// Class used to group all main information about UsersSearch class.
        /// </summary>
        public class UsersSearchInfo
        {
            /// <summary>
            /// Get the UsersSearch parent class of the object.
            /// </summary>
            public UsersSearch Parent { get; private set; }
            /// <summary>
            /// 
            /// </summary>
            public DropDownList Role { get { return this.Parent.ddlSearchByRole; } }
            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this.Parent.txtSearchByName.Text; }
                set { this.Parent.txtSearchByName.Text = value; }
            }

            /// <summary>
            /// Create a new instance of the class.
            /// </summary>
            /// <param name="owner">UsersSearch parent of the class.</param>
            public UsersSearchInfo(UsersSearch owner)
            {
                if (owner.IsNull())
                    throw new Exception("Owner can't be null.");

                this.Parent = owner;
            }
        }
    }
}
