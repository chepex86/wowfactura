﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersEditor.ascx.cs"
    Inherits="PACFDManager.Users.UsersEditor" %>
<%@ Register Src="UsersSearch.ascx" TagName="UsersSearch" TagPrefix="uc1" %>
<%--<%@ Register Src="../WebMessageBox.ascx" TagName="WebMessageBox" TagPrefix="uc2" %>--%>
<%@ Register Src="../Tooltip.ascx" TagName="Tooltip" TagPrefix="uc3" %>
<asp:Panel ID="pnlUsersOptions" runat="server" Visible="False" meta:resourcekey="pnlUsersOptionsResource1">
    <div id="container">
        <div class="form">
            <div class="formStyles">
                <div class="info">
                    <h2>
                        <asp:Label ID="lblTitleOptions" runat="server" Text="Titulo" meta:resourcekey="lblTitleOptionsResource1"></asp:Label>
                    </h2>
                </div>
                <ul>
                    <li class="left">
                        <table>
                            <tr>
                                <td>
                                    <label class="desc">
                                        <asp:Label ID="lblEmail" runat="server" Text="Correo:" meta:resourcekey="lblEmailResource1"></asp:Label></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra el identificador único asociado a este usuario." : "Introduce una dirección de correo valida.<br />ej: <b>nombre@mail.com</b>" %>'>
                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="200px" meta:resourcekey="txtEmailResource1"></asp:TextBox></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="validator-msg">
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                            Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Correo Incorrecto" ValidationExpression="^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$"
                                            ValidationGroup="Validators" meta:resourcekey="revEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </div>
                                    <br />
                                    <asp:Label ID="lblAlert" runat="server" ForeColor="Red" Text="Ya existe un usuario registrado con ese correo."
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <li class="left">
                        <table>
                            <tr>
                                <td>
                                    <label class="desc">
                                        <asp:Label ID="lblPassword" runat="server" Text="Contraseña:" meta:resourcekey="lblPasswordResource1"></asp:Label></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra la dirección de correo del usuario." : "Introduce una contraseña de <b>1</b> a <b>50</b> caracteres." %>'>
                                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" Width="200px" TextMode="Password"
                                            meta:resourcekey="txtPasswordResource1"></asp:TextBox></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="validator-msg">
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                            Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <li class="left">
                        <table>
                            <tr>
                                <td>
                                    <label class="desc">
                                        <asp:Label ID="lblPasswordConfirm" runat="server" Text="Confirmar Contraseña:" meta:resourcekey="lblPasswordConfirmResource1"></asp:Label></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="vtip" title="Introduce la misma contraseña que la anterior.">
                                        <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="50" Width="200px"
                                            TextMode="Password" meta:resourcekey="txtPasswordConfirmResource1"></asp:TextBox></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="validator-msg"">
                                        <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" ControlToValidate="txtPasswordConfirm"
                                            Display="Dynamic" ErrorMessage="Este campo es requerido" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvPasswordConfirm" runat="server" ControlToCompare="txtPassword"
                                            ControlToValidate="txtPasswordConfirm" ErrorMessage="Contraseña diferente" ValidationGroup="Validators"
                                            meta:resourcekey="cvPasswordConfirmResource1" Display="Dynamic"></asp:CompareValidator>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <li>
                        <label class="desc">
                            <asp:Label ID="lblRole" runat="server" meta:resourcekey="lblRoleResource1" Text="Rol:"></asp:Label></label>
                        <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra el Rol que tiene asignado el usuario.<br />(Para saber los privilegios de cada rol<br />consultar el manual del Usuario)." : "Selecciona el Rol que tendra el usuario.<br />(Para saber los privilegios de cada rol<br />consultar el manual del Usuario)."%>'>
                            <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" meta:resourcekey="ddlRoleResource1"
                                OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                                <asp:ListItem Value="0">Administrador</asp:ListItem>
                                <asp:ListItem Value="1">SAT</asp:ListItem>
                                <asp:ListItem Value="2">Usuario Avanzado</asp:ListItem>
                                <asp:ListItem Value="3">Usuario Básico</asp:ListItem>
                            </asp:DropDownList>
                        </span></li>
                    <li id="trGroup" runat="server">
                        <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGrupo"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvPasswordResource1" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblGroup" runat="server" Text="Grupo:"></asp:Label></label>
                        <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra el grupo al que esta ligado el usuario." : "Selecciona el grupo al que estará ligado el usuario."%>'>
                            <asp:DropDownList ID="ddlGrupo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrupo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </span></li>
                    <li id="trBillerList" runat="server" visible="false">
                        <label class="desc">
                            <asp:Label ID="lblBillerList" runat="server" Text="Listado de Empresas:" />
                        </label>
                        <asp:CheckBoxList ID="cblBillerList" runat="server">
                        </asp:CheckBoxList>
                        <asp:GridView ID="gvBillerList" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Seleccionar">
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox ID="cbBiller" runat="server" />
                                            <asp:HiddenField ID="hfBillerID" runat="server" Value='<%#Eval("BillerID")%>' />
                                            <asp:HiddenField ID="hfBranchID" runat="server" Value='<%#Eval("BranchID")%>' />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Empresa" />
                                <asp:BoundField DataField="BranchName" HeaderText="Sucursal" />
                            </Columns>
                        </asp:GridView>
                        <span>
                            <asp:Label ID="lblAlertList" runat="server" Text="Selecciona 1 empresa." ForeColor="Red"
                                Visible="False" /></span> </li>
                    <li id="trBiller" runat="server">
                        <asp:RequiredFieldValidator ID="rfvBiller" runat="server" ControlToValidate="ddlBiller"
                            Display="Dynamic" ErrorMessage="*" meta:resourcekey="rfvPasswordResource1" ValidationGroup="Validators"></asp:RequiredFieldValidator>
                        <label class="desc">
                            <asp:Label ID="lblBiller" runat="server" Text="Empresa:"></asp:Label></label>
                        <span class="vtip" title='<%=ToolTipMode() == "True" ? "Muestra la empresa a la que esta ligado el usuario." : "Selecciona la empresa a la que estará ligado el usuario."%>'>
                            <asp:DropDownList ID="ddlBiller" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </span></li>
                    <li class="buttons">
                        <div>
                            <asp:Button ID="btnAccept" runat="server" Text="Aceptar" CssClass="Button" OnClick="btnAccept_Click"
                                ValidationGroup="Validators" meta:resourcekey="btnAcceptResource1" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="Button" meta:resourcekey="btnCancelResource1"
                                OnClick="btnCancel_Click" Text="Cancelar" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Panel>
<%--<div>
    <uc2:WebMessageBox ID="WebMessageBox1" OnClick="WebMessageBox1_OnClick" runat="server" />
</div>--%>