﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using System.Data;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersEditor : PACFDManager.BaseUserControl
    {
        /// <summary>
        /// Event handler for the control Apply Changes.
        /// </summary>
        public event UserEditorApplyChangesEventHandler ApplyChanges;
        /// <summary>
        /// Event handler for the control Cancel Changes.
        /// </summary>
        public event EventHandler CancelClick;
        /// <summary>
        /// Current password property.
        /// </summary>
        private String CurrentPassword
        {
            get
            {
                if (!ViewState[String.Format("{0}-CurrentPassword", ClientID)].IsNull())
                    return ViewState[String.Format("{0}-CurrentPassword", ClientID)].ToString();

                return String.Empty;
            }
            set { ViewState[String.Format("{0}-CurrentPassword", ClientID)] = value; }
        }
        /// <summary>
        /// Current confirm password property
        /// </summary>
        private String CurrentConfirmPassword
        {
            get
            {
                if (!ViewState[String.Format("{0}-CurrentConfirmPassword", ClientID)].IsNull())
                    return ViewState[String.Format("{0}-CurrentConfirmPassword", ClientID)].ToString();

                return String.Empty;
            }
            set { ViewState[String.Format("{0}-CurrentConfirmPassword", ClientID)] = value; }
        }
        /// <summary>
        /// Disable the validators of the Password field.
        /// </summary>
        public bool DisablePasswordValidators
        {
            set
            {
                this.rfvPassword.Enabled =
                this.rfvPasswordConfirm.Enabled = value;
            }
        }
        /// <summary>
        /// Disable all validators of the editor.
        /// </summary>
        public bool DisableAllValidators
        {
            set
            {
                this.cvPasswordConfirm.Enabled =
                this.revEmail.Enabled =
                this.rfvEmail.Enabled =
                this.rfvPassword.Enabled =
                this.rfvPasswordConfirm.Enabled = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor.
        /// </summary>
        public bool AddMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("AddNewUser").ToString();
                this.btnAccept.Text = "Aceptar";//this.GetLocalResourceObject("Add").ToString();
                this.pnlUsersOptions.Visible = value;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor.
        /// </summary>
        public bool ModifyMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("ModifyUser").ToString();
                this.btnAccept.Text = "Aceptar";//this.GetLocalResourceObject("Modify").ToString();
                this.pnlUsersOptions.Visible = value;
                this.DisablePasswordValidators = true;
            }
        }
        /// <summary>
        /// Sets the mode of how to behave the editor.
        /// </summary>
        public bool DetailMode
        {
            set
            {
                this.lblTitleOptions.Text = this.GetLocalResourceObject("UserDetail").ToString();
                this.btnAccept.Text = "Aceptar";//this.GetLocalResourceObject("Return").ToString();
                this.lblEmail.Text = this.GetLocalResourceObject("UserID").ToString();
                this.lblPassword.Text = this.GetLocalResourceObject("Email").ToString();
                this.lblPasswordConfirm.Text = this.GetLocalResourceObject("State").ToString();
                this.txtPassword.TextMode = TextBoxMode.SingleLine;
                this.txtPasswordConfirm.TextMode = TextBoxMode.SingleLine;
                this.txtEmail.ReadOnly =
                this.txtPassword.ReadOnly = true;
                this.ddlRole.Enabled =
                this.txtPasswordConfirm.Enabled =
                this.txtPasswordConfirm.Visible =
                this.lblPasswordConfirm.Visible =
                this.btnCancel.Visible =
                this.DisableAllValidators =
                this.ddlBiller.Enabled =
                this.ddlGrupo.Enabled = false;
                this.pnlUsersOptions.Visible = value;
            }
        }
        /// <summary>
        /// Show or hide the user name tag repeated.
        /// </summary>
        public bool RepeatedUsername { set { this.lblAlert.Visible = value; } }
        /// <summary>
        /// Get or Set a UsersEditorMode value with the control mode.
        /// </summary>
        public UsersEditorMode EditorMode
        {
            get
            {
                object o;

                o = this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]];

                if (o.IsNull())
                    return UsersEditorMode.Add;

                return o.GetType() == typeof(UsersEditorMode) ? (UsersEditorMode)o : UsersEditorMode.Add;
            }
            set
            {
                this.ViewState[this.ClientID + "_" + System.Reflection.MethodInfo.GetCurrentMethod().Name.Split('_')[1]] = value;
            }
        }
        /// <summary>
        /// Get the last biller ID used.
        /// </summary>
        public int LastBillerID
        {
            set { ViewState[string.Format("{0}LastBillerID", ClientID)] = value; }
            get
            {
                if (ViewState[string.Format("{0}LastBillerID", ClientID)] != null)
                {
                    try
                    {
                        return (int)ViewState[string.Format("{0}LastBillerID", ClientID)];
                    }
                    catch { }

                }
                return -1;
            }
        }
        /// <summary>
        /// Load data for editor in edit mode.
        /// </summary>
        /// <param name="email">string email</param>
        /// <param name="password">string password</param>
        /// <param name="role">int role</param>
        /// <param name="level">int level</param>
        /// <param name="groupId">int groupId</param>
        /// <param name="billerID">int billerID</param>
        public void ModifyLoadData(int userID, String email, String password, int role, int level, int groupId, int billerID)
        {
            this.trGroup.Visible = false;
            this.trBiller.Visible = false;

            this.txtEmail.Text = email;
            this.ddlRole.SelectedIndex = role;
            password = Cryptography.DecryptUnivisitString(password);

            this.CurrentPassword = password;
            this.CurrentConfirmPassword = password;
            this.LoadGroup();
            if (role == 2)
            {
                this.trGroup.Visible = true;
                this.ddlGrupo.SelectedIndex = this.ddlGrupo.Items.IndexOf(this.ddlGrupo.Items.FindByValue(groupId.ToString()));
            }
            else if (role == 3)
            {
                trGroup.Visible = true;
                this.trBillerList.Visible = true;

                PACFD.Rules.Users u = new PACFD.Rules.Users();

                using (UsersDataSet.GetUserBillerSelectedDataTable t = u.GetUserBillerSelected(userID))
                {
                    //ListItem item;

                    this.gvBillerList.DataSource = t;
                    this.gvBillerList.DataBind();

                    for (int i = 0; i < gvBillerList.Rows.Count; i++)
                    {
                        (this.gvBillerList.Rows[i].FindControl("cbBiller") as CheckBox).Checked = t[i].BillerSelected == 1 ? true : false;
                    }

                    //for (int i = 0; i < t.Count; i++)
                    //{
                    //    item = new ListItem(t[i].Name, t[i].BillerID.ToString());
                    //    this.cblBillerList.Items.Add(item);
                    //    this.cblBillerList.Items[i].Selected = t[i].BillerSelected == 1 ? true : false;
                    //}
                    //ddlGrupo.SelectedIndex = ddlGrupo.Items.IndexOf(ddlGrupo.Items.FindByValue(t[0].GroupID.ToString()));
                    //this.trBillerList.Style["height"] = String.Format("{0}px", t.Count * 30);
                }

                this.ddlGrupo.SelectedIndex = this.ddlGrupo.Items.IndexOf(this.ddlGrupo.Items.FindByValue(groupId.ToString()));

                LastBillerID = billerID;
            }
        }
        /// <summary>
        /// Load data for editor in detail mode.
        /// </summary>
        /// <param name="UserID">string UserID</param>
        /// <param name="email">string email</param>
        /// <param name="role">int role</param>
        /// <param name="password">string password</param>
        /// <param name="groupId">int groupId</param>
        /// <param name="billerID">int billerID</param>
        public void DetailsLoadData(String UserID, String email, int role, String password, int groupId, int billerID)
        {
            this.txtEmail.Text = UserID;
            this.txtPassword.Text = email;
            this.txtPasswordConfirm.Text = password;
            this.ddlRole.SelectedIndex = role;
            password = Cryptography.DecryptUnivisitString(password);

            this.CurrentPassword = password;
            this.CurrentConfirmPassword = password;
            this.LoadGroup();
            this.trGroup.Visible = false;
            this.trBiller.Visible = false;
            if (role == 2)
            {
                this.trGroup.Visible = true;
                this.ddlGrupo.SelectedIndex = this.ddlGrupo.Items.IndexOf(this.ddlGrupo.Items.FindByValue(groupId.ToString()));
            }
            else if (role == 3)
            {
                this.trGroup.Visible = true;
                this.trBillerList.Visible = true;

                PACFD.Rules.Users u = new PACFD.Rules.Users();

                using (UsersDataSet.GetUserBillerSelectedDataTable t = u.GetUserBillerSelected(int.Parse(UserID)))
                {
                    //ListItem item;

                    this.gvBillerList.DataSource = t;
                    this.gvBillerList.DataBind();

                    for (int i = 0; i < gvBillerList.Rows.Count; i++)
                    {
                        (this.gvBillerList.Rows[i].FindControl("cbBiller") as CheckBox).Checked = t[i].BillerSelected == 1 ? true : false;
                        (this.gvBillerList.Rows[i].FindControl("cbBiller") as CheckBox).Enabled = false;
                    }

                    //for (int i = 0; i < t.Count; i++)
                    //{
                    //    item = new ListItem(t[i].Name, t[i].BillerID.ToString());
                    //    this.cblBillerList.Items.Add(item);
                    //    this.cblBillerList.Items[i].Selected = t[i].BillerSelected == 1 ? true : false;
                    //}

                    //ddlGrupo.SelectedIndex = ddlGrupo.Items.IndexOf(ddlGrupo.Items.FindByValue(t[0].GroupID.ToString()));
                    //this.cblBillerList.Enabled = false;
                    //this.trBillerList.Style["height"] = String.Format("{0}px", t.Count * 60);
                }

                this.ddlGrupo.SelectedIndex = this.ddlGrupo.Items.IndexOf(this.ddlGrupo.Items.FindByValue(groupId.ToString()));

                LastBillerID = billerID;
            }
        }
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.EditorMode == UsersEditorMode.Add)
                {
                    this.trGroup.Visible = false;
                    this.trBiller.Visible = false;
                }

                if (this.IsAdministrator && this.EditorMode ==UsersEditorMode.Add)
                {

                    this.LoadGroup(); }
                if (this.IsAdvancedClient)
                {
                    this.lblRole.Visible = false;
                    this.ddlRole.Visible = false;
                    this.ddlRole.ClearSelection();
                    this.ddlRole.Items.FindByValue("3").Selected = true;
                    this.ddlRole_SelectedIndexChanged(this.ddlRole, EventArgs.Empty);
                    this.LoadGroup();
                    this.ddlGrupo.Visible = false;
                    this.lblGroup.Visible = false;
                }
            }
            else
            {
                if (this.EditorMode == UsersEditorMode.Add || this.EditorMode == UsersEditorMode.Modify)
                {
                    this.CurrentPassword = this.txtPassword.Text;
                    this.CurrentConfirmPassword = this.txtPasswordConfirm.Text;
                }
            }

            this.txtPassword.Attributes["value"] = this.CurrentPassword;
            this.txtPasswordConfirm.Attributes["value"] = this.CurrentConfirmPassword;
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ApplyChanges event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnApplyChanges(UsersEditorEventArgs e)
        {
            if (e.IsNull())
                return;

            if (!this.ApplyChanges.IsNull())
            { this.ApplyChanges(this, e); }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.CancelClik event.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        protected virtual void OnCancelClick(EventArgs e)
        {
            if (!this.CancelClick.IsNull())
                this.CancelClick(this, e);
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.btnAccept_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.OnApplyChanges(this.OnGenerateAcceptArguments());
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.btnCancel_Click event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.OnCancelClick(e);
        }
        /// <returns>
        /// Returns a new instance of UsersEditorEventArgs, 
        /// the parameters entered into the editor of users.
        /// </returns>
        protected virtual UsersEditorEventArgs OnGenerateAcceptArguments()
        {
            int role = -1;
            int groupID = -1;
            int billerID = -1;
            int[] billersID = new int[this.gvBillerList.Rows.Count];//int[this.cblBillerList.Items.Count];
            int[] branchsID = new int[this.gvBillerList.Rows.Count];
            //int level = 0;

            if (this.IsAdministrator)
            { int.TryParse(ddlRole.SelectedValue, out role); }

            if (this.IsAdvancedClient)
            { role = 3; }

            if (role == 2)//level = 1; advanced user
            { int.TryParse(ddlGrupo.SelectedValue, out groupID); }
            else if (role == 3)//level = 2; basic user
            {
                int.TryParse(ddlGrupo.SelectedValue, out groupID);
                int.TryParse(ddlBiller.SelectedValue, out billerID);

                bool flag = false;

                for (int i = 0; i < this.gvBillerList.Rows.Count; i++)
                {
                    if ((this.gvBillerList.Rows[i].FindControl("cbBiller") as CheckBox).Checked) 
                    {
                        flag = true;
                        break;
                    }
                }

                //for (int i = 0; i < this.cblBillerList.Items.Count; i++)
                //{
                //    if (this.cblBillerList.Items[i].Selected)
                //    {
                //        flag = true;
                //        break;
                //    }
                //}

                if (!flag)
                {
                    this.lblAlertList.Visible = true;
                    return null;
                }
                else
                {
                    this.lblAlertList.Visible = false;
                }

                for (int i = 0; i < this.gvBillerList.Rows.Count; i++)
                {
                    if ((this.gvBillerList.Rows[i].FindControl("cbBiller") as CheckBox).Checked)
                    {
                        billersID[i] = int.Parse((this.gvBillerList.Rows[i].FindControl("hfBillerID") as HiddenField).Value);
                        branchsID[i] = int.Parse((this.gvBillerList.Rows[i].FindControl("hfBranchID") as HiddenField).Value);
                    }
                    else
                    {
                        billersID[i] = -1;
                        branchsID[i] = -1;
                    }
                }

                //for (int i = 0; i < this.cblBillerList.Items.Count; i++)
                //{
                //    if (this.cblBillerList.Items[i].Selected)
                //        billersID[i] = int.Parse(this.cblBillerList.Items[i].Value);
                //    else
                //        billersID[i] = -1;
                //}
            }

            return new UsersEditorEventArgs(this.txtEmail.Text, this.txtPassword.Text,
                Convert.ToInt32(this.ddlRole.SelectedValue), true, -1, groupID, billerID, billersID, branchsID, this.EditorMode);
        }
        /// <summary>
        /// Check the current status of the editor.
        /// </summary>
        /// <returns>Return a string true or false.</returns>
        public String ToolTipMode()
        {
            return this.EditorMode == UsersEditorMode.Details ? "True" : "False";
        }
        /// <summary>
        /// Load the group information.
        /// </summary>
        protected void LoadGroup()
        {
            System.Data.DataRow[] rows;
            this.ddlGrupo.Items.Clear();

            using (GroupsDataSet ds = new GroupsDataSet())
            {
                (new PACFD.Rules.Groups()).SelectBySearching(ds.Groups, null, true);

                if (this.IsAdvancedClient)
                { rows = ds.Groups.Select(string.Format("GroupID={0}", this.CurrentGroupID)); }
                else
                { rows = ds.Groups.Select(""); }
            }

            this.ddlGrupo.DataSource = rows;//ds.Groups;
            this.ddlGrupo.DataTextField = "Name";
            this.ddlGrupo.DataValueField = "GroupID";
            this.ddlGrupo.DataBind();

            //if (ds.Groups.Count > 0)
            if (this.ddlGrupo.Items.Count > 0)
            {
                this.ddlGrupo.SelectedIndex = 0;
                ddlGrupo_SelectedIndexChanged(this.ddlGrupo, EventArgs.Empty);
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ddlRole_SelectedIndexChanged event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Object caller of the method.</param>
        protected void ddlRole_SelectedIndexChanged(Object sender, EventArgs e)
        {
            int role = -1;
            this.trGroup.Visible =
            this.lblBillerList.Visible =
            this.trBiller.Visible = false;

            if (this.ddlRole.SelectedValue.IsNull() || !int.TryParse(this.ddlRole.SelectedValue, out role))
                return;

            switch (role)
            {
                case 0:
                case 1:
                    this.trGroup.Visible =
                    this.lblBillerList.Visible =
                    this.trBillerList.Visible = false;
                    break;
                case 2:
                    this.trGroup.Visible = true;
                    this.lblBillerList.Visible =
                    this.trBillerList.Visible = false;
                    break;
                case 3:
                    this.trGroup.Visible =
                    this.lblBillerList.Visible =
                    this.trBillerList.Visible = true;
                    break;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ddlGrupo_SelectedIndexChanged event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Object caller of the method.</param>
        protected void ddlGrupo_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (ddlGrupo.SelectedIndex == -1)
            { return; }

            try
            {
                if (this.EditorMode == UsersEditorMode.Add)
                    this.LoadBillerList();
                else if(IsPostBack)
                {
                    this.LoadBillerList();
 
                }
                //int id = -1;
                //ddlBiller.Items.Clear();
                //if (int.TryParse(ddlGrupo.SelectedValue, out id))
                //{
                //    PACFD.DataAccess.BillersDataSet ds = new PACFD.DataAccess.BillersDataSet();
                //    PACFD.Rules.Billers rules = new PACFD.Rules.Billers();
                //    rules.SelectBillersForList(ds.Billers_GetForList, id, true);
                //    if (ds.Billers_GetForList != null)
                //    {
                //        ddlBiller.DataSource = ds.Billers_GetForList;
                //        ddlBiller.DataTextField = "Name";
                //        ddlBiller.DataValueField = "BillerID";
                //        ddlBiller.DataBind();
                //        if (ds.Billers_GetForList.Count > 0)
                //        {
                //            if (EditorMode == UsersEditorMode.Details || EditorMode == UsersEditorMode.Modify)
                //                ddlBiller.SelectedIndex = ddlBiller.Items.IndexOf(ddlBiller.Items.FindByValue(LastBillerID.ToString()));
                //            else
                //                ddlBiller.SelectedIndex = 0;
                //        }
                //    }
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void LoadBillerList()
        {
            PACFD.Rules.Billers bill = new PACFD.Rules.Billers();
            BillersDataSet.Billers_GetForListDataTable dt = new BillersDataSet.Billers_GetForListDataTable();

            bill.SelectBillersForList(dt, int.Parse(this.ddlGrupo.SelectedItem.Value), true);

            this.gvBillerList.DataSource = dt;
            this.gvBillerList.DataBind();

            //ListItem item;
            //this.cblBillerList.Items.Clear();

            //for (int i = 0; i < dt.Count; i++)
            //{
            //    item = new ListItem(dt[i].Name, dt[i].BillerID.ToString());
            //    this.cblBillerList.Items.Add(item);
            //}

            //this.trBillerList.Style["height"] = String.Format("{0}px", dt.Count * 30);
        }

        private void LoadBillerListForDetails()
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet.UserBillerDataTable table;

            table = user.GetUserBillerByUserID(1);
        }
    }
}