﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersDetails : PACFDManager.BasePage
    {
        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                this.Response.Redirect(typeof(UsersList).Name + ".aspx");

            this.Title = this.GetLocalResourceObject("UserDetails").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            if (Request.QueryString["UserID"].IsNull())
                this.Response.Redirect(typeof(UsersList).Name + ".aspx");
            this.LoadData(Convert.ToInt32(Request.QueryString["UserID"]));
            this.ucUsersEditor.EditorMode = UsersEditorMode.Details;
            this.ucUsersEditor.DetailMode = true;
        }
        /// <summary>
        /// Load user information.
        /// </summary>
        /// <param name="UserID">int UserID</param>
        private void LoadData(int UserID)
        {
            PACFD.Rules.Users user = new PACFD.Rules.Users();
            UsersDataSet ds = new UsersDataSet();
            UsersDataSet.UsersDataTable ta;
            ta = user.SelectByIDUsers(UserID);
            ta = user.SelectByIDUsers(UserID);
            if (ta != null && ta.Count > 0)
            {
                UsersDataSet.UsersRow dr = ta[0];
                int level = -1;
                int groupID = -1;
                int billerID = -1;

                if (!dr.IsLevelNull())
                    level = dr.Level;

                if (!dr.IsGroupIDNull())
                    groupID = dr.GroupID;

                if (!dr.IsBillerIDNull())
                    billerID = dr.BillerID;

                this.ucUsersEditor.DetailsLoadData(ta[0].UserID.ToString(), ta[0].UserName, ta[0].Role,
                                       ta[0].Password, groupID, billerID);
            }   
        }
    }
}