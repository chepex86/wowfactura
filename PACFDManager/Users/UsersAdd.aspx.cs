﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PACFD.DataAccess;
using PACFD.Rules;
using PACFD.Common;
using PACFD.Rules.Mail;
using System.Data;
#endregion

namespace PACFDManager.Users
{
    public partial class UsersAdd : PACFDManager.BasePage
    {
        const string MESSAGE_ERROR = "MESSAGE_ERROR";
        const string MESSAGE_SUCCESS = "MESSAGE_SUCCESS";

        /// <summary>
        /// Load method of the web page.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            this.Title = this.GetLocalResourceObject("AddUser").ToString();
            this.CustomizeEditor();
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.ApplyChanges event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucUsersEditor_ApplyChanges(object sender, UsersEditorEventArgs e)
        {
            if (e.EditorMode != UsersEditorMode.Add)
            { return; }

            using (UsersDataSet ds = new UsersDataSet())
            {
                using (UsersDataSet.Users_GetByUserNameDataTable t = (new PACFD.Rules.Users()).SelectByUserName(e.Email.Trim()))
                {
                    if (t.Count > 0)
                    {
                        this.ucUsersEditor.RepeatedUsername = true;
                        this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                        this.WebMessageBox1.Title = "Agregar Usuario";
                        this.WebMessageBox1.ShowMessage("El Usuario ya existe", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                        return;
                    }

                    String BeforeDataSet = ds.GetXml();

                    UsersDataSet.UsersRow drUser = ds.Users.NewUsersRow();
                    drUser.UserName = e.Email;
                    drUser.Password = Cryptography.EncryptUnivisitString(e.Password);
                    drUser.Active = true;
                    drUser.Role = e.Role;
                    drUser.Level = e.Level;
                    drUser.GroupID = e.GroupID;
                    drUser.BillerID = e.BillerID;
                    ds.Users.AddUsersRow(drUser);

                    int userid = (new PACFD.Rules.Users()).UpdateUser(ds.Users);

                    if (userid == -1)
                    {
                        this.WebMessageBox1.CommandArguments = MESSAGE_ERROR;
                        this.WebMessageBox1.Title = "Agregar Usuario";
                        this.WebMessageBox1.ShowMessage("El Usuario ya existe", System.Drawing.Color.Green, WebMessageBoxButtonType.Accept);
                        return;
                    }

                    if (e.Role == 3)
                    {
                        UsersDataSet.UserBillerRow drUserBiller;

                        for (int i = 0; i < e.BillersID.Length; i++)
                        {
                            if (e.BillersID[i] != -1)
                            {
                                drUserBiller = ds.UserBiller.NewUserBillerRow();

                                drUserBiller.BillerID = e.BillersID[i];
                                drUserBiller.BranchID = e.BranchsID[i];
                                drUserBiller.UserID = userid;
                                ds.UserBiller.AddUserBillerRow(drUserBiller);
                            }
                        }

                        (new PACFD.Rules.Users()).Update_UserBiller(ds.UserBiller);

                        #region Add new entry to log system
                        PACFDLog.LogManager.Insert(this.UserID, String.Format("El usuario {0} agrego un nuevo usuario [{1}] al sistema.",
                            this.UserName, e.Email), this.CurrentBillerID, BeforeDataSet, ds.GetXml());
                        #endregion

                        this.SendEmail(e);
                    }
                }
            }

            LogManager.WriteStackTrace(new Exception("Éxito al Agregar Nuevo Usuario."));
            this.WebMessageBox1.CommandArguments = MESSAGE_SUCCESS;
            this.WebMessageBox1.Title = "Agregar Usuario";
            this.WebMessageBox1.ShowMessage("El Usuario ha sido agregado exitosamente,<br />¿Deseas agregar otro?",
                System.Drawing.Color.Green, WebMessageBoxButtonType.YesNo);
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersAdd.WebMessageBox.OnClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnClick(Object sender, WebMessageBoxEventArgs e)
        {
            if (e.DialogResult != WebMessageBoxDialogResultType.No)
            { return; }

            switch (e.CommandArguments)
            {
                case MESSAGE_ERROR:
                    break;
                case MESSAGE_SUCCESS:
                    this.Response.Redirect(typeof(UsersAdd).Name + ".aspx");
                    break;
            }
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersAdd.WebMessageBox.OnCancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void WebMessageBox1_OnCancelClick(object sender, EventArgs e)
        {
            //this.Response.Redirect(typeof(UsersAdd).Name + ".aspx");
            this.Response.Redirect(typeof(UsersList).Name + ".aspx");
        }
        /// <summary>
        /// Fires the PACFDManager.Users.UsersEditor.CancelClick event.
        /// </summary>
        /// <param name="sender">Object caller of the method.</param>
        /// <param name="e">Arguments send by the object.</param>
        protected void ucUsersEditor_CancelClick(object sender, EventArgs e)
        {
            this.Response.Redirect(typeof(UsersList).Name + ".aspx");
        }
        /// <summary>
        /// Inserts a new row with the customer information.
        /// </summary>
        /// <param name="e">Arguments send by the object.</param>
        /// <returns>Returns true if the process was completed successfully, otherwise returns false.</returns>
        //private bool Add(UsersEditorEventArgs e)
        //{
        //PACFD.Rules.Users user = new PACFD.Rules.Users();
        //UsersDataSet ds = new UsersDataSet();
        //UsersDataSet.Users_GetByUserNameDataTable t;

        //t = user.SelectByUserName(e.Email.Trim());

        //if (t.Count > 0)
        //{
        //    this.ucUsersEditor.RepeatedUsername = true;
        //    return false;
        //}

        //UsersDataSet.UsersRow drUser = ds.Users.NewUsersRow();

        //drUser.UserName = e.Email;
        //drUser.Password = Cryptography.EncryptUnivisitString(e.Password);
        //drUser.Active = true;
        //drUser.Role = e.Role;
        //drUser.Level = e.Level;
        //drUser.GroupID = e.GroupID;
        //drUser.BillerID = e.BillerID;

        //ds.Users.AddUsersRow(drUser);

        //if (!user.UpdateUsers(ds.Users))
        //{ return false; }

        //this.SendEmail(e);

        //t.Dispose();

        //LogManager.WriteStackTrace(new Exception("Éxito al Agregar Nuevo Usuario."));
        //return true;
        //}
        /// <summary>
        /// Customizes how the editor will work.
        /// </summary>
        private void CustomizeEditor()
        {
            this.ucUsersEditor.EditorMode = UsersEditorMode.Add;
            this.ucUsersEditor.AddMode = true;
        }
        /// <summary>
        /// Send a confirmation email to the user who just register.
        /// </summary>
        /// <param name="e">UsersEditorEventArgs e</param>
        /// <returns>Returns true if the email could be sent, otherwise returns false.</returns>
        private bool SendEmail(UsersEditorEventArgs e)
        {
            PACFD.Rules.Mail.MailSender sender = new PACFD.Rules.Mail.MailSender();
            String role = String.Empty;

            try
            {
                String path = this.Context.Request.MapPath("~/Includes/Mail/EmisorAdded.es.xml");
                sender.Message = MailSenderHelper.GetMessageFromXML(path, "002");

                switch (e.Role)
                {
                    case 0: role = "Administrador"; break;
                    case 1: role = "Usuario SAT"; break;
                    case 2: role = "Usuario Avanzado"; break;
                    case 3: role = "Usuario Básico"; break;
                }

                sender.Parameters.Add("lblRole", role);
                sender.Parameters.Add("lblEmail", e.Email);
                sender.Parameters.Add("lblPassword", e.Password);
                sender.Send();
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("[ERROR] " + this.GetType().FullName
                    + " " + System.Reflection.MethodInfo.GetCurrentMethod().Name + ex.Message);
#endif
                LogManager.WriteError(ex);
                return false;
            }

            return true;
        }
    }
}