﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PACFDManager
{
    public partial class PeriodChooser : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            cmbYear.Items.Clear();
            for (int i = 2010; i <= DateTime.Now.Year; i++)
            {
                cmbYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 1; i <= 12; i++)
            {

                cmbMonth.Items.Add(new ListItem(GetMonthName(i), i.ToString()));
            }
            cmbYear.SelectedIndex = cmbYear.Items.IndexOf(cmbYear.Items.FindByValue(DateTime.Now.AddMonths(-1).Year.ToString()));
            cmbMonth.SelectedIndex = cmbMonth.Items.IndexOf(cmbMonth.Items.FindByValue(DateTime.Now.AddMonths(-1).Month.ToString()));

        }
        private string GetMonthName(int month)
        {
            if (month == 1)
                return "Ene";
            else if (month == 2)
                return "Feb";
            else if (month == 3)
                return "Mar";
            else if (month == 4)
                return "Abr";
            else if (month == 5)
                return "May";
            else if (month == 6)
                return "Jun";
            else if (month == 7)
                return "Jul";
            else if (month == 8)
                return "Ago";
            else if (month == 9)
                return "Sep";
            else if (month == 10)
                return "Oct";
            else if (month == 11)
                return "Nov";
            else if (month == 12)
                return "Dic";
            return "";
        }

        public int Year
        {
            get { return int.Parse(cmbYear.SelectedValue); }
            set { this.cmbYear.SelectedValue = value.ToString(); }
        }

        public int Month
        {
            get { return int.Parse(cmbMonth.SelectedValue); }
            set { this.cmbMonth.SelectedValue = value.ToString(); }
        }
    }
}