﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using AjaxControlToolkit;
using PACFDManager.Billings;
using System.Data;

namespace PACFDManager
{
    /// <summary>
    /// Descripción breve de AutoComplete
    /// </summary>
    [WebService(Namespace = "http://www.univisit.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class AutoComplete : System.Web.Services.WebService, System.Web.SessionState.IReadOnlySessionState
    {
        /// <summary>
        /// Get a string array with the billers to search for.
        /// </summary>
        /// <param name="prefixText">Biller name to search.</param>
        /// <returns>Return a strin array of billers.</returns>
        [WebMethod(true)]
        public string[] GetBillersByName(string prefixText)
        {
            PACFD.Rules.Billers rules = new PACFD.Rules.Billers();
            PACFD.DataAccess.BillersDataSet.GetByClientIDAndNameDataTable table;
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            table = rules.SelectByClientIDAndName(Security.Security.CurrentGroupID, prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}\n{1}",
                "Tabla count row:" + table.Count.ToString(),
                "Group ID:" + Security.Security.CurrentGroupID
                    ));
#endif

            foreach (PACFD.DataAccess.BillersDataSet.GetByClientIDAndNameRow row in table)
            {
                list.Add(row.Name);
            }

            table.Dispose();

            return list.ToArray();
        }
        /// <summary>
        /// Get a string array with the cities founded in the data base with a determinate string.
        /// </summary>
        /// <param name="prefixText">City to search.</param>
        /// <returns>Return a strin array of cities.</returns>
        [WebMethod(true)]
        public string[] GetOzCities(string prefixText)
        {
            PACFD.Rules.CountrySelectors select = new PACFD.Rules.CountrySelectors();
            PACFD.DataAccess.CountrySelectorDataSet.Cities_GetByNameDataTable table;
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            table = select.SearchCity(prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}", "Tabla count row:" + table.Count.ToString()));
#endif

            foreach (PACFD.DataAccess.CountrySelectorDataSet.Cities_GetByNameRow row in table)
            {
                list.Add(row.Name);
            }

            table.Dispose();

            return list.ToArray();
        }
        /// <summary>
        /// Get a string array with the founded concepts.
        /// </summary>
        /// <param name="prefixText">Concept to look for.</param>
        /// <returns>Return a string array (string[]).</returns>
        [WebMethod(true)]
        public string[] GetConceptsByBillersIDAndDescription(string prefixText)
        {
            PACFD.Rules.Concepts concep = new PACFD.Rules.Concepts();
            PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionDataTable table;
            List<string> list = new List<string>();


            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            table = concep.SelectByBillerIDAndDescription(Security.Security.CurrentBillerID, prefixText);
            foreach (PACFD.DataAccess.ConceptsDataSet.GetByBillerIDAndDescriptionRow row in table)
            {
                //list.Add(row.Description);
                list.Add(AutoCompleteExtender.CreateAutoCompleteItem(row.Description, row.ConceptID.ToString()));
            }
            table.Dispose();
            return list.ToArray();
        }
        /// <summary>
        /// Get a string array with the founded receptors.
        /// </summary>        
        /// <param name="prefixText">Receptor name to look for.</param>
        /// <returns>Return a string array (string[]).</returns>
        [WebMethod(true)]
        public string[] GetReceptorByName(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Receptors receptor = new PACFD.Rules.Receptors();
            PACFD.DataAccess.ReceptorsDataSet.GetByNameAndBillersIDDataTable table;

            table = receptor.SelectByNameAndBillerID(Security.Security.CurrentBillerID, prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}", "Tabla count row:" + table.Count.ToString()));
#endif

            foreach (PACFD.DataAccess.ReceptorsDataSet.GetByNameAndBillersIDRow t in table.Rows)
                list.Add(t.Name);

            table.Dispose();
            receptor = null;

            return list.ToArray();
        }
        [WebMethod(true)]
        public string[] GetInstitutionsByName(string prefixText)
        {
           List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected || prefixText.Length < 2)
                return list.ToArray();

            DataTable dt = new DataTable();
            PACFD.Rules.Institutions inst = new PACFD.Rules.Institutions(System.Configuration.ConfigurationManager.
    ConnectionStrings["PACFD.DataAccess.Properties.Settings.PACFDConnectionString"].ConnectionString);
            inst.Fill(dt, prefixText);
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            //table = receptor.SelectByNameAndBillerID(Security.Security.CurrentBillerID, prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}", "Tabla count row:" + ds.Tables[0].Rows.Count.ToString()));
#endif

            foreach (DataRow dr in ds.Tables[0].Rows)
                list.Add(dr.ItemArray[2].ToString() + " - " + dr.ItemArray[3].ToString());

            ds.Dispose();
            dt.Dispose();

            return list.ToArray();
        }
        /// <summary>
        /// Get a string array with the founded rfcs.
        /// </summary>        
        /// <param name="prefixText">Receptor rfc to look for.</param>
        /// <returns>Return a string array (string[]).</returns>
        [WebMethod(true)]
        public string[] GetBillersRFCs(string prefixText)
        {
            PACFD.Rules.Billers rules = new PACFD.Rules.Billers();
            PACFD.DataAccess.BillersDataSet.GetRFCsDataTable table;
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            table = rules.GetRFCs(Security.Security.CurrentGroupID, prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}", "Tabla count row:" + table.Count.ToString()));
#endif

            foreach (PACFD.DataAccess.BillersDataSet.GetRFCsRow row in table)
            {
                list.Add(row.RFC);
            }

            table.Dispose();

            return list.ToArray();
        }
        /// <summary>
        /// Get a string array with the founded rfcs.
        /// </summary>        
        /// <param name="prefixText">Receptor rfc to look for.</param>
        /// <returns>Return a string array (string[]).</returns>
        [WebMethod(true)]
        public string[] GetReceptorsRFCs(string prefixText)
        {
            PACFD.Rules.Receptors rules = new PACFD.Rules.Receptors();
            PACFD.DataAccess.ReceptorsDataSet.GetRFCsDataTable table;
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            table = rules.GetRFCs(Security.Security.CurrentBillerID, prefixText);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("{0}", "Tabla count row:" + table.Count.ToString()));
#endif

            foreach (PACFD.DataAccess.ReceptorsDataSet.GetRFCsRow row in table)
            {
                list.Add(row.RFC);
            }

            table.Dispose();

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetConceptsByDescription(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            PACFD.DataAccess.ConceptsDataSet.Concepts_GetBySearchingDataTable ta;

            ta = concept.SelectConceptBySearching(Security.Security.CurrentBillerID,
                         String.Empty, prefixText, String.Empty);

            foreach (PACFD.DataAccess.ConceptsDataSet.Concepts_GetBySearchingRow t in ta.Rows)
                list.Add(t.Description);

            ta.Dispose();
            concept = null;

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetClaveUnidadByName(string prefixText)
        {
            List<string> list = prefixText.FindClaveUnidad();
            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetClaveProdByName(string prefixText)
        {
            List<string> list = prefixText.FindClaveProdServ();
            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetConceptsByCode(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Concepts concept = new PACFD.Rules.Concepts();
            PACFD.DataAccess.ConceptsDataSet.Concepts_GetBySearchingDataTable ta;

            ta = concept.SelectConceptBySearching(Security.Security.CurrentBillerID,
                         String.Empty, String.Empty, prefixText);

            foreach (PACFD.DataAccess.ConceptsDataSet.Concepts_GetBySearchingRow t in ta.Rows)
                list.Add(t.Code);

            ta.Dispose();
            concept = null;

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetClientByEmail(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Users user = new PACFD.Rules.Users();
            PACFD.DataAccess.UsersDataSet.Clients_GetBySearchingDataTable ta;

            ta = user.SelectClientBySearching(prefixText, String.Empty/*Email*/);

            foreach (PACFD.DataAccess.UsersDataSet.Clients_GetBySearchingRow t in ta.Rows)
                list.Add(t.UserName);

            ta.Dispose();
            user = null;

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetClientByName(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Users user = new PACFD.Rules.Users();
            PACFD.DataAccess.UsersDataSet.Clients_GetBySearchingDataTable ta;

            ta = user.SelectClientBySearching(String.Empty, prefixText  /*Name*/);

            foreach (PACFD.DataAccess.UsersDataSet.Clients_GetBySearchingRow t in ta.Rows)
                list.Add(t.Name);

            ta.Dispose();
            user = null;

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetUserByName(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Users user = new PACFD.Rules.Users();
            PACFD.DataAccess.UsersDataSet.Users_GetBySearchingDataTable ta;

            ta = user.SelectUserBySearching(null, prefixText);

            foreach (PACFD.DataAccess.UsersDataSet.Users_GetBySearchingRow t in ta.Rows)
                list.Add(t.UserName);

            ta.Dispose();
            user = null;

            return list.ToArray();
        }

        [WebMethod(true)]
        public string[] GetGroupName(string prefixText)
        {
            List<string> list = new List<string>();

            if (!Security.Security.IsBillerSelected)
                return list.ToArray();

            PACFD.Rules.Groups group = new PACFD.Rules.Groups();
            PACFD.DataAccess.GroupsDataSet.GroupsDataTable ta;

            ta = group.SelectBySearching(prefixText, true);

            foreach (PACFD.DataAccess.GroupsDataSet.GroupsRow t in ta.Rows)
                list.Add(t.Name);

            ta.Dispose();
            group = null;

            return list.ToArray();
        }
    }
}