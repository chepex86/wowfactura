﻿using PACFD.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.WebServices
{
    internal static class Extensions
    {
        public static System.IO.MemoryStream ToMemoryStream(this byte[] value)
        {
            System.IO.MemoryStream m = new System.IO.MemoryStream();

            m.Write(value, 0, value.Length);
            m.Seek(0, System.IO.SeekOrigin.Begin);

            return m;
        }

        public static byte[] ToByteArray(this System.IO.MemoryStream value)
        {
            byte[] b = new byte[value.Length];

            value.Seek(0, System.IO.SeekOrigin.Begin);
            value.Read(b, 0, b.Length);

            return b;
        }

        
    }
}
