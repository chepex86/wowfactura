﻿#region Usings
using PACFD.Common;
using PACFD.Rules;
using System;
using System.Linq;
using System.Text;
using System.Web.Services;
#endregion

namespace PACFD.WebServices
{
    /// <summary>
    /// Access clas for the billing data.
    /// </summary>
    [WebService(Namespace = "http://www.univisit.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class BillingWebService : System.Web.Services.WebService
    {
        /// <summary>
        /// Insert a billing in the system from a xml document.
        /// If the billing class insert the bill but fails on the seal the bill is updated as "precomprobante".
        /// If the client not exist is inserted.
        /// If the concept not exist is inserted.
        /// If the billing class fail inserting the bill, the client or concept(s) remain inserted.
        /// </summary>
        /// <param name="document">XML document with the data to be inserted.</param>
        /// <param name="allowrepeatexternalfolio">If the folio is managed externally and repeated is not allowed, set true else false.</param>
        /// <returns>If succes return a XmlDocument else null.</returns>
        [WebMethod(Description = "<b>Insert</b> a new billing. User name and password must be included in the document."
            + "<br/>If the external folio can be repeated, set true else false.")]
        public string InsertBillingByXMLDocument(System.Xml.XmlDocument document, bool allowrepeatexternalfolio)
        {
            const string token_user = "usuario";
            const string token_billing = "facturacion";
            const string token_name = "nombre";
            const string token_password = "contraseña";
            const string token_warnning = "advertencia";
            const string token_addendum = "addenda";
            int billerid = 0;
            string tmp;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table;
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            System.Xml.XmlNode node, childnode;
            System.Xml.XmlAttribute nodeattribute;
            PACFD.DataAccess.BillingsDataSet dataset = null;
            PACFD.Rules.ElectronicBillingType electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;
            PACFD.Rules.Billers billers;
            System.Drawing.Bitmap bitmapqr;
            System.IO.MemoryStream memorystream;
            bool wascfdiprebilling = false
                , iscfdiinserting = false
                , isconsultingbill = false;
            System.Data.DataSet addendumdataset = null;

            if (document == null)
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                   WebServiceErrorType.DocumentNull.ToDescription(), -1, -1, WebServiceErrorType.DocumentNull, string.Empty)).OuterXml;

            this.WriteTrace(new Exception(string.Format("Insert billing XML document IP: {0} \n\rRequest: {1}\n\r", this.Context.Request.UserHostAddress, document.OuterXml)));

            node = document.SelectSingleNode(token_billing);

            if (node == null)
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeBillingNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeBillingNotFound, string.Empty)).OuterXml;

            node = node.SelectSingleNode(token_user);

            if (node == null)
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeUserNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeUserNotFound, string.Empty)).OuterXml;

            if (node.Attributes[token_name] == null || node.Attributes[token_password] == null)
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeUserAttributeUserOrPasswordNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeUserAttributeUserOrPasswordNotFound, string.Empty)).OuterXml;

            using (table = this.IsUserValid(node.Attributes[token_name].Value, node.Attributes[token_password].Value))
            {
                if (table.Count < 1)
                {
                    LogManager.Write("Insert Billing WebService", "Document parsed, not biller(company) in xml found. PACFD system ok, returning null.", LogFlag.Error);
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = table[0].BillerID;
            }

            billers = new PACFD.Rules.Billers();

            billertable = billers.SelectByID(billerid);

            switch ((PACFD.Rules.ElectronicBillingType)billertable[0].ElectronicBillingType)
            {
                case PACFD.Rules.ElectronicBillingType.CFD:
                case PACFD.Rules.ElectronicBillingType.CFDI:
                case PACFD.Rules.ElectronicBillingType.CBB:
                case PACFD.Rules.ElectronicBillingType.CFD2_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                case PACFD.Rules.ElectronicBillingType.CFDI4_0: 
                    electronictype = (PACFD.Rules.ElectronicBillingType)billertable[0].ElectronicBillingType;
                break;
                //case PACFD.Rules.ElectronicBillingType.Indeterminate:
                default:
                    electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate;
                    break;
            }

            // Valida la fecha de validas para emitir faturas. 
            if (DateTime.ParseExact("2022/07/01", "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now)
            {
                LogManager.Write("Insert Billing WebService", "You can generate invoice with a deprecated version. 4.0 is needed.", LogFlag.Error);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.BillingCfdv2_0AndCfdiv3_0NeedCfdv2_2AndCfdi3_2.ToDescription(), -1, -1
                    , WebServiceErrorType.BillingCfdv2_0AndCfdiv3_0NeedCfdv2_2AndCfdi3_2, string.Empty)).OuterXml;
            }

                switch (electronictype)
            {
                case PACFD.Rules.ElectronicBillingType.CFDI:
                    //case PACFD.Rules.ElectronicBillingType.CFD:

                    if (DateTime.ParseExact("2012/07/01", "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now)
                    {
                        LogManager.Write("Insert Billing WebService", "Trying to facture in CFD 2.0 or CFDI 3.0,  CFDI 3.2 or CFD 2.2 is needed.", LogFlag.Error);
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            WebServiceErrorType.BillingCfdv2_0AndCfdiv3_0NeedCfdv2_2AndCfdi3_2.ToDescription(), -1, -1
                            , WebServiceErrorType.BillingCfdv2_0AndCfdiv3_0NeedCfdv2_2AndCfdi3_2, string.Empty)).OuterXml;
                    }

                    break;

                case PACFD.Rules.ElectronicBillingType.CFD2_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                case PACFD.Rules.ElectronicBillingType.CFDI4_0:

                    using (PACFD.DataAccess.BillersDataSet.FiscalRegimeGetByBillerIDDataTable t = billers.FiscalRegimeTable.GetByBillerID(billerid))
                    {
                        if (t.Count < 1)
                        {
                            LogManager.Write("Insert Billing WebService", "Fiscal regime is not configurated for this biller. CFDI 3.2 higher. CFD 2.2 or higher.", LogFlag.Error);
                            return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.BillingFisicalRegime.ToDescription(), -1, -1, WebServiceErrorType.BillingFisicalRegime, string.Empty)).OuterXml;
                        }
                    }
                    break;
                   
                case PACFD.Rules.ElectronicBillingType.CFD: //do nothing with of validations
                case PACFD.Rules.ElectronicBillingType.CBB:
                default:
                    break;
            }

            if (electronictype == PACFD.Rules.ElectronicBillingType.Indeterminate)
            {
                LogManager.Write("Insert Billing WebService", "ElectronicBillingType not determinated.", LogFlag.Error);

                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.BillerBillingError.ToDescription(), -1, -1, WebServiceErrorType.BillerBillingError, string.Empty)).OuterXml;
            }

            // tiene un tipo valido de certificado peo no tiene activo el PAC 
            if ((   electronictype == PACFD.Rules.ElectronicBillingType.CFDI || 
                    electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_2 || 
                    electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_3 || 
                    electronictype == PACFD.Rules.ElectronicBillingType.CFDI4_0 )
                && !(new PACFD.Rules.PacConfiguration().IsActive(billerid)))
            {
                LogManager.WriteError(new Exception("CFDI 3.3 or higher Insert fail, not pac configurated."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingInsertingPacNotConfigured("El Pac no esta configurado.")).OuterXml;
            }

            //this is the main call to 
            //fill billings dataset
            //and return an exception 
            //or success.
            try
            {
                dataset = billing.GetBillingFromXMLWebService(new PACFD.Rules.Billings.BillingFromXMLWebServiceEventArgs(
                    document, billertable, billerid, electronictype, allowrepeatexternalfolio, true));
            }
            catch (PACFD.Rules.BillingsExceptions.WebServiceCFDIInsertingErrorExternalFolioActiveAsPrebilling ex2)
            {   
                //fallo pero se inserto en la BD ponlo como pre-factura
                iscfdiinserting = true;
                wascfdiprebilling = ex2.IsPrebilling;
                dataset = ex2.Billling;
            }
            catch (PACFD.Rules.BillingsExceptions.WebServiceBillingInsertingErrorExternalFolioActive ex3)
            {
                //fallo pero se inserto en la BD ponlo como pre-factura
                System.Diagnostics.Debug.WriteLine(ex3.Billling.Billings[0].BillingID);
                isconsultingbill = true;
                dataset = ex3.Billling;
            }
            catch (PACFD.Rules.BillingsExceptions.BillingExceptionBase ex1)
            {
                //fallo errror
                LogManager.WriteError(ex1);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    string.Format(ex1.ErrorCode.ToDescription(), ex1.Value), ex1.RowIndex, ex1.ValueIndex, ex1.ErrorCode, ex1.Value)).OuterXml;
            }

            if (!this.BillingInsert_CheckForError(dataset))
            {
                LogManager.WriteStackTrace(new Exception("Insert billing error validating billing"));

                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.SystemError.ToDescription(), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
            }

            //check for addendum tag...
            //bricers used to debbug and 
            //separate linq variable declaration             
            //from the main method block.
            {
                var xn = from x in document.ChildNodes.OfType<System.Xml.XmlNode>()
                         where x.Name == token_billing
                         select x;

                if (xn.Any())
                {
                    xn = from x in xn.ToArray()[0].ChildNodes.OfType<System.Xml.XmlNode>()
                         where x.Name == token_addendum
                         select x;

                    if (xn.Any())
                    {
                        addendumdataset = new System.Data.DataSet();
                        tmp = xn.ToArray()[0].InnerXml;

                        try
                        {
                            addendumdataset.ReadXml(new System.Xml.XmlTextReader(new System.IO.StringReader(tmp)));
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                            return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.BillingAddendumFormatError.ToDescription(), -1, -1, WebServiceErrorType.BillingAddendumFormatError, string.Empty)).OuterXml;
                        }
                    }
                }

                tmp = string.Empty;
            }

            System.Net.ServicePointManager.SecurityProtocol |=  (System.Net.SecurityProtocolType)3072;
            //Aqui hace el Update del DS de la factura  3072 =tls12
            if (!isconsultingbill && !iscfdiinserting && !billing.Update(dataset))
            {
                LogManager.WriteStackTrace(new Exception("Insert biling error tring to update billing"));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.SystemError.ToDescription(), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
            }

            ErrorManager error = new ErrorManager();

            //this is the second main procces
            //it send to seal a billings
            //data set to the pac...
            if (!isconsultingbill && !billing.SealCFD(dataset, ref error))
            {
                if (error.Error.Code == "999")
                {
                    if (electronictype == PACFD.Rules.ElectronicBillingType.CFDI || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_2 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_3 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                    {
                        dataset.Billings[0].PreBilling = !(
                            (dataset.Billings[0].IsSealSatNull() || dataset.Billings[0].Seal.Trim().Length > 0)
                            || (dataset.Billings[0].IsOrignalStringSatNull() || dataset.Billings[0].OriginalString.Trim().Length > 0)
                            || (dataset.Billings[0].IsSealSatNull() || dataset.Billings[0].SealSat.Trim().Length > 0)
                            || (dataset.Billings[0].IsOrignalStringSatNull() || dataset.Billings[0].OrignalStringSat.Trim().Length > 0));
                    }
                    else if (electronictype == PACFD.Rules.ElectronicBillingType.CFD || electronictype == PACFD.Rules.ElectronicBillingType.CFD2_2)
                    {
                        dataset.Billings[0].PreBilling = !(
                            (dataset.Billings[0].IsSealSatNull() || dataset.Billings[0].Seal.Trim().Length > 0)
                            || (dataset.Billings[0].OriginalString.Trim().Length > 0));
                    }
                }
                //else
                //{
                //    dataset.Billings[0].PreBilling = true;
                //    dataset.Billings[0].OriginalString =
                //        dataset.Billings[0].Seal = string.Empty;
                //}

                billing.Update(dataset);
            }

            document = null;
            document = new System.Xml.XmlDocument();
            string s = billing.GetXml(dataset);
            document.LoadXml(s);

            node = null;
            node = document.CreateElement("Addenda"); //parent node addenda

            childnode = document.CreateElement("OriginalString");
            nodeattribute = document.CreateAttribute("value");
            nodeattribute.Value = dataset.Billings[0].OriginalString;
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            childnode = document.CreateElement("EstatusFactura");
            nodeattribute = document.CreateAttribute("value");
            nodeattribute.Value = dataset.Billings[0].Active.ToString();
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            //if error is genetared on insert then add warning
            if (error.HaveError)
            {
                childnode = document.CreateElement(token_warnning);
                nodeattribute = document.CreateAttribute("texto");
                nodeattribute.Value = string.Format("Se creo el comprobante pero hubo un error al sellar, se a actualizado el comprobante como precomprobante: {0}"
                    , error.Error.Message);
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("codigo");
                nodeattribute.Value = error.Error.Code;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
                this.WriteTrace(new Exception(string.Format(
                    "Seal billing document error, actual billing state: {0}. Returnning consult. Message: {1}"
                    , error.Error.Code, error.Error.Message)));
            }

            if (electronictype == PACFD.Rules.ElectronicBillingType.CBB)
            {
                childnode = document.CreateElement("ComprobantePDF");

                using (System.IO.MemoryStream stream = this.GetPDF(dataset.Billings[0].BillingID) as System.IO.MemoryStream)
                {
                    if (stream != null)
                    { childnode.InnerText = Convert.ToBase64String(stream.ToArray()); }
                }

                node.AppendChild(childnode);
            }
            else if (   electronictype == PACFD.Rules.ElectronicBillingType.CFDI || 
                        electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_2 || 
                        electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_3 ||
                        electronictype == PACFD.Rules.ElectronicBillingType.CFDI4_0
                        )
            {
                childnode = document.CreateElement("QrCFDI");

                if (!dataset.Billings[0].IsQrImageNull() && dataset.Billings[0].QrImage.Length > 0)
                {
                    using (bitmapqr = new System.Drawing.Bitmap(dataset.Billings[0].QrImage.ToMemoryStream()))
                    {
                        using (memorystream = new System.IO.MemoryStream())
                        {
                            memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                            bitmapqr.Save(memorystream, System.Drawing.Imaging.ImageFormat.Gif);
                            memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                            childnode.InnerText = Convert.ToBase64String(memorystream.ToByteArray());
                        }
                    }
                    //childnode.InnerText = Convert.ToBase64String(dataset.Billings[0].QrImage);
                }
#if DEBUG
                if (dataset.Billings[0].IsQrImageNull())
                {
                    using (bitmapqr = new System.Drawing.Bitmap(PACFD.DataAccess.BillingsDataSet.GetFakeQr().ToMemoryStream()))
                    {
                        using (memorystream = new System.IO.MemoryStream())
                        {
                            memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                            bitmapqr.Save(memorystream, System.Drawing.Imaging.ImageFormat.Gif);
                            memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                            childnode.InnerText = Convert.ToBase64String(memorystream.ToByteArray());
                        }
                    }
                    //childnode.InnerText = Convert.ToBase64String(PACFD.DataAccess.BillingsDataSet.GetFakeQr());
                }
#endif

                node.AppendChild(childnode);

                childnode = document.CreateElement("OriginalStringSAT");
                nodeattribute = document.CreateAttribute("value");
                nodeattribute.Value = dataset.Billings[0].IsOrignalStringSatNull() ? string.Empty : dataset.Billings[0].OrignalStringSat;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
            }

            childnode = document.CreateElement("FolioExterno");
            nodeattribute = document.CreateAttribute("value");
            nodeattribute.Value = dataset.Billings[0].IsExternalFolioNull() ? string.Empty : dataset.Billings[0].ExternalFolio;
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            if (iscfdiinserting && wascfdiprebilling)
            {
                childnode = document.CreateElement(token_warnning);
                nodeattribute = document.CreateAttribute("texto");
                nodeattribute.Value = PACFD.Common.WebServiceErrorType.CFDIInsertingErrorExternalFolioActiveAsPrebilling.ToDescription();
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("codigo");
                nodeattribute.Value = ((int)PACFD.Common.WebServiceErrorType.CFDIInsertingErrorExternalFolioActiveAsPrebilling).ToString();
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
                this.WriteTrace(new Exception(string.Format(
                    "Insert CFDI billing XML document error was prebilling, actual billing state: {0}. Returnning consult."
                    , dataset.Billings[0].PreBilling
                    )));
            }
            else if (iscfdiinserting)
            {
                childnode = document.CreateElement(token_warnning);
                nodeattribute = document.CreateAttribute("texto");
                nodeattribute.Value = PACFD.Common.WebServiceErrorType.BillingInsertingErrorExternalFolioActive.ToDescription();
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("codigo");
                nodeattribute.Value = ((int)PACFD.Common.WebServiceErrorType.BillingInsertingErrorExternalFolioActive).ToString();
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
                this.WriteTrace(new Exception(string.Format(
                    "Insert CFDI billing XML document error was prebilling, actual billing state: {0}. Returnning consult."
                    , dataset.Billings[0].PreBilling
                    )));
            }
            else if (isconsultingbill)
            {
                childnode = document.CreateElement(token_warnning);
                nodeattribute = document.CreateAttribute("texto");
                nodeattribute.Value = PACFD.Common.WebServiceErrorType.BillingInsertingErrorExternalFolioActive.ToDescription();
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("codigo");
                nodeattribute.Value = ((int)PACFD.Common.WebServiceErrorType.BillingInsertingErrorExternalFolioActive).ToString();
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
                this.WriteTrace(new Exception("Insert CFDI billing XML document. Returnning consult."));
            }

            childnode = null;
            childnode = node;
            node = document.ChildNodes[1];

            node.AppendChild(childnode);

            try
            {
                node = document.CreateElement("InfoAdicional");

                childnode = document.CreateElement("Facturacion");
                nodeattribute = document.CreateAttribute("FormaPago");
                nodeattribute.Value = Helperss.GetPaymentFormByCode(dataset.Billings[0].PaymentForm);
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("MetodoPago");
                nodeattribute.Value = Helperss.GetPaymentMethodByCode(dataset.Billings[0].PaymentMethod);
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("LugarExpedicion");
                nodeattribute.Value = Helperss.GetPlaceDispatch(dataset.Billings[0].PlaceDispatch, dataset.Billings[0].BillerID);
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);

                childnode = document.CreateElement("Conceptos");
                foreach (var detail in dataset.BillingsDetails)
                {
                    var otherchildnode = document.CreateElement("Concepto");

                    nodeattribute = document.CreateAttribute("ClavProdServ");
                    nodeattribute.Value = Helperss.GetClaveProdServCode(detail.IsClaveProdServNull() ? "" : detail.ClaveProdServ);
                    otherchildnode.Attributes.Append(nodeattribute);
                    childnode.AppendChild(otherchildnode);

                    nodeattribute = document.CreateAttribute("Impuesto");
                    nodeattribute.Value = Helperss.GetImpuesto(detail.IsTaxTypeNull() ? "": detail.TaxType);
                    otherchildnode.Attributes.Append(nodeattribute);
                    childnode.AppendChild(otherchildnode);

                }
                node.AppendChild(childnode);

                childnode = document.CreateElement("Emisor");
                nodeattribute = document.CreateAttribute("RegimenFiscal");
                nodeattribute.Value = Helperss.GetTaxSystemByCode(dataset.BillingsBillers[0].IsTaxSystemNull() ? "" : dataset.BillingsBillers[0].TaxSystem);
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Calle");
                nodeattribute.Value = dataset.BillingsBillers[0].BillerAddress;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("CodigoPostal");
                nodeattribute.Value = dataset.BillingsBillers[0].Zipcode;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Colonia");
                nodeattribute.Value = dataset.BillingsBillers[0].Colony;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Estado");
                nodeattribute.Value = dataset.BillingsBillers[0].State;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Localidad");
                nodeattribute.Value = dataset.BillingsBillers[0].Location;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Municipio");
                nodeattribute.Value = dataset.BillingsBillers[0].IsMunicipalityNull() ? "" : dataset.BillingsBillers[0].Municipality;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("NoExterior");
                nodeattribute.Value = dataset.BillingsBillers[0].ExternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("NoInterior");
                nodeattribute.Value = dataset.BillingsBillers[0].InternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Pais");
                nodeattribute.Value = dataset.BillingsBillers[0].Country;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);

                childnode = document.CreateElement("Receptor");
                nodeattribute = document.CreateAttribute("UsoCFDI");
                nodeattribute.Value = Helperss.GetUsoCFDIByCode(dataset.BillingsReceptors[0].IsTaxSystemNull() ? "" : dataset.BillingsReceptors[0].TaxSystem);
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Calle");
                nodeattribute.Value = dataset.BillingsReceptors[0].ReceptorAddress;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("CodigoPostal");
                nodeattribute.Value = dataset.BillingsReceptors[0].Zipcode;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Colonia");
                nodeattribute.Value = dataset.BillingsReceptors[0].Colony;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Estado");
                nodeattribute.Value = dataset.BillingsReceptors[0].State;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Localidad");
                nodeattribute.Value = dataset.BillingsReceptors[0].Location;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Municipio");
                nodeattribute.Value = dataset.BillingsReceptors[0].IsMunicipalityNull() ? "" : dataset.BillingsReceptors[0].Municipality;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("NoExterior");
                nodeattribute.Value = dataset.BillingsReceptors[0].ExternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("NoInterior");
                nodeattribute.Value = dataset.BillingsReceptors[0].InternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = document.CreateAttribute("Pais");
                nodeattribute.Value = dataset.BillingsReceptors[0].Country;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);

                if (dataset.BillingsIssued.Count > 0)
                {
                    childnode = document.CreateElement("Sucursal");
                    nodeattribute = document.CreateAttribute("Calle");
                    nodeattribute.Value = dataset.BillingsIssued[0].Address;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("CodigoPostal");
                    nodeattribute.Value = dataset.BillingsIssued[0].Zipcode;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("Colonia");
                    nodeattribute.Value = dataset.BillingsIssued[0].Colony;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("Estado");
                    nodeattribute.Value = dataset.BillingsIssued[0].State;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("Localidad");
                    nodeattribute.Value = dataset.BillingsIssued[0].Location;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("Municipio");
                    nodeattribute.Value = dataset.BillingsIssued[0].Municipality;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("NoExterior");
                    nodeattribute.Value = dataset.BillingsIssued[0].ExternalNumber;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("NoInterior");
                    nodeattribute.Value = dataset.BillingsIssued[0].InternalNumber;
                    childnode.Attributes.Append(nodeattribute);
                    nodeattribute = document.CreateAttribute("Pais");
                    nodeattribute.Value = dataset.BillingsIssued[0].Country;
                    childnode.Attributes.Append(nodeattribute);
                    node.AppendChild(childnode);
                }

                childnode = null;
                childnode = node;
                node = document.ChildNodes[1];

                node.AppendChild(childnode);
            } catch(Exception ex)
            {

            }

            //delete xmls from node tag, need add namespace (not time for that right now)
            tmp = document.OuterXml;
            tmp = tmp.Replace("<Addenda xmlns=\"\">", "<Addenda>");

            this.WriteTrace(new Exception(string.Format("Insert billing XML document IP: {0} \nResponse: {1}", this.Context.Request.UserHostAddress, tmp)));

            //add addendum to billing if exist
            if (addendumdataset != null)
            {
                using (PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable bat = new PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumDataTable())
                {
                    PACFD.DataAccess.BillingsAddendumDataSet.BillingsAddendumRow rowaddendum = bat.NewBillingsAddendumRow();
                    rowaddendum.BillingID = dataset.Billings[0].BillingID;
                    rowaddendum.Xml = addendumdataset.GetXml();
                    rowaddendum.SetAddendumIDNull();
                    bat.AddBillingsAddendumRow(rowaddendum);
                    bat.AcceptChanges();
                    rowaddendum.SetAdded();
                    (new Rules.BillingsAddendum()).Update(bat);
                }
            }

            return tmp;
        }
        /// <summary>
        /// Insert a billing in the system from a xml document.
        /// If the billing class insert the bill but fails on the seal the bill is updated as "precomprobante".
        /// If the client not exist is inserted.
        /// If the concept not exist is inserted.
        /// If the billing class fail inserting the bill, the client or concept(s) remain inserted.
        /// </summary>
        /// <param name="sdocument">A string with the XML to be inserted.</param>
        /// <param name="allowrepeatexternalfolio">If the folio is managed externally and repeated is not allowed, set true else false.</param>
        /// <returns>If succes return a XmlDocument else null.</returns>
        [WebMethod(Description = "<b>Insert</b> a new billing. User name and password must be included in the document."
            + "<br/>If the folio can be repeated, set true else false.")]
        public string InsertBillingByString(string sdocument, bool allowrepeatexternalfolio)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            //Chicanada para pruebas...
            sdocument = sdocument.Replace("&lt;", "<").Replace("&gt;", ">");

            sdocument = (new XMLHtmlCodesParser()).ParseToHtmlNumber(sdocument); //parse &

            try
            {
                doc.LoadXml(sdocument);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.DocumentErrorFormat.ToDescription(), -1, -1, WebServiceErrorType.DocumentErrorFormat, string.Empty)).OuterXml;
            }

            return this.InsertBillingByXMLDocument(doc, allowrepeatexternalfolio);
        }
        /// <summary>
        /// Cancel a billing.
        /// </summary>
        /// <param name="user">Username.</param>
        /// <param name="password">Password. Not encrypted string.</param>
        /// <param name="serie">Serie to look for.</param>
        /// <param name="folio">Folio to look for.</param>
        /// <returns>Return a System.Xml.XmlDocument with the result.</returns>
        [WebMethod(Description = "<b>Cancel</b> an existing billing. User name and password are needed.")]
        public string CancelBilling(string user, string password, string serie, string folio)
        {
            StringBuilder sb;
            System.Xml.XmlDocument doc;
            PACFD.Rules.Billings bill;
            PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable billtable;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table;
            bool found = false;
            int billerid;
            PACFD.Rules.ElectronicBillingType electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate;

            this.WriteTrace(new Exception(string.Format("Cancel billing IP: {0} \nRequest: {1}-{2}", this.Context.Request.UserHostAddress, serie, folio)));

            if (string.IsNullOrEmpty(folio))
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            WebServiceErrorType.FolioIsNullOrEmpty.ToDescription(), -1, -1, WebServiceErrorType.FolioIsNullOrEmpty, string.Empty)).OuterXml;

            using (table = this.IsUserValid(user, password))
            {
                if (table.Count < 1)
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;

                billerid = table[0].BillerID;
            }

            bill = new PACFD.Rules.Billings();

            using (billtable = bill.Search(PACFD.Rules.Billings.SearchPetitionFromFlag.WebService, billerid, null, serie, folio
                , (DateTime?)null, (bool?)null, (bool?)null, null, null, null, null, null))
            {
                if (billtable.Count < 1)
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;

                billerid = -1;

                foreach (PACFD.DataAccess.BillingsDataSet.BillingsSearchRow row in billtable)
                {
                    billerid++;

                    if ((row.Serial.Trim() == serie.Trim()) && row.Folio == folio)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                if (!billtable[billerid].Active)
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.BillingAlreadyCanceled, string.Empty)).OuterXml;

                try
                {

                    if (!bill.SetActive(billtable[billerid].BillingID, false))
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                string.Format(WebServiceErrorType.SystemError.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
                }
                catch (PACFD.Rules.BillingsExceptions.BillingCancelError ex1)
                {
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(ex1.Message + ", code: " + ex1.PacError.Error.Code, serie, folio), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
                }

                switch (billtable[billerid].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                    case 2: electronicbillingtype = (PACFD.Rules.ElectronicBillingType)billtable[billerid].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }
            }

            sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append(string.Empty);
            sb.AppendLine("<facturacion>");
            sb.AppendLine(string.Format("<factura texto=\"Exito al cancelar factura. serie='{0}', folio='{1}' \" />", serie, folio));

            //if (electronicbillingtype == PACFD.Rules.ElectronicBillingType.CFDI || electronicbillingtype == PACFD.Rules.ElectronicBillingType.CBB)
            //    sb.AppendLine(string.Format("<alerta texto=\"El comprobante fué cancelado solo en el sistema de wowfactura, para que sea cancelado ante el SAT es necesario entrar al sistema del SAT y cancelarlo.\" />"));

            sb.AppendLine("</facturacion>");

            doc = new System.Xml.XmlDocument();
            doc.LoadXml(sb.ToString());

            this.WriteTrace(new Exception(string.Format("Cancel billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));

            return doc.OuterXml;
        }
        /// <summary>
        /// Cancel a billing.
        /// </summary>
        /// <param name="user">Username.</param>
        /// <param name="password">Password. Not encrypted string.</param>
        /// <param name="externalfolio">External folio to look for.</param>
        /// <returns>Return a System.Xml.XmlDocument with the result.</returns>
        [WebMethod(Description = "<b>Cancel</b> an existing billing by <b>external folio</b>. User name and password are needed.")]
        public string CancelBillingByExternalFolio(string user, string password, string externalfolio)
        {
            StringBuilder sb;
            System.Xml.XmlDocument doc;
            PACFD.Rules.Billings bill;
            bool found = false;
            int billerid
                , billingid = 0
                ;
            PACFD.Rules.ElectronicBillingType electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate;

            this.WriteTrace(new Exception(string.Format("Cancel billing by external folio IP: {0} \nRequest: {1}", this.Context.Request.UserHostAddress, externalfolio)));

            if (string.IsNullOrEmpty(externalfolio))
            {
                LogManager.WriteError(new Exception(string.Format("Cancel billing error, billing not found external folio: {0}.", externalfolio)));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            WebServiceErrorType.FolioIsNullOrEmpty.ToDescription(), -1, -1, WebServiceErrorType.FolioIsNullOrEmpty, string.Empty)).OuterXml;
            }

            using (PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table = this.IsUserValid(user, password))
            {
                if (table.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Cancel billing error, billing not found external folio: {0}.", externalfolio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = table[0].BillerID;
            }

            bill = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable billtable = bill.SelectByExternalFolio(billerid, externalfolio))
            {
                if (billtable.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Cancel billing error, billing not found external folio: {0}.", externalfolio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                billerid = -1;

                foreach (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioRow row in billtable)
                {
                    billerid++;

                    if (!row.IsExternalFolioNull() && row.ExternalFolio.Trim() == externalfolio)
                    {
                        found = true;
                        billingid = billtable[billerid].BillingID;
                        break;
                    }
                }

                if (found && !billtable[billerid].Active)
                {
                    LogManager.WriteError(new Exception(string.Format("Cancel billing error, billing is not active external folio: {0}.", externalfolio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingAlreadyCanceled, string.Empty)).OuterXml;
                }

                switch (billtable[billerid].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                    case 2: electronicbillingtype = (PACFD.Rules.ElectronicBillingType)billtable[billerid].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }
            }

            if (!found)
            {
                LogManager.WriteError(new Exception(string.Format("Cancel billing error, billing not found external folio: {0}.", externalfolio)));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
            }

            try
            {
                if (!bill.SetActive(billingid, false))
                {
                    LogManager.WriteError(new Exception(string.Format("Cancel billing error, billingcan't be de-active external folio: {0}.", externalfolio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.SystemError.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
                }
            }
            catch (PACFD.Rules.BillingsExceptions.BillingCancelError ex1)
            {
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    string.Format(ex1.Message + ", code: " + ex1.PacError.Error.Code, string.Empty, externalfolio), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
            }


            sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append(string.Empty);
            sb.AppendLine("<facturacion>");
            sb.Append(string.Format("<factura texto=\"Exito al cancelar factura. serie='{0}', folio='{1}' \" />", string.Empty, externalfolio));

            //if (electronicbillingtype == PACFD.Rules.ElectronicBillingType.CFDI || electronicbillingtype == PACFD.Rules.ElectronicBillingType.CBB)
            //    sb.AppendLine(string.Format("<alerta texto=\"El comprobante fué cancelado solo en el sistema de wowfactura, para que sea cancelado ante el SAT es necesario entrar al sistema del SAT y cancelarlo.\" />"));

            sb.AppendLine("</facturacion>");

            doc = new System.Xml.XmlDocument();
            doc.LoadXml(sb.ToString());

            this.WriteTrace(new Exception(string.Format("Cancel billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));

            return doc.OuterXml;
        }
        /// <summary>
        /// Get a billing with the SAT format.
        /// </summary>
        /// <param name="user">Username.</param>
        /// <param name="password">Password. Not encrypted string.</param>
        /// <param name="serie">Serie to look for.</param>
        /// <param name="folio">Folio to look for.</param>
        /// <returns>Return a System.Xml.XmlDocument with the result.</returns>
        [WebMethod(Description = "<b>Consult</b> an existing billing. User name and password are needed.")]
        public string GetBilling(string user, string password, string serie, string folio)
        {
            string sxml;
            System.Xml.XmlNode node, childnode;
            System.Xml.XmlAttribute nodeattribute;
            System.Xml.XmlDocument doc;
            PACFD.Rules.Billings bill;
            int billerid
                , billingid
                ;
            bool found = false;
            PACFD.Rules.ElectronicBillingType electronictype;
            System.IO.MemoryStream memorystream;
            System.Drawing.Bitmap bitmap;
            ErrorManager error;

            this.WriteTrace(new Exception(string.Format("Get billing IP: {0} \nRequest: {1}-{2}", this.Context.Request.UserHostAddress, serie, folio)));

            using (PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table = this.IsUserValid(user, password))
            {
                if (table.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Get billing error, billing not found. serial: {0}, folio: {1}.", serie, folio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                    //return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase("Usuario o Contraseña no valido(a)", -1, -1, 1));
                }

                billerid = table[0].BillerID;
            }

            bill = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable billsearchtable =
                bill.Search(PACFD.Rules.Billings.SearchPetitionFromFlag.WebService, billerid, null, serie, folio
                , (DateTime?)null, (bool?)null, (bool?)null, null, (int?)null, null, null, null))
            {
                if (billsearchtable.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Get billing error, billing not found. serial: {0}, folio: {1}.", serie, folio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;

                }

                billerid = -1;

                foreach (PACFD.DataAccess.BillingsDataSet.BillingsSearchRow row in billsearchtable)
                {
                    billerid++;

                    if ((row.Serial.Trim() == serie.Trim()) && row.Folio == folio)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    LogManager.WriteError(new Exception(string.Format("Get billing error, billing not found. serial: {0}, folio: {1}.", serie, folio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;
                }

                billingid = billsearchtable[billerid].BillingID;
            }
            DataAccess.BillingsDataSet billingfull;
            using (billingfull = bill.GetFullBilling(billingid))
            {
                if (billingfull.Billings.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Get billing error, billing not found. serial: {0}, folio: {1}.", serie, folio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), serie, folio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;
                }

                ///
                ///TODO:si se envia a retimbrado volver a consultar xml 
                ///
                sxml = bill.GetXml(billingfull);

                doc = new System.Xml.XmlDocument();
                doc.LoadXml(sxml);

                electronictype = (PACFD.Rules.ElectronicBillingType)billingfull.Billings[0].ElectronicBillingType;

                node = null;
                node = doc.CreateElement("Addenda");
                childnode = doc.CreateElement("OriginalString");
                nodeattribute = doc.CreateAttribute("value");
                nodeattribute.Value = billingfull.Billings[0].OriginalString;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);

                if (electronictype == PACFD.Rules.ElectronicBillingType.CBB)
                {
                    childnode = doc.CreateElement("ComprobantePDF");

                    using (System.IO.MemoryStream stream = this.GetPDF(billingfull.Billings[0].BillingID) as System.IO.MemoryStream)
                        if (stream != null)
                            childnode.InnerText = Convert.ToBase64String(stream.ToArray());

                    node.AppendChild(childnode);
                }
                else if (electronictype == PACFD.Rules.ElectronicBillingType.CFDI || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_2 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_3 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                {
                    childnode = doc.CreateElement("QrCFDI");

                    if (!billingfull.Billings[0].IsQrImageNull() && billingfull.Billings[0].QrImage.Length > 0)
                    {
                        using (bitmap = new System.Drawing.Bitmap(billingfull.Billings[0].QrImage.ToMemoryStream()))
                        {
                            using (memorystream = new System.IO.MemoryStream())
                            {
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                bitmap.Save(memorystream, System.Drawing.Imaging.ImageFormat.Gif);
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                //bitmap.Save("c:\\Aaa", System.Drawing.Imaging.ImageFormat.Gif);
                                childnode.InnerText = Convert.ToBase64String(memorystream.ToByteArray());
                            }
                        }
                        //childnode.InnerText = Convert.ToBase64String(billingfull.Billings[0].QrImage);
                    }

                    if (billingfull.Billings[0].IsOrignalStringSatNull())
                    {
                        this.WriteTrace(new Exception(string.Format("CFDI is not Sealed, {0} on consulting: {1}-{2}", this.Context.Request.UserHostAddress, serie, folio)));
                        error = new ErrorManager();

                        if (!(new Rules.Billings()).SealCFD(billingfull, ref error))
                        {
                            LogManager.WriteError(new Exception(string.Format("CFDI can't be Sealed, {0} on consulting: {1}-{2}, message: {3} code: {4}"
                                , this.Context.Request.UserHostAddress, serie, folio, error.Error.Message, error.Error.Code)));
                        }
                    }

#if DEBUG
                    if (billingfull.Billings[0].IsQrImageNull())
                    {
                        using (bitmap = new System.Drawing.Bitmap(PACFD.DataAccess.BillingsDataSet.GetFakeQr().ToMemoryStream()))
                        {
                            using (memorystream = new System.IO.MemoryStream())
                            {
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                bitmap.Save(memorystream, System.Drawing.Imaging.ImageFormat.Gif);
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                //bitmap.Save("c:\\Aaa", System.Drawing.Imaging.ImageFormat.Gif);
                                childnode.InnerText = Convert.ToBase64String(memorystream.ToByteArray());
                            }
                        }
                        //childnode.InnerText = Convert.ToBase64String(PACFD.DataAccess.BillingsDataSet.GetFakeQr());
                    }
#endif

                    node.AppendChild(childnode);

                    childnode = doc.CreateElement("OriginalStringSAT");
                    nodeattribute = doc.CreateAttribute("value");
                    nodeattribute.Value = billingfull.Billings[0].IsOrignalStringSatNull() ? string.Empty : billingfull.Billings[0].OrignalStringSat;
                    childnode.Attributes.Append(nodeattribute);
                    node.AppendChild(childnode);

                }


                childnode = doc.CreateElement("FolioExterno");
                nodeattribute = doc.CreateAttribute("value");
                nodeattribute.Value = billingfull.Billings[0].IsExternalFolioNull() ? string.Empty : billingfull.Billings[0].ExternalFolio;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
            }

            childnode = null;
            childnode = node;
            node = doc.ChildNodes[1];

            node.AppendChild(childnode);

            node = doc.CreateElement("InfoAdicional");

            childnode = doc.CreateElement("Facturacion");
            nodeattribute = doc.CreateAttribute("FormaPago");
            nodeattribute.Value = Helperss.GetPaymentFormByCode(billingfull.Billings[0].PaymentForm);
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("MetodoPago");
            nodeattribute.Value = Helperss.GetPaymentMethodByCode(billingfull.Billings[0].PaymentMethod);
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("LugarExpedicion");
            nodeattribute.Value = Helperss.GetPlaceDispatch(billingfull.Billings[0].PlaceDispatch, billingfull.Billings[0].BillerID);
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            childnode = doc.CreateElement("Conceptos");
            foreach(var detail in billingfull.BillingsDetails)
            {
                var otherchildnode = doc.CreateElement("Concepto");

                nodeattribute = doc.CreateAttribute("ClavProdServ");
                nodeattribute.Value = Helperss.GetClaveProdServCode(detail.IsClaveProdServNull() ? "" : detail.ClaveProdServ);
                otherchildnode.Attributes.Append(nodeattribute);
                childnode.AppendChild(otherchildnode);
                
                nodeattribute = doc.CreateAttribute("Impuesto");
                nodeattribute.Value = Helperss.GetImpuesto(detail.IsTaxTypeNull() ? "" : detail.TaxType);
                otherchildnode.Attributes.Append(nodeattribute);
                childnode.AppendChild(otherchildnode);

            }
            node.AppendChild(childnode);

            childnode = doc.CreateElement("Emisor");
            nodeattribute = doc.CreateAttribute("RegimenFiscal");
            nodeattribute.Value = Helperss.GetTaxSystemByCode(billingfull.BillingsBillers[0].IsTaxSystemNull() ? "" : billingfull.BillingsBillers[0].TaxSystem);
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Calle");
            nodeattribute.Value = billingfull.BillingsBillers[0].BillerAddress;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("CodigoPostal");
            nodeattribute.Value = billingfull.BillingsBillers[0].Zipcode;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Colonia");
            nodeattribute.Value = billingfull.BillingsBillers[0].Colony;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Estado");
            nodeattribute.Value = billingfull.BillingsBillers[0].State;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Localidad");
            nodeattribute.Value = billingfull.BillingsBillers[0].Location;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Municipio");
            nodeattribute.Value = billingfull.BillingsBillers[0].IsMunicipalityNull() ? "" : billingfull.BillingsBillers[0].Municipality;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("NoExterior");
            nodeattribute.Value = billingfull.BillingsBillers[0].ExternalNumber;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("NoInterior");
            nodeattribute.Value = billingfull.BillingsBillers[0].InternalNumber;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Pais");
            nodeattribute.Value = billingfull.BillingsBillers[0].Country;
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            childnode = doc.CreateElement("Receptor");
            nodeattribute = doc.CreateAttribute("UsoCFDI");
            nodeattribute.Value = Helperss.GetUsoCFDIByCode(billingfull.BillingsReceptors[0].IsTaxSystemNull() ? "" : billingfull.BillingsReceptors[0].TaxSystem);
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Calle");
            nodeattribute.Value = billingfull.BillingsReceptors[0].ReceptorAddress;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("CodigoPostal");
            nodeattribute.Value = billingfull.BillingsReceptors[0].Zipcode;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Colonia");
            nodeattribute.Value = billingfull.BillingsReceptors[0].Colony;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Estado");
            nodeattribute.Value = billingfull.BillingsReceptors[0].State;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Localidad");
            nodeattribute.Value = billingfull.BillingsReceptors[0].Location;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Municipio");
            nodeattribute.Value = billingfull.BillingsReceptors[0].IsMunicipalityNull() ? "" : billingfull.BillingsReceptors[0].Municipality;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("NoExterior");
            nodeattribute.Value = billingfull.BillingsReceptors[0].ExternalNumber;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("NoInterior");
            nodeattribute.Value = billingfull.BillingsReceptors[0].InternalNumber;
            childnode.Attributes.Append(nodeattribute);
            nodeattribute = doc.CreateAttribute("Pais");
            nodeattribute.Value = billingfull.BillingsReceptors[0].Country;
            childnode.Attributes.Append(nodeattribute);
            node.AppendChild(childnode);

            if (billingfull.BillingsIssued.Count > 0)
            {
                childnode = doc.CreateElement("Sucursal");
                nodeattribute = doc.CreateAttribute("Calle");
                nodeattribute.Value = billingfull.BillingsIssued[0].Address;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("CodigoPostal");
                nodeattribute.Value = billingfull.BillingsIssued[0].Zipcode;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("Colonia");
                nodeattribute.Value = billingfull.BillingsIssued[0].Colony;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("Estado");
                nodeattribute.Value = billingfull.BillingsIssued[0].State;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("Localidad");
                nodeattribute.Value = billingfull.BillingsIssued[0].Location;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("Municipio");
                nodeattribute.Value = billingfull.BillingsIssued[0].Municipality;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("NoExterior");
                nodeattribute.Value = billingfull.BillingsIssued[0].ExternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("NoInterior");
                nodeattribute.Value = billingfull.BillingsIssued[0].InternalNumber;
                childnode.Attributes.Append(nodeattribute);
                nodeattribute = doc.CreateAttribute("Pais");
                nodeattribute.Value = billingfull.BillingsIssued[0].Country;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
            }
            childnode = null;
            childnode = node;
            node = doc.ChildNodes[1];

            node.AppendChild(childnode);

            sxml = doc.OuterXml;
            doc = null;
            node = null;
            childnode = null;
            sxml = sxml.Replace("<Addenda xmlns=\"\">", "<Addenda>");

            this.WriteTrace(new Exception(string.Format("Get billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, sxml)));

            return sxml;
        }
        /// <summary>
        /// Get a billing with the SAT format.
        /// </summary>
        /// <param name="user">Username.</param>
        /// <param name="password">Password. Not encrypted string.</param>
        /// <param name="externalfolio">External folio to look for.</param>
        /// <returns>Return a System.Xml.XmlDocument with the result.</returns>
        [WebMethod(Description = "<b>Consult</b> an existing billing by <b>external folio</b>. User name and password are needed.")]
        public string GetBillingByExternalFolio(string user, string password, string externalfolio)
        {
            string sxml;
            System.Xml.XmlNode node, childnode;
            System.Xml.XmlAttribute nodeattribute;
            System.Xml.XmlDocument doc;
            PACFD.Rules.Billings bill;
            int billerid, billingid;
            bool found = false;
            PACFD.Rules.ElectronicBillingType electronictype;
            System.Drawing.Bitmap bitmap;
            System.IO.MemoryStream memorystream;
            ErrorManager error;

            this.WriteTrace(new Exception(string.Format("Get billing by external folio IP: {0} \nRequest: {1}", this.Context.Request.UserHostAddress, externalfolio)));

            externalfolio = externalfolio.Trim();

            using (PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table = this.IsUserValid(user, password))
            {
                if (table.Count < 1)
                {
                    LogManager.WriteError(new Exception("Get billing error, User not found: " + user));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = table[0].BillerID;
            }

            bill = new PACFD.Rules.Billings();

            using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable billsearchtable = bill.SelectByExternalFolio(billerid, externalfolio))
            {
                if (billsearchtable.Count < 1)
                {
                    LogManager.WriteError(new Exception(string.Format("Get billing error, billing not found, external foilio: {0}", externalfolio)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;
                }

                billerid = -1;

                foreach (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioRow row in billsearchtable)
                {
                    billerid++;

                    if (!row.IsExternalFolioNull() && row.ExternalFolio.Trim() == externalfolio)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    LogManager.WriteError(new Exception("Get billing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;
                }

                billingid = billsearchtable[billerid].BillingID;
            }

            using (DataAccess.BillingsDataSet billingfull = bill.GetFullBilling(billingid))
            {
                if (billingfull.Billings.Count < 1)
                {
                    LogManager.WriteError(new Exception("Get billing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(string.Format(
                        WebServiceErrorType.BillingNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingNotFound, string.Empty)).OuterXml;
                }

                sxml = bill.GetXml(billingfull);

                doc = new System.Xml.XmlDocument();
                doc.LoadXml(sxml);

                electronictype = (PACFD.Rules.ElectronicBillingType)billingfull.Billings[0].ElectronicBillingType;

                node = null;
                node = doc.CreateElement("Addenda");
                childnode = doc.CreateElement("OriginalString");
                nodeattribute = doc.CreateAttribute("value");
                nodeattribute.Value = billingfull.Billings[0].OriginalString;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);

                if (electronictype == PACFD.Rules.ElectronicBillingType.CBB)
                {
                    childnode = doc.CreateElement("ComprobantePDF");

                    using (System.IO.MemoryStream stream = this.GetPDF(billingfull.Billings[0].BillingID) as System.IO.MemoryStream)
                    {
                        if (stream != null)
                        { childnode.InnerText = Convert.ToBase64String(stream.ToArray()); }

                        node.AppendChild(childnode);
                    }
                }
                else if (electronictype == PACFD.Rules.ElectronicBillingType.CFDI || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_2 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI3_3 || electronictype == PACFD.Rules.ElectronicBillingType.CFDI4_0)
                {
                    childnode = doc.CreateElement("QrCFDI");

                    if (!billingfull.Billings[0].IsQrImageNull() && billingfull.Billings[0].QrImage.Length > 0)
                    {
                        using (bitmap = new System.Drawing.Bitmap(billingfull.Billings[0].QrImage.ToMemoryStream()))
                        {
                            using (memorystream = new System.IO.MemoryStream())
                            {
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                bitmap.Save(memorystream, System.Drawing.Imaging.ImageFormat.Gif);
                                memorystream.Seek(0, System.IO.SeekOrigin.Begin);
                                childnode.InnerText = Convert.ToBase64String(memorystream.ToByteArray());
                            }
                        }
                        childnode.InnerText = Convert.ToBase64String(billingfull.Billings[0].QrImage);
                    }

                    if (billingfull.Billings[0].IsOrignalStringSatNull())
                    {
                        this.WriteTrace(new Exception(string.Format("CFDI is not Sealed, {0} on consulting: {1}", this.Context.Request.UserHostAddress, externalfolio)));
                        error = new ErrorManager();

                        if (!(new Rules.Billings()).SealCFD(billingfull, ref error))
                        {
                            LogManager.WriteError(new Exception(string.Format("CFDI can't be Sealed, {0} on consulting: {1}, message: {2} code: {3}"
                                , this.Context.Request.UserHostAddress, externalfolio, error.Error.Message, error.Error.Code)));
                        }
                    }

                    node.AppendChild(childnode);
                }

                childnode = doc.CreateElement("FolioExterno");
                nodeattribute = doc.CreateAttribute("value");
                nodeattribute.Value = billingfull.Billings[0].IsExternalFolioNull() ? string.Empty : billingfull.Billings[0].ExternalFolio;
                childnode.Attributes.Append(nodeattribute);
                node.AppendChild(childnode);
            }

            childnode = null;
            childnode = node;
            node = doc.ChildNodes[1];

            node.AppendChild(childnode);

            sxml = doc.OuterXml;
            doc = null;
            node = null;
            childnode = null;
            nodeattribute = null;
            sxml = sxml.Replace("<Addenda xmlns=\"\">", "<Addenda>");

            this.WriteTrace(new Exception(string.Format("Get billing by external folio IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, sxml)));

            return sxml;
        }
        /// <summary>
        /// Seal a billing by folio and/or serial.
        /// </summary>
        /// <param name="user">User name.</param>
        /// <param name="password">User password.</param>
        /// <param name="serial">Serie used to identify the billing. This can be empty.</param>
        /// <param name="folio">Folio used to identify the billing.</param>
        /// <returns>If success return a xml with the seal result else return a xml with the error.</returns>
        [WebMethod(Description = "<b>Seal</b> an existing billing. User name and password are needed.")]
        public string SealBillingBySerialAndFolio(string user, string password, string serial, string folio)
        {
            int billerid;
            int billingid;
            bool isbillfound = false;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable usertable;
            PACFD.Rules.ElectronicBillingType electronicbillingtype;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;
            PACFD.DataAccess.BillingsDataSet billingdataset;
            PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable billsearchtable;
            System.Xml.XmlDocument doc;
            ErrorManager error;

            this.WriteTrace(new Exception(string.Format("Sealing billing IP: {0} \nRequest: serial - {1}, folio - {2}", this.Context.Request.UserHostAddress, serial, folio)));

            using (usertable = this.IsUserValid(user, password))
            {
                if (usertable.Count < 1)
                {
                    LogManager.Write("User is not valid", "Document parsed, not user valid. PACFD system ok, returning error.", LogFlag.Error);
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = usertable[0].BillerID;
            }

            using (billertable = (new PACFD.Rules.Billers()).SelectByID(billerid))
            {
                switch (billertable[0].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 2: 
                        electronicbillingtype = (PACFD.Rules.ElectronicBillingType)billertable[0].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }
            }

            if (electronicbillingtype == PACFD.Rules.ElectronicBillingType.CBB)
            {
                LogManager.Write("Electronic level is not valid", "System ok, biller seal level access not valid, returning error.", LogFlag.Error);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillerElectronicBillingTypeIsNotValid(
                    WebServiceErrorType.BillerElectronicBillingTypeIsNotValid.ToDescription())).OuterXml;
            }

            using (billsearchtable = (new PACFD.Rules.Billings()).Search(PACFD.Rules.Billings.SearchPetitionFromFlag.WebService,
                billerid, null, serial, folio, (DateTime?)null, true, (bool?)false, null, (int?)null, null, null, null))
            {
                if (billsearchtable.Count < 1)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), serial, folio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                foreach (PACFD.DataAccess.BillingsDataSet.BillingsSearchRow row in billsearchtable)
                {
                    if (row.Serial.Trim() == serial.Trim() && row.Folio.Trim() == folio.Trim())
                    {
                        isbillfound = true;
                        break;
                    }
                }

                if (!isbillfound)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), serial, folio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                if (!billsearchtable[billerid].Active)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing is not active."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), serial, folio), -1, -1, WebServiceErrorType.BillingIsNotActive, string.Empty)).OuterXml;
                }

                billingid = billsearchtable[0].BillerID;
            }

            error = new ErrorManager();

            if (!(new PACFD.Rules.Billings()).SealCFD(billingid, ref error))
            {
                LogManager.WriteError(new Exception(string.Format("Sealing error, code: {0} message: {1}", error.Error.Code, error.Error.Message)));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), serial, folio), -1, -1, WebServiceErrorType.BillingIsNotActive, string.Empty)).OuterXml;
            }

            doc = new System.Xml.XmlDocument();

            using (billingdataset = (new PACFD.Rules.Billings()).GetFullBilling(billingid))
            {
                try
                {
                    doc.LoadXml((new PACFD.Rules.Billings()).GetXml(billingdataset));
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    this.WriteTrace(new Exception(string.Format("Sealing success system error loading xml billing, IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.SystemError.ToDescription(), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
                }
            }

            this.WriteTrace(new Exception(string.Format("Sealing billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));

            return doc.OuterXml;
        }
        /// <summary>
        /// Seal a billig by external folio.
        /// </summary>
        /// <param name="user">User name.</param>
        /// <param name="password">User password.</param>
        /// <param name="externalfolio">External folio to seal.</param>
        /// <returns>If success return a xml with the seal result else return a xml with the error.</returns>
        [WebMethod(Description = "<b>Seal</b> an existing billing. User name and password are needed.")]
        public string SealBillingByExternalFolio(string user, string password, string externalfolio)
        {
            int billerid;
            int billingid;
            bool isbillfound = false;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable usertable;
            PACFD.Rules.ElectronicBillingType electronicbillingtype;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;
            PACFD.DataAccess.BillingsDataSet billingdataset;
            PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable billsearchtable;
            System.Xml.XmlDocument doc;
            ErrorManager error;

            this.WriteTrace(new Exception(string.Format("Sealing billing IP: {0} \nRequest: external folio - {1}", this.Context.Request.UserHostAddress, externalfolio)));

            using (usertable = this.IsUserValid(user, password))
            {
                if (usertable.Count < 1)
                {
                    LogManager.Write("User is not valid", "Document parsed, not user valid. PACFD system ok, returning error.", LogFlag.Error);
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = usertable[0].BillerID;
            }

            using (billertable = (new PACFD.Rules.Billers()).SelectByID(billerid))
            {
                switch (billertable[0].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 2: electronicbillingtype = (PACFD.Rules.ElectronicBillingType)billertable[0].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronicbillingtype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }
            }

            if (electronicbillingtype == PACFD.Rules.ElectronicBillingType.CBB)
            {
                LogManager.Write("Electronic level is not valid", "System ok, biller seal level access not valid, returning error.", LogFlag.Error);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillerElectronicBillingTypeIsNotValid(
                    WebServiceErrorType.BillerElectronicBillingTypeIsNotValid.ToDescription())).OuterXml;
            }

            using (billsearchtable = (new PACFD.Rules.Billings()).SelectByExternalFolio(billerid, externalfolio))
            {
                if (billsearchtable.Count < 1)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                foreach (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioRow row in billsearchtable)
                {
                    if (row.ExternalFolio.Trim() == externalfolio.Trim())
                    {
                        isbillfound = true;
                        break;
                    }
                }

                if (!isbillfound)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing not found."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
                }

                if (!billsearchtable[billerid].Active)
                {
                    LogManager.WriteError(new Exception("Sealing error, billing is not active."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingIsNotActive, string.Empty)).OuterXml;
                }

                billingid = billsearchtable[0].BillerID;
            }

            error = new ErrorManager();

            if (!(new PACFD.Rules.Billings()).SealCFD(billingid, ref error))
            {
                LogManager.WriteError(new Exception(string.Format("Sealing error, code: {0} message: {1}", error.Error.Code, error.Error.Message)));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    string.Format(WebServiceErrorType.BillingAlreadyCanceled.ToDescription(), string.Empty, externalfolio), -1, -1, WebServiceErrorType.BillingIsNotActive, string.Empty)).OuterXml;
            }

            doc = new System.Xml.XmlDocument();

            using (billingdataset = (new PACFD.Rules.Billings()).GetFullBilling(billingid))
            {
                try
                {
                    doc.LoadXml((new PACFD.Rules.Billings()).GetXml(billingdataset));
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    this.WriteTrace(new Exception(string.Format("Sealing success system error loading xml billing, IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.SystemError.ToDescription(), -1, -1, WebServiceErrorType.SystemError, string.Empty)).OuterXml;
                }
            }

            this.WriteTrace(new Exception(string.Format("Sealing billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));

            return doc.OuterXml;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [WebMethod(Description = "Get a generated billing <b>reports</b> from a month. User name and password are needed."
            + "<br/>If the report doesn't exist then the report is generated.")]
        public String GetMonthlyReport(String user, String password, int year, int month)
        {
            DateTime dtNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime dtCom = new DateTime(year, month, 1);

            if (dtNow < dtCom)
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    String.Format(WebServiceErrorType.InvalidDate.ToDescription(), (month < 10 ? "0" + month.ToString() : month.ToString()),
                        year, DateTime.Now.ToString("MM/yyyy")), -1, -1, WebServiceErrorType.InvalidDate, String.Empty)).OuterXml;

            PACFD.Rules.Billers bill = new PACFD.Rules.Billers();
            PACFD.DataAccess.BillersDataSet.BillersDataTable dtBill;

            PACFD.Rules.Reports report = new PACFD.Rules.Reports();
            PACFD.DataAccess.ReportsDataSet.BillerMonthlyReports_GetByDateDataTable t;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table;
            System.Xml.XmlDocument doc;
            StringBuilder sb;
            int billerid;
            byte months = 0;

            this.WriteTrace(new Exception(string.Format("GetMonthlyReport billing IP: {0} \nRequest: year - {1}, month - {2}", this.Context.Request.UserHostAddress, year, month)));

            try
            {
                months = Convert.ToByte(month);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.MonthNotExist.ToDescription(), -1, -1, WebServiceErrorType.MonthNotExist, string.Empty)).OuterXml;
            }

            using (table = this.IsUserValid(user, password))
            {
                if (table.Count < 1)
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;

                billerid = table[0].BillerID;

                dtBill = bill.SelectByID(billerid);

                if (dtBill.Count > 0)
                    if ((PACFD.Rules.ElectronicBillingType)dtBill[0].ElectronicBillingType != PACFD.Rules.ElectronicBillingType.CFD)
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            WebServiceErrorType.BillerElectronicBillingTypeForMonthllyReportIsNotValid.ToDescription(),
                                -1, -1, WebServiceErrorType.BillerElectronicBillingTypeForMonthllyReportIsNotValid, String.Empty)).OuterXml;
            }

            while (true)
            {
                t = report.GetReportByDate(billerid, year, months);

                if (t.Count < 1)
                {
                    String r = report.GenerateReport(billerid, year, Convert.ToInt32(months), -1, user);

                    if (r == null)
                    {
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            String.Format(WebServiceErrorType.NotReportGeneratedInDate.ToDescription(), year, month), -1, -1, WebServiceErrorType.NotReportGeneratedInDate, string.Empty)).OuterXml;
                    }
                    else
                    { continue; }
                }
                else
                {
                    String FileName = "2" + t[0].RFC +
                        (t[0].Month < 10 ? "0" + t[0].Month.ToString() : t[0].Month.ToString()) + t[0].Year + ".txt";

                    sb = new StringBuilder();
                    sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                    sb.Append(string.Empty);
                    sb.AppendLine("<reporte>");
                    sb.AppendLine(String.Format("<detalles año=\"{0}\" mes=\"{1}\" contenido=\"{2}\" nombre=\"{3}\"/>",
                        year.ToString(), months.ToString(), t[0].Report.Trim(), FileName));
                    sb.AppendLine("</reporte>");

                    doc = new System.Xml.XmlDocument();
                    doc.LoadXml(sb.ToString());

                    this.WriteTrace(new Exception(string.Format("GetMonthlyReport billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, doc.OuterXml)));

                    return t[0].Report.Trim() != String.Empty ? doc.OuterXml :
                        this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                            String.Format(WebServiceErrorType.DateReportIsEmpty.ToDescription(), year, months), -1, -1, WebServiceErrorType.DateReportIsEmpty, string.Empty)).OuterXml;
                }
            }
        }
        /// <summary>
        /// Send an email to the destinataries.
        /// </summary>
        /// <param name="document">Docuemnt with all the data needed (billing, username, billing type, etc.)</param>
        /// <returns></returns>
        [WebMethod(Description = "Send an <b>email</b> about a new billing created to a client. User name and password are needed.")]
        public string SendNewBillingMailByXMLDocument(System.Xml.XmlDocument document)
        {
            const string token_user = "usuario";
            const string token_billing = "facturacion";
            const string token_bill = "factura";
            const string token_serial = "serie";
            const string token_folio = "folio";
            const string token_externalfolio = "folioexterno";
            const string token_name = "nombre";
            const string token_password = "contraseña";
            const string token_mail = "correo";
            const string token_to = "para";
            const string token_bcc = "cco";
            const string token_cc = "cc";
            const string token_subject = "asunto";
            System.Xml.XmlNode node;
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table;
            int billerid = 0
                , billingid = 0
            ;
            string serial = string.Empty
                , folio = string.Empty
                , serror = string.Empty
                , externalfolio = string.Empty
                , subject = string.Empty
            ;
            PACFD.Rules.BillingMailSender mail;
            PACFD.Rules.Mail.MailSendResult errormail;
            PACFD.Rules.Billings billings;
            PACFD.DataAccess.BillingsDataSet billingdataset;
            //PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable searchbillingtable;
            StringBuilder sb;
            PACFD.Rules.ElectronicBillingType electronictype;
            PACFD.DataAccess.BillersDataSet.BillersDataTable billertable;
            PACFD.Rules.Billers biller;
            string[] sendmailtolist = null
                , sendmailtolist_bcc = null
                , sendmailtolist_cc = null
                ;
            //EmailHelperClass helperclass = new EmailHelperClass();


            this.WriteTrace(new Exception(string.Format("Sending mail billing XML document IP: {0} \nRequest: {1}", this.Context.Request.UserHostAddress, document.OuterXml)));

            if (document == null)
            {
                LogManager.WriteError(new Exception("Document is null. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.DocumentNull.ToDescription(), -1, -1, WebServiceErrorType.DocumentNull, string.Empty)).OuterXml;
            }

            node = document.SelectSingleNode(token_billing);

            if (node == null)
            {
                LogManager.WriteError(new Exception("Sending mail, billing node is null. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeBillingNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeBillingNotFound, string.Empty)).OuterXml;
            }

            node = node.SelectSingleNode(token_user);

            if (node == null)
            {
                LogManager.WriteError(new Exception("Sending mail, user node is null. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeUserNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeUserNotFound, string.Empty)).OuterXml;
            }

            if (node.Attributes[token_name] == null || node.Attributes[token_password] == null)
            {
                LogManager.WriteError(new Exception("Sending mail, user node is null. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeUserAttributeUserOrPasswordNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeUserAttributeUserOrPasswordNotFound, string.Empty)).OuterXml;
            }

            using (table = this.IsUserValid(node.Attributes[token_name].Value, node.Attributes[token_password].Value))
            {
                if (table.Count < 1)
                {
                    LogManager.WriteError(new Exception("Document parsed, not biller(company) in xml found. PACFD system ok, returning null."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.UserOrPasswordNotValid.ToDescription(), -1, -1, WebServiceErrorType.UserOrPasswordNotValid, string.Empty)).OuterXml;
                }

                billerid = table[0].BillerID;
            }

            biller = new PACFD.Rules.Billers();

            using (billertable = biller.SelectByID(billerid))
            {
                switch (billertable[0].ElectronicBillingType)
                {
                    case 0:
                    case 1:
                    case 2: electronictype = (PACFD.Rules.ElectronicBillingType)billertable[0].ElectronicBillingType; break;
                    case 99:
                    default:
                        electronictype = PACFD.Rules.ElectronicBillingType.Indeterminate; break;
                }
            }

            node = node.ParentNode;
            node = node.SelectSingleNode(token_bill);

            if (node == null)
            {
                LogManager.WriteError(new Exception("Document parsed, node bill(company) in xml not found. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.NodeBillingNotFound.ToDescription(), -1, -1, WebServiceErrorType.NodeBillingNotFound, string.Empty)).OuterXml;
            }

            if ((node.Attributes[token_folio] == null || string.IsNullOrEmpty(node.Attributes[token_folio].Value))
                && (node.Attributes[token_externalfolio] == null || string.IsNullOrEmpty(node.Attributes[token_externalfolio].Value)))
            {
                LogManager.WriteError(new Exception("Document parsed, folio and external folio not found. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.FolioOrSerieNotFound.ToDescription(), -1, -1, WebServiceErrorType.FolioOrSerieNotFound, string.Empty)).OuterXml;
            }

            serial = node.Attributes[token_serial] == null ? string.Empty : node.Attributes[token_serial].Value.Trim();
            folio = node.Attributes[token_folio] == null ? null : node.Attributes[token_folio].Value.Trim();
            externalfolio = node.Attributes[token_externalfolio] == null ? null : node.Attributes[token_externalfolio].Value.Trim();

            billings = new PACFD.Rules.Billings();
            billingid = 0;

            if (!string.IsNullOrEmpty(folio))
            {
                using (PACFD.DataAccess.BillingsDataSet.BillingsSearchDataTable searchbillingtable =
                    billings.Search(PACFD.Rules.Billings.SearchPetitionFromFlag.WebService, billerid, null, serial, folio, null, null, null, null, null, null, null, null))
                {
                    if (searchbillingtable.Count < 1)
                    {
                        LogManager.WriteError(new Exception("Document parsed, not biller found with serie-folio. PACFD system ok, returning null."));
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.BillerBillingError.ToDescription(), -1, -1, WebServiceErrorType.BillerBillingError, string.Empty)).OuterXml;
                    }

                    foreach (PACFD.DataAccess.BillingsDataSet.BillingsSearchRow item in searchbillingtable)
                    {
                        if (string.IsNullOrEmpty(item.Folio))
                        { continue; }

                        if (item.Folio.Trim() != folio)
                        { continue; }

                        if (string.IsNullOrEmpty(serial) == string.IsNullOrEmpty(item.Serial))
                        {
                            billingid = item.BillingID;
                            break;
                        }

                        if (serial != item.Serial.Trim())
                        { continue; }

                        billingid = item.BillingID;
                        break;
                    }
                }
            }
            else
            {
                using (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioDataTable searchbillingtable = billings.SelectByExternalFolio(billerid, externalfolio))
                {
                    if (searchbillingtable.Count < 1)
                    {
                        LogManager.WriteError(new Exception("Document parsed, not biller found with external folio. PACFD system ok, returning null."));
                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.BillerBillingError.ToDescription(), -1, -1, WebServiceErrorType.BillerBillingError, string.Empty)).OuterXml;
                    }

                    foreach (PACFD.DataAccess.BillingsDataSet.GetByExternalFolioRow row in searchbillingtable)
                    {
                        if (row.IsExternalFolioNull())
                        { continue; }

                        if (string.IsNullOrEmpty(row.ExternalFolio))
                        { continue; }

                        if (row.ExternalFolio.Trim() != externalfolio)
                        { continue; }

                        billingid = row.BillingID;
                        break;
                    }
                }
            }

            if (billingid < 1)
            {
                LogManager.WriteError(new Exception("Document parsed, not bill found. PACFD system ok, returning null."));
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.BillerBillingError.ToDescription(), -1, -1, WebServiceErrorType.BillerBillingError, string.Empty)).OuterXml;
            }

            using (billingdataset = billings.GetFullBilling(billingid))
            {
                if (billingdataset == null)
                {
                    LogManager.WriteError(new Exception("Document parsed, not bill found. PACFD system ok, returning null."));
                    return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.BillerBillingError.ToDescription(), -1, -1, WebServiceErrorType.BillerBillingError, string.Empty)).OuterXml;
                }

                mail = new PACFD.Rules.BillingMailSender(this.Context.Request.PhysicalApplicationPath + @"Includes\Mail\Mail.es.xml", "004", billingdataset.Billings[0].BillerID);
                PACFD.Rules.BillingMailSender.SetBillingMail(mail, ref billingdataset, billerid);
                this.AddFilesToMail(mail, electronictype, billingdataset);
                //helperclass.AddFilesToMail(mail, electronictype, billingdataset, node);
            }

            node = document.SelectSingleNode(token_billing);
            node = node.SelectSingleNode(token_mail);
            subject = node.Attributes[token_subject] == null ? null : node.Attributes[token_subject].Value.Trim();

            if (node != null)
            {
                if (node.Attributes[token_to] != null && !string.IsNullOrEmpty(node.Attributes[token_to].Value))
                {
                    mail.Message.To.Clear();
                    sendmailtolist = node.Attributes[token_to].Value.Split(',');

                    for (int i = 0; i < sendmailtolist.Length; i++)
                    {
                        try
                        {
                            mail.Message.To.Add(sendmailtolist[i]);
                            sendmailtolist[i] = string.Format("Exito para ({0}){1}"
                                , sendmailtolist[i]
                                , i + 1 >= sendmailtolist.Length ? string.Empty : ","
                                );
                        }
                        catch (Exception)
                        {
                            sendmailtolist[i] = string.Format("Error para ({0}){1}"
                                , sendmailtolist[i]
                                , i + 1 >= sendmailtolist.Length ? string.Empty : ","
                                );
                        }
                    }

                    if (mail.Message.To.Count < 1)
                    {
                        this.WriteTrace(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.ErrorSendingMail.ToDescription(), -1, -1, WebServiceErrorType.ErrorSendingMailBadFormatVariableTo,
                                string.IsNullOrEmpty(folio) ? externalfolio : string.Format("{0} - {1}", serial, folio)));

                        LogManager.WriteError(new Exception("Can't load message for email. PACFD system error, returning null."));

                        return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                                WebServiceErrorType.ErrorSendingMail.ToDescription(), -1, -1, WebServiceErrorType.ErrorSendingMailBadFormatVariableTo,
                                string.IsNullOrEmpty(folio) ? externalfolio : string.Format("{0} - {1}", serial, folio))).OuterXml;
                    }
                }

                if (node.Attributes[token_bcc] != null && !string.IsNullOrEmpty(node.Attributes[token_bcc].Value))
                {
                    mail.Message.Bcc.Clear();
                    sendmailtolist_bcc = node.Attributes[token_bcc].Value.Split(',');

                    for (int i = 0; i < sendmailtolist_bcc.Length; i++)
                    {
                        try
                        {
                            mail.Message.Bcc.Add(sendmailtolist_bcc[i]);
                            sendmailtolist_bcc[i] = string.Format("Exito cco ({0}){1}"
                                , sendmailtolist_bcc[i]
                                , i + 1 >= sendmailtolist_bcc.Length ? string.Empty : ","
                                );
                        }
                        catch
                        {
                            sendmailtolist_bcc[i] = string.Format("Error cco ({0}){1}"
                                , sendmailtolist_bcc[i]
                                , i + 1 >= sendmailtolist_bcc.Length ? string.Empty : ","
                                );
                        }
                    }
                }

                if (node.Attributes[token_cc] != null && !string.IsNullOrEmpty(node.Attributes[token_cc].Value))
                {
                    mail.Message.Bcc.Clear();
                    sendmailtolist_cc = node.Attributes[token_cc].Value.Split(',');

                    for (int i = 0; i < sendmailtolist_cc.Length; i++)
                    {
                        try
                        {
                            mail.Message.Bcc.Add(sendmailtolist_cc[i]);
                            sendmailtolist_cc[i] = string.Format("Exito cc ({0}){1}"
                                , sendmailtolist_cc[i]
                                , i + 1 >= sendmailtolist_cc.Length ? string.Empty : ","
                                );
                        }
                        catch
                        {
                            sendmailtolist_cc[i] = string.Format("Error cc ({0}){1}"
                                , sendmailtolist_cc[i]
                                , i + 1 >= sendmailtolist_cc.Length ? string.Empty : ","
                                );
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(subject))
            { mail.Message.Subject = subject; }

            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            if (!(errormail = mail.Send()).IsSended)
            {
                if (LogManager.Enabled)
                { LogManager.WriteError(errormail.Error.InnerException); }

                this.WriteTrace(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.ErrorSendingMail.ToDescription(), -1, -1, WebServiceErrorType.ErrorSendingMail,
                        string.IsNullOrEmpty(folio) ? externalfolio : string.Format("{0} - {1}", serial, folio)));

                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                        WebServiceErrorType.ErrorSendingMail.ToDescription(), -1, -1, WebServiceErrorType.ErrorSendingMail,
                        string.IsNullOrEmpty(folio) ? externalfolio : string.Format("{0} - {1}", serial, folio))).OuterXml;
            }

            if (sendmailtolist != null && sendmailtolist.Length > 0)
            { serror = string.Join(string.Empty, sendmailtolist); }

            if (sendmailtolist_bcc != null && sendmailtolist_bcc.Length > 0)
            { serror += string.Join(string.Empty, sendmailtolist_bcc); }

            if (sendmailtolist_cc != null && sendmailtolist_cc.Length > 0)
            { serror += string.Join(string.Empty, sendmailtolist_cc); }

            sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append(string.Empty);
            sb.AppendLine("<facturacion>");
            sb.Append(string.Format("<factura texto=\"Exito al enviar correo. serie='{0}', folio='{1}'.\" Log=\"{2}\" />"
                , serial
                , string.IsNullOrEmpty(folio) ? externalfolio : folio
                , serror
                ));
            sb.AppendLine("</facturacion>");

            document = new System.Xml.XmlDocument();
            document.LoadXml(sb.ToString());

            this.WriteTrace(new Exception(string.Format("Sending mail billing IP: {0} \nResponce: {1}", this.Context.Request.UserHostAddress, document.OuterXml)));

            return document.OuterXml;
        }
        /// <summary>
        /// Send an email to the destinataries.
        /// </summary>
        /// <param name="document">String with all the data needed (billing, username, billing type, etc.)</param>
        /// <returns></returns>
        [WebMethod(Description = "Send an <b>email</b> about a new billing created to a client. User name and password are needed.")]
        public string SendNewBillingMailByString(string sdocument)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            try
            {
                doc.LoadXml(sdocument);
            }
            catch (Exception ex)
            {
                LogManager.WriteWarning(ex);
                return this.GetXmlDocumentError(new PACFD.Rules.BillingsExceptions.BillingExceptionBase(
                    WebServiceErrorType.DocumentErrorFormat.ToDescription(), -1, -1, WebServiceErrorType.DocumentErrorFormat, string.Empty)).OuterXml;
            }

            return this.SendNewBillingMailByXMLDocument(doc);
        }

        public void AddFilesToMail(PACFD.Rules.BillingMailSender mail, PACFD.Rules.ElectronicBillingType electronictype, PACFD.DataAccess.BillingsDataSet billingdataset)
        {
            System.Net.Mail.Attachment at;
            System.Net.Mime.ContentDisposition comp;
            System.IO.Stream stream;

            stream = this.GetPDF(billingdataset.Billings[0].BillingID);

            if (stream != null)
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                at = new System.Net.Mail.Attachment(stream, System.Net.Mime.MediaTypeNames.Application.Pdf);
                comp = at.ContentDisposition;
                comp.CreationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;

                if (electronictype != PACFD.Rules.ElectronicBillingType.CFDI && electronictype != PACFD.Rules.ElectronicBillingType.CFDI3_2 && electronictype != PACFD.Rules.ElectronicBillingType.CFDI3_3 || electronictype != PACFD.Rules.ElectronicBillingType.CFDI4_0)
                    at.Name = string.Format("{0} {1}.pdf", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"));
                else
                    at.Name = billingdataset.Billings[0].IsUUIDNull() ?
                        string.Format("{0} {1}.pdf", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"))
                        : string.Format("{0} {1}.pdf", billingdataset.Billings[0].UUID, DateTime.Now.ToString("MMM dd MM yyyy"));

                mail.Message.Attachments.Add(at);
            }

            stream = null;
            stream = this.GetXml(billingdataset.Billings[0].BillingID, electronictype);
            comp = null;

            if (stream != null && electronictype != PACFD.Rules.ElectronicBillingType.CBB)
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                at = new System.Net.Mail.Attachment(stream, "text/xml");
                comp = at.ContentDisposition;
                comp.CreationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;
                comp.ModificationDate = DateTime.Now;

                if (electronictype != PACFD.Rules.ElectronicBillingType.CFDI && electronictype != PACFD.Rules.ElectronicBillingType.CFDI3_2 && electronictype != PACFD.Rules.ElectronicBillingType.CFDI3_3 || electronictype != PACFD.Rules.ElectronicBillingType.CFDI4_0)
                    at.Name = string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"));
                else
                    at.Name = billingdataset.Billings[0].IsUUIDNull() ?
                            string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"))
                            : string.Format("{0} {1}.xml", billingdataset.Billings[0].UUID, DateTime.Now.ToString("MMM dd MM yyyy"));

                mail.Message.Attachments.Add(at);
            }
        }

        /// <summary>
        /// Get a PDF file from a billing.
        /// </summary>
        /// <param name="billingid">Billing ID used to get the PDF.</param>
        /// <returns>If success return a stream else null.</returns>
        private System.IO.Stream GetPDF(int billingid)
        {
            PACFD.Rules.PrintDocument.BillingDocumentPrintEventArgs r;
            PACFD.Rules.PrintDocument.BillingDocument doc = new PACFD.Rules.PrintDocument.BillingDocument();

            using (System.IO.Stream stream = this.GetPrintConfigStream(billingid))
            {
                if (stream == null)
                { return null; }

                doc.LoadFromXML(stream);
            }

            r = doc.Print(billingid);

            if (r == null)
            {
                LogManager.WriteError(new Exception("Getting pdf, document is null."));
                return null;
            }

            return r.PDFDocumnet;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingid"></param>
        /// <param name="electronictype"></param>
        /// <returns></returns>
        private System.IO.Stream GetXml(int billingid, PACFD.Rules.ElectronicBillingType electronictype)
        {
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            string fileName = string.Empty;

            byte[] b = billing.GetXml(billingid, ref fileName);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();

            if (b == null || electronictype == PACFD.Rules.ElectronicBillingType.CBB)
            { return null; }

            stream.Write(b, 0, b.Length);

            return stream;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingid"></param>
        /// <returns></returns>
        private System.IO.Stream GetPrintConfigStream(int billingid)
        {
            string s;
            System.IO.MemoryStream stream = null;
            PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable;
            PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
            PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;
            PACFD.Rules.PrintTemplates p = new PACFD.Rules.PrintTemplates();
            System.Text.UTF8Encoding ut8;
            byte[] b;

            billtable = billing.SelectByID(billingid);
            billing = null;

            if (billtable.Count < 1)
            {
                billtable.Dispose();
                billtable = null;
                return null;
            }

            table = p.SelectByID(billtable[0].PrintTemplateID);
            p = null;
            stream = new System.IO.MemoryStream();

            if (table.Count < 1)
            {
                switch ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType)
                {
                    case PACFD.Rules.ElectronicBillingType.CFD:
                    case PACFD.Rules.ElectronicBillingType.CFD2_2:
                        table = p.SelectByID(1); break;
                    case PACFD.Rules.ElectronicBillingType.CFDI:
                    case PACFD.Rules.ElectronicBillingType.CFDI3_2:
                    case PACFD.Rules.ElectronicBillingType.CFDI3_3:
                        table = p.SelectByID(2); break;
                    case PACFD.Rules.ElectronicBillingType.CFDI4_0:
                        table = p.SelectByID(2); break;
                    case PACFD.Rules.ElectronicBillingType.CBB: table = p.SelectByID(3); break;
                    case PACFD.Rules.ElectronicBillingType.Indeterminate:
                    default: break;
                }
            }

            if (table.Count < 1)
            {
                table.Dispose();
                table = null;

                s = ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CBB ?
                        PACFD.Rules.PrintTemplates.GetStringDefaultCBB() :
                           ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFD
                           || ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFD2_2
                           ? PACFD.Rules.PrintTemplates.GetStringDefaultCFD() :
                                   ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFDI
                                   || ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFDI3_2
                                   || ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFDI3_3
                                    || ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFDI4_0
                                   ? PACFD.Rules.PrintTemplates.GetStringDefaultCFDI() : string.Empty;
            }
            else
            {
                s = table[0].Text;
                table.Dispose();
                table = null;
            }

            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the posistion
            ut8 = new System.Text.UTF8Encoding(); //avoid text without UTF8 Encoding format
            b = ut8.GetBytes(s); //temporal bytes array nedeed
            stream.Write(b, 0, b.Length); //write array
            stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the position

            return stream;
        }
        /// <summary>
        /// Internal use for get the xlst's path.
        /// </summary>
        /// <returns>Return a string qith the xlst's path.</returns>
        private string get_pathXlst()
        {
            return this.Context.Request.MapPath("~/Includes/Xlst/");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        private void WriteTrace(Exception ex)
        {
            if (LogManager.WebServiceTraceEnable && LogManager.Enabled)
            { LogManager.WriteStackTrace(ex); }
        }
        /// <summary>
        /// Check for errors in dataset and write a in the log if found.
        /// </summary>
        /// <param name="dataset">DataSet to verify.</param>
        /// <returns>If success return true else false.</returns>
        private bool BillingInsert_CheckForError(PACFD.DataAccess.BillingsDataSet dataset)
        {
            if (dataset == null)
            {
                LogManager.Write("Insert Billing WebService", "Document parsed, not billing in xml found. PACFD system ok, returning null.", LogFlag.Error);
                return false;
            }

            if (dataset.Billings.Count < 1)
            {
                LogManager.Write("Insert Billing WebService", "Document parsed, not billing in xml found. PACFD system ok, returning null.", LogFlag.Error);
                return false;
            }

            if (dataset.BillingsDetails.Count < 1)
            {
                LogManager.Write("Insert Billing WebService", "Document parsed, not concept(s) in xml found. PACFD system ok, returning null.", LogFlag.Error);
                return false;
            }

            if (dataset.BillingsReceptors.Count < 1)
            {
                LogManager.Write("Insert Billing WebService", "Document parsed, not receptor in xml found. PACFD system ok, returning null.", LogFlag.Error);
                return false;
            }

            if (dataset.BillingsBillers.Count < 1)
            {
                LogManager.Write("Insert Billing WebService", "Document parsed, not biller(company) in xml found. PACFD system ok, returning null.", LogFlag.Error);
                return false;
            }

            return true;
        }
        /// <summary>
        /// Search for a user name and password in the web service access table.
        /// </summary>
        /// <param name="user">User name to look for. Not encrypted string.</param>
        /// <param name="password">Password of the user. Not encripted string.</param>
        /// <returns>Return a PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table.</returns>
        private PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable IsUserValid(string user, string password)
        {
            PACFD.DataAccess.WebServiceAccessesDataSet.SearchByNamePasswordDataTable table;
            PACFD.Rules.WebServiceAccesses w = new PACFD.Rules.WebServiceAccesses();
            table = w.SelectByNamePassword(user, PACFD.Common.Cryptography.EncryptUnivisitString(password));
            return table;
        }
        /// <summary>
        /// Get a System.Xml.XmlDocument with the error.
        /// </summary>
        /// <param name="ex">Error to be write in the xml.</param>
        /// <returns>Return a System.Xml.XmlDocument with the error.</returns>
        private System.Xml.XmlDocument GetXmlDocumentError(PACFD.Rules.BillingsExceptions.BillingExceptionBase ex)
        {
            StringBuilder sb = new StringBuilder();
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            this.WriteTrace(new Exception(string.Format("Error response. IP:{0} Message: {1}. Mothod Caller StackTrace: {2}", this.Context.Request.UserHostAddress, ex.Message, ex.StackTrace)));

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append(string.Empty);
            sb.AppendLine("<facturacion>");
            sb.AppendLine(string.Format("<error texto=\"{0}\" codigo=\"{1}\" />", ex.Message, (int)ex.ErrorCode));
            sb.AppendLine("</facturacion>");
            doc.LoadXml(sb.ToString());
            return doc;
        }
    }
}