﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PACFD.WebServices
{
    class XMLHtmlCodesParser
    {
        List<string> htmlnumber = new List<string>() { "&#38;", };
        List<string> htmlname = new List<string>() { "&amp;", };
        List<string> token = new List<string>() { "[_token_amp_#38;_]", };
        List<string> htmlchar = new List<string>() { "&", };

        /// <summary>
        /// Convert char in a xml to html number.
        /// </summary>
        /// <param name="text">Text to be parse.</param>
        /// <returns>Return a string value.</returns>
        public string ParseToHtmlNumber(string text)
        {
            for (int i = 0; i < this.htmlnumber.Count; i++)
            { text = text.Replace(this.htmlnumber[i], this.token[i]); }

            for (int i = 0; i < this.htmlname.Count; i++)
            { text = text.Replace(this.htmlname[i], this.token[i]); }

            for (int i = 0; i < this.htmlchar.Count; i++)
            { text = text.Replace(this.htmlchar[i], this.token[i]); }

            for (int i = 0; i < this.token.Count; i++)
            { text = text.Replace(this.token[i], this.htmlnumber[i]); }

            return text;
        }
        /// <summary>
        /// Convert html number in a xml to char.
        /// </summary>
        /// <param name="text">Text to be parse.</param>
        /// <returns>Return a string value.</returns>
        public string ParseToChar(string text)
        {
            for (int i = 0; i < this.htmlnumber.Count; i++)
            { text = text.Replace(this.htmlnumber[i], this.token[i]); }

            for (int i = 0; i < this.htmlname.Count; i++)
            { text = text.Replace(this.htmlname[i], this.token[i]); }

            for (int i = 0; i < this.token.Count; i++)
            { text = text.Replace(this.token[i], this.htmlnumber[i]); }

            return text;
        }
    }
}
