﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PACFD.Common;
using System.Collections;

namespace PACFD.WebServices
{
    ///// <summary>
    ///// Class used be the method SendNewBillingMailByXMLDocument to handle special Pdf and Xml options.
    ///// </summary>
    //class EmailHelperClass
    //{
    //    DinamycDocumentModifierCollection modifier = new DinamycDocumentModifierCollection();

    //    class DinamycDocumentModifierItem
    //    {
    //        public string TagID { get; set; }
    //        public List<string> Name { get; private set; }
    //        public List<string> Value { get; private set; }

    //        public DinamycDocumentModifierItem()
    //        {
    //            this.Name = new List<string>();
    //            this.Value = new List<string>();
    //        }
    //    }

    //    class DinamycDocumentModifierCollection : List<DinamycDocumentModifierItem>
    //    {
    //        public void ParseNode(System.Xml.XmlNode parent)
    //        {
    //            DinamycDocumentModifierItem item;

    //            foreach (System.Xml.XmlNode n in parent.ChildNodes)
    //            {
    //                item = new DinamycDocumentModifierItem();

    //                foreach (System.Xml.XmlAttribute a in n.Attributes)
    //                {
    //                    if (a.Name.ToLower() == "id")
    //                    {
    //                        item.TagID = a.Value;
    //                        continue;
    //                    }

    //                    item.Name.Add(a.Name);
    //                    item.Value.Add(a.Value);
    //                }

    //                this.Add(item);
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Add PDF and XML to the the mail.
    //    /// </summary>
    //    /// <param name="mail">Mail class to add the files.</param>
    //    /// <param name="billingid">Billing ID used as filter.</param>
    //    /// <param name="electronictype">Electronic billing type.</param>
    //    public void AddFilesToMail(PACFD.Rules.BillingMailSender mail, PACFD.Rules.ElectronicBillingType electronictype, PACFD.DataAccess.BillingsDataSet billingdataset
    //        , System.Xml.XmlNode billingnode)
    //    {
    //        System.Net.Mail.Attachment at;
    //        System.Net.Mime.ContentDisposition comp;
    //        System.IO.Stream stream;

    //        this.modifier.ParseNode(billingnode);
    //        stream = this.GetPDF(billingdataset.Billings[0].BillingID);

    //        if (stream != null)
    //        {
    //            stream.Seek(0, System.IO.SeekOrigin.Begin);
    //            at = new System.Net.Mail.Attachment(stream, System.Net.Mime.MediaTypeNames.Application.Pdf);
    //            comp = at.ContentDisposition;
    //            comp.CreationDate = DateTime.Now;
    //            comp.ModificationDate = DateTime.Now;
    //            comp.ModificationDate = DateTime.Now;

    //            if (electronictype != PACFD.Rules.ElectronicBillingType.CFDI)
    //            { at.Name = string.Format("{0} {1}.pdf", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy")); }
    //            else
    //            {
    //                at.Name = billingdataset.Billings[0].IsUUIDNull() ?
    //                    string.Format("{0} {1}.pdf", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"))
    //                    : string.Format("{0} {1}.pdf", billingdataset.Billings[0].UUID, DateTime.Now.ToString("MMM dd MM yyyy"));
    //            }

    //            mail.Message.Attachments.Add(at);
    //        }

    //        stream = null;
    //        stream = this.GetXml(billingdataset.Billings[0].BillingID, electronictype);
    //        comp = null;

    //        if (stream != null && electronictype != PACFD.Rules.ElectronicBillingType.CBB)
    //        {
    //            stream.Seek(0, System.IO.SeekOrigin.Begin);
    //            at = new System.Net.Mail.Attachment(stream, "text/xml");
    //            comp = at.ContentDisposition;
    //            comp.CreationDate = DateTime.Now;
    //            comp.ModificationDate = DateTime.Now;
    //            comp.ModificationDate = DateTime.Now;

    //            if (electronictype != PACFD.Rules.ElectronicBillingType.CFDI)
    //            { at.Name = string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy")); }
    //            else
    //            {
    //                at.Name = billingdataset.Billings[0].IsUUIDNull() ?
    //                        string.Format("{0} {1}.xml", mail.Parameters[PACFD.Rules.BillingMailSender.LBL_FOLIO], DateTime.Now.ToString("MMM dd MM yyyy"))
    //                        : string.Format("{0} {1}.xml", billingdataset.Billings[0].UUID, DateTime.Now.ToString("MMM dd MM yyyy"));
    //            }

    //            mail.Message.Attachments.Add(at);
    //        }
    //    }
    //    /// <summary>
    //    /// Get a PDF file from a billing. Using especial modifications values to alter the PDF
    //    /// </summary>
    //    /// <param name="billingid">Billing ID used to get the PDF.</param>
    //    /// <returns>If success return a stream else null.</returns>
    //    private System.IO.Stream GetPDF(int billingid)
    //    {
    //        PACFD.Rules.PrintDocument.BillingDocumentPrintEventArgs r;
    //        PACFD.Rules.PrintDocument.BillingDocument doc = new PACFD.Rules.PrintDocument.BillingDocument();

    //        using (System.IO.Stream stream = this.GetPrintConfigStream(billingid))
    //        {
    //            if (stream == null)
    //            { return null; }

    //            doc.LoadFromXML(stream);
    //        }

    //        doc.BeforePrint += new PACFD.Rules.PrintDocument.BillingDocumentBeforePrintEventHandler(PdfDocument_BeforePrint);
    //        r = doc.Print(billingid);
    //        doc.BeforePrint -= PdfDocument_BeforePrint;

    //        if (r == null)
    //        {
    //            LogManager.WriteError(new Exception("Getting pdf, document is null."));
    //            return null;
    //        }

    //        return r.PDFDocumnet;
    //    }
    //    /// <summary>
    //    /// Event send be the PrintDocument class before print.
    //    /// </summary>
    //    /// <param name="e"></param>
    //    private void PdfDocument_BeforePrint(PACFD.Rules.PrintDocument.BillingDocumentBeforePrintEventArgs e)
    //    {
    //        int ivalue;
    //        System.Reflection.PropertyInfo[] propertie;
    //        List<PACFD.Rules.PrintDocument.IPrintControl> controls = new List<PACFD.Rules.PrintDocument.IPrintControl>();

    //        foreach (DinamycDocumentModifierItem d in this.modifier)
    //        {
    //            foreach (PACFD.Rules.PrintDocument.PrintLayer layer in e.Layers)
    //            { controls.AddRange(GetControlById(d.TagID, layer)); }
    //        }

    //        foreach (DinamycDocumentModifierItem d in this.modifier)
    //        {
    //            foreach (PACFD.Rules.PrintDocument.IPrintControl c in controls)
    //            {
    //                if (d.TagID != c.ID)
    //                { continue; }

    //                propertie = c.GetType().GetProperties();//System.Reflection.BindingFlags.Default);
    //                ivalue = -1;

    //                foreach (string v in d.Name)
    //                {
    //                    ivalue++;
    //                    foreach (System.Reflection.PropertyInfo p in propertie)
    //                    {
    //                        if (v.ToLower() != p.Name.ToLower())
    //                        { continue; }

    //                        if (propertie.Length > 0)
    //                        {
    //                            p.SetValue(c, d.Value[ivalue], null);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    private List<PACFD.Rules.PrintDocument.IPrintControl> GetControlById(string s, PACFD.Rules.PrintDocument.IPrintControl e)
    //    {
    //        List<PACFD.Rules.PrintDocument.IPrintControl> result = new List<PACFD.Rules.PrintDocument.IPrintControl>();

    //        if (e.ID.Trim().ToLower() == s.Trim().ToLower())
    //        {
    //            result.Add(e);
    //        }

    //        if (e.GetType() == typeof(PACFD.Rules.PrintDocument.PrintLayer))
    //        {
    //            foreach (PACFD.Rules.PrintDocument.IPrintControl control in ((PACFD.Rules.PrintDocument.PrintLayer)e).PrintControls)
    //            {
    //                result.AddRange(this.GetControlById(s, control));
    //            }
    //        }

    //        return result;
    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="billingid"></param>
    //    /// <returns></returns>
    //    private System.IO.Stream GetPrintConfigStream(int billingid)
    //    {
    //        string s;
    //        System.IO.MemoryStream stream = null;
    //        PACFD.DataAccess.BillingsDataSet.BillingsDataTable billtable;
    //        PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
    //        PACFD.DataAccess.PrintTemplatesDataSet.PrintTemplatesDataTable table;
    //        PACFD.Rules.PrintTemplates p = new PACFD.Rules.PrintTemplates();
    //        System.Text.UTF8Encoding ut8;
    //        byte[] b;

    //        billtable = billing.SelectByID(billingid);
    //        billing = null;

    //        if (billtable.Count < 1)
    //        {
    //            billtable.Dispose();
    //            billtable = null;
    //            return null;
    //        }

    //        table = p.SelectByID(billtable[0].PrintTemplateID);
    //        p = null;
    //        stream = new System.IO.MemoryStream();

    //        if (table.Count < 1)
    //        {
    //            switch ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType)
    //            {
    //                case PACFD.Rules.ElectronicBillingType.CFD: table = p.SelectByID(1); break;
    //                case PACFD.Rules.ElectronicBillingType.CFDI: table = p.SelectByID(2); break;
    //                case PACFD.Rules.ElectronicBillingType.CBB: table = p.SelectByID(3); break;
    //                case PACFD.Rules.ElectronicBillingType.Indeterminate:
    //                default: break;
    //            }
    //        }

    //        if (table.Count < 1)
    //        {
    //            table.Dispose();
    //            table = null;

    //            s = ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CBB ?
    //                    PACFD.Rules.PrintTemplates.GetStringDefaultCBB() :
    //                       ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFD ?
    //                        PACFD.Rules.PrintTemplates.GetStringDefaultCFD() :
    //                               ((PACFD.Rules.ElectronicBillingType)billtable[0].ElectronicBillingType) == PACFD.Rules.ElectronicBillingType.CFDI ?
    //                                   PACFD.Rules.PrintTemplates.GetStringDefaultCFDI() : string.Empty;
    //        }
    //        else
    //        {
    //            s = table[0].Text;
    //            table.Dispose();
    //            table = null;
    //        }

    //        stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the posistion
    //        ut8 = new System.Text.UTF8Encoding(); //avoid text without UTF8 Encoding format
    //        b = ut8.GetBytes(s); //temporal bytes array nedeed
    //        stream.Write(b, 0, b.Length); //write array
    //        stream.Seek(0, System.IO.SeekOrigin.Begin); //relocate the position

    //        return stream;
    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="billingid"></param>
    //    /// <param name="electronictype"></param>
    //    /// <returns></returns>
    //    private System.IO.Stream GetXml(int billingid, PACFD.Rules.ElectronicBillingType electronictype)
    //    {
    //        PACFD.Rules.Billings billing = new PACFD.Rules.Billings();
    //        string fileName = string.Empty;

    //        byte[] b = billing.GetXml(billingid, ref fileName);
    //        System.IO.MemoryStream stream = new System.IO.MemoryStream();

    //        if (b == null || electronictype == PACFD.Rules.ElectronicBillingType.CBB)
    //        { return null; }

    //        stream.Write(b, 0, b.Length);

    //        return stream;
    //    }
    //}
}
